This repository has moved to [Codeberg](https://codeberg.org/BarrOff/nimPoP)


## nimPoP

This is a reimplementation of [SDLPoP](https://github.com/NagyD/SDLPoP) in Nim.
I do this mainly for educational reasons to learn about Nim. The initial step
was porting the C-code to Nim, as close as possible. As a result of this the Nim
code is rather not compliant to Nim coding standards, yet.


## Current Status

The basic code structure has been ported, except the screenshot and recording
features. Those have been postponed until the code is stable enough.
Therefore some bugs are still present, a (probably incomplete list) can be found
in the `docs` directory
However, the basic game is playable, as far as I could test it.

Mods have **not** been tested!


## Prerequisites

* [SDL](https://libsdl.org/) >= 2.0.5
* [SDL_image](https://www.libsdl.org/projects/SDL_image/) >= 2.0.0
* Nim compiler:
    * >= 1.4.2

## Installation

1. The game data can be found [here](https://www.popot.org/get_the_games.php?game=SDLPoP)
    1. download the zip-file for Windows/Linux
    2. unzip the file
2. Clone this repository:
    ```sh
    git clone https://gitea.com/BarrOff/nimPoP.git
    cd nimPoP
    ```
3. simply use `nimble build` to compile
4. copy the produced `nimPoP` binary to the directory created in step 1.2.


## Important information!

When **not** using the current devel version of Nim, there is a small change to
the SDLPoP.ini file, located in the game data folder, because of [this bug](https://github.com/nim-lang/Nim/issues/16206).
The file ends with 16 section names of the pattern `[Level x]`, where `x` is a number
between 0 and 15. You need to remove the spaces between 'Level' and 'x' for the
program to start correctly.

There is currently a bug connected to the MIDI sound output, which causes the
game to crash when leaving the start screen and loading a level.
I have been investigating this issue for quite some time now, using tools like
lldb, valgrind and cppcheck, but to no avail. Therefore the game **has to**
be started with the `stdsnd` command line flag like:

```sh
./nimPoP stdsnd
```


## Credits

* [NagyD](https://github.com/NagyD), creator of [SDLPoP](https://github.com/NagyD/SDLPoP)
* [Vladar4](https://github.com/Vladar4), creator of the SDL2 [wrapper](https://github.com/Vladar4/sdl2_nim) for Nim
* [nothings](https://nothings.org/stb_vorbis) for the vorbis decoder
* [yglukhov](https://github.com/yglukhov) for writing a nim [wrapper](https://github.com/yglukhov/sound) for the vorbis decoder

If you feel you should be listed here, please contact me and I will gladly add
your information!
