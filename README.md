## nimPoP

This is a reimplementation of [SDLPoP](https://github.com/NagyD/SDLPoP) in Nim.
I do this mainly for educational reasons to learn about Nim. The initial step
was porting the C-code to Nim, as close as possible. As a result of this the Nim
code is rather not compliant to Nim coding standards, yet.

## Current Status

The basic code structure has been ported, except the screenshot and recording
features. Those have been postponed until the code is stable enough.
Therefore some bugs are still present, a (probably incomplete list) can be found
in the `docs` directory
However, the basic game is playable, as far as I could test it.

Mods have **not** been tested!

## Prerequisites

- [SDL](https://libsdl.org/) >= 2.0.5
- [SDL_image](https://www.libsdl.org/projects/SDL_image/) >= 2.0.0
- a C compiler ([clang](https://clang.llvm.org/) and [gcc](https://gcc.gnu.org/)
  are known to work, [tinycc](https://bellard.org/tcc/) is currently not working)
- Nim compiler >= 1.4.2

## Installation

```sh
git clone https://codeberg.com/BarrOff/nimPoP.git
cd nimPoP
nimble build -d:release
```

Nimble will download the [game data](https://www.popot.org/get_the_games/software/SDLPoP/SDLPoP-1.23.zip)
and extract the `data` directory and the `SDLPoP.ini` config file.
Existing files will not be overwritten!
On POSIX like systems it can use [curl](https://curl.se/) or [wget](https://www.gnu.org/software/wget/),
on windows it will use [powershell](https://github.com/powershell/powershell)
builtin download abilities.

## Cleaning up

Enter the source directory and use the following command:

```sh
nimble clean
```

This removes the `nimPoP` executable, the `data` directory and the downloaded file.

## Important information!

When using a version of Nim \< 1.6.0, there is a small change to
the SDLPoP.ini file, located in the game data folder, because of [this bug](https://github.com/nim-lang/Nim/issues/16206).
The file ends with 16 section names of the pattern `[Level x]`, where `x` is a number
between 0 and 15. You need to remove the spaces between 'Level' and 'x' for the
program to start correctly.

There is a bug which causing a segfault when switching from level one to two.
I have been investigating this issue for quite some time now, using tools like
[lldb](https://lldb.llvm.org/), [valgrind](http://www.valgrind.org/), [cppcheck](http://cppcheck.sourceforge.net/) and [rr](https://rr-project.org/), but to no avail.
A (temporary?) solution was found by switching from Nims default `refc` gc to the newer `orc` gc available since Nim 1.4.0.

## Credits

- [NagyD](https://github.com/NagyD), creator of [SDLPoP](https://github.com/NagyD/SDLPoP)
- [Vladar4](https://github.com/Vladar4), creator of the SDL2 [wrapper](https://github.com/Vladar4/sdl2_nim) for Nim
- [nothings](https://nothings.org/stb_vorbis) for the vorbis decoder
- [yglukhov](https://github.com/yglukhov) for writing a nim [wrapper](https://github.com/yglukhov/sound) for the vorbis decoder
- [rr](https://rr-project.org/) for helping me detect and fix the MIDI sound output bug and the guard behaviour related issues

If you feel you should be listed here, please contact me and I will gladly add
your information!
