# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.2] - 2024-12-31

### Changed

- Release v0.4.1
- Added a modding option to skip the mouse in the ending scene.
- Always use the named RGBA mask constants when calling SDL_CreateRGBSurface(). (imported from SDLPoP)
- Replace local SDL functions by upstream versions
- Prepare gitignore for use with nimble develop
- Use `let` to create variable `distance`

### Fixed

- Adjust float format for seconds to 4.2f
- Correctly set shadow step options
- Typo in variable name
- Font glitch on big-endian platforms (imported from SDLPoP)
- RGB<->BGR mismatch on big endian architectures (imported from SDLPoP)

## [0.4.1] - 2024-08-18

### Added

- Add CHANGELOG file

### Changed

- Release v0.4.1
- Use upstream `setIntegerScale`
- Use builtin allocator for Nim >= 2.0.8
- Update version of `sdl2` dependency
- Fixed a turn-run bug that causes trick 1 and trick 2 to work.
- Use `rw` functions from sdl2 module

### Removed

- Remove unused variable

## [0.4.0] - 2023-12-31

### Added

- Add info about wrong sprite to bug list
- Add missing endian swapping
- Add type to `process` template parameter
- Add missing whitespace
- Add more exports from data.nim
- Add some whitespace for better readability

### Changed

- Release v0.4.0
- Use `int` type for `key` variables
- Convert nimPoP/opl3.nim into a module
- Replace zero `memset`s with `zeroMem`
- Only set variables when `USE_QUICKLOAD_PENALTY`
- Replace deprecated `readChars` overload
- Convert nimPoP/seqtbl.nim into a module
- Typo in function name
- Make `playSound` work with int and soundIds
- Use `int` instead of `int32` in quicksave functions
- Use `unsafeAddr` only when NimMajor > 1
- Make `quickControl` a local variable
- Convert nimPoP/data.nim into a module
- Rename `window` to `popWindow`
- Use SDL_NUM_SCANCODES from library
- Move all imports into main module
- Re-enable `tlsEmulation` and `stackTrace`
- Convert some `var` to `let`
- Change some integer types for easier maintenance
- Use soundIds to play sounds
- Replace sdl2_nim with sdl2 module
- Update `x11` and `winim` dependencies
- Mark quisave strings as constant

### Fixed

- Use correct target address of stream
- Use global `lightingMask` variable
- Some type errors when using numbers in conditions
- Get order of enums
- Typo in variables name
- Typo in typename
- Use `dealloc` in Nim
- Adjust imports for sdl2 module
- Define `useMalloc` for Nim < 2.0.4

### Removed

- Remove superfluos checks
- Remove unnecessary emit block
- Remove unused code
- Remove duplicate parameter types in signatures
- Remove superfluous code
- Remove superfluos colon (broke compilation)

## [0.3.0] - 2023-04-08

### Changed

- Release v0.3.0
- Let link in instruction point to codeberg
- Update `nimble.lock` file to version 2
- Update targeted SDLPoP version
- Mark guard loading issue as resolved
- Update link to new game data download
- No need for cast, use simple conversion
- Format using `nimpretty`
- Use native Nim implementation of pointer arithmetic

### Fixed

- Count inversion happens outside do_while loop
- Correct pointer arithmetic in `decompressRleUd`
- Use correct target pointers for assignment
- Use correct condition in `do_while` loops

### Removed

- Remove unused C emit blocks

## [0.2.4] - 2023-01-01

### Added

- Add missing negation of condition
- Add `do_while` template for efficient do-while loops
- Add missing change of `srcPos` pointer
- Add `do_while` template for efficient do-while loops

### Changed

- Update to v0.2.4
- Update dependencies in `nimble.lock`
- Use std/endians to swap endianess
- Use Tiles as true enum type
- Extract `seqtbl` elements in immutable variable
- Declare variable in block it is used in
- Make some variables immutable via `let`
- Check for inequality if suitable (imported from SDLPoP)
- Check for equality if suitable (imported from SDLPoP)
- Rename a bunch of variables (imported from SDLPoP)
- Refc and arc now work too, use default gc
- Bump stb_vorbis c header to 1.22
- Only need to type-annotate first array member

### Fixed

- Re-enable `orc` gc
- Do not use `var ptr` as type of parameter
- Change types of control variables

### Removed

- Remove superfluos whitespace

## [0.2.3] - 2022-07-03

### Added

- Add `prepare` nimble task
- Add .editorconfig

### Changed

- Update used `winim` dependency version
- Format markdown files with `mdformat`
- Dry fix-related configuration code

### Fixed

- Update to v0.2.3

[0.4.2]: https://github.com/BarrOff/nimPoP/compare/v0.4.1..v0.4.2
[0.4.1]: https://github.com/BarrOff/nimPoP/compare/v0.4.0..v0.4.1
[0.4.0]: https://github.com/BarrOff/nimPoP/compare/v0.3.0..v0.4.0
[0.3.0]: https://github.com/BarrOff/nimPoP/compare/v0.2.4..v0.3.0
[0.2.4]: https://github.com/BarrOff/nimPoP/compare/v0.2.3..v0.2.4
[0.2.3]: https://github.com/BarrOff/nimPoP/compare/v0.2.2..v0.2.3

<!-- generated by git-cliff -->
