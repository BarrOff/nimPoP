switch("threads", "on")

# using refc garbage collector causes crashes, orc runs fine
# use the following option till I find the cause
switch("gc", "orc")

when not defined(release):
  switch("debugger", "native")

when (NimMajor, NimMinor, NimPatch) < (2, 0, 8):
  switch("define", "useMalloc")
