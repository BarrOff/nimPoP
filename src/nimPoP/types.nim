import sdl2

import stbVorbis
export stbVorbis
import config
export config

const
  O_BINARY* = 0
  MAX_OPTION_VALUE_NAME_LENGTH* = 20
  NUM_GUARD_SKILLS* = 12
  NUM_TIMERS* = 3
  BASE_FPS* = 60
  FEATHER_FALL_LENGTH* = 18.75

type
  sbyte* = int8
  word* = uint16
  dword* = uint32

  RectType* = object
    top*, left*, bottom*, right*: int16
  RectTypeRef* = ref RectType

  Piece* = object
    baseId*: byte
    floorLeft*: byte
    baseY*: sbyte
    rightId*: byte
    floorRight*: byte
    rightY*: sbyte
    stripeId*: byte
    toprightId*: byte
    bottomId*: byte
    foreId*: byte
    foreX*: byte
    foreY*: sbyte

  TileAndMod* = object
    tiletype*: Tiles
    modifier*: byte

  AddTableType* = proc(chtabId: int16, id: int32, xh, xl: sbyte, ybottom,
      blit: int32, peel: byte): int32

  BackTableType* = object
    xh*: sbyte
    xl*: sbyte
    y*: int16
    chtabId*: byte
    id*: byte
    blit*: int32

  MidTableType* = object
    xh*: sbyte
    xl*: sbyte
    y*: int16
    chtabId*: byte
    id*: byte
    peel*: byte
    clip*: RectType
    blit*: int32

  WipeTableType* = object
    left*: int16
    bottom*: int16
    height*: sbyte
    width*: int16
    color*: sbyte
    layer*: sbyte

  Soundflags* = enum
    sfDigi = 1, sfMidi = 2, soundflags4 = 4, sfLoop = 0x80

  Tiles* = enum
    tiles0Empty = 0, tiles1Floor, tiles2Spike, tiles3Pillar, tiles4Gate,
    tiles5Stuck, tiles6Closer, tiles7DoortopWithFloor, tiles8BigpillarBottom,
    tiles9BigpillarTop, tiles10Potion, tiles11Loose, tiles12Doortop,
        tiles13Mirror,
    tiles14Debris, tiles15Opener, tiles16LevelDoorLeft, tiles17LevelDoorRight,
    tiles18Chomper, tiles19Torch, tiles20Wall, tiles21Skeleton, tiles22Sword,
    tiles23BalconyLeft, tiles24BalconyRight, tiles25LatticePillar,
        tiles26LatticeDown, tiles27LatticeSmall, tiles28LatticeLeft,
        tiles29LatticeRight, tiles30TorchWithDebris

  Chtabs* = enum
    idChtab0Sword = 0, idChtab1Flameswordpotion, idChtab2Kid,
        idChtab3Princessinstory, idChtab4JaffarinstoryPrincessincutscenes,
        idChtab5Guard, idChtab6Environment, idChtab7Environmentwall,
            idChtab8Princessroom, idchtab9Princessbed

when(USE_COLORED_TORCHES):
  type
    Blitters* = enum
      blitters0NoTransp = 0, blitters2Or = 2, blitters3Xor, blittersWhite = 8,
      blitters9Black, blitters10hTransp = 0x10, blitters40hMono = 0x40,
      blitters46hMono6 = 0x46, blitters4Chmono12 = 0x4C,
      blittersColoredFlame = 0x100, blittersColoredFlameLast = 0x13F
else:
  type
    Blitters* = enum
      blitters0NoTransp = 0, blitters2Or = 2, blitters3Xor, blittersWhite = 8,
      blitters9Black, blitters10hTransp = 0x10, blitters40hMono = 0x40,
      blitters46hMono6 = 0x46, blitters4Chmono12 = 0x4C

type
  FullImageId* = enum
    TITLE_MAIN = 0, TITLE_PRESENTS, TITLE_GAME, TITLE_POP, TITLE_MECHNER,
        HOF_POP, STORY_FRAME, STORY_ABSENCE, STORY_MARRY, STORY_HAIL,
        STORY_CREDITS, MAX_FULL_IMAGES

  Grmodes* = enum
    gmCga = 1, gmHgaHerc, gmEga, gmTga, gmMcgaVga

  SoundModes* = enum
    smAuto = 0, smAdlib, smGblast, smSblast, smCovox, smIbmg, smTandy

  LinkType* {.packed.} = object
    left*, right*, up*, down*: byte

  LevelType* {.packed.} = object
    fg*: array[720, byte]
    bg*: array[720, byte]
    doorlinks1*: array[256, byte]
    doorlinks2*: array[256, byte]
    roomlinks*: array[24, LinkType]
    usedRooms*: byte
    roomxs*: array[24, byte]
    roomys*: array[24, byte]
    fill1*: array[15, byte]
    startRoom*: byte
    startPos*: byte
    startDir*: sbyte
    fill2*: array[4, byte]
    guardsTile*: array[24, byte]
    guardsDir*: array[24, byte]
    guardsX*: array[24, byte]
    guardsSeqLo*: array[24, byte]
    guardsSkill*: array[24, byte]
    guardsSeqHi*: array[24, byte]
    guardsColor*: array[24, byte]
    fill3*: array[18, byte]

  SurfaceType* = SurfacePtr
  ImageType* = SurfacePtr
  PeelType* = object
    peel*: SurfacePtr
    rect*: RectType

  ChtabType* = ptr ChtabTypeObject
  ChtabTypeObject* = object
    nImages*: word
    chtabPaletteBits*: word
    hasPaletteBits*: word
    images*: UncheckedArray[ImageType]

  FullImageType* = object
    id*: int32
    # TODO:  chtaType** chtab
    chtab*: ptr ChtabType
    blitter*: Blitters
    xpos*, ypos*: int32

  RgbType* {.packed.} = object
    r*, g*, b*: byte

  DatPalType* {.packed.} = object
    rowBits*: word
    nColors*: byte
    vga*: array[16, RgbType]
    cga*: array[16, byte]
    ega*: array[32, byte]

  DatShplType* {.packed.} = object
    nImages*: byte
    palette*: DatPalType

  CharType* = object
    frame*: byte
    x*, y*: byte
    direction*: sbyte
    currCol*: sbyte
    currRow*: sbyte
    action*: byte
    fallX*: sbyte
    # Falling speed, but also used to check falling distance
    # (less than 22 = one row, less than 33 = two rows)
    fallY*: sbyte
    room*: byte
    repeat*: byte
    charid*: byte
    sword*: byte
    alive*: sbyte
    currSeq*: word

  Charids* = enum
    charid0Kid = 0, charid1Shadow, charid2Guard, charid3, charid4Skeleton,
        charid5Princess, charid6Vizier, charid24Mouse = 0x18

  SwordStatus* = enum
    sword0Sheathed = 0, sword2Drawn = 2

  AutoMoveType* = object
    time*, move*: int16

  ObjtableType* = object
    xh*: sbyte
    xl*: sbyte
    y*: int16
    chtabId*: byte
    id*: byte
    direction*: sbyte
    objType*: byte
    clip*: RectType
    tilepos*: byte

  FrameType* = object
    image*: byte
    sword*: byte

    # 0x3F*: sword image
    # 0xC0*: chtab
    dx*: sbyte
    dy*: sbyte

    # 0x1F*: weight x
    # 0x20*: thin
    # 0x40*: needs floor
    # 0x80*: even/odd pixel
    flags*: byte

  FrameFlags* = enum
    FRAME_WEIGHT_X = 0x1F,
    FRAME_THIN = 0x20,
    FRAME_NEEDS_FLOOR = 0x40,
    FRAME_EVEN_ODD_PIXEL = 0x80

  TrobType* = object
    tilepos*: byte
    room*: byte
    `type`*: sbyte

  MobType* = object
    xh*: byte
    y*: byte
    room*: byte
    speed*: sbyte
    `type`*: byte
    row*: byte

  Directions* = enum
    dirFFLeft = -1
    dir0Right = 0x00
    dir56None = 0x56

  Actions* = enum
    actions0Stand = 0, actions1RunJump, actions2HangClimb, actions3InMidair,
        actions4InFreefall, actions5bumped, actions6HangStraight, actions7Turn,
        actions99Hurt = 99

  SwordTableType* = object
    id*: byte
    x*: sbyte
    y*: sbyte

  DatHeaderType* {.packed.} = object
    tableOffset*: uint32
    tableSize*: uint16

  DatResType {.packed.} = object
    id*: uint16
    offset*: uint32
    size*: uint16

  DatTableType* {.packed.} = object
    resCount*: uint16
    entries*: UncheckedArray[DatResType]

  ImageDataType* {.packed.} = object
    height*: uint16
    width*: uint16
    flags*: uint16
    data*: UncheckedArray[byte]

  DatType* = ref object
    nextDat*: DatType
    handle*: File
    filename*: string
    datTable*: ptr DatTableType

  CutscenePtrType* = proc()

when(USE_FADE):
  type
    PaletteFadeType* = ref object
      whichRows*: word
      waitTime*: word
      fadePos*: word
      originalPal*: array[256, RgbType]
      fadedPal*: array[256, RgbType]
      procFadeFrame*: proc(paletteBuffer: PaletteFadeType): int32
      procRestoreFree*: proc(paletteBuffer: PaletteFadeType)

when(USE_TEXT):
  type
    FontType* = ref object
      firstChar*: byte
      lastChar*: byte
      heightAboveBaseline*: int16
      heightBelowBaseline*: int16
      spaceBetweenLines*: int16
      spaceBetweenChars*: int16
      chtab*: ChtabType

    TextstateType* = object
      currentX*: int16
      currentY*: int16
      textblit*: int16
      textcolor*: int16
      ptrFont*: FontType

    RawFontType* {.packed.} = object
      firstChar*: byte
      lastChar*: byte
      heightAboveBaseline*: int16
      heightBelowBaseline*: int16
      spaceBetweenLines*: int16
      spaceBetweenChars*: int16
      offsets*: UncheckedArray[word]

type
  DataLocation* = enum
    dataNone = 0, dataDAT, dataDirectory
  SoundType* = enum
    soundSpeaker = 0, soundDigi, soundMidi, soundChunk, soundMusic, soundOgg, soundDigiConverted

  NoteType* {.packed.} = object
    frequency*: word
    length*: byte

  SpeakerType* {.packed.} = object
    tempo*: word
    notes*: UncheckedArray[NoteType]

  DigiType* {.packed.} = object
    sampleRate*: word
    sampleCount*: word
    unknown*: word
    sampleSize*: byte
    samples*: UncheckedArray[byte]

  DigiNewType* {.packed.} = object
    sampleRate*: word
    sampleSize*: byte
    sampleCount*: word
    unknown*: word
    unknown2*: word
    samples*: UncheckedArray[byte]

  MidiTypeStruct* {.packed.} = object
    format*: word
    numTracks*: word
    delta*: word
    tracks*: UncheckedArray[byte]

  # MidiTypeUnion* {.union.} = object
  #   header*: MidiTypeStruct
  #   data*: UncheckedArray[byte]

  MidiType* {.packed.} = object
    ChunkType*: array[4, char]
    chunkLength*: dword
    # headerDataUnion*: MidiTypeUnion
    header*: MidiTypeStruct

  OggType {.packed.} = object
    totalLength*: int32
    fileContents*: ptr byte
    decoder*: StbVorbis

  ConvertedAudioType* {.packed.} = object
    length*: int32
    samples*: UncheckedArray[int16]

  SoundBufferUnion* {.union, packed.} = object
    speaker*: SpeakerType
    digi*: DigiType
    digiNew*: DigiNewType
    midi*: MidiType
    ogg*: OggType
    converted*: ConvertedAudioType

  SoundBufferType* {.packed.} = object
    `type`*: byte
    bu*: SoundBufferUnion

  MidiRawChunkUnionStruct* {.packed.} = object
    format*: word
    numTracks*: word
    timeDivision*: word
    # tracks*: array[0, byte] first implementation
    tracks*: UncheckedArray[byte]

  # MidiRawChunkUnion* {.union.} = object
  #   header*: MidiRawChunkUnionStruct
  #   # data*: array[0, byte]    first implementation
  #   data*: UncheckedArray[byte]

  MidiRawChunkType* {.packed.} = object
    chunkType*: array[4, char]
    chunkLength*: dword
    # rcu*: MidiRawChunkUnion
    header*: MidiRawChunkUnionStruct

  MidiEventChannel* = object
    channel*: byte
    param1*: byte
    param2*: byte

  MidiEventSysex* = object
    length*: dword
    data*: ptr byte

  MidiEventMeta* = object
    `type`*: byte
    length*: dword
    data*: ptr byte

  MidiEventUnion* {.union.} = object
    channel*: MidiEventChannel
    sysex*: MidiEventSysex
    meta*: MidiEventMeta

  MidiEventType* = object
    deltaTime*: dword
    eventType*: byte
    metu*: MidiEventUnion

  MidiTrackType* = object
    size*: dword
    numEvents*: int32
    events*: ptr MidiEventType # instead of pointer type use a seq
    eventIndex*: int32
    nextPauseTick*: int64

  ParsedMidiType* = object
    numTracks*: int32
    # tracks*: ptr MidiTrackType
    tracks*: ptr MidiTrackType
    ticksPerBeat*: dword

  OperatorType* {.packed.} = object
    mul*: byte
    kslTl*: byte
    aD*: byte
    sR*: byte
    waveform*: byte

  InstrumentType* {.packed.} = object
    blocknumLow*: byte
    blocknumHigh*: byte
    FBConn*: byte
    operators*: array[2, OperatorType]
    percussions*: byte
    unknown*: array[2, byte]

  DialogSettingsType* = object
    method1*: proc(dialog: ptr DialogType)
    method2Frame*: proc(dialog: ptr DialogType)
    topBorder*: int16
    leftBorder*: int16
    bottomBorder*: int16
    rightBorder*: int16
    shadowBottom*: int16
    shadowRight*: int16
    outerBorder*: int16

  DialogType* = object
    settings*: ptr DialogSettingsType
    textRect*: RectType
    peelRect*: RectType
    hasPeel*: word
    peel*: ptr PeelType

  Soundids* = enum
    sound0FellToDeath = 0,
    sound1Falling = 1,
    sound2TileCrashing = 2,
    sound3ButtonPressed = 3,
    sound4GateClosing = 4,
    sound5GateOpening = 5,
    sound6GateClosingFast = 6,
    sound7GateStop = 7,
    sound8Bumped = 8,
    sound9Grab = 9,
    sound10SwordVsSword = 10,
    sound11SwordMoving = 11,
    sound12GuardHurt = 12,
    sound13KidHurt = 13,
    sound14LeveldoorClosing = 14,
    sound15LeveldoorSliding = 15,
    sound16MediumLand = 16,
    sound17SoftLand = 17,
    sound18Drink = 18,
    sound19DrawSword = 19,
    sound20LooseShake1 = 20,
    sound21LooseShake2 = 21,
    sound22LooseShake3 = 22,
    sound23Footstep = 23,
    sound24DeathRegular = 24,
    sound25Presentation = 25,
    sound26Embrace = 26,
    sound27Cutscene24612 = 27,
    sound28DeathInFight = 28,
    sound29MeetJaffar = 29,
    sound30BigPotion = 30,
    # //soun31 = 31,
    sound32ShadowMusic = 32,
    sound33SmallPotion = 33,
    # //soun34 = 34,
    sound35Cutscene89 = 35,
    sound36OutOfTime = 36,
    sound37Victory = 37,
    sound38Blink = 38,
    sound39LowWeight = 39,
    sound40Cutscene12ShortTime = 40,
    sound41EndLevelMusic = 41,
    # //soun42 = 42,
    sound43VictoryJaffar = 43,
    sound44SkelAlive = 44,
    sound45JumpThroughMirror = 45,
    sound46Chomped = 46,
    sound47Chomper = 47,
    sound48Spiked = 48,
    sound49Spikes = 49,
    sound50Story2Princess = 50,
    sound51PrincessDoorOpening = 51,
    sound52Story4JaffarLeaves = 52,
    sound53Story3JaffarComes = 53,
    sound54IntroMusic = 54,
    sound55Story1Absence = 55,
    sound56EndingMusic = 56

  Timerids* = enum
    timer0 = 0, timer1, timer2

  Frameids* = enum
    frame0 = 0,
    frame1StartRun = 1,
    frame2StartRun = 2,
    frame3StartRun = 3,
    frame4StartRun = 4,
    frame5StartRun = 5,
    frame6StartRun = 6,
    frame7Run = 7,
    frame8Run = 8,
    frame9Run = 9,
    frame10Run = 10,
    frame11Run = 11,
    frame12Run = 12,
    frame13Run = 13,
    frame14Run = 14,
    frame15Stand = 15,
    frame16StandingJump1 = 16,
    frame17StandingJump2 = 17,
    frame18StandingJump3 = 18,
    frame19StandingJump4 = 19,
    frame20StandingJump5 = 20,
    frame21StandingJump6 = 21,
    frame22StandingJump7 = 22,
    frame23StandingJump8 = 23,
    frame24StandingJump9 = 24,
    frame25StandingJump10 = 25,
    frame26StandingJump11 = 26,
    frame27StandingJump12 = 27,
    frame28StandingJump13 = 28,
    frame29StandingJump14 = 29,
    frame30StandingJump15 = 30,
    frame31StandingJump16 = 31,
    frame32StandingJump17 = 32,
    frame33StandingJump18 = 33,
    frame34StartRunJump1 = 34,
    frame35StartRunJump2 = 35,
    frame36StartRunJump3 = 36,
    frame37StartRunJump4 = 37,
    frame38StartRunJump5 = 38,
    frame39StartRunJump6 = 39,
    frame40RunningJump1 = 40,
    frame41RunningJump2 = 41,
    frame42RunningJump3 = 42,
    frame43RunningJump4 = 43,
    frame44RunningJump5 = 44,
    frame45Turn = 45,
    frame46Turn = 46,
    frame47Turn = 47,
    frame48Turn = 48,
    frame49Turn = 49,
    frame50Turn = 50,
    frame51Turn = 51,
    frame52Turn = 52,
    frame53Runturn = 53,
    frame54Runturn = 54,
    frame55Runturn = 55,
    frame56Runturn = 56,
    frame57Runturn = 57,
    frame58Runturn = 58,
    frame59Runturn = 59,
    frame60Runturn = 60,
    frame61Runturn = 61,
    frame62Runturn = 62,
    frame63Runturn = 63,
    frame64Runturn = 64,
    frame65Runturn = 65,
    frame67StartJumpUp1 = 67,
    frame68StartJumpUp2 = 68,
    frame69StartJumpUp3 = 69,
    frame70Jumphang = 70,
    frame71Jumphang = 71,
    frame72Jumphang = 72,
    frame73Jumphang = 73,
    frame74Jumphang = 74,
    frame75Jumphang = 75,
    frame76Jumphang = 76,
    frame77Jumphang = 77,
    frame78Jumphang = 78,
    frame79Jumphang = 79,
    frame80Jumphang = 80,
    frame81Hangdrop1 = 81,
    frame82Hangdrop2 = 82,
    frame83Hangdrop3 = 83,
    frame84Hangdrop4 = 84,
    frame85Hangdrop5 = 85,
    frame86TestFoot = 86,
    frame87Hanging1 = 87,
    frame88Hanging2 = 88,
    frame89Hanging3 = 89,
    frame90Hanging4 = 90,
    frame91Hanging5 = 91,
    frame92Hanging6 = 92,
    frame93Hanging7 = 93,
    frame94Hanging8 = 94,
    frame95Hanging9 = 95,
    frame96Hanging10 = 96,
    frame97Hanging11 = 97,
    frame98Hanging12 = 98,
    frame99Hanging13 = 99,
    frame102StartFall1 = 102,
    frame103StartFall2 = 103,
    frame104StartFall3 = 104,
    frame105StartFall4 = 105,
    frame106Fall = 106,
    frame107FallLand1 = 107,
    frame108FallLand2 = 108,
    frame109Crouch = 109,
    frame110StandUpFromCrouch1 = 110,
    frame111StandUpFromCrouch2 = 111,
    frame112StandUpFromCrouch3 = 112,
    frame113StandUpFromCrouch4 = 113,
    frame114StandUpFromCrouch5 = 114,
    frame115StandUpFromCrouch6 = 115,
    frame116StandUpFromCrouch7 = 116,
    frame117StandUpFromCrouch8 = 117,
    frame118StandUpFromCrouch9 = 118,
    frame119StandUpFromCrouch10 = 119,
    frame121Stepping1 = 121,
    frame122Stepping2 = 122,
    frame123Stepping3 = 123,
    frame124Stepping4 = 124,
    frame125Stepping5 = 125,
    frame126Stepping6 = 126,
    frame127Stepping7 = 127,
    frame128Stepping8 = 128,
    frame129Stepping9 = 129,
    frame130Stepping10 = 130,
    frame131Stepping11 = 131,
    frame132Stepping12 = 132,
    frame133Sheathe = 133,
    frame134Sheathe = 134,
    frame135Climbing1 = 135,
    frame136Climbing2 = 136,
    frame137Climbing3 = 137,
    frame138Climbing4 = 138,
    frame139Climbing5 = 139,
    frame140Climbing6 = 140,
    frame141Climbing7 = 141,
    frame142Climbing8 = 142,
    frame143Climbing9 = 143,
    frame144Climbing10 = 144,
    frame145Climbing11 = 145,
    frame146Climbing12 = 146,
    frame147Climbing13 = 147,
    frame148Climbing14 = 148,
    frame149Climbing15 = 149,
    frame150Parry = 150,
    frame151Strike1 = 151,
    frame152Strike2 = 152,
    frame153Strike3 = 153,
    frame154Poking = 154,
    frame155Guy7 = 155,
    frame156Guy8 = 156,
    frame157WalkWithSword = 157,
    frame158StandWithSword = 158,
    frame159Fighting = 159,
    frame160Fighting = 160,
    frame161Parry = 161,
    frame162BlockToStrike = 162,
    frame163Fighting = 163,
    frame164Fighting = 164,
    frame165WalkWithSword = 165,
    frame166StandInactive = 166,
    frame167Blocked = 167,
    frame168Back = 168,
    frame169BeginBlock = 169,
    frame170StandWithSword = 170,
    frame171StandWithSword = 171,
    frame172Jumpfall2 = 172,
    frame173Jumpfall3 = 173,
    frame174Jumpfall4 = 174,
    frame175Jumpfall5 = 175,
    frame177Spiked = 177,
    frame178Chomped = 178,
    frame179Collapse1 = 179,
    frame180Collapse2 = 180,
    frame181Collapse3 = 181,
    frame182Collapse4 = 182,
    frame183Collapse5 = 183,
    frame185Dead = 185,
    frame186Mouse1 = 186,
    frame187Mouse2 = 187,
    frame188MouseStand = 188,
    frame191Drink = 191,
    frame192Drink = 192,
    frame193Drink = 193,
    frame194Drink = 194,
    frame195Drink = 195,
    frame196Drink = 196,
    frame197Drink = 197,
    frame198Drink = 198,
    frame199Drink = 199,
    frame200Drink = 200,
    frame201Drink = 201,
    frame202Drink = 202,
    frame203Drink = 203,
    frame204Drink = 204,
    frame205Drink = 205,
    frame207Draw1 = 207,
    frame208Draw2 = 208,
    frame209Draw3 = 209,
    frame210Draw4 = 210,
    frame217ExitStairs1 = 217,
    frame218ExitStairs2 = 218,
    frame219ExitStairs3 = 219,
    frame220ExitStairs4 = 220,
    frame221ExitStairs5 = 221,
    frame222ExitStairs6 = 222,
    frame223ExitStairs7 = 223,
    frame224ExitStairs8 = 224,
    frame225ExitStairs9 = 225,
    frame226ExitStairs10 = 226,
    frame227ExitStairs11 = 227,
    frame228ExitStairs12 = 228,
    frame229FoundSword = 229,
    frame230Sheathe = 230,
    frame231Sheathe = 231,
    frame232Sheathe = 232,
    frame233Sheathe = 233,
    frame234Sheathe = 234,
    frame235Sheathe = 235,
    frame236Sheathe = 236,
    frame237Sheathe = 237,
    frame238Sheathe = 238,
    frame239Sheathe = 239,
    frame240Sheathe = 240


  altset2ids* = enum
    alt2frame54Vstand = 54
    # incomplete


when USE_TELEPORTS:
  type
    seqids* = enum
      seq1StartRun = 1
      seq2Stand = 2
      seq3StandingJump = 3
      seq4RunJump = 4
      seq5Turn = 5
      seq6RunTurn = 6
      seq7Fall = 7
      seq8JumpUpAndGrabStraight = 8
      seq9GrabWhileJumping = 9
      seq10ClimbUp = 10
      seq11ReleaseLedgeAndLand = 11
      seq13StopRun = 13
      seq14JumpUpIntoCeiling = 14
      seq15GrabLedgeMidair = 15
      seq16JumpUpAndGrab = 16
      seq17SoftLand = 17
      seq18FallAfterStandingJump = 18
      seq19Fall = 19
      seq20MediumLand = 20
      seq21FallAfterRunningJump = 21
      seq22Crushed = 22
      seq23ReleaseLedgeAndFall = 23
      seq24JumpUpAndGrabForward = 24
      seq25HangAgainstWall = 25
      seq26CrouchWhileRunning = 26
      seq28JumpUpWithNothingAbove = 28
      seq29SafeStep1 = 29
      seq30SafeStep2 = 30
      seq31SafeStep3 = 31
      seq32SafeStep4 = 32
      seq33SafeStep5 = 33
      seq34SafeStep6 = 34
      seq35SafeStep7 = 35
      seq36SafeStep8 = 36
      seq37SafeStep9 = 37
      seq38SafeStep10 = 38
      seq39SafeStep11 = 39
      seq40SafeStep12 = 40
      seq41SafeStep13 = 41
      seq42SafeStep14 = 42
      seq43StartRunAfterTurn = 43
      seq44StepOnEdge = 44
      seq45Bumpfall = 45
      seq46Hardbump = 46
      seq47Bump = 47
      seq49StandUpFromCrouch = 49
      seq50Crouch = 50
      seq51Spiked = 51
      seq52LooseFloorFellOnKid = 52
      seq54Chomped = 54
      seq55DrawSword = 55
      seq56GuardForwardWithSword = 56
      seq57BackWithSword = 57
      seq58GuardStrike = 58
      seq60TurnWithSword = 60
      seq61ParryAfterStrike = 61
      seq62Parry = 62
      seq63GuardActiveAfterFall = 63
      seq64PushedBackWithSword = 64
      seq65BumpForwardWithSword = 65
      seq66StrikeAfterParry = 66
      seq68ClimbDown = 68
      seq69AttackWasParried = 69
      seq70GoUpOnLevelDoor = 70
      seq71Dying = 71
      seq73ClimbUpToClosedGate = 73
      seq74HitBySword = 74
      seq75Strike = 75
      seq77GuardStandInactive = 77
      seq78Drink = 78
      seq79CrouchHop = 79
      seq80StandFlipped = 80
      seq81KidPushedOffLedge = 81
      seq82GuardPushedOffLedge = 82
      seq83GuardFall = 83
      seq84Run = 84
      seq85StabbedToDeath = 85
      seq86ForwardWithSword = 86
      seq87GuardBecomeInactive = 87
      seq88SkelWakeUp = 88
      seq89TurnDrawSword = 89
      seq90EnGarde = 90
      seq91GetSword = 91
      seq92PutSwordAway = 92
      seq93PutSwordAwayFast = 93
      seq94PrincessStandPV1 = 94
      seq95JaffarStandPV1 = 95
      seq101MouseStandsUp = 101
      seq103PrincessLyingPV2 = 103
      seq104StartFallInFrontOfWall = 104
      seq105MouseForward = 105
      seq106Mouse = 106
      seq107MouseStandUpAndGo = 107
      seq108PrincessTurnAndHug = 108
      seq109PrincessStandPV2 = 109
      seq110PrincessCrouchingPV2 = 110
      seq111PrincessStandUpPV2 = 111
      seq112PrincessCrouchDownPV2 = 112
      seq114MouseStand = 114
      seq_teleport = 115
else:
  type
    seqids* = enum
      seq1StartRun = 1
      seq2Stand = 2
      seq3StandingJump = 3
      seq4RunJump = 4
      seq5Turn = 5
      seq6RunTurn = 6
      seq7Fall = 7
      seq8JumpUpAndGrabStraight = 8
      seq9GrabWhileJumping = 9
      seq10ClimbUp = 10
      seq11ReleaseLedgeAndLand = 11
      seq13StopRun = 13
      seq14JumpUpIntoCeiling = 14
      seq15GrabLedgeMidair = 15
      seq16JumpUpAndGrab = 16
      seq17SoftLand = 17
      seq18FallAfterStandingJump = 18
      seq19Fall = 19
      seq20MediumLand = 20
      seq21FallAfterRunningJump = 21
      seq22Crushed = 22
      seq23ReleaseLedgeAndFall = 23
      seq24JumpUpAndGrabForward = 24
      seq25HangAgainstWall = 25
      seq26CrouchWhileRunning = 26
      seq28JumpUpWithNothingAbove = 28
      seq29SafeStep1 = 29
      seq30SafeStep2 = 30
      seq31SafeStep3 = 31
      seq32SafeStep4 = 32
      seq33SafeStep5 = 33
      seq34SafeStep6 = 34
      seq35SafeStep7 = 35
      seq36SafeStep8 = 36
      seq37SafeStep9 = 37
      seq38SafeStep10 = 38
      seq39SafeStep11 = 39
      seq40SafeStep12 = 40
      seq41SafeStep13 = 41
      seq42SafeStep14 = 42
      seq43StartRunAfterTurn = 43
      seq44StepOnEdge = 44
      seq45Bumpfall = 45
      seq46Hardbump = 46
      seq47Bump = 47
      seq49StandUpFromCrouch = 49
      seq50Crouch = 50
      seq51Spiked = 51
      seq52LooseFloorFellOnKid = 52
      seq54Chomped = 54
      seq55DrawSword = 55
      seq56GuardForwardWithSword = 56
      seq57BackWithSword = 57
      seq58GuardStrike = 58
      seq60TurnWithSword = 60
      seq61ParryAfterStrike = 61
      seq62Parry = 62
      seq63GuardStandActive = 63
      seq64PushedBackWithSword = 64
      seq65BumpForwardWithSword = 65
      seq66StrikeAfterParry = 66
      seq68ClimbDown = 68
      seq69AttackWasParried = 69
      seq70GoUpOnLevelDoor = 70
      seq71Dying = 71
      seq73ClimbUpToClosedGate = 73
      seq74HitBySword = 74
      seq75Strike = 75
      seq77GuardStandInactive = 77
      seq78Drink = 78
      seq79CrouchHop = 79
      seq80StandFlipped = 80
      seq81KidPushedOffLedge = 81
      seq82GuardPushedOffLedge = 82
      seq83GuardFall = 83
      seq84Run = 84
      seq85StabbedToDeath = 85
      seq86ForwardWithSword = 86
      seq87GuardBecomeInactive = 87
      seq88SkelWakeUp = 88
      seq89TurnDrawSword = 89
      seq90EnGarde = 90
      seq91GetSword = 91
      seq92PutSwordAway = 92
      seq93PutSwordAwayFast = 93
      seq94PrincessStandPV1 = 94
      seq95JaffarStandPV1 = 95
      seq101MouseStandsUp = 101
      seq103PrincessLyingPV2 = 103
      seq104StartFallInFrontOfWall = 104
      seq105MouseForward = 105
      seq106Mouse = 106
      seq107MouseStandUpAndGo = 107
      seq108PrincessTurnAndHug = 108
      seq109PrincessStandPV2 = 109
      seq110PrincessCrouchingPV2 = 110
      seq111PrincessStandUpPV2 = 111
      seq112PrincessCrouchDownPV2 = 112
      seq114MouseStand = 114

type
  SeqtblInstruction* = enum
    SEQ_END_LEVEL = 0xF1,
    SEQ_SOUND = 0xF2,
    SEQ_GET_ITEM = 0xF3,
    SEQ_KNOCK_DOWN = 0xF4,
    SEQ_KNOCK_UP = 0xF5,
    SEQ_DIE = 0xF6,
    SEQ_JMP_IF_FEATHER = 0xF7,
    SEQ_SET_FALL = 0xF8,
    SEQ_ACTION = 0xF9,
    SEQ_DY = 0xFA,
    SEQ_DX = 0xFB,
    SEQ_DOWN = 0xFC,
    SEQ_UP = 0xFD,
    SEQ_FLIP = 0xFE,
    SEQ_JMP = 0xFF

  SeqtblSound* = enum
    SND_SILENT = 0,
    SND_FOOTSTEP = 1,
    SND_BUMP = 2,
    SND_DRINK = 3,
    SND_LEVEL = 4

  Colorids* = enum
    color0Black = 0,
    color1Blue = 1,
    color2Green = 2,
    color3Cyan = 3,
    color4Red = 4,
    color5Magenta = 5,
    color6Brown = 6,
    color7Lightgray = 7,
    color8Darkgray = 8,
    color9Brightblue = 9,
    color10Brightgreen = 10,
    color11Brightcyan = 11,
    color12Brightred = 12,
    color13Brightmagenta = 13,
    color14Brightyellow = 14,
    color15Brightwhite = 15

when(USE_REPLAY):
  type
    ReplaySpecialMoves* = enum
      MOVE_RESTART_LEVEL = 1,
      MOVE_EFFECT_END = 2

    ReplaySeekTarget* = enum
      replaySeek0NextRoom = 0,
      replaySeek1NextLevel = 1,
      replaySeek2End = 2

type
  KeyModifiers* = enum
    WITH_ALT = 0x2000,
    WITH_CTRL = 0x4000,
    WITH_SHIFT = 0x8000

  KeyValueType* = ref object
    key*: array[MAX_OPTION_VALUE_NAME_LENGTH, char]
    value*: int32

  NameListNames* = object
    data*: seq[string] # TODO: const char (* data)[][MAX_OPTION_VALUE_NAME_LENGTH]
    count*: word

  NameListKVPairs* = object
    data*: KeyValueType
    count*: word

  NameListUnion* {.union.} = object
    name*: NameListNames
    kvPairs*: NameListKVPairs

  NamesListType* = object
    `type`*: byte
    nlu*: NameListUnion

  FixesOptionsType* {.packed.} = object
    enableCrouchAfterClimbing*: byte
    enableFreezeTimeDuringEndMusic*: byte
    enableRememberGuardHp*: byte
    fixGateSounds*: byte
    fixTwoCollBug*: byte
    fixInfiniteDownBug*: byte
    fixGateDrawingBug*: byte
    fixBigpillarClimb*: byte
    fixJumpDistanceAtEdge*: byte
    fixEdgeDistanceCheckWhenClimbing*: byte
    fixPainlessFallOnGuard*: byte
    fixWallBumpTriggersTileBelow*: byte
    fixStandOnThinAir*: byte
    fixPressThroughClosedGates*: byte
    fixGrabFallingSpeed*: byte
    fixSkeletonChomperBlood*: byte
    fixMoveAfterDrink*: byte
    fixLooseLeftOfPotion*: byte
    fixGuardFollowingThroughClosedGates * : byte
    fixSafeLandingOnSpikes*: byte
    fixGlideThroughWall*: byte
    fixDropThroughTapestry*: byte
    fixLandAgainstGateOrTapestry*: byte
    fixUnintendedSwordStrike*: byte
    fixRetreatWithoutLeavingRoom*: byte
    fixRunningJumpThroughTapestry*: byte
    fixPushGuardIntoWall*: byte
    fixJumpThroughWallAboveGate*: byte
    fixChompersNotStarting*: byte
    fixFeatherInterruptedByLeveldoor*: byte
    fixOffscreenGuardsDisappearing*: byte
    fixMoveAfterSheathe*: byte
    fixHiddenFloorsDuringFlashing*: byte
    fixHangOnTeleport*: byte
    fixExitDoor*: byte
    fixQuicksaveDuringFeather*: byte
    fixCapedPrinceSlidingThroughGate*: byte
    fixDoortopDisablingGuard*: byte
    fixJumpingOverGuard*: byte
    fixDrop2RoomsClimbingLooseTile*: byte
    fixFallingThroughFloorDuringSwordStrike * : byte
    enableJumpGrab*: byte
    fixRegisterQuickInput*: byte
    fixTurnRunningNearWall*: byte

  CustomOptionsType* {.packed.} = object
    startMinutesLeft*: word
    startTicksLeft*: word
    startHitp*: word
    maxHitpAllowed*: word
    savingAllowedFirstLevel*: word
    savingAllowedLastLevel*: word
    startUpsideDown*: byte
    startInBlindMode*: byte
    copyprotLevel*: word
    drawnTileTopLevelEdge*: Tiles
    drawnTileLeftLevelEdge*: Tiles
    levelEdgeHitTile*: Tiles
    allowTriggeringAnyTile*: byte
    enableWdaInPalace*: byte
    vgaPalette*: array[16, RgbType]
    firstLevel*: word
    skipTitle*: byte
    shiftLAllowedUntilLevel*: word
    shiftLReducedMinutes*: word
    shiftLReducedTicks*: word
    demoHitp*: word
    demoEndRoom*: word
    introMusicLevel*: word
    haveSwordFromLevel*: word
    checkpointLevel*: word
    checkpointRespawnDir*: sbyte
    checkpointRespawnRoom*: byte
    checkpointRespawnTilepos*: byte
    checkpointClearTileRoom*: byte
    checkpointClearTileCol*: byte
    checkpointClearTileRow*: byte
    skeletonLevel*: word
    skeletonRoom*: byte
    skeletonTriggerColumn1*: byte
    skeletonTriggerColumn2*: byte
    skeletonColumn*: byte
    skeletonRow*: byte
    skeletonRequireOpenLevelDoor*: byte
    skeletonSkill*: byte
    skeletonReappearRoom*: byte
    skeletonReappearX*: byte
    skeletonReappearRow*: byte
    skeletonReappearDir*: byte
    mirrorLevel*: word
    mirrorRoom*: byte
    mirrorColumn*: byte
    mirrorRow*: byte
    mirrorTile*: Tiles
    showMirrorImage*: byte
    shadowStealLevel*: byte
    shadowStealRoom*: byte
    shadowStepLevel*: byte
    shadowStepRoom*: byte
    fallingExitLevel*: word
    fallingExitRoom*: byte
    fallingEntryLevel*: word
    fallingEntryRoom*: byte
    mouseLevel*: word
    mouseRoom*: byte
    mouseDelay*: word
    mouseObject*: byte
    mouseStartX*: byte
    looseTilesLevel*: word
    looseTilesRoom1*: byte
    looseTilesRoom2*: byte
    looseTilesFirstTile*: byte
    looseTilesLastTile*: byte
    jaffarVictoryLevel*: word
    jaffarVictoryFlashTime*: byte
    hideLevelNumberFromLevel*: word
    level13LevelNumber*: byte
    victoryStopsTimeLevel*: word
    winLevel*: word
    winRoom*: byte
    looseFloorDelay*: byte
    tblLevelType*: array[16, byte]
    tblLevelColor*: array[16, word]
    tblGuardType*: array[16, int16]
    tblGuardHp*: array[16, byte]
    tblCutscenesByIndex*: array[16, byte]
    tblEntryPose*: array[16, byte]
    tblSeamlessExit*: array[16, sbyte]

    # guard skills
    strikeprob*: array[NUM_GUARD_SKILLS, word]
    restrikeprob*: array[NUM_GUARD_SKILLS, word]
    blockprob*: array[NUM_GUARD_SKILLS, word]
    impblockprob*: array[NUM_GUARD_SKILLS, word]
    advprob*: array[NUM_GUARD_SKILLS, word]
    refractimer*: array[NUM_GUARD_SKILLS, word]
    extrastrength*: array[NUM_GUARD_SKILLS, word]

    # shadow's starting positions
    initShad6*: array[8, byte]
    initShad5*: array[8, byte]
    initShad12*: array[8, byte]
    # automatic moves
    demoMoves*: array[25, AutoMoveType]    # prince on demo level
    shadDrinkMove*: array[8, AutoMoveType] # shadow on level 5

    # speeds
    baseSpeed*: byte
    fightSpeed*: byte
    chomperSpeed*: byte
    noMouseInEnding*: byte

  DirectoryListingType* = object

  ControlType* = enum
    CONTROL_HELD = -1
    CONTROL_RELEASED = 0
    CONTROL_IGNORE = 1

  EdgeTp* = enum
    EDGE_TYPE_CLOSER, # closer/sword/potion
    EDGE_TYPE_WALL,   # wall/gate/tapestry/mirror/chomper
    EDGE_TYPE_FLOOR   # floor (nothing near char)

  # Types of gamepad input
  JoyinputType* = enum
    JOYINPUT_DPAD_LEFT,
    JOYINPUT_DPAD_RIGHT
    JOYINPUT_DPAD_UP,
    JOYINPUT_DPAD_DOWN,
    JOYINPUT_A,
    JOYINPUT_B,
    JOYINPUT_X,
    JOYINPUT_Y,
    JOYINPUT_START,
    JOYINPUT_BACK,
    JOYINPUT_NUM

  # Bit-flags used for the keystate array and joy_button_states array
  KeystateType* = enum
    KEYSTATE_HELD = 1 # True if key is currently held down
    KEYSTATE_HELD_NEW = 1 shl 1 # True if key was held down since last gameplay update

  HorizontalAlignment* = enum
    hAlignLeft = -1
    hAlignCenter
    hAlignRight

  VerticalAlignment* = enum
    vAlignTop = -1
    vAlignMiddle
    vAlignBottom

static:
  doAssert sizeof(LevelType) == 2305
  doAssert sizeof(DatShplType) == 100
  doAssert sizeof(DatHeaderType) == 6
  doAssert sizeof(DatResType) == 8
  doAssert sizeof(DatTableType) == 2
  doAssert sizeof(ImageDataType) == 6
  doAssert sizeof(RawfontType) == 10
  doAssert sizeof(NoteType) == 3
  doAssert sizeof(SpeakerType) == 2
  doAssert sizeof(DigiType) == 7
  doAssert sizeof(DigiNewType) == 9
  doAssert sizeof(OperatorType) == 5
  doAssert sizeof(InstrumentType) == 16
