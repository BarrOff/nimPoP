import types

import sdl2
import sdl2/[audio, gamecontroller, haptic, joystick]

const
  AUDIO_S16SYS* = AUDIO_S16LSB

var
  # data:5F8A
  textTimeRemaining*: word
  # data:4C56
  textTimeTotal*: word
  # data:431C
  isShowTime*: word
  # data:4C6E
  checkpoint*: word
  # data:4E92
  upsideDown*: word
  # data:3D36
  resurrectTime*: word
  # data:42B4
  dontResetTime*: word
  # data:4F7E
  remMin*: int16
  # data:4F82
  remTick*: word
  # data:4608
  hitpBegLev*: word
  # data:4CAA
  needLevel1Music*: word
  # data:4380
  offscreenSurface*: SurfaceType

  # data:31E5
  soundFlags*: byte = 0
  # data:295E
const
  screenRect* = RectType(top: 0, left: 0, bottom: 200, right: 320)
  TROBS_MAX* = 30
  JOY_AXIS_NUM* = 6 # Max quantity of analogue inputs on a gamepad/joystick

var
  # data:3D12
  drawMode*: word
  # data:42B8
  startLevel*: int16 = -1
  # data:4CE6
  guardPalettes*: ptr byte
  # data:4338
  chtabAddrs*: array[10, ChtabType]

when(USE_COPYPROT):
  var
    # data:4356
    copyprotPlac*: word
    # data:3D16
    copyprotIdx*: word
    # data:01CA

  const
    copyprotLetter* = @['A', 'A', 'B', 'B', 'C', 'C', 'D', 'D', 'E', 'F', 'F',
        'G', 'H', 'H', 'I', 'I', 'J', 'J', 'K', 'L', 'L', 'M', 'M', 'N', 'O',
        'O', 'P', 'P', 'R', 'R', 'S', 'S', 'T', 'T', 'U', 'U', 'V', 'Y', 'W', 'Y']

  var
    # data:4620
    cplevelEntr*: array[14, word]
var
  # data:46C6
  copyprotDialog*: ptr DialogType
  # data:2944
  dialogSettings*: DialogSettingsType

dialogSettings.topborder = 4
dialogSettings.leftBorder = 4
dialogSettings.bottomBorder = 4
dialogSettings.rightBorder = 4
dialogSettings.shadowBottom = 3
dialogSettings.shadowRight = 4
dialogSettings.outerBorder = 1
var
  # data:2B76
  dialogRect1* = RectType(top: 60, left: 56, bottom: 124, right: 264)
  # data:2B7E
  dialogRect2* = RectType(top: 61, left: 56, bottom: 120, right: 264)

  # data:409E
  drawnRoom*: word

  # data:4CCD
  currTile*: Tiles
  # data:4328
  currModifier*: byte

  # data:4CD8
  leftroom*: array[4, TileAndMod]
  # data:5950
  rowBelowLeft*: array[10, TileAndMod]

const
  # data:2274
  tblLine*: array[3, word] = [word(0), word(10), word(20)]

var
  # data:5966
  loadedRoom*: word
  # data:658A
  currRoomTiles*: ptr UncheckedArray[Tiles]
  # data:5F88
  currRoomModif*: ptr UncheckedArray[byte]
  # data:5968
  drawXh*: word
  # data:0F9E
  currentLevel*: word = high(word) # = -1
                                   # data:3021
  graphicsMode*: byte = 0

const
  # data:2BA6
  VGAPALETTEDEFAULT* = [
    RgbType(r: 0x00, g: 0x00, b: 0x00),
    RgbType(r: 0x00, g: 0x00, b: 0x2A),
    RgbType(r: 0x00, g: 0x2A, b: 0x00),
    RgbType(r: 0x00, g: 0x2A, b: 0x2A),
    RgbType(r: 0x2A, g: 0x00, b: 0x00),
    RgbType(r: 0x2A, g: 0x00, b: 0x2A),
    RgbType(r: 0x2A, g: 0x15, b: 0x00),
    RgbType(r: 0x2A, g: 0x2A, b: 0x2A),
    RgbType(r: 0x15, g: 0x15, b: 0x15),
    RgbType(r: 0x15, g: 0x15, b: 0x3F),
    RgbType(r: 0x15, g: 0x3F, b: 0x15),
    RgbType(r: 0x15, g: 0x3F, b: 0x3F),
    RgbType(r: 0x3F, g: 0x15, b: 0x15),
    RgbType(r: 0x3F, g: 0x15, b: 0x3F),
    RgbType(r: 0x3F, g: 0x3F, b: 0x15),
    RgbType(r: 0x3F, g: 0x3F, b: 0x3F)
  ]

var
  # data:4CC0
  roomL*: word
  # data:4CCE
  roomR*: word
  # data:4C90
  roomA*: word
  # data:4C96
  roomB*: word
  # data:461A
  roomBR*: word
  # data:43FE
  roomBL*: word
  # data:4614
  roomAR*: word
  # data:43DE
  roomAL*: word

  # data:4F84
  level*: LevelType

when (USE_COLORED_TORCHES):
  var
    torchColors*: array[1..24, array[30, byte]]         # indexed 1..24

var
  # data:42AA
  tableCounts*: array[5, int16]
  # TODO*: define statements for backtableCount, foretableCount,...
  # data:4D7E
  drectsCount*: int16
  # data:434E
  peelsCount*: int16

  # data:5FF4
  foretable*: array[200, BackTableType]
  # data:463C
  backtable*: array[200, BackTableType]
  # data:3D38
  midtable*: array[50, MidTableType]
  # data:5F1E
  peelsTable*: array[50, ptr PeelType]
  # data:4D9A
  drects*: array[30, RectType]

  # data:4CB8
  objDirection*: sbyte

const
  # data:2588
  chtabFlipClip*: array[10, byte] = [1'u8, 0, 1, 1, 1, 1, 0, 0, 0, 0]

var
  # data:42A6
  objClipLeft*: int16
  # data:42C6
  objClipTop*: int16
  # data:42C0
  objClipRight*: int16
  # data:4082
  objClipBottom*: int16
  # data:34D2
  wipetable*: array[300, WipeTableType]

const
  # data:2592
  chtabShift*: array[10, byte] = [0'u8, 1, 0, 0, 0, 0, 1, 1, 1, 0]

var
  # data:4354
  needDrects*: word
  # data:4CC2
  isBlindMode*: word

const
  # data:0F86
  rectTop* = RectType(top: 0, left: 0, bottom: 192, right: 320)
  # data:0F96
  rectBottomText* = RectType(top: 193, left: 70, bottom: 202, right: 250)

var
  # data:4CB2
  leveldoorRight*: word
  # data:4058
  leveldoorybottom*: word

  # data:4CFA
  palaceWallColors*: array[44*3, byte]

  # data:2942
  seedWasInit*: word = 0
  # data:4084
  randomSeed*: dword

  # data:3010
  currentTargetSurface*: SurfaceType

  # data:4C5C
  doorlink2Ad*: ptr UncheckedArray[byte]
  # data:4C5A
  doorlink1Ad*: ptr UncheckedArray[byte]

  # data:4CC6
  controlShift*: ControlType
  # data:461C
  controlY*: ControlType
  # data:4612
  controlX*: ControlType

when(USE_FADE):
  var
    # data:4CCA
    isGlobalFading*: word
    # data:4400
    fadePaletteBuffer*: PaletteFadeType

var
  # data:4358
  Kid*: CharType
  # data:295C
  isKeyboardMode*: word = 0
  # data:4E8A
  isPaused*: word
  # data:42D0
  isRestartLevel*: word
  # data:31E4
  soundMode*: byte = 0
  # data:42C8
  isJoystMode*: word
  # data:31E7
  isSoundOn*: byte = 0x0F
  # data:3D18
  nextLevel*: word
  # data:4C4A
  guardhpDelta*: int16
  # data:596A
  guardhpCurr*: word
  # data:4CC8
  nextRoom*: word
  # data:4C98
  hitpCurr*: word
  # data:5FF2
  hitpMax*: word
  # data:5FF0
  hitpDelta*: int16
  # data:4D94
  flashColor*: word
  # data:4350
  flashTime*: word
  # data:42DC
  Guard*: CharType

  # data:437E
  needQuotes*: word
  # data:4CF8
  roomleaveResult*: int16
  # data:4D96
  differentRoom*: word
  # data:4E94
  soundPointers*: array[58, ptr SoundBufferType]
  # data:4C58
  guardhpMax*: word
  # data:405C
  isFeatherFall*: word
  # data:4CBA
  chtabTitle40*: ChtabType
  # data:4CD0
  chtabTitle50*: ChtabType
  # data:405E
  hofCount*: int16


  # data:009A
  demoMode*: word = 0

  # data:42CA
  isCutscene*: word
  isEndingSequence*: bool # added

  # data:0FA0
  tblCutscenes*: array[16, CutscenePtrType]

var
  # data:408C
  mobsCount*: int16
  # data:4F7A
  trobsCount*: int16
  # data:4062
  nextSound*: int16
  # data:34AA
  grabTimer*: word
  # data:594C
  canGuardSeeKid*: int16
  # data:594E
  holdingSword*: word
  # data:4E90
  unitedWithShadow*: int16
  # data:409C
  leveldoorOpen*: word
  # data:4610
  demoIndex*: word
  # data:4CD4
  demoTime*: int16
  # data:34A2
  haveSword*: word

  # data:3D22
  Char*: CharType
  # data:4D80
  Opp*: CharType


  # data:42A2
  knock*: int16
  # data:4370
  isGuardNotice*: word

  # data:656C
  wipeFrames*: array[30, byte]
  # data:4C72
  wipeHeights*: array[30, sbyte]
  # data:34AC
  redrawFramesAnim*: array[30, byte]
  # data:43E0
  redrawFrames2*: array[30, byte]
  # data:4C1A
  redrawFramesFloorOverlay*: array[30, byte]
  # data:4064
  redrawFramesFull*: array[30, byte]
  # data:5EFE
  redrawFramesFore*: array[30, byte]
  # data:3484
  tileObjectRedraw*: array[30, byte]
  # data:4C64
  redrawFramesAbove*: array[10, byte]
  # data:4CE2
  needFullRedraw*: word
  # data:588E
  nCurrObjs*: int16
  # data:5BAC
  objtable*: array[50, ObjtableType]
  # data:5F8C
  currObjs*: array[50, int16]

  # data:4607
  objXh*: byte
  # data:4616
  objXl*: byte
  # data:4613
  objY*: byte
  # data:4C9A
  objChtab*: byte
  # data:42A4
  objId*: byte
  # data:431E
  objTilepos*: byte
  # data:4604
  objX*: int16

  # data:658C
  curFrame*: FrameType
  # data:5886
  seamless*: word
  # data:4CBC
  trob*: TrobType
  # data:4382
  trobs*: array[TROBS_MAX, TrobType]
  # data:431A
  redrawHeight*: int16
  # data:24DA
  soundInterruptible*: array[58, byte] = [
  0'u8, # sound0FellToDeath
  1,    # sound1Falling
  1,    # sound2TileCrashing
  1,    # sound3ButtonPressed
  1,    # sound4GateClosing
  1,    # sound5GateOpening
  0,    # sound6GateClosingFast
  1,    # sound7GateStop
  1,    # sound8Bumped
  1,    # sound9Grab
  1,    # sound10SwordVsSword
  1,    # sound11SwordMoving
  1,    # sound12GuardHurt
  1,    # sound13KidHurt
  0,    # sound14LeveldoorClosing
  0,    # sound15LeveldoorSliding
  1,    # sound16MediumLand
  1,    # sound17SoftLand
  0,    # sound18Drink
  1,    # sound19DrawSword
  1,    # sound20LooseShake1
  1,    # sound21LooseShake2
  1,    # sound22LooseShake3
  1,    # sound23Footstep
  0,    # sound24DeathRegular
  0,    # sound25Presentation
  0,    # sound26Embrace
  0,    # sound27Cutscene24612
  0,    # sound28DeathInFight
  1,    # sound29MeetJaffar
  0,    # sound30BigPotion
  0,    # sound31
  0,    # sound32ShadowMusic
  0,    # sound33SmallPotion
  0,    # sound34
  0,    # sound35Cutscene89
  0,    # sound36OutOfTime
  0,    # sound37Victory
  0,    # sound38Blink
  0,    # sound39LowWeight
  0,    # sound40Cutscene12int16Time
  0,    # sound41EndLevelMusic
  0,    # sound42
  0,    # sound43VictoryJaffar
  0,    # sound44SkelAlive
  0,    # sound45JumpThroughMirror
  0,    # sound46Chomped
  1,    # sound47Chomper
  0,    # sound48Spiked
  0,    # sound49Spikes
  0,    # sound50Story2Princess
  0,    # sound51PrincessDoorOpening
  0,    # sound52Story4JaffarLeaves
  0,    # sound53Story3JaffarComes
  0,    # sound54IntroMusic
  0,    # sound55Story1Absence
  0,    # sound56EndingMusic
  0
  ]
  # data:42ED
  currTilepos*: byte
  # data:432A
  currRoom*: int16
  # data:4CAC
  curmob*: MobType
  # data:4BB4
  mobs*: array[14, MobType]
  # data:4332
  tileCol*: int16

const
  # data:229C
  # List of every floor position for the 3 rows on screen (and also the row above the screen and the row below the screen)
  # These positions correspond to the standing position of characters on a floor
  yLand*: array[5, int16] = [-8'i16, 55, 118, 181, 244]

var
  # data:5888
  currGuardColor*: word
  # data:288C
  keyStates*: array[ord(SDL_NUM_SCANCODES), byte]
const
  # data:24A6
  # List of the left-most position of every tile on the screen (and the 5 tiles
  # to the left off-screen, and the 5 tiles to the right off-screen)
  xBump*: array[20, byte] = [uint8(255-12), 2, 16, 30, 44, 58, 72, 86, 100, 114,
      128, 142, 156, 170, 184, 198, 212, 226, 240, 254]

var
  # data:42F4
  isScreaming*: word
  # data:42EE
  offguard*: word   # name from Apple II source
                    # data:3D32
  droppedout*: word # name from Apple II source

when(USE_COPYPROT):
  var
    # data:00A2
    copyprotRoom*: array[14, word] = [3'u16, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4]
  const
    # data:00BE
    copyprotTile*: array[14, word] = [1'u16, 5, 7, 9, 11, 21, 1, 3, 7, 11, 17,
        21, 25, 27]

var
  # data:5BAA
  exitRoomTimer*: word
  # data:4372
  charColRight*: int16
  # data:5F86
  charColLeft*: int16
  # data:599C
  charTopRow*: int16
  # data:434C
  prevCharTopRow*: int16
  # data:432C
  prevCharColRight*: int16
  # data:42CE
  prevCharColLeft*: int16
  # data:34A4
  charBottomRow*: int16
  # data:3D34
  guardNoticeTimer*: int16
  # data:42A0
  jumpedThroughMirror*: int16
const
  # data:2292
  yClip*: array[5, int16] = [-60'i16, 3, 66, 129, 192]

var
  # data:42F9
  currTile2*: Tiles
  # data:4336
  tileRow*: int16




  # data:5F1C
  charWidthHalf*: word
  # data:4618
  charHeight*: word
  # data:3D1C
  charXLeft*: int16
  # data:3D20
  charXLeftColl*: int16
  # data:42F6
  charXRightColl*: int16
  # data:3D10
  charXRight*: int16
  # data:4D98
  charTopY*: int16
  # data:4096
  fallFrame*: byte
  # data:4C0E
  throughTile*: Tiles
  # data:5F82
  infrontx*: sbyte # name from Apple II source
const
  # data:228E
  dirFront*: array[2, sbyte] = [-1'i8, 1]
  # data:2290
  dirBehind*: array[2, sbyte] = [1'i8, -1]

var
  # data:4320
  currentSound*: word
  # data:4606
  controlShift2*: ControlType
  # data:42A8
  controlForward*: ControlType
  # data:4368
  guardSkill*: word
  # data:4088
  controlBackward*: ControlType
  # data:4322
  controlUp*: ControlType
  # data:409A
  controlDown*: ControlType
  # data:4CE0
  ctrl1Forward*: ControlType
  # data:4CD2
  ctrl1Backward*: ControlType
  # data:4D92
  ctrl1Up*: ControlType
  # data:4CD6
  ctrl1Down*: ControlType
  # data:42F8
  ctrl1Shift2*: ControlType

  # data:42F0
  shadowInitialized*: word

  # data:4330
  guardRefrac*: word
  # data:4098
  kidSwordStrike*: word


  # data:6591
  edgeType*: EdgeTp



  # data:596C
  onscreenSurface*: SurfaceType
  overlaySurface*: SurfaceType
  mergedSurface*: SurfaceType
  renderer*: RendererPtr
  isRendererTargettextureSupported*: bool
  popWindow*: WindowPtr
  isOverlayDisplayed*: bool
  textureSharp*: TexturePtr
  textureFuzzy*: TexturePtr
  textureBlurry*: TexturePtr
  targetTexture*: TexturePtr

  sdlController*: GameControllerPtr
  sdlJoystick*: JoystickPtr # in case our joystick is not compatible with SDLGameController
  usingSdlJoystickInterface*: byte
  joyAxis*: array[JOY_AXIS_NUM, int32] # hor/ver axes for left/right sticks + left and right triggers (in total 6 axes)
  joyAxisMax*: array[JOY_AXIS_NUM, int32] # same a above, but stores the highest value reached between game updates
  joyLeftStickStates*: array[2, int32]         # horizontal, vertical
  joyRightStickStates*: array[2, int32]
  joyButtonStates*: array[JOYINPUT_NUM, int32] # horizontal, vertical
  sdlHaptic*: HapticPtr

  perfCountersPerTick*: uint64
  perfFrequency*: uint64
  millisecondsPerCounter*: float

# moved here from seg009 to be usable in soundNames
const
  soundChannel*: int32 = 0
  maxSoundId*: int32 = 58

var
  # char** soundNames
  soundNames*: array[maxSoundId, string]

  gArgc*: int32
  # char** gArgv


  # data:405A
  collisionRow*: sbyte
  # data:42C2
  prevCollisionRow*: sbyte

  # data:4C10
  prevCollRoom*: array[10, sbyte]
  # data:4374
  currRowCollRoom*: array[10, sbyte]
  # data:3D06
  belowRowCollRoom*: array[10, sbyte]
  # data:42D2
  aboveRowCollRoom*: array[10, sbyte]
  # data:5890
  currRowCollFlags*: array[10, byte]
  # data:4CEA
  aboveRowCollFlags*: array[10, byte]
  # data:4C4C
  belowRowCollFlags*: array[10, byte]
  # data:5BA0
  prevCollFlags*: array[10, byte]


  # data:4F80
  pickupObjType*: int16


  # data:34CA
  justblocked*: word # name from Apple II source


  # data:5F84
  lastLooseSound*: word

  lastKeyScancode*: int
when(USE_TEXT):
  var
    hcFont*: FontType = new(FontType)
    textstate*: TextstateType = TextstateType(currentX: 0, currentY: 0,
        textblit: 0, textcolor: 15, ptrFont: hcFont)

var
  needQuickSave*: int32 = 0
  needQuickLoad*: int32 = 0


hcFont.firstChar = 0x01
hcFont.lastChar = 0xFF
hcFont.heightAboveBaseline = 7
hcFont.heightBelowBaseline = 2
hcFont.spaceBetweenLines = 1
hcFont.spaceBetweenChars = 1
hcFont.chtab = nil

when(USE_REPLAY):
  var
    recording*: byte = 0
    replaying*: byte = 0
    numReplayTicks*: dword = 0
    needStartReplay*: byte = 0
    needReplayCycle*: byte = 0
    # replaysFolder[POPMAXPATH] INIT(= "replays"): char
    replaysFolder* = "replays"
    specialMove*: byte
    savedRandomSeed*: dword
    preservedSeed*: dword
    keepLastSeed*: sbyte
    skippingReplay*: byte
    replaySeekTarget*: byte
    isValidateMode*: byte
    currTick*: dword = 0

var
  startFullscreen*: byte = 0
  fullCommandOption*: bool = false
  stdsndCommandOption*: bool = false
  popWindowWidth*: word = 640
  popWindowHeight*: word = 400
  useCustomLevelset*: byte = 0
  # levelsetName*: array[0..POPMAXPATH-1, char]
  # modDataPath*: array[0..POPMAXPATH-1, char]
  levelsetName*: string
  modDataPath*: string
  skipModDataFiles*: bool
  skipNormalDataFiles*: bool

  useFixesAndEnhancements*: byte = 0
  enableCopyprot*: byte = 0
  enableMusic*: byte = 1
  enableFade*: byte = 1
  enableFlash*: byte = 1
  enableText*: byte = 1
  enableInfoScreen*: byte = 1
  enableControllerRumble*: byte = 0
  joystickOnlyHorizontal*: byte = 0
  joystickThreshold*: int32 = 8000
  # gamecontrollerdbFile[POPMAXPATH] INIT(= ""): char
  gamecontrollerdbFile* = ""
  enableQuicksave*: byte = 1
  enableQuicksavePenalty*: byte = 1
  enableReplay*: byte = 1
  useHardwareAcceleration*: byte = 2
  useCorrectAspectRatio*: byte = 0
  useIntegerScaling*: byte = 0
  scalingType*: byte = 0
when(USE_LIGHTING):
  var
    enableLighting*: byte = 0
    lightingMask*: ImageType

var
  fixesSaved*: FixesOptionsType
  fixesDisabledState*: FixesOptionsType
  fixes*: ptr FixesOptionsType = addr(fixesDisabledState)
  useCustomOptions*: byte
  customSaved*: CustomOptionsType
  # customDefaults* = new(CustomOptionsType(
  customDefaults*: CustomOptionsType

customDefaults.startMinutesLeft = 60
customDefaults.startTicksLeft = 719
customDefaults.startHitp = 3
customDefaults.maxHitpAllowed = 10
customDefaults.savingAllowedFirstLevel = 3
customDefaults.savingAllowedLastLevel = 13
customDefaults.startUpsideDown = 0
customDefaults.startInBlindMode = 0
# data =009E
customDefaults.copyprotLevel = 2
customDefaults.drawnTileTopLevelEdge = tiles1Floor
customDefaults.drawnTileLeftLevelEdge = tiles20Wall
customDefaults.levelEdgeHitTile = tiles20Wall
customDefaults.allowTriggeringAnyTile = 0
customDefaults.enableWdaInPalace = 0
customDefaults.vgaPalette = VGAPALETTEDEFAULT
customDefaults.firstLevel = 1
customDefaults.skipTitle = 0
customDefaults.shiftLAllowedUntilLevel = 4
customDefaults.shiftLReducedMinutes = 15
customDefaults.shiftLReducedTicks = 719
customDefaults.demoHitp = 4
customDefaults.demoEndRoom = 24
customDefaults.introMusicLevel = 1
customDefaults.haveSwordFromLevel = 2
customDefaults.checkpointLevel = 3
customDefaults.checkpointRespawnDir = sbyte(dirFFLeft)
customDefaults.checkpointRespawnRoom = 2
customDefaults.checkpointRespawnTilepos = 6
customDefaults.checkpointClearTileRoom = 7
customDefaults.checkpointClearTileCol = 4
customDefaults.checkpointClearTileRow = 0
customDefaults.skeletonLevel = 3
customDefaults.skeletonRoom = 1
customDefaults.skeletonTriggerColumn1 = 2
customDefaults.skeletonTriggerColumn2 = 3
customDefaults.skeletonColumn = 5
customDefaults.skeletonRow = 1
customDefaults.skeletonRequireOpenLevelDoor = 1
customDefaults.skeletonSkill = 2
customDefaults.skeletonReappearRoom = 3
customDefaults.skeletonReappearX = 133
customDefaults.skeletonReappearRow = 1
customDefaults.skeletonReappearDir = byte(dir0Right)
customDefaults.mirrorLevel = 4
customDefaults.mirrorRoom = 4
customDefaults.mirrorColumn = 4
customDefaults.mirrorRow = 0
customDefaults.mirrorTile = tiles13Mirror
customDefaults.showMirrorImage = 1
customDefaults.shadowStealLevel = 5
customDefaults.shadowStealRoom = 24
customDefaults.shadowStepLevel = 6
customDefaults.shadowStepRoom = 1
customDefaults.fallingExitLevel = 6
customDefaults.fallingExitRoom = 1
customDefaults.fallingEntryLevel = 7
customDefaults.fallingEntryRoom = 17
customDefaults.mouseLevel = 8
customDefaults.mouseRoom = 16
customDefaults.mouseDelay = 150
customDefaults.mouseObject = 24
customDefaults.mouseStartX = 200
customDefaults.looseTilesLevel = 13
customDefaults.looseTilesRoom1 = 23
customDefaults.looseTilesRoom2 = 16
customDefaults.looseTilesFirstTile = 22
customDefaults.looseTilesLastTile = 27
customDefaults.jaffarVictoryLevel = 13
customDefaults.jaffarVictoryFlashTime = 18
customDefaults.hideLevelNumberFromLevel = 14
customDefaults.level13LevelNumber = 12
customDefaults.victoryStopsTimeLevel = 13
customDefaults.winLevel = 14
customDefaults.winRoom = 5
customDefaults.looseFloorDelay = 11
# data =02B2
customDefaults.tblLevelType = [0'u8, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0]
# 1.3
customDefaults.tblLevelColor = [0'u16, 0, 0, 1, 0, 0, 0, 1, 2, 2, 0, 0, 3, 3, 4, 0]
# data =03D4
customDefaults.tblGuardType = [0'i16, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 0, 4, 3, -1, -1]
# data =0EDA
customDefaults.tblGuardHp = [4'u8, 3, 3, 3, 3, 4, 5, 4, 4, 5, 5, 5, 4, 6, 0, 0]
customDefaults.tblCutscenesByIndex = [0'u8, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
    12, 13, 14, 15]
customDefaults.tblEntryPose = [0'u8, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0]
customDefaults.tblSeamlessExit = [-1'i8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, 23, -1, -1, -1]

# guard skills
customDefaults.strikeprob = [61'u16, 100, 61, 61, 61, 40, 100, 220, 0, 48, 32, 48]
customDefaults.restrikeprob = [0'u16, 0, 0, 5, 5, 175, 16, 8, 0, 255, 255, 150]
customDefaults.blockprob = [0'u16, 150, 150, 200, 200, 255, 200, 250, 0, 255, 255, 255]
customDefaults.impblockprob = [0'u16, 61, 61, 100, 100, 145, 100, 250, 0, 145, 255, 175]
customDefaults.advprob = [255'u16, 200, 200, 200, 255, 255, 200, 0, 0, 255, 100, 100]
customDefaults.refractimer = [16'u16, 16, 16, 16, 8, 8, 8, 8, 0, 8, 0, 0]
customDefaults.extrastrength = [0'u16, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]

# shadow's starting positions
customDefaults.initShad6 = [0x0F'u8, 0x51, 0x76, 0, 0, 1, 0, 0]
customDefaults.initShad5 = [0x0F'u8, 0x37, 0x37, 0, 0xFF, 0, 0, 0]
customDefaults.initShad12 = [0x0F'u8, 0x51, 0xE8, 0, 0, 0, 0, 0]
# automatic moves
customDefaults.demoMoves = [AutoMoveType(time: 0x00, move: 0), AutoMoveType(time: 0x01,
      move: 1), AutoMoveType(time: 0x0D, move: 0), AutoMoveType(time: 0x1E,
      move: 1), AutoMoveType(time: 0x25, move: 5), AutoMoveType(time: 0x2F,
      move: 0), AutoMoveType(time: 0x30, move: 1), AutoMoveType(time: 0x41,
      move: 0), AutoMoveType(time: 0x49, move: 2), AutoMoveType(time: 0x4B,
      move: 0), AutoMoveType(time: 0x63, move: 2), AutoMoveType(time: 0x64,
      move: 0), AutoMoveType(time: 0x73, move: 5), AutoMoveType(time: 0x80,
      move: 6), AutoMoveType(time: 0x88, move: 3), AutoMoveType(time: 0x9D,
      move: 7), AutoMoveType(time: 0x9E, move: 0), AutoMoveType(time: 0x9F,
      move: 1), AutoMoveType(time: 0xAB, move: 4), AutoMoveType(time: 0xB1,
      move: 0), AutoMoveType(time: 0xB2, move: 1), AutoMoveType(time: 0xBC,
      move: 0), AutoMoveType(time: 0xC1, move: 1), AutoMoveType(time: 0xCD,
      move: 0), AutoMoveType(time: 0xE9, move: -1)]
customDefaults.shadDrinkMove = [AutoMoveType(time: 0x00, move: 0), AutoMoveType(time: 0x01,
      move: 1), AutoMoveType(time: 0x0E, move: 0), AutoMoveType(time: 0x12,
      move: 6), AutoMoveType(time: 0x1D, move: 7), AutoMoveType(time: 0x2D,
      move: 2), AutoMoveType(time: 0x31, move: 1), AutoMoveType(time: 0xFF, move: -2)]
customDefaults.baseSpeed = 5'u8
customDefaults.fightSpeed = 6'u8
customDefaults.chomperSpeed = 15'u8
customDefaults.noMouseInEnding = 0

var
  custom*: ptr CustomOptionsType = addr(customDefaults)

  fullImage*: array[ord(MAX_FULLIMAGES), FullImageType]

fullImage[ord(TITLE_MAIN)] = FullImageType(id: 0, chtab: addr(chtabTitle50),
    blitter: blitters0NoTransp, xpos: 0, ypos: 0)
fullImage[ord(TITLE_PRESENTS)] = FullImageType(id: 1, chtab: addr(chtabTitle50),
    blitter: blitters0NoTransp, xpos: 96, ypos: 106)
fullImage[ord(TITLE_GAME)] = FullImageType(id: 2, chtab: addr(chtabTitle50),
    blitter: blitters0NoTransp, xpos: 96, ypos: 122)
fullImage[ord(TITLE_POP)] = FullImageType(id: 3, chtab: addr(chtabTitle50),
    blitter: blitters10hTransp, xpos: 24, ypos: 107)
fullImage[ord(TITLE_MECHNER)] = FullImageType(id: 4, chtab: addr(chtabTitle50),
    blitter: blitters0NoTransp, xpos: 48, ypos: 184)
fullImage[ord(HOF_POP)] = FullImageType(id: 3, chtab: addr(chtabTitle50),
    blitter: blitters10hTransp, xpos: 24, ypos: 24)
fullImage[ord(STORY_FRAME)] = FullImageType(id: 0, chtab: addr(chtabTitle40),
    blitter: blitters0NoTransp, xpos: 0, ypos: 0)
fullImage[ord(STORY_ABSENCE)] = FullImageType(id: 1, chtab: addr(chtabTitle40),
    blitter: blittersWhite, xpos: 24, ypos: 25)
fullImage[ord(STORY_MARRY)] = FullImageType(id: 2, chtab: addr(chtabTitle40),
    blitter: blittersWhite, xpos: 24, ypos: 25)
fullImage[ord(STORY_HAIL)] = FullImageType(id: 3, chtab: addr(chtabTitle40),
    blitter: blittersWhite, xpos: 24, ypos: 25)
fullImage[ord(STORY_CREDITS)] = FullImageType(id: 4, chtab: addr(chtabTitle40),
    blitter: blittersWhite, xpos: 24, ypos: 26)

var
  # data:009C
  cheatsEnabled*: word = 0
when(USE_DEBUG_CHEATS):
  var
    debugCheatsEnabled*: byte = 0
    isTimerDisplayed*: byte = 0
    isFeatherTimerDisplayed*: byte = 0

when(USE_MENU):
  var
    hcSmallFont* = new(FontType)
    haveMouseInput*: bool
    haveKeyboardOrControllerInput*: bool
    mouseX*, mouseY*: cint
    mouseMoved*: bool
    mouseClicked*: bool
    mouseButtonClickedRight*: bool
    pressedEnter*: bool
    escapeKeySuppressed*: bool
    menuControlScrollY*: int32
    isMenuShown*: sbyte
    enablePauseMenu*: byte = 1

  hcSmallFont.firstChar = 32
  hcSmallFont.lastChar = 126
  hcSmallFont.heightAboveBaseline = 5
  hcSmallFont.heightBelowBaseline = 2
  hcSmallFont.spaceBetweenLines = 1
  hcSmallFont.spaceBetweenChars = 1
  hcSmallFont.chtab = nil
var
  # modsFolder[POPMAXPATH] INIT(= "mods"): char
  modsFolder* = "mods"

  playDemoLevel*: bool = false

when(USE_REPLAY):
  var
    gDeprecationNumber*: int32

var
  alwaysUseOriginalMusic*: byte
  alwaysUseOriginalGraphics*: byte

when (USE_FAST_FORWARD):
  var
    audioSpeed*: int32 = 1 # =1 normally, >1 during fast forwarding

const
  # Horizontal size of tile in the internal coordinate system (a tile is 32 pixels wide in screen space)
  TILE_SIZEX* = 14
  TILE_MIDX* = 7          # Middle horizontal point of a tile
  TILE_RIGHTX* = 13       # Right-most point of a tile
                      # Vertical size of a tile (this also matches the pixel height of tiles in screen space)
  TILE_SIZEY* = 63
  # Position of the left-most pixel in screenspace in the internal coordinate system
  SCREENSPACE_X* = 58
  SCREEN_TILECOUNTX* = 10 # Quantity of columns of tiles visible in a room
  SCREEN_TILECOUNTY* = 3  # Quantity of rows of tiles visible in a room
                          # Used for referencing the first column visible in screen space in the x_bump array
  FIRST_ONSCREEN_COLUMN* = 5
  FALLING_SPEED_MAX* = 33
  FALLING_SPEED_ACCEL* = 3
  FALLING_SPEED_MAX_FEATHER* = 4
  FALLING_SPEED_ACCEL_FEATHER* = 1
  ROOMCOUNT* = 24         # Max quantity of rooms for any level
                          # Portion of the screen space dedicated to gameplay graphics
  SCREEN_GAMEPLAY_HEIGHT* = 192

  # for controlX in seg000.nim:
  CONTROL_HELD_LEFT* = CONTROL_HELD
  CONTROL_HELD_RIGHT* = CONTROL_IGNORE
  # for controlX elsewhere:
  CONTROL_HELD_FORWARD* = CONTROL_HELD
  CONTROL_HELD_BACKWARD* = CONTROL_IGNORE
  # for controlY:
  CONTROL_HELD_UP* = CONTROL_HELD
  CONTROL_HELD_DOWN* = CONTROL_IGNORE

var
  sdlv*: SDL_Version

sdl2.getVersion(sdlv)

when(cpuEndian == bigEndian):
  const
    Rmsk* = 0x00ff0000
    Gmsk* = 0x0000ff00
    Bmsk* = 0x000000ff
    Amsk* = 0xff000000'u32
else:
  const
    Rmsk* = 0x000000ff
    Gmsk* = 0x0000ff00
    Bmsk* = 0x00ff0000
    Amsk* = 0xff000000'u32
