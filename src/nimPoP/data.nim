const
  NUM_SCANCODES = 512

var
  # data:5F8A
  textTimeRemaining*: word
  # data:4C56
  textTimeTotal*: word
  # data:431C
  isShowTime*: word
  # data:4C6E
  checkpoint*: word
  # data:4E92
  upsideDown*: word
  # data:3D36
  resurrectTime*: word
  # data:42B4
  dontResetTime*: word
  # data:4F7E
  remMin*: int16
  # data:4F82
  remTick*: word
  # data:4608
  hitpBegLev*: word
  # data:4CAA
  needLevel1Music*: word
  # data:4380
  offscreenSurface*: SurfaceType

  # data:31E5
  soundFlags*: byte = 0
  # data:295E
var
  screenRect* = RectType(top: 0, left: 0, bottom: 200, right: 320)

var
  # data:3D12
  drawMode*: word
  # data:42B8
  startLevel*: int16 = -1
  # data:4CE6
  guardPalettes*: ptr byte
  # data:4338
  chtabAddrs*: array[10, ChtabType]

when(USE_COPYPROT):
  var
    # data:4356
    copyprotPlac*: word
    # data:3D16
    copyprotIdx*: word
    # data:01CA

  const
    copyprotLetter* = @['A', 'A', 'B', 'B', 'C', 'C', 'D', 'D', 'E', 'F', 'F',
        'G', 'H', 'H', 'I', 'I', 'J', 'J', 'K', 'L', 'L', 'M', 'M', 'N', 'O',
        'O', 'P', 'P', 'R', 'R', 'S', 'S', 'T', 'T', 'U', 'U', 'V', 'Y', 'W', 'Y']

  var
    # data:4620
    cplevelEntr*: array[14, word]
var
  # data:46C6
  copyprotDialog*: ptr DialogType
  # data:2944
  dialogSettings*: DialogSettingsType

dialogSettings.method1 = addDialogRect
dialogSettings.method2Frame = dialogMethod2Frame
dialogSettings.topborder = 4
dialogSettings.leftBorder = 4
dialogSettings.bottomBorder = 4
dialogSettings.rightBorder = 4
dialogSettings.shadowBottom = 3
dialogSettings.shadowRight = 4
dialogSettings.outerBorder = 1
var
  # data:2B76
  dialogRect1* = RectType(top: 60, left: 56, bottom: 124, right: 264)
  # data:2B7E
  dialogRect2* = RectType(top: 61, left: 56, bottom: 120, right: 264)

  # data:409E
  drawnRoom*: word

  # data:4CCD
  currTile*: byte
  # data:4328
  currModifier*: byte

  # data:4CD8
  leftroom*: array[4, TileAndMod]
  # data:5950
  rowBelowLeft*: array[10, TileAndMod]
  # data:2274
  tblLine*: array[3, word] = [word(0), word(10), word(20)]

  # data:5966
  loadedRoom*: word
  # data:658A
  currRoomTiles*: ptr UncheckedArray[byte]
  # data:5F88
  currRoomModif*: ptr UncheckedArray[byte]
  # data:5968
  drawXh*: word
  # data:0F9E
  currentLevel*: word = high(word) # = -1
                                   # data:3021
  graphicsMode*: byte = 0

const
  # data:2BA6
  VGAPALETTEDEFAULT* = [
    RgbType(r: 0x00, g: 0x00, b: 0x00),
    RgbType(r: 0x00, g: 0x00, b: 0x2A),
    RgbType(r: 0x00, g: 0x2A, b: 0x00),
    RgbType(r: 0x00, g: 0x2A, b: 0x2A),
    RgbType(r: 0x2A, g: 0x00, b: 0x00),
    RgbType(r: 0x2A, g: 0x00, b: 0x2A),
    RgbType(r: 0x2A, g: 0x15, b: 0x00),
    RgbType(r: 0x2A, g: 0x2A, b: 0x2A),
    RgbType(r: 0x15, g: 0x15, b: 0x15),
    RgbType(r: 0x15, g: 0x15, b: 0x3F),
    RgbType(r: 0x15, g: 0x3F, b: 0x15),
    RgbType(r: 0x15, g: 0x3F, b: 0x3F),
    RgbType(r: 0x3F, g: 0x15, b: 0x15),
    RgbType(r: 0x3F, g: 0x15, b: 0x3F),
    RgbType(r: 0x3F, g: 0x3F, b: 0x15),
    RgbType(r: 0x3F, g: 0x3F, b: 0x3F)
  ]

var
  # data:4CC0
  roomL*: word
  # data:4CCE
  roomR*: word
  # data:4C90
  roomA*: word
  # data:4C96
  roomB*: word
  # data:461A
  roomBR*: word
  # data:43FE
  roomBL*: word
  # data:4614
  roomAR*: word
  # data:43DE
  roomAL*: word

  # data:4F84
  level*: LevelType

when (USE_COLORED_TORCHES):
  var
    torchColors*: array[1..24, array[30, byte]]         # indexed 1..24

var
  # data:42AA
  tableCounts*: array[5, int16]
  # TODO*: define statements for backtableCount, foretableCount,...
  # data:4D7E
  drectsCount*: int16
  # data:434E
  peelsCount*: int16

  # data:5FF4
  foretable*: array[200, BackTableType]
  # data:463C
  backtable*: array[200, BackTableType]
  # data:3D38
  midtable*: array[50, MidTableType]
  # data:5F1E
  peelsTable*: array[50, ptr PeelType]
  # data:4D9A
  drects*: array[30, RectType]

  # data:4CB8
  objDirection*: sbyte

const
  # data:2588
  chtabFlipClip*: array[10, byte] = [1'u8, 0'u8, 1'u8, 1'u8, 1'u8, 1'u8, 0'u8,
      0'u8, 0'u8, 0'u8]

var
  # data:42A6
  objClipLeft*: int16
  # data:42C6
  objClipTop*: int16
  # data:42C0
  objClipRight*: int16
  # data:4082
  objClipBottom*: int16
  # data:34D2
  wipetable*: array[300, WipeTableType]

const
  # data:2592
  chtabShift*: array[10, byte] = [0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 1'u8,
      1'u8, 1'u8, 0'u8]

var
  # data:4354
  needDrects*: word
  # data:4CC2
  isBlindMode*: word

var
  # data:0F86
  rectTop* = RectType(top: 0, left: 0, bottom: 192, right: 320)
  # data:0F96
  rectBottomText* = RectType(top: 193, left: 70, bottom: 202, right: 250)

var
  # data:4CB2
  leveldoorRight*: word
  # data:4058
  leveldoorybottom*: word

  # data:4CFA
  palaceWallColors*: array[44*3, byte]

  # data:2942
  seedWasInit*: word = 0
  # data:4084
  randomSeed*: dword

  # data:3010
  currentTargetSurface*: SurfaceType

  # data:4C5C
  doorlink2Ad*: ptr UncheckedArray[byte]
  # data:4C5A
  doorlink1Ad*: ptr UncheckedArray[byte]

  # data:4CC6
  controlShift*: sbyte
  # data:461C
  controlY*: sbyte
  # data:4612
  controlX*: sbyte

when(USE_FADE):
  var
    # data:4CCA
    isGlobalFading*: word
    # data:4400
    fadePaletteBuffer*: PaletteFadeType

var
  # data:4358
  Kid*: CharType
  # data:295C
  isKeyboardMode*: word = 0
  # data:4E8A
  isPaused*: word
  # data:42D0
  isRestartLevel*: word
  # data:31E4
  soundMode*: byte = 0
  # data:42C8
  isJoystMode*: word
  # data:31E7
  isSoundOn*: byte = 0x0F
  # data:3D18
  nextLevel*: word
  # data:4C4A
  guardhpDelta*: int16
  # data:596A
  guardhpCurr*: word
  # data:4CC8
  nextRoom*: word
  # data:4C98
  hitpCurr*: word
  # data:5FF2
  hitpMax*: word
  # data:5FF0
  hitpDelta*: int16
  # data:4D94
  flashColor*: word
  # data:4350
  flashTime*: word
  # data:42DC
  Guard*: CharType

  # data:437E
  needQuotes*: word
  # data:4CF8
  roomleaveResult*: int16
  # data:4D96
  differentRoom*: word
  # data:4E94
  soundPointers*: array[58, ptr SoundBufferType]
  # data:4C58
  guardhpMax*: word
  # data:405C
  isFeatherFall*: word
  # data:4CBA
  chtabTitle40*: ChtabType
  # data:4CD0
  chtabTitle50*: ChtabType
  # data:405E
  hofCount*: int16


  # data:009A
  demoMode*: word = 0

  # data:42CA
  isCutscene*: word
  isEndingSequence*: bool # added

  # data:0FA0
  tblCutscenes*: array[16, CutscenePtrType]
tblCutscenes[2] = cutscene26
tblCutscenes[4] = cutscene4
tblCutscenes[6] = cutscene26
tblCutscenes[8] = cutscene8
tblCutscenes[9] = cutscene9
tblCutscenes[12] = cutscene12

var
  # data:408C
  mobsCount*: int16
  # data:4F7A
  trobsCount*: int16
  # data:4062
  nextSound*: int16
  # data:34AA
  grabTimer*: word
  # data:594C
  canGuardSeeKid*: int16
  # data:594E
  holdingSword*: word
  # data:4E90
  unitedWithShadow*: int16
  # data:409C
  leveldoorOpen*: word
  # data:4610
  demoIndex*: word
  # data:4CD4
  demoTime*: int16
  # data:34A2
  haveSword*: word

  # data:3D22
  Char*: CharType
  # data:4D80
  Opp*: CharType


  # data:42A2
  knock*: int16
  # data:4370
  isGuardNotice*: word

  # data:656C
  wipeFrames*: array[30, byte]
  # data:4C72
  wipeHeights*: array[30, sbyte]
  # data:34AC
  redrawFramesAnim*: array[30, byte]
  # data:43E0
  redrawFrames2*: array[30, byte]
  # data:4C1A
  redrawFramesFloorOverlay*: array[30, byte]
  # data:4064
  redrawFramesFull*: array[30, byte]
  # data:5EFE
  redrawFramesFore*: array[30, byte]
  # data:3484
  tileObjectRedraw*: array[30, byte]
  # data:4C64
  redrawFramesAbove*: array[10, byte]
  # data:4CE2
  needFullRedraw*: word
  # data:588E
  nCurrObjs*: int16
  # data:5BAC
  objtable*: array[50, ObjtableType]
  # data:5F8C
  currObjs*: array[50, int16]

  # data:4607
  objXh*: byte
  # data:4616
  objXl*: byte
  # data:4613
  objY*: byte
  # data:4C9A
  objChtab*: byte
  # data:42A4
  objId*: byte
  # data:431E
  objTilepos*: byte
  # data:4604
  objX*: int16

  # data:658C
  curFrame*: FrameType
  # data:5886
  seamless*: word
  # data:4CBC
  trob*: TrobType
  # data:4382
  trobs*: array[30, TrobType]
  # data:431A
  redrawHeight*: int16
  # data:24DA
  soundInterruptible*: seq[byte] = @[
  0'u8, # sound0FellToDeath
  1'u8, # sound1Falling
  1'u8, # sound2TileCrashing
  1'u8, # sound3ButtonPressed
  1'u8, # sound4GateClosing
  1'u8, # sound5GateOpening
  0'u8, # sound6GateClosingFast
  1'u8, # sound7GateStop
  1'u8, # sound8Bumped
  1'u8, # sound9Grab
  1'u8, # sound10SwordVsSword
  1'u8, # sound11SwordMoving
  1'u8, # sound12GuardHurt
  1'u8, # sound13KidHurt
  0'u8, # sound14LeveldoorClosing
  0'u8, # sound15LeveldoorSliding
  1'u8, # sound16MediumLand
  1'u8, # sound17SoftLand
  0'u8, # sound18Drink
  1'u8, # sound19DrawSword
  1'u8, # sound20LooseShake1
  1'u8, # sound21LooseShake2
  1'u8, # sound22LooseShake3
  1'u8, # sound23Footstep
  0'u8, # sound24DeathRegular
  0'u8, # sound25Presentation
  0'u8, # sound26Embrace
  0'u8, # sound27Cutscene24612
  0'u8, # sound28DeathInFight
  1'u8, # sound29MeetJaffar
  0'u8, # sound30BigPotion
  0'u8, # sound31
  0'u8, # sound32ShadowMusic
  0'u8, # sound33SmallPotion
  0'u8, # sound34
  0'u8, # sound35Cutscene89
  0'u8, # sound36OutOfTime
  0'u8, # sound37Victory
  0'u8, # sound38Blink
  0'u8, # sound39LowWeight
  0'u8, # sound40Cutscene12int16Time
  0'u8, # sound41EndLevelMusic
  0'u8, # sound42
  0'u8, # sound43VictoryJaffar
  0'u8, # sound44SkelAlive
  0'u8, # sound45JumpThroughMirror
  0'u8, # sound46Chomped
  1'u8, # sound47Chomper
  0'u8, # sound48Spiked
  0'u8, # sound49Spikes
  0'u8, # sound50Story2Princess
  0'u8, # sound51PrincessDoorOpening
  0'u8, # sound52Story4JaffarLeaves
  0'u8, # sound53Story3JaffarComes
  0'u8, # sound54IntroMusic
  0'u8, # sound55Story1Absence
  0'u8, # sound56EndingMusic
  0'u8
  ]
  # data:42ED
  currTilepos*: byte
  # data:432A
  currRoom*: int16
  # data:4CAC
  curmob*: MobType
  # data:4BB4
  mobs*: array[14, MobType]
  # data:4332
  tileCol*: int16

const
  # data:229C
  yLand*: array[5, int16] = [ -8'i16, 55'i16, 118'i16, 181'i16, 244'i16]

var
  # data:5888
  currGuardColor*: word
  # data:288C
  keyStates*: array[NUM_SCANCODES, byte]
const
  # data:24A6
  xBump*: array[20, byte] = [uint8(255-12), 2'u8, 16'u8, 30'u8, 44'u8, 58'u8,
      72'u8, 86'u8, 100'u8, 114'u8, 128'u8, 142'u8, 156'u8, 170'u8, 184'u8,
      198'u8, 212'u8, 226'u8, 240'u8, 254'u8]

var
  # data:42F4
  isScreaming*: word
  # data:42EE
  offguard*: word   # name from Apple II source
                    # data:3D32
  droppedout*: word # name from Apple II source

when(USE_COPYPROT):
  var
    # data:00A2
    copyprotRoom*: array[14, word] = [3'u16, 3'u16, 3'u16, 3'u16, 3'u16,
        3'u16, 4'u16, 4'u16, 4'u16, 4'u16, 4'u16, 4'u16, 4'u16, 4'u16]
  const
    # data:00BE
    copyprotTile*: array[14, word] = [1'u16, 5'u16, 7'u16, 9'u16, 11'u16,
        21'u16, 1'u16, 3'u16, 7'u16, 11'u16, 17'u16, 21'u16, 25'u16, 27'u16]

var
  # data:5BAA
  exitRoomTimer*: word
  # data:4372
  charColRight*: int16
  # data:5F86
  charColLeft*: int16
  # data:599C
  charTopRow*: int16
  # data:434C
  prevCharTopRow*: int16
  # data:432C
  prevCharColRight*: int16
  # data:42CE
  prevCharColLeft*: int16
  # data:34A4
  charBottomRow*: int16
  # data:3D34
  guardNoticeTimer*: int16
  # data:42A0
  jumpedThroughMirror*: int16
const
  # data:2292
  yClip*: array[5, int16] = [ -60'i16, 3'i16, 66'i16, 129'i16, 192'i16]

var
  # data:42F9
  currTile2*: byte
  # data:4336
  tileRow*: int16




  # data:5F1C
  charWidthHalf*: word
  # data:4618
  charHeight*: word
  # data:3D1C
  charXLeft*: int16
  # data:3D20
  charXLeftColl*: int16
  # data:42F6
  charXRightColl*: int16
  # data:3D10
  charXRight*: int16
  # data:4D98
  charTopY*: int16
  # data:4096
  fallFrame*: byte
  # data:4C0E
  throughTile*: byte
  # data:5F82
  infrontx*: sbyte # name from Apple II source
const
  # data:228E
  dirFront*: array[2, sbyte] = [ -1'i8, 1'i8]
  # data:2290
  dirBehind*: array[2, sbyte] = [1'i8, -1'i8]

var
  # data:4320
  currentSound*: word
  # data:4606
  controlShift2*: sbyte
  # data:42A8
  controlForward*: sbyte
  # data:4368
  guardSkill*: word
  # data:4088
  controlBackward*: sbyte
  # data:4322
  controlUp*: sbyte
  # data:409A
  controlDown*: sbyte
  # data:4CE0
  ctrl1Forward*: sbyte
  # data:4CD2
  ctrl1Backward*: sbyte
  # data:4D92
  ctrl1Up*: sbyte
  # data:4CD6
  ctrl1Down*: sbyte
  # data:42F8
  ctrl1Shift2*: sbyte

  # data:42F0
  shadowInitialized*: word

  # data:4330
  guardRefrac*: word
  # data:4098
  kidSwordStrike*: word


  # data:6591
  edgeType*: byte



  # data:596C
  onscreenSurface*: Surface
  overlaySurface*: Surface
  mergedSurface*: Surface
  renderer*: Renderer
  isRendererTargettextureSupported*: bool
  window*: Window
  isOverlayDisplayed*: bool
  textureSharp*: Texture
  textureFuzzy*: Texture
  textureBlurry*: Texture
  targetTexture*: Texture

  sdlController*: GameController
  sdlJoystick*: Joystick # in case our joystick is not compatible with SDLGameController
  usingSdlJoystickInterface*: byte
  joyAxis*: array[6, int32] # hor/ver axes for left/right sticks + left and right triggers (in total 6 axes)
  joyLeftStickStates*: array[2, int32] # horizontal, vertical
  joyRightStickStates*: array[2, int32]
  joyHatStates*: array[2, int32]       # horizontal, vertical
  joyAYButtonsState*: int32
  joyXButtonState*: int32
  joyBButtonState*: int32
  sdlHaptic*: Haptic

  perfCountersPerTick*: uint64
  perfFrequency*: uint64
  millisecondsPerCounter*: float

# moved here from seg009 to be usable in soundNames
const
  soundChannel: int32 = 0
  maxSoundId: int32 = 58

var
  # char** soundNames
  soundNames*: array[maxSoundId, string]

  gArgc*: int32
  # char** gArgv


  # data:405A
  collisionRow*: sbyte
  # data:42C2
  prevCollisionRow*: sbyte

  # data:4C10
  prevCollRoom*: array[10, sbyte]
  # data:4374
  currRowCollRoom*: array[10, sbyte]
  # data:3D06
  belowRowCollRoom*: array[10, sbyte]
  # data:42D2
  aboveRowCollRoom*: array[10, sbyte]
  # data:5890
  currRowCollFlags*: array[10, byte]
  # data:4CEA
  aboveRowCollFlags*: array[10, byte]
  # data:4C4C
  belowRowCollFlags*: array[10, byte]
  # data:5BA0
  prevCollFlags*: array[10, byte]


  # data:4F80
  pickupObjType*: int16


  # data:34CA
  justblocked*: word # name from Apple II source


  # data:5F84
  lastLooseSound*: word

  lastKeyScancode*: int32
when(USE_TEXT):
  var
    hcFont*: FontType = new(FontType)
    textstate*: TextstateType = TextstateType(currentX: 0, currentY: 0,
        textblit: 0, textcolor: 15, ptrFont: hcFont)

var
  needQuickSave*: int32 = 0
  needQuickLoad*: int32 = 0


hcFont.firstChar = 0x01
hcFont.lastChar = 0xFF
hcFont.heightAboveBaseline = 7
hcFont.heightBelowBaseline = 2
hcFont.spaceBetweenLines = 1
hcFont.spaceBetweenChars = 1
hcFont.chtab = nil

when(USE_REPLAY):
  var
    recording*: byte = 0
    replaying*: byte = 0
    numReplayTicks*: dword = 0
    needStartReplay*: byte = 0
    needReplayCycle*: byte = 0
    # replaysFolder[POPMAXPATH] INIT(= "replays"): char
    replaysFolder* = "replays"
    specialMove*: byte
    savedRandomSeed*: dword
    preservedSeed*: dword
    keepLastSeed*: sbyte
    skippingReplay*: byte
    replaySeekTarget*: byte
    isValidateMode*: byte
    currTick*: dword = 0

var
  startFullscreen*: byte = 0
  fullCommandOption*: bool = false
  stdsndCommandOption*: bool = false
  popWindowWidth*: word = 640
  popWindowHeight*: word = 400
  useCustomLevelset*: byte = 0
  # levelsetName*: array[0..POPMAXPATH-1, char]
  # modDataPath*: array[0..POPMAXPATH-1, char]
  levelsetName*: string
  modDataPath*: string
  skipModDataFiles*: bool
  skipNormalDataFiles*: bool

  useFixesAndEnhancements*: byte = 0
  enableCopyprot*: byte = 0
  enableMusic*: byte = 1
  enableFade*: byte = 1
  enableFlash*: byte = 1
  enableText*: byte = 1
  enableInfoScreen*: byte = 1
  enableControllerRumble*: byte = 0
  joystickOnlyHorizontal*: byte = 0
  joystickThreshold*: int32 = 8000
  # gamecontrollerdbFile[POPMAXPATH] INIT(= ""): char
  gamecontrollerdbFile* = ""
  enableQuicksave*: byte = 1
  enableQuicksavePenalty*: byte = 1
  enableReplay*: byte = 1
  useCorrectAspectRatio*: byte = 0
  useIntegerScaling*: byte = 0
  scalingType*: byte = 0
when(USE_LIGHTING):
  var
    enableLighting*: byte = 0
    lightingMask*: ImageType

var
  fixesSaved*: FixesOptionsType
  fixesDisabledState*: FixesOptionsType
  fixes*: ptr FixesOptionsType = addr(fixesDisabledState)
  useCustomOptions*: byte
  customSaved*: CustomOptionsType
  # customDefaults* = new(CustomOptionsType(
  customDefaults*: CustomOptionsType

customDefaults.startMinutesLeft = 60
customDefaults.startTicksLeft = 719
customDefaults.startHitp = 3
customDefaults.maxHitpAllowed = 10
customDefaults.savingAllowedFirstLevel = 3
customDefaults.savingAllowedLastLevel = 13
customDefaults.startUpsideDown = 0
customDefaults.startInBlindMode = 0
# data =009E
customDefaults.copyprotLevel = 2
customDefaults.drawnTileTopLevelEdge = byte(tiles1Floor)
customDefaults.drawnTileLeftLevelEdge = byte(tiles20Wall)
customDefaults.levelEdgeHitTile = byte(tiles20Wall)
customDefaults.allowTriggeringAnyTile = 0
customDefaults.enableWdaInPalace = 0
customDefaults.vgaPalette = VGAPALETTEDEFAULT
customDefaults.firstLevel = 1
customDefaults.skipTitle = 0
customDefaults.shiftLAllowedUntilLevel = 4
customDefaults.shiftLReducedMinutes = 15
customDefaults.shiftLReducedTicks = 719
customDefaults.demoHitp = 4
customDefaults.demoEndRoom = 24
customDefaults.introMusicLevel = 1
customDefaults.haveSwordFromLevel = 2
customDefaults.checkpointLevel = 3
customDefaults.checkpointRespawnDir = sbyte(dirFFLeft)
customDefaults.checkpointRespawnRoom = 2
customDefaults.checkpointRespawnTilepos = 6
customDefaults.checkpointClearTileRoom = 7
customDefaults.checkpointClearTileCol = 4
customDefaults.checkpointClearTileRow = 0
customDefaults.skeletonLevel = 3
customDefaults.skeletonRoom = 1
customDefaults.skeletonTriggerColumn1 = 2
customDefaults.skeletonTriggerColumn2 = 3
customDefaults.skeletonColumn = 5
customDefaults.skeletonRow = 1
customDefaults.skeletonRequireOpenLevelDoor = 1
customDefaults.skeletonSkill = 2
customDefaults.skeletonReappearRoom = 3
customDefaults.skeletonReappearX = 133
customDefaults.skeletonReappearRow = 1
customDefaults.skeletonReappearDir = byte(dir0Right)
customDefaults.mirrorLevel = 4
customDefaults.mirrorRoom = 4
customDefaults.mirrorColumn = 4
customDefaults.mirrorRow = 0
customDefaults.mirrorTile = byte(tiles13Mirror)
customDefaults.showMirrorImage = 1
customDefaults.fallingExitLevel = 6
customDefaults.fallingExitRoom = 1
customDefaults.fallingEntryLevel = 7
customDefaults.fallingEntryRoom = 17
customDefaults.mouseLevel = 8
customDefaults.mouseRoom = 16
customDefaults.mouseDelay = 150
customDefaults.mouseObject = 24
customDefaults.mouseStartX = 200
customDefaults.looseTilesLevel = 13
customDefaults.looseTilesRoom1 = 23
customDefaults.looseTilesRoom2 = 16
customDefaults.looseTilesFirstTile = 22
customDefaults.looseTilesLastTile = 27
customDefaults.jaffarVictoryLevel = 13
customDefaults.jaffarVictoryFlashTime = 18
customDefaults.hideLevelNumberFromLevel = 14
customDefaults.level13LevelNumber = 12
customDefaults.victoryStopsTimeLevel = 13
customDefaults.winLevel = 14
customDefaults.winRoom = 5
customDefaults.looseFloorDelay = 11
# data =02B2
customDefaults.tblLevelType = [0'u8, 0'u8, 0'u8, 0'u8, 1'u8, 1'u8, 1'u8, 0'u8,
    0'u8, 0'u8, 1'u8, 1'u8, 0'u8, 0'u8, 1'u8, 0'u8]
# 1.3
customDefaults.tblLevelColor = [0'u16, 0'u16, 0'u16, 1'u16, 0'u16, 0'u16, 0'u16,
    1'u16, 2'u16, 2'u16, 0'u16, 0'u16, 3'u16, 3'u16, 4'u16, 0'u16]
# data =03D4
customDefaults.tblGuardType = [0'i16, 0'i16, 0'i16, 2'i16, 0'i16, 0'i16, 1'i16,
    0'i16, 0'i16, 0'i16, 0'i16, 0'i16, 4'i16, 3'i16, -1'i16, -1'i16]
# data =0EDA
customDefaults.tblGuardHp = [4'u8, 3'u8, 3'u8, 3'u8, 3'u8, 4'u8, 5'u8, 4'u8,
    4'u8, 5'u8, 5'u8, 5'u8, 4'u8, 6'u8, 0'u8, 0'u8]
customDefaults.tblCutscenesByIndex = [0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8,
    7'u8, 8'u8, 9'u8, 10'u8, 11'u8, 12'u8, 13'u8, 14'u8, 15'u8]
customDefaults.tblEntryPose = [0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8,
    0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 2'u8, 0'u8, 0'u8]
customDefaults.tblSeamlessExit = [ -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, -1'i8,
    -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, 23'i8, -1'i8, -1'i8, -1'i8]

# guard skills
customDefaults.strikeprob = [61'u16, 100'u16, 61'u16, 61'u16, 61'u16, 40'u16,
    100'u16, 220'u16, 0'u16, 48'u16, 32'u16, 48'u16]
customDefaults.restrikeprob = [0'u16, 0'u16, 0'u16, 5'u16, 5'u16, 175'u16,
    16'u16, 8'u16, 0'u16, 255'u16, 255'u16, 150'u16]
customDefaults.blockprob = [0'u16, 150'u16, 150'u16, 200'u16, 200'u16, 255'u16,
    200'u16, 250'u16, 0'u16, 255'u16, 255'u16, 255'u16]
customDefaults.impblockprob = [0'u16, 61'u16, 61'u16, 100'u16, 100'u16, 145'u16,
    100'u16, 250'u16, 0'u16, 145'u16, 255'u16, 175'u16]
customDefaults.advprob = [255'u16, 200'u16, 200'u16, 200'u16, 255'u16, 255'u16,
    200'u16, 0'u16, 0'u16, 255'u16, 100'u16, 100'u16]
customDefaults.refractimer = [16'u16, 16'u16, 16'u16, 16'u16, 8'u16, 8'u16,
    8'u16, 8'u16, 0'u16, 8'u16, 0'u16, 0'u16]
customDefaults.extrastrength = [0'u16, 0'u16, 0'u16, 0'u16, 1'u16, 0'u16, 0'u16,
    0'u16, 0'u16, 0'u16, 0'u16, 0'u16]

# shadow's starting positions
customDefaults.initShad6 = [0x0F'u8, 0x51'u8, 0x76'u8, 0'u8, 0'u8, 1'u8, 0'u8, 0'u8]
customDefaults.initShad5 = [0x0F'u8, 0x37'u8, 0x37'u8, 0'u8, 0xFF'u8, 0'u8,
    0'u8, 0'u8]
customDefaults.initShad12 = [0x0F'u8, 0x51'u8, 0xE8'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8]
# automatic moves
customDefaults.demoMoves = [AutoMoveType(time: 0x00, move: 0), AutoMoveType(time: 0x01,
      move: 1), AutoMoveType(time: 0x0D, move: 0), AutoMoveType(time: 0x1E,
      move: 1), AutoMoveType(time: 0x25, move: 5), AutoMoveType(time: 0x2F,
      move: 0), AutoMoveType(time: 0x30, move: 1), AutoMoveType(time: 0x41,
      move: 0), AutoMoveType(time: 0x49, move: 2), AutoMoveType(time: 0x4B,
      move: 0), AutoMoveType(time: 0x63, move: 2), AutoMoveType(time: 0x64,
      move: 0), AutoMoveType(time: 0x73, move: 5), AutoMoveType(time: 0x80,
      move: 6), AutoMoveType(time: 0x88, move: 3), AutoMoveType(time: 0x9D,
      move: 7), AutoMoveType(time: 0x9E, move: 0), AutoMoveType(time: 0x9F,
      move: 1), AutoMoveType(time: 0xAB, move: 4), AutoMoveType(time: 0xB1,
      move: 0), AutoMoveType(time: 0xB2, move: 1), AutoMoveType(time: 0xBC,
      move: 0), AutoMoveType(time: 0xC1, move: 1), AutoMoveType(time: 0xCD,
      move: 0), AutoMoveType(time: 0xE9, move: -1)]
customDefaults.shadDrinkMove = [AutoMoveType(time: 0x00, move: 0), AutoMoveType(time: 0x01,
      move: 1), AutoMoveType(time: 0x0E, move: 0), AutoMoveType(time: 0x12,
      move: 6), AutoMoveType(time: 0x1D, move: 7), AutoMoveType(time: 0x2D,
      move: 2), AutoMoveType(time: 0x31, move: 1), AutoMoveType(time: 0xFF, move: -2)]
customDefaults.baseSpeed = 5'u8
customDefaults.fightSpeed = 6'u8
customDefaults.chomperSpeed = 15'u8

var
  custom*: ptr CustomOptionsType = addr(customDefaults)

  fullImage*: array[ord(MAX_FULLIMAGES), FullImageType]

fullImage[ord(TITLE_MAIN)] = FullImageType(id: 0, chtab: addr(chtabTitle50),
    blitter: blitters0NoTransp, xpos: 0, ypos: 0)
fullImage[ord(TITLE_PRESENTS)] = FullImageType(id: 1, chtab: addr(chtabTitle50),
    blitter: blitters0NoTransp, xpos: 96, ypos: 106)
fullImage[ord(TITLE_GAME)] = FullImageType(id: 2, chtab: addr(chtabTitle50),
    blitter: blitters0NoTransp, xpos: 96, ypos: 122)
fullImage[ord(TITLE_POP)] = FullImageType(id: 3, chtab: addr(chtabTitle50),
    blitter: blitters10hTransp, xpos: 24, ypos: 107)
fullImage[ord(TITLE_MECHNER)] = FullImageType(id: 4, chtab: addr(chtabTitle50),
    blitter: blitters0NoTransp, xpos: 48, ypos: 184)
fullImage[ord(HOF_POP)] = FullImageType(id: 3, chtab: addr(chtabTitle50),
    blitter: blitters10hTransp, xpos: 24, ypos: 24)
fullImage[ord(STORY_FRAME)] = FullImageType(id: 0, chtab: addr(chtabTitle40),
    blitter: blitters0NoTransp, xpos: 0, ypos: 0)
fullImage[ord(STORY_ABSENCE)] = FullImageType(id: 1, chtab: addr(chtabTitle40),
    blitter: blittersWhite, xpos: 24, ypos: 25)
fullImage[ord(STORY_MARRY)] = FullImageType(id: 2, chtab: addr(chtabTitle40),
    blitter: blittersWhite, xpos: 24, ypos: 25)
fullImage[ord(STORY_HAIL)] = FullImageType(id: 3, chtab: addr(chtabTitle40),
    blitter: blittersWhite, xpos: 24, ypos: 25)
fullImage[ord(STORY_CREDITS)] = FullImageType(id: 4, chtab: addr(chtabTitle40),
    blitter: blittersWhite, xpos: 24, ypos: 26)

var
  # data:009C
  cheatsEnabled*: word = 0
when(USE_DEBUG_CHEATS):
  var
    debugCheatsEnabled*: byte = 0
    isTimerDisplayed*: byte = 0
    isFeatherTimerDisplayed*: byte = 0

when(USE_MENU):
  var
    hcSmallFont* = new(FontType)
    haveMouseInput*: bool
    haveKeyboardOrControllerInput*: bool
    mouseX*, mouseY*: cint
    mouseMoved*: bool
    mouseClicked*: bool
    mouseButtonClickedRight*: bool
    pressedEnter*: bool
    escapeKeySuppressed*: bool
    menuControlScrollY*: int32
    isMenuShown*: sbyte
    enablePauseMenu*: byte = 1

  hcSmallFont.firstChar = 32
  hcSmallFont.lastChar = 126
  hcSmallFont.heightAboveBaseline = 5
  hcSmallFont.heightBelowBaseline = 2
  hcSmallFont.spaceBetweenLines = 1
  hcSmallFont.spaceBetweenChars = 1
  hcSmallFont.chtab = nil
var
  # modsFolder[POPMAXPATH] INIT(= "mods"): char
  modsFolder* = "mods"

  playDemoLevel: bool = false

when(USE_REPLAY):
  var
    gDeprecationNumber: int32

when (USE_FAST_FORWARD):
  var
    audioSpeed: int32 = 1 # =1 normally, >1 during fast forwarding
