from strutils import toHex

const
  SEQTBL_BASE = 0x196E

# Labels
const
  running = SEQTBL_BASE               # 0x196E
  startrun = 5 + running              #SEQTBL_BASE + 5     # 0x1973
  runstt1 = 2 + startrun              #SEQTBL_BASE + 7     # 0x1975
  runstt4 = 3 + runstt1               #SEQTBL_BASE + 10    # 0x1978
  runcyc1 = 9 + runstt4               #SEQTBL_BASE + 19    # 0x1981
  runcyc7 = 20 + runcyc1              #SEQTBL_BASE + 39    # 0x1995
  stand = 11 + runcyc7                #SEQTBL_BASE + 50    # 0x19A0
  goalertstand = 6 + stand            #SEQTBL_BASE + 56    # 0x19A6
  alertstand = 2 + goalertstand       #SEQTBL_BASE + 58    # 0x19A8
  arise = 4 + alertstand              #SEQTBL_BASE + 62    # 0x19AC
  guardengarde = 21 + arise           #SEQTBL_BASE + 83    # 0x19C1
  engarde = 3 + guardengarde          #SEQTBL_BASE + 86    # 0x19C4
  ready = 14 + engarde                #SEQTBL_BASE + 100   # 0x19D2
  readyLoop = 6 + ready               #SEQTBL_BASE + 106   # 0x19D8
  stabbed = 4 + readyLoop             #SEQTBL_BASE + 110   # 0x19DC
  strikeadv = 29 + stabbed            #SEQTBL_BASE + 139   # 0x19F9
  strikeret = 14 + strikeadv          #SEQTBL_BASE + 153   # 0x1A07
  advance = 12 + strikeret            #SEQTBL_BASE + 165   # 0x1A13
  fastadvance = 15 + advance          #SEQTBL_BASE + 180   # 0x1A22
  retreat = 12 + fastadvance          #SEQTBL_BASE + 192   # 0x1A2E
  strike = 14 + retreat               #SEQTBL_BASE + 206   # 0x1A3C
  faststrike = 6 + strike             #SEQTBL_BASE + 212   # 0x1A42
  guy4 = 3 + faststrike               #SEQTBL_BASE + 215   # 0x1A45
  guy7 = 5 + guy4                     #SEQTBL_BASE + 220   # 0x1A4A
  guy8 = 3 + guy7                     #SEQTBL_BASE + 223   # 0x1A4D
  blockedstrike = 7 + guy8            #SEQTBL_BASE + 230   # 0x1A54
  blocktostrike = 6 + blockedstrike   #SEQTBL_BASE + 236   # 0x1A5A
  readyblock = 4 + blocktostrike      #SEQTBL_BASE + 240   # 0x1A5E
  blocking = 1 + readyblock           #SEQTBL_BASE + 241   # 0x1A5F
  striketoblock = 4 + blocking        #SEQTBL_BASE + 245   # 0x1A63
  landengarde = 5 + striketoblock     #SEQTBL_BASE + 250   # 0x1A68
  bumpengfwd = 6 + landengarde        #SEQTBL_BASE + 256   # 0x1A6E
  bumpengback = 7 + bumpengfwd        #SEQTBL_BASE + 263   # 0x1A75
  flee = 7 + bumpengback              #SEQTBL_BASE + 270   # 0x1A7C
  turnengarde = 7 + flee              #SEQTBL_BASE + 277   # 0x1A83
  alertturn = 8 + turnengarde         #SEQTBL_BASE + 285   # 0x1A8B
  standjump = 8 + alertturn           #SEQTBL_BASE + 293   # 0x1A93
  sjland = 29 + standjump             #SEQTBL_BASE + 322   # 0x1AB0
  runjump2 = 29 + sjland              #SEQTBL_BASE + 351   # 0x1ACD
  rjlandrun = 46 + runjump2           #SEQTBL_BASE + 397   # 0x1AFB
  rdiveroll = 9 + rjlandrun           #SEQTBL_BASE + 406   # 0x1B04
  rdiverollCrouch = 18 + rdiveroll    #SEQTBL_BASE + 424   # 0x1B16
  sdiveroll = 4 + rdiverollCrouch     #SEQTBL_BASE + 428   # 0x1B1A
  crawl = 1 + sdiveroll               #SEQTBL_BASE + 429   # 0x1B1B
  crawlCrouch = 14 + crawl            #SEQTBL_BASE + 443   # 0x1B29
  turndraw = 4 + crawlCrouch          #SEQTBL_BASE + 447   # 0x1B2D
  turn = 12 + turndraw                #SEQTBL_BASE + 459   # 0x1B39
  turnrun = 26 + turn                 #SEQTBL_BASE + 485   # 0x1B53
  runturn = 7 + turnrun               #SEQTBL_BASE + 492   # 0x1B5A
  fightfall = 43 + runturn            #SEQTBL_BASE + 535   # 0x1B85
  efightfall = 28 + fightfall         #SEQTBL_BASE + 563   # 0x1BA1
  efightfallfwd = 30 + efightfall     #SEQTBL_BASE + 593   # 0x1BBF
  stepfall = 28 + efightfallfwd       #SEQTBL_BASE + 621   # 0x1BDB
  fall1 = 9 + stepfall                #SEQTBL_BASE + 630   # 0x1BE4
  patchfall = 22 + fall1              #SEQTBL_BASE + 652   # 0x1BFA
  stepfall2 = 7 + patchfall           #SEQTBL_BASE + 659   # 0x1C01
  stepfloat = 5 + stepfall2           #SEQTBL_BASE + 664   # 0x1C06
  jumpfall = 22 + stepfloat           #SEQTBL_BASE + 686   # 0x1C1C
  rjumpfall = 28 + jumpfall           #SEQTBL_BASE + 714   # 0x1C38
  jumphangMed = 28 + rjumpfall        #SEQTBL_BASE + 742   # 0x1C54
  jumphangLong = 21 + jumphangMed     #SEQTBL_BASE + 763   # 0x1C69
  jumpbackhang = 27 + jumphangLong    #SEQTBL_BASE + 790   # 0x1C84
  hang = 29 + jumpbackhang            #SEQTBL_BASE + 819   # 0x1CA1
  hang1 = 3 + hang                    #SEQTBL_BASE + 822   # 0x1CA4
  hangstraight = 45 + hang1           #SEQTBL_BASE + 867   # 0x1CD1
  hangstraightLoop = 7 + hangstraight #SEQTBL_BASE + 874   # 0x1CD8
  climbfail = 4 + hangstraightLoop    #SEQTBL_BASE + 878   # 0x1CDC
  climbdown = 16 + climbfail          #SEQTBL_BASE + 894   # 0x1CEC
  climbup = 24 + climbdown            #SEQTBL_BASE + 918   # 0x1D04
  hangdrop = 33 + climbup             #SEQTBL_BASE + 951   # 0x1D25
  hangfall2 = 17 + hangdrop           #SEQTBL_BASE + 968   # 0x1D36
  freefall = 19 + hangfall2           #SEQTBL_BASE + 987   # 0x1D49
  freefallLoop = 2 + freefall         #SEQTBL_BASE + 989   # 0x1D4B
  runstop = 4 + freefallLoop          #SEQTBL_BASE + 993   # 0x1D4F
  jumpup2 = 25 + runstop              #SEQTBL_BASE + 1018  # 0x1D68
  highjump = 21 + jumpup2             #SEQTBL_BASE + 1039  # 0x1D7D
  superhijump = 30 + highjump         #SEQTBL_BASE + 1069  # 0x1D9B
  fallhang = 91 + superhijump         #SEQTBL_BASE + 1160  # 0x1DF6
  bump = 6 + fallhang                 #SEQTBL_BASE + 1166  # 0x1DFC
  bumpfall = 10 + bump                #SEQTBL_BASE + 1176  # 0x1E06
  bumpfloat = 31 + bumpfall           #SEQTBL_BASE + 1207  # 0x1E25
  hardbump = 22 + bumpfloat           #SEQTBL_BASE + 1229  # 0x1E3B
  testfoot = 30 + hardbump            #SEQTBL_BASE + 1259  # 0x1E59
  stepback = 31 + testfoot            #SEQTBL_BASE + 1290  # 0x1E78
  step14 = 5 + stepback               #SEQTBL_BASE + 1295  # 0x1E7D
  step13 = 31 + step14                #SEQTBL_BASE + 1326  # 0x1E9C
  step12 = 31 + step13                #SEQTBL_BASE + 1357  # 0x1EBB
  step11 = 31 + step12                #SEQTBL_BASE + 1388  # 0x1EDA
  step10 = 29 + step11                #SEQTBL_BASE + 1417  # 0x1EF7
  step10a = 5 + step10                #SEQTBL_BASE + 1422  # 0x1EFC
  step9 = 23 + step10a                #SEQTBL_BASE + 1445  # 0x1F13
  step8 = 6 + step9                   #SEQTBL_BASE + 1451  # 0x1F19
  step7 = 26 + step8                  #SEQTBL_BASE + 1477  # 0x1F33
  step6 = 21 + step7                  #SEQTBL_BASE + 1498  # 0x1F48
  step5 = 21 + step6                  #SEQTBL_BASE + 1519  # 0x1F5D
  step4 = 21 + step5                  #SEQTBL_BASE + 1540  # 0x1F72
  step3 = 16 + step4                  #SEQTBL_BASE + 1556  # 0x1F82
  step2 = 16 + step3                  #SEQTBL_BASE + 1572  # 0x1F92
  step1 = 12 + step2                  #SEQTBL_BASE + 1584  # 0x1F9E
  stoop = 9 + step1                   #SEQTBL_BASE + 1593  # 0x1FA7
  stoopCrouch = 8 + stoop             #SEQTBL_BASE + 1601  # 0x1FAF
  standup = 4 + stoopCrouch           #SEQTBL_BASE + 1605  # 0x1FB3
  pickupsword = 23 + standup          #SEQTBL_BASE + 1628  # 0x1FCA
  resheathe = 16 + pickupsword        #SEQTBL_BASE + 1644  # 0x1FDA
  fastsheathe = 33 + resheathe        #SEQTBL_BASE + 1677  # 0x1FFB
  drinkpotion = 14 + fastsheathe      #SEQTBL_BASE + 1691  # 0x2009
  softland = 34 + drinkpotion         #SEQTBL_BASE + 1725  # 0x202B
  softlandCrouch = 11 + softland      #SEQTBL_BASE + 1736  # 0x2036
  landrun = 4 + softlandCrouch        #SEQTBL_BASE + 1740  # 0x203A
  medland = 32 + landrun              #SEQTBL_BASE + 1772  # 0x205A
  hardland = 66 + medland             #SEQTBL_BASE + 1838  # 0x209C
  hardlandDead = 9 + hardland         #SEQTBL_BASE + 1847  # 0x20A5
  stabkill = 4 + hardlandDead         #SEQTBL_BASE + 1851  # 0x20A9
  dropdead = 5 + stabkill             #SEQTBL_BASE + 1856  # 0x20AE
  dropdeadDead = 12 + dropdead        #SEQTBL_BASE + 1868  # 0x20BA
  impale = 4 + dropdeadDead           #SEQTBL_BASE + 1872  # 0x20BE
  impaleDead = 7 + impale             #SEQTBL_BASE + 1879  # 0x20C5
  halve = 4 + impaleDead              #SEQTBL_BASE + 1883  # 0x20C9
  halveDead = 4 + halve               #SEQTBL_BASE + 1887  # 0x20CD
  crush = 4 + halveDead               #SEQTBL_BASE + 1891  # 0x20D1
  deadfall = 3 + crush                #SEQTBL_BASE + 1894  # 0x20D4
  deadfallLoop = 5 + deadfall         #SEQTBL_BASE + 1899  # 0x20D9
  climbstairs = 4 + deadfallLoop      #SEQTBL_BASE + 1903  # 0x20DD
  climbstairsLoop = 81 + climbstairs  #SEQTBL_BASE + 1984  # 0x212E
  Vstand = 4 + climbstairsLoop        #SEQTBL_BASE + 1988  # 0x2132
  Vraise = 4 + Vstand                 #SEQTBL_BASE + 1992  # 0x2136
  VraiseLoop = 21 + Vraise            #SEQTBL_BASE + 2013  # 0x214B
  Vwalk = 4 + VraiseLoop              #SEQTBL_BASE + 2017  # 0x214F
  Vwalk1 = 2 + Vwalk                  #SEQTBL_BASE + 2019  # 0x2151
  Vwalk2 = 3 + Vwalk1                 #SEQTBL_BASE + 2022  # 0x2154
  Vstop = 18 + Vwalk2                 #SEQTBL_BASE + 2040  # 0x2166
  Vexit = 7 + Vstop                   #SEQTBL_BASE + 2047  # 0x216D
  Pstand = 40 + Vexit                 #SEQTBL_BASE + 2087  # 0x2195
  Palert = 4 + Pstand                 #SEQTBL_BASE + 2091  # 0x2199
  Pstepback = 15 + Palert             #SEQTBL_BASE + 2106  # 0x21A8
  PstepbackLoop = 16 + Pstepback      #SEQTBL_BASE + 2122  # 0x21B8
  Plie = 4 + PstepbackLoop            #SEQTBL_BASE + 2126  # 0x21BC
  Pwaiting = 4 + Plie                 #SEQTBL_BASE + 2130  # 0x21C0
  Pembrace = 4 + Pwaiting             #SEQTBL_BASE + 2134  # 0x21C4
  PembraceLoop = 30 + Pembrace        #SEQTBL_BASE + 2164  # 0x21E2
  Pstroke = 4 + PembraceLoop          #SEQTBL_BASE + 2168  # 0x21E6
  Prise = 4 + Pstroke                 #SEQTBL_BASE + 2172  # 0x21EA
  PriseLoop = 14 + Prise              #SEQTBL_BASE + 2186  # 0x21F8
  Pcrouch = 4 + PriseLoop             #SEQTBL_BASE + 2190  # 0x21FC
  PcrouchLoop = 64 + Pcrouch          #SEQTBL_BASE + 2254  # 0x223C
  Pslump = 4 + PcrouchLoop            #SEQTBL_BASE + 2258  # 0x2240
  PslumpLoop = 1 + Pslump             #SEQTBL_BASE + 2259  # 0x2241
  Mscurry = 4 + PslumpLoop            #SEQTBL_BASE + 2263  # 0x2245
  Mscurry1 = 2 + Mscurry              #SEQTBL_BASE + 2265  # 0x2247
  Mstop = 12 + Mscurry1               #SEQTBL_BASE + 2277  # 0x2253
  Mraise = 4 + Mstop                  #SEQTBL_BASE + 2281  # 0x2257
  Mleave = 4 + Mraise                 #SEQTBL_BASE + 2285  # 0x225B
  Mclimb = 19 + Mleave                #SEQTBL_BASE + 2304  # 0x226E
  MclimbLoop = 2 + Mclimb             #SEQTBL_BASE + 2306  # 0x2270

const
  seqtblOffsets: array[115, word] = [
        word(0x0000), word(startrun), word(stand), word(standjump),
        word(runjump2), word(turn), word(runturn), word(stepfall),
        word(jumphangMed), word(hang), word(climbup), word(hangdrop),
        word(freefall), word(runstop), word(jumpup2), word(fallhang),
        word(jumpbackhang), word(softland), word(jumpfall), word(stepfall2),
        word(medland), word(rjumpfall), word(hardland), word(hangfall2),
        word(jumphangLong), word(hangstraight), word(rdiveroll), word(
            sdiveroll),
        word(highjump), word(step1), word(step2), word(step3),
        word(step4), word(step5), word(step6), word(step7),
        word(step8), word(step9), word(step10), word(step11),
        word(step12), word(step13), word(step14), word(turnrun),
        word(testfoot), word(bumpfall), word(hardbump), word(bump),
        word(superhijump), word(standup), word(stoop), word(impale),
        word(crush), word(deadfall), word(halve), word(engarde),
        word(advance), word(retreat), word(strike), word(flee),
        word(turnengarde), word(striketoblock), word(readyblock), word(
            landengarde),
        word(bumpengfwd), word(bumpengback), word(blocktostrike), word(
            strikeadv),
        word(climbdown), word(blockedstrike), word(climbstairs), word(dropdead),
        word(stepback), word(climbfail), word(stabbed), word(faststrike),
        word(strikeret), word(alertstand), word(drinkpotion), word(crawl),
        word(alertturn), word(fightfall), word(efightfall), word(efightfallfwd),
        word(running), word(stabkill), word(fastadvance), word(goalertstand),
        word(arise), word(turndraw), word(guardengarde), word(pickupsword),
        word(resheathe), word(fastsheathe), word(Pstand), word(Vstand),
        word(Vwalk), word(Vstop), word(Palert), word(Pstepback),
        word(Vexit), word(Mclimb), word(Vraise), word(Plie),
        word(patchfall), word(Mscurry), word(Mstop), word(Mleave),
        word(Pembrace), word(Pwaiting), word(Pstroke), word(Prise),
        word(Pcrouch), word(Pslump), word(Mraise)
    ]

var
  # data:196E
  seqtbl: array[2310, byte] = [

      # running
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_JMP),
        runcyc1 and 0x00FF, byte((runcyc1 and 0xFF00) shr 8), # goto running: frame 7

      # startrun
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame1StartRun)),
    byte(frame2StartRun), byte(frame3StartRun), byte(ord(
        frame4StartRun)),
    byte(SEQ_DX), 8'u8, byte(frame5StartRun),
    byte(SEQ_DX), 3'u8, byte(frame6StartRun),
    byte(SEQ_DX), 3'u8, byte(frame7Run),
    byte(SEQ_DX), 5'u8, byte(frame8Run),
    byte(SEQ_DX), 1'u8, byte(SEQ_SOUND), byte(SND_FOOTSTEP),
        byte(frame9Run),
    byte(SEQ_DX), 2'u8, byte(frame10Run),
    byte(SEQ_DX), 4'u8, byte(frame11Run),
    byte(SEQ_DX), 5'u8, byte(frame12Run),
    byte(SEQ_DX), 2'u8, byte(SEQ_SOUND), byte(SND_FOOTSTEP),
        byte(frame13Run),
    byte(SEQ_DX), 3'u8, byte(frame14Run),
    byte(SEQ_DX), 4'u8, byte(SEQ_JMP), runcyc1 and 0x00FF, byte( (
        runcyc1 and 0xFF00) shr 8),

    # stand
    byte(SEQ_ACTION), byte(actions0Stand), byte(frame15Stand),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # alert stand
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame166StandInactive)),
    byte(SEQ_JMP), alertstand and 0x00FF, byte( (alertstand and
        0xFF00) shr 8),                                # goto "alertstand"

      # arise (skeleton)
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_DX), 10'u8,
        ord(frame177Spiked),
    byte(frame177Spiked),
    byte(SEQ_DX), byte(256-7), byte(SEQ_DY), byte(256-2), byte(ord(
        frame178Chomped)),
    byte(SEQ_DX), 5'u8, byte(SEQ_DY), 2'u8, byte(ord(
        frame166StandInactive)),
    byte(SEQ_DX), byte(256-1), byte(SEQ_JMP), ready and 0x00FF, byte((
        ready and 0xFF00) shr 8),                      # goto "ready"

      # guard engarde
    byte(SEQ_JMP), ready and 0x00FF, byte( (ready and 0xFF00) shr 8), # goto "ready"

      # engarde
    byte(SEQ_ACTION), byte(actions1RunJump),
    byte(SEQ_DX), 2'u8, byte(frame207Draw1),
    byte(frame208Draw2),
    byte(SEQ_DX), 2'u8, byte(frame209Draw3),
    byte(SEQ_DX), 2'u8, byte(frame210Draw4),
    byte(SEQ_DX), 3'u8, byte(SEQ_ACTION), byte(actions1RunJump),
        byte(SEQ_SOUND), byte(SND_SILENT), byte(ord(
            frame158StandWithSword)),
    byte(frame170StandWithSword),
    byte(frame171StandWithSword),
    byte(SEQ_JMP), readyLoop and 0x00FF, byte( (readyLoop and
        0xFF00) shr 8),                                # goto ":loop"

      # stabbed
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_SET_FALL),
        byte(256-1), 0'u8, byte(frame172Jumpfall2),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), 1'u8, byte(ord(
        frame173Jumpfall3)),
    byte(SEQ_DX), byte(256-1), byte(frame174Jumpfall4),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), 2'u8,  # frame 175 is commented out in the Apple II source
    byte(SEQ_DX), byte(256-2), byte(SEQ_DY), 1'u8,
    byte(SEQ_DX), byte(256-5), byte(SEQ_DY), byte(256-4),
    byte(SEQ_JMP), guy8 and 0x00FF, byte( (guy8 and 0xFF00) shr 8), # goto "guy8"

      # strike - advance
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_SET_FALL),
        1'u8, 0'u8, ord(frame155Guy7),
    byte(SEQ_DX), 2'u8, byte(frame165WalkWithSword),
    byte(SEQ_DX), byte(256-2), byte(SEQ_JMP), ready and 0x00FF, byte((
        ready and 0xFF00) shr 8),                      # goto "ready"

      # strike - retreat
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_SET_FALL),
        byte(256-1), 0'u8, byte(frame155Guy7),
    byte(frame156Guy8),
    byte(frame157WalkWithSword),
    byte(frame158StandWithSword),
    byte(SEQ_JMP), retreat and 0x00FF, byte( (retreat and 0xFF00) shr 8), # goto "retreat"

      # advance
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_SET_FALL),
        1'u8, 0'u8,
    byte(SEQ_DX), 2'u8, byte(frame163Fighting),
    byte(SEQ_DX), 4'u8, byte(frame164Fighting),
    byte(frame165WalkWithSword),
    byte(SEQ_JMP), ready and 0x00FF, byte( (ready and 0xFF00) shr 8), # goto "ready"

      # fast advance
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_SET_FALL),
        1'u8, 0'u8, ord(SEQ_DX), 6'u8, byte(frame164Fighting),
    byte(frame165WalkWithSword),
    byte(SEQ_JMP), ready and 0x00FF, byte( (ready and 0xFF00) shr 8), # goto "ready"

      # retreat
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_SET_FALL),
        byte(256-1), 0'u8, byte(SEQ_DX), byte(256-3), byte(ord(
            frame160Fighting)),
    byte(SEQ_DX), byte(256-2), byte(frame157WalkWithSword),
    byte(SEQ_JMP), ready and 0x00FF, byte( (ready and 0xFF00) shr 8), # goto "ready"

      # strike
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_SET_FALL),
        byte(256-1), 0'u8, byte(frame168Back),
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame151Strike1)),
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame152Strike2)),
    byte(frame153Strike3),
    byte(frame154Poking),
    byte(SEQ_ACTION), byte(actions5Bumped), byte(frame155Guy7),
    byte(SEQ_ACTION), byte(actions1RunJump), byte(frame156Guy8),
    byte(frame157WalkWithSword),
    byte(SEQ_JMP), ready and 0x00FF, byte( (ready and 0xFF00) shr 8), # goto "ready"

      # blocked strike
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame167Blocked)),
    byte(SEQ_JMP), guy7 and 0x00FF, byte( (guy7 and 0xFF00) shr 8), # goto "guy7"

      # block to strike
      # "blocktostrike"
    byte(frame162BlockToStrike),
    byte(SEQ_JMP), guy4 and 0x00FF, byte( (guy4 and 0xFF00) shr 8), # goto "guy4"

      # ready block
    byte(frame169BeginBlock),
    byte(frame150Parry),
    byte(SEQ_JMP), ready and 0x00FF, byte( (ready and 0xFF00) shr 8), # goto "ready"

      # strike to block
    byte(frame159Fighting),
    byte(frame160Fighting),
    byte(SEQ_JMP), blocking and 0x00FF, byte( (blocking and 0xFF00) shr 8), # goto "blocking"

      # land en garde
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        SEQ_KNOCK_DOWN)), byte(SEQ_JMP), ready and 0x00FF, byte( (ready and
            0xFF00) shr 8),                            # goto "ready"

      # bump en garde (forward)
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_DX), byte(
        256-8), ord(SEQ_JMP), ready and 0x00FF, byte( (ready and 0xFF00) shr 8), # goto "ready"

      # bump en garde (back)
    byte(SEQ_ACTION), byte(actions5Bumped), byte(ord(
        frame160Fighting)),
    byte(frame157WalkWithSword),
    byte(SEQ_JMP), ready and 0x00FF, byte( (ready and 0xFF00) shr 8), # goto "ready"

      # flee
    byte(SEQ_ACTION), byte(actions7Turn), byte(SEQ_DX), byte(
        256-8), byte(SEQ_JMP), turn and 0x00FF, byte( (turn and
            0xFF00) shr 8),                            # goto "turn"

      # turn en garde
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_FLIP), byte(
        ord(SEQ_DX)), 5'u8, ord(SEQ_JMP), retreat and 0x00FF, byte((retreat and
            0xFF00) shr 8),                            # goto "retreat"

      # alert turn (for enemies)
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_FLIP), byte(
        ord(SEQ_DX)), 18'u8, byte(SEQ_JMP), goalertstand and 0x00FF, byte((
            goalertstand and 0xFF00) shr 8),           # goto "goalertstand"

      # standing jump
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame16StandingJump1)),
    byte(frame17StandingJump2),
    byte(SEQ_DX), 2'u8, byte(frame18StandingJump3),
    byte(SEQ_DX), 2'u8, byte(frame19StandingJump4),
    byte(SEQ_DX), 2'u8, byte(frame20StandingJump5),
    byte(SEQ_DX), 2'u8, byte(frame21StandingJump6),
    byte(SEQ_DX), 2'u8, byte(frame22StandingJump7),
    byte(SEQ_DX), 7'u8, byte(frame23StandingJump8),
    byte(SEQ_DX), 9'u8, byte(frame24StandingJump9),
    byte(SEQ_DX), 5'u8, byte(SEQ_DY), byte(256-6), byte(ord(
        frame25StandingJump10)),
    byte(SEQ_DX), 1'u8, byte(SEQ_DY), 6'u8, byte(ord(
        frame26StandingJump11)),
    byte(SEQ_DX), 4'u8, byte(SEQ_KNOCK_DOWN), byte(SEQ_SOUND),
        byte(SND_FOOTSTEP), ord(frame27StandingJump12),
    byte(SEQ_DX), byte(256-3), byte(frame28StandingJump13),
    byte(SEQ_DX), 5'u8, byte(frame29StandingJump14),
    byte(SEQ_SOUND), byte(SND_FOOTSTEP), byte(ord(
        frame30StandingJump15)),
    byte(frame31StandingJump16),
    byte(frame32StandingJump17),
    byte(frame33StandingJump18),
    byte(SEQ_DX), 1'u8, byte(SEQ_JMP), stand and 0x00FF, byte((
        stand and 0xFF00) shr 8),                      # goto "stand"

      # running jump
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_SOUND),
        byte(SND_FOOTSTEP), byte(frame34StartRunJump1),
    byte(SEQ_DX), 5'u8, byte(frame35StartRunJump2),
    byte(SEQ_DX), 6'u8, byte(frame36StartRunJump3),
    byte(SEQ_DX), 3'u8, byte(frame37StartRunJump4),
    byte(SEQ_DX), 5'u8, byte(SEQ_SOUND), byte(SND_FOOTSTEP), ord(
        frame38StartRunJump5),
    byte(SEQ_DX), 7'u8, byte(frame39StartRunJump6),
    byte(SEQ_DX), 12'u8, byte(SEQ_DY), byte(256-3), byte(ord(
        frame40RunningJump1)),
    byte(SEQ_DX), 8'u8, byte(SEQ_DY), byte(256-9), byte(ord(
        frame41RunningJump2)),
    byte(SEQ_DX), 8'u8, byte(SEQ_DY), byte(256-2), byte(ord(
        frame42RunningJump3)),
    byte(SEQ_DX), 4'u8, byte(SEQ_DY), 11'u8, byte(ord(
        frame43RunningJump4)),
    byte(SEQ_DX), 4'u8, byte(SEQ_DY), 3'u8, byte(ord(
        frame44RunningJump5)),
    byte(SEQ_DX), 5'u8, byte(SEQ_KNOCK_DOWN), byte(SEQ_SOUND),
        byte(SND_FOOTSTEP), ord(SEQ_JMP), runcyc1 and 0x00FF, byte((
            runcyc1 and 0xFF00) shr 8),                # goto "runcyc1"

      # run dive roll
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_DX), 1'u8,
        ord(frame107FallLand1),
    byte(SEQ_DX), 2'u8, byte(SEQ_DX), 2'u8, byte(ord(
        frame108FallLand2)),
    byte(SEQ_DX), 2'u8, byte(frame109Crouch),
    byte(SEQ_DX), 2'u8, byte(frame109Crouch),
    byte(SEQ_DX), 2'u8, byte(frame109Crouch),
    byte(SEQ_JMP), rdiverollCrouch and 0x00FF, byte((rdiverollCrouch and
        0xFF00) shr 8),                                # goto ":crouch"

    0x00,                                              # stand dive roll; not implemented

                                                       # crawl
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_DX), 1'u8,
        ord(frame110StandUpFromCrouch1),
    byte(frame111StandUpFromCrouch2),
    byte(SEQ_DX), 2'u8, byte(frame112StandUpFromCrouch3),
    byte(SEQ_DX), 2'u8, byte(frame108FallLand2),
    byte(SEQ_DX), 2'u8, byte(frame109Crouch),
    byte(SEQ_JMP), crawlCrouch and 0x00FF, byte( (crawlCrouch and
        0xFF00) shr 8),                                # goto ":crouch"

      # turn draw
    byte(SEQ_ACTION), byte(actions7Turn), byte(SEQ_FLIP), byte(
        ord(SEQ_DX)), 6'u8, ord(frame45Turn),
    byte(SEQ_DX), 1'u8, byte(frame46Turn),
    byte(SEQ_JMP), engarde and 0x00FF, byte( (engarde and 0xFF00) shr 8), # goto "engarde"

      # turn
    byte(SEQ_ACTION), byte(actions7Turn), byte(SEQ_FLIP), byte(
        ord(SEQ_DX)), 6'u8, ord(frame45Turn),
    byte(SEQ_DX), 1'u8, byte(frame46Turn),
    byte(SEQ_DX), 2'u8, byte(frame47Turn),
    byte(SEQ_DX), byte(256-1), byte(frame48Turn),
    byte(SEQ_DX), 1'u8, byte(frame49Turn),
    byte(SEQ_DX), byte(256-2), byte(frame50Turn),
    byte(frame51Turn),
    byte(frame52Turn),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # turnrun (from frame 48)
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_DX), byte(
        256-1), ord(SEQ_JMP), runstt1 and 0x00FF, byte( (runstt1 and
            0xFF00) shr 8),                            # goto "runstt1"

      # runturn
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_DX), 1'u8,
        ord(frame53Runturn),
    byte(SEQ_DX), 1'u8, byte(SEQ_SOUND), byte(SND_FOOTSTEP),
        byte(frame54Runturn),
    byte(SEQ_DX), 8'u8, byte(frame55Runturn),
    byte(SEQ_SOUND), byte(SND_FOOTSTEP), byte(frame56Runturn),
    byte(SEQ_DX), 7'u8, byte(frame57Runturn),
    byte(SEQ_DX), 3'u8, byte(frame58Runturn),
    byte(SEQ_DX), 1'u8, byte(frame59Runturn),
    byte(frame60Runturn),
    byte(SEQ_DX), 2'u8, byte(frame61Runturn),
    byte(SEQ_DX), byte(256-1), byte(frame62Runturn),
    byte(frame63Runturn),
    byte(frame64Runturn),
    byte(SEQ_DX), byte(256-1), byte(frame65Runturn),
    byte(SEQ_DX), byte(256-14), byte(SEQ_FLIP), byte(SEQ_JMP),
        runcyc7 and 0x00FF, byte( (runcyc7 and 0xFF00) shr 8), # goto "runcyc7"

      # fightfall (backward)
    byte(SEQ_ACTION), byte(actions3InMidair), byte(SEQ_DY), byte(
        256-1), ord(frame102StartFall1),
    byte(SEQ_DX), byte(256-2), byte(SEQ_DY), 6'u8, byte(ord(
        frame103StartFall2)),
    byte(SEQ_DX), byte(256-2), byte(SEQ_DY), 9'u8, byte(ord(
        frame104StartFall3)),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), 12'u8, byte(ord(
        frame105StartFall4)),
    byte(SEQ_DX), byte(256-3), byte(SEQ_SET_FALL), 0'u8, 15'u8, byte(
        ord(SEQ_JMP)), freefall and 0x00FF, byte( (freefall and 0xFF00) shr 8), # goto "freefall"

      # enemy fight fall
    byte(SEQ_ACTION), byte(actions3InMidair), byte(SEQ_DY), byte(
        256-1), ord(SEQ_DX), byte(256-2), byte(frame102StartFall1),
    byte(SEQ_DX), byte(256-3), byte(SEQ_DY), 6'u8, byte(ord(
        frame103StartFall2)),
    byte(SEQ_DX), byte(256-3), byte(SEQ_DY), 9'u8, byte(ord(
        frame104StartFall3)),
    byte(SEQ_DX), byte(256-2), byte(SEQ_DY), 12'u8, byte(ord(
        frame105StartFall4)),
    byte(SEQ_DX), byte(256-3), byte(SEQ_SET_FALL), 0'u8, 15'u8, byte(
        ord(SEQ_JMP)), freefall and 0x00FF, byte( (freefall and 0xFF00) shr 8), # goto "freefall"

      # enemy fight fall forward
    byte(SEQ_ACTION), byte(actions3InMidair), byte(SEQ_DX), 1'u8,
        byte(SEQ_DY), byte(256-1), byte(frame102StartFall1),
    byte(SEQ_DX), 2'u8, byte(SEQ_DY), 6'u8, byte(ord(
        frame103StartFall2)),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), 9'u8, byte(ord(
        frame104StartFall3)),
    byte(SEQ_DY), 12'u8, byte(frame105StartFall4),
    byte(SEQ_DX), byte(256-2), byte(SEQ_SET_FALL), 1'u8, 15'u8, byte(
        ord(SEQ_JMP)), freefall and 0x00FF, byte( (freefall and 0xFF00) shr 8), # goto "freefall"

      # stepfall
    byte(SEQ_ACTION), byte(actions3InMidair), byte(SEQ_DX), 1'u8,
        byte(SEQ_DY), 3'u8, byte(SEQ_JMP_IF_FEATHER), stepfloat and
            0x00FF, byte((stepfloat and 0xFF00) shr 8), # goto "stepfloat"
    byte(frame102StartFall1),
    byte(SEQ_DX), 2'u8, byte(SEQ_DY), 6'u8, byte(ord(
        frame103StartFall2)),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), 9'u8, byte(ord(
        frame104StartFall3)),
    byte(SEQ_DY), 12'u8, byte(frame105StartFall4),
    byte(SEQ_DX), byte(256-2), byte(SEQ_SET_FALL), 1'u8, 15'u8, byte(
        ord(SEQ_JMP)), freefall and 0x00FF, byte( (freefall and 0xFF00) shr 8), # goto "freefall"

      # patchfall
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), byte(256-3), byte(ord(
        SEQ_JMP)), fall1 and 0x00FF, byte( (fall1 and 0xFF00) shr 8), # goto "fall1"

      # stepfall2 (from frame 12)
    byte(SEQ_DX), 1'u8, byte(SEQ_JMP), stepfall and 0x00FF, byte((
        stepfall and 0xFF00) shr 8),                   # goto "stepfall"

      # stepfloat
    byte(frame102StartFall1),
    byte(SEQ_DX), 2'u8, byte(SEQ_DY), 3'u8, byte(ord(
        frame103StartFall2)),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), 4'u8, byte(ord(
        frame104StartFall3)),
    byte(SEQ_DY), 5'u8, byte(frame105StartFall4),
    byte(SEQ_DX), byte(256-2), byte(SEQ_SET_FALL), 1'u8, 6'u8, byte(
        ord(SEQ_JMP)), freefall and 0x00FF, byte( (freefall and 0xFF00) shr 8), # goto "freefall"

      # jump fall (from standing jump)
    byte(SEQ_ACTION), byte(actions3InMidair), byte(SEQ_DX), 1'u8,
        byte(SEQ_DY), 3'u8, byte(frame102StartFall1),
    byte(SEQ_DX), 2'u8, byte(SEQ_DY), 6'u8, byte(ord(
        frame103StartFall2)),
    byte(SEQ_DX), 1'u8, byte(SEQ_DY), 9'u8, byte(ord(
        frame104StartFall3)),
    byte(SEQ_DX), 2'u8, byte(SEQ_DY), 12'u8, byte(ord(
        frame105StartFall4)),
    byte(SEQ_SET_FALL), 2'u8, 15'u8, byte(SEQ_JMP), freefall and
        0x00FF, byte( (freefall and 0xFF00) shr 8),    # goto "freefall"

      # running jump fall
    byte(SEQ_ACTION), byte(actions3InMidair), byte(SEQ_DX), 1'u8,
        byte(SEQ_DY), 3'u8, byte(frame102StartFall1),
    byte(SEQ_DX), 3'u8, byte(SEQ_DY), 6'u8, byte(ord(
        frame103StartFall2)),
    byte(SEQ_DX), 2'u8, byte(SEQ_DY), 9'u8, byte(ord(
        frame104StartFall3)),
    byte(SEQ_DX), 3'u8, byte(SEQ_DY), 12'u8, byte(ord(
        frame105StartFall4)),
    byte(SEQ_SET_FALL), 3'u8, 15'u8, byte(SEQ_JMP), freefall and
        0x00FF, byte( (freefall and 0xFF00) shr 8),    # goto "freefall"

      # jumphang (medium: DX = 0)
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame67StartJumpUp1)),
    byte(frame68StartJumpUp2), byte(frame69StartJumpUp3), byte(ord(
        frame70Jumphang)), byte(frame71Jumphang),
    byte(frame72Jumphang), byte(frame73Jumphang), byte(ord(
        frame74Jumphang)), ord(frame75Jumphang), byte(frame76Jumphang),
    byte(frame77Jumphang),
    byte(SEQ_ACTION), byte(actions2HangClimb), byte(ord(
        frame78Jumphang)), ord(frame79Jumphang), byte(frame80Jumphang),
    byte(SEQ_JMP), hang and 0x00FF, byte( (hang and 0xFF00) shr 8), # goto "hang"

      # jumphang (long: DX = 4)
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame67StartJumpUp1)),
    byte(frame68StartJumpUp2), byte(frame69StartJumpUp3), byte(ord(
        frame70Jumphang)), byte(frame71Jumphang),
    byte(frame72Jumphang), byte(frame73Jumphang), byte(ord(
        frame74Jumphang)), ord(frame75Jumphang), byte(frame76Jumphang),
    byte(frame77Jumphang),
    byte(SEQ_ACTION), byte(actions2HangClimb), byte(SEQ_DX),
        1'u8, ord(frame78Jumphang),
    byte(SEQ_DX), 2'u8, byte(frame79Jumphang),
    byte(SEQ_DX), 1'u8, byte(frame80Jumphang),
    byte(SEQ_JMP), hang and 0x00FF, byte( (hang and 0xFF00) shr 8), # goto "hang"

      # jumpbackhang
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame67StartJumpUp1)),
    byte(frame68StartJumpUp2), byte(frame69StartJumpUp3), byte(ord(
        frame70Jumphang)), byte(frame71Jumphang),
    byte(frame72Jumphang), byte(frame73Jumphang), byte(ord(
        frame74Jumphang)), ord(frame75Jumphang), byte(frame76Jumphang),
    byte(SEQ_DX), byte(256-1), byte(frame77Jumphang),
    byte(SEQ_ACTION), byte(actions2HangClimb), byte(SEQ_DX),
        byte(256-2), ord(frame78Jumphang),
    byte(SEQ_DX), byte(256-1), byte(frame79Jumphang),
    byte(SEQ_DX), byte(256-1), byte(frame80Jumphang),
    byte(SEQ_JMP), hang and 0x00FF, byte( (hang and 0xFF00) shr 8), # goto "hang"

      # hang
    byte(SEQ_ACTION), byte(actions2HangClimb), byte(ord(
        frame91Hanging5)),
    byte(frame90Hanging4), byte(frame89Hanging3), byte(ord(
        frame88Hanging2)),
    byte(frame87Hanging1), byte(frame87Hanging1), byte(ord(
        frame87Hanging1)), ord(frame88Hanging2),
    byte(frame89Hanging3), byte(frame90Hanging4), byte(ord(
        frame91Hanging5)), ord(frame92Hanging6),
    byte(frame93Hanging7), byte(frame94Hanging8), byte(ord(
        frame95Hanging9)), ord(frame96Hanging10),
    byte(frame97Hanging11), byte(frame98Hanging12), byte(ord(
        frame99Hanging13)), ord(frame97Hanging11),
    byte(frame96Hanging10), byte(frame95Hanging9), byte(ord(
        frame94Hanging8)), ord(frame93Hanging7),
    byte(frame92Hanging6), byte(frame91Hanging5), byte(ord(
        frame90Hanging4)), ord(frame89Hanging3),
    byte(frame88Hanging2), byte(frame87Hanging1), byte(ord(
        frame88Hanging2)), ord(frame89Hanging3),
    byte(frame90Hanging4), byte(frame91Hanging5), byte(ord(
        frame92Hanging6)), ord(frame93Hanging7),
    byte(frame94Hanging8), byte(frame95Hanging9), byte(ord(
        frame96Hanging10)), ord(frame95Hanging9),
    byte(frame94Hanging8), byte(frame93Hanging7), byte(ord(
        frame92Hanging6)),
    byte(SEQ_JMP), hangdrop and 0x00FF, byte( (hangdrop and 0xFF00) shr 8), # goto "hangdrop"

      # hangstraight
    byte(SEQ_ACTION), byte(actions6HangStraight), byte(ord(
        frame92Hanging6)),                             # Apple II source has a bump sound here
    byte(frame93Hanging7), byte(frame93Hanging7), byte(ord(
        frame92Hanging6)), ord(frame92Hanging6),
    byte(frame91Hanging5),
    byte(SEQ_JMP), hangstraightLoop and 0x00FF, byte((hangstraightLoop and
        0xFF00) shr 8),                                # goto ":loop"

      # climbfail
    byte(frame135Climbing1), byte(frame136Climbing2), byte(ord(
        frame137Climbing3)), ord(frame137Climbing3),
    byte(frame138Climbing4), byte(frame138Climbing4), byte(ord(
        frame138Climbing4)), ord(frame138Climbing4),
    byte(frame137Climbing3), byte(frame136Climbing2), byte(ord(
        frame135Climbing1)),
    byte(SEQ_DX), byte(256-7), byte(SEQ_JMP), hangdrop and 0x00FF,
        byte((hangdrop and 0xFF00) shr 8),             # goto "hangdrop"

      # climbdown
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame148Climbing14)),
    byte(frame145Climbing11), byte(frame144Climbing10), byte(ord(
        frame143Climbing9)), byte(frame142Climbing8),
    byte(frame141Climbing7),
    byte(SEQ_DX), byte(256-5), byte(SEQ_DY), 63'u8, byte(ord(
        SEQ_DOWN)), ord(SEQ_ACTION), byte(actions3InMidair), byte(ord(
            frame140Climbing6)),
    byte(frame138Climbing4), byte(frame136Climbing2),
    byte(frame91Hanging5),
    byte(SEQ_ACTION), byte(actions2HangClimb), byte(SEQ_JMP),
        hang1 and 0x00FF, byte( (hang1 and 0xFF00) shr 8), # goto "hang1"

      # climbup
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame135Climbing1)),
    byte(frame136Climbing2), byte(frame137Climbing3), byte(ord(
        frame138Climbing4)),
    byte(frame139Climbing5), byte(frame140Climbing6),
    byte(SEQ_DX), 5'u8, byte(SEQ_DY), byte(256-63), byte(SEQ_UP),
        ord(frame141Climbing7),
    byte(frame142Climbing8), byte(frame143Climbing9), byte(ord(
        frame144Climbing10)), byte(frame145Climbing11),
    byte(frame146Climbing12), byte(frame147Climbing13), byte(ord(
        frame148Climbing14)),
    byte(SEQ_ACTION), byte(actions5Bumped),            # to clear flags
    byte(frame149Climbing15),
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame118StandUpFromCrouch9)), ord(frame119StandUpFromCrouch10),
    byte(SEQ_DX), 1'u8, byte(SEQ_JMP), stand and 0x00FF, byte((
        stand and 0xFF00) shr 8),                      # goto "stand"

      # hangdrop
    byte(frame81Hangdrop1), byte(frame82Hangdrop2),
    byte(SEQ_ACTION), byte(actions5Bumped), byte(ord(
        frame83Hangdrop3)),
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        SEQ_KNOCK_DOWN)), byte(SEQ_SOUND), ord(SND_SILENT),
    byte(frame84Hangdrop4), byte(frame85Hangdrop5),
    byte(SEQ_DX), 3'u8, byte(SEQ_JMP), stand and 0x00FF, byte((
        stand and 0xFF00) shr 8),                      # goto "stand"

      # hangfall
    byte(SEQ_ACTION), byte(actions3InMidair), byte(ord(
        frame81Hangdrop1)),
    byte(SEQ_DY), 6'u8, byte(frame81Hangdrop1),
    byte(SEQ_DY), 9'u8, byte(frame81Hangdrop1),
    byte(SEQ_DY), 12'u8, byte(SEQ_DX), 2'u8, byte(SEQ_SET_FALL),
        0'u8, 12'u8, ord(SEQ_JMP), freefall and 0x00FF, byte((freefall and
            0xFF00) shr 8),                            # goto "freefall"

      # freefall
    byte(SEQ_ACTION), byte(actions4InFreefall), byte(ord(
        frame106Fall)),
    byte(SEQ_JMP), freefallLoop and 0x00FF, byte((freefallLoop and
        0xFF00) shr 8),                                # goto :loop

      # runstop
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame53Runturn)),
    byte(SEQ_DX), 2'u8, byte(SEQ_SOUND), byte(SND_FOOTSTEP),
        byte(frame54Runturn),
    byte(SEQ_DX), 7'u8, byte(frame55Runturn),
    byte(SEQ_SOUND), byte(SND_FOOTSTEP), byte(frame56Runturn),
    byte(SEQ_DX), 2'u8, byte(frame49Turn),
    byte(SEQ_DX), byte(256-2), byte(frame50Turn),
    byte(frame51Turn), byte(frame52Turn),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # jump up (and touch ceiling)
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame67StartJumpUp1)),
    byte(frame68StartJumpUp2), byte(frame69StartJumpUp3), byte(ord(
        frame70Jumphang)), byte(frame71Jumphang),
    byte(frame72Jumphang), byte(frame73Jumphang), byte(ord(
        frame74Jumphang)), ord(frame75Jumphang),
    byte(frame76Jumphang), byte(frame77Jumphang), byte(ord(
        frame78Jumphang)),
    byte(SEQ_ACTION), byte(actions0Stand), byte(SEQ_KNOCK_UP),
        byte(frame79Jumphang),
    byte(SEQ_JMP), hangdrop and 0x00FF, byte( (hangdrop and 0xFF00) shr 8), # goto "hangdrop"

      # highjump (no ceiling above)
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame67StartJumpUp1)),
    byte(frame68StartJumpUp2), byte(frame69StartJumpUp3), byte(ord(
        frame70Jumphang)), byte(frame71Jumphang),
    byte(frame72Jumphang), byte(frame73Jumphang), byte(ord(
        frame74Jumphang)), ord(frame75Jumphang),
    byte(frame76Jumphang), byte(frame77Jumphang), byte(ord(
        frame78Jumphang)), ord(frame79Jumphang),
    byte(SEQ_DY), byte(256-4), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-2), byte(frame79Jumphang),
    byte(frame79Jumphang),
    byte(SEQ_DY), 2'u8, byte(frame79Jumphang),
    byte(SEQ_DY), 4'u8, byte(SEQ_JMP), hangdrop and 0x00FF, byte((
        hangdrop and 0xFF00) shr 8),                   # goto "hangdrop"

      # superhijump (when weightless)
    byte(frame67StartJumpUp1), byte(frame68StartJumpUp2), ord(
        frame69StartJumpUp3), byte(frame70Jumphang),
    byte(frame71Jumphang), byte(frame72Jumphang), byte(ord(
        frame73Jumphang)), ord(frame74Jumphang),
    byte(frame75Jumphang), byte(frame76Jumphang),
    byte(SEQ_DY), byte(256-1), byte(frame77Jumphang),
    byte(SEQ_DY), byte(256-3), byte(frame78Jumphang),
    byte(SEQ_DY), byte(256-4), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-10), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-9), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-8), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-7), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-6), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-5), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-4), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-3), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-2), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-2), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-1), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-1), byte(frame79Jumphang),
    byte(SEQ_DY), byte(256-1), byte(frame79Jumphang),
    byte(frame79Jumphang), byte(frame79Jumphang), byte(ord(
        frame79Jumphang)),
    byte(SEQ_DY), 1'u8, byte(frame79Jumphang),
    byte(SEQ_DY), 1'u8, byte(frame79Jumphang),
    byte(SEQ_DY), 2'u8, byte(frame79Jumphang),
    byte(SEQ_DY), 2'u8, byte(frame79Jumphang),
    byte(SEQ_DY), 3'u8, byte(frame79Jumphang),
    byte(SEQ_DY), 4'u8, byte(frame79Jumphang),
    byte(SEQ_DY), 5'u8, byte(frame79Jumphang),
    byte(SEQ_DY), 6'u8, byte(frame79Jumphang),
    byte(SEQ_SET_FALL), 0'u8, 6'u8, byte(SEQ_JMP), freefall and
        0x00FF, byte( (freefall and 0xFF00) shr 8),    # goto "freefall"

      # fall hang
    byte(SEQ_ACTION), byte(actions3InMidair), byte(ord(
        frame80Jumphang)),
    byte(SEQ_JMP), hang and 0x00FF, byte( (hang and 0xFF00) shr 8), # goto "hang"

      # bump
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_DX), byte(
        256-4), ord(frame50Turn),
    byte(frame51Turn), byte(frame52Turn),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # bumpfall
      # action is patched to 3InMidair by FIX_WALLBUMP_TRIGGERS_TILE_BELOW */
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_DX), 1'u8,
        byte(SEQ_DY), 3'u8, byte(SEQ_JMP_IF_FEATHER), bumpfloat and
            0x00FF, byte((bumpfloat and 0xFF00) shr 8),
    byte(frame102StartFall1),
    byte(SEQ_DX), 2'u8, byte(SEQ_DY), 6'u8, byte(ord(
        frame103StartFall2)),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), 9'u8, byte(ord(
        frame104StartFall3)),
    byte(SEQ_DY), 12'u8, byte(frame105StartFall4),
    byte(SEQ_DX), byte(256-2), byte(SEQ_SET_FALL), 0'u8, 15'u8, byte(
        ord(SEQ_JMP)), freefall and 0x00FF, byte( (freefall and 0xFF00) shr 8), # goto "freefall"

      # bumpfloat
    byte(frame102StartFall1),
    byte(SEQ_DX), 2'u8, byte(SEQ_DY), 3'u8, byte(ord(
        frame103StartFall2)),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), 4'u8, byte(ord(
        frame104StartFall3)),
    byte(SEQ_DY), 5'u8, byte(frame105StartFall4),
    byte(SEQ_DX), byte(256-2), byte(SEQ_SET_FALL), 0'u8, 6'u8, byte(
        ord(SEQ_JMP)), freefall and 0x00FF, byte( (freefall and 0xFF00) shr 8), # goto "freefall"

      # hard bump
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_DX), byte(
        256-1), byte(SEQ_DY), byte(256-4), byte(frame102StartFall1),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), 3'u8, byte(SEQ_DX),
        byte(256-3), ord(SEQ_DY), 1'u8, byte(SEQ_KNOCK_DOWN),
    byte(SEQ_DX), 1'u8, byte(SEQ_SOUND), byte(SND_FOOTSTEP), ord(
        frame107FallLand1),
    byte(SEQ_DX), 2'u8, byte(frame108FallLand2),
    byte(SEQ_SOUND), byte(SND_FOOTSTEP), byte(frame109Crouch),
    byte(SEQ_JMP), standup and 0x00FF, byte( (standup and 0xFF00) shr 8), # goto "standup"

      # test foot
    byte(frame121Stepping1),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(frame123Stepping3),
    byte(SEQ_DX), 2'u8, byte(frame124Stepping4),
    byte(SEQ_DX), 4'u8, byte(frame125Stepping5),
    byte(SEQ_DX), 3'u8, byte(frame126Stepping6),
    byte(SEQ_DX), byte(256-4), byte(frame86TestFoot),
    byte(SEQ_SOUND), byte(SND_FOOTSTEP), byte(SEQ_KNOCK_DOWN),
        byte(SEQ_DX), byte(256-4), byte(frame116StandUpFromCrouch7),
    byte(SEQ_DX), byte(256-2), byte(frame117StandUpFromCrouch8),
    byte(frame118StandUpFromCrouch9),
    byte(frame119StandUpFromCrouch10),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step back
    byte(SEQ_DX), byte(256-5), byte(SEQ_JMP), stand and 0x00FF, byte((
        stand and 0xFF00) shr 8),                      # goto "stand"

      # step forward 14 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 3'u8, byte(frame124Stepping4),
    byte(SEQ_DX), 4'u8, byte(frame125Stepping5),
    byte(SEQ_DX), 3'u8, byte(frame126Stepping6),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DX), 3'u8, byte(ord(
        frame127Stepping7)),
    byte(frame128Stepping8), byte(frame129Stepping9), byte(ord(
        frame130Stepping10)),
    byte(frame131Stepping11), byte(frame132Stepping12),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 13 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 3'u8, byte(frame124Stepping4),
    byte(SEQ_DX), 4'u8, byte(frame125Stepping5),
    byte(SEQ_DX), 3'u8, byte(frame126Stepping6),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DX), 2'u8, byte(ord(
        frame127Stepping7)),
    byte(frame128Stepping8), byte(frame129Stepping9), byte(ord(
        frame130Stepping10)),
    byte(frame131Stepping11), byte(frame132Stepping12),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 12 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 3'u8, byte(frame124Stepping4),
    byte(SEQ_DX), 4'u8, byte(frame125Stepping5),
    byte(SEQ_DX), 3'u8, byte(frame126Stepping6),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DX), 1'u8, byte(ord(
        frame127Stepping7)),
    byte(frame128Stepping8), byte(frame129Stepping9), byte(ord(
        frame130Stepping10)),
    byte(frame131Stepping11), byte(frame132Stepping12),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 11 pixels (normal step)
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 3'u8, byte(frame124Stepping4),
    byte(SEQ_DX), 4'u8, byte(frame125Stepping5),
    byte(SEQ_DX), 3'u8, byte(frame126Stepping6),
    byte(SEQ_DX), byte(256-1), byte(frame127Stepping7),
    byte(frame128Stepping8), byte(frame129Stepping9), byte(ord(
        frame130Stepping10)),
    byte(frame131Stepping11), byte(frame132Stepping12),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 10 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 3'u8, byte(frame124Stepping4),
    byte(SEQ_DX), 4'u8, byte(frame125Stepping5),
    byte(SEQ_DX), 3'u8, byte(frame126Stepping6),
    byte(SEQ_DX), byte(256-2),
    byte(frame128Stepping8), byte(frame129Stepping9), byte(ord(
        frame130Stepping10)),
    byte(frame131Stepping11), byte(frame132Stepping12),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 9 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_JMP), step10a and 0x00FF, byte( (step10a and 0xFF00) shr 8), # goto "step10a"

      # step forward 8 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 3'u8, byte(frame124Stepping4),
    byte(SEQ_DX), 4'u8, byte(frame125Stepping5),
    byte(SEQ_DX), byte(256-1), byte(frame127Stepping7),
    byte(frame128Stepping8), byte(frame129Stepping9), byte(ord(
        frame130Stepping10)), byte(frame131Stepping11),
    byte(frame132Stepping12),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 7 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 3'u8, byte(frame124Stepping4),
    byte(SEQ_DX), 2'u8, byte(frame129Stepping9),
    byte(frame130Stepping10), byte(frame131Stepping11), byte(ord(
        frame132Stepping12)),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 6 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 2'u8, byte(frame124Stepping4),
    byte(SEQ_DX), 2'u8, byte(frame129Stepping9),
    byte(frame130Stepping10), byte(frame131Stepping11), byte(ord(
        frame132Stepping12)),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 5 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 2'u8, byte(frame124Stepping4),
    byte(SEQ_DX), 1'u8, byte(frame129Stepping9),
    byte(frame130Stepping10), byte(frame131Stepping11), byte(ord(
        frame132Stepping12)),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 4 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 2'u8, byte(frame131Stepping11),
    byte(frame132Stepping12),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 3 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame123Stepping3),
    byte(SEQ_DX), 1'u8, byte(frame131Stepping11),
    byte(frame132Stepping12),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 2 pixels
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame122Stepping2),
    byte(SEQ_DX), 1'u8, byte(frame132Stepping12),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # step forward 1 pixel
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame121Stepping1)),
    byte(SEQ_DX), 1'u8, byte(frame132Stepping12),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # stoop
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_DX), 1'u8,
        ord(frame107FallLand1),
    byte(SEQ_DX), 2'u8, byte(frame108FallLand2),
    byte(frame109Crouch),
    byte(SEQ_JMP), stoopCrouch and 0x00FF, byte( (stoopCrouch and
        0xFF00) shr 8),                                # goto ":crouch

      # stand up
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_DX), 1'u8,
        ord(frame110StandUpFromCrouch1),
    byte(frame111StandUpFromCrouch2),
    byte(SEQ_DX), 2'u8, byte(frame112StandUpFromCrouch3),
    byte(frame113StandUpFromCrouch4),
    byte(SEQ_DX), 1'u8, byte(frame114StandUpFromCrouch5),
    byte(frame115StandUpFromCrouch6), byte(ord(
        frame116StandUpFromCrouch7)),
    byte(SEQ_DX), byte(256-4), byte(frame117StandUpFromCrouch8),
    byte(frame118StandUpFromCrouch9), byte(ord(
        frame119StandUpFromCrouch10)),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # pick up sword
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_GET_ITEM),
        1, ord(frame229FoundSword),
    byte(frame229FoundSword), byte(frame229FoundSword), byte(ord(
        frame229FoundSword)), byte(frame229FoundSword),
    byte(frame229FoundSword), byte(frame230Sheathe), byte(ord(
        frame231Sheathe)), ord(frame232Sheathe),
    byte(SEQ_JMP), resheathe and 0x00FF, byte( (resheathe and
        0xFF00) shr 8),                                # goto "resheathe"

      # resheathe
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_DX), byte(
        256-5), ord(frame233Sheathe),
    byte(frame234Sheathe), byte(frame235Sheathe), byte(ord(
        frame236Sheathe)), ord(frame237Sheathe),
    byte(frame238Sheathe), byte(frame239Sheathe), byte(ord(
        frame240Sheathe)), ord(frame133Sheathe),
    byte(frame133Sheathe), byte(frame134Sheathe), byte(ord(
        frame134Sheathe)), ord(frame134Sheathe),
    byte(frame48Turn),
    byte(SEQ_DX), 1'u8, byte(frame49Turn),
    byte(SEQ_DX), byte(256-2), byte(SEQ_ACTION), byte(ord(
        actions5Bumped)), ord(frame50Turn),
    byte(SEQ_ACTION), byte(actions1RunJump), byte(frame51Turn),
    byte(frame52Turn),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # fast sheathe
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_DX), byte(
        256-5), ord(frame234Sheathe),
    byte(frame236Sheathe), byte(frame238Sheathe), byte(ord(
        frame240Sheathe)), ord(frame134Sheathe),
    byte(SEQ_DX), byte(256-1), byte(SEQ_JMP), stand and 0x00FF, byte((
        stand and 0xFF00) shr 8),                      # goto "stand"

      # drink potion
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_DX), 4'u8,
        ord(frame191Drink),
    byte(frame192Drink), byte(frame193Drink), byte(ord(
        frame194Drink)), ord(frame195Drink),
    byte(frame196Drink), byte(frame197Drink), byte(SEQ_SOUND),
        byte(SND_DRINK),
    byte(frame198Drink), byte(frame199Drink), byte(ord(
        frame200Drink)), ord(frame201Drink),
    byte(frame202Drink), byte(frame203Drink), byte(ord(
        frame204Drink)), ord(frame205Drink),
    byte(frame205Drink), byte(frame205Drink),
    byte(SEQ_GET_ITEM), 1, byte(frame205Drink),
    byte(frame205Drink), byte(frame201Drink), byte(ord(
        frame198Drink)),
    byte(SEQ_DX), byte(256-4), byte(SEQ_JMP), stand and 0x00FF, byte((
        stand and 0xFF00) shr 8),                      # goto "stand"

      # soft land
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_KNOCK_DOWN),
        byte(SEQ_DX), 1'u8, byte(frame107FallLand1),
    byte(SEQ_DX), 2'u8, byte(frame108FallLand2),
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame109Crouch)),
    byte(SEQ_JMP), softlandCrouch and 0x00FF, byte((softlandCrouch and
        0xFF00) shr 8),                                # goto ":crouch"

      # land run
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_DY), byte(
        256-2), ord(SEQ_DX), 1'u8, byte(frame107FallLand1),
    byte(SEQ_DX), 2'u8, byte(frame108FallLand2),
    byte(frame109Crouch),
    byte(SEQ_DX), 1'u8, byte(frame110StandUpFromCrouch1),
    byte(frame111StandUpFromCrouch2),
    byte(SEQ_DX), 2'u8, byte(frame112StandUpFromCrouch3),
    byte(frame113StandUpFromCrouch4),
    byte(SEQ_DX), 1'u8, byte(SEQ_DY), 1'u8, byte(ord(
        frame114StandUpFromCrouch5)),
    byte(SEQ_DY), 1'u8, byte(frame115StandUpFromCrouch6),
    byte(SEQ_DX), byte(256-2), byte(SEQ_JMP), runstt4 and 0x00FF,
        byte((runstt4 and 0xFF00) shr 8),              # goto "runstt4"

      # medium land (1.5 - 2 stories)
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_KNOCK_DOWN),
        byte(SEQ_DY), byte(256-2), byte(SEQ_DX), 1'u8, byte(ord(
            SEQ_DX)), 2'u8, byte(frame108FallLand2),
    byte(frame109Crouch), byte(frame109Crouch), byte(ord(
        frame109Crouch)), ord(frame109Crouch),
    byte(frame109Crouch), byte(frame109Crouch), byte(ord(
        frame109Crouch)), ord(frame109Crouch),
    byte(frame109Crouch), byte(frame109Crouch), byte(ord(
        frame109Crouch)), ord(frame109Crouch),
    byte(frame109Crouch), byte(frame109Crouch), byte(ord(
        frame109Crouch)), ord(frame109Crouch),
    byte(frame109Crouch), byte(frame109Crouch), byte(ord(
        frame109Crouch)), ord(frame109Crouch),
    byte(frame109Crouch), byte(frame109Crouch), byte(ord(
        frame109Crouch)), ord(frame109Crouch),
    byte(frame109Crouch), byte(frame109Crouch), byte(ord(
        frame109Crouch)), ord(frame109Crouch),
    byte(frame109Crouch),
    byte(SEQ_DX), 1'u8, byte(frame110StandUpFromCrouch1),
    byte(frame110StandUpFromCrouch1), byte(ord(
        frame110StandUpFromCrouch1)), ord(frame111StandUpFromCrouch2),
    byte(SEQ_DX), 2'u8, byte(frame112StandUpFromCrouch3),
    byte(frame113StandUpFromCrouch4),
    byte(SEQ_DX), 1'u8, byte(SEQ_DY), 1'u8, byte(ord(
        frame114StandUpFromCrouch5)),
    byte(SEQ_DY), 1'u8, byte(frame115StandUpFromCrouch6),
    byte(frame116StandUpFromCrouch7),
    byte(SEQ_DX), byte(256-4), byte(frame117StandUpFromCrouch8),
    byte(frame118StandUpFromCrouch9), byte(ord(
        frame119StandUpFromCrouch10)),
    byte(SEQ_JMP), stand and 0x00FF, byte( (stand and 0xFF00) shr 8), # goto "stand"

      # hard land (splat!; >2 stories)
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_KNOCK_DOWN),
        byte(SEQ_DY), byte(256-2), byte(SEQ_DX), 3'u8, byte(ord(
            frame185Dead)),
    byte(SEQ_DIE), byte(frame185Dead),
    byte(SEQ_JMP), hardlandDead and 0x00FF, byte((hardlandDead and
        0xFF00) shr 8),                                # goto ":dead"

      # stabkill
    byte(SEQ_ACTION), byte(actions5Bumped), byte(SEQ_JMP),
        dropdead and 0x00FF, byte( (dropdead and 0xFF00) shr 8), # goto "dropdead"

      # dropdead
    byte(SEQ_ACTION), byte(actions1RunJump), byte(SEQ_DIE), byte(
        ord(frame179Collapse1)),
    byte(frame180Collapse2), byte(frame181Collapse3), byte(ord(
        frame182Collapse4)),
    byte(SEQ_DX), 1'u8, byte(frame183Collapse5),
    byte(SEQ_DX), byte(256-4), byte(frame185Dead),
    byte(SEQ_JMP), dropdeadDead and 0x00FF, byte((dropdeadDead and
        0xFF00) shr 8),                                # goto ":dead"

      # impale
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        SEQ_KNOCK_DOWN)), byte(SEQ_DX), 4'u8, byte(frame177Spiked),
    byte(SEQ_DIE), byte(frame177Spiked),
    byte(SEQ_JMP), impaleDead and 0x00FF, byte( (impaleDead and
        0xFF00) shr 8),                                # goto ":dead"

      # halve
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame178Chomped)),
    byte(SEQ_DIE), byte(frame178Chomped),
    byte(SEQ_JMP), halveDead and 0x00FF, byte( (halveDead and
        0xFF00) shr 8),                                # goto ":dead"

      # crush
    byte(SEQ_JMP), medland and 0x00FF, byte( (medland and 0xFF00) shr 8), # goto "medland"

      # deadfall
    byte(SEQ_SET_FALL), 0'u8, 0'u8, byte(SEQ_ACTION), byte(ord(
        actions4InFreefall)), byte(frame185Dead),
    byte(SEQ_JMP), deadfallLoop and 0x00FF, byte((deadfallLoop and
        0xFF00) shr 8),                                # goto ":loop"

      # climb stairs
    byte(SEQ_ACTION), byte(actions5Bumped),
    byte(SEQ_DX), byte(256-5), byte(SEQ_DY), byte(256-1), byte(ord(
        SEQ_SOUND)), ord(SND_FOOTSTEP), byte(frame217ExitStairs1),
    byte(frame218ExitStairs2), byte(frame219ExitStairs3),
    byte(SEQ_DX), 1'u8, byte(frame220ExitStairs4),
    byte(SEQ_DX), byte(256-4), byte(SEQ_DY), byte(256-3), byte(ord(
        SEQ_SOUND)), ord(SND_FOOTSTEP), byte(frame221ExitStairs5),
    byte(SEQ_DX), byte(256-4), byte(SEQ_DY), byte(256-2), ord(
        frame222ExitStairs6),
    byte(SEQ_DX), byte(256-2), byte(SEQ_DY), byte(256-3), ord(
        frame223ExitStairs7),
    byte(SEQ_DX), byte(256-3), byte(SEQ_DY), byte(256-8), byte(ord(
        SEQ_SOUND)), ord(SND_LEVEL), byte(SEQ_SOUND), byte(ord(
            SND_FOOTSTEP)), byte(frame224ExitStairs8),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), byte(256-1), ord(
        frame225ExitStairs9),
    byte(SEQ_DX), byte(256-3), byte(SEQ_DY), byte(256-4), ord(
        frame226ExitStairs10),
    byte(SEQ_DX), byte(256-1), byte(SEQ_DY), byte(256-5), byte(ord(
        SEQ_SOUND)), ord(SND_FOOTSTEP), byte(frame227ExitStairs11),
    byte(SEQ_DX), byte(256-2), byte(SEQ_DY), byte(256-1), ord(
        frame228ExitStairs12),
    byte(frame0),
    byte(SEQ_SOUND), byte(SND_FOOTSTEP), byte(frame0), byte(ord(
        frame0)), byte(frame0),  # these footsteps are only heard when the music is off
    byte(SEQ_SOUND), byte(SND_FOOTSTEP), byte(frame0), byte(ord(
        frame0)), byte(frame0),
    byte(SEQ_SOUND), byte(SND_FOOTSTEP), byte(frame0), byte(ord(
        frame0)), byte(frame0),
    byte(SEQ_SOUND), byte(SND_FOOTSTEP), byte(SEQ_END_LEVEL),
        byte(frame0),
    byte(SEQ_JMP), climbstairsLoop and 0x00FF, byte((climbstairsLoop and
        0xFF00) shr 8),                                # goto ":loop"

      # Vizier: stand
    byte(alt2frame54Vstand),
    byte(SEQ_JMP), Vstand and 0x00FF, byte( (Vstand and 0xFF00) shr 8), # goto "Vstand"

      # Vizier: raise arms
    85, 67, 67, 67,  # numbers refer to frames in the "alternate" frame sets
    67, 67, 67, 67,
    67, 67, 67, 68,
    69, 70, 71, 72,
    73, 74, 75, 83,
    84, 76,
    byte(SEQ_JMP), VraiseLoop and 0x00FF, byte( (VraiseLoop and
        0xFF00) shr 8),                                # goto ":loop"

      # Vizier: walk
    byte(SEQ_DX), 1'u8, 48,
    byte(SEQ_DX), 2'u8, 49,
    byte(SEQ_DX), 6'u8, 50,
    byte(SEQ_DX), 1'u8, 51,
    byte(SEQ_DX), byte(256-1), 52,
    byte(SEQ_DX), 1'u8, 53,
    byte(SEQ_DX), 1'u8, byte(SEQ_JMP), Vwalk1 and 0x00FF, byte((
        Vwalk1 and 0xFF00) shr 8),                     # goto "Vwalk1"

      # Vizier: stop
    byte(SEQ_DX), 1'u8, 55,
    56,
    byte(SEQ_JMP), Vstand and 0x00FF, byte( (Vstand and 0xFF00) shr 8),

    # Vizier: lower arms, turn & exit ("Vexit")
    77, 78, 79, 80,
    81, 82,
    byte(SEQ_DX), 1'u8, 54,
    54, 54, 54, 54,
    54, 57, 58, 59,
    60, 61,
    byte(SEQ_DX), 2'u8, 62,
    byte(SEQ_DX), byte(256-1), 63,
    byte(SEQ_DX), byte(256-3), 64,
    65,
    byte(SEQ_DX), byte(256-1), 66,
    byte(SEQ_FLIP), byte(SEQ_DX), 16'u8, byte(SEQ_DX), 3'u8,
        byte(SEQ_JMP), Vwalk2 and 0x00FF, byte( (Vwalk2 and 0xFF00) shr 8), # goto "Vwalk2"

      # Princess: stand
    11,
    byte(SEQ_JMP), Pstand and 0x00FF, byte( (Pstand and 0xFF00) shr 8), # goto "Pstand"

      # Princess: alert
    2, 3, 4, 5,
    6, 7, 8, 9,
    byte(SEQ_FLIP), byte(SEQ_DX), 8'u8, 11,
    byte(SEQ_JMP), Pstand and 0x00FF, byte( (Pstand and 0xFF00) shr 8),

    # Princess: step back
    byte(SEQ_FLIP), byte(SEQ_DX), 11'u8, 12,
    byte(SEQ_DX), 1'u8, 13,
    byte(SEQ_DX), 1'u8, 14,
    byte(SEQ_DX), 3'u8, 15,
    byte(SEQ_DX), 1'u8, 16,
    17,
    byte(SEQ_JMP), PstepbackLoop and 0x00FF, byte((PstepbackLoop and
        0xFF00) shr 8),                                # goto ":loop"

      # Princess lying on cushions ("Plie")
    19,
    byte(SEQ_JMP), Plie and 0x00FF, byte( (Plie and 0xFF00) shr 8), # goto "Plie"

      # Princess: waiting
    20,
    byte(SEQ_JMP), Pwaiting and 0x00FF, byte( (Pwaiting and 0xFF00) shr 8), # goto ":loop"

      # Princess: embrace
    21,
    byte(SEQ_DX), 1'u8, 22,
    23, 24,
    byte(SEQ_DX), 1'u8, 25,
    byte(SEQ_DX), byte(256-3), 26,
    byte(SEQ_DX), byte(256-2), 27,
    byte(SEQ_DX), byte(256-4), 28,
    byte(SEQ_DX), byte(256-3), 29,
    byte(SEQ_DX), byte(256-2), 30,
    byte(SEQ_DX), byte(256-3), 31,
    byte(SEQ_DX), byte(256-1), 32,
    33,
    byte(SEQ_JMP), PembraceLoop and 0x00FF, byte((PembraceLoop and
        0xFF00) shr 8),                                # goto ":loop"

      # Princess: stroke mouse
    37,
    byte(SEQ_JMP), Pstroke and 0x00FF, byte( (Pstroke and 0xFF00) shr 8), # goto ":loop"

      # Princess: rise
    37, 38, 39, 40,
    41, 42, 43, 44,
    45, 46, 47,
    byte(SEQ_FLIP), byte(SEQ_DX), 12'u8, 11,
    byte(SEQ_JMP), PriseLoop and 0x00FF, byte( (PriseLoop and
        0xFF00) shr 8),                                # goto ":loop"

      # Princess: crouch & stroke mouse
    11, 11,
    byte(SEQ_FLIP), byte(SEQ_DX), 13'u8, 47,
    46, 45, 44, 43,
    42, 41, 40, 39,
    38, 37,
    36, 36, 36,
    35, 35, 35,
    34, 34, 34, 34, 34, 34, 34,
    35, 35,
    36, 36, 36,
    35, 35, 35,
    34, 34, 34, 34, 34, 34, 34,
    35, 35,
    36, 36, 36,
    35, 35, 35,
    34, 34, 34, 34, 34, 34, 34, 34, 34,
    35, 35, 35,
    36,
    byte(SEQ_JMP), PcrouchLoop and 0x00FF, byte( (PcrouchLoop and
        0xFF00) shr 8),                                # goto ":loop"

      # Princess: slump shoulders
    1, 18,
    byte(SEQ_JMP), PslumpLoop and 0x00FF, byte( (PslumpLoop and
        0xFF00) shr 8),                                # goto ":loop"

      # Mouse: scurry
    byte(SEQ_ACTION), byte(actions1RunJump), byte(ord(
        frame186Mouse1)),
    byte(SEQ_DX), 5'u8, byte(frame186Mouse1),
    byte(SEQ_DX), 3'u8, byte(frame187Mouse2),
    byte(SEQ_DX), 4'u8, byte(SEQ_JMP), Mscurry1 and 0x00FF, byte((
        Mscurry1 and 0xFF00) shr 8),                   # goto "Mscurry1"

      # Mouse: stop
    byte(frame186Mouse1),
    byte(SEQ_JMP), Mstop and 0x00FF, byte( (Mstop and 0xFF00) shr 8), # goto ":loop"

      # Mouse: raise head
    byte(frame188MouseStand),
    byte(SEQ_JMP), Mraise and 0x00FF, byte( (Mraise and 0xFF00) shr 8), # goto ":loop"

      # Mouse: leave
    byte(SEQ_ACTION), byte(actions0Stand), byte(frame186Mouse1),
    byte(frame186Mouse1), byte(frame186Mouse1), byte(ord(
        frame188MouseStand)), ord(frame188MouseStand),
    byte(frame188MouseStand), byte(frame188MouseStand), byte(ord(
        frame188MouseStand)), byte(frame188MouseStand),
    byte(frame188MouseStand), byte(frame188MouseStand),
    byte(SEQ_FLIP), byte(SEQ_DX), 8'u8, byte(SEQ_JMP),
        Mscurry1 and 0x00FF, byte( (Mscurry1 and 0xFF00) shr 8), # goto "Mscurry1"

      # Mouse: climb
    byte(frame186Mouse1), byte(frame186Mouse1), byte(ord(
        frame188MouseStand)),
    byte(SEQ_JMP), MclimbLoop and 0x00FF, byte( (MclimbLoop and
        0xFF00) shr 8)                                 # goto ":loop"
  ]

proc applySeqtblPatches() =
  when (FIX_WALL_BUMP_TRIGGERS_TILE_BELOW):
    if (fixes.fixWallBumpTriggersTileBelow != 0):
      seqtbl[bumpfall + 1 - SEQTBL_BASE] = ord(
          actions3InMidair) # instead of ord(actions5Bumped)
  else:
    discard


when (CHECK_SEQTABLE_MATCHES_ORIGINAL):

  # unmodified original sequence table
  const
    originalSeqtbl: array[2310, byte] = [0xF9'u8, 0x01'u8, 0xFF'u8, 0x81'u8,
        0x19'u8, 0xF9'u8, 0x01'u8, 0x01'u8, 0x02'u8, 0x03'u8, 0x04'u8, 0xFB'u8,
        0x08'u8, 0x05'u8, 0xFB'u8, 0x03'u8, 0x06'u8, 0xFB'u8, 0x03'u8, 0x07'u8,
        0xFB'u8, 0x05'u8, 0x08'u8, 0xFB'u8, 0x01'u8, 0xF2'u8, 0x01'u8, 0x09'u8,
        0xFB'u8, 0x02'u8, 0x0A'u8, 0xFB'u8, 0x04'u8, 0x0B'u8, 0xFB'u8, 0x05'u8,
        0x0C'u8, 0xFB'u8, 0x02'u8, 0xF2'u8, 0x01'u8, 0x0D'u8, 0xFB'u8, 0x03'u8,
        0x0E'u8, 0xFB'u8, 0x04'u8, 0xFF'u8, 0x81'u8, 0x19'u8, 0xF9'u8, 0x00'u8,
        0x0F'u8, 0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0xA6'u8, 0xFF'u8,
        0xA8'u8, 0x19'u8, 0xF9'u8, 0x05'u8, 0xFB'u8, 0x0A'u8, 0xB1'u8, 0xB1'u8,
        0xFB'u8, 0xF9'u8, 0xFA'u8, 0xFE'u8, 0xB2'u8, 0xFB'u8, 0x05'u8, 0xFA'u8,
        0x02'u8, 0xA6'u8, 0xFB'u8, 0xFF'u8, 0xFF'u8, 0xD2'u8, 0x19'u8, 0xFF'u8,
        0xD2'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0xFB'u8, 0x02'u8, 0xCF'u8, 0xD0'u8,
        0xFB'u8, 0x02'u8, 0xD1'u8, 0xFB'u8, 0x02'u8, 0xD2'u8, 0xFB'u8, 0x03'u8,
        0xF9'u8, 0x01'u8, 0xF2'u8, 0x00'u8, 0x9E'u8, 0xAA'u8, 0xAB'u8, 0xFF'u8,
        0xD8'u8, 0x19'u8, 0xF9'u8, 0x05'u8, 0xF8'u8, 0xFF'u8, 0x00'u8, 0xAC'u8,
        0xFB'u8, 0xFF'u8, 0xFA'u8, 0x01'u8, 0xAD'u8, 0xFB'u8, 0xFF'u8, 0xAE'u8,
        0xFB'u8, 0xFF'u8, 0xFA'u8, 0x02'u8, 0xFB'u8, 0xFE'u8, 0xFA'u8, 0x01'u8,
        0xFB'u8, 0xFB'u8, 0xFA'u8, 0xFC'u8, 0xFF'u8, 0x4D'u8, 0x1A'u8, 0xF9'u8,
        0x01'u8, 0xF8'u8, 0x01'u8, 0x00'u8, 0x9B'u8, 0xFB'u8, 0x02'u8, 0xA5'u8,
        0xFB'u8, 0xFE'u8, 0xFF'u8, 0xD2'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0xF8'u8,
        0xFF'u8, 0x00'u8, 0x9B'u8, 0x9C'u8, 0x9D'u8, 0x9E'u8, 0xFF'u8, 0x2E'u8,
        0x1A'u8, 0xF9'u8, 0x01'u8, 0xF8'u8, 0x01'u8, 0x00'u8, 0xFB'u8, 0x02'u8,
        0xA3'u8, 0xFB'u8, 0x04'u8, 0xA4'u8, 0xA5'u8, 0xFF'u8, 0xD2'u8, 0x19'u8,
        0xF9'u8, 0x01'u8, 0xF8'u8, 0x01'u8, 0x00'u8, 0xFB'u8, 0x06'u8, 0xA4'u8,
        0xA5'u8, 0xFF'u8, 0xD2'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0xF8'u8, 0xFF'u8,
        0x00'u8, 0xFB'u8, 0xFD'u8, 0xA0'u8, 0xFB'u8, 0xFE'u8, 0x9D'u8, 0xFF'u8,
        0xD2'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0xF8'u8, 0xFF'u8, 0x00'u8, 0xA8'u8,
        0xF9'u8, 0x01'u8, 0x97'u8, 0xF9'u8, 0x01'u8, 0x98'u8, 0x99'u8, 0x9A'u8,
        0xF9'u8, 0x05'u8, 0x9B'u8, 0xF9'u8, 0x01'u8, 0x9C'u8, 0x9D'u8, 0xFF'u8,
        0xD2'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0xA7'u8, 0xFF'u8, 0x4A'u8, 0x1A'u8,
        0xA2'u8, 0xFF'u8, 0x45'u8, 0x1A'u8, 0xA9'u8, 0x96'u8, 0xFF'u8, 0xD2'u8,
        0x19'u8, 0x9F'u8, 0xA0'u8, 0xFF'u8, 0x5F'u8, 0x1A'u8, 0xF9'u8, 0x01'u8,
        0xF4'u8, 0xFF'u8, 0xD2'u8, 0x19'u8, 0xF9'u8, 0x05'u8, 0xFB'u8, 0xF8'u8,
        0xFF'u8, 0xD2'u8, 0x19'u8, 0xF9'u8, 0x05'u8, 0xA0'u8, 0x9D'u8, 0xFF'u8,
        0xD2'u8, 0x19'u8, 0xF9'u8, 0x07'u8, 0xFB'u8, 0xF8'u8, 0xFF'u8, 0x39'u8,
        0x1B'u8, 0xF9'u8, 0x05'u8, 0xFE'u8, 0xFB'u8, 0x05'u8, 0xFF'u8, 0x2E'u8,
        0x1A'u8, 0xF9'u8, 0x05'u8, 0xFE'u8, 0xFB'u8, 0x12'u8, 0xFF'u8, 0xA6'u8,
        0x19'u8, 0xF9'u8, 0x01'u8, 0x10'u8, 0x11'u8, 0xFB'u8, 0x02'u8, 0x12'u8,
        0xFB'u8, 0x02'u8, 0x13'u8, 0xFB'u8, 0x02'u8, 0x14'u8, 0xFB'u8, 0x02'u8,
        0x15'u8, 0xFB'u8, 0x02'u8, 0x16'u8, 0xFB'u8, 0x07'u8, 0x17'u8, 0xFB'u8,
        0x09'u8, 0x18'u8, 0xFB'u8, 0x05'u8, 0xFA'u8, 0xFA'u8, 0x19'u8, 0xFB'u8,
        0x01'u8, 0xFA'u8, 0x06'u8, 0x1A'u8, 0xFB'u8, 0x04'u8, 0xF4'u8, 0xF2'u8,
        0x01'u8, 0x1B'u8, 0xFB'u8, 0xFD'u8, 0x1C'u8, 0xFB'u8, 0x05'u8, 0x1D'u8,
        0xF2'u8, 0x01'u8, 0x1E'u8, 0x1F'u8, 0x20'u8, 0x21'u8, 0xFB'u8, 0x01'u8,
        0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0xF2'u8, 0x01'u8, 0x22'u8,
        0xFB'u8, 0x05'u8, 0x23'u8, 0xFB'u8, 0x06'u8, 0x24'u8, 0xFB'u8, 0x03'u8,
        0x25'u8, 0xFB'u8, 0x05'u8, 0xF2'u8, 0x01'u8, 0x26'u8, 0xFB'u8, 0x07'u8,
        0x27'u8, 0xFB'u8, 0x0C'u8, 0xFA'u8, 0xFD'u8, 0x28'u8, 0xFB'u8, 0x08'u8,
        0xFA'u8, 0xF7'u8, 0x29'u8, 0xFB'u8, 0x08'u8, 0xFA'u8, 0xFE'u8, 0x2A'u8,
        0xFB'u8, 0x04'u8, 0xFA'u8, 0x0B'u8, 0x2B'u8, 0xFB'u8, 0x04'u8, 0xFA'u8,
        0x03'u8, 0x2C'u8, 0xFB'u8, 0x05'u8, 0xF4'u8, 0xF2'u8, 0x01'u8, 0xFF'u8,
        0x81'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0xFB'u8, 0x01'u8, 0x6B'u8, 0xFB'u8,
        0x02'u8, 0xFB'u8, 0x02'u8, 0x6C'u8, 0xFB'u8, 0x02'u8, 0x6D'u8, 0xFB'u8,
        0x02'u8, 0x6D'u8, 0xFB'u8, 0x02'u8, 0x6D'u8, 0xFF'u8, 0x16'u8, 0x1B'u8,
        0x00'u8, 0xF9'u8, 0x01'u8, 0xFB'u8, 0x01'u8, 0x6E'u8, 0x6F'u8, 0xFB'u8,
        0x02'u8, 0x70'u8, 0xFB'u8, 0x02'u8, 0x6C'u8, 0xFB'u8, 0x02'u8, 0x6D'u8,
        0xFF'u8, 0x29'u8, 0x1B'u8, 0xF9'u8, 0x07'u8, 0xFE'u8, 0xFB'u8, 0x06'u8,
        0x2D'u8, 0xFB'u8, 0x01'u8, 0x2E'u8, 0xFF'u8, 0xC4'u8, 0x19'u8, 0xF9'u8,
        0x07'u8, 0xFE'u8, 0xFB'u8, 0x06'u8, 0x2D'u8, 0xFB'u8, 0x01'u8, 0x2E'u8,
        0xFB'u8, 0x02'u8, 0x2F'u8, 0xFB'u8, 0xFF'u8, 0x30'u8, 0xFB'u8, 0x01'u8,
        0x31'u8, 0xFB'u8, 0xFE'u8, 0x32'u8, 0x33'u8, 0x34'u8, 0xFF'u8, 0xA0'u8,
        0x19'u8, 0xF9'u8, 0x01'u8, 0xFB'u8, 0xFF'u8, 0xFF'u8, 0x75'u8, 0x19'u8,
        0xF9'u8, 0x01'u8, 0xFB'u8, 0x01'u8, 0x35'u8, 0xFB'u8, 0x01'u8, 0xF2'u8,
        0x01'u8, 0x36'u8, 0xFB'u8, 0x08'u8, 0x37'u8, 0xF2'u8, 0x01'u8, 0x38'u8,
        0xFB'u8, 0x07'u8, 0x39'u8, 0xFB'u8, 0x03'u8, 0x3A'u8, 0xFB'u8, 0x01'u8,
        0x3B'u8, 0x3C'u8, 0xFB'u8, 0x02'u8, 0x3D'u8, 0xFB'u8, 0xFF'u8, 0x3E'u8,
        0x3F'u8, 0x40'u8, 0xFB'u8, 0xFF'u8, 0x41'u8, 0xFB'u8, 0xF2'u8, 0xFE'u8,
        0xFF'u8, 0x95'u8, 0x19'u8, 0xF9'u8, 0x03'u8, 0xFA'u8, 0xFF'u8, 0x66'u8,
        0xFB'u8, 0xFE'u8, 0xFA'u8, 0x06'u8, 0x67'u8, 0xFB'u8, 0xFE'u8, 0xFA'u8,
        0x09'u8, 0x68'u8, 0xFB'u8, 0xFF'u8, 0xFA'u8, 0x0C'u8, 0x69'u8, 0xFB'u8,
        0xFD'u8, 0xF8'u8, 0x00'u8, 0x0F'u8, 0xFF'u8, 0x49'u8, 0x1D'u8, 0xF9'u8,
        0x03'u8, 0xFA'u8, 0xFF'u8, 0xFB'u8, 0xFE'u8, 0x66'u8, 0xFB'u8, 0xFD'u8,
        0xFA'u8, 0x06'u8, 0x67'u8, 0xFB'u8, 0xFD'u8, 0xFA'u8, 0x09'u8, 0x68'u8,
        0xFB'u8, 0xFE'u8, 0xFA'u8, 0x0C'u8, 0x69'u8, 0xFB'u8, 0xFD'u8, 0xF8'u8,
        0x00'u8, 0x0F'u8, 0xFF'u8, 0x49'u8, 0x1D'u8, 0xF9'u8, 0x03'u8, 0xFB'u8,
        0x01'u8, 0xFA'u8, 0xFF'u8, 0x66'u8, 0xFB'u8, 0x02'u8, 0xFA'u8, 0x06'u8,
        0x67'u8, 0xFB'u8, 0xFF'u8, 0xFA'u8, 0x09'u8, 0x68'u8, 0xFA'u8, 0x0C'u8,
        0x69'u8, 0xFB'u8, 0xFE'u8, 0xF8'u8, 0x01'u8, 0x0F'u8, 0xFF'u8, 0x49'u8,
        0x1D'u8, 0xF9'u8, 0x03'u8, 0xFB'u8, 0x01'u8, 0xFA'u8, 0x03'u8, 0xF7'u8,
        0x06'u8, 0x1C'u8, 0x66'u8, 0xFB'u8, 0x02'u8, 0xFA'u8, 0x06'u8, 0x67'u8,
        0xFB'u8, 0xFF'u8, 0xFA'u8, 0x09'u8, 0x68'u8, 0xFA'u8, 0x0C'u8, 0x69'u8,
        0xFB'u8, 0xFE'u8, 0xF8'u8, 0x01'u8, 0x0F'u8, 0xFF'u8, 0x49'u8, 0x1D'u8,
        0xFB'u8, 0xFF'u8, 0xFA'u8, 0xFD'u8, 0xFF'u8, 0xE4'u8, 0x1B'u8, 0xFB'u8,
        0x01'u8, 0xFF'u8, 0xDB'u8, 0x1B'u8, 0x66'u8, 0xFB'u8, 0x02'u8, 0xFA'u8,
        0x03'u8, 0x67'u8, 0xFB'u8, 0xFF'u8, 0xFA'u8, 0x04'u8, 0x68'u8, 0xFA'u8,
        0x05'u8, 0x69'u8, 0xFB'u8, 0xFE'u8, 0xF8'u8, 0x01'u8, 0x06'u8, 0xFF'u8,
        0x49'u8, 0x1D'u8, 0xF9'u8, 0x03'u8, 0xFB'u8, 0x01'u8, 0xFA'u8, 0x03'u8,
        0x66'u8, 0xFB'u8, 0x02'u8, 0xFA'u8, 0x06'u8, 0x67'u8, 0xFB'u8, 0x01'u8,
        0xFA'u8, 0x09'u8, 0x68'u8, 0xFB'u8, 0x02'u8, 0xFA'u8, 0x0C'u8, 0x69'u8,
        0xF8'u8, 0x02'u8, 0x0F'u8, 0xFF'u8, 0x49'u8, 0x1D'u8, 0xF9'u8, 0x03'u8,
        0xFB'u8, 0x01'u8, 0xFA'u8, 0x03'u8, 0x66'u8, 0xFB'u8, 0x03'u8, 0xFA'u8,
        0x06'u8, 0x67'u8, 0xFB'u8, 0x02'u8, 0xFA'u8, 0x09'u8, 0x68'u8, 0xFB'u8,
        0x03'u8, 0xFA'u8, 0x0C'u8, 0x69'u8, 0xF8'u8, 0x03'u8, 0x0F'u8, 0xFF'u8,
        0x49'u8, 0x1D'u8, 0xF9'u8, 0x01'u8, 0x43'u8, 0x44'u8, 0x45'u8, 0x46'u8,
        0x47'u8, 0x48'u8, 0x49'u8, 0x4A'u8, 0x4B'u8, 0x4C'u8, 0x4D'u8, 0xF9'u8,
        0x02'u8, 0x4E'u8, 0x4F'u8, 0x50'u8, 0xFF'u8, 0xA1'u8, 0x1C'u8, 0xF9'u8,
        0x01'u8, 0x43'u8, 0x44'u8, 0x45'u8, 0x46'u8, 0x47'u8, 0x48'u8, 0x49'u8,
        0x4A'u8, 0x4B'u8, 0x4C'u8, 0x4D'u8, 0xF9'u8, 0x02'u8, 0xFB'u8, 0x01'u8,
        0x4E'u8, 0xFB'u8, 0x02'u8, 0x4F'u8, 0xFB'u8, 0x01'u8, 0x50'u8, 0xFF'u8,
        0xA1'u8, 0x1C'u8, 0xF9'u8, 0x01'u8, 0x43'u8, 0x44'u8, 0x45'u8, 0x46'u8,
        0x47'u8, 0x48'u8, 0x49'u8, 0x4A'u8, 0x4B'u8, 0x4C'u8, 0xFB'u8, 0xFF'u8,
        0x4D'u8, 0xF9'u8, 0x02'u8, 0xFB'u8, 0xFE'u8, 0x4E'u8, 0xFB'u8, 0xFF'u8,
        0x4F'u8, 0xFB'u8, 0xFF'u8, 0x50'u8, 0xFF'u8, 0xA1'u8, 0x1C'u8, 0xF9'u8,
        0x02'u8, 0x5B'u8, 0x5A'u8, 0x59'u8, 0x58'u8, 0x57'u8, 0x57'u8, 0x57'u8,
        0x58'u8, 0x59'u8, 0x5A'u8, 0x5B'u8, 0x5C'u8, 0x5D'u8, 0x5E'u8, 0x5F'u8,
        0x60'u8, 0x61'u8, 0x62'u8, 0x63'u8, 0x61'u8, 0x60'u8, 0x5F'u8, 0x5E'u8,
        0x5D'u8, 0x5C'u8, 0x5B'u8, 0x5A'u8, 0x59'u8, 0x58'u8, 0x57'u8, 0x58'u8,
        0x59'u8, 0x5A'u8, 0x5B'u8, 0x5C'u8, 0x5D'u8, 0x5E'u8, 0x5F'u8, 0x60'u8,
        0x5F'u8, 0x5E'u8, 0x5D'u8, 0x5C'u8, 0xFF'u8, 0x25'u8, 0x1D'u8, 0xF9'u8,
        0x06'u8, 0x5C'u8, 0x5D'u8, 0x5D'u8, 0x5C'u8, 0x5C'u8, 0x5B'u8, 0xFF'u8,
        0xD8'u8, 0x1C'u8, 0x87'u8, 0x88'u8, 0x89'u8, 0x89'u8, 0x8A'u8, 0x8A'u8,
        0x8A'u8, 0x8A'u8, 0x89'u8, 0x88'u8, 0x87'u8, 0xFB'u8, 0xF9'u8, 0xFF'u8,
        0x25'u8, 0x1D'u8, 0xF9'u8, 0x01'u8, 0x94'u8, 0x91'u8, 0x90'u8, 0x8F'u8,
        0x8E'u8, 0x8D'u8, 0xFB'u8, 0xFB'u8, 0xFA'u8, 0x3F'u8, 0xFC'u8, 0xF9'u8,
        0x03'u8, 0x8C'u8, 0x8A'u8, 0x88'u8, 0x5B'u8, 0xF9'u8, 0x02'u8, 0xFF'u8,
        0xA4'u8, 0x1C'u8, 0xF9'u8, 0x01'u8, 0x87'u8, 0x88'u8, 0x89'u8, 0x8A'u8,
        0x8B'u8, 0x8C'u8, 0xFB'u8, 0x05'u8, 0xFA'u8, 0xC1'u8, 0xFD'u8, 0x8D'u8,
        0x8E'u8, 0x8F'u8, 0x90'u8, 0x91'u8, 0x92'u8, 0x93'u8, 0x94'u8, 0xF9'u8,
        0x05'u8, 0x95'u8, 0xF9'u8, 0x01'u8, 0x76'u8, 0x77'u8, 0xFB'u8, 0x01'u8,
        0xFF'u8, 0xA0'u8, 0x19'u8, 0x51'u8, 0x52'u8, 0xF9'u8, 0x05'u8, 0x53'u8,
        0xF9'u8, 0x01'u8, 0xF4'u8, 0xF2'u8, 0x00'u8, 0x54'u8, 0x55'u8, 0xFB'u8,
        0x03'u8, 0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x03'u8, 0x51'u8, 0xFA'u8,
        0x06'u8, 0x51'u8, 0xFA'u8, 0x09'u8, 0x51'u8, 0xFA'u8, 0x0C'u8, 0xFB'u8,
        0x02'u8, 0xF8'u8, 0x00'u8, 0x0C'u8, 0xFF'u8, 0x49'u8, 0x1D'u8, 0xF9'u8,
        0x04'u8, 0x6A'u8, 0xFF'u8, 0x4B'u8, 0x1D'u8, 0xF9'u8, 0x01'u8, 0x35'u8,
        0xFB'u8, 0x02'u8, 0xF2'u8, 0x01'u8, 0x36'u8, 0xFB'u8, 0x07'u8, 0x37'u8,
        0xF2'u8, 0x01'u8, 0x38'u8, 0xFB'u8, 0x02'u8, 0x31'u8, 0xFB'u8, 0xFE'u8,
        0x32'u8, 0x33'u8, 0x34'u8, 0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x01'u8,
        0x43'u8, 0x44'u8, 0x45'u8, 0x46'u8, 0x47'u8, 0x48'u8, 0x49'u8, 0x4A'u8,
        0x4B'u8, 0x4C'u8, 0x4D'u8, 0x4E'u8, 0xF9'u8, 0x00'u8, 0xF5'u8, 0x4F'u8,
        0xFF'u8, 0x25'u8, 0x1D'u8, 0xF9'u8, 0x01'u8, 0x43'u8, 0x44'u8, 0x45'u8,
        0x46'u8, 0x47'u8, 0x48'u8, 0x49'u8, 0x4A'u8, 0x4B'u8, 0x4C'u8, 0x4D'u8,
        0x4E'u8, 0x4F'u8, 0xFA'u8, 0xFC'u8, 0x4F'u8, 0xFA'u8, 0xFE'u8, 0x4F'u8,
        0x4F'u8, 0xFA'u8, 0x02'u8, 0x4F'u8, 0xFA'u8, 0x04'u8, 0xFF'u8, 0x25'u8,
        0x1D'u8, 0x43'u8, 0x44'u8, 0x45'u8, 0x46'u8, 0x47'u8, 0x48'u8, 0x49'u8,
        0x4A'u8, 0x4B'u8, 0x4C'u8, 0xFA'u8, 0xFF'u8, 0x4D'u8, 0xFA'u8, 0xFD'u8,
        0x4E'u8, 0xFA'u8, 0xFC'u8, 0x4F'u8, 0xFA'u8, 0xF6'u8, 0x4F'u8, 0xFA'u8,
        0xF7'u8, 0x4F'u8, 0xFA'u8, 0xF8'u8, 0x4F'u8, 0xFA'u8, 0xF9'u8, 0x4F'u8,
        0xFA'u8, 0xFA'u8, 0x4F'u8, 0xFA'u8, 0xFB'u8, 0x4F'u8, 0xFA'u8, 0xFC'u8,
        0x4F'u8, 0xFA'u8, 0xFD'u8, 0x4F'u8, 0xFA'u8, 0xFE'u8, 0x4F'u8, 0xFA'u8,
        0xFE'u8, 0x4F'u8, 0xFA'u8, 0xFF'u8, 0x4F'u8, 0xFA'u8, 0xFF'u8, 0x4F'u8,
        0xFA'u8, 0xFF'u8, 0x4F'u8, 0x4F'u8, 0x4F'u8, 0x4F'u8, 0xFA'u8, 0x01'u8,
        0x4F'u8, 0xFA'u8, 0x01'u8, 0x4F'u8, 0xFA'u8, 0x02'u8, 0x4F'u8, 0xFA'u8,
        0x02'u8, 0x4F'u8, 0xFA'u8, 0x03'u8, 0x4F'u8, 0xFA'u8, 0x04'u8, 0x4F'u8,
        0xFA'u8, 0x05'u8, 0x4F'u8, 0xFA'u8, 0x06'u8, 0x4F'u8, 0xF8'u8, 0x00'u8,
        0x06'u8, 0xFF'u8, 0x49'u8, 0x1D'u8, 0xF9'u8, 0x03'u8, 0x50'u8, 0xFF'u8,
        0xA1'u8, 0x1C'u8, 0xF9'u8, 0x05'u8, 0xFB'u8, 0xFC'u8, 0x32'u8, 0x33'u8,
        0x34'u8, 0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x05'u8, 0xFB'u8, 0x01'u8,
        0xFA'u8, 0x03'u8, 0xF7'u8, 0x25'u8, 0x1E'u8, 0x66'u8, 0xFB'u8, 0x02'u8,
        0xFA'u8, 0x06'u8, 0x67'u8, 0xFB'u8, 0xFF'u8, 0xFA'u8, 0x09'u8, 0x68'u8,
        0xFA'u8, 0x0C'u8, 0x69'u8, 0xFB'u8, 0xFE'u8, 0xF8'u8, 0x00'u8, 0x0F'u8,
        0xFF'u8, 0x49'u8, 0x1D'u8, 0x66'u8, 0xFB'u8, 0x02'u8, 0xFA'u8, 0x03'u8,
        0x67'u8, 0xFB'u8, 0xFF'u8, 0xFA'u8, 0x04'u8, 0x68'u8, 0xFA'u8, 0x05'u8,
        0x69'u8, 0xFB'u8, 0xFE'u8, 0xF8'u8, 0x00'u8, 0x06'u8, 0xFF'u8, 0x49'u8,
        0x1D'u8, 0xF9'u8, 0x05'u8, 0xFB'u8, 0xFF'u8, 0xFA'u8, 0xFC'u8, 0x66'u8,
        0xFB'u8, 0xFF'u8, 0xFA'u8, 0x03'u8, 0xFB'u8, 0xFD'u8, 0xFA'u8, 0x01'u8,
        0xF4'u8, 0xFB'u8, 0x01'u8, 0xF2'u8, 0x01'u8, 0x6B'u8, 0xFB'u8, 0x02'u8,
        0x6C'u8, 0xF2'u8, 0x01'u8, 0x6D'u8, 0xFF'u8, 0xB3'u8, 0x1F'u8, 0x79'u8,
        0xFB'u8, 0x01'u8, 0x7A'u8, 0x7B'u8, 0xFB'u8, 0x02'u8, 0x7C'u8, 0xFB'u8,
        0x04'u8, 0x7D'u8, 0xFB'u8, 0x03'u8, 0x7E'u8, 0xFB'u8, 0xFC'u8, 0x56'u8,
        0xF2'u8, 0x01'u8, 0xF4'u8, 0xFB'u8, 0xFC'u8, 0x74'u8, 0xFB'u8, 0xFE'u8,
        0x75'u8, 0x76'u8, 0x77'u8, 0xFF'u8, 0xA0'u8, 0x19'u8, 0xFB'u8, 0xFB'u8,
        0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0x79'u8, 0xFB'u8, 0x01'u8,
        0x7A'u8, 0xFB'u8, 0x01'u8, 0x7B'u8, 0xFB'u8, 0x03'u8, 0x7C'u8, 0xFB'u8,
        0x04'u8, 0x7D'u8, 0xFB'u8, 0x03'u8, 0x7E'u8, 0xFB'u8, 0xFF'u8, 0xFB'u8,
        0x03'u8, 0x7F'u8, 0x80'u8, 0x81'u8, 0x82'u8, 0x83'u8, 0x84'u8, 0xFF'u8,
        0xA0'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0x79'u8, 0xFB'u8, 0x01'u8, 0x7A'u8,
        0xFB'u8, 0x01'u8, 0x7B'u8, 0xFB'u8, 0x03'u8, 0x7C'u8, 0xFB'u8, 0x04'u8,
        0x7D'u8, 0xFB'u8, 0x03'u8, 0x7E'u8, 0xFB'u8, 0xFF'u8, 0xFB'u8, 0x02'u8,
        0x7F'u8, 0x80'u8, 0x81'u8, 0x82'u8, 0x83'u8, 0x84'u8, 0xFF'u8, 0xA0'u8,
        0x19'u8, 0xF9'u8, 0x01'u8, 0x79'u8, 0xFB'u8, 0x01'u8, 0x7A'u8, 0xFB'u8,
        0x01'u8, 0x7B'u8, 0xFB'u8, 0x03'u8, 0x7C'u8, 0xFB'u8, 0x04'u8, 0x7D'u8,
        0xFB'u8, 0x03'u8, 0x7E'u8, 0xFB'u8, 0xFF'u8, 0xFB'u8, 0x01'u8, 0x7F'u8,
        0x80'u8, 0x81'u8, 0x82'u8, 0x83'u8, 0x84'u8, 0xFF'u8, 0xA0'u8, 0x19'u8,
        0xF9'u8, 0x01'u8, 0x79'u8, 0xFB'u8, 0x01'u8, 0x7A'u8, 0xFB'u8, 0x01'u8,
        0x7B'u8, 0xFB'u8, 0x03'u8, 0x7C'u8, 0xFB'u8, 0x04'u8, 0x7D'u8, 0xFB'u8,
        0x03'u8, 0x7E'u8, 0xFB'u8, 0xFF'u8, 0x7F'u8, 0x80'u8, 0x81'u8, 0x82'u8,
        0x83'u8, 0x84'u8, 0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0x79'u8,
        0xFB'u8, 0x01'u8, 0x7A'u8, 0xFB'u8, 0x01'u8, 0x7B'u8, 0xFB'u8, 0x03'u8,
        0x7C'u8, 0xFB'u8, 0x04'u8, 0x7D'u8, 0xFB'u8, 0x03'u8, 0x7E'u8, 0xFB'u8,
        0xFE'u8, 0x80'u8, 0x81'u8, 0x82'u8, 0x83'u8, 0x84'u8, 0xFF'u8, 0xA0'u8,
        0x19'u8, 0xF9'u8, 0x01'u8, 0x79'u8, 0xFF'u8, 0xFC'u8, 0x1E'u8, 0xF9'u8,
        0x01'u8, 0x79'u8, 0xFB'u8, 0x01'u8, 0x7A'u8, 0xFB'u8, 0x01'u8, 0x7B'u8,
        0xFB'u8, 0x03'u8, 0x7C'u8, 0xFB'u8, 0x04'u8, 0x7D'u8, 0xFB'u8, 0xFF'u8,
        0x7F'u8, 0x80'u8, 0x81'u8, 0x82'u8, 0x83'u8, 0x84'u8, 0xFF'u8, 0xA0'u8,
        0x19'u8, 0xF9'u8, 0x01'u8, 0x79'u8, 0xFB'u8, 0x01'u8, 0x7A'u8, 0xFB'u8,
        0x01'u8, 0x7B'u8, 0xFB'u8, 0x03'u8, 0x7C'u8, 0xFB'u8, 0x02'u8, 0x81'u8,
        0x82'u8, 0x83'u8, 0x84'u8, 0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x01'u8,
        0x79'u8, 0xFB'u8, 0x01'u8, 0x7A'u8, 0xFB'u8, 0x01'u8, 0x7B'u8, 0xFB'u8,
        0x02'u8, 0x7C'u8, 0xFB'u8, 0x02'u8, 0x81'u8, 0x82'u8, 0x83'u8, 0x84'u8,
        0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0x79'u8, 0xFB'u8, 0x01'u8,
        0x7A'u8, 0xFB'u8, 0x01'u8, 0x7B'u8, 0xFB'u8, 0x02'u8, 0x7C'u8, 0xFB'u8,
        0x01'u8, 0x81'u8, 0x82'u8, 0x83'u8, 0x84'u8, 0xFF'u8, 0xA0'u8, 0x19'u8,
        0xF9'u8, 0x01'u8, 0x79'u8, 0xFB'u8, 0x01'u8, 0x7A'u8, 0xFB'u8, 0x01'u8,
        0x7B'u8, 0xFB'u8, 0x02'u8, 0x83'u8, 0x84'u8, 0xFF'u8, 0xA0'u8, 0x19'u8,
        0xF9'u8, 0x01'u8, 0x79'u8, 0xFB'u8, 0x01'u8, 0x7A'u8, 0xFB'u8, 0x01'u8,
        0x7B'u8, 0xFB'u8, 0x01'u8, 0x83'u8, 0x84'u8, 0xFF'u8, 0xA0'u8, 0x19'u8,
        0xF9'u8, 0x01'u8, 0x79'u8, 0xFB'u8, 0x01'u8, 0x7A'u8, 0xFB'u8, 0x01'u8,
        0x84'u8, 0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0x79'u8, 0xFB'u8,
        0x01'u8, 0x84'u8, 0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8, 0x01'u8, 0xFB'u8,
        0x01'u8, 0x6B'u8, 0xFB'u8, 0x02'u8, 0x6C'u8, 0x6D'u8, 0xFF'u8, 0xAF'u8,
        0x1F'u8, 0xF9'u8, 0x05'u8, 0xFB'u8, 0x01'u8, 0x6E'u8, 0x6F'u8, 0xFB'u8,
        0x02'u8, 0x70'u8, 0x71'u8, 0xFB'u8, 0x01'u8, 0x72'u8, 0x73'u8, 0x74'u8,
        0xFB'u8, 0xFC'u8, 0x75'u8, 0x76'u8, 0x77'u8, 0xFF'u8, 0xA0'u8, 0x19'u8,
        0xF9'u8, 0x01'u8, 0xF3'u8, 0x01'u8, 0xE5'u8, 0xE5'u8, 0xE5'u8, 0xE5'u8,
        0xE5'u8, 0xE5'u8, 0xE6'u8, 0xE7'u8, 0xE8'u8, 0xFF'u8, 0xDA'u8, 0x1F'u8,
        0xF9'u8, 0x01'u8, 0xFB'u8, 0xFB'u8, 0xE9'u8, 0xEA'u8, 0xEB'u8, 0xEC'u8,
        0xED'u8, 0xEE'u8, 0xEF'u8, 0xF0'u8, 0x85'u8, 0x85'u8, 0x86'u8, 0x86'u8,
        0x86'u8, 0x30'u8, 0xFB'u8, 0x01'u8, 0x31'u8, 0xFB'u8, 0xFE'u8, 0xF9'u8,
        0x05'u8, 0x32'u8, 0xF9'u8, 0x01'u8, 0x33'u8, 0x34'u8, 0xFF'u8, 0xA0'u8,
        0x19'u8, 0xF9'u8, 0x01'u8, 0xFB'u8, 0xFB'u8, 0xEA'u8, 0xEC'u8, 0xEE'u8,
        0xF0'u8, 0x86'u8, 0xFB'u8, 0xFF'u8, 0xFF'u8, 0xA0'u8, 0x19'u8, 0xF9'u8,
        0x01'u8, 0xFB'u8, 0x04'u8, 0xBF'u8, 0xC0'u8, 0xC1'u8, 0xC2'u8, 0xC3'u8,
        0xC4'u8, 0xC5'u8, 0xF2'u8, 0x03'u8, 0xC6'u8, 0xC7'u8, 0xC8'u8, 0xC9'u8,
        0xCA'u8, 0xCB'u8, 0xCC'u8, 0xCD'u8, 0xCD'u8, 0xCD'u8, 0xF3'u8, 0x01'u8,
        0xCD'u8, 0xCD'u8, 0xC9'u8, 0xC6'u8, 0xFB'u8, 0xFC'u8, 0xFF'u8, 0xA0'u8,
        0x19'u8, 0xF9'u8, 0x05'u8, 0xF4'u8, 0xFB'u8, 0x01'u8, 0x6B'u8, 0xFB'u8,
        0x02'u8, 0x6C'u8, 0xF9'u8, 0x01'u8, 0x6D'u8, 0xFF'u8, 0x36'u8, 0x20'u8,
        0xF9'u8, 0x01'u8, 0xFA'u8, 0xFE'u8, 0xFB'u8, 0x01'u8, 0x6B'u8, 0xFB'u8,
        0x02'u8, 0x6C'u8, 0x6D'u8, 0xFB'u8, 0x01'u8, 0x6E'u8, 0x6F'u8, 0xFB'u8,
        0x02'u8, 0x70'u8, 0x71'u8, 0xFB'u8, 0x01'u8, 0xFA'u8, 0x01'u8, 0x72'u8,
        0xFA'u8, 0x01'u8, 0x73'u8, 0xFB'u8, 0xFE'u8, 0xFF'u8, 0x78'u8, 0x19'u8,
        0xF9'u8, 0x05'u8, 0xF4'u8, 0xFA'u8, 0xFE'u8, 0xFB'u8, 0x01'u8, 0xFB'u8,
        0x02'u8, 0x6C'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8,
        0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8,
        0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8,
        0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0x6D'u8, 0xFB'u8,
        0x01'u8, 0x6E'u8, 0x6E'u8, 0x6E'u8, 0x6F'u8, 0xFB'u8, 0x02'u8, 0x70'u8,
        0x71'u8, 0xFB'u8, 0x01'u8, 0xFA'u8, 0x01'u8, 0x72'u8, 0xFA'u8, 0x01'u8,
        0x73'u8, 0x74'u8, 0xFB'u8, 0xFC'u8, 0x75'u8, 0x76'u8, 0x77'u8, 0xFF'u8,
        0xA0'u8, 0x19'u8, 0xF9'u8, 0x05'u8, 0xF4'u8, 0xFA'u8, 0xFE'u8, 0xFB'u8,
        0x03'u8, 0xB9'u8, 0xF6'u8, 0xB9'u8, 0xFF'u8, 0xA5'u8, 0x20'u8, 0xF9'u8,
        0x05'u8, 0xFF'u8, 0xAE'u8, 0x20'u8, 0xF9'u8, 0x01'u8, 0xF6'u8, 0xB3'u8,
        0xB4'u8, 0xB5'u8, 0xB6'u8, 0xFB'u8, 0x01'u8, 0xB7'u8, 0xFB'u8, 0xFC'u8,
        0xB9'u8, 0xFF'u8, 0xBA'u8, 0x20'u8, 0xF9'u8, 0x01'u8, 0xF4'u8, 0xFB'u8,
        0x04'u8, 0xB1'u8, 0xF6'u8, 0xB1'u8, 0xFF'u8, 0xC5'u8, 0x20'u8, 0xF9'u8,
        0x01'u8, 0xB2'u8, 0xF6'u8, 0xB2'u8, 0xFF'u8, 0xCD'u8, 0x20'u8, 0xFF'u8,
        0x5A'u8, 0x20'u8, 0xF8'u8, 0x00'u8, 0x00'u8, 0xF9'u8, 0x04'u8, 0xB9'u8,
        0xFF'u8, 0xD9'u8, 0x20'u8, 0xF9'u8, 0x05'u8, 0xFB'u8, 0xFB'u8, 0xFA'u8,
        0xFF'u8, 0xF2'u8, 0x01'u8, 0xD9'u8, 0xDA'u8, 0xDB'u8, 0xFB'u8, 0x01'u8,
        0xDC'u8, 0xFB'u8, 0xFC'u8, 0xFA'u8, 0xFD'u8, 0xF2'u8, 0x01'u8, 0xDD'u8,
        0xFB'u8, 0xFC'u8, 0xFA'u8, 0xFE'u8, 0xDE'u8, 0xFB'u8, 0xFE'u8, 0xFA'u8,
        0xFD'u8, 0xDF'u8, 0xFB'u8, 0xFD'u8, 0xFA'u8, 0xF8'u8, 0xF2'u8, 0x04'u8,
        0xF2'u8, 0x01'u8, 0xE0'u8, 0xFB'u8, 0xFF'u8, 0xFA'u8, 0xFF'u8, 0xE1'u8,
        0xFB'u8, 0xFD'u8, 0xFA'u8, 0xFC'u8, 0xE2'u8, 0xFB'u8, 0xFF'u8, 0xFA'u8,
        0xFB'u8, 0xF2'u8, 0x01'u8, 0xE3'u8, 0xFB'u8, 0xFE'u8, 0xFA'u8, 0xFF'u8,
        0xE4'u8, 0x00'u8, 0xF2'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0xF2'u8,
        0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0xF2'u8, 0x01'u8, 0x00'u8, 0x00'u8,
        0x00'u8, 0xF2'u8, 0x01'u8, 0xF1'u8, 0x00'u8, 0xFF'u8, 0x2E'u8, 0x21'u8,
        0x36'u8, 0xFF'u8, 0x32'u8, 0x21'u8, 0x55'u8, 0x43'u8, 0x43'u8, 0x43'u8,
        0x43'u8, 0x43'u8, 0x43'u8, 0x43'u8, 0x43'u8, 0x43'u8, 0x43'u8, 0x44'u8,
        0x45'u8, 0x46'u8, 0x47'u8, 0x48'u8, 0x49'u8, 0x4A'u8, 0x4B'u8, 0x53'u8,
        0x54'u8, 0x4C'u8, 0xFF'u8, 0x4B'u8, 0x21'u8, 0xFB'u8, 0x01'u8, 0x30'u8,
        0xFB'u8, 0x02'u8, 0x31'u8, 0xFB'u8, 0x06'u8, 0x32'u8, 0xFB'u8, 0x01'u8,
        0x33'u8, 0xFB'u8, 0xFF'u8, 0x34'u8, 0xFB'u8, 0x01'u8, 0x35'u8, 0xFB'u8,
        0x01'u8, 0xFF'u8, 0x51'u8, 0x21'u8, 0xFB'u8, 0x01'u8, 0x37'u8, 0x38'u8,
        0xFF'u8, 0x32'u8, 0x21'u8, 0x4D'u8, 0x4E'u8, 0x4F'u8, 0x50'u8, 0x51'u8,
        0x52'u8, 0xFB'u8, 0x01'u8, 0x36'u8, 0x36'u8, 0x36'u8, 0x36'u8, 0x36'u8,
        0x36'u8, 0x39'u8, 0x3A'u8, 0x3B'u8, 0x3C'u8, 0x3D'u8, 0xFB'u8, 0x02'u8,
        0x3E'u8, 0xFB'u8, 0xFF'u8, 0x3F'u8, 0xFB'u8, 0xFD'u8, 0x40'u8, 0x41'u8,
        0xFB'u8, 0xFF'u8, 0x42'u8, 0xFE'u8, 0xFB'u8, 0x10'u8, 0xFB'u8, 0x03'u8,
        0xFF'u8, 0x54'u8, 0x21'u8, 0x0B'u8, 0xFF'u8, 0x95'u8, 0x21'u8, 0x02'u8,
        0x03'u8, 0x04'u8, 0x05'u8, 0x06'u8, 0x07'u8, 0x08'u8, 0x09'u8, 0xFE'u8,
        0xFB'u8, 0x08'u8, 0x0B'u8, 0xFF'u8, 0x95'u8, 0x21'u8, 0xFE'u8, 0xFB'u8,
        0x0B'u8, 0x0C'u8, 0xFB'u8, 0x01'u8, 0x0D'u8, 0xFB'u8, 0x01'u8, 0x0E'u8,
        0xFB'u8, 0x03'u8, 0x0F'u8, 0xFB'u8, 0x01'u8, 0x10'u8, 0x11'u8, 0xFF'u8,
        0xB8'u8, 0x21'u8, 0x13'u8, 0xFF'u8, 0xBC'u8, 0x21'u8, 0x14'u8, 0xFF'u8,
        0xC0'u8, 0x21'u8, 0x15'u8, 0xFB'u8, 0x01'u8, 0x16'u8, 0x17'u8, 0x18'u8,
        0xFB'u8, 0x01'u8, 0x19'u8, 0xFB'u8, 0xFD'u8, 0x1A'u8, 0xFB'u8, 0xFE'u8,
        0x1B'u8, 0xFB'u8, 0xFC'u8, 0x1C'u8, 0xFB'u8, 0xFD'u8, 0x1D'u8, 0xFB'u8,
        0xFE'u8, 0x1E'u8, 0xFB'u8, 0xFD'u8, 0x1F'u8, 0xFB'u8, 0xFF'u8, 0x20'u8,
        0x21'u8, 0xFF'u8, 0xE2'u8, 0x21'u8, 0x25'u8, 0xFF'u8, 0xE6'u8, 0x21'u8,
        0x25'u8, 0x26'u8, 0x27'u8, 0x28'u8, 0x29'u8, 0x2A'u8, 0x2B'u8, 0x2C'u8,
        0x2D'u8, 0x2E'u8, 0x2F'u8, 0xFE'u8, 0xFB'u8, 0x0C'u8, 0x0B'u8, 0xFF'u8,
        0xF8'u8, 0x21'u8, 0x0B'u8, 0x0B'u8, 0xFE'u8, 0xFB'u8, 0x0D'u8, 0x2F'u8,
        0x2E'u8, 0x2D'u8, 0x2C'u8, 0x2B'u8, 0x2A'u8, 0x29'u8, 0x28'u8, 0x27'u8,
        0x26'u8, 0x25'u8, 0x24'u8, 0x24'u8, 0x24'u8, 0x23'u8, 0x23'u8, 0x23'u8,
        0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x23'u8,
        0x23'u8, 0x24'u8, 0x24'u8, 0x24'u8, 0x23'u8, 0x23'u8, 0x23'u8, 0x22'u8,
        0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x23'u8, 0x23'u8,
        0x24'u8, 0x24'u8, 0x24'u8, 0x23'u8, 0x23'u8, 0x23'u8, 0x22'u8, 0x22'u8,
        0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x22'u8, 0x23'u8,
        0x23'u8, 0x23'u8, 0x24'u8, 0xFF'u8, 0x3C'u8, 0x22'u8, 0x01'u8, 0x12'u8,
        0xFF'u8, 0x41'u8, 0x22'u8, 0xF9'u8, 0x01'u8, 0xBA'u8, 0xFB'u8, 0x05'u8,
        0xBA'u8, 0xFB'u8, 0x03'u8, 0xBB'u8, 0xFB'u8, 0x04'u8, 0xFF'u8, 0x47'u8,
        0x22'u8, 0xBA'u8, 0xFF'u8, 0x53'u8, 0x22'u8, 0xBC'u8, 0xFF'u8, 0x57'u8,
        0x22'u8, 0xF9'u8, 0x00'u8, 0xBA'u8, 0xBA'u8, 0xBA'u8, 0xBC'u8, 0xBC'u8,
        0xBC'u8, 0xBC'u8, 0xBC'u8, 0xBC'u8, 0xBC'u8, 0xBC'u8, 0xFE'u8, 0xFB'u8,
        0x08'u8, 0xFF'u8, 0x47'u8, 0x22'u8, 0xBA'u8, 0xBA'u8, 0xBC'u8, 0xFF'u8,
        0x70'u8, 0x22'u8]

    originalSeqtblOffsets: array[115, word] = [
        word(0x0000), word(0x1973), word(0x19A0), word(0x1A93), word(0x1ACD),
            word(0x1B39), word(0x1B5A), word(0x1BDB),
        word(0x1C54), word(0x1CA1), word(0x1D04), word(0x1D25), word(0x1D49),
            word(0x1D4F), word(0x1D68), word(0x1DF6),
        word(0x1C84), word(0x202B), word(0x1C1C), word(0x1C01), word(0x205A),
            word(0x1C38), word(0x209C), word(0x1D36),
        word(0x1C69), word(0x1CD1), word(0x1B04), word(0x1B1A), word(0x1D7D),
            word(0x1F9E), word(0x1F92), word(0x1F82),
        word(0x1F72), word(0x1F5D), word(0x1F48), word(0x1F33), word(0x1F19),
            word(0x1F13), word(0x1EF7), word(0x1EDA),
        word(0x1EBB), word(0x1E9C), word(0x1E7D), word(0x1B53), word(0x1E59),
            word(0x1E06), word(0x1E3B), word(0x1DFC),
        word(0x1D9B), word(0x1FB3), word(0x1FA7), word(0x20BE), word(0x20D1),
            word(0x20D4), word(0x20C9), word(0x19C4),
        word(0x1A13), word(0x1A2E), word(0x1A3C), word(0x1A7C), word(0x1A83),
            word(0x1A63), word(0x1A5E), word(0x1A68),
        word(0x1A6E), word(0x1A75), word(0x1A5A), word(0x19F9), word(0x1CEC),
            word(0x1A54), word(0x20DD), word(0x20AE),
        word(0x1E78), word(0x1CDC), word(0x19DC), word(0x1A42), word(0x1A07),
            word(0x19A8), word(0x2009), word(0x1B1B),
        word(0x1A8B), word(0x1B85), word(0x1BA1), word(0x1BBF), word(0x196E),
            word(0x20A9), word(0x1A22), word(0x19A6),
        word(0x19AC), word(0x1B2D), word(0x19C1), word(0x1FCA), word(0x1FDA),
            word(0x1FFB), word(0x2195), word(0x2132),
        word(0x214F), word(0x2166), word(0x2199), word(0x21A8), word(0x216D),
            word(0x226E), word(0x2136), word(0x21BC),
        word(0x1BFA), word(0x2245), word(0x2253), word(0x225B), word(0x21C4),
            word(0x21C0), word(0x21E6), word(0x21EA),
        word(0x21FC), word(0x2240), word(0x2257)
      ]

  proc checkSeqtableMatchesOriginal() =
    echo("Checking that the sequence table matches the original DOS version...")
    var
      different: int32 = 0
    for i in 0..<originalSeqtbl.len:
      if (seqtbl[i] != originalSeqtbl[i]):
        different = 1
        echo("Seqtbl difference at index " & $(i) & " (0x" & toHex(i) &
            "; shifted offset 0x" & toHex(i + SEQTBL_BASE) & "): value is " & $(
            seqtbl[i]) & ", should be " & $(originalSeqtbl[i]))

    for i in 0..<originalSeqtblOffsets.len:
      if (seqtblOffsets[i] != originalSeqtblOffsets[i]):
        different = 1
        echo("Seqtbl offset difference at index " & $(i) & ": value is 0x" &
            toHex(seqtblOffsets[i]) & ", should be 0x" & toHex(
            originalSeqtblOffsets[i]))

    if (different == 0):
      echo("All good, no differences found!")
