when (USE_LIGHTING):
  var
    screenOverlay: ImageType = nil
    bgcolor: uint32

  const
    maskFilename = "data/light.png"
    ambientLevel: uint8 = 128

  # Called once at startup.
  proc initLighting() =
    if (enableLighting == 0):
      return

    let
      lightingMask = load(cstring(locateFile(maskFilename)))

    if isNil(lightingMask):
      sdlperror("IMG_LOAD (lightingMask)")
      enableLighting = 0
      return

    let
      screenOverlay: Surface = createRGBSurface(0, 320, 192, 32, 0xFF shl 0,
          0xFF shl 8, 0xFF shl 16, 0xFF'u32 shl 24)
    if isNil(screenOverlay):
      sdlperror("SDLCreateRGBSurface (screenOverlay)")
      enableLighting = 0
      return

    # color modulate, i.e. multiply
    var
      res = setSurfaceBlendMode(screenOverlay, BLENDMODE_MOD)
    if res != 0:
      sdlperror("SDLSetSurfaceBlendMode (screenOverlay)")

    res = setSurfaceBlendMode(lightingMask, BLENDMODE_ADD)
    if res != 0:
      sdlperror("SDLSetSurfaceBlendMode (lightingMask)")

    # ambient lighting
    bgcolor = mapRGBA(screenOverlay.format, ambientLevel, ambientLevel,
        ambientLevel, ALPHA_OPAQUE)

  # Recreate the lighting overlay based on the torches in the current room.
  # Called when the current room changes.
  proc redrawLighting() =
    if (enableLighting == 0):
      return

    if isNil(lightingMask):
      return

    if currRoomTiles == nil:
      return

    if (isCutscene != 0):
      return

    var
      res = fillRect(screenOverlay, nil, bgcolor)

    if res != 0:
      sdlperror("SDLFillRect (screenOverlay)")

    # TODO: Also process nearby offscreen torches
    for tilePos in 0..<30:
      var
        tileType = currRoomTiles[tilePos] and 0x1f
      if (tileType == ord(tiles19Torch)) or (tileType == ord(
          tiles30TorchWithDebris)):
        # Center of the flame
        let
          x: int32 = (tilePos mod 10) * 32 + 48
          y: int32 = (tilePos div 10) * 63 + 22

        # Align the center of lighting mask to the center of the flame
        var
          destReact: Rect
          res2: int32

        destReact.x = x - lightingMask.w div 2
        destReact.y = y - lightingMask.h div 2
        destReact.w = lightingMask.w
        destReact.h = lightingMask.h

        res2 = blitSurface(lightingMask, nil, screenOverlay, addr(destReact))
        if res2 != 0:
          sdlperror("SDLBlitSurface (lightingMask)")

    if (upsideDown != 0):
      flipScreen(screenOverlay)

  # Copy a part of the lighting overlay onto the screen.
  # Called when the screen is updated.
  proc updateLighting(sourceRectPtr: var RectType) =
    if (enableLighting == 0):
      return
    if isNil(lightingMask):
      return
    if (currRoomTiles == nil):
      return
    if (isCutscene != 0):
      return

    var
      sdlrect: Rect
    rectToSdlrect(sourceRectPtr, sdlrect)

    let
      res = blitSurface(screenOverlay, addr(sdlrect), onscreenSurface, addr(sdlrect))
    if res != 0:
      sdlperror("SDLBlitSurface (screenOverlay)")
