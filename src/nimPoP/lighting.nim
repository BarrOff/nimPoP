when (USE_LIGHTING):
  var
    screenOverlay: ImageType = nil
    bgcolor: uint32

  const
    maskFilename = "data/light.png"
    ambientLevel: uint8 = 128

  # Called once at startup.
  proc initLighting() =
    if (enableLighting == 0):
      return

    lightingMask = load(cstring(locateFile(maskFilename)))

    if isNil(lightingMask):
      sdlperror("IMG_LOAD (lightingMask)")
      enableLighting = 0
      return

    let
      screenOverlay: SurfaceType = createRGBSurface(0, 320, 192, 32, Rmsk, Gmsk,
          Bmsk, Amsk)
    if isNil(screenOverlay):
      sdlperror("SDLCreateRGBSurface (screenOverlay)")
      enableLighting = 0
      return

    # color modulate, i.e. multiply
    var
      res = setSurfaceBlendMode(screenOverlay, BLENDMODE_MOD)
    if res != 0:
      sdlperror("SDLSetSurfaceBlendMode (screenOverlay)")

    res = setSurfaceBlendMode(lightingMask, BLENDMODE_ADD)
    if res != 0:
      sdlperror("SDLSetSurfaceBlendMode (lightingMask)")

    # ambient lighting
    bgcolor = mapRGBA(screenOverlay.format, ambientLevel, ambientLevel,
        ambientLevel, SDL_ALPHA_OPAQUE)

  # Recreate the lighting overlay based on the torches in the current room.
  # Called when the current room changes.
  proc redrawLighting() =
    if (enableLighting == 0):
      return

    if isNil(lightingMask):
      return

    if isNil(currRoomTiles):
      return

    if (isCutscene != 0):
      return

    let
      res = fillRect(screenOverlay, nil, bgcolor)

    if res != SdlSuccess:
      sdlperror("SDLFillRect (screenOverlay)")

    # TODO: Also process nearby offscreen torches
    for tilePos in 0..<30:
      let
        tileType = Tiles(byte(currRoomTiles[tilePos]) and 0x1f)
      if (tileType == tiles19Torch) or (tileType == tiles30TorchWithDebris):
        # Center of the flame
        let
          x: int32 = int32((tilePos mod 10) * 32 + 48)
          y: int32 = int32((tilePos div 10) * 63 + 22)

        # Align the center of lighting mask to the center of the flame
        var
          destReact: Rect

        destReact.x = x - lightingMask.w div 2
        destReact.y = y - lightingMask.h div 2
        destReact.w = lightingMask.w
        destReact.h = lightingMask.h

        let
          res2 = blitSurface(lightingMask, nil, screenOverlay, addr(destReact))
        if res2 != SdlSuccess:
          sdlperror("SDLBlitSurface (lightingMask)")

    if (upsideDown != 0):
      flipScreen(screenOverlay)

  # Copy a part of the lighting overlay onto the screen.
  # Called when the screen is updated.
  proc updateLighting(sourceRectPtr: ptr RectType) =
    if (enableLighting == 0):
      return
    if isNil(lightingMask):
      return
    if isNil(currRoomTiles):
      return
    if (isCutscene != 0):
      return

    var
      sdlrect: Rect
    rectToSdlrect(sourceRectPtr[], sdlrect)

    let
      res = blitSurface(screenOverlay, addr(sdlrect), onscreenSurface, addr(sdlrect))
    if res != SdlSuccess:
      sdlperror("SDLBlitSurface (screenOverlay)")
