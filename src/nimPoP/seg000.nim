type
  JmpBuf {.importc: "jmp_buf", header: "<setjmp.h>".} = object

proc setjmp(buf: JmpBuf): cint {.importc: "setjmp", header: "<setjmp.h>".}
proc longjmp(buf: JmpBuf, status: cint) {.importc: "longjmp",
    header: "<setjmp.h>".}

var
  # data:461E
  datHandle: DatType
  # data:4C08
  needRedrawBecauseFlipped: word

proc fixSoundPriorities()

# seg000:0000
proc popMain() =
  # if checkParam("--version") or checkParam("-v"):
  #   echo "SDLPoP v", SDLPOP_VERSION
  #   quitPoP(0)

  # if checkParam("--help") or checkParam("-h") or checkParam("-?"):
  #   echo "See doc/Readme.txt"
  #   quitPoP(0)

  # var
  #   temp: string = checkParam("seed=")
  var
    p = initOptParser(commandLineParams())
    previousLevel: int32 = -1
  while true:
    p.next()
    case p.kind
    of cmdEnd:
      break
    of cmdShortOption, cmdLongOption:
      case p.key
      of "version", "v":
        echo "nimPoP v", NIMPOP_VERSION
        quitPoP(0)
      of "help", "h", "?":
        echo "See Readme.md"
        quitPoP(0)
      else:
        discard
    of cmdArgument:
      when(USE_REPLAY):
        if p.key == "validate":
          isValidateMode = 1
          startWithReplayFile(p.val)
          continue
      when(USE_DEBUG_CHEATS):
        if p.key == "debug":
          if p.val.len != 0:
            debugCheatsEnabled = 1
          if debugCheatsEnabled == 1:
            # param 'megahit' not necessary if 'debug' is used
            cheatsEnabled = 1
          continue
      when(USE_REPLAY):
        if p.key.len > 4 and (p.key[^4..p.key.len] == ".P1R"):
          startWithReplayFile(p.key)
          continue
      case p.key
      of "megahit":
        if p.val.len != 0:
          cheatsEnabled = 1
      of "draw":
        drawMode = word(p.val.len != 0 and (cheatsEnabled != 0))
      of "demo":
        demoMode = word(p.val.len != 0)
      of "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
          "13", "14", "15":
        when (USE_DEBUG_CHEATS or USE_REPLAY):
          previousLevel = int32(parseInt(p.key))
          startLevel = int16(previousLevel)
        else:
          for s in commandLineParams():
            if s.contains("megahit") or s.contains("debug"):
              previousLevel = int32(parseInt(p.key))
              startLevel = int16(previousLevel)
              break
      of "playdemo":
        playDemoLevel = true
      of "full":
        fullCommandOption = true
      of "stdsnd":
        stdsndCommandOption = true
      else:
        if "seed=" in p.key:
          randomSeed = uint32(parseUInt(p.key[5..^1]))
          seedWasInit = 1


  # debug only: check that the sequence table deobfuscation did not mess things up
  when(CHECK_SEQTABLE_MATCHES_ORIGINAL):
    checkSeqtableMatchesOriginal()

  when(FIX_SOUND_PRIORITIES):
    fixSoundPriorities()

  loadGlobalOptions()
  checkModParam()
  when(USE_MENU):
    loadIngameSettings()
  # Turn off sound/music if those options were set
  turnSoundOnOff(byte(isSoundOn != 0) * 15'u8)

  # Initialize everything before load_mod_options() so it can show an error
  # dialog if needed.
  # video mode
  discard parseGrmode()
  currentTargetSurface = rectSthg(onscreenSurface, screenRect)
  setHcPal()
  initCopyProtDialog()

  loadModOptions()

  # CusPop option
  isBlindMode = custom.startInBlindMode
  # Fix bug: with start_in_blind_mode enabled, moving objects are not displayed
  # until blind mode is toggled off+on??
  needDrects = 1

  applySeqtblPatches()


  initTimer(BASE_FPS)
  parseCmdlineSound()

  showLoading()
  discard setJoyMode()

  when(USE_REPLAY):
    initRecordReplay()

    # I moved this after init_copyprot_dialog(), so open_dat() can show an error dialog if needed.
    dathandle = openDat("PRINCE.DAT", int32('G'))

    if (cheatsEnabled or recording) != 0:
      if previousLevel != -1:
        startLevel = int16(previousLevel)
  else:
    # I moved this after init_copyprot_dialog(), so open_dat() can show an error dialog if needed.
    dathandle = openDat("PRINCE.DAT", int32('G'))

    if (cheatsEnabled) != 0:
      if previousLevel != -1:
        startLevel = int16(previousLevel)

  when(USE_SCREENSHOT):
    initScreenshot()

  when(USE_MENU):
    initMenu()

  initGameMain()

var
  levelVarPalettes: ptr byte

# seg000:024F
proc initGameMain() =
  doorlink1Ad = cast[ptr UncheckedArray[byte]](addr(level.doorlinks1[0]))
  doorlink2Ad = cast[ptr UncheckedArray[byte]](addr(level.doorlinks2[0]))
  discard prandom(1)
  if graphicsMode == ord(gmMcgaVga):
    # Guard palettes
    guardPalettes = cast[ptr byte](loadFromOpendatsAlloc(10, "bin", nil, nil))
    # (blood, hurt flash) #E00030 = red
    setPal(12, 0x38, 0x00, 0x0C)
    # (palace wall pattern) #C09850 = light brown
    setPal(6, 0x30, 0x26, 0x14)

    # Level color variations (1.3)
    levelVarPalettes = cast[ptr byte](loadFromOpendatsAlloc(20, "bin", nil, nil))

  # PRINCE.DAT: sword
  chtabAddrs[ord(idChtab0Sword)] = loadSpritesFromFile(700, 1 shl 2, 1)
  # PRINCE.DAT: flame, sword on floor, potion
  chtabAddrs[ord(idChtab1Flameswordpotion)] = loadSpritesFromFile(150, 1 shl 3, 1)
  closeDat(dathandle)

  when(USE_LIGHTING):
    initLighting()

  loadAllSounds()

  hofRead()
  showSplash()
  startGame()

var
  # data:02C2
  firstStart: word = 1
  # data:4C38
  setjmpBuf: JmpBuf
# seg000:0358
proc startGame() =
  when(USE_COPYPROT):
    var
      whichEntry, pos: word
      entryUsed: array[40, word]
      lettsUsed: array[26, byte]

  # Prevent filling of stack.
  # start_game is called from many places to restart the game, for example:
  # process_key, play_frame, draw_game_frame, play_level, control_kid,
  # end_sequence, expired
  if (firstStart) != 0:
    firstStart = 0
    discard setjmp(setjmpBuf)
  else:
    drawRect(getAddress(screenRect), ord(color0Black))
    showQuotes()
    clearScreenAndSounds()
    longjmp(setjmpBuf, -1)

  releaseTitleImages()
  freeOptsndChtab()
  when(USE_COPYPROT):
    copyprotPlac = prandom(13)
    for pos in 0..<14:
      do_while((entryUsed[whichEntry] or word(lettsUsed[byte(copyprotLetter[
            whichEntry]) - byte('A')])) != 0):
        if pos == int32(copyprotPlac):
          whichEntry = prandom(39)
          copyprotIdx = whichEntry
        else:
          whichEntry = prandom(39)
      cplevelEntr[pos] = whichEntry
      entryUsed[whichEntry] = 1
      lettsUsed[byte(copyprotLetter[whichEntry]) - byte('A')] = 1

  if (custom.skipTitle) != 0:
    # CusPop option: skip the title sequence (level loads instantly)
    var
      levelNumber: int32 = if startLevel >= 0:
          int32(startLevel)
        else:
          int32(custom.firstLevel)
    initGame(levelNumber)
    return

  if startLevel < 0:
    showTitle()
  else:
    initGame(startLevel)

when(USE_QUICKSAVE):
  # All these functions return true on success, false otherwise.
  var
    quickFp: File

  type
    ProcessFuncType = proc(f: File, buffer: pointer, length: Natural): int

  template process(x: typed, processFunc: ProcessFuncType) =
    ok = ok and processFunc(quickFp, addr(x), sizeof(x))

  proc quickProcess(processFunc: ProcessFuncType): int =
    var
      ok: int = 1

    when (USE_DEBUG_CHEATS):
      # Don't load the level if the user holds either Shift key while pressing F9.
      if (debugCheatsEnabled != 0'u8) and (((keyStates[ord(SCANCODE_LSHIFT)] and
          ord(KEYSTATE_HELD)) != 0'u8) or ((keyStates[ord(SCANCODE_RSHIFT)] and
          ord(KEYSTATE_HELD)) != 0'u8)):
        setFilePos(quickFp, sizeof(level), fspCur)
      else:
        process(level, processFunc)
    else:
      process(level, processFunc)

    process(checkpoint, processFunc)
    process(upsideDown, processFunc)
    process(drawnRoom, processFunc)
    process(currentLevel, processFunc)
    process(nextLevel, processFunc)
    process(mobsCount, processFunc)
    process(mobs, processFunc)
    process(trobsCount, processFunc)
    process(trobs, processFunc)
    process(leveldoorOpen, processFunc)
    # process(exitRoomTimer, processFunc)
    # kid
    process(Kid, processFunc)
    process(hitpCurr, processFunc)
    process(hitpMax, processFunc)
    process(hitpBegLev, processFunc)
    process(grabTimer, processFunc)
    process(holdingSword, processFunc)
    process(unitedWithShadow, processFunc)
    process(haveSword, processFunc)
    #[process(ctrl1Forward, processFunc)
    process(ctrl1Backward, processFunc)
    process(ctrl1Up, processFunc)
    process(ctrl1Down, processFunc)
    process(ctrl1Shift2, processFunc)]#
    process(kidSwordStrike, processFunc)
    process(pickupObjType, processFunc)
    process(offguard, processFunc)
    # guard
    process(Guard, processFunc)
    process(Char, processFunc)
    process(Opp, processFunc)
    process(guardhpCurr, processFunc)
    process(guardhpMax, processFunc)
    process(demoIndex, processFunc)
    process(demoTime, processFunc)
    process(currGuardColor, processFunc)
    process(guardNoticeTimer, processFunc)
    process(guardSkill, processFunc)
    process(shadowInitialized, processFunc)
    process(guardRefrac, processFunc)
    process(justblocked, processFunc)
    process(droppedout, processFunc)
    # collision
    process(currRowCollRoom, processFunc)
    process(currRowCollFlags, processFunc)
    process(belowRowCollRoom, processFunc)
    process(belowRowCollFlags, processFunc)
    process(aboveRowCollRoom, processFunc)
    process(aboveRowCollFlags, processFunc)
    process(prevCollisionRow, processFunc)
    # flash
    process(flashColor, processFunc)
    process(flashTime, processFunc)
    # sounds
    process(needLevel1Music, processFunc)
    process(isScreaming, processFunc)
    process(isFeatherFall, processFunc)
    process(lastLooseSound, processFunc)
    # process(nextSound, processFunc)
    # process(currentSound, processFunc)
    # random
    process(randomSeed, processFunc)
    # remaining time
    process(remMin, processFunc)
    process(remTick, processFunc)
    # saved controls
    process(controlX, processFunc)
    process(controlY, processFunc)
    process(controlShift, processFunc)
    process(controlForward, processFunc)
    process(controlBackward, processFunc)
    process(controlUp, processFunc)
    process(controlDown, processFunc)
    process(controlShift2, processFunc)
    process(ctrl1Forward, processFunc)
    process(ctrl1Backward, processFunc)
    process(ctrl1Up, processFunc)
    process(ctrl1Down, processFunc)
    process(ctrl1Shift2, processFunc)
    # replay recording state
    when(USE_REPLAY):
      process(currTick, processFunc)

    when(USE_COLORED_TORCHES):
      process(torchColors, processFunc)

    process(isGuardNotice, processFunc)
    process(canGuardSeeKid, processFunc)

    return ok

  const
    quickFile: string = "QUICKSAVE.SAV"
    quickVersion: string = "V1.16b4"

  proc getQuickPath(): string =
    return getWritableFilePath(quickFile)

  proc quickSave(): int =
    var
      ok: int = 0
      path: string = getQuickPath()
    discard open(quickFp, path, fmWrite)
    if quickFp != nil:
      discard writeChars(quickFp, quickVersion, 0, sizeof(quickVersion))
      ok = quickProcess(writeBuffer)
      close(quickFp)
    else:
      perror("quickSave: fopen")
      echo "Tried to open for writing: ", path

    return ok

  proc restoreRoomAfterQUickLoad() =
    var
      temp1: int32 = int32(currGuardColor)
      temp2: int32 = int32(nextLevel)
    resetLevelUnusedFields(false)
    loadLevSpr(int32(currentLevel))
    currGuardColor = word(temp1)
    nextLevel = word(temp2)

    # feather fall can only get restored if the fix enabled
    if ((fixes.fixQuicksaveDuringFeather == 0'u8) and isFeatherFall > 0'u16):
      isFeatherFall = 0
      stopSounds()

    # needFullRedraw = 1
    differentRoom = 1
    # Show the room where the prince is, even if the player moved the view away from it (with the H,J,U,N keys).
    nextRoom = Kid.room
    drawnRoom = Kid.room
    loadRoomLinks()
    # isGuardNotice = 0 prevent guard turning around immediately

    # for falling
    drawGameFrame()

    # force HP redraw
    hitpDelta = 1
    guardhpDelta = 1
    # Don't draw guard HP if a previously viewed room (with the H,J,U,N keys) had a guard but the current room doesn't have one.
    if Guard.room != drawnRoom:
      # Like in clearChar
      Guard.direction = ord(dir56None)
      guardhpCurr = 0

    drawHp()
    loadkidAndOpp()
    # Get rid of "press button" message if kid was dead before quickload.
    textTimeTotal = 0
    textTimeRemaining = 0

    exitRoomTimer = 0

  proc quickLoad(): int =
    var
      ok: int = 0
      path = getQuickPath()
    discard open(quickFp, path, fmRead)
    if quickFp != nil:
      # check quicksave version is compatible
      var
        quickControl: string = newString(quickVersion.len)
      discard readChars(quickFp, quickControl)
      if quickControl != quickVersion:
        close(quickFp)
        quickFp = nil
        return 0

      stopSounds()
      drawRect(getAddress(screenRect), ord(color0Black))
      updateScreen()
      # briefly display a black screen as a visual cue
      delayTicks(5)

      when(USE_QUICKLOAD_PENALTY):
        let
          oldRemMin = remMin
          oldRemTick = remTick

      ok = quickProcess(readBuffer)
      close(quickFp)
      quickFp = nil

      restoreRoomAfterQUickLoad()
      updateScreen()

      when(USE_QUICKLOAD_PENALTY):
        # Subtract one minute from the remaining time (if it is above 5 minutes)
        if (enableQuicksavePenalty != 0) and (currentLevel <
            custom.victoryStopsTimeLevel or (currentLevel ==
            custom.victoryStopsTimeLevel and levelDoorOpen < 2)):
          var
            ticksElapsed: int32 = 720 * int32(remMin - oldRemMin) + int32(
                remTick - oldRemTick)
          # don't restore time at all if the elapsed time is between 0 and 1 minutes
          if ticksElapsed > 0 and ticksElapsed < 720:
            remMin = oldRemMin
            remTick = oldRemTick
          else:
            if remMin == 6:
              remTick = 719
            if remMin > 5 or remMin < 0:
              dec(remMin)

    else:
      perror("quickLoad: fopen")
      echo "Tried to open for reading: ", path
    return ok

  proc checkQuickOp() =
    if enableQuicksave == 0:
      return
    if (needQuickSave) != 0:
      if (not(isFeatherFall != 0) or fixes.fixQuicksaveDuringFeather != 0) and (
          quickSave() != 0):
        displayTextBottom("QUICKSAVE")
      else:
        displayTextBottom("NO QUICKSAVE")

      needQuickSave = 0
      textTimeTotal = 24
      textTimeRemaining = 24
    if (needQuickLoad) != 0:
      if (quickLoad()) != 0:
        displayTextBottom("QUICKLOAD")
      else:
        displayTextBottom("NO QUICKLOAD")
      needQuickLoad = 0
      textTimeTotal = 24
      textTimeRemaining = 24

proc tempShiftReleaseCallback(interval: uint32,
    param: pointer): uint32 {.cdecl.} =
  let
    state = getKeyboardState(nil)
  if (state[ord(SDL_SCANCODE_LSHIFT)]) != 0:
    keyStates[ord(SDL_SCANCODE_LSHIFT)] = keyStates[ord(SDL_SCANCODE_LSHIFT)] or
        ord(KEYSTATE_HELD) or ord(KEYSTATE_HELD_NEW)
  if (state[ord(SDL_SCANCODE_RSHIFT)]) != 0:
    keyStates[ord(SDL_SCANCODE_RSHIFT)] = keyStates[ord(SDL_SCANCODE_RSHIFT)] or
        ord(KEYSTATE_HELD) or ord(KEYSTATE_HELD_NEW)
  return 0

proc processKey(): int32 =
  var
    answerText: string = ""
    key: int = keyTestQuit()
    needShowText: word = 0

  when(USE_MENU):
    if (isPaused and word(isMenuShown)) != 0:
      key = keyTestPausedMenu(key)
      if key == 0:
        return 0

  if startLevel < 0:
    if (key or ord(controlShift)) != 0:
      when(USE_QUICKSAVE):
        if key == ord(SDL_SCANCODE_F9):
          needQuickLoad = 1
      when(USE_REPLAY):
        if key == ord(SDL_SCANCODE_TAB) or (needStartReplay) != 0:
          startReplay()
        elif key == (ord(SDL_SCANCODE_TAB) or ord(WITH_CTRL)):
          startLevel = int16(custom.firstLevel)
          startRecording()
        elif key == (ord(SDL_SCANCODE_L) or ord(WITH_CTRL)):
          if loadGame() == 0:
            return 0
        else:
          startLevel = int16(custom.firstLevel)
      else:
        if key == (ord(SDL_SCANCODE_L) or ord(WITH_CTRL)):
          if loadGame() == 0:
            return 0
        else:
          startLevel = int16(custom.firstLevel)
      drawRect(getAddress(screenRect), ord(color0Black))
      when(USE_FADE):
        if (isGlobalFading) != 0:
          fadePaletteBuffer.procRestoreFree(fadePaletteBuffer)
          isGlobalFading = 0
      startGame()
  # If the kid died, enter or shift will restart the level
  if remMin != 0 and Kid.alive > 5 and ((controlShift != CONTROL_RELEASED) or
      key == ord(SDL_SCANCODE_RETURN)):
    key = ord(SDL_SCANCODE_A) or ord(WITH_CTRL)

  when(USE_REPLAY):
    if (recording) != 0:
      keyPressWhileRecording(addr(key))
    elif (replaying) != 0:
      keyPressWhileReplaying(addr(key))
  if key == 0:
    return 0
  if (isKeyboardMode) != 0:
    clearKbdBuf()

  case key
  of ord(SDL_SCANCODE_ESCAPE), ord(SDL_SCANCODE_ESCAPE) or ord(WITH_SHIFT):
    isPaused = 1
    when(USE_MENU):
      if (enablePauseMenu != 0) and (isCutscene == 0) and not(isEndingSequence):
        isMenuShown = 1
  of ord(SDL_SCANCODE_BACKSPACE):
    when(USE_MENU):
      if (isCutscene == 0) and not(isEndingSequence):
        isPaused = 1
        isMenuShown = 1
    else:
      discard
  of ord(SDL_SCANCODE_SPACE):
    isShowTime = 1
  of ord(SDL_SCANCODE_A) or ord(WITH_CTRL):
    if currentLevel != 15:
      stopSounds()
      isRestartLevel = 1
  of ord(SDL_SCANCODE_G) or ord(WITH_CTRL):
    # CusPoP: first and last level where saving is allowed
    if currentLevel >= custom.savingAllowedFirstLevel and currentLevel <=
        custom.savingAllowedLastLevel:
      saveGame()
  of ord(SDL_SCANCODE_J) or ord(WITH_CTRL):
    if ((soundFlags and ord(sfDigi)) != 0 and soundMode == ord(smTandy)):
      answerText = "JOYSTICK UNAVAILABLE"
    else:
      if setJoyMode() == 0:
        answerText = "JOYSTICK MODE"
      else:
        answerText = "JOYSTICK NOT FOUND"
    needShowText = 1
  of ord(SDL_SCANCODE_K) or ord(WITH_CTRL):
    answerText = "KEYBOARD MODE"
    isJoystMode = 0
    isKeyboardMode = 1
    needShowText = 1
  of ord(SDL_SCANCODE_R) or ord(WITH_CTRL):
    startLevel = -1
    when(USE_MENU):
      if (isMenuShown) != 0:
        # Do necessary cleanup.
        menuWasClosed()

    startGame()
  of ord(SDL_SCANCODE_S) or ord(WITH_CTRL):
    turnSoundOnOff(byte(not(isSoundOn)) * 15'u8)
    answerText = "SOUND OFF"
    if (isSoundOn) != 0:
      answerText = "SOUND ON"
    needShowText = 1
  of ord(SDL_SCANCODE_V) or ord(WITH_CTRL):
    answerText = "nimPoP v" & NIMPOP_VERSION
    needShowText = 1
  of ord(SDL_SCANCODE_C) or ord(WITH_CTRL):
    var
      verc, verl: SDL_Version
    getVersion(verc)
    getVersion(verl)
    let
      vcmajor: uint8 = verc.major
      vcminor: uint8 = verc.minor
      vcpatch: uint8 = verc.patch
      vlmajor: uint8 = verl.major
      vlminor: uint8 = verl.minor
      vlpatch: uint8 = verl.patch
    answerText = "SDL COMPv" & $(vcmajor) & "." & $(vcminor) & "." & $(
        vcpatch) & "LINK v" & $(vlmajor) & "." & $(vlminor) & "." & $(vlpatch)
    needShowText = 1
  of ord(SDL_SCANCODE_L) or ord(WITH_SHIFT):
    if currentLevel < custom.shiftLAllowedUntilLevel or (cheatsEnabled != 0):
      # if shift is not released within the delay, the cutscene is skipped
      let
        delay: uint32 = 250
      keyStates[ord(SDL_SCANCODE_LSHIFT)] = 0
      keyStates[ord(SDL_SCANCODE_RSHIFT)] = 0
      var
        timer: TimerID
      timer = addTimer(delay, tempShiftReleaseCallback, nil)
      if timer == 0:
        sdlperror("SDL_AddTimer")
        quitPoP(1)
      if currentLevel == 14:
        nextLevel = 1
      else:
        if currentLevel == 15 and (cheatsEnabled != 0):
          when(USE_COPYPROT):
            if (enableCopyprot) != 0:
              nextLevel = custom.copyprotLevel
              # Note: C code uses -1, which is equal to 65355 for uint16_t
              # see here: https://stackoverflow.com/questions/2760502/question-about-c-behaviour-for-unsigned-integer-underflow/2760612#2760612
              custom.copyprotLevel = high(typeof(custom.copyprotLevel))
          else:
            discard
        else:
          nextLevel = currentLevel + 1
          if (cheatsEnabled == 0) and word(remMin) >
              custom.shiftLReducedMinutes:
            remMin = int16(custom.shiftLReducedMinutes)
            remTick = custom.shiftLReducedTicks
      stopSounds()
  of ord(SDL_SCANCODE_F6), ord(SDL_SCANCODE_F6) or ord(WITH_SHIFT):
    when(USE_QUICKSAVE):
      if Kid.alive < 0:
        needQuickSave = 1
    else:
      discard
  of ord(SDL_SCANCODE_F9), ord(SDL_SCANCODE_F9) or ord(WITH_SHIFT):
    when(USE_QUICKSAVE):
      needQuickLoad = 1
    else:
      discard
  of ord(SDL_SCANCODE_TAB) or ord(WITH_CTRL), ord(SDL_SCANCODE_TAB) or ord(
      WITH_CTRL) or ord(WITH_SHIFT):
    when(USE_REPLAY and USE_QUICKSAVE):
      if (recording) != 0:
        stopRecording()
      else:
        startRecording()
  else:
    discard

  if (cheatsEnabled) != 0:
    case key
    of ord(SDL_SCANCODE_C):
      answerText = "S" & $(drawnRoom) & " L" & $(roomL) & " R" & $(roomR) &
          " A" & $(roomA) & " B" & $(roomB)
      needShowText = 1
    of ord(SDL_SCANCODE_C) or ord(WITH_SHIFT):
      answerText = "AL" & $(roomAL) & " AR" & $(roomAR) & " BL" & $(roomBL) &
          " BR" & $(roomBR)
      needShowText = 1
    of ord(SDL_SCANCODE_KP_MINUS): # '-' --> subtract time cheat
      when(ALLOW_INFINITE_TIME):
        if remMin > 1:
          dec(remMin)
        elif remMin < -1:
          inc(remMin)
        elif remMin == -1:
          remTick = 720
      else:
        if remMin > 1:
          dec(remMin)
      textTimeTotal = 0
      textTimeRemaining = 0
      isShowTime = 1
    of ord(SDL_SCANCODE_KP_PLUS): # '+' --> add time cheat
      when(ALLOW_INFINITE_TIME):
        if remMin > low(int16):
          dec(remMin)
        else:
          inc(remMin)
      else:
        inc(remMin)

      textTimeTotal = 0
      textTimeRemaining = 0
      isShowTime = 1
    of ord(SDL_SCANCODE_R): # R --> revive kid cheat
      if Kid.alive > 0:
        resurrectTime = 20
        Kid.alive = -1
        eraseBottomText(1)
    of ord(SDL_SCANCODE_K): # K --> kill guard cheat
      if Guard.charid != byte(charid4Skeleton):
        # CAUTION: DANGER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! was -guardhpCurr
        guardhpDelta = - int16(guardhpCurr)
        Guard.alive = 0
    of ord(SDL_SCANCODE_I) or ord(WITH_SHIFT): # shift+I --> invert cheat
      toggleUpside()
    of ord(SDL_SCANCODE_W) or ord(WITH_SHIFT): # shift+W --> feather fall cheat
      featherFall()
    of ord(SDL_SCANCODE_H): # H --> view room to the left
      drawGuardHp(0, 10)
      nextRoom = roomL
    of ord(SDL_SCANCODE_J): # J --> view room to the right
      drawGuardHp(0, 10)
      nextRoom = roomR
    of ord(SDL_SCANCODE_U): # U --> view room above
      drawGuardHp(0, 10)
      nextRoom = roomA
    of ord(SDL_SCANCODE_N): # N --> view room below
      drawGuardHp(0, 10)
      nextRoom = roomB
    of ord(SDL_SCANCODE_B) or ord(WITH_CTRL):
      drawGuardHp(0, 10)
      nextRoom = Kid.room
    of ord(SDL_SCANCODE_B) or ord(WITH_SHIFT):
      isBlindMode = not(isBlindMode)
      if (isBlindMode) != 0:
        drawRect(getAddress(rectTop), ord(color0Black))
        discard
      else:
        needFullRedraw = 1
    of ord(SDL_SCANCODE_S) or ord(WITH_SHIFT):
      if hitpCurr != hitpMax:
        playSound(sound33SmallPotion)
        hitpDelta = 1
        flashColor = 4 # red
        flashTime = 2
    of ord(SDL_SCANCODE_T) or ord(WITH_SHIFT):
      playSound(sound30BigPotion)
      flashColor = 4 # red
      flashTime = 4
      addLife()
    of ord(SDL_SCANCODE_T):
      when(USE_DEBUG_CHEATS):
        isTimerDisplayed = 1 - isTimerDisplayed
      else:
        discard
    of ord(SDL_SCANCODE_F):
      when(USE_DEBUG_CHEATS):
        if (fixes.fixQuicksaveDuringFeather != 0'u8):
          isFeatherTimerDisplayed = 1 - isFeatherTimerDisplayed # toggle
        else:
          isFeatherTimerDisplayed = 0
      else:
        discard
    else:
      discard

  if (needShowText) != 0:
    displayTextBottom(answerText)
    textTimeTotal = 24
    textTimeRemaining = 24

  return 1

# seg000:08EB
proc playFrame() =
  # play feather fall music if there is more than 1 second of feather fall left
  if ((fixes.fixQuicksaveDuringFeather != 0'u8) and (isFeatherFall >=
      10'u16) and not(checkSoundPlaying() != 0)):
    playSound(sound39LowWeight)

  doMobs()
  processTrobs()
  checkSkel()
  checkCanGuardSeeKid()
  # if level is restarted, return immediately
  if (playKidFrame()) != 0:
    return
  playGuardFrame()
  if resurrectTime == 0:
    checkSwordHurting()
    checkSwordHurt()
  checkSwordVsSword()
  doDeltaHp()
  exitRoom()
  checkTheEnd()
  checkGuardFallout()
  if currentLevel == 0:
    # Special event: level 0 running exit
    if word(Kid.room) == custom.demoEndRoom:
      drawRect(getAddress(screenRect), ord(color0Black))
      startLevel = -1
      needQuotes = 1
      startGame()
  elif currentLevel == custom.fallingExitLevel:
    # Special event: level 6 falling exit
    if roomleaveResult == -2:
      # Note: C code uses -1, which is equal to 255 for uint8_t
      Kid.y = high(typeof(Kid.y))
      stopSounds()
      inc(nextLevel)
  elif custom.tblSeamlessExit[currentLevel] >= 0:
    # Special event: level 12 running exit
    if int(Kid.room) == int(custom.tblSeamlessExit[currentLevel]):
      inc(nextLevel)
      # Sounds must be stopped, because playLevel2() check that nextLevel only
      # if there are no sounds playing.
      stopSounds()
      seamless = 1
  showTime()
  # expiring doesn't count on Jaffar/princess level
  if currentLevel < 13 and remMin == 0:
    expired()

# seg000:09B6
proc drawGameFrame() =
  if (needFullRedraw) != 0:
    redrawScreen(0)
    needFullRedraw = 0
  else:
    if (differentRoom) != 0:
      drawnRoom = nextRoom
      if (custom.tblLevelType[currentLevel]) != 0:
        genPalaceWallColors()
      redrawScreen(1)
    else:
      if (needRedrawBecauseFlipped) != 0:
        needRedrawBecauseFlipped = 0
        redrawScreen(0)
      else:
        for i in 0..<tableCounts.len:
          tableCounts[i] = 0
        drawMoving()
        drawTables()
        if (isBlindMode) != 0:
          drawRect(getAddress(rectTop), ord(color0Black))
        if (upsideDown) != 0:
          flipScreen(offscreenSurface)
        while drectsCount > 0:
          dec(drectsCount)
          copyScreenRect(drects[drectsCount].addr)
        if (upsideDown) != 0:
          flipScreen(offscreenSurface)
        drectsCount = 0

  playNextSound()
  # Note: texts are identified by their total time!
  if textTimeRemaining == 1:
    # If the text's is about to expire:
    if textTimeTotal == 36 or textTimeTotal == 288:
      # 36: died on demo/potions level
      # 288: press button to continue
      # In this case restart the game.
      startLevel = -1
      needQuotes = 1
      when(USE_REPLAY):
        if (recording) != 0:
          stopRecording()
        if (replaying) != 0:
          endReplay()

      startGame()
    else:
      # Otherwise, just clear it.
      eraseBottomText(1)
  else:
    if textTimeRemaining != 0 and textTimeTotal != 1188:
      # 1188: potions level (page/line/word) -- this one does not disappear
      dec(textTimeRemaining)
      if textTimeTotal == 288 and textTimeRemaining < 72:
        # 288: press button to continue
        # Blink the message
        var
          blinkFrame: int16
        blinkFrame = int16(textTimeRemaining mod 2)
        if blinkFrame > 3:
          eraseBottomText(0)
        else:
          if blinkFrame == 3:
            displayTextBottom("Press Button to Continue")
            # press button blink
            playSoundFromBuffer(soundPointers[int8(sound38Blink)])

proc animTileModif() =
  for tilepos in 0'u16..<30'u16:
    case getCurrTile(int16(tilepos))
    of tiles10Potion:
      startAnimPotion(int16(drawnRoom), int16(tilepos))
    of tiles19Torch, tiles30TorchWithDebris:
      startAnimTorch(int16(drawnRoom), int16(tilepos))
    of tiles22Sword:
      startAnimSword(int16(drawnRoom), int16(tilepos))
    else:
      discard

  # Animate torches in the rightmost column of the left-side room as well,
  # because they are visible in the current room.
  for row in 0..2:
    case getTile(int32(roomL), 9, int32(row))
    of tiles19Torch, tiles30TorchWithDebris:
      startAnimTorch(int16(roomL), int16(row * 10 + 9))
    else:
      discard

# seg000:0B72
proc loadSounds(minSound, maxSound: int32) =
  var
    ibmDat: DatType = nil
    digi1Dat: DatType = nil
    digi3Dat: DatType = nil
    midiDat: DatType = nil
  ibmDat = openDat("IBM_SND1.DAT", 0)
  if (soundFlags and ord(sfDigi)) != 0:
    digi1Dat = openDat("DIGISND1.DAT", 0)
    digi3Dat = openDat("DIGISND3.DAT", 0)
  if (soundFlags and ord(sfMidi)) != 0:
    midiDat = openDat("MIDISND1.DAT", 0)

  loadSoundNames()

  for current in minSound..maxSound:
    if soundPointers[current] != nil:
      continue
    soundPointers[current] = loadSound(current)

  if midiDat != nil:
    closeDat(midiDat)
  if digi1Dat != nil:
    closeDat(digi1Dat)
  if digi3Dat != nil:
    closeDat(digi3Dat)
  closeDat(ibmDat)

# seg000:0C5E
proc loadOptSounds(first, last: int32) =
  # stub
  var
    ibmDat: DatType = nil
    digiDat: DatType = nil
    midiDat: DatType = nil
  ibmDat = openDat("IBM_SND2.DAT", 0)
  if (soundFlags and ord(sfDigi)) != 0:
    digiDat = openDat("DIGISND2.DAT", 0)
  if (soundFlags and ord(sfMidi)) != 0:
    midiDat = openDat("MIDISND2.DAT", 0)
  for current in first..last:
    # we don't free sounds, so load only once
    if soundPointers[current] != nil:
      continue
    soundPointers[current] = loadSound(current)
  if midiDat != nil:
    closeDat(midiDat)
  if digiDat != nil:
    closeDat(digiDat)
  closeDat(ibmDat)

let
  # data:03BA
  tblGuardDat: array[5, string] = ["GUARD.DAT", "FAT.DAT", "SKEL.DAT",
      "VIZIER.DAT", "SHADOW.DAT"]
  # data:03C4
  tblEnvirGr: array[6, string] = ["", "C", "C", "E", "E", "V"]
  # data:03D0
  tblEnvirKi: array[2, string] = ["DUNGEON", "PALACE"]
# seg000:03D0
proc loadLevSpr(level: int32) =
  var
    dathandle: DatType = nil
    guardtype: int16
    filename: string
  currentLevel = word(level)
  nextLevel = word(level)
  drawRect(getAddress(screenRect), ord(color0Black))
  freeOptsndChtab()
  filename = $(tblEnvirGr[graphicsMode]) & $(tblEnvirKi[custom.tblLevelType[
      currentLevel]]) & ".DAT"
  loadChtabFromFile(ord(idChtab6Environment), 200, filename, 1 shl 5)
  loadMoreOptGraf(filename)
  guardtype = int16(custom.tblLevelType[currentLevel])
  if guardtype != -1:
    if guardtype == 0:
      if custom.tblLevelType[currentLevel] != 0:
        dathandle = openDat("GUARD1.DAT", int32('G'))
      else:
        dathandle = openDat("GUARD2.DAT", int32('G'))
    loadChtabFromFile(ord(idChtab5Guard), 750, tblGuardDat[guardtype], 1 shl 8)
    if dathandle != nil:
      closeDat(dathandle)
  currGuardColor = 0
  loadChtabFromFile(ord(idChtab7Environmentwall), 360, filename, 1 shl 6)

  # Level colors (1.3)
  if graphicsMode == ord(gmMcgaVga) and levelVarPalettes != nil:
    var
      levelColor: int32 = int32(custom.tblLevelColor[currentLevel])
    if levelColor != 0:
      var
        envPal: ptr byte = cast[ptr byte](cast[int](levelVarPalettes) + 0x30 * (
            levelColor - 1))
        wallPal: ptr byte = cast[ptr byte](cast[int](envPal) + 0x30 *
            int(custom.tblLevelType[currentLevel]))
      setPalArr(0x50, 0x10, cast[ptr RgbType](envPal))
      setPalArr(0x60, 0x10, cast[ptr RgbType](wallPal))
      setChtabPalette(chtabAddrs[int8(idChtab6Environment)], envPal, 0x10)
      setChtabPalette(chtabAddrs[int8(idChtab7Environmentwall)], wallPal, 0x10)
  # skel alive
  loadOptSounds(44, 44)
  # mirror
  loadOptSounds(45, 45)
  # something chopped, chomper
  loadOptSounds(46, 47)
  # something spiked, spikes
  loadOptSounds(48, 49)

# seg000:0E6C
proc loadLevel() =
  var
    dathandle: DatType = openDat("LEVELS.DAT", 0)
  discard loadFromOpendatsToArea(int32(currentLevel + 2000), addr(level),
      int32(sizeof(level)), "bin")
  closeDat(dathandle)

  alterModsAllrm()
  # added
  resetLevelUnusedFields(true)

proc resetLevelUnusedFields(loadingCleanLevel: bool) =
  # Entirely unused fields in the level format: reset to zero for now
  # They can be repurposed to add new stuff to the level format in the future
  zeroMem(addr(level.roomxs[0]), level.roomxs.len * sizeof(byte))
  zeroMem(addr(level.roomys[0]), level.roomys.len * sizeof(byte))
  zeroMem(addr(level.fill1[0]), level.fill1.len * sizeof(byte))
  zeroMem(addr(level.fill2[0]), level.fill2.len * sizeof(byte))
  zeroMem(addr(level.fill3[0]), level.fill3.len * sizeof(byte))

  if level.usedRooms > ROOM_COUNT:
    level.usedRooms = ROOM_COUNT
  # For these fields, only use the bits that are actually used, and set the rest
  # to zero. Good for repurposing the unused bits in the future.
  for i in 0'u32..<level.usedRooms:
    # 4 bits in use
    level.guardsSkill[i] = byte(level.guardsSkill[i] and 0x0F)

  # In savestates, additional information may be stored (e.g. remmebered guard
  # hp) - should not reset this then!
  if loadingCleanLevel:
    for i in 0'u32..<level.usedRooms:
      level.guardsColor[i] = byte(level.guardsColor[i] and 0x0F)

# seg000:0EA8
proc playKidFrame(): int32 =
  loadkidAndOpp()
  loadFramDetCol()
  checkKilledShadow()
  playKid()
  if (upsideDown != 0) and Char.alive >= 0:
    upsideDown = 0
    needRedrawBecauseFlipped = 1
  if (isRestartLevel) != 0:
    return 1
  if Char.room != 0:
    playSeq()
    fallAccel()
    fallSpeed()
    loadFrameToObj()
    loadFramDetCol()
    setCharCollision()
    bumpIntoOpponent()
    checkCollisions()
    checkBumped()
    checkGatePush()
    checkAction()
    checkPress()
    checkSpikeBelow()
    if resurrectTime == 0:
      checkSpiked()
      checkChompedKid()
    checkKnock()
  savekid()
  return 0

# seg000:0F48
proc playGuardFrame() =
  if Guard.direction != ord(dir56None):
    loadshadAndOpp()
    loadFramDetCol()
    checkKilledShadow()
    playGuard()
    if word(Char.room) == drawnRoom:
      playSeq()
      if Char.x >= 44 and Char.x < 211:
        fallAccel();
        fallSpeed();
        loadFrameToObj();
        loadFramDetCol();
        setCharCollision();
        checkGuardBumped();
        checkAction();
        checkPress();
        checkSpikeBelow();
        checkSpiked();
        checkChompedGuard();
    saveshad()

# seg000:0FBD
proc checkTheEnd() =
  if nextRoom != 0 and nextRoom != drawnRoom:
    drawnRoom = nextRoom
    loadRoomLinks()
    if currentLevel == custom.winLevel and drawnRoom == custom.winRoom:
      when(USE_REPLAY):
        if (recording) != 0:
          stopRecording()
        if (replaying) != 0:
          endReplay()
      # Special event: end of game
      endSequence()
    differentRoom = 1
    loadKid()
    animTileModif()
    startChompers()
    checkFallFlo()
    checkShadow()

# seg000:1009
proc checkFallFlo() =
  # Special event: falling floors
  if currentLevel == custom.looseTilesLevel and drawnRoom ==
      custom.looseTilesRoom1 or drawnRoom == custom.looseTilesRoom2:
    currRoom = int16(roomA)
    getRoomAddress(currRoom)
    currTilepos = custom.looseTilesFirstTile
    while currTilepos <= custom.looseTilesLastTile:
      makeLooseFall(cast[byte](-((int32(prandom(0xFF)) and 0x0F))))
      inc(currTilepos)

proc getJoystickState(rawX, rawY: int32, axisState: var openArray[int32]) =
  # check if the X/Y position is within the 'dead zone' of the joystick
  var
    distSquared: int32 = rawX * rawX + rawY * rawY
  # FIXED: Left jump (top-left) didn't work on some gamepads.
  # On some gamepads, raw_x = raw_y = -32768 in the top-left corner.
  # In this case, dist_squared is calculated as -32768 * -32768 + -32768 * -32768 = 2147483648.
  # But dist_squared is a 32-bit signed integer, which cannot store that number, so it overflows to -2147483648.
  # Therefore, dist_squared < joystick_threshold*joystick_threshold becomes true, and the game thinks the stick is centered.
  # To fix this, we cast both sides of the comparison to an unsigned 32-bit type.
  if dword(distSquared) < dword(joystickThreshold * joystickThreshold):
    axisState[0] = 0
    axisState[1] = 0
  else:
    var
      # angle of the joystick: 0 = right, >0 = downward, <0 upword
      angle: float64 = arctan2(float64(rawY), float64(rawX))
    if abs(angle) < (PI / 3):
      # 120 degree range facing right
      axisState[0] = 1
    elif abs(angle) < (2 * PI / 3):
      # 120 degree range facing left
      axisState[0] = -1
    else:
      # joystick is neutral horizontally, so the control should be released
      # however: prevent stop running if the Kid was already running / trying to do a running-jump
      # (this tweak makes it a bit easier to do (multiple) running jumps)
      if not(angle < 0 and Kid.action == ord(actions1RunJump)):
        axisState[0] = 0

    if angle < (-PI / 6) and angle > (-5 * PI / 6):
      # 120 degree range facing up
      axisState[1] = 1

    # down slightly less sensitive than up (prevent annoyance when your thumb
    # slips down a bit by accident)
    # (not sure if this adjustment is really necessary)
    elif angle > (35 * PI / 180) and angle < (145 * PI / 180):
      # 110 degree range facing down
      axisState[1] = 1
    else:
      # joystick is neutral vertically, so the control should be released
      # however: should prevent unintended standing up when attempting to crouch-hop
      if not((Kid.frame >= ord(frame108FallLand2) and Kid.frame <= ord(
          frame112StandUpFromCrouch3)) and angle > 0):
        # facing downward
        axisState[1] = 0

proc getJoystickStateHorOnly(rawX: int32, axisState: var openArray[int32]) =
  if rawX > joyStickThreshold:
    axisState[0] = 1
  elif rawX < -joystickThreshold:
    axisState[0] = -1
  else:
    axisState[0] = 0

  # disregard all vertical input from the joystick controls (only use Y and A
  # buttons or D-pad for up/down)
  axisState[1] = 0

# seg000:1051
proc readJoystControl() =
  var
    keyState: int32
    joyAxisPtr: ptr UncheckedArray[int32]

  if fixes.fixRegisterQuickInput != 0:
    keyState = ord(KEYSTATE_HELD) or ord(KEYSTATE_HELD_NEW)
    joyAxisPtr = cast[ptr UncheckedArray[int32]](addr(joyAxisMax[0]))
  else:
    keyState = ord(KEYSTATE_HELD)
    joyAxisPtr = cast[ptr UncheckedArray[int32]](addr(joyAxis[0]))
  if (joystickOnlyHorizontal) != 0:
    getJoystickStateHorOnly(joyAxisPtr[ord(SDL_CONTROLLER_AXIS_LEFTX)], joyLeftStickStates)
    getJoystickStateHorOnly(joyAxisPtr[ord(SDL_CONTROLLER_AXIS_RIGHTX)], joyRightStickStates)
  else:
    getJoystickState(joyAxisPtr[ord(SDL_CONTROLLER_AXIS_LEFTX)], joyAxisPtr[ord(
        SDL_CONTROLLER_AXIS_LEFTY)], joyLeftStickStates)
    getJoystickState(joyAxisPtr[ord(SDL_CONTROLLER_AXIS_RIGHTX)], joyAxisPtr[
        ord(SDL_CONTROLLER_AXIS_RIGHTY)], joyRightStickStates)

  if joyLeftStickStates[0] == -1 or joyRightStickStates[0] == -1 or
      (joyButtonStates[JOYINPUT_DPAD_LEFT] and keyState) != 0:
    controlX = CONTROL_HELD_LEFT

  if joyLeftStickStates[0] == 1 or joyRightStickStates[0] == 1 or (
      joyButtonStates[JOYINPUT_DPAD_RIGHT] and keyState) != 0:
    controlX = CONTROL_HELD_RIGHT

  if joyLeftStickStates[1] == -1 or joyRightStickStates[1] == -1 or ((
      joyButtonStates[JOYINPUT_DPAD_UP] or joyButtonStates[JOYINPUT_Y]) and
      keyState) != 0:
    controlY = CONTROL_HELD_UP

  if joyLeftStickStates[1] == 1 or joyRightStickStates[1] == 1 or ((
      joyButtonStates[JOYINPUT_DPAD_DOWN] or joyButtonStates[
      JOYINPUT_A]) and keyState) != 0:
    controlY = CONTROL_HELD_DOWN

  if (joyButtonStates[JOYINPUT_X] and keyState) != 0 or joyAxisPtr[ord(
      SDL_CONTROLLER_AXIS_TRIGGERLEFT)] > 8000 or joyAxisPtr[ord(
      SDL_CONTROLLER_AXIS_TRIGGERRIGHT)] > 8000:
    controlShift = CONTROL_HELD

# seg000:10EA
proc drawKidHp(currHp, maxHp: int16) =
  for drawnHpIndex in currHp..<maxHp:
    # empty HP
    discard method6BlitImgToScr(getImage(ord(idChtab2Kid), 217), drawnHpIndex *
        7, 194, ord(blitters0NoTransp))
  for drawnHpIndex in 0..<currHp:
    # full HP
    discard method6BlitImgToScr(getImage(ord(idChtab2Kid), 216), int32(
        drawnHpIndex * 7), 194, ord(blitters0NoTransp))

# seg000:1159
proc drawGuardHp(currHp, maxHp: int16) =
  var
    guardCharid: int16
  if isNil(chtabAddrs[ord(idChtab5Guard)]):
    return
  guardCharid = int16(Guard.charid)
  if guardCharid != ord(charid4Skeleton) and guardCharid != ord(
      charid24Mouse) and (guardCharid != ord(charid1Shadow) or currentLevel == 12):
    # shadow has HP only on level 12
    for drawnHpIndex in currHp..<maxHp:
      discard method6BlitImgToScr(chtabAddrs[ord(idChtab5Guard)].images[0],
          int32(314 - drawnHpIndex * 7), 194, ord(blitters9Black))
    for drawnHpIndex in 0..<currHp:
      discard method6BlitImgToScr(chtabAddrs[ord(idChtab5Guard)].images[0],
          int32(314 - drawnHpIndex * 7), 194, ord(blitters0NoTransp))

# seg000:11EC
proc addLife() =
  var
    hpmax = hitpMax
  inc(hpmax)
  # CusPop: set maximum number of hitpoints (max_hitp_allowed, default = 10)
  if hpmax > custom.maxHitpAllowed:
    hpmax = custom.maxHitpAllowed
  hitpMax = hpmax
  setHealthLife()

# seg000:1200
proc setHealthLife() =
  hitpDelta = int16(hitpMax - hitpCurr)

# seg000:120B
proc drawHp() =
  if (hitpDelta) != 0:
    drawKidHp(int16(hitpCurr), int16(hitpMax))
  if hitpCurr == 1 and currentLevel != 15:
    # blinking hitpoint
    if (remTick and 1) != 0:
      drawKidHp(1, 0)
    else:
      drawKidHp(0, 1)
  if (guardhpDelta) != 0:
    drawGuardHp(int16(guardhpCurr), int16(guardhpMax))
  if guardhpCurr == 1:
    if (remTick and 1) != 0:
      drawGuardHp(1, 0)
    else:
      drawGuardHp(0, 1)

# seg000:127B
proc doDeltaHp() =
  # level 12: if the shadow is hurt, Kis is also hurt
  if (Opp.charid == ord(charid1Shadow) and currentLevel == 12 and
      guardhpDelta != 0):
    hitpDelta = guardhpDelta
  hitpCurr = min(max(hitpCurr + cast[word](hitpDelta), 0), hitpMax)
  guardhpCurr = min(max(guardhpCurr + word(guardhpDelta), 0), guardhpMax)

var
  soundPrioTable = [
      0x14'u8, # sound_0_fell_to_death
    0x1E'u8,   # sound_1_falling
    0x23'u8,   # sound_2_tile_crashing
    0x66'u8,   # sound_3_button_pressed
    0x32'u8,   # sound_4_gate_closing
    0x37'u8,   # sound_5_gate_opening
    0x30'u8,   # sound_6_gate_closing_fast
    0x30'u8,   # sound_7_gate_stop
    0x4B'u8,   # sound_8_bumped
    0x50'u8,   # sound_9_grab
    0x0A'u8,   # sound_10_sword_vs_sword
    0x12'u8,   # sound_11_sword_moving
    0x0C'u8,   # sound_12_guard_hurt
    0x0B'u8,   # sound_13_kid_hurt
    0x69'u8,   # sound_14_leveldoor_closing
    0x6E'u8,   # sound_15_leveldoor_sliding
    0x73'u8,   # sound_16_medium_land
    0x78'u8,   # sound_17_soft_land
    0x7D'u8,   # sound_18_drink
    0x82'u8,   # sound_19_draw_sword
    0x91'u8,   # sound_20_loose_shake_1
    0x96'u8,   # sound_21_loose_shake_2
    0x9B'u8,   # sound_22_loose_shake_3
    0xA0'u8,   # sound_23_footstep
    0x01'u8,   # sound_24_death_regular
    0x01'u8,   # sound_25_presentation
    0x01'u8,   # sound_26_embrace
    0x01'u8,   # sound_27_cutscene_2_4_6_12
    0x01'u8,   # sound_28_death_in_fight
    0x13'u8,   # sound_29_meet_Jaffar
    0x01'u8,   # sound_30_big_potion
    0x01'u8,   # sound_31
    0x01'u8,   # sound_32_shadow_music
    0x01'u8,   # sound_33_small_potion
    0x01'u8,   # sound_34
    0x01'u8,   # sound_35_cutscene_8_9
    0x01'u8,   # sound_36_out_of_time
    0x01'u8,   # sound_37_victory
    0x01'u8,   # sound_38_blink
    0x00'u8,   # sound_39_low_weight
    0x01'u8,   # sound_40_cutscene_12_short_time
    0x01'u8,   # sound_41_end_level_music
    0x01'u8,   # sound_42
    0x01'u8,   # sound_43_victory_Jaffar
    0x87'u8,   # sound_44_skel_alive
    0x8C'u8,   # sound_45_jump_through_mirror
    0x0F'u8,   # sound_46_chomped
    0x10'u8,   # sound_47_chomper
    0x19'u8,   # sound_48_spiked
    0x16'u8,   # sound_49_spikes
    0x01'u8,   # sound_50_story_2_princess
    0x00'u8,   # sound_51_princess_door_opening
    0x01'u8,   # sound_52_story_4_Jaffar_leaves
    0x01'u8,   # sound_53_story_3_Jaffar_comes
    0x01'u8,   # sound_54_intro_music
    0x01'u8,   # sound_55_story_1_absence
    0x01'u8,   # sound_56_ending_music
    0x00'u8
  ]
  soundPcspeakerExists = [
      1'u8,    # sound_0_fell_to_death
    0'u8,      # sound_1_falling
    1'u8,      # sound_2_tile_crashing
    1'u8,      # sound_3_button_pressed
    1'u8,      # sound_4_gate_closing
    1'u8,      # sound_5_gate_opening
    1'u8,      # sound_6_gate_closing_fast
    1'u8,      # sound_7_gate_stop
    1'u8,      # sound_8_bumped
    1'u8,      # sound_9_grab
    1'u8,      # sound_10_sword_vs_sword
    0'u8,      # sound_11_sword_moving
    1'u8,      # sound_12_guard_hurt
    1'u8,      # sound_13_kid_hurt
    1'u8,      # sound_14_leveldoor_closing
    1'u8,      # sound_15_leveldoor_sliding
    1'u8,      # sound_16_medium_land
    1'u8,      # sound_17_soft_land
    1'u8,      # sound_18_drink
    0'u8,      # sound_19_draw_sword
    0'u8,      # sound_20_loose_shake_1
    0'u8,      # sound_21_loose_shake_2
    0'u8,      # sound_22_loose_shake_3
    1'u8,      # sound_23_footstep
    1'u8,      # sound_24_death_regular
    1'u8,      # sound_25_presentation
    1'u8,      # sound_26_embrace
    1'u8,      # sound_27_cutscene_2_4_6_12
    1'u8,      # sound_28_death_in_fight
    1'u8,      # sound_29_meet_Jaffar
    1'u8,      # sound_30_big_potion
    1'u8,      # sound_31
    1'u8,      # sound_32_shadow_music
    1'u8,      # sound_33_small_potion
    1'u8,      # sound_34
    1'u8,      # sound_35_cutscene_8_9
    1'u8,      # sound_36_out_of_time
    1'u8,      # sound_37_victory
    1'u8,      # sound_38_blink
    1'u8,      # sound_39_low_weight
    1'u8,      # sound_40_cutscene_12_short_time
    1'u8,      # sound_41_end_level_music
    1'u8,      # sound_42
    1'u8,      # sound_43_victory_Jaffar
    1'u8,      # sound_44_skel_alive
    1'u8,      # sound_45_jump_through_mirror
    1'u8,      # sound_46_chomped
    1'u8,      # sound_47_chomper
    1'u8,      # sound_48_spiked
    1'u8,      # sound_49_spikes
    1'u8,      # sound_50_story_2_princess
    1'u8,      # sound_51_princess_door_opening
    1'u8,      # sound_52_story_4_Jaffar_leaves
    1'u8,      # sound_53_story_3_Jaffar_comes
    1'u8,      # sound_54_intro_music
    1'u8,      # sound_55_story_1_absence
    1'u8,      # sound_56_ending_music
    0'u8
  ]

proc fixSoundPriorities() =
  # Change values to match those in PoP 1.3.

  # The "Spiked" sound didn't interrupt the normal spikes sound when the prince
  # ran into the spikes.
  soundInterruptible[ord(sound49Spikes)] = 1
  soundPrioTable[ord(sound48Spiked)] = 0x15 # moved above spikes

  # With PoP 1.3 sounds, the "guard hurt" sound didn't play when you hit a guard
  # directly after parrying.
  soundPrioTable[ord(sound10SwordVsSword)] = 0x0D # moved below hit_ser/hit_guard

# seg000:12C5
proc playSound(soundId: int) =
  if nextSound < 0 or soundPrioTable[soundId] <= soundPrioTable[nextSound]:
    if isNil(soundPointers[soundId]):
      return
    if soundPcspeakerExists[soundId] != 0 or soundPointers[soundId].`type` !=
        ord(soundSpeaker):
      nextSound = int16(soundId)

# seg000:1304
proc playNextSound() =
  if nextSound >= 0:
    if (checkSoundPlaying() == 0) or (soundInterruptible[currentSound] !=
        0 and soundPrioTable[nextSound] <= soundPrioTable[currentSound]):
      currentSound = word(nextSound)
      playSoundFromBuffer(soundPointers[currentSound])
  nextSound = -1

# seg000:1353
proc checkSwordVsSword() =
  if Kid.frame == 167 or Guard.frame == 167:
    # sword vs. sword
    playSound(sound10SwordVsSword)

# seg000:136A
proc loadChtabFromFile(chtabId, resource: int32, filename: string,
    paletteBits: int32) =
  var
    dathandle: DatType
  if chtabAddrs[chtabId] != nil:
    return
  dathandle = openDat(filename, int32('G'))
  chtabAddrs[chtabId] = loadSpritesFromFile(resource, paletteBits, 1)
  closeDat(dathandle)

# seg000:13BA
proc freeAllChtabsFrom(first: int32) =
  freePeels()
  for chtabId in first..<10:
    if chtabAddrs[chtabId] != nil:
      freeChtab(chtabAddrs[chtabId])
      chtabAddrs[chtabId] = nil

# seg009:12EF
proc loadOneOptgraf(chtabPtr: ChtabType, palPtr: var DatPalType, baseId,
    minIndex, maxIndex: int32) =
  for index in minIndex..maxIndex:
    var
      image: ImageType = loadImage(baseId + index + 1, palPtr)
    if image != nil:
      chtabPtr.images[index] = image

let
  optgrafMin: array[8, byte] = [
      0x01'u8, 0x1E'u8, 0x4B'u8, 0x4E'u8, 0x56'u8, 0x65'u8, 0x7F'u8, 0x0A'u8
    ]
  optgrafMax: array[8, byte] = [
      0x09'u8, 0x1F'u8, 0x4D'u8, 0x53'u8, 0x5B'u8, 0x7B'u8, 0x8F'u8, 0x0D'u8
    ]
# seg000:13FC
proc loadMoreOptGraf(filename: string) =
  # stub
  var
    dathandle: DatType = nil
    area: DatShplType
  for grafIndex in 0..<8:
    if isNil(dathandle):
      dathandle = openDat(filename, int32('G'))
      discard loadFromOpendatsToArea(200, addr(area), int32(sizeof(area)), "pal")
      area.palette.rowBits = 0x20
    loadOneOptgraf(chtabAddrs[ord(idChtab6Environment)], area.palette, 1200,
        int32(optgrafMin[grafIndex] - 1), int32(optgrafMax[grafIndex] - 1))
  if dathandle != nil:
    closeDat(dathandle)

# seg0000:148D
proc doPaused(): int32 =
  when(USE_REPLAY):
    if (replaying and skippingReplay) != 0:
      return 0
  var
    key: word = 0
  nextRoom = 0
  controlShift = CONTROL_RELEASED
  controlX = CONTROL_RELEASED
  controlY = CONTROL_RELEASED
  if (isJoystMode) != 0:
    readJoystControl()
  else:
    readKeybControl()
  key = word(processKey())
  if isEndingSequence and (isPaused != 0):
    # fix being able to pause the game during the ending sequence
    isPaused = 0
  if (isPaused) != 0:
    # feather fall gets interrupted by pause
    if ((fixes.fixQuicksaveDuringFeather != 0'u8) and isFeatherFall > 0'u16 and
        (checkSoundPlaying() != 0)):
      stopSounds()

    displayTextBottom("GAME PAUSED")
    when(USE_MENU):
      if (int(enablePauseMenu) or int(isMenuShown)) != 0:
        drawMenu()
        menuWasClosed()
      else:
        isPaused = 0
        # busy waiting?
        do_while(processKey() == 0):
          idle()
          delayTicks(1)
    else:
      isPaused = 0
      # busy waiting?
      do_while(processKey() == 0):
        idle()
        delayTicks(1)
    eraseBottomText(1)
  # As we processed input for current gameplay tick remove the bit flagging any button press as new
  for i in 0..<ord(SDL_NUM_SCANCODES):
    keyStates[i] = keyStates[i] and not(byte(KEYSTATE_HELD_NEW))
  return int32(key) or int32(controlShift)

# seg000:1500
proc readKeybControl() =
  let
    statesOr: byte = byte(KEYSTATE_HELD) or byte(KEYSTATE_HELD_NEW)
  if ((keyStates[ord(SDL_SCANCODE_UP)] or keyStates[ord(SDL_SCANCODE_HOME)] or
      keyStates[ord(SDL_SCANCODE_PAGEUP)] or keyStates[ord(
          SDL_SCANCODE_KP_8)] or
      keyStates[ord(SDL_SCANCODE_KP_7)] or keyStates[ord(
          SDL_SCANCODE_KP_9)]) and statesOr) != 0:
    controlY = CONTROL_HELD_UP
  elif ((keyStates[ord(SDL_SCANCODE_CLEAR)] or keyStates[ord(
      SDL_SCANCODE_DOWN)] or keyStates[ord(SDL_SCANCODE_KP_5)] or keyStates[ord(
          SDL_SCANCODE_KP_2)]) and statesOr) != 0:
    controlY = CONTROL_HELD_DOWN
  if ((keyStates[ord(SDL_SCANCODE_LEFT)] or keyStates[ord(SDL_SCANCODE_HOME)] or
      keyStates[ord(SDL_SCANCODE_KP_4)] or keyStates[ord(
          SDL_SCANCODE_KP_7)]) and statesOr) != 0:
    controlX = CONTROL_HELD_LEFT
  elif ((keyStates[ord(SDL_SCANCODE_RIGHT)] or keyStates[ord(
      SDL_SCANCODE_PAGEUP)] or keyStates[ord(SDL_SCANCODE_KP_6)] or keyStates[
          ord(SDL_SCANCODE_KP_9)]) and statesOr) != 0:
    controlX = CONTROL_HELD_RIGHT
  if ((keyStates[ord(SDL_SCANCODE_LSHIFT)] or keyStates[
      ord(SDL_SCANCODE_RSHIFT)]) and statesOr) != 0:
    controlShift = CONTROL_HELD
  else:
    controlShift = CONTROL_RELEASED

  when(USE_DEBUG_CHEATS):
    if (cheatsEnabled and debugCheatsEnabled) != 0:
      if (keyStates[ord(SDL_SCANCODE_RIGHTBRACKET)] and statesOr) != 0:
        inc(Char.x)
      elif (keyStates[ord(SDL_SCANCODE_LEFTBRACKET)] and statesOr) != 0:
        dec(Char.x)

# seg000:156D
proc copyScreenRect(sourceRectPtr: ptr RectType) =
  var
    targetRect: RectType
  if (upsideDown) != 0:
    targetRect = sourceRectPtr[]
    targetRect.top = SCREEN_GAMEPLAY_HEIGHT - sourceRectPtr.bottom
    targetRect.bottom = SCREEN_GAMEPLAY_HEIGHT - sourceRectPtr.top
  else:
    targetRect = sourceRectPtr[]

  method1BlitRect(onScreenSurface, offscreenSurface, targetRect.addr,
      targetRect.addr, 0)
  when(USE_LIGHTING):
    updateLighting(targetRect.addr)

# seg000:15E9
proc toggleUpside() =
  upsideDown = not(upsideDown)
  needRedrawBecauseFlipped = 1

# seg000:15F8
proc featherFall() =
  echo "slow fall started at: rem_min = ", remMin, ", rem_tick = ", remTick
  if (fixes.fixQuicksaveDuringFeather != 0'u8):
    # feather fall is treated as a timer
    isFeatherFall = uint16(FEATHER_FALL_LENGTH * getTicksPerSec(ord(timer1)))
  else:
    isFeatherFall = 1
  flashColor = 2 # green
  flashTime = 3
  stopSounds()
  playSound(sound39LowWeight) # low weight

# seg000:1618
proc parseGrmode(): int32 =
  # stub
  setGrMode(ord(gmMcgaVga))
  return ord(gmMcgaVga)

# seg000:172C
proc genPalaceWallColors() =
  var
    oldRandseed: dword
    prevColor: word
    colorBase: word

  oldRandseed = randomSeed
  randomSeed = drawnRoom
  discard prandom(1)
  for row in 0..<3:
    for subrow in 0..<4:
      if (subrow mod 2) != 0:
        colorBase = 0x61 # 0x61..0x64 in subrow 1 and 3
      else:
        colorBase = 0x66 # 0x66..0x69 in subrow 0 and 3
      # Note: C code uses -1, which is equal to 65355 for uint16_t
      prevColor = high(typeof(prevColor))
      for column in 0..10:
        var
          color: word
        do_while(color == prevColor):
          color = colorBase + prandom(3)
        palaceWallColors[44 * row + 11 * subrow + column] = byte(color)
        prevColor = color
  randomSeed = oldRandseed

# data:042E
var
  rectTiles: RectType = RectType(top: 106, left: 24, bottom: 195, right: 296)

# seg000:17E6
proc showTitle() =
  # main theme, story, princess door
  loadOptSounds(ord(sound50Story2Princess), ord(sound55Story1Absence))
  dontResetTime = 0
  if offscreenSurface != nil:
    freeSurface(offscreenSurface)
  offscreenSurface = makeOffscreenBuffer(getAddress(screenRect))
  loadTitleImages(1)
  currentTargetSurface = offscreenSurface
  # modified
  idle()
  discard doPaused()

  drawFullImage(TITLE_MAIN)
  # STUB
  fadeIn2(offscreenSurface, 0x1000)
  method1BlitRect(onscreenSurface, offscreenSurface, getAddress(screenRect),
      getAddress(screenRect), ord(blitters0NoTransp))
  currentSound = ord(sound54IntroMusic)
  playSoundFromBuffer(soundPointers[ord(sound54IntroMusic)])
  startTimer(ord(timer0), 0x82)
  drawFullImage(TITLE_PRESENTS)
  discard doWait(ord(timer0))

  startTimer(ord(timer0), 0xCD)
  method1BlitRect(onscreenSurface, offscreenSurface, rectTiles.addr,
      rectTiles.addr, ord(blitters0NoTransp))
  drawFullImage(TITLE_MAIN)
  discard doWait(ord(timer0))

  startTimer(ord(timer0), 0x41)
  method1BlitRect(onscreenSurface, offscreenSurface, rectTiles.addr,
      rectTiles.addr, ord(blitters0NoTransp))
  drawFullImage(TITLE_MAIN)
  drawFullImage(TITLE_GAME)
  discard doWait(ord(timer0))

  startTimer(ord(timer0), 0x10E)
  method1BlitRect(onscreenSurface, offscreenSurface, rectTiles.addr,
      rectTiles.addr, ord(blitters0NoTransp))
  drawFullImage(TITLE_MAIN)
  discard doWait(ord(timer0))

  startTimer(ord(timer0), 0xEB)
  method1BlitRect(onscreenSurface, offscreenSurface, rectTiles.addr,
      rectTiles.addr, ord(blitters0NoTransp))
  drawFullImage(TITLE_MAIN)
  drawFullImage(TITLE_POP)
  drawFullImage(TITLE_MECHNER)
  discard doWait(ord(timer0))

  method1BlitRect(onscreenSurface, offscreenSurface, rectTiles.addr,
      rectTiles.addr, ord(blitters0NoTransp))
  drawFullImage(STORY_FRAME)
  drawFullImage(STORY_ABSENCE)
  currentTargetSurface = onscreenSurface
  while checkSoundPlaying() != 0:
    idle()
    discard doPaused()
    delayTicks(1)

  # story 1: In the absence
  playSoundFromBuffer(soundPointers[ord(sound55Story1Absence)])
  transitionLtr()
  discard popWait(ord(timer0), 0x258)
  fadeOut2(0x800)
  releasetitleImages()

  loadIntro(0, pvScene, 0)

  loadTitleImages(1)
  currentTargetSurface = offscreenSurface
  drawFullImage(STORY_FRAME)
  drawFullImage(STORY_MARRY)
  fadeIn2(offscreenSurface, 0x800)
  drawFullImage(TITLE_MAIN)
  drawFullImage(TITLE_POP)
  drawFullImage(TITLE_MECHNER)
  while checkSoundPlaying() != 0:
    idle()
    discard doPaused()
    delayTicks(1)
  transitionLtr()
  discard popWait(ord(timer0), 0x78)
  drawFullImage(STORY_FRAME)
  drawFullImage(STORY_CREDITS)
  transitionLtr()
  discard popWait(ord(timer0), 0x168)
  if hofCount != 0:
    drawFullImage(STORY_FRAME)
    drawFullImage(HOF_POP)
    showHof()
    transitionLtr()
    discard popWait(ord(timer0), 0xF0)
  currentTargetSurface = onscreenSurface
  while checkSoundPlaying() != 0:
    idle()
    discard doPaused()
    delayTicks(1)
  fadeOut2(0x1800)
  freeSurface(offscreenSurface)
  offscreenSurface = nil
  releaseTitleImages()
  initGame(0)

var
  lastTransitionCounter: uint64

# seg000:1BB3
proc transitionLtr() =
  var
    rect: RectType = RectType(top: 0, left: 0, bottom: 200, right: 2)
    # Estimated transition fps based on the speed of the transition on an Apple
    # IIe.
    overshoot: int32 = 0
  when(USE_FAST_FORWARD):
    let
      transitionFps: int32 = 120 * audioSpeed
  else:
    let
      transitionFps: int32 = 120
  let
    countersPerFrame: uint64 = perfFrequency div uint64(transitionFps)
  lastTransitionCounter = getPerformanceCounter()
  for position in countup(0, 319, 2):
    method1BlitRect(onscreenSurface, offscreenSurface, rect.addr, rect.addr, 0)
    rect.left += 2
    rect.right += 2
    if overshoot > 0 and overshoot < 10:
      dec(overshoot)
      continue
    idle()
    # Add an appropriate delay until the next frame, so that the animation isn't instantaneous on fast CPUs.
    discard doPaused()
    while true:
      var
        currentCounter: uint64 = getPerformanceCounter()
        frametimesElapsed: int32 = int32((currentCounter div countersPerFrame) -
            (lastTransitionCounter div countersPerFrame))
      if frametimesElapsed > 0:
        overshoot = frametimesElapsed - 1
        lastTransitionCounter = currentCounter
        break # proceed to the next frame.
      else:
        delay(1)

# seg000:1C0F
proc releaseTitleImages() =
  if chtabTitle50 != nil:
    freeChtab(chtabTitle50)
    chtabTitle50 = nil
  if chtabTitle40 != nil:
    freeChtab(chtabTitle40)
    chtabTitle40 = nil

# seg000:1C3A
proc drawFullImage(id: FullImageId) =
  var
    decodedImage: ImageType
    mask: ImageType = nil
    xpos, ypos, blit: int32
  if id >= MAX_FULL_IMAGES:
    return
  if isNil(fullImage[ord(id)].chtab):
    return
  decodedImage = fullImage[ord(id)].chtab.images[ord(fullImage[ord(id)].id)]
  blit = int32(fullImage[ord(id)].blitter)
  xpos = fullImage[ord(id)].xpos
  ypos = fullImage[ord(id)].ypos

  case blit
  of ord(blittersWhite):
    blit = getTextColor(15, ord(color15Brightwhite), 0x800)
    # fall through
    discard method3BlitMono(decodedImage, xpos, ypos, ord(blitters0NoTransp),
        byte(blit))
  of ord(blitters10hTransp):
    if (graphicsMode == ord(gmCga) or graphicsMode == ord(gmHgaHerc)):
      discard
    else:
      mask = decodedImage
    drawImageTransp(decodedImage, mask, xpos, ypos)
    if (graphicsMode == ord(gmCga) or graphicsMode == ord(gmHgaHerc)):
      dealloc(mask)
  of ord(blitters0NoTransp):
    discard method6BlitImgToScr(decodedImage, xpos, ypos, blit)
  else:
    discard method3BlitMono(decodedImage, xpos, ypos, ord(blitters0NoTransp),
        byte(blit))

# seg000:1D2C
proc loadKidSprite() =
  loadChtabFromFile(ord(idChtab2Kid), 400, "KID.DAT", 1 shl 7)

const
  saveFile: string = "PRINCE.SAV"

proc getSavePath(customPathBuffer: var string, maxLen: int32): string =
  return getWritableFilePath(saveFile)

# seg000:1D45
proc saveGame() =
  var
    handle: File
    customSavePath: string
    savePath: string = getSavePath(customSavePath, POP_MAX_PATH)
  if open(handle, savePath, fmWrite):
    try:
      if writeBuffer(handle, addr(remMin), 2) != 2:
        raise newException(IOError, "could not write 'remMin' to " & savePath)
      if writeBuffer(handle, addr(remTick), 2) != 2:
        raise newException(IOError, "could not write 'remTick' to " & savePath)
      if writeBuffer(handle, addr(currentLevel), 2) != 2:
        raise newException(IOError, "could not read 'currentLevel' to " & savePath)
      if writeBuffer(handle, addr(hitpBegLev), 2) != 2:
        raise newException(IOError, "could not read 'hitpBegLev' to " & savePath)
      displayTextBottom("GAME SAVED")
    except IOError:
      let
        msg = getCurrentExceptionMsg()
      removeFile(savePath)
      displayTextBottom("UNABLE TO SAVE GAME")
      perror(cstring("Got IO exception with message " & msg))
      #playSoundFromBuffer(soundCantSave)
    except CatchableError:
      displayTextBottom("Unknown exception!")
      # reraise the unknown exception:
      raise
    finally:
      close(handle)
  else:
    perror("saveGame: fopen")
    echo "Tried to open for writing: ", savePath

  textTimeRemaining = 24

# seg000:1E38
proc loadGame(): int16 =
  var
    success: int16 = 0
    handle: File
    customSavePath: string
    savePath: string = getSavePath(customSavePath, POP_MAX_PATH)
  if open(handle, savePath, fmWrite):
    try:
      if readBuffer(handle, addr(remMin), 2) != 2:
        raise newException(IOError, "could not read 'remMin' from " & savePath)
      if readBuffer(handle, addr(remTick), 2) != 2:
        raise newException(IOError, "could not read 'remTick' from " & savePath)
      if readBuffer(handle, addr(startLevel), 2) != 2:
        raise newException(IOError, "could not read 'startLevel' from " & savePath)
      if readBuffer(handle, addr(hitpBegLev), 2) != 2:
        raise newException(IOError, "could not read 'hitpBegLev' from " & savePath)
      when(USE_COPYPROT):
        if (enableCopyprot != 0) and custom.copyprotLevel > 0:
          custom.copyprotLevel = word(startLevel)
      displayTextBottom("GAME LOADED")
      success = 1
      dontResetTime = 1
    except IOError:
      let
        msg = getCurrentExceptionMsg()
      displayTextBottom("UNABLE TO LOAD GAME")
      perror(cstring("Got IO exception with message " & msg))
    except CatchableError:
      displayTextBottom("Unknown exception!")
      # reraise the unknown exception:
      raise
    finally:
      close(handle)
  else:
    perror("loadGame: fopen")
    echo "Tried to open for reading: ", savePath

  return success

# seg000:1F02
proc clearScreenAndSounds() =
  stopSounds()
  currentTargetSurface = rectSthg(onscreenSurface, screenRect)

  isCutscene = 0
  isEndingSequence = false
  peelsCount = 0
  # should these be freed
  for index in 2..<10:
    if chtabAddrs[index] != nil:
      # Original code does not free these?
      freeChtab(chtabAddrs[index])
      chtabAddrs[index] = nil

  # Note: C code uses -1, which is equal to 65355 for uint16_t
  currentLevel = high(typeof(currentLevel))

# seg000:1F7B
proc parseCmdlineSound() =
  # stub
  if not(stdsndCommandOption):
    soundFlags = byte(soundFlags or ord(sfDigi))
    soundFlags = byte(soundFlags or ord(sfMidi))
    soundMode = ord(smSblast)

# seg000:226D
proc freeOptionalSounds() =
  discard

proc freeAllSounds() =
  for i in 0..<58:
    freeSound(soundPointers[i])
    soundPointers[i] = nil

proc loadAllSounds() =
  if (useCustomLevelset == 0) or (alwaysUseOriginalMusic != 0):
    loadSounds(0, 43)
    loadOptSounds(43, 56) # added
  else:
    # First load any sounds included in the mod folder...
    skipNormalDataFiles = true
    loadSounds(0, 43)
    loadOptSounds(43, 56)
    skipNormalDataFiles = false
    # ... then load any missing sounds from SDLPoP's own resources.
    skipModDataFiles = true
    loadSounds(0, 43)
    loadOptSounds(43, 56)
    skipModDataFiles = false

# seg000:22BB
proc freeOptsndChtab() =
  freeOptionalSounds()
  freeAllChtabsFrom(ord(idChtab3Princessinstory))

# seg000:22C8
proc loadTitleImages(bgcolor: int32) =
  var
    dathandle: DatType = openDat("TITLE.DAT", int32('G'))
  chtabTitle40 = loadSpritesFromFile(40, 1 shl 11, 1)
  chtabTitle50 = loadSpritesFromFile(50, 1 shl 12, 1)
  closeDat(datHandle)
  if graphicsMode == ord(gmMcgaVga):
    # background of text frame
    var
      color: Color
    if (bgcolor) != 0:
      # RGB(4,0,18h) = # 100060 = dark blue
      setPal((findFirstPalRow(1 shl 11) shl 4) + 14, 0x04, 0x00, 0x18)
      color.r = 0x10
      color.g = 0x00
      color.b = 0x60
      color.a = 0xFF
    else:
      # RGB(20h, 0, 0) = #800000 = dark red
      setPal((findFirstPalRow(1 shl 11) shl 4) + 14, 0x20, 0x00, 0x00)
      color.r = 0x80
      color.g = 0x00
      color.b = 0x00
      color.a = 0xFF
    if chtabTitle40 != nil:
      discard setPaletteColors(chtabTitle40.images[0].format.palette, addr(
          color), 14, 1)
  elif graphicsMode == ord(gmEga) or graphicsMode == ord(gmTga):
    # ...
    discard

when(USE_COPYPROT):
  const
    # data:017A
    copyprotWord: array[40, word] = [9'u16, 1, 6, 4, 5, 3, 6, 3, 4, 4, 3, 2, 12,
        5, 13, 1, 9, 2, 2, 4, 9, 4, 11, 8, 5, 4, 1, 6, 2, 4, 6, 8, 4, 2, 7, 11,
        5, 4, 1, 2]
    # data:012A
    copyprotLine: array[40, word] = [2'u16, 1, 5, 4, 3, 5, 1, 3, 7, 2, 2, 4, 6,
        6, 2, 6, 3, 1, 2, 3, 2, 2, 3, 10, 5, 6, 5, 6, 3, 5, 7, 2, 2, 4, 5, 7, 2,
        6, 5, 5]
    # data:00DA
    copyprotPage: array[40, word] = [5'u16, 3, 7, 3, 3, 4, 1, 5, 12, 5, 11, 10,
        1, 2, 8, 8, 2, 4, 6, 1, 4, 7, 3, 2, 1, 7, 10, 1, 4, 3, 4, 1, 4, 1, 8, 1,
        1, 10, 3, 3]

# seg000:23F4
proc showCopyprot(where: int32) =
  when(USE_COPYPROT):
    var
      sprintfTemp: string
    if currentLevel != 15:
      return
    if (where) != 0:
      if (textTimeRemaining or isCutscene) != 0:
        return
      textTimeTotal = 1188
      textTimeRemaining = 1188
      isShowTime = 0
      sprintfTemp = "WORD " & $(copyprotWord[copyprotIdx]) & " LINE " & $(
          copyprotLine[copyprotIdx]) & " PAGE " & $(copyprotPage[copyprotIdx])
      displayTextBottom(sprintfTemp)
    else:
      sprintfTemp = "Drink potion matching the first letter of Word " & $(
          copyprotWord[copyprotIdx]) & " on Line " & $(copyprotLine[
          copyprotIdx]) & "\nof Page " & $(copyprotPage[copyprotIdx]) & " of the manual."
      showDialog(sprintfTemp)
  else:
    discard

# seg000:2489
proc showLoading() =
  showText(getAddress(screenRect), hAlignCenter, vAlignMiddle, "Loading. . . .")
  updateScreen()

# data:42C4
var
  whichQuote: word

const
  tblQuotes: array[2, string] = [
      "\"(****/****) Incredibly realistic. . . The " &
      "adventurer character actually looks human as he " &
      "runs, jumps, climbs, and hangs from ledges.\"\n" &
      "\n" &
      "                                  Computer Entertainer\n" &
      "\n" &
      "\n" &
      "\n" &
      "\n" &
      "\"A tremendous achievement. . . Mechner has crafted " &
      "the smoothest animation ever seen in a game of this " &
      "type.\n" &
      "\n" &
      "\"PRINCE OF PERSIA is the STAR WARS of its field.\"\n" &
      "\n" &
      "                                  Computer Gaming World",
      "\"An unmitigated delight. . . comes as close to " &
      "(perfection) as any arcade game has come in a long, " &
      "long time. . . what makes this game so wonderful (am " &
      "I gushing?) is that the little onscreen character " &
      "does not move like a little onscreen character -- he " &
      "moves like a person.\"\n" &
      "\n" &
      "                                      Nibble"
    ]

# seg000:249D
proc showQUotes() =
  # startTimer(ord(timer0), 0)
  # removeTimer(ord(timer0))
  if (demoMode and needQuotes) != 0:
    drawRect(getAddress(screenRect), ord(color0Black))
    showText(getAddress(screenRect), hAlignLeft, vAlignMiddle, tblQuotes[whichQuote])
    whichQuote = not(whichQuote)
    startTimer(ord(timer0), 0x384)
  needQuotes = 0

var
  splashText1Rect: RectType = RectType(top: 0, left: 0, bottom: 50, right: 320)
  splashText2Rect: RectType = RectType(top: 50, left: 0, bottom: 200, right: 320)
  splashText1: string = "nimPoP " & NIMPOP_VERSION
  splashText2: string =
    "In-game, Esc opens a settings/quicksave menu.\n" &
    (when(USE_REPLAY):
      "To record replays, press Ctrl+Tab in-game.\n" &
      "To view replays, press Tab on the title screen.\n" &
      "\n" &
      "Edit SDLPoP.ini to customize nimPoP.\n" &
      "Mods also work with nimPoP.\n" & "\n" &
      "For more information, read Readme.md.\n" &
      "Questions? Visit https://forum.princed.org\n" &
      "\n" &
      "Press any key to continue..." else:
      "To quick save/load, press F6/F9 in-game.\n" &
      "\n" &
      "Edit SDLPoP.ini to customize nimPoP.\n" &
      "Mods also work with nimPoP.\n" &
      "\n" &
      "For more information, read Readme.md.\n" &
      "Questions? Visit https://forum.princed.org\n" &
      "\n" &
      "Press any key to continue...")

proc showSplash() =
  if (enableInfoScreen == 0) or startLevel >= 0:
    return
  currentTargetSurface = onscreenSurface
  drawRect(getAddress(screenRect), ord(color0Black))
  showTextWithColor(splashText1Rect, hAlignCenter, vAlignMiddle, splashText1,
      ord(color15Brightwhite))
  showTextWithColor(splashText2Rect, hAlignCenter, vAlignTop, splashText2, ord(color7Lightgray))

  var
    key: int = 0
  when(USE_TEXT): # Don't wait for a keypress it there is no text for the user to read.
    do_while(key == 0 and ((keyStates[ord(SDL_SCANCODE_LSHIFT)] or keyStates[
        ord(SDL_SCANCODE_RSHIFT)]) and ord(KEYSTATE_HELD)) == 0):
      idle()
      key = keyTestQuit()
      var
        joyInput: bool = false

      for i in 0..<ord(JOYINPUT_NUM):
        if any(joyButtonStates, proc(x: int32): bool = (x and ord(
            KEYSTATE_HELD)) != 0):
          joyInput = true
          break

      if joyInput:
        apply(joyButtonStates, func(x: var int32) = x = 0)
        # close the splash screen using the gamepad
        keyStates[ord(SDL_SCANCODE_LSHIFT)] = keyStates[ord(
            SDL_SCANCODE_LSHIFT)] or ord(KEYSTATE_HELD)
      delayTicks(1)

    if ((key and ord(WITH_CTRL)) != 0 or ((enableQuicksave != 0) and key == ord(
        SDL_SCANCODE_F9)) or ((enableReplay != 0) and key == ord(
            SDL_SCANCODE_TAB))):
      # can immediately do Ctrl+L, etc from the splash screen
      lastKeyScancode = key

    # don't immediately start the game if shift was pressed!
    keyStates[ord(SDL_SCANCODE_LSHIFT)] = 0
    keyStates[ord(SDL_SCANCODE_RSHIFT)] = 0

proc getWritableFilePath(fileName: string): string =
  # If the SDLPOP_SAVE_PATH environment variable is set, put all saves into the
  # directory it points to. Otherwise, save to the home directory
  when defined(windows):
    var
      savePath: string = getEnv("SDLPOP_SAVE_PATH")
  else:
    var
      savePath: string
    let
      customSavePath = getEnv("SDLPOP_SAVE_PATH")
      homePath: string = getHomeDir()
    if customSavePath != "":
      savePath = customSavePath
    elif homePath != "":
      savePath = homePath & "/." & POP_DIR_NAME
  if savePath != "":
    createDir(savePath)
    setFilePermissions(savePath, {fpUserRead, fpUserWrite, fpUserExec})
    if useCustomLevelset != 0:
      savePath = savePath & "/" & levelsetName
      createDir(savePath)
      setFilePermissions(savePath, {fpUserRead, fpUserWrite, fpUserExec})
    return savePath & "/" & filename

  if useCustomLevelset == 0:
    return fileName
  # if playing a custom levelset, try to use the mod folder
  return modDataPath & fileName
