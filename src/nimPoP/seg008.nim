var
  # data:27E0
  ptrAddTable: AddTableType = addBacktable

const
  # data:259C
  tileTable: array[31, Piece] = [
      Piece(baseId: 0'u8, floorLeft: 0'u8, baseY: 0'i8, rightId: 0'u8,
          floorRight: 0'u8, rightY: 0'i8, stripeId: 0'u8, toprightId: 0'u8,
          bottomId: 0'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x00 empty
    Piece(baseId: 41'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 42'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x01 floor
    Piece(baseId: 127'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 133'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x02 spike
    Piece(baseId: 92'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 93'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 94'u8,
        bottomId: 43'u8, foreId: 95'u8, foreX: 1'u8, foreY: 0'i8), # 0x03 pillar
    Piece(baseId: 46'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 47'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 48'u8,
        bottomId: 43'u8, foreId: 49'u8, foreX: 3'u8, foreY: 0'i8), # 0x04 door
    Piece(baseId: 41'u8, floorLeft: 1'u8, baseY: 1'i8, rightId: 35'u8,
        floorRight: 1'u8, rightY: 3'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 36'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x05 stuck floor
    Piece(baseId: 41'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 42'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 96'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x06 close button
    Piece(baseId: 46'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 0'u8,
        floorRight: 0'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 49'u8, foreX: 3'u8, foreY: 0'i8), # 0x07 door top with floor
    Piece(baseId: 86'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 87'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 88'u8, foreX: 1'u8, foreY: 0'i8), # 0x08 big pillar bottom
    Piece(baseId: 0'u8, floorLeft: 0'u8, baseY: 0'i8, rightId: 89'u8,
        floorRight: 0'u8, rightY: 3'i8, stripeId: 0'u8, toprightId: 90'u8,
        bottomId: 0'u8, foreId: 91'u8, foreX: 1'u8, foreY: 3'i8), # 0x09 big pillar top
    Piece(baseId: 41'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 42'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 12'u8, foreX: 2'u8, foreY: -3'i8), # 0x0A potion
    Piece(baseId: 0'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 0'u8,
        floorRight: 0'u8, rightY: 0'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 0'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x0B loose floor
    Piece(baseId: 0'u8, floorLeft: 0'u8, baseY: 0'i8, rightId: 0'u8,
        floorRight: 0'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 85'u8, foreId: 49'u8, foreX: 3'u8, foreY: 0'i8), # 0x0C door top
    Piece(baseId: 75'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 42'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 77'u8, foreX: 0'u8, foreY: 0'i8), # 0x0D mirror
    Piece(baseId: 97'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 98'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 100'u8, foreX: 0'u8, foreY: 0'i8), # 0x0E debris
    Piece(baseId: 147'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 42'u8,
        floorRight: 1'u8, rightY: 1'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 149'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x0F open button
    Piece(baseId: 41'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 37'u8,
        floorRight: 0'u8, rightY: 0'i8, stripeId: 0'u8, toprightId: 38'u8,
        bottomId: 43'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x10 leveldoor left
    Piece(baseId: 0'u8, floorLeft: 0'u8, baseY: 0'i8, rightId: 39'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 40'u8,
        bottomId: 43'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x11 leveldoor right
    Piece(baseId: 0'u8, floorLeft: 0'u8, baseY: 0'i8, rightId: 42'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x12 chomper
    Piece(baseId: 41'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 42'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x13 torch
    Piece(baseId: 0'u8, floorLeft: 0'u8, baseY: 0'i8, rightId: 1'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 2'u8,
        bottomId: 0'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x14 wall
    Piece(baseId: 30'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 31'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x15 skeleton
    Piece(baseId: 41'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 42'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x16 sword
    Piece(baseId: 41'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 10'u8,
        floorRight: 0'u8, rightY: 0'i8, stripeId: 0'u8, toprightId: 11'u8,
        bottomId: 43'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x17 balcony left
    Piece(baseId: 0'u8, floorLeft: 0'u8, baseY: 0'i8, rightId: 12'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 13'u8,
        bottomId: 43'u8, foreId: 0'u8, foreX: 0'u8, foreY: 0'i8), # 0x18 balcony right
    Piece(baseId: 92'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 42'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 145'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 95'u8, foreX: 1'u8, foreY: 0'i8), # 0x19 lattice pillar
    Piece(baseId: 1'u8, floorLeft: 0'u8, baseY: 0'i8, rightId: 0'u8,
        floorRight: 0'u8, rightY: 0'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 2'u8, foreId: 9'u8, foreX: 0'u8, foreY: -53'i8), # 0x1A lattice down
    Piece(baseId: 3'u8, floorLeft: 0'u8, baseY: -10'i8, rightId: 0'u8,
        floorRight: 0'u8, rightY: 0'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 0'u8, foreId: 9'u8, foreX: 0'u8, foreY: -53'i8), # 0x1B lattice small
    Piece(baseId: 4'u8, floorLeft: 0'u8, baseY: -10'i8, rightId: 0'u8,
        floorRight: 0'u8, rightY: 0'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 0'u8, foreId: 9'u8, foreX: 0'u8, foreY: -53'i8), # 0x1C lattice left
    Piece(baseId: 5'u8, floorLeft: 0'u8, baseY: -10'i8, rightId: 0'u8,
        floorRight: 0'u8, rightY: 0'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 0'u8, foreId: 9'u8, foreX: 0'u8, foreY: -53'i8), # 0x1D lattice right
    Piece(baseId: 97'u8, floorLeft: 1'u8, baseY: 0'i8, rightId: 98'u8,
        floorRight: 1'u8, rightY: 2'i8, stripeId: 0'u8, toprightId: 0'u8,
        bottomId: 43'u8, foreId: 100'u8, foreX: 0'u8, foreY: 0'i8), # 0x1E debris with torch
  ]

var
  # data:4334
  drawnRow: int16
  # data:4352
  drawBottomY: int16
  # data:4326
  drawMainY: int16
  # data:34D0
  drawnCol: int16

  # data:6592
  tileLeft: byte
  # data:4CCC
  modifierLeft: byte

# seg008:0006
proc redrawRoom() =
  freePeels()
  for i in 0..high(tableCounts):
    tableCounts[i] = 0
  resetObjClip()
  drawRoom()
  clearTileWipes()


# seg008:0035
proc loadRoomLinks() =
  roomBR = 0
  roomBL = 0
  roomAR = 0
  roomAL = 0
  if (drawnRoom) != 0:
    getRoomAddress(int(drawnRoom))
    roomL = level.roomlinks[drawnRoom-1].left
    roomR = level.roomlinks[drawnRoom-1].right
    roomA = level.roomlinks[drawnRoom-1].up
    roomB = level.roomlinks[drawnRoom-1].down
    if (roomA) != 0:
      roomAL = level.roomlinks[roomA-1].left
      roomAR = level.roomlinks[roomA-1].right
    else:
      if (roomL) != 0:
        roomAL = level.roomlinks[roomL-1].up
      if (roomR) != 0:
        roomAR = level.roomlinks[roomR-1].up
    if (roomB) != 0:
      roomBL = level.roomlinks[roomB-1].left
      roomBR = level.roomlinks[roomB-1].right
    else:
      if (roomL) != 0:
        roomBL = level.roomlinks[roomL-1].down
      if (roomR) != 0:
        roomBR = level.roomlinks[roomR-1].down
  else:
    roomB = 0
    roomA = 0
    roomR = 0
    roomL = 0


# seg008:0125
proc drawRoom() =
  var
    savedRoom: word
  loadLeftroom()
  # for (drawnRow = 3; drawnRow--; ) { /*2,1,0*/
  drawnRow = 3
  while drawnRow > 0:
    dec(drawnRow)
    loadRowbelow()
    drawBottomY = 63 * drawnRow + 65
    drawMainY = drawBottomY - 3
    drawnCol = 0
    while drawnCol < 10:
      loadCurrAndLeftTile()
      drawTile()
      inc(drawnCol)
  savedRoom = drawnRoom
  drawnRoom = roomA
  loadRoomLinks()
  loadLeftroom()
  drawnRow = 2
  loadRowbelow()
  drawnCol = 0
  # for (drawnCol = 0; drawnCol < 10; ++drawnCol) {
  while drawnCol < 10:
    loadCurrAndLeftTile()
    drawMainY = -1
    drawBottomY = 2
    drawTileAboveroom()
    inc(drawnCol)

  drawnRoom = savedRoom
  loadRoomLinks()


# seg008:01C7
proc drawTile() =
  drawTileFloorright()
  drawTileAnimTopright()
  drawTileRight()
  drawTileAnimRight()
  drawTileBottom(0)
  drawLoose(0)
  drawTileBase()
  drawTileAnim()
  drawTileFore()


# seg008:01F2
proc drawTileAboveroom() =
  drawTileFloorright()
  drawTileAnimTopright()
  drawTileRight()
  drawTileBottom(0)
  drawLoose(0)
  drawTileFore()


# seg008:0211
proc redrawNeeded(tilepos: int16) =
  if (wipeFrames[tilepos]) != 0:
    dec(wipeFrames[tilepos])
    drawTileWipe(byte(wipeHeights[tilepos]))

  if (redrawFramesFull[tilepos]) != 0:
    dec(redrawFramesFull[tilepos])
    drawTile()
  else:
    if (redrawFramesAnim[tilepos]) != 0:
      dec(redrawFramesAnim[tilepos])
      drawTileAnimTopright()
      drawTileAnimRight()
      drawTileAnim()
      when (FIX_ABOVE_GATE):
        draw_tile_fore()
        draw_tile_bottom(0)


  if (redrawFrames2[tilepos]) != 0:
    dec(redrawFrames2[tilepos])
    drawOtherOverlay()
  else:
    if (redrawFramesFloorOverlay[tilepos]) != 0:
      dec(redrawFramesFloorOverlay[tilepos])
      drawFloorOverlay()


  if (tileObjectRedraw[tilepos]) != 0:
    if (tileObjectRedraw[tilepos] == 0xFF):
      drawObjtableItemsAtTile(byte(tilepos - 1))

    drawObjtableItemsAtTile(byte(tilepos))
    tileObjectRedraw[tilepos] = 0

  if (redrawFramesFore[tilepos]) != 0:
    dec(redrawFramesFore[tilepos])
    drawTileFore()


# seg008:02C1
proc redrawNeededAbove(column: int32) =
  if (redrawFramesAbove[column] != 0):
    dec(redrawFramesAbove[column])
    when (FIX_BIGPILLAR_JUMP_UP):
      if currTile != ord(tiles9BigpillarTop):
        drawTileWipe(3)

    drawTileFloorright()
    drawTileAnimTopright()
    drawTileRight()
    drawTileBottom(1)
    drawLoose(1)
    drawTileFore()


# seg008:02FE
proc getTileToDraw(room, column, row: int32, ptrTile, ptrModifier: ptr byte,
    tileRoom0: byte): int32 =
  var
    tilepos: word = word(int32(tblLine[row]) + column)
  if (column == -1):
    ptrTile[] = leftroom[row].tiletype
    ptrModifier[] = leftroom[row].modifier
  elif (room) != 0:
    ptrTile[] = byte(currRoomTiles[tilepos] and 0x1F)
    ptrModifier[] = currRoomModif[tilepos]
  else:
    ptrModifier[] = 0
    ptrTile[] = tileRoom0

  # Is this a pressed button?
  var
    tiletype: byte = byte( (ptrTile[]) and 0x1F)
    modifier: byte = ptrModifier[]
  if (tiletype == ord(tiles6Closer)):
    if (getDoorlinkTimer(int16(modifier)) > 1):
      ptrTile[] = ord(tiles5Stuck)
  elif (tiletype == ord(tiles15Opener)):
    if (getDoorlinkTimer(int16(modifier)) > 1):
      ptrModifier[] = 0
      ptrTile[] = ord(tiles1Floor)
  else:
    when (USE_FAKE_TILES):
      if (tiletype == ord(tiles0Empty)):
        if (modifier == 4 or modifier == 12): # display a fake floor
          ptrTile[] = ord(tiles1Floor)
          ptrModifier[] = if (modifier == 12): 1 else: 0 # 12: noblue option
        elif (modifier == 5 or modifier == 13): # display a fake wall
          ptrTile[] = ord(tiles20Wall)
          ptrModifier[] = if (modifier == 13): 0x80 else: 0 # 13: noblue option
        elif (modifier == 50): # display a fake wall (pattern: no walls left or right)
          ptrTile[] = ord(tiles20Wall)
          ptrModifier[] = 0
        elif (modifier == 51): # display a fake wall (pattern: wall only to the right)
          ptrTile[] = ord(tiles20Wall)
          ptrModifier[] = 1
        elif (modifier == 52): # display a fake wall (pattern: wall only to the left)
          ptrTile[] = ord(tiles20Wall)
          ptrModifier[] = 2
        elif (modifier == 53): # display a fake wall (pattern: wall on both sides)
          ptrTile[] = ord(tiles20Wall)
          ptrModifier[] = 3
      elif (tiletype == ord(tiles1Floor)):
        if (modifier == 6 or modifier == 14): # display nothing (invisible floor)
          ptrTile[] = ord(tiles0Empty)
          ptrModifier[] = if (modifier ==
              14): 1 else: 0 # modifier should be '0' for noblue, instead of '1'
        elif (modifier == 5 or modifier == 13): # display a fake wall
          ptrTile[] = ord(tiles20Wall)
        elif (modifier == 50): # display a fake wall (pattern: no walls left or right (noblue))
          ptrTile[] = ord(tiles20Wall)
          ptrModifier[] = 0
        elif (modifier == 51): # display a fake wall (pattern: wall only to the right)
          ptrTile[] = ord(tiles20Wall)
          ptrModifier[] = 1
        elif (modifier == 52): # display a fake wall (pattern: wall only to the left)
          ptrTile[] = ord(tiles20Wall)
          ptrModifier[] = 2
        elif (modifier == 53): # display a fake wall (pattern: wall on both sides)
          ptrTile[] = ord(tiles20Wall)
          ptrModifier[] = 3
      elif (tiletype == ord(tiles20Wall)):
        # Walls are a bit strange, because a lot of modifier information is discarded in loadAlterMod() (seg008.c)
        # Also, the "noblue" info for the wall tile is moved to the --most significant-- modifier bit there.

        # loadAlterMod() has been tweaked to retain more information (now stored in the most significant 4 bytes)
        # Modifiers 2-7 are now accessible to define various fake tiles
        # Modifiers 9-15 'loop back' onto 2-7 (identical tiles), EXCEPT they also have the "noblue" bit set
        if (((modifier shr 4) and 7) == 4): # display a floor (invisible wall)
          ptrTile[] = ord(tiles1Floor)
          ptrModifier[] = (modifier shr 7) # modifier should be '1' for noblue option
        elif (((modifier shr 4) and 7) == 6): # display empty tile (invisible wall)
          ptrTile[] = ord(tiles0Empty)
          ptrModifier[] = if modifier shr 7 !=
              0: 1 else: 0 # modifier should be '0' for noblue, instead of '1'
      else:
        when (FIX_LOOSE_LEFT_OF_POTION):
          if ((fixes.fixLooseLeftOfPotion != 0) and tiletype == ord(tiles11Loose)):
            if ((ptrModifier[] and 0x7F) == 0):
              ptrTile[] = ord(tiles1Floor)
        else:
          discard
    elif (FIX_LOOSE_LEFT_OF_POTION):
      if ((fixes.fixLooseLeftOfPotion != 0) and tiletype == ord(tiles11Loose)):
        if ((ptrModifier[] and 0x7F) == 0):
          ptrTile[] = ord(tiles1Floor)
    else:
      discard
  return int32(ptrTile[])


const
  # data:24C6
  colXh: array[10, word] = [0'u16, 4'u16, 8'u16, 12'u16, 16'u16, 20'u16, 24'u16,
      28'u16, 32'u16, 36'u16]

# seg008:03BB
proc loadCurrAndLeftTile() =
  var
    tiletype: word
  tiletype = ord(tiles20Wall)
  if (drawnRow == 2):
    tiletype = custom.drawnTileTopLevelEdge # floor at top of level (default: ord(tiles1Floor))

  discard getTileToDraw(int(drawnRoom), int32(drawnCol), int32(drawnRow), addr(
      currTile), addr(currModifier), byte(tiletype))
  discard getTileToDraw(int(drawnRoom), int32(drawnCol - 1), int32(drawnRow),
      addr(tileLeft), addr(modifierLeft), byte(tiletype))
  drawXh = colXh[drawnCol]


# seg008:041A
proc loadLeftroom() =
  getRoomAddress(int(roomL))
  for row in 0 ..< 3:
    # wall at left of level (drawnTileLeftLevelEdge), default: ord(tiles20Wall)
    discard getTileToDraw(int(roomL), 9, int32(row), addr(leftroom[
        row].tiletype), addr(leftroom[row].modifier),
        custom.drawnTileLeftLevelEdge)


# seg008:0460
proc loadRowbelow() =
  var
    rowBelow: word
    room: word
    roomLeft: word
  if (drawnRow == 2):
    room = roomB
    roomLeft = roomBL
    rowBelow = 0
  else:
    room = drawnRoom
    roomLeft = roomL
    rowBelow = word(drawnRow + 1)

  getRoomAddress(int(room))
  for column in 1 ..< 10:
    discard getTileToDraw(int(room), int32(column - 1), int32(rowBelow), addr(
        rowBelowLeft[column].tiletype), addr(rowBelowLeft[column].modifier),
            ord(tiles0Empty))

  getRoomAddress(int(roomLeft))
  # wall at left of level
  discard getTileToDraw(int(roomLeft), 9, int32(rowBelow), addr(rowBelowLeft[
      0].tiletype), addr(rowBelowLeft[0].modifier), ord(tiles20Wall))
  getRoomAddress(int(drawnRoom))


# seg008:04FA
proc drawTileFloorright() =
  if (canSeeBottomleft() == 0):
    return
  drawTileTopright()
  if (tileTable[tileLeft].floorRight == 0):
    return
  discard addBacktable(ord(idChtab6Environment), 42, sbyte(drawXh), 0,
      tileTable[ord(tiles1Floor)].rightY + drawMainY, ord(blitters9Black), 1)


# seg008:053A
proc canSeeBottomleft(): int32 =
  return int32(currTile == ord(tiles0Empty) or
    currTile == ord(tiles9BigpillarTop) or
    currTile == ord(tiles12Doortop) or
    currTile == ord(tiles26LatticeDown))


const
  doortopFramTop: array[4, byte] = [0'u8, 81'u8, 83'u8, 0'u8]

# seg008:055A
proc drawTileTopright() =
  var
    tiletype: byte
  tiletype = rowBelowLeft[drawnCol].tiletype
  if (tiletype == ord(tiles7DoortopWithFloor) or tiletype == ord(
      tiles12Doortop)):
    if (custom.tblLevelType[currentLevel] == 0):
      return
    discard addBacktable(ord(idChtab6Environment), int32(doortopFramTop[
        rowBelowLeft[drawnCol].modifier]), sbyte(drawXh), 0, ord(drawBottomY),
        ord(blitters2Or), 0)
  elif (tiletype == ord(tiles20Wall)):
    discard addBacktable(ord(idChtab7Environmentwall), 2, sbyte(drawXh), 0, ord(
        drawBottomY), ord(blitters2Or), 0)
  else:
    discard addBacktable(ord(idChtab6Environment), int32(tileTable[
        tiletype].toprightId), sbyte(drawXh), 0, ord(drawBottomY), ord(
            blitters2Or), 0)


const
  doorFramTop: array[8, byte] = [60'u8, 61'u8, 62'u8, 63'u8, 64'u8, 65'u8,
      66'u8, 67'u8]

# seg008:05D1
proc drawTileAnimTopright() =
  var
    modifier: word
  if ( (currTile == ord(tiles0Empty) or
    currTile == ord(tiles9BigpillarTop) or
    currTile == ord(tiles12Doortop)) and
    rowBelowLeft[drawnCol].tiletype == ord(tiles4Gate)
  ):
    discard addBacktable(ord(idChtab6Environment), 68, sbyte(drawXh), 0, ord(
        drawBottomY), ord(blitters40hMono), 0)
    modifier = rowBelowLeft[drawnCol].modifier
    if (modifier > 188'u16):
      modifier = 188
    discard addBacktable(ord(idChtab6Environment), int32(doorFramTop[(
        modifier shr 2) mod 8]), sbyte(drawXh), 0, ord(drawBottomY), ord(
            blitters2Or), 0)


const
  bluelineFram1: array[4, byte] = [0'u8, 124'u8, 125'u8, 126'u8]
  bluelineFramY: array[4, sbyte] = [0'i8, -20'i8, -20'i8, 0'i8]
  bluelineFram3: array[4, byte] = [44'u8, 44'u8, 45'u8, 45'u8]
  doortopFramBot: array[4, byte] = [78'u8, 80'u8, 82'u8, 0'u8]

# seg008:066A
proc drawTileRight() =
  var
    id: byte
    blit: byte
    var2: byte
  if (currTile == ord(tiles20Wall)):
    return
  case (tileLeft)
  of ord(tiles0Empty):
    if (modifierLeft > 3):
      return
    discard addBacktable(ord(idChtab6Environment), int32(bluelineFram1[
        modifierLeft]), sbyte(drawXh), 0, bluelineFramY[modifierLeft] +
            drawMainY, ord(blitters2Or), 0)
  of ord(tiles1Floor):
    discard ptrAddTable(ord(idChtab6Environment), 42, sbyte(drawXh), 0, int32(
        tileTable[tileLeft].rightY + drawMainY), ord(blitters10hTransp), 0)
    var2 = modifierLeft
    if (var2 > 3):
      var2 = 0
    if (var2 == not(not(custom.tblLevelType[currentLevel]))):
      return
    discard addBacktable(ord(idChtab6Environment), int16(bluelineFram3[var2]),
        sbyte(drawXh), 0, drawMainY - 20, ord(blitters0NoTransp), 0)
  of ord(tiles7DoortopWithFloor), ord(tiles12Doortop):
    if (custom.tblLevelType[currentLevel] == 0):
      return
    discard addBacktable(ord(idChtab6Environment), int32(doortopFramBot[
        modifierLeft]), sbyte(drawXh), 0, tileTable[tileLeft].rightY +
            drawMainY, ord(blitters2Or), 0)
  of ord(tiles20Wall):
    if ((custom.tblLevelType[currentLevel] != 0) and (modifierLeft and 0x80) == 0):
      discard addBacktable(ord(idChtab6Environment), 84, sbyte(drawXh + 3), 0,
          drawMainY - 27, ord(blitters0NoTransp), 0)
    discard addBacktable(ord(idChtab7Environmentwall), 1, sbyte(drawXh), 0,
        tileTable[tileLeft].rightY + drawMainY, ord(blitters2Or), 0)
  else:
    id = tileTable[tileLeft].rightId
    if (id) != 0:
      if (tileLeft == ord(tiles5Stuck)):
        blit = ord(ord(blitters10hTransp))
        if (currTile == ord(tiles0Empty) or currTile == ord(tiles5Stuck)):
          id = 42
      else:
        blit = ord(blitters2Or)
      discard addBacktable(ord(idChtab6Environment), int16(id), sbyte(drawXh),
          0, int32(tileTable[tileLeft].rightY + drawMainY), int32(blit), 0)
    if (custom.tblLevelType[currentLevel] != 0):
      discard addBacktable(ord(idChtab6Environment), int16(tileTable[
          tileLeft].stripeId), sbyte(drawXh), 0, int32(drawMainY - 27), ord(
              blitters2Or), 0)
    if (tileLeft == ord(tiles19Torch) or tileLeft == ord(
        tiles30TorchWithDebris)):
      discard addBacktable(ord(idChtab6Environment), 146, sbyte(drawXh), 0,
          drawBottomY - 28, ord(blitters0NoTransp), 0)


const
  spikesFramRight: array[10, byte] = [0'u8, 134'u8, 135'u8, 136'u8, 137'u8,
      138'u8, 137'u8, 135'u8, 134'u8, 0'u8]
  looseFramRight: array[12, byte] = [42'u8, 71'u8, 42'u8, 72'u8, 72'u8, 42'u8,
      42'u8, 42'u8, 72'u8, 72'u8, 72'u8, 0'u8]

# seg008:08A0
proc getSpikeFrame(modifier: byte): int32 =
  if (modifier and 0x80) != 0:
    return 5
  else:
    return int32(modifier)


# seg008:08B5
proc drawTileAnimRight() =
  case (tileLeft)
  of ord(tiles2Spike):
    discard addBacktable(ord(idChtab6Environment), int32(spikesFramRight[
        getSpikeFrame(modifierLeft)]), sbyte(drawXh), 0, int32(drawMainY - 7),
            ord(blitters10hTransp), 0)
  of ord(tiles4Gate):
    drawGateBack()
  of ord(tiles11Loose):
    discard addBacktable(ord(idChtab6Environment), int32(looseFramRight[
        getLooseFrame(modifierLeft)]), sbyte(drawXh), 0, int32(drawBottomY - 1),
            ord(blitters2Or), 0)
  of ord(tiles16LevelDoorLeft):
    drawLeveldoor()
  of ord(tiles19Torch), ord(tiles30TorchWithDebris):
    if (modifierLeft < 9):
      var
        blit: int32 = ord(blitters0NoTransp)
      when (USE_COLORED_TORCHES):
        var
          color: int32
        if (drawnCol == 0):
          # Torch is in the rightmost column of the left-side room.
          color = int32(torchColors[roomL][drawnRow * 10 + 9])
        else:
          color = int32(torchColors[drawnRoom][drawnRow * 10 + drawnCol - 1])
        if (color != 0):
          blit = ord(blittersColoredFlame) + (color and 0x3F)
      # images 1..9 are the flames
      discard addBacktable(ord(idChtab1Flameswordpotion), int32(modifierLeft +
          1), sbyte(drawXh + 1), 0, int32(drawMainY - 40), blit, 0)
  else:
    discard


const
  wallFramBottom: array[4, byte] = [7'u8, 9'u8, 5'u8, 3'u8]

# seg008:0971
proc drawTileBottom(arg0: word) =
  var
    chtabId: word
    id: byte
    blit: byte
  id = 0
  blit = ord(blitters0NoTransp)
  chtabId = ord(idChtab6Environment)
  case (currTile)
  of ord(tiles20Wall):
    if (custom.tblLevelType[currentLevel] == 0 or (custom.enableWdaInPalace !=
        0) or graphicsMode != ord(gmMcgaVga)):
      id = wallFramBottom[currModifier and 0x7F]
    chtabId = ord(idChtab7Environmentwall)
  of ord(tiles12Doortop):
    blit = ord(blitters2Or)
    # fallthrough!
    id = tileTable[currTile].bottomId
  else:
    id = tileTable[currTile].bottomId

  if (ptrAddTable(int16(chtabId), int32(id), sbyte(drawXh), 0, int32(
      drawBottomY), int32(blit), 0) and int32(arg0)) != 0:
    discard addForetable(int16(chtabId), int32(id), sbyte(drawXh), 0, int32(
        drawBottomY), int32(blit), 0)

  if (chtabId == ord(idChtab7Environmentwall) and graphicsMode != ord(gmCga) and
      graphicsMode != ord(gmHgaHerc)):
    wallPattern(0, 0)



const
  looseFramBottom: array[12, byte] = [43'u8, 73'u8, 43'u8, 74'u8, 74'u8, 43'u8,
      43'u8, 43'u8, 74'u8, 74'u8, 74'u8, 0'u8]

# seg008:0A38
proc drawLoose(arg0: int32) =
  var
    id: word
  if (currTile == ord(tiles11Loose)):
    id = looseFramBottom[getLooseFrame(currModifier)]
    discard addBacktable(ord(idChtab6Environment), int32(id), sbyte(drawXh), 0,
        drawBottomY, ord(blitters0NoTransp), 0)
    discard addForetable(ord(idChtab6Environment), int32(id), sbyte(drawXh), 0,
        drawBottomY, ord(blitters0NoTransp), 0)


const
  looseFramLeft: array[12, byte] = [41'u8, 69'u8, 41'u8, 70'u8, 70'u8, 41'u8,
      41'u8, 41'u8, 70'u8, 70'u8, 70'u8, 0'u8]

# seg008:0A8E
proc drawTileBase() =
  var
    ybottom: word
    id: word
  ybottom = word(drawMainY)
  if (tileLeft == ord(tiles26LatticeDown) and currTile == ord(tiles12Doortop)):
    id = 6 # Lattice + door A
    ybottom += 3
  elif (currTile == ord(tiles11Loose)):
    id = looseFramLeft[getLooseFrame(currModifier)]
  elif (currTile == ord(tiles15Opener) and tileLeft == ord(tiles0Empty) and
      custom.tblLevelType[currentLevel] == 0):
    id = 148 # left half of open button with no floor to the left
  else:
    id = tileTable[currTile].baseId

  discard ptrAddTable(ord(idChtab6Environment), int32(id), sbyte(drawXh), 0,
      int32(word(tileTable[currTile].baseY) + ybottom), ord(blitters10hTransp), 0)


const
  spikesFramLeft: array[10, byte] = [0'u8, 128'u8, 129'u8, 130'u8, 131'u8,
      132'u8, 131'u8, 129'u8, 128'u8, 0'u8]
  potionFramBubb: array[8, byte] = [0'u8, 16'u8, 17'u8, 18'u8, 19'u8, 20'u8,
      21'u8, 22'u8]
  chomperFram1: array[8, byte] = [3'u8, 2'u8, 0'u8, 1'u8, 4'u8, 3'u8, 3'u8, 0'u8]
  chomperFramBot: array[6, byte] = [101'u8, 102'u8, 103'u8, 104'u8, 105'u8, 0'u8]
  chomperFramTop: array[6, byte] = [0'u8, 0'u8, 111'u8, 112'u8, 113'u8, 0'u8]
  chomperFramY: array[5, byte] = [0'u8, 0'u8, 0x25, 0x2F, 0x32]

# seg008:0B2B
proc drawTileAnim() =
  var
    color: word
    potSize: word
    var4: word
  potSize = 0
  color = 12 # red
  case (currTile)
  of ord(tiles2Spike):
    discard ptrAddTable(ord(idChtab6Environment), int32(spikesFramLeft[
        getSpikeFrame(currModifier)]), sbyte(drawXh), 0, int32(drawMainY) - 2,
            ord(blitters10hTransp), 0)
  of ord(tiles10Potion):
    case((currModifier and 0xF8) shr 3)
    of 0:
      return #empty
    of 5, 6: # hurt, open
      color = 9 # blue
    of 3, 4: # slow fall, upside down
      color = 10 # green
      # fallthrough!
      potSize = 1
    of 2: # life
      potSize = 1
    else:
      discard

    discard addBacktable(ord(idChtab1Flameswordpotion), 23, sbyte(drawXh) + 3,
        1, int32(uint16(drawMainY) - (potSize shl 2) - 14), ord(
            blitters40hMono), 0)
    discard addForetable(ord(idChtab1Flameswordpotion), int32(potionFramBubb[
        currModifier and 0x7]), sbyte(drawXh) + 3, 1, int32(uint16(drawMainY) -
            (potSize shl 2) - 14), int32(color + ord(blitters40hMono)), 0)
  of ord(tiles22Sword):
    discard addMidtable(ord(idChtab1Flameswordpotion), int32(currModifier ==
        1) + 10, sbyte(drawXh), 0, int32(drawMainY) - 3, ord(blitters10hTransp),
        byte(currModifier == 1))
  of ord(tiles18Chomper):
    var4 = chomperFram1[min(currModifier and 0x7F, 6)]
    discard addBacktable(ord(idChtab6Environment), int32(chomperFramBot[var4]),
        sbyte(drawXh), 0, int32(drawMainY), ord(blitters10hTransp), 0)
    if (currModifier and 0x80) != 0: # blood
      discard addBacktable(ord(idChtab6Environment), int32(var4 + 114), sbyte(
          drawXh) + 1, 4, int32(drawMainY) - 6, ord(blitters4ChMono12), 0)

    discard addBacktable(ord(idChtab6Environment), int32(chomperFramTop[var4]),
        sbyte(drawXh), 0, int32(drawMainY - int16(chomperFramY[var4])), ord(
            blitters10hTransp), 0)
  else:
    discard


const
  spikesFramFore: array[10, byte] = [0'u8, 139'u8, 140'u8, 141'u8, 142'u8,
      143'u8, 142'u8, 140'u8, 139'u8, 0'u8]
  chomperFramFor: array[6, byte] = [106'u8, 107'u8, 108'u8, 109'u8, 110'u8, 0'u8]
  wallFramMain: array[4, byte] = [8'u8, 10'u8, 6'u8, 4'u8]

# seg008:0D15
proc drawTileFore() =
  var
    ybottom: word
    xh: byte
    potionType: word
    id: word
    var2: word

  if (tileLeft == ord(tiles4Gate) and Kid.currRow == drawnRow and Kid.currCol ==
      drawnCol - 1 and word(Kid.room) != roomR):
    drawGateFore()

  case (currTile)
  of ord(tiles2Spike):
    discard addForetable(ord(idChtab6Environment), int32(spikesFramFore[
        getSpikeFrame(currModifier)]), sbyte(drawXh), 0, int32(drawMainY - 2),
            ord(blitters10hTransp), 0)
  of ord(tiles18Chomper):
    var2 = chomperFram1[min(currModifier and 0x7F, 6)]
    discard addForetable(ord(idChtab6Environment), int32(chomperFramFor[var2]),
        sbyte(drawXh), 0, int32(drawMainY), ord(blitters10hTransp), 0)
    if (currModifier and 0x80) != 0:
      discard addForetable(ord(idChtab6Environment), int32(var2 + 119), sbyte(
          drawXh) + 1, 4, int32(drawMainY - 6), ord(blitters4ChMono12), 0)
  of ord(tiles20Wall):
    if (custom.tblLevelType[currentLevel] == 0 or (custom.enableWdaInPalace !=
        0) or graphicsMode != ord(gmMcgaVga)):
      discard addForetable(ord(idChtab7Environmentwall), int32(wallFramMain[
          currModifier and 0x7F]), sbyte(drawXh), 0, int32(drawMainY), ord(
          blitters0NoTransp), 0)
    if (graphicsMode != ord(gmCga) and graphicsMode != ord(gmHgaHerc)):
      wallPattern(1, 1)
  else:
    id = tileTable[currTile].foreId
    if (id == 0):
      return
    if (currTile == ord(tiles10Potion)):
      # large pots are drawn for potion types 2, 3, 4
      potionType = word( (currModifier and 0xF8) shr 3)
      if (potionType < 5 and potionType >= 2'u16):
        id = 13 # small pot = 12, large pot = 13

    xh = byte(word(tileTable[currTile].foreX) + drawXh)
    ybottom = cast[word](tileTable[currTile].foreY + drawMainY)
    if (currTile == ord(tiles10Potion)):
      # potions look different in the dungeon and the palace
      if (custom.tblLevelType[currentLevel] != 0):
        id += 2
      discard addForetable(ord(idChtab1Flameswordpotion), int32(id), sbyte(xh),
          6, int32(ybottom), ord(blitters10hTransp), 0)
    else:
      if ((currTile == ord(tiles3Pillar) and custom.tblLevelType[
          currentLevel] == 0) or (currTile >= ord(tiles27LatticeSmall) and
          currTile < ord(tiles30TorchWithDebris))):
        discard addForetable(ord(idChtab6Environment), int32(id), sbyte(xh), 0,
            int32(ybottom), ord(blitters0NoTransp), 0)
      else:
        discard addForetable(ord(idChtab6Environment), int32(id), sbyte(xh), 0,
            int32(ybottom), ord(blitters10hTransp), 0)


# seg008:0FF6
proc getLooseFrame(modifier: byte): int32 =
  # Don't display garbled tiles if the delay is greater than the default.
  var
    mdfr: byte = modifier
  if ((modifier and 0x80) != 0 or custom.looseFloorDelay > 11):
    mdfr = byte(mdfr and 0x7F)
    if (mdfr > 10):
      return 1
  return int32(mdfr)


# Get an image, with index and NULL checks.
proc getImage(chtabId: int16, id: int32): ImageType =
  if (chtabId < 0 or chtabId > chtabAddrs.len):
    echo("Tried to use chtab " & $(chtabId) & " not in 0.." & $(chtabAddrs.len))
    return nil

  var
    chtab: ChtabType = chtabAddrs[chtabId]
  if (chtab == nil):
    echo("Tried to use null chtab " & $(chtabId))
    return nil

  if (id < 0 or id >= int32(chtab.nImages)):
    if (id != 255):
      echo("Tried to use image " & $(id) & " of chtab " & $(chtabId) &
          ", not in 0.." & $(chtab.nImages-1))
    return nil

  return chtab.images[id]


# seg008:10A8
proc addBacktable(chtabId: int16, id: int32, xh, xl: sbyte, ybottom,
    blit: int32, peel: byte): int32 =
  var
    index: word
  if (id == 0):
    return 0

  index = word(tableCounts[0])
  if (index >= 200'u16):
    showDialog("BackTable Overflow")
    return 0 # added

  var
    backtableItem: ptr BackTableType = addr(backtable[index])
  backtableItem.xh = xh
  backtableItem.xl = xl
  backtableItem.chtabId = byte(chtabId)
  backtableItem.id = byte(id - 1)
  var
    image: ImageType = getImage(chtabId, id - 1)
  if (image == nil):
    return 0

  backtableItem.y = int16(ybottom - image.h + 1)
  backtableItem.blit = blit
  if (drawMode) != 0:
    drawBackFore(0, int32(index))

  inc(tableCounts[0])
  return 1


# seg008:1017
proc addForetable(chtabId: int16, id: int32, xh, xl: sbyte, ybottom,
    blit: int32, peel: byte): int32 =
  var
    index: word
  if (id == 0):
    return 0
  index = word(tableCounts[1])
  if (index >= 200'u16):
    showDialog("ForeTable Overflow")
    return 0 # added

  var
    foretableItem: ptr BackTableType = addr(foretable[index])
  foretableItem.xh = xh
  foretableItem.xl = xl
  foretableItem.chtabId = byte(chtabId)
  foretableItem.id = byte(id - 1)
  var
    image: ImageType = getImage(chtabId, id - 1)
  if (image == nil):
    return 0

  foretableItem.y = cast[int16](ybottom - image.h + 1)
  foretableItem.blit = blit
  if (drawMode) != 0:
    drawBackFore(1, int32(index))

  inc(tableCounts[1])
  return 1


# seg008:113A
proc addMidtable(chtabId: int16, id: int32, xh, xl: sbyte, ybottom, blit: int32,
    peel: byte): int32 =
  var
    index: word
  if (id == 0):
    return 0

  index = word(tableCounts[3])
  if (index >= 50'u16):
    showDialog("MidTable Overflow")
    return 0 # added

  var
    midtableItem: ptr MidTableType = addr(midtable[index])
  midtableItem.xh = xh
  midtableItem.xl = xl
  midtableItem.chtabId = byte(chtabId)
  midtableItem.id = byte(id - 1)
  var
    image: ImageType = getImage(chtabId, id - 1)
    blitI: int32 = blit
  if (image == nil):
    return 0

  midtableItem.y = int16(ybottom - image.h + 1)
  if (objDirection == ord(dir0Right) and (chtabFlipClip[chtabId] != 0)):
    blitI = blit + 0x80

  midtableItem.blit = blitI
  midtableItem.peel = peel
  midtableItem.clip.left = objClipLeft
  midtableItem.clip.right = objClipRight
  midtableItem.clip.top = objClipTop
  midtableItem.clip.bottom = objClipBottom
  if (drawMode) != 0:
    drawMid(int(index))

  inc(tableCounts[3])
  return 1


# seg008:1208
proc addPeel(left, right, top, height: int32) =
  var
    rect: RectType
  if (peelsCount >= 50):
    showDialog("Peels OverFlow")
    return # added

  rect.left = int16(left)
  rect.right = int16(right)
  rect.top = int16(top)
  rect.bottom = int16(top + height)
  peelsTable[peelsCount] = readPeelFromScreen(rect)
  inc(peelsCount)


# seg008:1254
proc addWipetable(layer: sbyte, left, bottom: int16, height: sbyte,
    width: int16, color: sbyte) =
  var
    index: word
  index = word(tableCounts[2])
  if (index >= 300'u16):
    showDialog("WipeTable Overflow")
    return # added

  var
    wipetableItem: WipeTableType = wipetable[index]
  wipetableItem.left = left
  wipetableItem.bottom = bottom + 1
  wipetableItem.height = height
  wipetableItem.width = width
  wipetableItem.color = color
  wipetableItem.layer = layer
  if (drawMode) != 0:
    drawWipe(int(index))

  inc(tableCounts[2])


# seg008:12BB
proc drawTable(whichTable: int32) =
  var
    count: int16
  count = tableCounts[whichTable]
  for index in 0 ..< count:
    if (whichTable == 3):
      drawMid(index)
    else:
      drawBackFore(whichTable, index)


# seg008:12FE
proc drawWipes(which: int32) =
  var
    count: word
  count = word(tableCounts[2])
  for index in 0..<int(count):
    if (which == wipetable[index].layer):
      drawWipe(int(index))


# seg008:133B
proc drawBackFore(whichTable, index: int32) =
  var
    image: ImageType
    mask: ImageType
    tableEntry: ptr BackTableType
  if (whichTable == 0):
    tableEntry = addr(backtable[index])
  else:
    tableEntry = addr(foretable[index])

  image = getImage(int16(tableEntry.chtabId), int32(tableEntry.id))
  mask = image
  drawImage(image, mask, int32(tableEntry.xh) * 8 + int32(tableEntry.xl), int32(
      tableEntry.y), tableEntry.blit)


proc hflip(input: Surface): Surface =
  var
    width: int32 = input.w
    height: int32 = input.h
    targetX: int32

  var
  # The simplest way to create a surface with same format as input:
    output: Surface = convertSurface(input, input.format, 0)
  discard setSurfacePalette(output, input.format.palette)
  # The copied image will be overwritten anyway.
  if (output == nil):
    sdlperror("SDL_ConvertSurface")
    quitPoP(1)

  discard setSurfaceBlendMode(input, BLENDMODE_NONE)
  # Temporarily turn off alpha and colorkey on input. So we overwrite the output image.
  discard setColorKey(input, 0, 0)
  discard setColorKey(output, 0, 0)
  discard setSurfaceAlphaMod(input, 255)

  targetX = width - 1
  for sourceX in 0 ..< width:
    var
      srcrect: Rect = Rect(x: sourceX, y: 0, w: 1, h: height)
      dstrect: Rect = Rect(x: targetX, y: 0, w: 1, h: height)
    if (blitSurface(input, addr(srcrect), output, addr(dstrect)) != 0):
      sdlperror("SDL_BlitSurface")
      quitPoP(1)
    dec(targetX)

  return output


# seg008:140C
proc drawMid(index: int32) =
  var
    needFreeMask: word
    imageId: word
    mask: ImageType
    chtabId: word
    blitFlip: word
    ypos: int16
    xpos: int16
    midtableEntry: ptr MidTableType
    blit: word
    needFreeImage: word
    image: ImageType
#  word imageFlipped

  blitFlip = 0
  needFreeImage = 0
  needFreeMask = 0
  midtableEntry = addr(midtable[index])
  imageId = midtableEntry.id
  chtabId = midtableEntry.chtabId
  image = getImage(int16(chtabId), int32(imageId))
  mask = image
  xpos = int16(midtableEntry.xh) * 8 + int16(midtableEntry.xl)
  ypos = midtableEntry.y
  blit = word(midtableEntry.blit)
  if (blit and 0x80) != 0:
    blitFlip = 0x8000
    blit = blit and 0x7F


  if (chtabFlipClip[chtabId]) != 0:
    setClipRect(midtableEntry.clip)
    if (chtabId != ord(idChtab0Sword)):
      xpos = calcScreenXCoord(xpos)


  if (blitFlip) != 0:
    xpos -= int16(image.w)
    # for this version:
    needFreeImage = 1
    image = hflip(image)


  if (midtableEntry.peel) != 0:
    addPeel(roundXposToByte(xpos, 0), roundXposToByte(image.w + xpos, 1), ypos, image.h)

  drawImage(image, mask, xpos, ypos, int32(blit))

  if (chtabFlipClip[chtabId]) != 0:
    resetClipRect()

  if (needFreeImage) != 0:
    #freeFar(image)
    freeSurface(image)

  if (needFreeMask) != 0:
    free(mask)


# seg008:167B
proc drawImage(image, mask: ImageType, xpos, ypos, blit: int32) =
  var
    rect: RectType
  case blit
  of ord(blitters10hTransp):
    drawImageTransp(image, mask, xpos, ypos)
  of ord(blitters9Black):
    discard method6BlitImgToScr(mask, xpos, ypos, ord(blitters9Black))
  of ord(blitters0NoTransp), ord(blitters2Or), ord(blitters3Xor):
    discard method6BlitImgToScr(image, xpos, ypos, blit)
  else:
    if (blit >= 0x100):
      discard method6BlitImgToScr(mask, xpos, ypos, blit)
    else:
      discard method3BlitMono(image, xpos, ypos, 0, byte(blit and 0xBF))
  if (needDrects) != 0:
    rect.left = int16(xpos)
    rect.right = int16(xpos)
    rect.right += int16(image.w)
    rect.top = int16(ypos)
    rect.bottom = int16(ypos)
    rect.bottom += int16(image.h)
    addDrect(rect)


# seg008:1730
proc drawWipe(index: int32) =
  var
    rect: RectType
    wPtr: ptr WipeTableType
  wPtr = addr(wipetable[index])
  rect.left = wPtr.left
  rect.right = wPtr.left
  rect.right += wPtr.width
  rect.bottom = wPtr.bottom
  rect.top = wPtr.bottom
  rect.top -= wPtr.height
  drawRect(rect, wPtr.color)
  if (needDrects) != 0:
    addDrect(rect)


var
  # data:4E8C
  gateTopY: word
  # data:4CB6
  gateOpenness: word
  # data:436C
  gateBottomY: word

# seg008:178E
proc calcGatePos() =
  gateTopY = word(drawBottomY - 62)
  gateOpenness = word( (min(modifierLeft, 188) shr 2) + 1)
  gateBottomY = word(drawMainY) - gateOpenness


const
  # data:2785
  doorFramSlice: array[9, byte] = [67'u8, 59'u8, 58'u8, 57'u8, 56'u8, 55'u8,
      54'u8, 53'u8, 52'u8]

# seg008:17B7
proc drawGateBack() =
  var
    ybottom: int16
    var2: word
  calcGatePos()
  if (gateBottomY + 12 < word(drawMainY)):
    discard addBacktable(ord(idChtab6Environment), 50, sbyte(drawXh), 0, int32(
        gateBottomY), ord(blitters0NoTransp), 0)
  else:
    # The following line (erroneously) erases the top-right of the tile below-left (because it is drawn non-transparently).
    # -- But it draws something that was already drawn! (in drawTileRight()).
    discard addBacktable(ord(idChtab6Environment), int32(tileTable[ord(
        tiles4Gate)].rightId), sbyte(drawXh), 0, int32(int16(tileTable[ord(
        tiles4Gate)].rightY) + drawMainY), ord(blitters0NoTransp), 0)
    # And this line tries to fix it. But it fails if it was a gate or a pillar.
    if (canSeeBottomleft()) != 0:
      drawTileTopright()
    when (FIX_GATE_DRAWING_BUG):
      if (fixes.fixGateDrawingBug) != 0:
        drawTileAnimTopright() # redraw the erased top-right section of the gate below-left
    # The following 3 lines draw things that are drawn after this anyway.
    drawTileBottom(0)
    drawLoose(0)
    drawTileBase()
    discard addBacktable(ord(idChtab6Environment), 51, sbyte(drawXh), 0, int32(
        gateBottomY - 2), ord(blitters10hTransp), 0)
  ybottom = int16(gateBottomY - 12)
  if (ybottom < 192):
    while ybottom >= 0 and ybottom > 7 and word(ybottom - 7) > gateTopY:
      discard addBacktable(ord(idChtab6Environment), 52, sbyte(drawXh), 0,
          ybottom, ord(blitters0NoTransp), 0)
      ybottom -= 8
  var2 = word(ybottom) - gateTopY + 1
  if (var2 > 0'u16 and var2 < 9):
    discard addBacktable(ord(idChtab6Environment), int32(doorFramSlice[var2]),
        sbyte(drawXh), 0, int32(ybottom), ord(blitters0NoTransp), 0)


# seg008:18BE
proc drawGateFore() =
  var
    ybottom: int16
  calcGatePos()
  discard addForetable(ord(idChtab6Environment), 51, sbyte(drawXh), 0, int32(
      gateBottomY - 2), ord(blitters10hTransp), 0)
  ybottom = int16(gateBottomY - 12)
  if (ybottom < 192):
    while ybottom >= 0 and ybottom > 7 and word(ybottom - 7) > gateTopY:
      discard addForetable(ord(idChtab6Environment), 52, sbyte(drawXh), 0,
          int32(ybottom), ord(blitters10hTransp), 0)
      ybottom -= 8


# seg008:1937
proc alterModsAllrm() =
  when (USE_COLORED_TORCHES):
    memset(addr(torchColors[1]), 0, csize_t(sizeof(torchColors)))

  let
    rooms = if level.usedRooms == 25: 24 else: level.usedRooms

  for room in 1..rooms:
    getRoomAddress(room)
    roomL = level.roomlinks[room-1].left
    roomR = level.roomlinks[room-1].right
    for tilepos in 0 ..< 30:
      loadAlterMod(tilepos)


proc readAdjTileModifInCurrRoom(adjTileIndex: int32): int32 =
  when(USE_FAKE_TILES):
    return int32(currRoomModif[adjTileIndex])
  else:
    return 0
proc readAdjTileModifInExternalRoom(adjTileIndex: int32): int32 =
  when(USE_FAKE_TILES):
    return int32(level.bg[adjTileIndex])
  else:
    return 0
proc WALL_CONNECTION_CONDITION(adjTile, adjTileModif: int32): bool =
  when(USE_FAKE_TILES):
    return (adjTile == ord(tiles20Wall) and adjTileModif != 4 and (
        adjTileModif shr 4) != 4 and adjTileModif != 6 and (
        adjTileModif shr 4) != 6) or (adjTile == ord(tiles0Empty) and (
        adjTileModif == 5 or adjTileModif == 13 or (adjTileModif >= 50 and
        adjTileModif <= 53))) or (adjTile == ord(tiles1Floor) and (
        adjTileModif == 5 or adjTileModif == 13 or (adjTileModif >= 50 and
        adjTileModif <= 53)))
  else:
    return adjTile == ord(tiles20Wall)


# seg008:198E
proc loadAlterMod(tilepos: int32) =
  var
    wallToRight: word
    tiletype: word
    wallToLeft: word
    currTileModif: ptr byte
  currTileModif = addr(currRoomModif[tilepos])
  tiletype = word(currRoomTiles[tilepos] and 0x1F)
  case (tiletype)
  of ord(tiles4Gate):
    if (currTileModif[] == 1):
      currTileModif[] = 188
    else:
      currTileModif[] = 0
  of ord(tiles11Loose):
    currTileModif[] = 0
  of ord(tiles10Potion):
    currTileModif[] = currTileModif[] shl 3
    when (USE_COPYPROT):
      if (currentLevel == 15):
        # Copy protection
        if (copyprotRoom[copyprotPlac] == loadedRoom and
          int32(copyprotTile[copyprotPlac]) == tilepos
        ):
          currTileModif[] = 6 shl 3 # place open potion
  of ord(tiles20Wall):
    var
      storedModif: byte = currTileModif[]
      #*currTileModif shl= 7 # original: "no blue" mod becomes most significant bit
    if (storedModif == 1):
      currTileModif[] = 0x80
    else:
      currTileModif[] = (storedModif shl 4)
    # retain three bits more information:
    # most significant bit:       1 ==> "no blue"
    # next 3 bits:                for displaying various fake tiles (invisible walls)
    # ..
    # least significant 2 bits:   wall to left/right?
    # fallthrough: not done yet, just moving to another scope
    #  goto labelWallContinued
    if (graphicsMode != ord(gmCga) and graphicsMode != ord(gmHgaHerc)):
      wallToRight = 1
      wallToLeft = 1
      var
        adjTileIndex, adjTile: int32
      if (tilepos mod 10 == 0):
        if roomL != 0:
          adjTileIndex = 30*int32(roomL-1)+tilepos+9
          adjTile = int32(level.fg[adjTileIndex] and 0x1F)
          var
            adjTileModif: int32 = readAdjTileModifInExternalRoom(
                adjTileIndex) # only executed when fake tiles are enabled
          wallToLeft = word(WALL_CONNECTION_CONDITION(adjTile, adjTileModif))
      else:
        adjTileIndex = tilepos-1
        adjTile = int32(currRoomTiles[adjTileIndex] and 0x1F)
        var
          adjTileModif: int32 = readAdjTileModifInCurrRoom(adjTileIndex)
        wallToLeft = word(WALL_CONNECTION_CONDITION(adjTile, adjTileModif))
      if (tilepos mod 10 == 9):
        if roomR != 0:
          adjTileIndex = 30*int(roomR-1)+tilepos-9
          adjTile = int32(level.fg[adjTileIndex] and 0x1F)
          var
            adjTileModif: int32 = readAdjTileModifInExternalRoom(adjTileIndex)
          wallToRight = word(WALL_CONNECTION_CONDITION(adjTile, adjTileModif))
      else:
        adjTileIndex = tilepos+1
        adjTile = int32(currRoomTiles[adjTileIndex] and 0x1F)
        var
          adjTileModif = readAdjTileModifInCurrRoom(adjTileIndex)
        wallToRight = word(WALL_CONNECTION_CONDITION(adjTile, adjTileModif))
      when (USE_FAKE_TILES):
        if (tiletype == ord(tiles1Floor) or tiletype == ord(tiles0Empty)):
          if (wallToLeft and wallToRight) != 0:
            currTileModif[] = 53
          elif (wallToLeft) != 0:
            currTileModif[] = 52
          elif (wallToRight) != 0:
            currTileModif[] = 51
      if (wallToLeft and wallToRight) != 0:
        currTileModif[] = byte(currTileModif[] or 3)
      elif (wallToLeft) != 0:
        currTileModif[] = byte(currTIleModif[] or 2)
      elif (wallToRight) != 0:
        currTileModif[] = byte(currTIleModif[] or 1)
    else:
      currTileModif[] = 3
    # when (USE_FAKE_TILES):
    # Need to deal with the possibility that certain tiles can impersonate walls
  of ord(tiles0Empty), ord(tiles1Floor):
    when(not(USE_FAKE_TILES)):
      if ((currTileModif[] and 7) == 5):
        # if tile is a fake wall, fall through
        if (graphicsMode != ord(gmCga) and graphicsMode != ord(gmHgaHerc)):
          wallToRight = 1
          wallToLeft = 1
          var
            adjTileIndex, adjTile: int32
          if (tilepos mod 10 == 0):
            if (roomL) != 0:
              adjTileIndex = 30*int(roomL-1)+tilepos+9
              adjTile = int32(level.fg[adjTileIndex] and 0x1F)
              var
                adjTileModif: int32 = readAdjTileModifInExternalRoom(
                    adjTileIndex) # only executed when fake tiles are enabled
              wallToLeft = word(WALL_CONNECTION_CONDITION(adjTile, adjTileModif))
          else:
            adjTileIndex = tilepos-1
            adjTile = int32(currRoomTiles[adjTileIndex] and 0x1F)
            var
              adjTileModif: int32 = readAdjTileModifInCurrRoom(adjTileIndex)
            wallToLeft = word(WALL_CONNECTION_CONDITION(adjTile, adjTileModif))
          if (tilepos mod 10 == 9):
            if (roomR) != 0:
              adjTileIndex = 30*int(roomR-1)+tilepos-9
              adjTile = int32(level.fg[adjTileIndex] and 0x1F)
              var
                adjTileModif: int32 = readAdjTileModifInExternalRoom(adjTileIndex)
              wallToRight = word(WALL_CONNECTION_CONDITION(adjTile, adjTileModif))
          else:
            adjTileIndex = tilepos+1
            adjTile = int32(currRoomTiles[adjTileIndex] and 0x1F)
            var
              adjTileModif: int32 = readAdjTileModifInCurrRoom(adjTileIndex)
            wallToRight = word(WALL_CONNECTION_CONDITION(adjTile, adjTileModif))
          when (USE_FAKE_TILES):
            if (tiletype == ord(tiles1Floor) or tiletype == ord(tiles0Empty)):
              if (wallToLeft and wallToRight):
                currTileModif[] = 53
              elif (wallToLeft):
                currTileModif = 52
              elif (wallToRight):
                currTileModif[] = 51
          if (wallToLeft and wallToRight) != 0:
            currTileModif[] = byte(currTileModif[] or 3)
          elif (wallToLeft) != 0:
            currTileModif[] = byte(currTIleModif[] or 2)
          elif (wallToRight) != 0:
            currTileModif[] = byte(currTIleModif[] or 1)
        else:
          currTileModif[] = 3
    else:
      discard
  of ord(tiles19Torch), ord(tiles30TorchWithDebris):
    when (USE_COLORED_TORCHES):
      torchColors[loadedRoom][tilepos] = currTileModif[]
      currTileModif[] = 0
    else:
      discard
  else:
    discard


# seg008:1AF8
proc drawMoving() =
  drawMobs()
  drawPeople()
  redrawNeededTiles()


# seg008:1B06
proc redrawNeededTiles() =
  var
    savedDrawnRoom: word
  loadLeftroom()
  drawObjtableItemsAtTile(30)
  drawnRow = 3
  # for (drawnRow = 3; drawnRow--; ) {
  for i in countdown(2'i16, 0'i16):
    drawnRow = i
    loadRowbelow()
    drawBottomY = 63 * drawnRow + 65
    drawMainY = drawBottomY - 3
    drawnCol = 0
    # for (drawnCol = 0; drawnCol < 10; ++drawnCol) {
    for j in 0'i16..<10'i16:
      drawnCol = j
      loadCurrAndLeftTile()
      redrawNeeded(int16(tblLine[drawnRow] + word(drawnCol)))
  drawnRow = -1

  savedDrawnRoom = drawnRoom
  drawnRoom = roomA
  loadRoomLinks()
  loadLeftroom()
  drawnRow = 2
  loadRowbelow()
  drawnCol = 0
  # for (drawnCol = 0; drawnCol < 10; ++drawnCol) {
  while drawnCol < 10:
    loadCurrAndLeftTile()
    drawMainY = -1
    drawBottomY = 2
    redrawNeededAbove(drawnCol)
    inc(drawnCol)
  drawnRoom = savedDrawnRoom
  loadRoomLinks()
  # Note: the C code calls the function with an argument of -1, which is equal to 255 for uint8
  drawObjtableItemsAtTile(255)


# seg008:1BCB
proc drawTileWipe(height: byte) =
  addWipetable(0, int16(drawXh*8), drawBottomY, sbyte(height), 4*8, 0)


# seg008:1BEB
proc drawTables() =
  drectsCount = 0
  currentTargetSurface = offscreenSurface
  if (isBlindMode) != 0:
    var
      rectTopIntern: RectType = rectTop
    drawRect(rectTopIntern, 0)

  restorePeels()
  drawWipes(0)
  drawTable(0) # backtable
  #printf("midtableCount = modd\n", midtableCount) # debug
  drawTable(3) # midtable
  drawWipes(1)
  drawTable(1) # foretable
  currentTargetSurface = onscreenSurface
  showCopyprot(1)


# seg008:1C4E
proc restorePeels() =
  while (peelsCount > 0):
    dec(peelsCount)
    if (needDrects) != 0:
      addDrect(peelsTable[peelsCount].rect) # ?

    restorePeel(peelsTable[peelsCount])
  dec(peelsCount)

  peelsCount = 0


# seg008:1C8F
proc addDrect(source: var RectType) =
  var
    targetRect: RectType
  for index in 0..<drectsCount:
    var
      intermediateRect = shrink2Rect(targetRect, source, -1, -1)
    if (intersectRect(targetRect, intermediateRect, drects[index])) != 0:
      discard unionRect(drects[index], drects[index], source)
      return
  if (drectsCount >= 30):
    showDialog("DRects Overflow")
    return # added

  drects[drectsCount] = source
  inc(drectsCount)


# seg008:1D29
proc drawLeveldoor() =
  var
    var6: int16
    ybottom: int16
    leveldoorWidth: int16
    xLow: sbyte
  ybottom = drawMainY - 13
  leveldoorRight = (drawXh shl 3) + 48
  if (custom.tblLevelType[currentLevel]) != 0:
    leveldoorRight += 8
  discard addBacktable(ord(idChtab6Environment), 99, sbyte(drawXh + 1), 0,
      int32(ybottom), ord(blitters0NoTransp), 0)
  if (modifierLeft) != 0:
    if (word(level.startRoom) != drawnRoom):
      discard addBacktable(ord(idChtab6Environment), 144, sbyte(drawXh + 1), 0,
          int32(ybottom - 4), ord(blitters0NoTransp), 0)
    else:
      leveldoorWidth = if (custom.tblLevelType[currentLevel] ==
          0): 39 else: 48
      xLow = if (custom.tblLevelType[currentLevel] ==
          0): 2 else: 0 # dungeon level doors are shifted 2px to the right
      addWipetable(0, int16(8'u16*(drawXh + 1) + word(xLow)), int16(ybottom -
          4), 45, leveldoorWidth, 0)

  leveldoorYbottom = word(ybottom - (modifierLeft and 3) - 48)
  var6 = int16(ybottom - modifierLeft)
  discard addBacktable(ord(idChtab6Environment), 33, sbyte(drawXh + 1), 0,
      int32(leveldoorYbottom), ord(blitters0NoTransp), 0)
  while word(var6) > leveldoorYbottom:
    leveldoorYbottom += 4
    discard addBacktable(ord(idChtab6Environment), 33, sbyte(drawXh + 1), 0,
        int32(leveldoorYbottom), ord(blitters0NoTransp), 0)

# runs at least once?
  discard addBacktable(ord(idChtab6Environment), 34, sbyte(drawXh + 1), 0,
      int32(drawMainY - 64), ord(blitters0NoTransp), 0)


# TODO: RoomModif and currRoomTiles are seqs or pointers
# # seg008:1E0C
proc getRoomAddress(room: int32) =
  loadedRoom = word(room)
  if (room) != 0:
    currRoomTiles = cast[ptr UncheckedArray[byte]](addr(level.fg[(room-1)*30]))
    currRoomModif = cast[ptr UncheckedArray[byte]](addr(level.bg[(room-1)*30]))


const
  # data:286A
  floorLeftOverlay: array[8, word] = [32'u16, 151'u16, 151'u16, 150'u16,
      150'u16, 151'u16, 32'u16, 32'u16]

# seg008:1E3A
proc drawFloorOverlay() =
  when (FIX_BIGPILLAR_CLIMB):
    if (tileLeft != ord(tiles0Empty)):
      # Bug: When climbing up to a floor with a big pillar top behind, turned right, Kid sees through floor.
      # The bigpillarTop tile should be treated similarly to an empty tile here.
      if ((fixes.fixBigpillarClimb == 0) or (tileLeft != ord(
          tiles9BigpillarTop))):
        return


  else:
    if (tileLeft != ord(tiles0Empty)):
      return
  if (currTile == ord(tiles1Floor) or
    currTile == ord(tiles3Pillar) or
    currTile == ord(tiles5Stuck) or
    currTile == ord(tiles19Torch)
  ):
    # frames 137..144: climb
    # index overflow here?
    if (Kid.frame >= ord(frame137Climbing3) and Kid.frame <= ord(
        frame144Climbing10)):
      discard addMidtable(ord(idChtab6Environment), int16(floorLeftOverlay[
          Kid.frame - 137]), sbyte(drawXh), 0, int32(int16(currTile == ord(
              tiles5Stuck)) +
          drawMainY), ord(blitters10hTransp), 0)
    else:
      # triggered by 02-random-broken
      echo("drawFloorOverlay: attempted to draw floor overlay with frame " & $(
          int32(Kid.frame)) & " not in 137..144")
      #quit(1)

    ptrAddTable = addMidTable
    drawTileBottom(0)
    ptrAddTable = addBackTable
  else:
    drawOtherOverlay()


# seg008:1EB5
proc drawOtherOverlay() =
  var
    tiletype: byte
    modifier: byte
  if (tileLeft == ord(tiles0Empty)):
    ptrAddTable = addMidTable
    drawTile2()
  elif (currTile != ord(tiles0Empty) and drawnCol > 0 and
    getTileToDraw(int(drawnRoom), int32(drawnCol - 2), int32(drawnRow), addr(
        tiletype), addr(modifier), ord(tiles0Empty)) == ord(tiles0Empty)
  ):
    ptrAddTable = addMidTable
    drawTile2()
    ptrAddTable = addBackTable
    drawTile2()
    tileObjectRedraw[tblLine[drawnRow] + word(drawnCol)] = 0xFF

  ptrAddTable = addBackTable


# seg008:1F48
proc drawTile2() =
  drawTileRight()
  drawTileAnimRight()
  drawTileBase()
  drawTileAnim()
  drawTileBottom(0)
  drawLoose(0)


# seg008:1F67
proc drawObjtableItemsAtTile(tilepos: byte) =
  #printf("drawObjtableItemsAtTile(modd)\n",tilepos) # debug
  var
    objCount: int16
    objIndex: int16
  objCount = tableCounts[4]
  if (objCount) != 0:
    objIndex = objCount - 1
    nCurrObjs = 0
    # for (objIndex = objCount - 1, nCurrObjs = 0; objIndex >= 0; --objIndex) {
    while objIndex >= 0:
      if (objtable[objIndex].tilepos == tilepos):
        currObjs[nCurrObjs] = objIndex
        inc(nCurrObjs)
      dec(objIndex)
    if (nCurrObjs) != 0:
      sortCurrObjs()
      objIndex = 0
      # for (objIndex = 0; objIndex < nCurrObjs; ++objIndex) {
      while objIndex < nCurrObjs:
        drawObjtableItem(currObjs[objIndex])
        inc(objIndex)


# seg008:1FDE
proc sortCurrObjs() =
  var
    swapped: int16
    temp: int16
    last: int16
    index: int16
  # bubble sort
  last = nCurrObjs - 1
  swapped = 0
  index = 0
  while index < last:
    if (compareCurrObjs(index, index + 1)) != 0:
      temp = currObjs[index]
      currObjs[index] = currObjs[index + 1]
      currObjs[index + 1] = temp
      swapped = 1
    inc(index)
  # --last ?
  while swapped != 0:
    swapped = 0
    index = 0
    # for (swapped = index = 0; index < last; ++index) {
    while index < last:
      if (compareCurrObjs(index, index + 1)) != 0:
        temp = currObjs[index]
        currObjs[index] = currObjs[index + 1]
        currObjs[index + 1] = temp
        swapped = 1
      inc(index)


# seg008:203C
proc compareCurrObjs(index1, index2: int32): int32 =
  var
    objIndex1: int16
    objIndex2: int16
  objIndex1 = currObjs[index1]
  if (objtable[objIndex1].objType == 1):
    return 1
  objIndex2 = currObjs[index2]
  if (objtable[objIndex2].objType == 1):
    return 0
  if (objtable[objIndex1].objType == 0x80 and
    objtable[objIndex2].objType == 0x80
  ):
    return int32(objtable[objIndex1].y < objtable[objIndex2].y)
  else:
    return int32(objtable[objIndex1].y > objtable[objIndex2].y)

  return 1


# seg008:20CA
proc drawObjtableItem(index: int32) =
  case (loadObjFromObjtable(index))
  of 0, 4: # Kid, mirror image
    #printf("index = modd, objId = modd\n", index, objId) # debug
    if (objId == 0xFF):
      return
    # the Kid blinks a bit after uniting with shadow
    if ((unitedWithShadow != 0) and (unitedWithShadow mod 2) == 0):
      if (unitedWithShadow == 2):
        playSound(ord(sound41EndLevelMusic)) # united with shadow

      discard addMidtable(int16(objChtab), int32(objId + 1), sbyte(objXh),
          sbyte(objXl), int32(objY), ord(blitters2Or), 1)
      discard addMidtable(int16(objChtab), int32(objId + 1), sbyte(objXh),
          sbyte(objXl + 1), int32(objY), ord(blitters3Xor), 1)
    else:
      discard addMidtable(int16(objChtab), int32(objId + 1), cast[sbyte](objXh),
          cast[sbyte](objXl), int32(objY), ord(blitters10hTransp), 1)
  of 2, 3, 5: # Guard, sword, hurt splash
    discard addMidtable(int16(objChtab), int32(objId + 1), cast[sbyte](objXh),
        cast[sbyte](objXl), int32(objY), ord(blitters10hTransp), 1)
  of 1: # shadow
    if (unitedWithShadow == 2):
      playSound(ord(sound41EndLevelMusic)) # united with shadow

    discard addMidtable(int16(objChtab), int32(objId + 1), cast[sbyte](objXh),
        cast[sbyte](objXl), int32(objY), ord(blitters2Or), 1)
    discard addMidtable(int16(objChtab), int32(objId + 1), cast[sbyte](objXh),
        cast[sbyte](objXl + 1), int32(objY), ord(blitters3Xor), 1)
  of 0x80: # loose floor
    objDirection = ord(dirFFLeft)
    discard addMidtable(int16(objChtab), int32(looseFramLeft[objId]), cast[
        sbyte](objXh), sbyte(objXl), int32(objY - 3), ord(blitters10hTransp), 1)
    discard addMidtable(int16(objChtab), int32(looseFramBottom[objId]), cast[
        sbyte](objXh), cast[sbyte](objXl), int32(objY), 0, 1)
    discard addMidtable(int16(objChtab), int32(looseFramRight[objId]), cast[
        sbyte](objX + 4), cast[sbyte](objXl), int32(objY - 1), ord(
            blitters10hTransp), 1)
  else:
    discard


# seg008:2228
proc loadObjFromObjtable(index: int32): int32 =
  var
    currObj: ptr ObjtableType
  currObj = addr(objtable[index])
  objXh = cast[byte](currObj.xh)
  objX = currObj.xh
  objXl = byte(currObj.xl)
  objY = byte(currObj.y)
  objId = currObj.id
  objChtab = currObj.chtabId
  objDirection = currObj.direction
  objClipTop = currObj.clip.top
  objClipBottom = currObj.clip.bottom
  objClipLeft = currObj.clip.left
  objClipRight = currObj.clip.right
  return int32(currObj.objType)


# seg008:228A
proc drawPeople() =
  checkMirror()
  drawKid()
  drawGuard()
  resetObjClip()
  drawHp()


# seg008:22A2
proc drawKid() =
  if (Kid.room != 0 and word(Kid.room) == drawnRoom):
    addKidToObjtable()
    if (hitpDelta < 0):
      drawHurtSplash()
    addSwordToObjtable()


# seg008:22C9
proc drawGuard() =
  if (Guard.direction != ord(dir56None) and word(Guard.room) == drawnRoom):
    addGuardToObjtable()
    if (guardhpDelta < 0):
      drawHurtSplash()
    addSwordToObjtable()


# seg008:22F0
proc addKidToObjtable() =
  #printf("addKidToObjtable\n")
  loadkid()
  loadFramDetCol()
  loadFrameToObj()
  stuckLower()
  setCharCollision()
  setObjtileAtChar()
  redrawAtChar()
  redrawAtChar2()
  clipChar()
  addObjtable(0) # Kid


# seg008:2324
proc addGuardToObjtable() =
  var
    objType: word
  loadshad()
  loadFramDetCol()
  loadFrameToObj()
  stuckLower()
  setCharCollision()
  setObjtileAtChar()
  redrawAtChar()
  redrawAtChar2()
  clipChar()
  if (Char.charid == ord(charid1Shadow)):
    # Special event: shadow is clipped: may appear only right from the mirror
    if (currentLevel == custom.mirrorLevel and Char.room == custom.mirrorRoom):
      objClipLeft = 137
      objClipLeft += int16((custom.mirrorColumn - 4) * 32) # added
    objType = 1 # shadow
  else:
    objType = 2 # Guard
  addObjtable(byte(objType))


# seg008:2388
proc addObjtable(objType: byte) =
  var
    index: word
    entryAddr: ptr ObjtableType
  #printf("in addObjtable: objtableCount = modd\n",objtableCount) # debug
  index = word(tableCounts[4])
  inc(tableCounts[4])
  #printf("in addObjtable: objtableCount = modd\n",objtableCount) # debug
  if (index >= 50'u16):
    showDialog("ObjTable Overflow")
    return # added

  entryAddr = addr(objtable[index])
  entryAddr.objType = objType
  xToXhAndXl(int(objX), entryAddr.xh, entryAddr.xl)
  entryAddr.y = int16(objY)
  entryAddr.clip.top = objClipTop
  entryAddr.clip.bottom = objClipBottom
  entryAddr.clip.left = objClipLeft
  entryAddr.clip.right = objClipRight
  entryAddr.chtabId = objChtab
  entryAddr.id = objId
  entryAddr.direction = objDirection
  markObjTileRedraw(int(index))


# seg008:2423
proc markObjTileRedraw(index: int32) =
  #printf("markObjTileRedraw: objTile = modd\n", objTile) # debug
  objtable[index].tilepos = objTilepos
  if (objTilepos < 30):
    tileObjectRedraw[objTilepos] = 1


# seg008:2448
proc loadFrameToObj() =
  var
    chtabBase: word
  chtabBase = ord(idChtab2Kid)
  resetObjClip()
  loadFrame()
  objDirection = Char.direction
  objId = curFrame.image
  # top 6 bits of sword are the chtab
  objChtab = byte(chtabBase + (curFrame.sword shr 6))
  objX = int16( (charDxForward(curFrame.dx) shl 1) - 116)
  objY = cast[byte](curFrame.dy + Char.y)
  if (cast[sbyte](curFrame.flags) xor objDirection) >= 0:
    # 0x80: even/odd pixel
    inc(objX)


# seg008:24A8
proc showTime() =
  var
    sprintfTemp: string
    remSec: word
  if (Kid.alive < 0 and
    (when (FREEZE_TIME_DURING_END_MUSIC):
    (not((fixes.enableFreezeTimeDuringEndMusic != 0) and nextLevel !=
        currentLevel)) else: true) and
    (when (ALLOW_INFINITE_TIME):
    # prevent overflow
    (not(remMin == low(int16) and remTick == 1)) else: true) and
    remMin != 0 and
    (currentLevel < custom.victoryStopsTimeLevel or (currentLevel ==
        custom.victoryStopsTimeLevel and leveldoorOpen == 0)) and
    currentLevel < 15
  ):
    # Time passes
    dec(remTick)
    if (remTick == 0):
      remTick = 719 # 720=12*60 ticks = 1 minute
      dec(remMin)
      when(ALLOW_INFINITE_TIME):
        if (remMin > 0 and (remMin <= 5 or remMin mod 5 == 0)):
          isShowTime = 1
        elif (remMin < 0):
          isShowTime = if (not(remMin) mod 5 == 0): 1 else: 0
      else:
        if (remMin != 0 and (remMin <= 5 or remMin mod 5 == 0)):
          isShowTime = 1
    else:
      if (remMin == 1 and remTick mod 12 == 0):
        isShowTime = 1
        textTimeRemaining = 0
  if ((isShowTime != 0) and textTimeRemaining == 0):
    textTimeRemaining = 24
    textTimeTotal = 24
    if (remMin > 0):
      if (remMin == 1):
        remSec = (remTick + 1) div 12
        if (remSec == 1):
          sprintfTemp = "1 SECOND LEFT"
          textTimeRemaining = 12
          textTimeTotal = 12
        else:
          sprintfTemp = $(remSec) & " SECONDS LEFT"
      else:
        sprintfTemp = $(remMin) & " MINUTES LEFT"
      displayTextBottom(sprintfTemp)
    else:
      when (ALLOW_INFINITE_TIME):
        if (remMin < 0):
          if (not(remMin) == 0):
            # don't display time elapsed in the first minute
            textTimeRemaining = 0
            textTimeTotal = 0
            sprintfTemp = ""
          elif (not(remMin) == 1):
            sprintfTemp = "1 MINUTE PASSED"
          else:
            sprintfTemp = $(not(remMin)) & " MINUTES PASSED"
          displayTextBottom(sprintfTemp)
        elif (remMin == 0): # may also be negative, don't report "expired" in that case!
          displayTextBottom("TIME HAS EXPIRED!")
      else:
        displayTextBottom("TIME HAS EXPIRED!")
    isShowTime = 0


# seg008:25A8
proc showLevel() =
  when (FIX_LEVEL_14_RESTARTING):
    textTimeRemaining = 0
    textTimeTotal = 0
  var
    dispLevel: byte
    sprintfTemp: string
  dispLevel = byte(currentLevel)
  if (dispLevel != 0 and word(dispLevel) < custom.hideLevelNumberFromLevel and
      seamless == 0):
    if (dispLevel == 13):
      dispLevel = custom.level13LevelNumber
    textTimeRemaining = 24
    textTimeTotal = 24
    sprintfTemp = "LEVEL " & $(int(dispLevel))
    displayTextBottom(sprintfTemp)
    isShowTime = 1
  seamless = 0


# seg008:2602
proc calcScreenXCoord(logicalX: int16): int16 =
  return int16(int32(logicalX)*320 div 280)


# seg008:2627
proc freePeels() =
  while (peelsCount > 0):
    dec(peelsCount)
    freePeel(peelsTable[peelsCount])


# seg008:2644
proc displayTextBottom(text: string) =
  drawRect(rectBottomText, 0)
  showText(addr(rectBottomText), 0, 1, text)
  when not(USE_TEXT):
    SetWindowTitle(window, text)


# seg008:266D
proc eraseBottomText(arg0: int32) =
  drawRect(rectBottomText, 0)
  if (arg0) != 0:
    textTimeTotal = 0
    textTimeRemaining = 0

  when not(USE_TEXT):
    SetWindowTitle(window.WINDOW_TITLE)


# Dungeon wall drawing algorithm by HTamas

const
  RSET_WALL = 7

  RES_WALL_FACE_MAIN = 1   #(face stack main.bmp)
  RES_WALL_FACE_TOP = 2    #(face stack top.bmp)
  RES_WALL_CENTRE_BASE = 3 #(centre stack base.bmp)
  RES_WALL_CENTRE_MAIN = 4 #(centre stack main.bmp)
  RES_WALL_RIGHT_BASE = 5  #(right stack base.bmp)
  RES_WALL_RIGHT_MAIN = 6  #(right stack main.bmp)
  RES_WALL_SINGLE_BASE = 7 #(single stack base.bmp)
  RES_WALL_SINGLE_MAIN = 8 #(single stack main.bmp)
  RES_WALL_LEFT_BASE = 9   #(left stack base.bmp)
  RES_WALL_LEFT_MAIN = 10  #(left stack main.bmp)
  RES_WALL_DIVIDER1 = 11   #(divider01.bmp, the broad divider)
  RES_WALL_DIVIDER2 = 12   #(divider02.bmp, the narrow divider)
  RES_WALL_RNDBLOCK = 13   #(random block.bmp)
  RES_WALL_MARK_TL = 14    #(mark01.bmp, top left mark)
  RES_WALL_MARK_BL = 15    #(mark02.bmp, bottom left mark)
  RES_WALL_MARK_TR = 16    #(mark03.bmp, top right mark)
  RES_WALL_MARK_BR = 17    #(mark04.bmp, bottom right mark)

  BLIT_NO_TRANS = 0
  BLIT_OR = 2
  BLIT_XOR = 3
  BLIT_BLACK = 9
  BLIT_TRANS = 16

  GRAPHICS_CGA = 1
  GRAPHICS_HERCULES = 2
  GRAPHICS_EGA = 3
  GRAPHICS_TANDY = 4
  GRAPHICS_VGA = 5

  DESIGN_DUNGEON = 0
  DESIGN_PALACE = 1

  WALL_MODIFIER_SWS = 0
  WALL_MODIFIER_SWW = 1
  WALL_MODIFIER_WWS = 2
  WALL_MODIFIER_WWW = 3


# seg008:268F
proc wallPattern(whichPart, whichTable: int32) =
  var
    # local variables
    savedSim: AddTableType
    v2, v3: word
    v4, v5: byte
    bgModifier: byte
    savedPrngState: dword
    isDungeon: word
  # save the value for the sprite insertion method, so that it can be restored
  savedSim = ptrAddTable
  # set the sprite insertion method based on the arguments
  if (whichTable == 0):
    ptrAddTable = addBackTable
  else:
    ptrAddTable = addForeTable

  # save the state of the pseudorandom number generator
  savedPrngState = randomSeed
  # set the new seed
  randomSeed = drawnRoom + tblLine[drawnRow] + word(drawnCol)
  discard prandom(1) # fetch a random number and discard it
  isDungeon = byte(custom.tblLevelType[currentLevel] < DESIGN_PALACE) or
      custom.enableWdaInPalace
  if ((isDungeon == 0) and (graphicsMode == GRAPHICS_VGA)):
    # I haven't traced the palace WDA
    #[...]#
    if (whichPart) != 0:
      addWipetable(sbyte(whichTable), int16(8'u16*(drawXh)), int16(drawMainY -
          40), 20, 4*8, sbyte(palaceWallColors[44 * drawnRow + drawnCol]))
      addWipetable(sbyte(whichTable), int16(8'u16*(drawXh)), int16(drawMainY -
          19), 21, 2*8, sbyte(palaceWallColors[44 * drawnRow + 11 + drawnCol]))
      addWipetable(sbyte(whichTable), int16(8'u16*(drawXh + 2)), int16(
          drawMainY - 19), 21, 2*8, sbyte(palaceWallColors[44 * drawnRow + 12 + drawnCol]))
      addWipetable(sbyte(whichTable), int16(8'u16*(drawXh)), int16(drawMainY),
          19, 1*8, sbyte(palaceWallColors[44 * drawnRow + 22 + drawnCol]))
      addWipetable(sbyte(whichTable), int16(8'u16*(drawXh + 1)), int16(
          drawMainY), 19, 3*8, sbyte(palaceWallColors[44 * drawnRow + 23 + drawnCol]))
      discard ptrAddTable(ord(idChtab7Environmentwall), int32(prandom(2) + 3),
          sbyte(drawXh + 3), 0, int32(drawMainY - 53), ord(blitters46hMono6), 0)
      discard ptrAddTable(ord(idChtab7Environmentwall), int32(prandom(2) + 6),
          sbyte(drawXh), 0, int32(drawMainY - 34), ord(blitters46hMono6), 0)
      discard ptrAddTable(ord(idChtab7Environmentwall), int32(prandom(2) + 9),
          sbyte(drawXh), 0, int32(drawMainY - 13), ord(blitters46hMono6), 0)
      discard ptrAddTable(ord(idChtab7Environmentwall), int32(prandom(2) + 12),
          sbyte(drawXh), 0, int32(drawMainY), ord(blitters46hMono6), 0)

    addWipetable(sbyte(whichTable), int16(8'u16*drawXh), drawBottomY, 3, 4*8,
        sbyte(palaceWallColors[44 * drawnRow + 33 + drawnCol]))
    discard ptrAddTable(ord(idChtab7Environmentwall), int32(prandom(2) + 15),
        sbyte(drawXh), 0, int32(drawBottomY), ord(blitters46hMono6), 0)
  else:
    v3 = prandom(1)
    v5 = byte(prandom(4))
    v2 = prandom(1)
    v4 = byte(prandom(4))
    # store the background modifier for the current tile in a local variable
    # apparently, for walls, the modifier stores whether there are adjacent walls
    bgModifier = byte(currModifier and 0x7F)
    case (bgModifier)
    of WALL_MODIFIER_WWW:
      if (whichPart != 0):
        if (prandom(4) == 0):
          discard ptrAddTable(RSET_WALL, RES_WALL_RNDBLOCK, sbyte(drawXh), 0,
              int32(drawBottomY - 42), BLIT_NO_TRANS, 0)
        discard ptrAddTable(RSET_WALL, int32(word(RES_WALL_DIVIDER1) + v3),
            sbyte(drawXh + 1), sbyte(v5), int32(drawBottomY - 21), BLIT_TRANS, 0)
      discard ptrAddTable(RSET_WALL, int32(word(RES_WALL_DIVIDER1) + v2), sbyte(
          drawXh), sbyte(v4), int32(drawBottomY), BLIT_TRANS, 0)
      if (whichPart != 0):
        if (isDungeon) != 0:
          if (prandom(4) == 0):
            drawRightMark(prandom(3), v5)
          if (prandom(4) == 0):
            drawLeftMark(prandom(4), word(v5) - v3, word(v4) - v2)
    of WALL_MODIFIER_SWS:
      if (isDungeon) != 0:
        if (whichPart != 0):
          if (prandom(6) == 0):
            drawLeftMark(prandom(1), word(v5) - v3, word(v4) - v2)
    of WALL_MODIFIER_SWW:
      if (whichPart != 0):
        if (prandom(4) == 0):
          discard ptrAddTable(RSET_WALL, RES_WALL_RNDBLOCK, sbyte(drawXh), 0,
              int32(drawBottomY - 42), BLIT_NO_TRANS, 0)
        discard ptrAddTable(RSET_WALL, int32(word(RES_WALL_DIVIDER1) + v3),
            sbyte(drawXh + 1), sbyte(v5), int32(drawBottomY - 21), BLIT_TRANS, 0)
        if (isDungeon) != 0:
          if (prandom(4) == 0):
            drawRightMark(prandom(3), v5)
          if (prandom(4) == 0):
            drawLeftMark(prandom(3), word(v5) - v3, word(v4) - v2)
    of WALL_MODIFIER_WWS:
      if (whichPart != 0):
        discard ptrAddTable(RSET_WALL, int32(word(RES_WALL_DIVIDER1) + v3),
            sbyte(drawXh + 1), sbyte(v5), int32(drawBottomY - 21), BLIT_TRANS, 0)
      discard ptrAddTable(RSET_WALL, int32(word(RES_WALL_DIVIDER1) + v2), sbyte(
          drawXh), sbyte(v4), int32(drawBottomY), BLIT_TRANS, 0)
      if (whichPart != 0):
        if (isDungeon) != 0:
          if (prandom(4) == 0):
            drawRightMark(prandom(1) + 2, v5)
          if (prandom(4) == 0):
            drawLeftMark(prandom(4), word(v5) - v3, word(v4) - v2)
    else:
      discard

  randomSeed = savedPrngState
  ptrAddTable = savedSim

const
  RPOS: array[4, word] = [52'u16, 42'u16, 31'u16, 21'u16]
  LPOS: array[5, word] = [58'u16, 41'u16, 37'u16, 20'u16, 16'u16]

proc drawLeftMark (arg3, arg2, arg1: word) =
  var
    lv1: word
    lv2: word
  lv1 = RES_WALL_MARK_TL
  lv2 = 0
  if (arg3 mod 2) != 0:
    lv1 = RES_WALL_MARK_BL
  if (arg3 > 3'u16):
    lv2 = arg1 + 6
  elif (arg3 > 1'u16):
    lv2 = arg2 + 6

  discard ptrAddTable(RSET_WALL, int32(lv1), sbyte(drawXh + word(arg3 == 2 or
      arg3 == 3)), sbyte(lv2), int32(word(drawBottomY) - LPOS[arg3]),
          BLIT_TRANS, 0)


proc drawRightMark(arg2, arg1: word) =
  var
    rv: word
    arg1Intern: word
  rv = RES_WALL_MARK_TR
  if (arg2 mod 2) != 0:
    rv = RES_WALL_MARK_BR
  if (arg2 < 2'u16):
    arg1Intern = 24
  else:
    arg1Intern -= 3

  discard ptrAddTable(RSET_WALL, int32(rv), sbyte(drawXh + word(arg2 > 1'u16)),
      sbyte(arg1), drawBottomY - int32(RPOS[arg2]), BLIT_TRANS, 0)
