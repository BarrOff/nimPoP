# data:3D1A
var
  distanceMirror: sbyte
  rectReplacement: RectType = rectTop

# seg003:0000
proc initGame(level: int32) =
  if(offscreenSurface != nil):
    freeSurface(offscreenSurface) # missing in original
    offscreenSurface = nil

  offscreenSurface = makeOffscreenBuffer(rectReplacement)
  loadKidSprite()
  textTimeRemaining = 0
  textTimeTotal = 0
  isShowTime = 0
  checkpoint = 0
  upsideDown = 0; # N.B. upsideDown is also reset in setStartPos()
  resurrectTime = 0
  if dontResetTime == 0:
    remMin = int16(custom.startMinutesLeft) # 60
    remTick = custom.startTicksLeft # 719
    hitpBegLev = custom.startHitp # 3

  needLevel1Music = word(level == int32(custom.introMusicLevel))
  playLevel(level)


# seg003:005C
proc playLevel(levelNumber: int32) =
  var
    cutsceneFunc: CutscenePtrType
    lvlNumber: int32 = levelNumber
  when(USE_COPYPROT):
    if (enableCopyprot != 0) and lvlNumber == int32(custom.copyprotLevel):
      lvlNumber = 15

  while true:
    if ((demoMode != 0) and lvlNumber > 2):
      startLevel = -1
      needQuotes = 1
      startGame()

    if (lvlNumber != int32(currentLevel)):
      if (lvlNumber < 0 or lvlNumber > 15):
        echo ("Tried to load cutscene for level " & $(lvlNumber) & ", not in 0..15\n")
        quitPoP(1)

      cutsceneFunc = tblCutscenes[custom.tblCutscenesByIndex[lvlNumber]]
      if (when(USE_REPLAY and USE_SCREENSHOT): cutsceneFunc != nil and ((
          recording or replaying) == 0) and not(wantAutoScreenshot()) elif (
          USE_REPLAY): cutsceneFunc != nil and not(recording or
          replaying) elif (USE_SCREENSHOT): cutsceneFunc != nil and not(
          wantAutoScreenshot()) else: cutsceneFunc != nil):
        loadIntro(int(lvlNumber > 2), cutsceneFunc, 1)

    if (lvlNumber != int32(currentLevel)):
      loadLevSpr(lvlNumber)

    loadLevel()
    posGuards()
    clearCollRooms()
    clearSavedCtrl()
    drawnRoom = 0
    mobsCount = 0
    trobsCount = 0
    nextSound = -1
    holdingSword = 0
    grabTimer = 0
    canGuardSeeKid = 0
    unitedWithShadow = 0
    flashTime = 0
    leveldoorOpen = 0
    demoIndex = 0
    demoTime = 0
    guardhpCurr = 0
    hitpDelta = 0
    Guard.charid = ord(charid2Guard)
    Guard.direction = ord(dir56None)
    doStartpos()
    haveSword = word(lvlNumber == 0 or lvlNumber >=
        int32(custom.haveSwordFromLevel))
    findStartLevelDoor()
    # busy waiting?
    while ((checkSoundPlaying() != 0) and (doPaused() == 0)):
      idle()
    stopSounds()
    when (USE_REPLAY):
      if replaying != 0:
        replayRestoreLevel()
      if skippingReplay != 0:
        if replaySeekTarget == ord(replaySeek0NextRoom) or replaySeekTarget ==
            ord(replaySeek1NextLevel):
          skippingReplay = 0 # resume replay from here

    drawLevelFirst()
    showCopyprot(0)
    lvlNumber = playLevel2()
    # hacked...
    when(USE_COPYPROT):
      if ((enableCopyprot != 0) and lvlNumber == int32(custom.copyprotLevel) and
          (demoMode == 0)):
        lvlNumber = 15
      else:
        if (lvlNumber == 16):
          lvlNumber = int32(custom.copyprotLevel)
          # Note: C code uses -1, which is equal to 65355 for uint16_t
          custom.copyprotLevel = high(typeof(custom.copyprotLevel))
    freePeels()


# seg003:01A3
proc doStartpos() =
  var
    x: word
  # Special event: start at checkpoint
  if (currentLevel == custom.checkpointLevel and (checkpoint != 0)):
    level.startDir = custom.checkpointRespawnDir
    level.startRoom = custom.checkpointRespawnRoom
    level.startPos = custom.checkpointRespawnTilepos
    # Special event: remove loose floor
    discard getTile(int(custom.checkpointClearTileRoom),
        int32(custom.checkpointClearTileCol),
        int32(custom.checkpointClearTileRow))
    currRoomTiles[currTilepos] = ord(tiles0Empty)

  nextRoom = level.startRoom
  Char.room = level.startRoom
  x = level.startPos
  Char.currCol = sbyte(x mod 10)
  Char.currRow = sbyte(x div 10)
  Char.x = byte(xBump[Char.currCol + 5] + 14)
  # Start in the opposite direction (and turn into the correct one).
  Char.direction = not(level.startDir)
  if (seamless == 0):
    if (currentLevel != 0):
      x = hitpBegLev
    else:
      # HP on demo level
      x = custom.demoHitp

    hitpMax = x
    hitpCurr = x

  if (custom.tblEntryPose[currentLevel] == 1):
    # Special event: press button + falling entry
    discard getTile(5, 2, 0)
    triggerButton(0, 0, -1)
    seqtblOffsetChar(ord(seq7Fall)) # fall
  elif (custom.tblEntryPose[currentLevel] == 2):
    # Special event: running entry
    seqtblOffsetChar(ord(seq84Run)) # run
  else:
    seqtblOffsetChar(ord(seq5Turn)) # turn

  setStartPos()


# seg003:028A
proc setStartPos() =
  Char.y = byte(yLand[Char.currRow + 1])
  Char.alive = -1
  Char.charid = ord(charid0Kid)
  isScreaming = 0
  knock = 0
  upsideDown = custom.startUpsideDown # 0
  isFeatherFall = 0
  Char.fallY = 0
  Char.fallX = 0
  offguard = 0
  Char.sword = ord(sword0Sheathed)
  droppedout = 0
  playSeq()
  if (currentLevel == custom.fallingEntryLevel and Char.room ==
      custom.fallingEntryRoom):
    # Special event: level 7 falling entry
    # level 7, room 17: show room below
    discard gotoOtherRoom(3)

  savekid()


# seg003:02E6
proc findStartLevelDoor() =
  getRoomAddress(int(Kid.room))
  for tilepos in 0..<30:
    if ((currRoomTiles[tilepos] and 0x1F) == ord(tiles16LevelDoorLeft)):
      startLevelDoor(int16(Kid.room), int16(tilepos))


# seg003:0326
proc drawLevelFirst() =
  nextRoom = Kid.room
  checkTheEnd()
  if custom.tblLevelType[currentLevel] != 0:
    genPalaceWallColors()

  drawRect(screenRect, 0)
  showLevel()
  redrawScreen(0)
  drawKidHp(int16(hitpCurr), int16(hitpMax))

  when(USE_QUICKSAVE):
    checkQuickOp()

  when(USE_SCREENSHOT):
    autoScreenshot()

  # Busy waiting!
  startTimer(ord(timer1), 5)
  doSimpleWait(1)


# seg003:037B
proc redrawScreen(drawingDifferentRoom: int32) =
  #removeFlash()
  if drawingDifferentRoom != 0:
    drawRect(rectTop, 0)


  differentRoom = 0
  if isBlindMode != 0:
    drawRect(rectTop, 0)
  else:
    if currGuardColor != 0:
      # Moved *before* drawings.
      setChtabPalette(chtabAddrs[ord(idChtab5Guard)], addr(cast[
          ptr UncheckedArray[byte]](guardPalettes)[0x30'u16 * currGuardColor -
          0x30]), 0x10)

    needDrects = 0
    redrawRoom()
    when(USE_LIGHTING):
      redrawLighting()
    if isKeyboardMode != 0:
      clearKbdBuf()

    isBlindMode = 1
    drawTables()
    if isKeyboardMode != 0:
      clearKbdBuf()

    when(USE_COPYPROT):
      if (currentLevel == 15):
        # letters on potions level
        currentTargetSurface = offscreenSurface
        for var2 in 0 ..< 14:
          if (copyprotRoom[var2] == drawnRoom):
            setCurrPos(int( (copyprotTile[var2] mod 10 shl 5) + 24), int32(
                copyprotTile[var2] div 10 * 63 + 38))
            discard drawTextCharacter(byte(copyprotLetter[cplevelEntr[var2]]))

      currentTargetSurface = onscreenSurface

    isBlindMode = 0
    for i in 0..high(tableCounts):
      tableCounts[i] = 0
    drawMoving()
    drawTables()
    if isKeyboardMode != 0:
      clearKbdBuf()

    needDrects = 1
    if currGuardColor != 0:
      #setPalArr(0x80, 0x10, &guardPalettes[0x30 * currGuardColor - 0x30], 1)
      discard

    if upsideDown != 0:
      flipScreen(offscreenSurface)

    var
      rectReplacement = rectTop
    copyScreenRect(rectReplacement)
    if upsideDown != 0:
      flipScreen(offscreenSurface)

    if isKeyboardMode != 0:
      clearKbdBuf()


  exitRoomTimer = 2


when(CHECK_TIMING):
  type
    testTimingStateType = object
      alreadyHadFirstFrame: bool
      levelStartCounter: uint64
      ticksLeftAtLevelStart: int32
      secondsLeftAtLevelStart: float

  proc testTimings(state: var testTimingStateType) =

    if not(state.alreadyHadFirstFrame):
      state.levelStartCounter = getPerformanceCounter()
      state.ticksLeftAtLevelStart = int32(remMin-1)*720 + int32(remTick)
      state.secondsLeftAtLevelStart = float(1.0 / 60.0) * float(5 *
          state.ticksLeftAtLevelStart)
      echo("Seconds left = " & $(state.secondsLeftAtLevelStart))
      state.alreadyHadFirstFrame = true
    elif (remTick mod 12 == 11):
      var
        currentCounter: uint64 = getPerformanceCounter()
        actualSecondsElapsed: float = float(currentCounter -
            state.levelStartCounter) / float(getPerformanceFrequency())

        ticksLeft: int32 = int32(remMin-1)*720 + int32(remTick)
        gameSecondsLeft: float = float(1.0 / 60.0) * float(5 * ticksLeft)
        gameSecondsElapsed: float = state.secondsLeftAtLevelStart - gameSecondsLeft

      echo("remMin: " & $(remMin) & "   game elapsed (s): " & $(
          gameSecondsElapsed) & "    actual elapsed (s): " & $(
          actualSecondsElapsed) & "     delta: " & $(actualSecondsElapsed -
          gameSecondsElapsed))


# seg003:04F8
# Returns a level number:
# - The current level if it was restarted.
# - The next level if the level was completed.
proc playLevel2(): int32 =
  resetTimer(ord(timer1))
  when(CHECK_TIMING):
    var
      testTimingState: testTimingStateType
  while true: # main loop
    when(USE_QUICKSAVE):
      checkQuickOp()
    when(CHECK_TIMING):
      testTimings(testTimingState)

    when(USE_REPLAY):
      if needReplayCycle != 0:
        replayCycle()

    if (Kid.sword == ord(sword2Drawn)):
      # speed when fighting (smaller is faster)
      setTimerLength(ord(timer1), int32(custom.fightSpeed)) # 6
    else:
      # speed when not fighting (smaller is faster)
      setTimerLength(ord(timer1), int32(custom.baseSpeed)) # 5

    guardhpDelta = 0
    hitpDelta = 0
    timers()
    playFrame()

    when(USE_REPLAY):
      # At the exact "end of level" frame, preserve the seed to ensure reproducibility,
      # regardless of how long the sound is still playing *after* this frame (torch animation modifies the seed!)
      if (keepLastSeed == 1):
        preservedSeed = randomSeed
        keepLastSeed = -1 # disable repeat

    if isRestartLevel != 0:
      isRestartLevel = 0
      return int32(currentLevel)
    else:
      if (nextLevel == currentLevel or (checkSoundPlaying() != 0)):
        drawGameFrame()
        discard flashIfHurt()
        removeFlashIfHurt()
        doSimpleWait(ord(timer1))
      else:
        stopSounds()
        hitpBegLev = hitpMax
        checkpoint = 0

        when (USE_REPLAY):
          if (keepLastSeed == -1):
            randomSeed = preservedSeed # Ensure reproducibility in the next level.
            keepLastSeed = 0

        return int32(nextLevel)


# seg003:0576
proc redrawAtChar() =
  var
    xTopRow: int16
    xColLeft: int16
    xColRight: int16
  if (Char.sword >= ord(sword2Drawn)):
    # If char is holding sword, it makes redraw-area bigger.
    if (Char.direction >= ord(dir0Right)):
      inc(charColRight)
      if (charColRight > 9):
        charColRight = 9
      # charColRight = MIN(charColRight + 1, 9)
    else:
      dec(charColRight)
      if (charColLeft < 0):
        charColLeft = 0
      # charColLeft = MAX(charColLeft - 1, 0)

  if (Char.charid == ord(charid0Kid)):
    xTopRow = min(charTopRow, prevCharTopRow)
    xColRight = max(charColRight, prevCharColRight)
    xColLeft = min(charColLeft, prevCharColLeft)
  else:
    xTopRow = charTopRow
    xColRight = charColRight
    xColLeft = charColLeft

  for tileRow in xTopRow..charBottomRow:
    for tileCol in xColLeft..xColRight:
      setRedrawFore(int16(getTilepos(tileCol, tileRow)), 1)


  if (Char.charid == ord(charid0Kid)):
    prevCharTopRow = charTopRow
    prevCharColRight = charColRight
    prevCharColLeft = charColLeft



# seg003:0645
proc redrawAtChar2() =
  var
    charAction: int16
    charFrame: int16
    redrawFunc: proc(a: int16, b: byte)
  charAction = int16(Char.action)
  charFrame = int16(Char.frame)
  redrawFunc = setRedraw2
  # frames 78..80: grab
  if (charFrame < ord(frame78Jumphang) or charFrame >= ord(frame80Jumphang)):
    # frames 135..149: climb up
    if (charFrame >= ord(frame137Climbing3) and charFrame < ord(
        frame145Climbing11)):
      redrawFunc = setRedrawFloorOverlay
    else:
      # frames 102..106: fall
      if (charAction != ord(actions2HangClimb) and charAction != ord(
          actions3InMidair) and charAction != ord(actions4InFreefall) and
              charAction != ord(actions6HangStraight) and
          (charAction != ord(actions5Bumped) or charFrame < ord(
              frame102StartFall1) or charFrame > ord(frame106Fall))):
        return

  for tileCol in countdown(charColRight, charColLeft, 1):
    if (charAction != 2):
      redrawFunc(int16(getTilepos(tileCol, charBottomRow)), 1)

    if (charTopRow != charBottomRow):
      redrawFunc(int16(getTilepos(tileCol, charTopRow)), 1)


# seg003:0706
proc checkKnock() =
  if knock != 0:
    doKnock(int32(Char.room), Char.currRow - byte(knock > 0))
    knock = 0


# seg003:0735
proc timers() =
  if (unitedWithShadow > 0):
    dec(unitedWithShadow)
    if (unitedWithShadow == 0):
      dec(unitedWithShadow)


  if (guardNoticeTimer > 0):
    dec(guardNoticeTimer)

  if (resurrectTime > 0'u16):
    dec(resurrectTime)

  if isFeatherFall != 0:
    inc(isFeatherFall)

  if (fixes.fixQuicksaveDuringFeather != 0'u8):
    if (isFeatherFall > 0'u16):
      dec(isFeatherFall)
      if (isFeatherFall == 0'u16):
        if (checkSoundPlaying() != 0):
          stopSounds()

        echo "slow fall ended at: remMin = " & $(remMin) & ", remTick = remTick"
        echo "length = " & $(isFeatherFall) & " ticks"
        when (USE_REPLAY):
          if (recording != 0'u8):
            specialMove = MOVE_EFFECT_END
  else:
    if (isFeatherFall != 0'u16):
      inc(isFeatherFall)

    if ((isFeatherFall != 0'u16) and ((checkSoundPlaying() == 0) or
        isFeatherFall > 225'u16)):
      echo "slow fall ended at: remMin = " & $(remMin) & ", remTick = remTick"
      echo "length = " & $(isFeatherFall) & " ticks"
      when (USE_REPLAY):
        if (recording != 0'u8):
          specialMove = MOVE_EFFECT_END
        if (replaying == 0'u8): # during replays, feather effect gets cancelled in doReplayMove()
          isFeatherFall = 0'u16
      else:
        isFeatherFall = 0'u16

  if (int32(isFeatherFall) and (not(checkSoundPlaying()) or int32(
      isFeatherFall > 225'u16))) != 0:
    echo "slow fall ended at: rem_min = ", remMin, ", rem_tick = ", remTick
    echo "length = ", isFeatherFall, " ticks"
    when(USE_REPLAY):
      if recording != 0:
        specialMove = ord(MOVE_EFFECT_END)
      if (replaying == 0): # during replays, feather effect gets cancelled in doReplayMove()
        isFeatherFall = 0
    else:
      isFeatherFall = 0

  # Special event: mouse
  if (currentLevel == custom.mouseLevel and Char.room == custom.mouseRoom and
      (leveldoorOpen != 0)):
    inc(leveldoorOpen)
    # time before mouse comes: 150/12=12.5 seconds
    if (leveldoorOpen == custom.mouseDelay):
      doMouse()


# seg003:0798
proc checkMirror() =
  var
    clipTop: word
  if (jumpedThroughMirror == -1):
    jumpThroughMirror()
  else:
    if (getTileAtChar() == ord(tiles13Mirror)):
      loadkid()
      loadFrame()
      checkMirrorImage()
      if (distanceMirror >= 0 and (custom.showMirrorImage != 0) and word(
          Char.room) == drawnRoom):
        loadFrameToObj()
        resetObjClip()
        clipTop = uint16(yClip[Char.currRow + 1])
        if (clipTop < objY):
          objClipTop = int16(clipTop)
          objClipLeft = (Char.currCol shl 5) + 9
          addObjtable(4) # mirror image


# seg003:080A
proc jumpThroughMirror() =
  loadkid()
  loadFrame()
  checkMirrorImage()
  jumpedThroughMirror = 0
  Char.charid = ord(charid1Shadow)
  playSound(ord(sound45JumpThroughMirror)) # jump through mirror
  saveshad()
  guardhpMax = hitpMax
  guardhpCurr = hitpMax
  hitpCurr = 1
  drawKidHp(1, int16(hitpMax))
  drawGuardHp(int16(guardhpCurr), int16(guardhpMax))


# seg003:085B
proc checkMirrorImage() =
  var
    distance: int16
    xpos: int16
  xpos = int16(xBump[Char.currCol + 5]) + 10
  distance = int16(distanceToEdgeWeight())
  if (Char.direction >= ord(dir0Right)):
    distance = not(distance) + 14

  distanceMirror = sbyte(distance) - 2
  Char.x = byte(xpos shl 1) - Char.x
  Char.direction = not(Char.direction)


# seg003:08AA
proc bumpIntoOpponent() =
  # This is called from playKidFrame, so char=Kid, Opp=Guard
  var
    distance: int16
  if (canGuardSeeKid >= 2 and
      Char.sword == ord(sword0Sheathed) and # Kid must not be in fighting pose
    Opp.sword != ord(sword0Sheathed) and # but Guard must
    Opp.action < 2 and
    Char.direction != Opp.direction # must be facing toward each other
    ):
    distance = int16(charOppDist())
    if (abs(distance) <= 15):

      when (FIX_PAINLESS_FALL_ON_GUARD):
        if fixes.fixPainlessFallOnGuard != 0:
          if (Char.fallY >= 33):
            return # don't bump; dead
          elif (Char.fallY >= 22): # medium land
            discard takeHp(1)
            playSound(ord(sound16MediumLand))

      Char.y = byte(yLand[Char.currRow + 1])
      Char.fallY = 0
      seqtblOffsetChar(ord(seq47Bump)); # bump into opponent
      playSeq()


# seg003:0913
proc posGuards() =
  var
    guardTile: int16
  for room1 in 0..<24:
    guardTile = int16(level.guardsTile[room1])
    if (guardTile < 30):
      level.guardsX[room1] = byte(xBump[guardTile mod 10 + 5] + 14)
      level.guardsSeqHi[room1] = 0


# seg003:0959
proc checkCanGuardSeeKid() =
#[
Possible results in canGuardSeeKid:
0: Guard can't see Kid
1: Guard can see Kid, but won't come
2: Guard can see Kid, and will come
]#
  var
    kidFrame: int16
    leftPos: int16
    temp: int16
    rightPos: int16
  kidFrame = int16(Kid.frame)
  if (Guard.charid == ord(charid24Mouse)):
    return

  if ((Guard.charid != ord(charid1Shadow) or currentLevel == 12) and
      # frames 217..228: going up on stairs
    kidFrame != 0 and (kidFrame < ord(frame219ExitStairs3) or kidFrame >=
        229) and Guard.direction != ord(dir56None) and Kid.alive < 0 and
        Guard.alive < 0 and Kid.room == Guard.room and Kid.currRow ==
        Guard.currRow):
    canGuardSeeKid = 2
    leftPos = int16(xBump[Kid.currCol + 5]) + 7
    when(FIX_DOORTOP_DISABLING_GUARD):
      if fixes.fixDoortopDisablingGuard != 0'u8:
        # When the kid is hanging on the right side of a doortop, Kid.curr_col points at the doortop tile and a guard on the left side will see the prince.
        # This fixes that.
        if (Kid.action == ord(actions2HangClimb) or Kid.action == ord(
            actions6HangStraight)):
          leftPos += 14
    # echo "Kid.curr_col = ", Kid.currCol, ", Kid.action = ", Kid.action
    rightPos = int16(xBump[Guard.currCol + 5]) + 7
    if (leftPos > rightPos):
      temp = leftPos
      leftPos = rightPos
      rightPos = temp

    # A chomper is on the left side of a tile, so it doesn't count.
    if (getTileAtKid(leftPos) == ord(tiles18Chomper)):
      leftPos += 14

    # A gate is on the right side of a tile, so it doesn't count.
    if (when(FIX_DOORTOP_DISABLING_GUARD): (fixes.fixDoortopDisablingGuard ==
        0) and getTileAtKid(rightPos) == ord(tiles4Gate) or getTileAtKid(
        rightPos) == ord(tiles7DoortopWithFloor) or getTileAtKid(rightPos) ==
        ord(tiles12Doortop) else: getTileAtKid(rightPos) == ord(tiles4Gate)):
      rightPos -= 14

    if (rightPos >= leftPos):
      while (leftPos <= rightPos):
        # Can't see through these tiles.
        if (getTileAtKid(leftPos) == ord(tiles20Wall) or
            currTile2 == ord(tiles7DoortopWithFloor) or
            currTile2 == ord(tiles12Doortop)):
          canGuardSeeKid = 0
          return

        # Can see through these, but won't go through them.
        if (currTile2 == ord(tiles11Loose) or
            currTile2 == ord(tiles18Chomper) or
            (currTile2 == ord(tiles4Gate) and currRoomModif[currTilepos] <
            112) or (tileIsFloor(int32(currTile2)) == 0)):
          canGuardSeeKid = 1

        leftPos += 14
  else:
    canGuardSeeKid = 0


# seg003:0A99
proc getTileAtKid(xpos: int32): byte =
  return byte(getTile(int(Kid.room), getTileDivModM7(xpos), int32(Kid.currRow)))


# seg003:0ABA
proc doMouse() =
  loadkid()
  Char.charid = custom.mouseObject
  Char.x = custom.mouseStartX
  Char.currRow = 0
  Char.y = byte(yLand[Char.currRow + 1])
  Char.alive = -1
  Char.direction = ord(dirFFLeft)
  guardhpCurr = 1
  seqtblOffsetChar(ord(seq105MouseForward)) # mouse forward
  playSeq()
  saveshad()


# seg003:0AFC
proc flashIfHurt(): int32 =
  if (flashTime != 0):
    doFlash(int16(flashColor))
    return 1
  elif (hitpDelta < 0):
    if (isJoystMode and enableControllerRumble) != 0:
      when(sdl.PATCHLEVEL >= 9):
        if (sdlHaptic != nil):
          discard hapticRumblePlay(sdlHaptic, 1.0,
              100) # rumble at full strength for 100 milliseconds
        elif (sdlController != nil):
          discard gameControllerRumble(sdlController, 0xFFFF, 0xFFFF, 100)
        else:
          discard joystickRumble(sdlJoystick, 0xFFFF, 0xFFFF, 100)
      else:
        if (sdlHaptic != nil):
          discard hapticRumblePlay(sdlHaptic, 1.0,
              100) # rumble at full strength for 100 milliseconds

    doFlash(ord(color12Brightred)) # red
    return 1

  return 0 # not flashed


# seg003:0B1A
proc removeFlashIfHurt() =
  if (flashTime != 0):
    dec(flashTime)
  else:
    if (hitpDelta >= 0):
      return

  removeFlash()
