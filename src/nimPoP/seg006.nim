# const
#   SEQTBL_BASE = 0x196E

# template SEQTBL0() =
#   seqtbl - SEQTBL_BASE

# var
#   seqtbl: seq[byte]

# seg006:0006
proc getTile(room, col, row: int32): int32 =
  currRoom = int16(room)
  tileCol = int16(col)
  tileRow = int16(row)
  currRoom = int16(findRoomOfTile())
  # bugfix: checkChompedKid may call with room = -1
  if (currRoom > 0):
    getRoomAddress(currRoom)
    currTilepos = byte(int16(tblLine[tileRow]) + tileCol)
    currTile2 = byte(currRoomTiles[currTilepos] and 0x1F)
  else:
    # wall in room 0
    currTile2 = custom.levelEdgeHitTile # ord(tiles20Wall)

  return int32(currTile2)


# seg006:005D
proc findRoomOfTile(): int32 =
  # Check tileRow < 0 first, this way the prince can grab a ledge at the bottom right corner of a room with no room below.
  # Details: https:#forum.princed.org/viewtopic.php?p=30410#p30410
  while true:
    when (FIX_CORNER_GRAB):
      if (tileRow < 0):
        tileRow += 3
        if currRoom != 0:
          currRoom = int16(level.roomlinks[currRoom - 1].up)
        continue
    if (tileCol < 0):
      tileCol += 10
      if currRoom != 0:
        currRoom = int16(level.roomlinks[currRoom - 1].left)
      continue
    if (tileCol >= 10):
      tileCol -= 10
      if currRoom != 0:
        currRoom = int16(level.roomlinks[currRoom - 1].right)
      continue
    when not(FIX_CORNER_GRAB):
    # if (tileRow < 0) was here originally
      if (tile_row < 0):
        tile_row += 3
        if (currRoom != 0):
          currRoom = int16(level.roomlinks[currRoom - 1].up)
        # findRoomOfTile
        continue
    if (tileRow >= 3):
      tileRow -= 3
      if currRoom != 0:
        currRoom = int16(level.roomlinks[currRoom - 1].down)
      continue
    return int32(currRoom)


# seg006:00EC
proc getTilepos(tileCol, tileRow: int32): int32 =
  if (tileRow < 0):
    return -(tileCol + 1)
  elif (tileRow >= 3 or tileCol >= 10 or tileCol < 0):
    return 30
  else:
    return int32(int16(tblLine[tileRow]) + tileCol)


# seg006:0124
proc getTileposNominus(tileCol, tileRow: int32): int32 =
  var
    var2: int16
  var2 = int16(getTilepos(tileCol, tileRow))
  if (var2 < 0):
    return 30
  else:
    return var2


# seg006:0144
proc loadFramDetCol() =
  loadFrame()
  determineCol()


# seg006:014D
proc determineCol() =
  Char.currCol = sbyte(getTileDivModM7(dxWeight()))


# data:0FE0
const
  frameTableKid: array[241, FrameType] = [
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 0'u8, sword: byte(0x00 or 0), dx: 1'i8, dy: 0'i8,
          flags: byte(0xC0 or 4)),
      FrameType(image: 1'u8, sword: byte(0x00 or 0), dx: 1'i8, dy: 0'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 2'u8, sword: byte(0x00 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0x40 or 7)),
      FrameType(image: 3'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: 0'i8,
          flags: byte(0x40 or 8)),
      FrameType(image: 4'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xE0 or 6)),
      FrameType(image: 5'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 9)),
      FrameType(image: 6'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 10)),
      FrameType(image: 7'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 5)),
      FrameType(image: 8'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 9'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 7)),
      FrameType(image: 10'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 11)),
      FrameType(image: 11'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 12'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 13'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 7)),
      FrameType(image: 14'u8, sword: byte(0x00 or 9), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 15'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 16'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 17'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 6)),
      FrameType(image: 18'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 8)),
      FrameType(image: 19'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 9)),
      FrameType(image: 20'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 11)),
      FrameType(image: 21'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 11)),
      FrameType(image: 22'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 17)),
      FrameType(image: 23'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 7)),
      FrameType(image: 24'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 5)),
      FrameType(image: 25'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 1)),
      FrameType(image: 26'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 6)),
      FrameType(image: 27'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 28'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 8)),
      FrameType(image: 29'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 2)),
      FrameType(image: 30'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 2)),
      FrameType(image: 31'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 2)),
      FrameType(image: 32'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 2)),
      FrameType(image: 33'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 34'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 8)),
      FrameType(image: 35'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 14)),
      FrameType(image: 36'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 1)),
      FrameType(image: 37'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 5)),
      FrameType(image: 38'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 14)),
      FrameType(image: 39'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 11)),
      FrameType(image: 40'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 11)),
      FrameType(image: 41'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 10)),
      FrameType(image: 42'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 1)),
      FrameType(image: 43'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 4)),
      FrameType(image: 44'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 45'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 46'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xA0 or 5)),
      FrameType(image: 47'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xA0 or 4)),
      FrameType(image: 48'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x60 or 6)),
      FrameType(image: 49'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: 0'i8,
          flags: byte(0x60 or 7)),
      FrameType(image: 50'u8, sword: byte(0x00 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0x60 or 6)),
      FrameType(image: 51'u8, sword: byte(0x00 or 0), dx: 1'i8, dy: 0'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 64'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 2)),
      FrameType(image: 65'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 1)),
      FrameType(image: 66'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 2)),
      FrameType(image: 67'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 68'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 69'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 70'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 71'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 72'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 73'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 74'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 75'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 76'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 80'u8, sword: byte(0x00 or 0), dx: -2'i8, dy: 0'i8,
          flags: byte(0x40 or 1)),
      FrameType(image: 81'u8, sword: byte(0x00 or 0), dx: -2'i8, dy: 0'i8,
          flags: byte(0x40 or 1)),
      FrameType(image: 82'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: 0'i8,
          flags: byte(0xC0 or 2)),
      FrameType(image: 83'u8, sword: byte(0x00 or 0), dx: -2'i8, dy: 0'i8,
          flags: byte(0x40 or 2)),
      FrameType(image: 84'u8, sword: byte(0x00 or 0), dx: -2'i8, dy: 0'i8,
          flags: byte(0x40 or 1)),
      FrameType(image: 85'u8, sword: byte(0x00 or 0), dx: -2'i8, dy: 0'i8,
          flags: byte(0x40 or 1)),
      FrameType(image: 86'u8, sword: byte(0x00 or 0), dx: -2'i8, dy: 0'i8,
          flags: byte(0x40 or 1)),
      FrameType(image: 87'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: 0'i8,
          flags: byte(0x00 or 7)),
      FrameType(image: 88'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: 0'i8,
          flags: byte(0x00 or 5)),
      FrameType(image: 89'u8, sword: byte(0x00 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x00 or 7)),
      FrameType(image: 90'u8, sword: byte(0x00 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x00 or 7)),
      FrameType(image: 91'u8, sword: byte(0x00 or 0), dx: 2'i8, dy: -3'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 92'u8, sword: byte(0x00 or 0), dx: 2'i8, dy: -10'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 93'u8, sword: byte(0x00 or 0), dx: 2'i8, dy: -11'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 94'u8, sword: byte(0x00 or 0), dx: 3'i8, dy: -2'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 95'u8, sword: byte(0x00 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 96'u8, sword: byte(0x00 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 97'u8, sword: byte(0x00 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0x60 or 3)),
      FrameType(image: 98'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: 0'i8,
          flags: byte(0xE0 or 3)),
      FrameType(image: 28'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 99'u8, sword: byte(0x00 or 0), dx: 7'i8, dy: -14'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 100'u8, sword: byte(0x00 or 0), dx: 7'i8, dy: -12'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 101'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: -12'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 102'u8, sword: byte(0x00 or 0), dx: 3'i8, dy: -10'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 103'u8, sword: byte(0x00 or 0), dx: 2'i8, dy: -10'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 104'u8, sword: byte(0x00 or 0), dx: 1'i8, dy: -10'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 105'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: -11'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 106'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: -12'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 107'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: -14'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 108'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: -14'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 109'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: -15'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 110'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: -15'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 111'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: -15'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 112'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 6)),
      FrameType(image: 113'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 6)),
      FrameType(image: 114'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 5)),
      FrameType(image: 115'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 5)),
      FrameType(image: 116'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 2)),
      FrameType(image: 117'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 4)),
      FrameType(image: 118'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 5)),
      FrameType(image: 119'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 6)),
      FrameType(image: 120'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 7)),
      FrameType(image: 121'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 7)),
      FrameType(image: 122'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 9)),
      FrameType(image: 123'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 8)),
      FrameType(image: 124'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 9)),
      FrameType(image: 125'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 9)),
      FrameType(image: 126'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 5)),
      FrameType(image: 127'u8, sword: byte(0x00 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x40 or 5)),
      FrameType(image: 128'u8, sword: byte(0x00 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0xC0 or 5)),
      FrameType(image: 129'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 133'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 134'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 4)),
      FrameType(image: 135'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 5)),
      FrameType(image: 136'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 8)),
      FrameType(image: 137'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x60 or 12)),
      FrameType(image: 138'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xE0 or 15)),
      FrameType(image: 139'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x60 or 3)),
      FrameType(image: 140'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 141'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 142'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 143'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 144'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 172'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0xC0 or 1)),
      FrameType(image: 173'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0xC0 or 7)),
      FrameType(image: 145'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: -12'i8,
          flags: byte(0x00 or 1)),
      FrameType(image: 146'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: -21'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 147'u8, sword: byte(0x00 or 0), dx: 1'i8, dy: -26'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 148'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: -32'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 149'u8, sword: byte(0x00 or 0), dx: 6'i8, dy: -36'i8,
          flags: byte(0x80 or 1)),
      FrameType(image: 150'u8, sword: byte(0x00 or 0), dx: 7'i8, dy: -41'i8,
          flags: byte(0x80 or 2)),
      FrameType(image: 151'u8, sword: byte(0x00 or 0), dx: 2'i8, dy: 17'i8,
          flags: byte(0x40 or 2)),
      FrameType(image: 152'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: 9'i8,
          flags: byte(0xC0 or 4)),
      FrameType(image: 153'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: 5'i8,
          flags: byte(0xC0 or 9)),
      FrameType(image: 154'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: 4'i8,
          flags: byte(0xC0 or 8)),
      FrameType(image: 155'u8, sword: byte(0x00 or 0), dx: 5'i8, dy: 0'i8,
          flags: byte(0x60 or 9)),
      FrameType(image: 156'u8, sword: byte(0x00 or 0), dx: 5'i8, dy: 0'i8,
          flags: byte(0xE0 or 9)),
      FrameType(image: 157'u8, sword: byte(0x00 or 0), dx: 5'i8, dy: 0'i8,
          flags: byte(0xE0 or 8)),
      FrameType(image: 158'u8, sword: byte(0x00 or 0), dx: 5'i8, dy: 0'i8,
          flags: byte(0x60 or 9)),
      FrameType(image: 159'u8, sword: byte(0x00 or 0), dx: 5'i8, dy: 0'i8,
          flags: byte(0x60 or 9)),
      FrameType(image: 184'u8, sword: byte(0x00 or 16), dx: 0'i8, dy: 2'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 174'u8, sword: byte(0x00 or 26), dx: 0'i8, dy: 2'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 175'u8, sword: byte(0x00 or 18), dx: 3'i8, dy: 2'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 176'u8, sword: byte(0x00 or 22), dx: 7'i8, dy: 2'i8,
          flags: byte(0xC0 or 4)),
      FrameType(image: 177'u8, sword: byte(0x00 or 21), dx: 10'i8, dy: 2'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 178'u8, sword: byte(0x00 or 23), dx: 7'i8, dy: 2'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 179'u8, sword: byte(0x00 or 25), dx: 4'i8, dy: 2'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 180'u8, sword: byte(0x00 or 24), dx: 0'i8, dy: 2'i8,
          flags: byte(0xC0 or 14)),
      FrameType(image: 181'u8, sword: byte(0x00 or 15), dx: 0'i8, dy: 2'i8,
          flags: byte(0xC0 or 13)),
      FrameType(image: 182'u8, sword: byte(0x00 or 20), dx: 3'i8, dy: 2'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 183'u8, sword: byte(0x00 or 31), dx: 3'i8, dy: 2'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 184'u8, sword: byte(0x00 or 16), dx: 0'i8, dy: 2'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 185'u8, sword: byte(0x00 or 17), dx: 0'i8, dy: 2'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 186'u8, sword: byte(0x00 or 32), dx: 0'i8, dy: 2'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 187'u8, sword: byte(0x00 or 33), dx: 0'i8, dy: 2'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 188'u8, sword: byte(0x00 or 34), dx: 2'i8, dy: 2'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 14'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 189'u8, sword: byte(0x00 or 19), dx: 7'i8, dy: 2'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 190'u8, sword: byte(0x00 or 14), dx: 1'i8, dy: 2'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 191'u8, sword: byte(0x00 or 27), dx: 0'i8, dy: 2'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 181'u8, sword: byte(0x00 or 15), dx: 0'i8, dy: 2'i8,
          flags: byte(0xC0 or 13)),
      FrameType(image: 181'u8, sword: byte(0x00 or 15), dx: 0'i8, dy: 2'i8,
          flags: byte(0xC0 or 13)),
      FrameType(image: 112'u8, sword: byte(0x00 or 43), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 6)),
      FrameType(image: 113'u8, sword: byte(0x00 or 44), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 6)),
      FrameType(image: 114'u8, sword: byte(0x00 or 45), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 5)),
      FrameType(image: 115'u8, sword: byte(0x00 or 46), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 5)),
      FrameType(image: 114'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 5)),
      FrameType(image: 78'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 3'i8,
          flags: byte(0x80 or 10)),
      FrameType(image: 77'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: 3'i8,
          flags: byte(0x80 or 7)),
      FrameType(image: 211'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 212'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 213'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 214'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0x40 or 7)),
      FrameType(image: 215'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 7'i8,
          flags: byte(0x40 or 11)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 79'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: 7'i8,
          flags: byte(0x40 or 9)),
      FrameType(image: 130'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 131'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 132'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 2'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 192'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 193'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 194'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 195'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 196'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 197'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 198'u8, sword: byte(0x00 or 0), dx: -1'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 199'u8, sword: byte(0x00 or 0), dx: -4'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 200'u8, sword: byte(0x00 or 0), dx: -4'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 201'u8, sword: byte(0x00 or 0), dx: -4'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 202'u8, sword: byte(0x00 or 0), dx: -4'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 203'u8, sword: byte(0x00 or 0), dx: -4'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 204'u8, sword: byte(0x00 or 0), dx: -4'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 205'u8, sword: byte(0x00 or 0), dx: -5'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 206'u8, sword: byte(0x00 or 0), dx: -5'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 207'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0x40 or 6)),
      FrameType(image: 208'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0xC0 or 6)),
      FrameType(image: 209'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0xC0 or 8)),
      FrameType(image: 210'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 1'i8,
          flags: byte(0x40 or 10)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 52'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 53'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 54'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 55'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 56'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 57'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 58'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 59'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 60'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 61'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 62'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 63'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 160'u8, sword: byte(0x00 or 35), dx: 1'i8, dy: 1'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 161'u8, sword: byte(0x00 or 36), dx: 0'i8, dy: 1'i8,
          flags: byte(0x40 or 9)),
      FrameType(image: 162'u8, sword: byte(0x00 or 37), dx: 0'i8, dy: 1'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 163'u8, sword: byte(0x00 or 38), dx: 0'i8, dy: 1'i8,
          flags: byte(0x40 or 9)),
      FrameType(image: 164'u8, sword: byte(0x00 or 39), dx: 0'i8, dy: 1'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 165'u8, sword: byte(0x00 or 40), dx: 1'i8, dy: 1'i8,
          flags: byte(0x40 or 9)),
      FrameType(image: 166'u8, sword: byte(0x00 or 41), dx: 1'i8, dy: 1'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 167'u8, sword: byte(0x00 or 42), dx: 1'i8, dy: 1'i8,
          flags: byte(0xC0 or 9)),
      FrameType(image: 168'u8, sword: byte(0x00 or 0), dx: 4'i8, dy: 1'i8,
          flags: byte(0xC0 or 6)),
      FrameType(image: 169'u8, sword: byte(0x00 or 0), dx: 3'i8, dy: 1'i8,
          flags: byte(0xC0 or 10)),
      FrameType(image: 170'u8, sword: byte(0x00 or 0), dx: 1'i8, dy: 1'i8,
          flags: byte(0x40 or 3)),
      FrameType(image: 171'u8, sword: byte(0x00 or 0), dx: 1'i8, dy: 1'i8,
          flags: byte(0xC0 or 8))
    ]

  # data:1496
  frameTblGuard: array[41, FrameType] = [
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 12'u8, sword: byte(0xC0 or 13), dx: 2'i8, dy: 1'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 2'u8, sword: byte(0xC0 or 1), dx: 3'i8, dy: 1'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 3'u8, sword: byte(0xC0 or 2), dx: 4'i8, dy: 1'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 4'u8, sword: byte(0xC0 or 3), dx: 7'i8, dy: 1'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 5'u8, sword: byte(0xC0 or 4), dx: 10'i8, dy: 1'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 6'u8, sword: byte(0xC0 or 5), dx: 7'i8, dy: 1'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 7'u8, sword: byte(0xC0 or 6), dx: 4'i8, dy: 1'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 8'u8, sword: byte(0xC0 or 7), dx: 0'i8, dy: 1'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 9'u8, sword: byte(0xC0 or 8), dx: 0'i8, dy: 1'i8,
          flags: byte(0xC0 or 13)),
      FrameType(image: 10'u8, sword: byte(0xC0 or 11), dx: 7'i8, dy: 1'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 11'u8, sword: byte(0xC0 or 12), dx: 3'i8, dy: 1'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 12'u8, sword: byte(0xC0 or 13), dx: 2'i8, dy: 1'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 13'u8, sword: byte(0xC0 or 0), dx: 2'i8, dy: 1'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 14'u8, sword: byte(0xC0 or 28), dx: 0'i8, dy: 1'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 15'u8, sword: byte(0xC0 or 29), dx: 0'i8, dy: 1,
          flags: byte(0x80 or 0)),
      FrameType(image: 16'u8, sword: byte(0xC0 or 30), dx: 2'i8, dy: 1'i8,
          flags: byte(0xC0 or 3)),
      FrameType(image: 17'u8, sword: byte(0xC0 or 9), dx: -1'i8, dy: 1'i8,
          flags: byte(0x40 or 8)),
      FrameType(image: 18'u8, sword: byte(0xC0 or 10), dx: 7'i8, dy: 1'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 19'u8, sword: byte(0xC0 or 14), dx: 3'i8, dy: 1'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 9'u8, sword: byte(0xC0 or 8), dx: 0'i8, dy: 1'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 20'u8, sword: byte(0xC0 or 8), dx: 0'i8, dy: 1'i8,
          flags: byte(0xC0 or 13)),
      FrameType(image: 21'u8, sword: byte(0xC0 or 8), dx: 0'i8, dy: 1'i8,
          flags: byte(0xC0 or 13)),
      FrameType(image: 22'u8, sword: byte(0xC0 or 47), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 6)),
      FrameType(image: 23'u8, sword: byte(0xC0 or 48), dx: 0'i8, dy: 0'i8,
          flags: byte(0x40 or 6)),
      FrameType(image: 24'u8, sword: byte(0xC0 or 49), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 5)),
      FrameType(image: 24'u8, sword: byte(0xC0 or 49), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 5)),
      FrameType(image: 24'u8, sword: byte(0xC0 or 49), dx: 0'i8, dy: 0'i8,
          flags: byte(0xC0 or 5)),
      FrameType(image: 26'u8, sword: byte(0xC0 or 0), dx: 0'i8, dy: 3'i8,
          flags: byte(0x80 or 10)),
      FrameType(image: 27'u8, sword: byte(0xC0 or 0), dx: 4'i8, dy: 4'i8,
          flags: byte(0x80 or 7)),
      FrameType(image: 28'u8, sword: byte(0xC0 or 0), dx: -2'i8, dy: 1'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 29'u8, sword: byte(0xC0 or 0), dx: -2'i8, dy: 1'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 30'u8, sword: byte(0xC0 or 0), dx: -2'i8, dy: 1'i8,
          flags: byte(0x40 or 4)),
      FrameType(image: 31'u8, sword: byte(0xC0 or 0), dx: -2'i8, dy: 2'i8,
          flags: byte(0x40 or 7)),
      FrameType(image: 32'u8, sword: byte(0xC0 or 0), dx: -2'i8, dy: 2'i8,
          flags: byte(0x40 or 10)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 33'u8, sword: byte(0xC0 or 0), dx: 3'i8, dy: 4'i8,
          flags: byte(0xC0 or 9)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0))
    ]

  # data:1564
  frameTblCuts: array[86, FrameType] = [
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 15'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 1'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 2'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 3'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 4'u8, sword: byte(0x40 or 0), dx: -1'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 5'u8, sword: byte(0x40 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 6'u8, sword: byte(0x40 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 7'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 8'u8, sword: byte(0x40 or 0), dx: 1'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 255'u8, sword: byte(0x00 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 0'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 9'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 10'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 11'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 12'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 13'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 14'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 16'u8, sword: byte(0x40 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 0'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 2'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 3'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 4'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 5'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 6'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 7'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 8'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 9'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 10'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 11'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 12'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 13'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 14'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 15'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 16'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 17'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 18'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 19'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 20'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 21'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 22'u8, sword: byte(0x80 or 0), dx: 1'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 23'u8, sword: byte(0x80 or 0), dx: -1'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 24'u8, sword: byte(0x80 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 25'u8, sword: byte(0x80 or 0), dx: 1'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 26'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 27'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 28'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 29'u8, sword: byte(0x80 or 0), dx: -1'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 0'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 1'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0,
          flags: byte(0x80 or 0)),
      FrameType(image: 2'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 3'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 4'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 5'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 6'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 7'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 8'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 9'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 10'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 11'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 12'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 13'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 14'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 15'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 16'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 17'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 18'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 19'u8, sword: byte(0x80 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 20'u8, sword: byte(0x80 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 21'u8, sword: byte(0x80 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 22'u8, sword: byte(0x80 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 23'u8, sword: byte(0x80 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 24'u8, sword: byte(0x80 or 0), dx: 5'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 25'u8, sword: byte(0x80 or 0), dx: 5'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 26'u8, sword: byte(0x80 or 0), dx: 1'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 27'u8, sword: byte(0x80 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 28'u8, sword: byte(0x80 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 29'u8, sword: byte(0x80 or 0), dx: 1'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 30'u8, sword: byte(0x80 or 0), dx: 1'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 31'u8, sword: byte(0x80 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 32'u8, sword: byte(0x80 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 33'u8, sword: byte(0x80 or 0), dx: 3'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
      FrameType(image: 34'u8, sword: byte(0x80 or 0), dx: 0'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 35'u8, sword: byte(0x80 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 36'u8, sword: byte(0x80 or 0), dx: 2'i8, dy: 0'i8,
          flags: byte(0x80 or 0)),
      FrameType(image: 37'u8, sword: byte(0x80 or 0), dx: 1'i8, dy: 0'i8,
          flags: byte(0x00 or 0)),
    ]

  blankFrame: FrameType = FrameType(image: 255'u8, sword: 0'u8, dx: 0'i8,
      dy: 0'i8, flags: 0'u8)

proc getFrameInternal(frameTable: openArray[FrameType], frame: int32,
    frameTableName: string, count: int32) =
  if (frame >= 0 and frame < count):
    curFrame = frameTable[frame]
  else:
    echo "Tried to use " & frameTableName & "[" & $(frame) & "], not in 0.." &
        $(count - 1)
    # static const frameType blankFrame = {255, 0, 0, 0, 0}
    curFrame = blankFrame


#define getFrame(frameTable, frame) getFrameInternal(frameTable, frame, #frameTable, COUNT(frameTable))
template getFrame(frameTable: typed, frame: typed) =
  getFrameInternal(frameTable, frame, "frameTable", frameTable.len)

# seg006:015A
proc loadFrame() =
  var
    frame: int16
    addFrame: int16
  frame = int16(Char.frame)
  addFrame = 0
  case (Char.charid)
  of ord(charid0Kid), ord(charid24Mouse):
    getFrame(frameTableKid, frame)
  of ord(charid2Guard), ord(charid4Skeleton):
    if (frame >= 102 and frame < 107):
      addFrame = 70
    getFrame(frameTblGuard, frame + addFrame - 149)
  of ord(charid1Shadow):
    if (frame < 150 or frame >= 190):
      getFrame(frameTableKid, frame)
    else:
      getFrame(frameTblGuard, frame + addFrame - 149)
  of ord(charid5Princess), ord(charid6Vizier):
  # useTableCutscene:
    getFrame(frameTblCuts, frame)
  else:
    discard


# seg006:01F5
proc dxWeight(): int16 =
  var
    var2: sbyte
  var2 = curFrame.dx - sbyte(curFrame.flags and ord(FRAME_WEIGHT_X))
  return int16(charDxForward(var2))


# seg006:0213
proc charDxForward(deltaX: int32): int32 =
  var
    dltX: int32 = deltaX
  if (Char.direction < ord(dir0Right)):
    dltX = -deltaX

  return dltX + int32(Char.x)


# seg006:0234
proc objDxForward(deltaX: int32): int32 =
  var
    dltX = deltaX
  if (objDirection < ord(dir0Right)):
    dltX = -deltaX

  objX += int16(dltX)
  return objX


# seg006:0254
proc playSeq() =
  while true:
    var
      item: byte = seqtbl[Char.currSeq - SEQTBL_BASE]
    inc(Char.currSeq)
    case (item)
    of ord(SEQ_DX): # dx
      Char.x = cast[byte](charDxForward(int(seqtbl[Char.currSeq -
          SEQTBL_BASE])))
      inc(Char.currSeq)
    of ord(SEQ_DY): # dy
      Char.y += seqtbl[Char.currSeq - SEQTBL_BASE]
      inc(Char.currSeq)
    of ord(SEQ_FLIP): # flip
      Char.direction = not(Char.direction)
    of ord(SEQ_JMP_IF_FEATHER): # jump if feather
      if (isFeatherFall == 0):
        inc(Char.currSeq)
        inc(Char.currSeq)
      # fallthrough!
      else:
        Char.currSeq = cast[ptr word](addr(seqtbl[Char.currSeq - SEQTBL_BASE]))[]
    of ord(SEQ_JMP): # jump
      Char.currSeq = cast[ptr word](addr(seqtbl[Char.currSeq - SEQTBL_BASE]))[]
    of ord(SEQ_UP): # up
      dec(Char.currRow)
      startChompers()
    of ord(SEQ_DOWN): # down
      incCurrRow()
      startChompers()
    of ord(SEQ_ACTION): # action
      Char.action = seqtbl[Char.currSeq - SEQTBL_BASE]
      inc(Char.currSeq)
    of ord(SEQ_SET_FALL): # set fall
      Char.fallX = cast[sbyte](seqtbl[Char.currSeq - SEQTBL_BASE])
      inc(Char.currSeq)
      Char.fallY = cast[sbyte](seqtbl[Char.currSeq - SEQTBL_BASE])
      inc(Char.currSeq)
    of ord(SEQ_KNOCK_UP): # knock up
      knock = 1
    of ord(SEQ_KNOCK_DOWN): # knock down
      knock = -1
    of ord(SEQ_SOUND): # sound
      case (seqtbl[Char.currSeq - SEQTBL_BASE]):
      of ord(SND_SILENT): # no sound actually played, but guards still notice the kid
        inc(Char.currseq)
        isGuardNotice = 1
      of ord(SND_FOOTSTEP): # feet
        inc(Char.currseq)
        playSound(ord(sound23Footstep)) # footstep
        isGuardNotice = 1
      of ord(SND_BUMP): # bump
        inc(Char.currseq)
        playSound(ord(sound8Bumped)) # touching a wall
        isGuardNotice = 1
      of ord(SND_DRINK): # drink
        inc(Char.currseq)
        playSound(ord(sound18Drink)) # drink
      of ord(SND_LEVEL): # level
        inc(Char.currseq)
        block myBlock:
          when (USE_REPLAY):
            if (recording or replaying) != 0:
              break myBlock # don't do end level music in replays
          if (isSoundOn) != 0:
            if (currentLevel == 4):
              playSound(ord(sound32ShadowMusic)) # end level with shadow (level 4)
            elif (currentLevel != 13 and currentLevel != 15):
              playSound(ord(sound41EndLevelMusic)) # end level
      else:
        discard
    of ord(SEQ_END_LEVEL): # end level
      inc(nextLevel)
      when (USE_REPLAY):
        # Preserve the seed in this frame, to ensure reproducibility of the replay in the next level,
        # regardless of how long the sound is still playing *after* this frame.
        # Animations (e.g. torch) can change the seed!
        keepLastSeed = 1
        if (replaying and skippingReplay) != 0:
          stopSounds()
    of ord(SEQ_GET_ITEM): # get item
      if (seqtbl[Char.currSeq - SEQTBL_BASE] == 1):
        inc(Char.currSeq)
        procGetObject()
      inc(Char.currSeq)
    of ord(SEQ_DIE): # nop
      discard
    else:
      Char.frame = item
      #if (Char.frame == 185) Char.frame = 185
      return


# seg006:03DE
proc getTileDivModM7(xpos: int32): int32 =
  return getTileDivMod(xpos - 7)


# data:22A6
const
  tileDivTbl: array[256, sbyte] = [
      -5'i8, -5'i8,
      -4'i8, -4'i8, -4'i8, -4'i8, -4'i8, -4'i8, -4'i8, -4'i8, -4'i8, -4'i8,
      -4'i8, -4'i8, -4'i8, -4'i8,
      -3'i8, -3'i8, -3'i8, -3'i8, -3'i8, -3'i8, -3'i8, -3'i8, -3'i8, -3'i8,
      -3'i8, -3'i8, -3'i8, -3'i8,
      -2'i8, -2'i8, -2'i8, -2'i8, -2'i8, -2'i8, -2'i8, -2'i8, -2'i8, -2'i8,
      -2'i8, -2'i8, -2'i8, -2'i8,
      -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, -1'i8,
      -1'i8, -1'i8, -1'i8, -1'i8,
      0'i8, 0'i8, 0'i8, 0'i8, 0'i8, 0'i8, 0'i8, 0'i8, 0'i8, 0'i8, 0'i8, 0'i8,
      0'i8, 0'i8,
      1'i8, 1'i8, 1'i8, 1'i8, 1'i8, 1'i8, 1'i8, 1'i8, 1'i8, 1'i8, 1'i8, 1'i8,
      1'i8, 1'i8,
      2'i8, 2'i8, 2'i8, 2'i8, 2'i8, 2'i8, 2'i8, 2'i8, 2'i8, 2'i8, 2'i8, 2'i8,
      2'i8, 2'i8,
      3'i8, 3'i8, 3'i8, 3'i8, 3'i8, 3'i8, 3'i8, 3'i8, 3'i8, 3'i8, 3'i8, 3'i8,
      3'i8, 3'i8,
      4'i8, 4'i8, 4'i8, 4'i8, 4'i8, 4'i8, 4'i8, 4'i8, 4'i8, 4'i8, 4'i8, 4'i8,
      4'i8, 4'i8,
      5'i8, 5'i8, 5'i8, 5'i8, 5'i8, 5'i8, 5'i8, 5'i8, 5'i8, 5'i8, 5'i8, 5'i8,
      5'i8, 5'i8,
      6'i8, 6'i8, 6'i8, 6'i8, 6'i8, 6'i8, 6'i8, 6'i8, 6'i8, 6'i8, 6'i8, 6'i8,
      6'i8, 6'i8,
      7'i8, 7'i8, 7'i8, 7'i8, 7'i8, 7'i8, 7'i8, 7'i8, 7'i8, 7'i8, 7'i8, 7'i8,
      7'i8, 7'i8,
      8'i8, 8'i8, 8'i8, 8'i8, 8'i8, 8'i8, 8'i8, 8'i8, 8'i8, 8'i8, 8'i8, 8'i8,
      8'i8, 8'i8,
      9'i8, 9'i8, 9'i8, 9'i8, 9'i8, 9'i8, 9'i8, 9'i8, 9'i8, 9'i8, 9'i8, 9'i8,
      9'i8, 9'i8,
      10'i8, 10'i8, 10'i8, 10'i8, 10'i8, 10'i8, 10'i8, 10'i8, 10'i8, 10'i8,
      10'i8, 10'i8, 10'i8, 10'i8,
      11'i8, 11'i8, 11'i8, 11'i8, 11'i8, 11'i8, 11'i8, 11'i8, 11'i8, 11'i8,
      11'i8, 11'i8, 11'i8, 11'i8,
      12'i8, 12'i8, 12'i8, 12'i8, 12'i8, 12'i8, 12'i8, 12'i8, 12'i8, 12'i8,
      12'i8, 12'i8, 12'i8, 12'i8,
      13'i8, 13'i8, 13'i8, 13'i8, 13'i8, 13'i8, 13'i8, 13'i8, 13'i8, 13'i8,
      13'i8, 13'i8, 13'i8, 13'i8,
      14'i8, 14'i8
    ]

  # data:23A6
  tileModTbl: array[256, byte] = [
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1'u8, 2'u8, 3'u8, 4'u8, 5'u8, 6'u8, 7'u8, 8'u8, 9'u8, 10'u8, 11'u8,
      12'u8, 13'u8,
      0'u8, 1
    ]


# seg006:03F0
proc getTileDivMod(xpos: int32): int32 =
  # Determine tile column (xh) and the position within the tile (xl) from xpos.
  # xpos might be negative if the kid is far off left.
  # In this case, the array index overflows.
  if (xpos < 0 or xpos >= 256):
    echo "getTileDivMod(): xpos = ", xpos
  # printf("getTileDivMod(): xpos = %d\n", xpos)
  #
  # DOS PoP does this:
  #  objXl = tileModTbl[xpos]
  #  return tileDivTbl[xpos]
  var
    # xpos uses a coordinate system in which the left edge of the screen is 58, and each tile is 14 units wide.
    x: int32 = xpos - 58
    xl: int32 = x mod 14
    xh: int32 = x div 14
  if (xl < 0):
    # Integer division rounds towards zero, but we want to round down.
    dec(xh)
    # Modulo returns a negative number if x is negative, but we want 0 <= xl < 14.
    xl += 14

  # For compatibility with the DOS version, we allow for overflow access to these tables
  # Considering the case of negative overflow
  if (xpos < 0):
    # In this case DOS PoP reads the bytes directly before tile_div_tbl[] and tile_mod_tbl[] in the memory.
    # Here we simulate these reads.
    # Before tile_mod_tbl[] is tile_div_tbl[], and before tile_div_tbl[] are the following bytes:
    const
      bogus: array[34, byte] = [0x02'u8, 0x00'u8, 0x41'u8, 0x00'u8, 0x80'u8,
          0x00'u8, 0xBF'u8, 0x00'u8, 0xFE'u8, 0x00'u8, 0xFF'u8, 0x01'u8,
          0x01'u8, 0xFF'u8, 0xC4'u8, 0xFF'u8, 0x03'u8, 0x00'u8, 0x42'u8,
          0x00'u8, 0x81'u8, 0x00'u8, 0xC0'u8, 0x00'u8, 0xF8'u8, 0xFF'u8,
          0x37'u8, 0x00'u8, 0x76'u8, 0x00'u8, 0xB5'u8, 0x00'u8, 0xF4'u8, 0x00]
    if (len(bogus) + xpos >= 0):
      xh = int32(bogus[len(bogus) + xpos]) # simulating tile_div_tbl[xpos]
      xl = int32(tileDivTbl[len(tileDivTbl) + xpos]) # simulating tile_mod_tbl[xpos]
    else:
      # printf("xpos = %d (< %d) out of range for simulation of index overflow!\n", xpos, -(int)COUNT(bogus));
      echo "xpos = ", xpos, "(< ", -len(bogus), ") out of range for simulation of index overflow!"

  # COnsidering the case of positive overflow
  let
    tblSize: int32 = 256

  if (xpos >= tblSize):
    # In this case DOS PoP reads the bytes directly after tile_div_tbl[], that is: and tile_mod_tbl[]
    # Here we simulate these reads.
    # After tile_mod_tbl[] there are the following bytes:
    const
      bogus: array[34, byte] = [0xF4'u8, 0x02'u8, 0x10'u8, 0x1E'u8, 0x2C'u8,
          0x3A'u8, 0x48'u8, 0x56'u8, 0x64'u8, 0x72'u8, 0x80'u8, 0x8E'u8,
          0x9C'u8, 0xAA'u8, 0xB8'u8, 0xC6'u8, 0xD4'u8, 0xE2'u8, 0xF0'u8,
          0xFE'u8, 0x00'u8, 0x0A'u8, 0x00'u8, 0xFF'u8, 0x00'u8, 0x00'u8,
          0x00'u8, 0x00'u8, 0x0A'u8, 0x0D'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8]
    if (xpos - tblSize < len(bogus)):
      xh = int32(tileModTbl[xpos-tblSize]) # simulating tileDivTbl[xpos]
      xl = int32(bogus[xpos-tblSize]) # simulating tileModTbl[xpos]
    else:
      echo "xpos = ", xpos, " (> ", len(bogus) + tblSize, ") out of range for simulation of index overflow!"

  objXl = byte(xl)
  return xh


# seg006:0433
proc yToRowMod4(ypos: int32): int32 =
  return (ypos + 60) div 63 mod 4 - 1


# seg006:044F
proc loadkid() =
  Char = Kid


# seg006:0464
proc savekid() =
  Kid = Char


# seg006:0479
proc loadshad() =
  Char = Guard


# seg006:048E
proc saveshad() =
  Guard = Char


# seg006:04A3
proc loadkidAndOpp() =
  loadkid()
  Opp = Guard


# seg006:04BC
proc savekidAndOpp() =
  savekid()
  Guard = Opp


# seg006:04D5
proc loadshadAndOpp() =
  loadshad()
  Opp = Kid


# seg006:04EE
proc saveshadAndOpp() =
  saveshad()
  Kid = Opp


# seg006:0507
proc resetObjClip() =
  objClipLeft = 0
  objClipTop = 0
  objClipRight = 320
  objClipBottom = 192


# seg006:051C
proc xToXhAndXl(xpos: int32, xhAddr, xlAddr: var sbyte) =
  if (xpos < 0):
    xhAddr = -int8((abs(-xpos) shr 3) + 1)
    xlAddr = -int8 ((-xpos - 1) mod 8 - 7)
  else:
    xhAddr = int8(abs(xpos) shr 3)
    xlAddr = int8(xpos mod 8)


# seg006:057C
proc fallAccel() =
  if (Char.action == ord(actions4InFreefall)):
    if isFeatherFall != 0:
      inc(Char.fallY)
      if (Char.fallY > 4):
        Char.fallY = 4
    else:
      Char.fallY += 3
      if (Char.fallY > 33):
        Char.fallY = 33


# seg006:05AE
proc fallSpeed() =
  Char.y += byte(Char.fallY)
  if (Char.action == ord(actions4InFreefall)):
    Char.x = byte(charDxForward(Char.fallX))
    loadFramDetCol()


# seg006:05CD
proc checkAction() =
  var
    frame: int16
    action: int16
  action = int16(Char.action)
  frame = int16(Char.frame)
  # frame 109: crouching
  if (action == ord(actions6HangStraight) or
    action == ord(actions5Bumped)
  ):
    if frame == ord(frame109Crouch) or
        (when(FIX_STAND_ON_THIN_AIR):
        ((fixes.fixStandOnThinAir != 0) and
        frame >= ord(frame110StandUpFromCrouch1) and
        frame <= ord(frame119StandUpFromCrouch10)) else: false):
      checkOnFloor()
  elif (action == ord(actions4InFreefall)):
    doFall()
  elif (action == ord(actions3InMidair)):
    # frame 102..106: start fall + fall
    if (frame >= ord(frame102StartFall1) and frame < ord(frame106Fall)):
      checkGrab()
  elif (action != ord(actions2HangClimb)):
    checkOnFloor()


# seg006:0628
proc tileIsFloor(tiletype: int32): int32 =
  case (tiletype)
  of ord(tiles0Empty):
    return 0
  of ord(tiles9BigpillarTop):
    return 0
  of ord(tiles12Doortop):
    return 0
  of ord(tiles20Wall):
    return 0
  of ord(tiles26LatticeDown):
    return 0
  of ord(tiles27LatticeSmall):
    return 0
  of ord(tiles28LatticeLeft):
    return 0
  of ord(tiles29LatticeRight):
    return 0
  else:
    return 1


# seg006:0658
proc checkSpiked() =
  var
    harmful: int16
    frame: int16
  frame = int16(Char.frame)
  if getTile(int(Char.room), int32(Char.currCol), int32(Char.currRow)) == ord(tiles2Spike):
    harmful = int16(isSpikeHarmful())
    # frames 7..14: running
    # frames 34..39: start run-jump
    # frame 43: land from run-jump
    # frame 26: lang from standing jump
    if (
      (harmful >= 2 and ((frame >= ord(frame7Run) and frame < 15) or (frame >=
          ord(frame34StartRunJump1) and frame < 40))) or
      ((frame == ord(frame43RunningJump4) or frame == ord(
          frame26StandingJump11)) and harmful != 0)
    ):
      spiked()


# seg006:06BD
proc takeHp(count: int32): int32 =
  var
    dead: word
  dead = 0
  if (Char.charid == ord(charid0Kid)):
    if (count >= int32(hitpCurr)):
      hitpDelta = -int16(hitpCurr)
      dead = 1
    else:
      hitpDelta = -int16(count)
  else:
    if (count >= int32(guardhpCurr)):
      guardhpDelta = -int16(guardhpCurr)
      dead = 1
    else:
      guardhpDelta = -int16(count)
  return int32(dead)


# seg006:070D
proc getTileAtChar(): int32 =
  return getTile(int(Char.room), int32(Char.currCol), int32(Char.currRow))


# seg006:0723
proc setCharCollision() =
  var
    image: ImageType = getImage(int16(objChtab), int32(objId))
  if (image == nil):
    charWidthHalf = 0
    charHeight = 0
  else:
    charWidthHalf = word( (image.w + 1) div 2)
    charHeight = word(image.h)

  charXLeft = objX div 2 + 58
  if (Char.direction >= ord(dir0Right)):
    charXLeft -= int16(charWidthHalf)

  charXLeftColl = charXLeft
  charXRightColl = charXLeft + int16(charWidthHalf)
  charXRight = charXRightColl
  charTopY = int16(objY) - int16(charHeight) + 1
  if (charTopY >= 192):
    charTopY = 0

  charTopRow = int16(yToRowMod4(charTopY))
  charBottomRow = int16(yToRowMod4(int(objY)))
  if (charBottomRow == -1):
    charBottomRow = 3

  charColLeft = int16(max(getTileDivMod(charXLeft), 0))
  charColRight = int16(min(getTileDivMod(charXRight), 9))
  if (curFrame.flags and ord(FRAME_THIN)) != 0:
    # "thin" this frame for collision detection
    charXLeftColl += 4
    charXRightColl -= 4


# seg006:0815
proc checkOnFloor() =
  if (curFrame.flags and ord(FRAME_NEEDS_FLOOR)) != 0:
    if (getTileAtChar() == ord(tiles20Wall)):
      inWall()

    if (tileIsFloor(int(currTile2)) == 0):
      # Special event: floors appear
      if (currentLevel == 12 and
          (when (FIX_HIDDEN_FLOORS_DURING_FLASHING):
          (unitedWithShadow < 0 or
          ((fixes.fixHiddenFloorsDuringFlashing != 0) and unitedWithShadow > 0)) else:
          unitedWithShadow < 0) and
          Char.currRow == 0 and
          (Char.room == 2 or (Char.room == 13 and tileCol >= 6))
      ):
        currRoomTiles[currTilepos] = ord(tiles1Floor)
        setWipe(int16(currTilepos), 1)
        setRedrawFull(int16(currTilepos), 1)
        inc(currTilepos)
        setWipe(int16(currTilepos), 1)
        setRedrawFull(int16(currTilepos), 1)
      else:
        when (FIX_STAND_ON_THIN_AIR):
          if ((fixes.fixStandOnThinAir != 0) and
              Char.frame >= ord(frame110StandUpFromCrouch1) and
              Char.frame <= ord(frame119StandUpFromCrouch10)):
            # We need to prevent the Kid from stepping off a ledge accidentally while standing up.
            # (This can happen because the "standing up" frames now require a floor.)
            # -. Cancel the fall, if the tile at dx=2 behind the kid is a valid floor.
            var
              col: int32 = getTileDivModM7(dxWeight() + backDeltaX(2))
            if (tileIsFloor(getTile(int(Char.room), col, int32(
                Char.currRow))) != 0):
              return
        startFall()


# seg006:08B9
proc startFall() =
  var
    frame: int16
    seqId: word
  frame = int16(Char.frame)
  Char.sword = ord(sword0Sheathed)
  incCurrRow()
  startChompers()
  fallFrame = byte(frame)
  if (frame == ord(frame9Run)):
    # frame 9: run
    seqId = ord(seq7Fall) # fall (when?)
  elif (frame == ord(frame13Run)):
    # frame 13: run
    seqId = ord(seq19Fall) # fall (when?)
  elif (frame == ord(frame26StandingJump11)):
    # frame 26: land after standing jump
    seqId = ord(seq18FallAfterStandingJump) # fall after standing jump
  elif (frame == ord(frame44RunningJump5)):
    # frame 44: land after running jump
    seqId = ord(seq21FallAfterRunningJump) # fall after running jump
  elif (frame >= ord(frame81Hangdrop1) and frame < 86):
    # frame 81..85: land after jump up
    seqId = ord(seq19Fall) # fall after jumping up
    Char.x = byte(charDxForward(5))
    loadFramDetCol()
  elif (frame >= 150 and frame < 180):
    # frame 150..179: with sword + fall + dead
    if (Char.charid == ord(charid2Guard)):
      if (Char.currRow == 3 and Char.currCol == 10):
        clearChar()
        return
      if (Char.fallX < 0):
        seqId = ord(seq82GuardPushedOffLedge) # Guard is pushed off the ledge
        if (Char.direction < ord(dir0Right) and distanceToEdgeWeight() <= 7):
          Char.x = byte(charDxForward(-5))
      else:
        droppedout = 0
        seqId = ord(seq83GuardFall) # fall after forwarding with sword
    else:
      droppedout = 1
      if (Char.direction < ord(dir0Right) and distanceToEdgeWeight() <= 7):
        Char.x = byte(charDxForward(-5))
      seqId = ord(seq81KidPushedOffLedge) # fall after backing with sword / Kid is pushed off the ledge
  else:
    seqId = ord(seq7Fall) # fall after stand, run, step, crouch
  seqtblOffsetChar(int16(seqId))
  playSeq()
  loadFramDetCol()
  if (getTileAtChar() == ord(tiles20Wall)):
    inWall()
    return

  var
    tile: int32 = getTileInfrontofChar()
  if (tile == ord(tiles20Wall) or
      (when (FIX_RUNNING_JUMP_THROUGH_TAPESTRY):
      # Also treat tapestries (when approached to the left) like a wall here.
      ((fixes.fixRunningJumpThroughTapestry != 0) and Char.direction == ord(
          dirFFLeft) and
      (tile == ord(tiles12Doortop) or tile == ord(
          tiles7DoortopWithFloor))) else: false)
  ):
    if (fallFrame != 44 or distanceToEdgeWeight() >= 6):
      Char.x = byte(charDxForward(-1))
    else:
      seqtblOffsetChar(ord(seq104StartFallInFrontOfWall)) # start fall (when?)
      playSeq()
    loadFramDetCol()


# seg006:0A19
proc checkGrab() =
  var
    oldX: word

  when (FIX_GRAB_FALLING_SPEED):
    let
      MAX_GRAB_FALLING_SPEED = if fixes.fixGrabFallingSpeed != 0: 30 else: 32
  else:
    const
      MAX_GRAB_FALLING_SPEED = 32

  if (controlShift < 0 and # press shift to grab
    Char.fallY < MAX_GRAB_FALLING_SPEED and # you can't grab if you're falling too fast ...
    Char.alive < 0 and # ... or dead
    yLand[Char.currRow + 1] <= int16(Char.y) + 25
  ):
    #printf("Falling speed: %d\t x: %d\n", Char.fallY, Char.x)
    oldX = Char.x
    Char.x = byte(charDxForward(-8))
    loadFramDetCol()
    if (canGrabFrontAbove() == 0):
      Char.x = byte(oldX)
    else:
      Char.x = byte(charDxForward(distanceToEdgeWeight()))
      Char.y = byte(yLand[Char.currRow + 1])
      Char.fallY = 0
      seqtblOffsetChar(ord(seq15GrabLedgeMidair)) # grab a ledge (after falling)
      playSeq()
      grabTimer = 12
      playSound(ord(sound9Grab)) # grab
      isScreaming = 0
      when (FIX_CHOMPERS_NOT_STARTING):
        if fixes.fixChompersNotStarting != 0:
          startChompers()


# seg006:0ABD
proc canGrabFrontAbove(): int32 =
  throughTile = byte(getTileAboveChar())
  discard getTileFrontAboveChar()
  return canGrab()


# seg006:0ACD
proc inWall() =
  var
    deltaX: int16
  deltaX = int16(distanceToEdgeWeight())
  if (deltaX >= 8 or getTileInfrontofChar() == ord(tiles20Wall)):
    deltaX = 6 - deltaX
  else:
    deltaX += 4

  Char.x = byte(charDxForward(deltaX))
  loadFramDetCol()
  discard getTileAtChar()


# seg006:0B0C
proc getTileInfrontofChar(): int32 =
  infrontx = dirFront[Char.direction + 1] + Char.currCol
  return getTile(int(Char.room), int32(infrontx), int32(Char.currRow))


# seg006:0B30
proc getTileInfrontof2Char(): int32 =
  var
    var2: int16
  var2 = dirFront[Char.direction + 1]
  infrontx = sbyte( (var2 shl 1) + Char.currCol)
  return getTile(int(Char.room), int32(infrontx), int32(Char.currRow))


# seg006:0B66
proc getTileBehindChar(): int32 =
  return getTile(int(Char.room), int32(dirBehind[Char.direction + 1] +
      Char.currCol), int32(Char.currRow))


# seg006:0B8A
proc distanceToEdgeWeight(): int32 =
  return distanceToEdge(dxWeight())


# seg006:0B94
proc distanceToEdge(xpos: int32): int32 =
  var
    distance: int16
  discard getTileDivModM7(xpos)
  distance = int16(objXl)
  if (Char.direction == ord(dir0Right)):
    distance = 13 - distance

  return int32(distance)


# seg006:0BC4
proc fellOut() =
  if (Char.alive < 0 and Char.room == 0):
    discard takeHp(100)
    Char.alive = 0
    eraseBottomText(1)
    Char.frame = ord(frame185Dead) # dead


# seg006:0BEE
proc playKid() =
  fellOut()
  controlKid()
  if (Char.alive >= 0 and (isDead() != 0)):
    if resurrectTime != 0:
      stopSounds()
      loadkid()
      hitpDelta = int16(hitpMax)
      seqtblOffsetChar(ord(seq2Stand)) # stand
      Char.x += 8
      playSeq()
      loadFramDetCol()
      setStartPos()

    if ((checkSoundPlaying() != 0) and currentSound != 5): # gate opening
      return

    isShowTime = 0
    if (Char.alive < 0 or Char.alive >= 6):
      if (Char.alive == 6):
        if ((isSoundOn != 0) and
            currentLevel != 0 and # no death music on demo level
          currentLevel != 15 # no death music on potions level
        ):
          playDeathMusic()
      else:
        if (Char.alive != 7 or (checkSoundPlaying() != 0)):
          return
        if (remMin == 0):
          expired()

        if (currentLevel != 0 and # no message if died on demo level
          currentLevel != 15 # no message if died on potions level
        ):
          textTimeRemaining = 288
          textTimeTotal = 288
          displayTextBottom("Press Button to Continue")
        else:
          textTimeRemaining = 36
          textTimeTotal = 36
    inc(Char.alive)



# seg006:0CD1
proc controlKid() =
  var
    key: word
  if (Char.alive < 0 and hitpCurr == 0):
    Char.alive = 0
    # stop feather fall when kid dies
    if ((fixes.fixQuicksaveDuringFeather != 0'u8) and isFeatherFall > 0'u16):
      isFeatherFall = 0
      if (checkSoundPlaying() != 0):
        stopSounds()

  if (grabTimer != 0):
    dec(grabTimer)

  if (when(USE_REPLAY): currentLevel == 0 and not(playDemoLevel) and not(
      replaying) else: currentLevel == 0 and not(playDemoLevel)):
    doDemo()
    control()
    # The player can start a new game or load a saved game during the demo.
    key = uint16(keyTestQuit())
    if (key == (ord(SCANCODE_L) or ord(WITH_CTRL))): # ctrl-L
      if loadGame() != 0:
        startGame()
    else:
      if key != 0:
        startLevel = int16(custom.firstLevel) # 1
        startGame()
  else:
    restCtrl1()
    discard doPaused()
    when (USE_REPLAY):
      if recording != 0:
        addReplayMove()
      if replaying != 0:
        doReplayMove()
    readUserControl()
    userControl()
    saveCtrl1()


## This was moved to customOptionsType.
#/*
#const autoMoveType demoMoves[] = {
#{0x00, 0},
#{0x01, 1},
#{0x0D, 0},
#{0x1E, 1},
#{0x25, 5},
#{0x2F, 0},
#{0x30, 1},
#{0x41, 0},
#{0x49, 2},
#{0x4B, 0},
#{0x63, 2},
#{0x64, 0},
#{0x73, 5},
#{0x80, 6},
#{0x88, 3},
#{0x9D, 7},
#{0x9E, 0},
#{0x9F, 1},
#{0xAB, 4},
#{0xB1, 0},
#{0xB2, 1},
#{0xBC, 0},
#{0xC1, 1},
#{0xCD, 0},
#{0xE9,-1},

#*/

# seg006:0D49
proc doDemo() =
  if checkpoint != 0:
    controlShift2 = sbyte(releaseArrows())
    controlForward = -1
    controlX = -1
  elif Char.sword != 0:
    guardSkill = 10
    autocontrolOpponent()
    guardSkill = 11
  else:
    doAutoMoves(custom.demoMoves)


# seg006:0D85
proc playGuard() =
  if (Char.charid == ord(charid24Mouse)):
    autocontrolOpponent()
  else:
    if (Char.alive < 0):
      if (guardhpCurr == 0):
        Char.alive = 0
        onGuardKilled()
        if (Char.charid == ord(charid1Shadow)):
          clearChar()
    autocontrolOpponent()
    control()


# seg006:0DC0
proc userControl() =
  if (Char.direction >= ord(dir0Right)):
    flipControlX()
    control()
    flipControlX()
  else:
    control()


# seg006:0DDC
proc flipControlX() =
  var
    temp: byte
  controlX = -controlX
  temp = cast[byte](controlForward)
  controlForward = controlBackward
  controlBackward = cast[sbyte](temp)


# seg006:0E00
proc releaseArrows(): int32 =
  controlBackward = 0
  controlForward = 0
  controlUp = 0
  controlDown = 0
  return 1


# seg006:0E12
proc saveCtrl1() =
  ctrl1Forward = controlForward
  ctrl1Backward = controlBackward
  ctrl1Up = controlUp
  ctrl1Down = controlDown
  ctrl1Shift2 = controlShift2


# seg006:0E31
proc restCtrl1() =
  controlForward = ctrl1Forward
  controlBackward = ctrl1Backward
  controlUp = ctrl1Up
  controlDown = ctrl1Down
  controlShift2 = ctrl1Shift2


# seg006:0E8E
proc clearSavedCtrl() =
  ctrl1Forward = 0
  ctrl1Backward = 0
  ctrl1Up = 0
  ctrl1Down = 0
  ctrl1Shift2 = 0


# seg006:0EAF
proc readUserControl() =
  if (controlForward >= 0):
    if (controlX < 0):
      if (controlForward == 0):
        controlForward = -1
    else:
      controlForward = 0
  if (controlBackward >= 0):
    if (controlX == 1):
      if (controlBackward == 0):
        controlBackward = -1
    else:
      controlBackward = 0
  if (controlUp >= 0):
    if (controlY < 0):
      if (controlUp == 0):
        controlUp = -1
    else:
      controlUp = 0
  if (controlDown >= 0):
    if (controlY == 1):
      if (controlDown == 0):
        controlDown = -1
    else:
      controlDown = 0
  if (controlShift2 >= 0):
    if (controlShift < 0):
      if (controlShift2 == 0):
        controlShift2 = -1
    else:
      controlShift2 = 0


# seg006:0F55
proc canGrab(): int32 =
  var
  # Can char grab currTile2 through throughTile?
    modifier: byte
  modifier = currRoomModif[currTilepos]
  # can't grab through wall
  if (throughTile == ord(tiles20Wall)):
    return 0
  # can't grab through a door top if looking right
  if (throughTile == ord(tiles12Doortop) and Char.direction >= ord(dir0Right)):
    return 0
  # can't grab through floor
  if tileIsFloor(int(throughTile)) != 0:
    return 0
  # can't grab a shaking loose floor
  # Allow climbing onto a shaking loose floor if the delay is greater than the default. TODO: This should be a separate option.
  if (currTile2 == ord(tiles11Loose) and modifier != 0 and not(
      custom.looseFloorDelay > 11)):
    return 0
  # a doortop with floor can be grabbed only from the left (looking right)
  if (currTile2 == ord(tiles7DoortopWithFloor) and Char.direction < ord(dir0Right)):
    return 0
  # can't grab something that has no floor
  if tileIsFloor(int(currTile2)) == 0:
    return 0
  return 1


# seg006:0FC3
proc wallType(tiletype: byte): int32 =
  case (tiletype)
  of ord(tiles4Gate), ord(tiles7DoortopWithFloor), ord(tiles12Doortop):
    return 1 # wall at right
  of ord(tiles13Mirror):
    return 2 # wall at left
  of ord(tiles18Chomper):
    return 3 # chomper at left
  of ord(tiles20Wall):
    return 4 # wall at both sides
  else:
    return 0 # no wall


# seg006:1005
proc getTileAboveChar(): int32 =
  return getTile(int(Char.room), int32(Char.currCol), int32(Char.currRow) - 1)


# seg006:1020
proc getTileBehindAboveChar(): int32 =
  return getTile(int(Char.room), int32(dirBehind[Char.direction + 1] +
      Char.currCol), int32(Char.currRow) - 1)


# seg006:1049
proc getTileFrontAboveChar(): int32 =
  infrontx = dirFront[Char.direction + 1] + Char.currCol
  return getTile(int(Char.room), int32(infrontx), int32(Char.currRow) - 1)


# seg006:1072
proc backDeltaX(deltaX: int32): int32 =
  if (Char.direction < ord(dir0Right)):
    # direction = left
    return deltaX
  else:
    # direction = right
    return -deltaX


# seg006:108A
proc doPickup(objType: int32) =
  pickupObjType = int16(objType)
  controlShift2 = 1
  # erase picked up item
  currRoomTiles[currTilepos] = ord(tiles1Floor)
  currRoomModif[currTilepos] = 0
  redrawHeight = 35
  setWipe(int16(currTilepos), 1)
  setRedrawFull(int16(currTilepos), 1)


# seg006:10E6
proc checkPress() =
  var
    frame: int16
    action: int16
  frame = int16(Char.frame)
  action = int16(Char.action)
  # frames 87..99: hanging
  # frames 135..140: start climb up
  if ((frame >= ord(frame87Hanging1) and frame < 100) or (frame >= ord(
      frame135Climbing1) and frame < ord(frame141Climbing7))):
    # the pressed tile is the one that the char is grabbing
    discard getTileAboveChar()
  elif (action == ord(actions7Turn) or action == ord(actions5Bumped) or action <
      ord(actions2HangClimb)):
    # frame 79: jumping up
    if (frame == ord(frame79Jumphang) and getTileAboveChar() == ord(tiles11Loose)):
      # break a loose floor from above
      makeLooseFall(1)
    else:
      # the pressed tile is the one that the char is standing on
      if (curFrame.flags and ord(FRAME_NEEDS_FLOOR)) == 0:
        return
      when (FIX_PRESS_THROUGH_CLOSED_GATES):
        if fixes.fixPressThroughClosedGates != 0:
          determineCol()
      discard getTileAtChar()
  else:
    return
  if (currTile2 == ord(tiles15Opener) or currTile2 == ord(tiles6Closer)):
    if (Char.alive < 0):
      triggerButton(1, 0, -1)
    else:
      diedOnButton()
  elif (currTile2 == ord(tiles11Loose)):
    isGuardNotice = 1
    makeLooseFall(1)


# seg006:1199
proc checkSpikeBelow() =
  var
    notFinished: int16 = 1
    room: int16
    row: int16
    rightCol: int16
  rightCol = int16(getTileDivModM7(charXRight))
  if (rightCol < 0):
    return
  row = Char.currRow
  room = int16(Char.room)
  for col in getTileDivModM7(charXLeft)..rightCol:
    row = Char.currRow
    notFinished = 1
    while (notFinished != 0):
      notFinished = 0
      if (getTile(room, col, row) == ord(tiles2Spike)):
        startAnimSpike(currRoom, int16(currTilepos))
      elif (
          (tileIsFloor(int(currTile2)) == 0) and
          currRoom != 0 and
          (when (FIX_INFINITE_DOWN_BUG): (if fixes.fixInfiniteDownBug != 0: (
              row <= 2) else: (room == currRoom)) else: room == currRoom)
      ):
        inc(row)
        notFinished = 1


# seg006:1231
proc clipChar() =
  var
    frame: int16
    room: int16
    action: int16
    col: int16
    var_A: int16
    row: int16
    var_E: int16
  frame = int16(Char.frame)
  action = int16(Char.action)
  room = int16(Char.room)
  row = Char.currRow
  resetObjClip()
  # frames 217..228: going up the level door
  if (frame >= ord(frame224ExitStairs8) and frame < 229):
    objClipTop = int16(leveldoorYbottom + 1)
    objClipRight = int16(leveldoorRight)
  else:
    if (
      getTile(room, charColLeft, charTopRow) == ord(tiles20Wall) or
      (tileIsFloor(int(currTile2)) != 0)
    ):
      # frame 79: jump up, frame 81: grab
      if ((action == ord(actions0Stand) and (frame == ord(frame79Jumphang) or
          frame == ord(frame81Hangdrop1))) or
        getTile(room, charColRight, charTopRow) == ord(tiles20Wall) or
        (tileIsFloor(int(currTile2)) != 0)
      ):
        var_E = row + 1
        var_A = yClip[var_E]
        if (var_E == 1 or
          (var_A < int16(objY) and var_A - 15 < charTopY)
        ):
          objClipTop = yClip[var_E]
          charTopY = objClipTop
    col = int16(getTileDivMod(charXLeftColl - 4))
    if (getTile(room, col + 1, row) == ord(tiles7DoortopWithFloor) or
      currTile2 == ord(tiles12Doortop)
    ):
      objClipRight = (tileCol shl 5) + 32
    else:
      if ((getTile(room, col, row) != ord(tiles7DoortopWithFloor) and
        currTile2 != ord(tiles12Doortop)) or
        action == ord(actions3InMidair) or
        (action == ord(actions4InFreefall) and frame == ord(frame106Fall)) or
        (action == ord(actions5Bumped) and frame == ord(frame107FallLand1)) or
        (Char.direction < ord(dir0Right) and (
          action == ord(actions2HangClimb) or
          action == ord(actions6HangStraight) or
          (action == ord(actions1RunJump) and
          frame >= ord(frame137Climbing3) and frame < ord(frame140Climbing6))
        ))
      ):
        if (
          (getTile(room, col = getTileDivMod(charXRightColl), row) == ord(
              tiles20Wall) or
          (currTile2 == ord(tiles13Mirror) and Char.direction == ord(
              dir0Right))) and
          (getTile(room, col, charTopRow) == ord(tiles20Wall) or
          currTile2 == ord(tiles13Mirror)) and
          room == currRoom
        ):
          objClipRight = tileCol shl 5
      else:
        objClipRight = (tileCol shl 5) + 32


# seg006:13E6
proc stuckLower() =
  if (getTileAtChar() == ord(tiles5Stuck)):
    inc(Char.y)


# seg006:13F3
proc setObjtileAtChar() =
  var
    charFrame: int16
    charAction: int16
  charFrame = int16(Char.frame)
  charAction = int16(Char.action)
  if (charAction == ord(actions1RunJump)):
    tileRow = charBottomRow
    tileCol = charColLeft
  else:
    tileRow = Char.currRow
    tileCol = Char.currCol

  # frame 135..148: climbing
  if ((charFrame >= ord(frame135Climbing1) and charFrame < 149) or
    charAction == ord(actions2HangClimb) or
    charAction == ord(actions3InMidair) or
    charAction == ord(actions4InFreefall) or
    charAction == ord(actions6HangStraight)
  ):
    dec(tileCol)

  objTilepos = byte(getTileposNominus(tileCol, tileRow))
  #printf("setObjtileAtChar: objTile = %d\n", objTile) # debug


# seg006:1463
proc procGetObject() =
  if (Char.charid != ord(charid0Kid) or pickupObjType == 0):
    return
  if (pickupObjType == -1):
    # Note: C code uses -1, which is equal to 65355 for uint16_t
    haveSword = high(typeof(haveSword))
    playSound(ord(sound37Victory)) # get sword
    flashColor = ord(color14Brightyellow)
    flashTime = 8
  else:
    dec(pickupObjType)
    case (pickupObjType)
    of 0: # health
      if (hitpCurr != hitpMax):
        stopSounds()
        playSound(ord(sound33SmallPotion)) # small potion
        hitpDelta = 1
        flashColor = ord(color4Red)
        flashTime = 2
    of 1: # life
      stopSounds()
      playSound(ord(sound30BigPotion)) # big potion
      flashColor = ord(color4Red)
      flashTime = 4
      addLife()
    of 2: # feather
      featherFall()
    of 3: # invert
      toggleUpside()
    of 5: # open
      discard getTile(8, 0, 0)
      triggerButton(0, 0, -1)
    of 4: # hurt
      stopSounds()
      playSound(ord(sound13KidHurt)) # Kid hurt (by potion)
      # Special event: blue potions on potions level take half of HP
      if (currentLevel == 15):
        hitpDelta = -int16((hitpMax + 1) shr 1)
      else:
        hitpDelta = -1
    else:
      discard


# seg006:1599
proc isDead(): int32 =
  # 177: spiked, 178: chomped, 185: dead
  # or maybe this was a switch-case?
  return int32(Char.frame >= ord(frame177Spiked) and (Char.frame <= ord(
      frame178Chomped) or Char.frame == ord(frame185Dead)))


# seg006:15B5
proc playDeathMusic() =
  var
    soundId: word
  if (Guard.charid == ord(charid1Shadow)):
    soundId = ord(sound32ShadowMusic) # killed by shadow
  elif holdingSword != 0:
    soundId = ord(sound28DeathInFight) # death in fight
  else:
    soundId = ord(sound24DeathRegular) # death not in fight
  playSound(int(soundId))


# seg006:15E8
proc onGuardKilled() =
  if (currentLevel == 0):
    # demo level: after killing Guard, run out of room
    checkpoint = 1
    demoIndex = 0
    demoTime = 0
  elif (currentLevel == custom.jaffarVictoryLevel):
    # Jaffar's level: flash
    flashColor = ord(color15Brightwhite) # white
    flashTime = custom.jaffarVictoryFlashTime
    isShowTime = 1
    leveldoorOpen = 2
    playSound(ord(sound43VictoryJaffar)) # Jaffar's death
  elif (Char.charid != ord(charid1Shadow)):
    playSound(ord(sound37Victory)) # Guard's death


# seg006:1634
proc clearChar() =
  Char.direction = ord(dir56None)
  Char.alive = 0
  Char.action = 0
  drawGuardHp(0, int16(guardhpCurr))
  guardhpCurr = 0


var
  # data:42EC
  obj2Tilepos: byte
  # data:34A6
  obj2X: word
  # data:34A8
  obj2Y: byte
  # data:599E
  obj2Direction: sbyte
  # data:5948
  obj2Id: byte
  # data:42BE
  obj2Chtab: byte
  # data:4D90
  obj2ClipTop: int16
  # data:460C
  obj2ClipBottom: int16
  # data:4C94
  obj2ClipLeft: int16
  # data:4CDE
  obj2ClipRight: int16

# seg006:1654
proc saveObj() =
  obj2Tilepos = objTilepos
  obj2X = uint16(objX)
  obj2Y = objY
  obj2Direction = objDirection
  obj2Id = objId
  obj2Chtab = objChtab
  obj2ClipTop = objClipTop
  obj2ClipBottom = objClipBottom
  obj2ClipLeft = objClipLeft
  obj2ClipRight = objClipRight


# seg006:1691
proc loadObj() =
  objTilepos = obj2Tilepos
  objX = int16(obj2X)
  objY = obj2Y
  objDirection = obj2Direction
  objId = obj2Id
  objChtab = obj2Chtab
  objClipTop = obj2ClipTop
  objClipBottom = obj2ClipBottom
  objClipLeft = obj2ClipLeft
  objClipRight = obj2ClipRight


# seg006:16CE
proc drawHurtSplash() =
  var
    frame: int16
  frame = int16(Char.frame)
  if (frame != ord(frame178Chomped)): # chomped
    saveObj()
    # Note: C code uses -1, which is equal to 255 for uint8_t
    objTilepos = high(typeof(objTilepos))
    # frame 185: dead
    # frame 106..110: fall + land
    if (frame == ord(frame185Dead) or (frame >= ord(frame106Fall) and frame < 111)):
      objY += 4
      discard objDxForward(5)
    elif (frame == ord(frame177Spiked)): # spiked
      discard objDxForward(-5)
    else:
      objY -= byte( (byte(Char.charid == ord(charid0Kid)) shl 2) + 11)
      discard objDxForward(5)

    if (Char.charid == ord(charid0Kid)):
      objChtab = ord(idChtab2Kid)
      objId = 218 # splash!
    else:
      objChtab = ord(idChtab5Guard)
      objId = 1 # splash!

    resetObjClip()
    addObjtable(5) # hurt splash
    loadObj()


# seg006:175D
proc checkKilledShadow() =
  # Special event: killed the shadow
  if (currentLevel == 12):
    if ((Char.charid or Opp.charid) == ord(charid1Shadow) and
      Char.alive < 0 and Opp.alive >= 0
    ):
      flashColor = ord(color15Brightwhite) # white
      flashTime = 5
      discard takeHp(100)


const
  # data:1712
  swordTbl: array[51, SwordTableType] = [
      SwordTableType(id: 255'u8, x: 0'i8, y: 0'i8),
      SwordTableType(id: 0'u8, x: 0'i8, y: -9'i8),
      SwordTableType(id: 5'u8, x: -9'i8, y: -29'i8),
      SwordTableType(id: 1'u8, x: 7'i8, y: -25'i8),
      SwordTableType(id: 2'u8, x: 17'i8, y: -26'i8),
      SwordTableType(id: 6'u8, x: 7'i8, y: -14'i8),
      SwordTableType(id: 7'u8, x: 0'i8, y: -5'i8),
      SwordTableType(id: 3'u8, x: 17'i8, y: -16'i8),
      SwordTableType(id: 4'u8, x: 16'i8, y: -19'i8),
      SwordTableType(id: 30'u8, x: 12'i8, y: -9'i8),
      SwordTableType(id: 8'u8, x: 13'i8, y: -34'i8),
      SwordTableType(id: 9'u8, x: 7'i8, y: -25'i8),
      SwordTableType(id: 10'u8, x: 10'i8, y: -16'i8),
      SwordTableType(id: 11'u8, x: 10'i8, y: -11'i8),
      SwordTableType(id: 12'u8, x: 22'i8, y: -21'i8),
      SwordTableType(id: 13'u8, x: 28'i8, y: -23'i8),
      SwordTableType(id: 14'u8, x: 13'i8, y: -35'i8),
      SwordTableType(id: 15'u8, x: 0'i8, y: -38'i8),
      SwordTableType(id: 16'u8, x: 0'i8, y: -29'i8),
      SwordTableType(id: 17'u8, x: 21'i8, y: -19'i8),
      SwordTableType(id: 18'u8, x: 14'i8, y: -23'i8),
      SwordTableType(id: 19'u8, x: 21'i8, y: -22'i8),
      SwordTableType(id: 19'u8, x: 22'i8, y: -23'i8),
      SwordTableType(id: 17'u8, x: 7'i8, y: -13'i8),
      SwordTableType(id: 17'u8, x: 15'i8, y: -18'i8),
      SwordTableType(id: 7'u8, x: 0'i8, y: -8'i8),
      SwordTableType(id: 1'u8, x: 7'i8, y: -27'i8),
      SwordTableType(id: 28'u8, x: 14'i8, y: -28'i8),
      SwordTableType(id: 8'u8, x: 7'i8, y: -27'i8),
      SwordTableType(id: 4'u8, x: 6'i8, y: -23'i8),
      SwordTableType(id: 4'u8, x: 9'i8, y: -21'i8),
      SwordTableType(id: 10'u8, x: 11'i8, y: -18'i8),
      SwordTableType(id: 13'u8, x: 24'i8, y: -23'i8),
      SwordTableType(id: 13'u8, x: 19'i8, y: -23'i8),
      SwordTableType(id: 13'u8, x: 21'i8, y: -23'i8),
      SwordTableType(id: 20'u8, x: 7'i8, y: -32'i8),
      SwordTableType(id: 21'u8, x: 14'i8, y: -32'i8),
      SwordTableType(id: 22'u8, x: 14'i8, y: -31'i8),
      SwordTableType(id: 23'u8, x: 14'i8, y: -29'i8),
      SwordTableType(id: 24'u8, x: 28'i8, y: -28'i8),
      SwordTableType(id: 25'u8, x: 28'i8, y: -28'i8),
      SwordTableType(id: 26'u8, x: 21'i8, y: -25'i8),
      SwordTableType(id: 27'u8, x: 14'i8, y: -22'i8),
      SwordTableType(id: 255'u8, x: 14'i8, y: -25'i8),
      SwordTableType(id: 255'u8, x: 21'i8, y: -25'i8),
      SwordTableType(id: 29'u8, x: 0'i8, y: -16'i8),
      SwordTableType(id: 8'u8, x: 8'i8, y: -37'i8),
      SwordTableType(id: 31'u8, x: 14'i8, y: -24'i8),
      SwordTableType(id: 32'u8, x: 14'i8, y: -24'i8),
      SwordTableType(id: 33'u8, x: 7'i8, y: -14'i8),
      SwordTableType(id: 8'u8, x: 8'i8, y: -37'i8)
    ]


# seg006:1798
proc addSwordToObjtable() =
  var
    frame: int16
    swordFrame: int16
  frame = int16(Char.frame)
  if ((frame >= ord(frame229FoundSword) and frame < 238) or # found sword + put sword away
    Char.sword != ord(sword0Sheathed) or
    (Char.charid == ord(charid2Guard) and Char.alive < 0)
  ):
    swordFrame = int16(curFrame.sword and 0x3F)
    if swordFrame != 0:
      objId = swordTbl[swordFrame].id
      if (objId != 0xFF):
        objX = calcScreenXCoord(objX)
        discard objDxForward(swordTbl[swordFrame].x)
        objY += cast[byte](swordTbl[swordFrame].y)
        objChtab = ord(idChtab0Sword)
        addObjtable(3) # sword


# seg006:1827
proc controlGuardInactive() =
  if (Char.frame == ord(frame166StandInactive) and controlDown < 0):
    if (controlForward < 0):
      drawSword()
    else:
      controlDown = 1
      seqtblOffsetChar(ord(seq80StandFlipped)) # stand flipped


# seg006:1852
proc charOppDist(): int32 =
  # >0 if Opp is in front of char
  # <0 if Opp is behind char
  var
    distance: int16
  if (Char.room != Opp.room):
    return 999

  distance = int16(Opp.x) - int16(Char.x)
  if (Char.direction < ord(dir0Right)):
    distance = -distance

  if (distance >= 0 and Char.direction != Opp.direction):
    distance += 13

  return distance


# seg006:189B
proc incCurrRow() =
  inc(Char.currRow)
