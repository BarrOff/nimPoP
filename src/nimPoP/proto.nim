proc perror*(message: cstring) {.importc: "perror", header: "stdio.h".}
proc memset*(dest: pointer, c: cint, n: csize_t) {.importc: "memset",
    header: "string.h".}
proc memcpy*(dest: pointer, src: pointer, n: csize_t) {.importc: "memcpy",
    header: "string.h".}
proc fread*(buffer: pointer, sz, n: csize_t, fp: File): csize_t {.
    importc: "fread", header: "stdio.h".}

# translation from proto.h

# SEG000.nim
proc popMain*()
proc initGameMain*()
proc startGame*()
proc processKey*(): int32
proc playFrame*()
proc drawGameFrame*()
proc animTileModif*()
proc loadSoundNames*()
proc loadSounds*(minSound: int32, maxSound: int32)
proc loadOptSounds*(first: int32, last: int32)
proc loadLevSpr*(level: int32)
proc loadLevel*()
proc resetLevelUnusedFields*(loadingCleanLevel: bool)
proc playKidFrame*(): int32
proc playGuardFrame*()
proc checkTheEnd*()
proc checkFallFlo*()
proc readJoystControl()
proc drawKidHp*(currHp: int16, maxHp: int16)
proc drawGuardHp*(currHp: int16, maxHp: int16)
proc addLife*()
proc setHealthLife*()
proc drawHp*()
proc doDeltaHp*()
proc playSound*(soundId: int32)
proc playNextSound*()
proc checkSwordVsSword*()
proc loadChtabFromFile*(chtabId: int32, resource: int32, filename: string,
    paletteBits: int32)
proc freeAllChtabsFrom*(first: int32)
proc loadMoreOptGraf*(filename: string)
proc doPaused*(): int32
proc readKeybControl*()
proc copyScreenRect*(sourceRectPtr: var RectType)
proc toggleUpside*()
proc featherFall*()
proc parseGrmode*(): int32
proc genPalaceWallColors*()
proc showTitle*()
proc transitionLtr*()
proc releaseTitleImages*()
proc drawFullImage*(id: FullImageId)
proc loadKidSprite*()
proc saveGame*()
proc loadGame*(): int16
proc clearScreenAndSounds*()
proc parseCmdlineSound*()
proc freeOptionalSounds*()
proc freeAllSounds*()
proc loadAllSounds*()
proc freeOptsndChtab*()
proc loadTitleImages*(bgcolor: int32)
proc showCopyprot*(where: int32)
proc showLoading*()
proc showQuotes*()
proc showSplash*()
when (USE_QUICKSAVE):
  proc checkQuickOp*()
  proc restoreRoomAfterQuickLoad*()

# SEG001.C
proc procCutsceneFrame(waitFrames: int32): int32
proc playBothSeq*()
proc drawProomDrects*()
proc playKidSeq*()
proc playOppSeq*()
proc drawPrincessRoomBg*()
proc seqtblOffsetShadChar*(seqIndex: int32)
proc seqtblOffsetKidChar*(seqIndex: int32)
proc initMouseCu8*()
proc initMouseGo*()
proc princessCrouching*()
proc princessStand*()
proc initPrincessX156*()
proc princessLying*()
proc initPrincessRight*()
proc initEndingPrincess*()
proc initMouse1*()
proc initPrincess*()
proc initVizier*()
proc initEndingKid*()
proc cutscene8*()
proc cutscene9*()
# proc endSequenceAnim*()
# proc timeExpired*()
proc cutscene12*()
proc cutscene4*()
proc cutscene2_6*()
proc pvScene*()
proc setHourglassState*(state: int32)
proc hourglassFrame*(): int32
proc princessRoomTorch*()
proc drawHourglass*()
proc resetCutscene*()
proc doFlash*(color: int16)
proc delayTicks*(ticks: uint32)
proc removeFlash*()
proc endSequence*()
proc expired*()
proc loadIntro*(whichImgs: int32, `func`: CutscenePtrType, freeSounds: int32)
proc drawStar*(whichStar: int32, markDirty: int32)
proc showHof*()
proc hofWrite*()
proc hofRead*()
proc showHofText*(rect: var RectType, xAlign: int32, yAlign: int32, text: string)
proc fadeIn1*(): int32
proc fadeOut1*(): int32

# SEG002.C
proc doInitShad*(source: openArray[byte], seqIndex: int32)
proc getGuardHp*()
proc checkShadow*()
proc enterGuard*()
proc checkGuardFallout*()
proc leaveGuard*()
proc followGuard*()
proc exitRoom*()
proc gotoOtherRoom*(direction: int16): int32
proc leaveRoom*(): int16
proc JaffarExit*()
proc level3SetChkp*()
proc swordDisappears*()
proc meetJaffar*()
proc playMirrMus*()
proc move0Nothing*()
proc move1Forward*()
proc move2Backward*()
proc move3Up*()
proc move4Down*()
proc moveUpBack*()
proc moveDownBack*()
proc moveDownForw*()
proc move6Shift*()
proc move7*()
proc autocontrolOpponent*()
proc autocontrolMouse*()
proc autocontrolShadow*()
proc autocontrolSkeleton*()
proc autocontrolJaffar*()
proc autocontrolKid*()
proc autocontrolGuard*()
proc autocontrolGuardInactive*()
proc autocontrolGuardActive*()
proc autocontrolGuardKidFar*()
proc guardFollowsKidDown*()
proc autocontrolGuardKidInSight*(distance: int16)
proc autocontrolGuardKidArmed*(distance: int16)
proc guardAdvance*()
proc guardBlock*()
proc guardStrike*()
proc hurtBySword*()
proc checkSwordHurt*()
proc checkSwordHurting*()
proc checkHurting*()
proc checkSkel*()
proc doAutoMoves*(movesPtr: openArray[AutoMoveType])
proc autocontrolShadowLevel4*()
proc autocontrolShadowLevel5*()
proc autocontrolShadowLevel6*()
proc autocontrolShadowLevel12*()

# SEG003.C
proc initGame*(level: int32)
proc playLevel*(levelNumber: int32)
proc doStartpos*()
proc setStartPos*()
proc findStartLevelDoor*()
proc drawLevelFirst*()
proc redrawScreen*(drawingDifferentRoom: int32)
proc playLevel2*(): int32
proc redrawAtChar*()
proc redrawAtChar2*()
proc checkKnock*()
proc timers*()
proc checkMirror*()
proc jumpThroughMirror*()
proc checkMirrorImage*()
proc bumpIntoOpponent*()
proc posGuards*()
proc checkCanGuardSeeKid*()
proc getTileAtKid*(xpos: int32): byte
proc doMouse*()
proc flashIfHurt*(): int32
proc removeFlashIfHurt*()

# SEG004.C
proc checkCollisions*()
proc moveCollToPrev*()
proc getRowCollisionData*(row: int16, rowCollRoomPtr: var openArray[sbyte],
    rowCollFlagsPtr: var openArray[byte])
proc getLeftWallXpos*(room: int32, column: int32, row: int32): int32
proc getRightWallXpos*(room: int32, column: int32, row: int32): int32
proc checkBumped*()
proc checkBumpedLookLeft*()
proc checkBumpedLookRight*()
proc isObstacleAtCol*(tileCol: int32): int32
proc isObstacle*(): int32
proc xposInDrawnRoom*(xpos: int32): int32
proc bumped*(deltaX: sbyte, direction: sbyte)
proc bumpedFall*()
proc bumpedFloor*(direction: sbyte)
proc bumpedSound*()
proc clearCollRooms*()
proc canBumpIntoGate*(): int32
proc getEdgeDistance*(): int32
proc checkChompedKid*()
proc chomped*()
proc checkGatePush*()
proc checkGuardBumped*()
proc checkChompedGuard*()
proc checkChompedHere*(): int32
proc distFromWallForward*(tiletype: byte): int32
proc distFromWallBehind*(tiletype: byte): int32

# SEG005.C
proc seqtblOffsetChar*(seqIndex: int16)
proc seqtblOffsetOpp*(seqIndex: int32)
proc doFall*()
proc land*()
proc spiked*()
proc control*()
proc controlCrouched*()
proc controlStanding*()
proc upPressed*()
proc downPressed*()
proc goUpLeveldoor*()
proc controlTurning*()
proc crouch*()
proc backPressed*()
proc forwardPressed*()
proc controlRunning*()
proc safeStep*()
proc checkGetItem*(): int32
proc getItem*()
proc controlStartrun*()
proc controlJumpup*()
proc standingJump*()
proc checkJumpUp*()
proc jumpUpOrGrab*()
proc grabUpNoFloorBehind*()
proc jumpUp*()
proc controlHanging*()
proc canClimbUp*()
proc hangFall*()
proc grabUpWithFloorBehind*()
proc runJump*()
proc backWithSword*()
proc forwardWithSword*()
proc drawSword*()
proc controlWithSword*()
proc swordfight*()
proc swordStrike*()
proc parry*()

# SEG006.C
proc getTile*(room: int32, col: int32, row: int32): int32
proc findRoomOfTile*(): int32
proc getTilepos*(tileCol: int32, tileRow: int32): int32
proc getTileposNominus*(tileCol: int32, tileRow: int32): int32
proc loadFramDetCol*()
proc determineCol*()
proc loadFrame*()
proc dxWeight*(): int16
proc charDxForward*(deltaX: int32): int32
proc objDxForward*(deltaX: int32): int32
proc playSeq*()
proc getTileDivModM7*(xpos: int32): int32
proc getTileDivMod*(xpos: int32): int32
# proc sub70B6*(ypos: int32): int32
proc yToRowMod4*(ypos: int32): int32
proc loadkid*()
proc savekid*()
proc loadshad*()
proc saveshad*()
proc loadkidAndOpp*()
proc savekidAndOpp*()
proc loadshadAndOpp*()
proc saveshadAndOpp*()
proc resetObjClip*()
proc xToXhAndXl*(xpos: int32, xhAddr, xlAddr: var sbyte)
proc fallAccel*()
proc fallSpeed*()
proc checkAction*()
proc tileIsFloor*(tiletype: int32): int32
proc checkSpiked*()
proc takeHp*(count: int32): int32
proc getTileAtChar*(): int32
proc setCharCollision*()
proc checkOnFloor*()
proc startFall*()
proc checkGrab*()
proc canGrabFrontAbove*(): int32
proc inWall*()
proc getTileInfrontofChar*(): int32
proc getTileInfrontof2Char*(): int32
proc getTileBehindChar*(): int32
proc distanceToEdgeWeight*(): int32
proc distanceToEdge*(xpos: int32): int32
proc fellOut*()
proc playKid*()
proc controlKid*()
proc doDemo*()
proc playGuard*()
proc userControl*()
proc flipControlX*()
proc releaseArrows*(): int32
proc saveCtrl1*()
proc restCtrl1*()
proc clearSavedCtrl*()
proc readUserControl*()
proc canGrab*(): int32
proc wallType*(tiletype: byte): int32
proc getTileAboveChar*(): int32
proc getTileBehindAboveChar*(): int32
proc getTileFrontAboveChar*(): int32
proc backDeltaX*(deltaX: int32): int32
proc doPickup*(objType: int32)
proc checkPress*()
proc checkSpikeBelow*()
proc clipChar*()
proc stuckLower*()
proc setObjtileAtChar*()
proc procGetObject*()
proc isDead*(): int32
proc playDeathMusic*()
proc onGuardKilled*()
proc clearChar*()
proc saveObj*()
proc loadObj*()
proc drawHurtSplash*()
proc checkKilledShadow*()
proc addSwordToObjtable*()
proc controlGuardInactive*()
proc charOppDist*(): int32
proc incCurrRow*()

# SEG007.C
proc processTrobs*()
proc animateTile*()
proc isTrobInDrawnRoom*(): int16
proc setRedrawAnimRight*()
proc setRedrawAnimCurr*()
proc redrawAtTrob*()
proc redraw21h*()
proc redraw11h*()
proc redraw20h*()
proc drawTrob*()
proc redrawTileHeight*()
proc getTrobPosInDrawnRoom*(): int16
proc getTrobRightPosInDrawnRoom*(): int16
proc getTrobRightAbovePosInDrawnRoom*(): int16
proc animateTorch*()
proc animatePotion*()
proc animateSword*()
proc animateChomper*()
proc animateSpike*()
proc animateDoor*()
proc gateStop*()
proc animateLeveldoor*()
proc bubbleNextFrame*(curr: int16): int16
proc getTorchFrame*(curr: int16): int16
proc setRedrawAnim*(tilepos: int16, frames: byte)
proc setRedraw2*(tilepos: int16, frames: byte)
proc setRedrawFloorOverlay*(tilepos: int16, frames: byte)
proc setRedrawFull*(tilepos: int16, frames: byte)
proc setRedrawFore*(tilepos: int16, frames: byte)
proc setWipe*(tilepos: int16, frames: byte)
proc startAnimTorch*(room: int16, tilepos: int16)
proc startAnimPotion*(room: int16, tilepos: int16)
proc startAnimSword*(room: int16, tilepos: int16)
proc startAnimChomper*(room: int16, tilepos: int16, modifier: byte)
proc startAnimSpike*(room: int16, tilepos: int16)
proc triggerGate*(room: int16, tilepos: int16, buttonType: int16): int16
proc trigger1*(targetType: int16, room: int16, tilepos: int16,
    buttonType: int16): int16
proc doTriggerList*(index: int16, buttonType: int16)
proc addTrob*(room: byte, tilepos: byte, `type`: sbyte)
proc findTrob*(): int16
proc clearTileWipes*()
proc getDoorlinkTimer*(index: int16): int16
proc setDoorlinkTimer*(index: int16, value: byte): int16
proc getDoorlinkTile*(index: int16): int16
proc getDoorlinkNext*(index: int16): int16
proc getDoorlinkRoom*(index: int16): int16
proc triggerButton*(playsound: int32, buttonType: int32, modifier: int32)
proc diedOnButton*()
proc animateButton*()
proc startLevelDoor*(room: int16, tilepos: int16)
proc animateEmpty*()
proc animateLoose*()
proc looseShake*(arg0: int32)
proc removeLoose*(room: int32, tilepos: int32): int32
proc makeLooseFall*(modifier: byte)
proc startChompers*()
proc nextChomperTiming*(timing: byte): int32
proc looseMakeShake*()
proc doKnock*(room: int32, tileRow: int32)
proc addMob*()
proc getCurrTile*(tilepos: int16): int16
proc doMobs*()
proc moveMob*()
proc moveLoose*()
proc looseLand*()
proc looseFall*()
proc redrawAtCurMob*()
proc mobDownARow*()
proc drawMobs*()
proc drawMob*()
proc addMobToObjtable*(ypos: int32)
proc sub9A8E*()
proc isSpikeHarmful*(): int32
proc checkLooseFallOnKid*()
proc fellOnYourHead*()
proc playDoorSoundIfVisible*(soundId: int32)

# SEG008.C
proc redrawRoom*()
proc loadRoomLinks*()
proc drawRoom*()
proc drawTile*()
proc drawTileAboveroom*()
proc redrawNeeded*(tilepos: int16)
proc redrawNeededAbove*(column: int32)
proc getTileToDraw*(room: int32, column: int32, row: int32, ptrTile: ptr byte,
    ptrModifier: ptr byte, tileRoom0: byte): int32
proc loadCurrAndLeftTile*()
proc loadLeftroom*()
proc loadRowbelow*()
proc drawTileFloorright*()
proc canSeeBottomleft*(): int32
proc drawTileTopright*()
proc drawTileAnimTopright*()
proc drawTileRight*()
proc getSpikeFrame*(modifier: byte): int32
proc drawTileAnimRight*()
proc drawTileBottom*(arg0: word)
proc drawLoose*(arg0: int32)
proc drawTileBase*()
proc drawTileAnim*()
proc drawTileFore*()
proc getLooseFrame*(modifier: byte): int32
proc addBacktable*(chtabId: int16, id: int32, xh: sbyte, xl: sbyte,
    ybottom: int32, blit: int32, peel: byte): int32
proc addForetable*(chtabId: int16, id: int32, xh: sbyte, xl: sbyte,
    ybottom: int32, blit: int32, peel: byte): int32
proc addMidtable*(chtabId: int16, id: int32, xh: sbyte, xl: sbyte,
    ybottom: int32, blit: int32, peel: byte): int32
proc addPeel*(left: int32, right: int32, top: int32, height: int32)
proc addWipetable*(layer: sbyte, left: int16, bottom: int16, height: sbyte,
    width: int16, color: sbyte)
proc drawTable*(whichTable: int32)
proc drawWipes*(which: int32)
proc drawBackFore*(whichTable: int32, index: int32)
proc drawMid*(index: int32)
proc drawImage*(image: ImageType, mask: ImageType, xpos: int32, ypos: int32, blit: int32)
proc drawWipe*(index: int32)
proc calcGatePos*()
proc drawGateBack*()
proc drawGateFore*()
proc alterModsAllrm*()
proc loadAlterMod*(tilepos: int32)
proc drawMoving*()
proc redrawNeededTiles*()
proc drawTileWipe*(height: byte)
proc drawTables*()
proc restorePeels*()
proc addDrect*(source: var RectType)
proc drawLeveldoor*()
proc getRoomAddress*(room: int32)
proc drawFloorOverlay*()
proc drawOtherOverlay*()
proc drawTile2*()
proc drawObjtableItemsAtTile*(tilepos: byte)
proc sortCurrObjs*()
proc compareCurrObjs*(index1: int32, index2: int32): int32
proc drawObjtableItem*(index: int32)
proc loadObjFromObjtable*(index: int32): int32
proc drawPeople*()
proc drawKid*()
proc drawGuard*()
proc addKidToObjtable*()
proc addGuardToObjtable*()
proc addObjtable*(objType: byte)
proc markObjTileRedraw*(index: int32)
proc loadFrameToObj*()
proc showTime*()
proc showLevel*()
proc calcScreenXCoord*(logicalX: int16): int16
proc freePeels*()
proc displayTextBottom*(text: string)
proc eraseBottomText*(arg0: int32)
proc wallPattern*(whichPart: int32, whichTable: int32)
proc drawLeftMark*(arg3: word, arg2: word, arg1: word)
proc drawRightMark*(arg2: word, arg1: word)
proc getImage*(chtabId: int16, id: int32): ImageType

# SEG009.C
# proc sdlperror*( header: charptr )
# proc fileExists*( filename: charptr ): bool
proc sdlperror*(header: string)
# proc fileExists*(filename: string): bool
#define locateFile(filename) locateFile(filename, alloca(POPMAXPATH),
#POPMAXPATH)
# proc locateFile*(filename: string, pathBuffer: string, bufferSize: int32): string
proc locateFile*(filename: string): string
#ifdef _WIN32
# FILEptr  fopenUTF8( charptr  filename, charptr:     mode);
#define fopen fopenUTF8
# chdirUTF8( charptr  path): int32
#define chdir chdirUTF8
# accessUTF8( charptr  filenameUTF8, mode: int32): int32
#ifdef access
#undef access
#endif
#define access accessUTF8
#endif #_WIN32
# proc createDirectoryListingAndFindFirstFile*( charptr  directory, charptr:     extension): directoryListingTypeptr
# proc getCurrentFilenameFromDirectoryListing*(data: directoryListingType): ptr char
# proc createDirectoryListingAndFindFirstFile*(directory: string,
#     extension: string): DirectoryListingType
# proc getCurrentFilenameFromDirectoryListing*(data: DirectoryListingType): string
# proc findNextFile*(data: DirectoryListingType): bool
# proc closeDirectoryListing*(data: DirectoryListingType)
proc readKey*(): int32
proc clearKbdBuf*()
proc prandom*(`max`: word): word
proc roundXposToByte*(xpos: int32, roundDirection: int32): int32
proc showDialog*(text: string)
proc quitPoP*(exitCode: int32)
proc restoreStuff*()
proc keyTestQuit*(): int32
# const char* __pascal far check_param(const char *param);
# proc checkParam*(param: string): string
proc popWait*(timerIndex: int32, time: int32): int32
proc openDat*(filename: string, drive: int32): DatType
proc setLoadedPalette*(palettePtr: var DatPalType)
proc loadSpritesFromFile*(resource: int32, paletteBits: int32,
    quitOnError: int32): ChtabType
proc freeChtab*(chtabPtr: ChtabType)
proc decodeImage*(imageData: var ImageDataType,
    palette: var DatPalType): ImageType
proc loadImage*(resourceId: int32, palette: var DatPalType): ImageType
proc drawImageTransp*(image: ImageType, mask: ImageType, xpos: int32, ypos: int32)
proc setJoyMode*(): int32
proc makeOffscreenBuffer*(rect: var RectType): SurfaceType
# proc freeSurface*(surface: SurfaceType)
proc freePeel*(peelPtr: ptr PeelType)
proc setHcPal*()
proc flipNotEga*(memory: ptr byte, height: int32, stride: int32)
proc flipScreen*(surface: SurfaceType)
proc fadeIn2*(sourceSurface: SurfaceType, whichRows: int32)
proc fadeOut2*(rows: int32)
proc drawImageTranspVga*(image: ImageType, xpos: int32, ypos: int32)
proc getLineWidth*(text: string, length: int32): int32
proc drawTextCharacter*(character: byte): int32
proc drawRect*(rect: var RectType, color: int32)
# proc drawRectWithAlpha*(rect: RectType, color: byte, alpha: byte)
proc drawRectContours*(rect: var RectType, color: byte)
proc rectSthg*(surface: SurfaceType, rect: ptr RectType): SurfaceType
proc shrink2Rect*(targetRect, sourceRect: var RectType, deltaX: int32,
    deltaY: int32): RectType
proc setCurrPos*(xpos: int32, ypos: int32)
proc restorePeel*(peelPtr: ptr PeelType)
proc readPeelFromScreen*(rect: var RectType): ptr PeelType
proc showText*(rectPtr: ptr RectType, xAlign: int32, yAlign: int32, text: string)
proc intersectRect*(output, input1, input2: var RectType): int32
proc unionRect*(output, input1, input2: var RectType): RectType
proc stopSounds*()
proc initDigi*()
proc playSoundFromBuffer*(buffer: ptr SoundBufferType)
proc turnMusicOnOff*(newState: byte)
proc turnSoundOnOff*(newState: byte)
proc checkSoundPlaying*(): int32
proc applyAspectRatio*()
proc windowResized*()
proc setGrMode*(grmode: byte)
proc getFinalSurface*(): Surface
proc updateScreen*()
proc setPalArr*(start: int32, count: int32, `array`: pointer, vsync: int32)
proc setPal*(index: int32, red: int32, green: int32, blue: int32, vsync: int32)
proc addPaletteBits*(nColors: byte): int32
# proc processPalette*(target: pointer, source: ptr DatPalType)
proc findFirstPalRow*(whichRowsMask: int32): int32
proc getTextColor*(cgaColor: int32, lowHalf: int32, highHalfMask: int32): int32
# proc closeDat*(pntr: ptr DatType)  ptr not necessary, because ref obj
proc closeDat*(pntr: DatType)
proc loadFromOpendatsAlloc*(resource: int32, extension: string,
    outResult: ptr DataLocation, outSize: ptr int32): pointer
proc loadFromOpendatsToArea*(resource: int32, area: pointer, length: int32,
    extension: string): int32
proc rectToSdlrect*(rect: var RectType, sdlrect: var Rect)
proc method1BlitRect*(targetSurface, sourceSurface: SurfaceType,
    targetRect, sourceRect: var RectType, blit: int32)
proc method3BlitMono*(image: ImageType, xpos: int32, ypos: int32,
    blitter: int32, color: byte): ImageType
proc method5Rect*(rect: var RectType, blit: int32, color: byte): ptr RectType
proc drawRectWithAlpha*(rect: var RectType, color: byte, alpha: byte)
proc method6BlitImgToScr*(image: ImageType, xpos, ypos,
    blit: int32): ImageType
proc resetTimer*(timerIndex: int32)
proc getTicksPerSec*(timerIndex: int32): float64
proc setTimerLength*(timerIndex: int32, length: int32)
proc startTimer*(timerIndex: int32, length: int32)
proc doWait*(timerIndex: int32): int32
proc initTimer*(frequency: int32)
proc setClipRect*(rect: var RectType)
proc resetClipRect*()
proc setBgAttr*(vgaPalIndex: int32, hcPalIndex: int32)
proc offset4RectAdd*(dest: var RectType, source: var RectType, dLeft, dTop,
    dRight, dBottom: int32): RectType
proc inputStr*(rect: var RectType, buffer: var string, maxLength: int32,
    initial: string, hasInitial, arg4, color, bgcolor: int32): int32
proc offset2Rect*(dest: var RectType, source: var RectType, deltaX: int32,
    deltaY: int32): RectType
proc showTextWithColor*(rectPtr: var RectType, xAlign: int32, yAlign: int32,
    text: string, color: int32)
proc doSimpleWait*(timerIndex: int32)
proc processEvents*()
proc idle*()
proc initCopyprotDialog*()
proc makeDialogInfo*(settings: ptr DialogSettingsType, dialogRect,
    textRect: var RectType, dialogPeel: var PeelType): ptr DialogType
proc makeDialogInfo*(settings: ptr DialogSettingsType, dialogRect,
    textRect: var RectType): ptr DialogType
proc calcDialogPeelRect*(dialog: ptr DialogType)
proc readDialogPeel*(dialog: ptr DialogType)
proc drawDialogFrame*(dialog: ptr DialogType)
proc addDialogRect*(dialog: ptr DialogType)
proc dialogMethod2Frame*(dialog: ptr DialogType)
when(USE_FADE):
  # proc fadeIn2*(sourceSurface: SurfaceType, whichRows: int32)
  proc makePalBufferFadein*(sourceSurface: SurfaceType, whichRows: int32,
      waitTime: int32): PaletteFadeType
  proc palRestoreFreeFadein*(paletteBuffer: PaletteFadeType)
  proc fadeInFrame*(paletteBuffer: PaletteFadeType): int32
  # proc fadeOut2*(rows: int32)
  proc makePalBufferFadeout*(whichRows: int32, waitTime: int32): PaletteFadeType
  proc palRestoreFreeFadeout*(paletteBuffer: PaletteFadeType)
  proc fadeOutFrame*(paletteBuffer: PaletteFadeType): int32
  proc readPalette256*(target: var openArray[RgbType])
  proc setPal256*(source: var openArray[RgbType])
proc setChtabPalette*(chtab: ChtabType, colors: ptr byte, nColors: int32)
proc hasTimerStopped*(timerIndex: int32): int32
proc loadSound*(index: int32): ptr SoundBufferType
proc freeSound*(buffer: ptr SoundBufferType)

# SEQTABLE.C
proc applySeqtblPatches*()
when (CHECK_SEQTABLE_MATCHES_ORIGINAL):
  proc checkSeqtableMatchesOriginal*()
#endif

# OPTIONS.C
proc turnFixesAndEnhancementsOnOff*(newState: byte)
proc turnCustomOptionsOnOff*(newState: byte)
proc setOptionsToDefault*()
proc loadGlobalOptions*()
proc checkModParam*()
proc loadModOptions*()
proc processRwWrite*(rw: ptr RWops, data: pointer, dataSize: csize_t): csize_t
proc processRwRead*(rw: ptr RWops, data: pointer, dataSize: csize_t): csize_t
# proc loadDosExeModifications( charptr  folderName)
proc loadDosExeModifications*(folderName: string)

# REPLAY.C
when(USE_REPLAY):
  proc startWithReplayFile*(filename: string)
  proc initRecordReplay*()
  proc replayRestoreLevel*()
  proc restoreSavestateFromBuffer*(): int32
  proc startRecording*()
  proc addReplayMove*()
  proc stopRecording*()
  proc startReplay*()
  proc endReplay*()
  proc doReplayMove*()
  proc saveRecordedReplayDialog*(): int32
  proc saveRecordedReplay*(fullFilename: String): int32
  proc replayCycle*()
  proc loadReplay*(): int32
  proc keyPressWhileRecording*(keyPtr: ptr int32)
  proc keyPressWhileReplaying*(keyPtr: ptr int32)

# lighting.c
when(USE_LIGHTING):
  proc initLighting*()
  proc redrawLighting*()
  proc updateLighting*(sourceRectPtr: var RectType)

# screenshot.c
when(USE_SCREENSHOT):
  proc saveScreenshot*()
  proc autoScreenshot*()
  proc wantAutoScreenshot*(): bool
  proc initScreenshot*()
  proc saveLevelScreenshot*(wantExtras: bool)

# menu.c
when(USE_MENU):
  proc initMenu*()
  proc menuScroll(y: int32)
  proc drawMenu*()
  proc clearMenuControls*()
  proc processAdditionalMenuInput*()
  proc keyTestPausedMenu*(key: int32): int32
  proc loadIngameSettings*()
  proc menuWasClosed*()

# midi.c
proc stopMidi*()
proc initMidi*()
proc midiCallback*(userdata: pointer, stream: ptr uint8, length: int32)
proc playMidiSound*(buffer: ptr SoundBufferType)


proc snprintfCheck*(dst: var string, size: uint, src: string) =
  if len(src) >= int32(size):
    write(stderr, ": buffer truncation detected!\n")
    write(stderr, "got size of: " & $(size) & " but src is only: " & $(
        src.len) & " long.\n")
    write(stderr, "src is: " & src & "\n")
    quitPoP(2)
  else:
    dst = src

