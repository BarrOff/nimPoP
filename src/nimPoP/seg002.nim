# seg002:0000
proc doInitShad(source: openArray[byte], seqIndex: int32) =
  # TODO: memcpy_near(&Char, source, 7)
  seqtblOffsetChar(int16(seqIndex))
  Char.charid = ord(charid1Shadow)
  demoTime = 0
  guardSkill = 3
  guardhpDelta = 4
  guardhpCurr = 4
  guardhpMax = 4
  saveshad()

proc getGuardHp() =
  guardhpDelta = int16(custom.extrastrength[guardSkill]) + int16(
      custom.tblGuardHp[currentLevel])
  guardhpCurr = word(guardhpDelta)
  guardhpMax = word(guardhpDelta)

# seg002:0064
proc checkShadow() =
  offguard = 0
  if currentLevel == 12:
    # Special even: level 12 shadow
    if (unitedWithShadow == 0) and drawnRoom == 15:
      Char.room = byte(drawnRoom)
      if getTile(15, 1, 0) == ord(tiles22Sword):
        return
      shadowInitialized = 0
      doInitShad(custom.initShad12, 7)
      return
  elif currentLevel == 6:
    # Special even: level6 shadow
    Char.room = byte(drawnRoom)
    if Char.room == 1:
      if leveldoorOpen != 0x4D:
        # presentation (level 6 shadow)
        playSound(ord(sound25Presentation))
        leveldoorOpen = 0x4D
      doInitShad(custom.initShad6, 2)
      return
  elif currentLevel == 5:
    # Special event: level 5 shadow
    Char.room = byte(drawnRoom)
    if Char.room == 24:
      if getTile(24, 3, 0) != ord(tiles10Potion):
        return
      doInitShad(custom.initShad5, 2)
      return
  enterGuard()

# seg002:0112
proc enterGuard() =
  var
    roomMinus1: word
    guardTile: word
    frame: word
    seqHi: byte
  # arrays are indexed 0..23 instead of 1..24
  roomMinus1 = drawnRoom - 1
  frame = Char.frame
  guardTile = level.guardsTile[roomMinus1]

  when(FIX_OFFSCREEN_GUARDS_DISAPPEARING):
    if guardTile >= 30'u16:
      return
  else:
    if guardTile >= 30'u16:
      if (fixes.fixOffscreenGuardsDisappearing == 0):
        return
      # try to see if there are offscreen guards in the left and right rooms
      # that might be visible from this room
      var
        leftGuardTile: int16 = 31
        rightGuardTile: int16 = 31
      if roomL > 0'u16:
        leftGuardTile = level.guardsTile[roomL - 1]
      if roomR > 0'u16:
        rightGuardTile = level.guardsTile[roomR - 1]

      var
        otherGuardX: int32
        otherGuardDir: sbyte
        deltaX: int32
        otherRoomMinus1: int32
      if rightGuardTile >= 0'u16 and rightGuardTile <= 30'u16:
        otherRoomMinus1 = int32(roomR) - 1
        otherGuardX = int32(level.guardsX[otherRoomMinus1])
        otherGuardDir = cast[sbyte](level.guardsDir[otherRoomMinus1])
        # left edge of the guard matters
        if otherGuardDir == ord(dir0Right):
          # only retrieve a guard if they will be visible
          otherGuardX -= 9
        if otherGuardDir == ord(dirFFLeft):
          # getting these right was mostly trial and error
          otherGuardX += 1
        # only retrieve offscreen guards
        if not(otherGuardX < 58 + 4):
          # check the left offscreen guard
          if (left_guard_tile >= 0'u16 and left_guard_tile < 30'u16):
            otherRoomMinus1 = int32(roomL) - 1
            otherGuardX = int32(level.guardsX[otherRoomMinus1])
            otherGuardDir = sbyte(level.guardsDir[otherRoomMinus1])
            # right edge of the guard matters
            if otherGuardDir == ord(dir0Right):
              otherGuardX -= 9
            if otherGuardDir == ord(dirFFLeft):
              otherGuardX += 1
            # only retrieve offscreen guards
            if not(otherGuardX > 190 - 4):
              return
            # guard leaves to the right
            deltaX = -140
            guardTile = leftGuardTile
          else:
            return
        # guard leaves to the left
        deltaX = 140
        guardTile = rightGuardTile
      elif leftGuardTile >= 0'u16 and leftGuardTile < 30'u16:
        otherRoomMinus1 = int32(roomL) - 1
        otherGuardX = int32(level.guardsX[otherRoomMinus1])
        otherGuardDir = sbyte(level.guardsDir[otherRoomMinus1])
        # right edge of the guard matters
        if otherGuardDir == ord(dir0Right):
          otherGuardX -= 9
        if otherGuardDir == ord(dirFFLeft):
          otherGuardX += 1
        # only retrieve offscreen guards
        if not(otherGuardX > 190 - 4):
          return
        # guard leaves to the right
        deltaX = -140
        guardTile = leftGuardTile
      else:
        return

      # retrieve guard from adjacent room
      level.guardsX[roomMinus1] = level.guardsX[otherRoomMinus1] + byte(deltaX)
      level.guardsColor[roomMinus1] = level.guardsColor[otherRoomMinus1]
      level.guardsDir[roomMinus1] = level.guardsDir[otherRoomMinus1]
      level.guardsSeqHi[roomMinus1] = level.guardsSeqHi[otherRoomMinus1]
      level.guardsSeqLo[roomMinus1] = level.guardsSeqLo[otherRoomMinus1]
      level.guardsSkill[roomMinus1] = level.guardsSkill[otherRoomMinus1]

      level.guardsTile[otherRoomMinus1] = 0xFF
      level.guardsSeqHi[otherRoomMinus1] = 0
  Char.room = byte(drawnRoom)
  Char.currRow = cast[sbyte](guardTile div 10)
  Char.y = byte(yLand[Char.currRow + 1])
  Char.x = level.guardsX[roomMinus1]
  Char.direction = cast[sbyte](level.guardsDir[roomMinus1])
  # only regular guards have different colors (and only on vga)
  if graphicsMode == ord(gmMcgaVga) and custom.tblGuardType[currentLevel] == 0:
    currGuardColor = level.guardsColor[roomMinus1]
  else:
    currGuardColor = 0

  when(REMEMBER_GUARD_HP):
    var
      rememberedHp: int32 = int32( (level.guardsColor[roomMinus1] and 0xF0) shr 4)
  # added; only least significant 4 bits are used for guard color
  currGuardColor = currGuardColor and 0x0F

  # level 3 has skeletons with infinite lives
  if custom.tblGuardType[currentLevel] == 2:
    Char.charid = ord(charid4Skeleton)
  else:
    Char.charid = ord(charid2Guard)
  seqHi = level.guardsSeqHi[roomMinus1]
  if seqHi == 0:
    if Char.charid == ord(charid4Skeleton):
      Char.sword = ord(sword2Drawn)
      # stand active (when entering room) (skeleton)
      seqtblOffsetChar(ord(seq63GuardStandActive))
    else:
      Char.sword = ord(sword0Sheathed)
      # stand inactive (when entering room)
      seqtblOffsetChar(ord(seq77GuardStandInactive))
  else:
    Char.currSeq = level.guardsSeqLo[roomMinus1] + (seqHi shl 8)
  playSeq()
  guardSkill = level.guardsSkill[roomMinus1]
  if int32(guardSkill) >= NUM_GUARD_SKILLS:
    guardSkill = 3
  frame = Char.frame
  if frame == ord(frame185Dead) or frame == ord(frame177Spiked) or frame ==
    ord(frame178Chomped):
    Char.alive = 1
    drawGuardHp(0, int16(guardhpCurr))
    guardhpCurr = 0
  else:
    Char.alive = -1
    justblocked = 0
    guardRefrac = 0
    isGuardNotice = 0
    getGuardHp()
    when (REMEMBER_GUARD_HP):
      guardhpDelta = int16(rememberedHp)
      guardhpCurr = word(guardhpDelta)
  Char.fallY = 0
  Char.fallX = 0
  Char.action = ord(actions1RunJump)
  saveshad()

# seg002:0269
proc checkGuardFallout() =
  if (Guard.direction == ord(dir56None) or Guard.y < 211):
    return

  if (Guard.charid == ord(charid1Shadow)):
    if (Guard.action != ord(actions4InFreefall)):
      return

    loadshad()
    clearChar()
    saveshad()
  elif (Guard.charid == ord(charid4Skeleton) and
    (Guard.room == level.roomlinks[Guard.room - 1].down) ==
    (custom.skeletonReappearRoom != 0)):
    # if skeleton falls down into room 3
    Guard.x = custom.skeletonReappearX
    Guard.currRow = cast[sbyte](custom.skeletonReappearRow)
    Guard.direction = cast[sbyte](custom.skeletonReappearDir)
    Guard.alive = -1
    leaveGuard()
  else:
    onGuardKilled()
    level.guardsTile[drawnRoom - 1] = high(typeof(level.guardsTile[drawnRoom - 1]))
    Guard.direction = ord(dir56None)
    drawGuardHp(0, int16(guardhpCurr))
    guardhpCurr = 0

# seg002:02F5
proc leaveGuard() =
  var
    roomMinus1: word
  if (Guard.direction == ord(dir56None) or Guard.charid == ord(charid1Shadow
    ) or Guard.charid == ord(charid24Mouse)):
    return

  # arrays are indexed 0..23 instead of 1..24
  roomMinus1 = word(Guard.room - 1)
  level.guardsTile[roomMinus1] = byte(getTilepos(0, int32(Guard.currRow)))

  level.guardsColor[roomMinus1] = byte(currGuardColor and 0x0F) # restriction to 4 bits added
  when (REMEMBER_GUARD_HP):
    if ((fixes.enableRememberGuardHp != 0) and guardhpCurr < 16): # can remember 1..15 hp
      level.guardsColor[roomMinus1] = byte(level.guardsColor[roomMinus1] or (
          guardhpCurr shl 4))

  level.guardsX[roomMinus1] = Guard.x
  level.guardsDir[roomMinus1] = cast[byte](Guard.direction)
  level.guardsSkill[roomMinus1] = byte(guardSkill)
  if (Guard.alive < 0):
    level.guardsSeqHi[roomMinus1] = 0
  else:
    level.guardsSeqLo[roomMinus1] = byte(Guard.currSeq)
    level.guardsSeqHi[roomMinus1] = byte(Guard.currSeq shr 8)

  Guard.direction = ord(dir56None)
  drawGuardHp(0, int16(guardhpCurr))
  guardhpCurr = 0


# seg002:039E
proc followGuard() =
  level.guardsTile[Kid.room - 1] = 0xFF
  level.guardsTile[Guard.room - 1] = 0xFF
  loadshad()
  discard gotoOtherRoom(roomleaveResult)
  saveshad()


# seg002:03C7
proc exitRoom() =
  var
    leave: int16
    kidRoomM1: int16
  leave = 0
  if (exitRoomTimer != 0):
    dec(exitRoomTimer)
    when (FIX_HANG_ON_TELEPORT):
      if (not((fixes.fixHangOnTeleport != 0) and Char.y >= 211 and
          Char.currRow >= 2)):
        return
    else:
      return

  loadkid()
  loadFrameToObj()
  setCharCollision()
  roomleaveResult = leaveRoom()
  if (roomleaveResult < 0):
    return

  savekid()
  nextRoom = Char.room
  if (Guard.direction == ord(dir56None)):
    return
  if (Guard.alive < 0 and Guard.sword == ord(sword2Drawn)):
    kidRoomM1 = int16(Kid.room - 1)
    # kidRoomM1 might be 65535 (-1) when the prince fell out of the level (to room 0) while a guard was active.
    # In this case, the indexing in the following condition crashes on Linux.
    if (kidRoomM1 >= 0'i16 and kidRoomM1 <= 23'i16) and
      (level.guardsTile[kidRoomM1] >= 30'i16 or level.guardsSeqHi[kidRoomM1] != 0):
      if (roomleaveResult == 0):
        when(FIX_GUARD_FOLLOWING_THROUGH_CLOSED_GATES):
          # left
          if (Guard.x >= 91):
            leave = 1
          elif ((fixes.fixGuardFollowingThroughClosedGates != 0) and
              canGuardSeeKid != 2 and Kid.sword != ord(sword2Drawn)):
            leave = 1
        else:
          # left
          if (Guard.x >= 91):
            leave = 1

      elif (roomleaveResult == 1):
        when(FIX_GUARD_FOLLOWING_THROUGH_CLOSED_GATES):
          # right
          if (Guard.x < 165):
            leave = 1
          elif ((fixes.fixGuardFollowingThroughClosedGates != 0) and
              canGuardSeeKid != 2 and Kid.sword != ord(sword2Drawn)):
            leave = 1
        else:
          # right
          if (Guard.x < 165):
            leave = 1
      elif (roomleaveResult == 2):
        # up
        if (Guard.currRow >= 0):
          leave = 1
      else:
        # down
        if (Guard.currRow < 3):
          leave = 1

    else:
      leave = 1

  else:
    leave = 1

  if leave != 0:
    leaveGuard()
  else:
    followGuard()

# seg002:0486
proc gotoOtherRoom(direction: int16): int32 =
  var
    oppositeDir: int16
    otherRoom = (cast[ptr UncheckedArray[byte]](addr(level.roomlinks[Char.room -
      1])))[direction]

  when (FIX_ENTERING_GLITCHED_ROOMS):
    if (Char.room == 0'u8):
      otherRoom = 0'u8

  Char.room = otherRoom

  if (direction == 0):
    # left
    Char.x += 140
    oppositeDir = 1
  elif (direction == 1):
    # right
    Char.x -= 140
    oppositeDir = 0
  elif (direction == 2):
    # up
    Char.y += 189
    Char.currRow = sbyte(yToRowMod4(int(Char.y)))
    oppositeDir = 3
  else:
    # down
    Char.y -= 189
    Char.currRow = sbyte(yToRowMod4(int(Char.y)))
    oppositeDir = 2

  return oppositeDir


# seg002:0504
proc leaveRoom(): int16 =
  var
    frame: int16
    action: word
    chary: int16
    leaveDir: int16
  chary = int16(Char.y)
  action = Char.action
  frame = int16(Char.frame)
  if (action != ord(actions5Bumped) and action != ord(actions4InFreefall) and
      action != ord(actions3InMidair) and cast[sbyte](chary) < 10 and cast[
          sbyte](chary) > -16):
    leaveDir = 2 # up
  elif (chary >= 211):
    leaveDir = 3 # down
  elif (
      # frames 135..149: climb up
    (frame >= ord(frame135Climbing1) and frame < 150) or
    # frames 110..119: standing up from crouch
    (frame >= ord(frame110StandUpFromCrouch1) and frame < 120) or
    # frames 150..162: with sword
    (frame >= ord(frame150Parry) and frame < 163 and


    # By repeatedly pressing 'back' in a swordfight, you can retreat out of a room without the room changing. (Trick 35)

    # The game waits for a 'legal frame' (e.g. frame170StandWithSword) until leaving is possible
    # However, this frame is never reached if you press 'back' in the correct pattern!

    # Solution: also allow the room to be changed on frame157WalkWithSword
    # Note that this means that the delay for leaving rooms in a swordfight becomes noticably shorter.

    (when(FIX_RETREAT_WITHOUT_LEAVING_ROOM): frame != ord(
        frame_157_walk_with_sword) or (fixes.fixRetreatWithoutLeavingRoom == 0) else: true)
    ) or
    # frames 166..168: with sword
    (frame >= ord(frame166StandInactive) and frame < 169) or
    action == ord(actions7Turn)): # turn
    return -1
  elif (Char.direction != ord(dir0Right)):
    # looking left
    if (charXLeft <= 54):
      leaveDir = 0; # left
    elif (charXLeft >= 198):
      leaveDir = 1 # right
    else:
      return -1

  else:
    # looking right
    discard getTile(int(Char.room), 9, int32(Char.currRow))
    if currTile2 != ord(tiles7DoortopWithFloor) and currTile2 != ord(
        tiles12Doortop) and charXRight >= 201:
      leaveDir = 1 # right
    elif (charXRight <= 57):
      leaveDir = 0 # left
    else:
      return -1

  case leaveDir
  of 0: # left
    playMirrMus()
    level3SetChkp()
    JaffarExit()
  of 1: # right
    swordDisappears()
    meetJaffar()
  #of 2: # up
  of 3: # down
    # Special event: falling exit
    if (currentLevel == custom.fallingExitLevel and Char.room ==
        custom.fallingExitRoom):
      return -2
  else:
    discard

  discard gotoOtherRoom(leaveDir)
  when(USE_REPLAY):
    if ((skippingReplay != 0) and replaySeekTarget == ord(replaySeek0NextRoom)):
      skippingReplay = 0
  return leaveDir


# seg002:0643
proc JaffarExit() =
  if (leveldoorOpen == 2):
    discard getTile(24, 0, 0)
    triggerButton(0, 0, -1)

# seg002:0665
proc level3SetChkp() =
  # Special event: set checkpoint
  if currentLevel == custom.checkpointLevel and Char.room ==
      7: # TODO: add a custom option */)
    checkpoint = 1
    hitpBegLev = hitpMax

# seg002:0680
proc swordDisappears() =
  # Special event: sword disappears
  if (currentLevel == 12 and Char.room == 18):
    discard getTile(15, 1, 0)
    currRoomTiles[currTilepos] = ord(tiles1Floor)
    currRoomModif[currTilepos] = 0 # added, a nonzero modifier may show fake tiles

# seg002:06AE
proc meetJaffar() =
  # Special event: play music
  if (currentLevel == 13 and leveldoorOpen == 0 and Char.room == 3):
    playSound(ord(sound29MeetJaffar)); # meet Jaffar
    # Special event: Jaffar waits a bit (28/12=2.33 seconds)
    guardNoticeTimer = 28



# seg002:06D3
proc playMirrMus() =
  # Special event: mirror music
  if (
    leveldoorOpen != 0 and
    leveldoorOpen != 0x4D and # was the music played already?
    currentLevel == custom.mirrorLevel and
    byte(Char.currRow) == custom.mirrorRow and
    Char.room == 11): # TODO: add a custom option */

    playSound(ord(sound25Presentation)) # presentation (level 4 mirror)
    leveldoorOpen = 0x4D

# seg002:0706
proc move0Nothing() =
  controlShift = 0
  controlY = 0
  controlX = 0
  controlShift2 = 0
  controlDown = 0
  controlUp = 0
  controlBackward = 0
  controlForward = 0

# seg002:0721
proc move1Forward() =
  controlX = -1
  controlForward = -1

# seg002:072A
proc move2Backward() =
  controlBackward = -1
  controlX = 1

# seg002:0735
proc move3Up() =
  controlY = -1
  controlUp = -1

# seg002:073E
proc move4Down() =
  controlDown = -1
  controlY = 1


# seg002:0749
proc moveUpBack() =
  controlUp = -1
  move2Backward()


# seg002:0753
proc moveDownBack() =
  controlDown = -1
  move2Backward()


# seg002:075D
proc moveDownForw() =
  controlDown = -1
  move1Forward()


# seg002:0767
proc move6Shift() =
  controlShift = -1
  controlShift2 = -1


# seg002:0770
proc move7() =
  controlShift = 0


# seg002:0776
proc autocontrolOpponent() =
  var
    charid: word
  move0Nothing()
  charid = Char.charid
  if (charid == ord(charid0Kid)):
    autocontrolKid()
  else:
    if justblocked != 0:
      dec(justblocked)
    if kidSwordStrike != 0:
      dec(kidSwordStrike)
    if guardRefrac != 0:
      dec(guardRefrac)
    if (charid == ord(charid24Mouse)):
      autocontrolMouse()
    elif (charid == ord(charid4Skeleton)):
      autocontrolSkeleton()
    elif (charid == ord(charid1Shadow)):
      autocontrolShadow()
    elif (currentLevel == 13):
      autocontrolJaffar()
    else:
      autocontrolGuard()

# seg002:07EB
proc autocontrolMouse() =
  if (Char.direction == ord(dir56None)):
    return

  if (Char.action == ord(actions0Stand)):
    if (Char.x >= 200):
      clearChar()

  else:
    if (Char.x < 166):
      seqtblOffsetChar(ord(seq107MouseStandUpAndGo)) # mouse
      playSeq()


# seg002:081D
proc autocontrolShadow() =
  if (currentLevel == 4):
    autocontrolShadowLevel4()
  elif (currentLevel == 5):
    autocontrolShadowLevel5()
  elif (currentLevel == 6):
    autocontrolShadowLevel6()
  elif (currentLevel == 12):
    autocontrolShadowLevel12()



# seg002:0850
proc autocontrolSkeleton() =
  Char.sword = ord(sword2Drawn)
  autocontrolGuard()


# seg002:085A
proc autocontrolJaffar() =
  autocontrolGuard()


# seg002:085F
proc autocontrolKid() =
  autocontrolGuard()


# seg002:0864
proc autocontrolGuard() =
  if (Char.sword < ord(sword2Drawn)):
    autocontrolGuardInactive()
  else:
    autocontrolGuardActive()


## seg002:0876
proc autocontrolGuardInactive() =
  var
    distance: int16
  if (Kid.alive >= 0):
    return
  distance = int16(charOppDist())
  if (Opp.currRow != Char.currRow or distance < -8):
    # If Kid made a sound ...
    if isGuardNotice != 0:
      isGuardNotice = 0
      if (distance < 0):
        # ... and Kid is behind Guard, Guard turns around.
        if (distance < -4):
          move4Down()

        return

  elif (distance < 0):
    return


  if canGuardSeeKid != 0:
    # If Guard can see Kid, Guard moves to fighting pose.
    if (currentLevel != 13 or guardNoticeTimer == 0):
      moveDownForw()


# seg002:08DC
proc autocontrolGuardActive() =
  var
    oppFrame: int16
    charFrame: int16
    distance: int16
  charFrame = int16(Char.frame)
  if (charFrame != ord(frame166StandInactive) and charFrame >= 150 and
      canGuardSeeKid != 1):
    if (canGuardSeeKid == 0):
      if (droppedout != 0):
        guardFollowsKidDown()
        #return
      elif (Char.charid != ord(charid4Skeleton)):
        moveDownBack()

      #return
    else: # canGuardSeeKid == 2
      oppFrame = int16(Opp.frame)
      distance = int16(charOppDist())
      if (distance >= 12 and
          # frames 102..117: falling and landing
        oppFrame >= ord(frame102StartFall1) and oppFrame < ord(
            frame118StandUpFromCrouch9) and
        Opp.action == ord(actions5Bumped)):
        return

      if (distance < 35):
        if ((Char.sword < ord(sword2Drawn) and distance < 8) or distance < 12):
          if (Char.direction == Opp.direction):
            # turn around
            move2Backward()
            #return
          else:
            move1Forward()
            #return

        else:
          autocontrolGuardKidInSight(distance)
          #return

      else:
        if (guardRefrac != 0):
          return
        if (Char.direction != Opp.direction):
          # frames 7..14: running
          # frames 34..43: run-jump
          if (oppFrame >= ord(frame7Run) and oppFrame < 15):
            if (distance < 40):
              move6Shift()
            return
          elif (oppFrame >= ord(frame34StartRunJump1) and oppFrame < 44):
            if (distance < 50):
              move6Shift()
            return
            #return
        autocontrolGuardKidFar()

# seg002:09CB
proc autocontrolGuardKidFar() =
  if (tileIsFloor(getTileInfrontofChar()) or tileIsFloor(
      getTileInfrontof2Char())) != 0:
    move1Forward()
  else:
    move2Backward()


# seg002:09F8
proc guardFollowsKidDown() =
  # This is called from autocontrolGuardActive, so char=Guard, Opp=Kid
  var
    oppAction: word
  oppAction = Opp.action
  if (oppAction == ord(actions2HangClimb) or oppAction == ord(
      actions6HangStraight)):
    return

  if ( # there is wall in front of Guard
    wallType(byte(getTileInfrontofChar())) != 0 or
      ((tileIsFloor(int(currTile2)) == 0) and (
      (getTile(int(currRoom), int32(tileCol), int32(tileRow + 1)) == ord(
          tiles2Spike) or
      # Guard would fall on loose floor
      currTile2 == ord(tiles11Loose) or
      # ... or wall (?)
      wallType(currTile2) != 0 or
      # ... or into a chasm
      (tileIsFloor(int(currTile2)) == 0)) or
      # ... or Kid is not below
      Char.currRow + 1 != Opp.currRow))):
    inc(tileRow)
    # don't follow
    droppedout = 0
    move2Backward()
  else:
    inc(tileRow)
    # follow
    move1Forward()


# seg002:0A93
proc autocontrolGuardKidInSight(distance: int16) =
  if (Opp.sword == ord(sword2Drawn)):
    autocontrolGuardKidArmed(distance)
  elif (guardRefrac == 0):
    if (distance < 29):
      move6Shift()
    else:
      move1Forward()


# seg002:0AC1
proc autocontrolGuardKidArmed(distance: int16) =
  if (distance < 10 or distance >= 29):
    guardAdvance()
  else:
    guardBlock()
    if (guardRefrac == 0):
      if (distance < 12 or distance >= 29):
        guardAdvance()
      else:
        guardStrike()


# seg002:0AF5
proc guardAdvance() =
  if (guardSkill == 0 or kidSwordStrike == 0):
    if (custom.advprob[guardSkill] > prandom(255)):
      move1Forward()


# seg002:0B1D
proc guardBlock() =
  var
    oppFrame: word
  oppFrame = Opp.frame
  if (oppFrame == ord(frame152Strike2) or oppFrame == ord(frame153Strike3) or
      oppFrame == ord(frame162BlockToStrike)):
    if (justblocked != 0):
      if (custom.impblockprob[guardSkill] > prandom(255)):
        move3Up()

    else:
      if (custom.blockprob[guardSkill] > prandom(255)):
        move3Up()


# seg002:0B73
proc guardStrike() =
  var
    oppFrame: word
    charFrame: word
  oppFrame = Opp.frame
  if (oppFrame == ord(frame169BeginBlock) or oppFrame == ord(frame151Strike1)):
    return
  charFrame = Char.frame
  if (charFrame == ord(frame161Parry) or charFrame == ord(frame150Parry)):
    if (custom.restrikeprob[guardSkill] > prandom(255)):
      move6Shift()

  else:
    if (custom.strikeprob[guardSkill] > prandom(255)):
      move6Shift()


# seg002:0BCD
proc hurtBySword() =
  var
    distance: int16
  if (Char.alive >= 0):
    return
  if (Char.sword != ord(sword2Drawn)):
    # Being hurt when not in fighting pose means death.
    discard takeHp(100)
    seqtblOffsetChar(ord(seq85StabbedToDeath)) # dying (stabbed unarmed)
    if (getTileBehindChar() != 0 or distanceToEdgeWeight() < 4):
      distance = int16(distanceToEdgeWeight())
      seqtblOffsetChar(ord(seq85StabbedToDeath)) # dying (stabbed)
      if (Char.charid != ord(charid0Kid) and
          Char.direction < ord(dir0Right) and # looking left
        (currTile2 == ord(tiles4Gate) or getTileAtChar() == ord(tiles4Gate))):
        when (FIX_OFFSCREEN_GUARDS_DISAPPEARING):
          # a guard can get teleported to the other side of kid's room
          # when fighting between rooms and hitting a gate
          if (fixes.fixOffscreenGuardsDisappearing != 0'u8):
            var
              gateCol: int16 = tileCol
            if (currRoom != Char.room):
              if (currRoom == level.roomlinks[Char.room - 1].right):
                gateCol += 10
              elif (currRoom == level.roomlinks[Char.room - 1].left):
                gateCol -= 10
            Char.x = byte(xBump[tileCol - int16(currTile2 != ord(tiles4Gate)) +
                5] + 7)
          else:
            Char.x = byte(xBump[tileCol - int16(currTile2 != ord(tiles4Gate)) +
                5] + 7)
        Char.x = byte(charDxForward(10))

      Char.y = byte(yLand[Char.currRow + 1])
      Char.fallY = 0
    else:
      distance = int16(distanceToEdgeWeight())
      Char.x = byte(charDxForward(int(distance - 20)))
      loadFramDetCol()
      incCurrRow()
      # Kid/Guard is killed and pushed off the ledge
      seqtblOffsetChar(ord(seq81KidPushedOffLedge))
  else:
    # You can't hurt skeletons
    if (Char.charid != ord(charid4Skeleton)):
      if takeHp(1) != 0:

        if (getTileBehindChar() != 0 or distanceToEdgeWeight() < 4):
          distance = int16(distanceToEdgeWeight())
          seqtblOffsetChar(ord(seq85StabbedToDeath)) # dying (stabbed)
          if (Char.charid != ord(charid0Kid) and
              Char.direction < ord(dir0Right) and # looking left
            (currTile2 == ord(tiles4Gate) or getTileAtChar() == ord(tiles4Gate))):
            when (FIX_OFFSCREEN_GUARDS_DISAPPEARING):
              # a guard can get teleported to the other side of kid's room
              # when fighting between rooms and hitting a gate
              if (fixes.fixOffscreenGuardsDisappearing != 0'u8):
                var
                  gateCol: int16 = tileCol
                if (currRoom != Char.room):
                  if (currRoom == level.roomlinks[Char.room - 1].right):
                    gateCol += 10
                  elif (currRoom == level.roomlinks[Char.room - 1].left):
                    gateCol -= 10
                Char.x = byte(xBump[gateCol - int16(currTile2 != ord(
                    tiles4Gate)) + 5] + 7)
              else:
                Char.x = byte(xBump[tileCol - int16(currTile2 != ord(
                    tiles4Gate)) + 5] + 7)
            else:
              Char.x = byte(xBump[tileCol - int16(currTile2 != ord(
                  tiles4Gate)) + 5] + 7)
            Char.x = byte(charDxForward(10))

          Char.y = byte(yLand[Char.currRow + 1])
          Char.fallY = 0
        else:
          distance = int16(distanceToEdgeWeight())
          Char.x = byte(charDxForward(int(distance - 20)))
          loadFramDetCol()
          incCurrRow()
          # Kid/Guard is killed and pushed off the ledge
          seqtblOffsetChar(ord(seq81KidPushedOffLedge))
    seqtblOffsetChar(ord(seq74HitBySword)) # being hit with sword
    Char.y = byte(yLand[Char.currRow + 1])
    Char.fallY = 0

  # sound 13: Kid hurt (by sword), sound 12: Guard hurt (by sword)
  playSound(if Char.charid == ord(charid0Kid): ord(sound13KidHurt) else: ord(sound12GuardHurt))
  playSeq()


# seg002:0CD4
proc checkSwordHurt() =
  if (Guard.action == ord(actions99Hurt)):
    if (Kid.action == ord(actions99Hurt)):
      Kid.action = ord(actions1RunJump)

    loadshad()
    hurtBySword()
    saveshad()
    guardRefrac = custom.refractimer[guardSkill]
  else:
    if (Kid.action == ord(actions99Hurt)):
      loadkid()
      hurtBySword()
      savekid()


# seg002:0D1A
proc checkSwordHurting() =
  var
    kidFrame: int16
  kidFrame = int16(Kid.frame)
  # frames 217..228: go up on stairs
  if (kidFrame != 0 and (kidFrame < ord(frame219ExitStairs3) or kidFrame >= 229)):
    loadshadAndOpp()
    checkHurting()
    saveshadAndOpp()
    loadkidAndOpp()
    checkHurting()
    savekidAndOpp()


# seg002:0D56
proc checkHurting() =
  var
    oppFrame, charFrame, distance, minHurtRange: int16
  if (Char.sword != ord(sword2Drawn)):
    return
  if (Char.currRow != Opp.currRow):
    return
  charFrame = int16(Char.frame)
  # frames 153..154: poking with sword
  if (charFrame != ord(frame153Strike3) and charFrame != ord(frame154Poking)):
    return
  # If char is poking ...
  distance = int16(charOppDist())
  oppFrame = int16(Opp.frame)
  # frames 161 and 150: parrying
  if (distance < 0 or distance >= 29 or
    (oppFrame != ord(frame161Parry) and oppFrame != ord(frame150Parry))):
    # ... and Opp is not parrying
    # frame 154: poking
    if (Char.frame == ord(frame154Poking)):
      if (Opp.sword < ord(sword2Drawn)):
        minHurtRange = 8
      else:
        minHurtRange = 12

      distance = int16(charOppDist())
      if (distance >= minHurtRange and distance < 29):
        Opp.action = ord(actions99Hurt)


  else:
    Opp.frame = ord(frame161Parry)
    if (Char.charid != ord(charid0Kid)):
      justblocked = 4

    seqtblOffsetChar(ord(seq69AttackWasParried)) # attack was parried
    playSeq()

  if (Char.direction == ord(dir56None)):
    return # Fix looping "sword moving" sound.
  # frame 154: poking
  # frame 161: parrying
  if (Char.frame == ord(frame154Poking) and Opp.frame != ord(frame161Parry) and
      Opp.action != ord(actions99Hurt)):
    playSound(ord(sound11SwordMoving)) # sword moving


# seg002:0E1F
proc checkSkel() =
  # Special event: skeleton wakes
  if (currentLevel == custom.skeletonLevel and
      Guard.direction == ord(dir56None) and
      drawnRoom == custom.skeletonRoom and
      (leveldoorOpen != 0 or (custom.skeletonRequireOpenLevelDoor == 0)) and
      (byte(Kid.currCol) == custom.skeletonTriggerColumn1 or
      byte(Kid.currCol) == custom.skeletonTriggerColumn2)):
    discard getTile(int(drawnRoom), int32(custom.skeletonColumn), int32(
        custom.skeletonRow))
    if (currTile2 == ord(tiles21Skeleton)):
      # erase skeleton
      currRoomTiles[currTilepos] = ord(tiles1Floor)
      redrawHeight = 24
      setRedrawFull(int16(currTilepos), 1)
      setWipe(int16(currTilepos), 1)
      inc(currTilepos)
      setRedrawFull(int16(currTilepos), 1)
      setWipe(int16(currTilepos), 1)
      Char.room = byte(drawnRoom)
      Char.currRow = sbyte(custom.skeletonRow)
      Char.y = byte(yLand[Char.currRow + 1])
      Char.currCol = sbyte(custom.skeletonColumn)
      Char.x = byte(xBump[Char.currCol + 5] + 14)
      Char.direction = ord(dirFFLeft)
      seqtblOffsetChar(ord(seq88SkelWakeUp)) # skel wake up
      playSeq()
      playSound(ord(sound44SkelAlive)) # skel alive
      guardSkill = custom.skeletonSkill
      Char.alive = -1
      guardhpMax = 3
      guardhpCurr = 3
      Char.fallX = 0
      Char.fallY = 0
      isGuardNotice = 0
      guardRefrac = 0
      Char.sword = ord(sword2Drawn)
      Char.charid = ord(charid4Skeleton)
      saveshad()




# seg002:0F3F
proc doAutoMoves(movesPtr: openArray[AutoMoveType]) =
  var
    demoindex: int16
    currMove: int16
  if (demoTime >= 0xFE):
    return
  inc(demoTime)
  demoindex = demoIndex
  if (movesPtr[demoindex].time <= demoTime):
    inc(demoIndex)
  else:
    demoindex = demoIndex - 1

  currMove = movesPtr[demoindex].move
  case (currMove)
  of high(typeof(currMove)):
    discard
  of 0:
    move0Nothing()
  of 1:
    move1Forward()
  of 2:
    move2Backward()
  of 3:
    move3Up()
  of 4:
    move4Down()
  of 5:
    move3Up()
    move1Forward()
  of 6:
    move6Shift()
  of 7:
    move7()
  else:
    discard



# seg002:1000
proc autocontrolShadowLevel4() =
  if (Char.room == 4):
    if (Char.x < 80):
      clearChar()
    else:
      move1Forward()


## This was moved to customOptionsType.
#/*
## data:0F02
#const autoMoveType shadDrinkMove[] = {
#{0x00, 0},
#{0x01, 1},
#{0x0E, 0},
#{0x12, 6},
#{0x1D, 7},
#{0x2D, 2},
#{0x31, 1},
#{0xFF,-2},

#*/

# seg002:101A
proc autocontrolShadowLevel5() =
  if (Char.room == 24):
    if (demoTime == 0):
      discard getTile(24, 1, 0)
      # is the door open?
      if (currRoomModif[currTilepos] < 80):
        return
      demoIndex = 0

    doAutoMoves(custom.shadDrinkMove)
    if (Char.x < 15):
      clearChar()


# seg002:1064
proc autocontrolShadowLevel6() =
  if (Char.room == 1 and
    Kid.frame == ord(frame43RunningJump4) and # a frame in run-jump
    Kid.x < 128):
    move6Shift()
    move1Forward()


# seg002:1082
proc autocontrolShadowLevel12() =
  var
    oppFrame: int16
    xdiff: int16
  if (Char.room == 15 and shadowInitialized == 0):
    if (Opp.x >= 150):
      doInitShad(custom.initShad12, 7)
      return

    shadowInitialized = 1

  if (Char.sword >= ord(sword2Drawn)):
    # if the Kid puts his sword away, the shadow does the same,
    # but only if the shadow was already hurt (?)
    if (offguard == 0 or guardRefrac == 0):
      autocontrolGuardActive()
    else:
      move4Down()

    return

  if (Opp.sword >= ord(sword2Drawn) or offguard == 0):
    xdiff = 0x7000; # bugfix/workaround
    # This behavior matches the DOS version but not the Apple II source.
    if (canGuardSeeKid < 2 or charOppDist() >= 90):
      xdiff = int16(charOppDist())
      if (xdiff < 0):
        move2Backward()

      return
    xdiff = int16(charOppDist())

    # Shadow draws his sword
    if (Char.frame == 15):
      moveDownForw()

    return

  if (charOppDist() < 10):
    # unite with the shadow
    flashColor = ord(color15Brightwhite) # white
    flashTime = 18
    # get an extra HP for uniting the shadow
    addLife()
    # time of Kid-shadow flash
    unitedWithShadow = 42
    # put the Kid where the shadow was
    Char.charid = ord(charid0Kid)
    savekid()
    # remove the shadow
    clearChar()
    return

  if (canGuardSeeKid == 2):
    # If Kid runs to shadow, shadow runs to Kid.
    oppFrame = int16(Opp.frame)
    # frames 1..14: running
    # frames 121..132: stepping
    if ((oppFrame >= ord(frame3StartRun) and oppFrame < ord(frame15Stand)) or
      (oppFrame >= ord(frame127Stepping7) and oppFrame < 133)):
      move1Forward()
