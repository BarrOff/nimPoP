# data:1888
# var
#   seqtblOffsets: seq[word]

# seg005:000A
proc seqtblOffsetChar(seqIndex: int16) =
  Char.currSeq = seqtblOffsets[seqIndex]


# seg005:001D
proc seqtblOffsetOpp(seqIndex: int32) =
  Opp.currSeq = seqtblOffsets[seqIndex]


# seg005:0030
proc doFall() =
  if (isScreaming == 0 and Char.fallY >= 31):
    playSound(ord(sound1Falling)) # falling
    isScreaming = 1

  if (yLand[Char.currRow + 1] > int16(Char.y)):
    checkGrab()

    when (FIX_GLIDE_THROUGH_WALL):
      if (fixes.fixGlideThroughWall != 0):
        # Fix for the kid falling through walls after turning around while running (especially when weightless)
        determineCol()
        discard getTileAtChar()
        if (currTile2 == ord(tiles20Wall) or
            ((currTile2 == ord(tiles12Doortop) or currTile2 == ord(
                tiles7DoortopWithFloor)) and
             Char.direction == ord(dir_FFLeft))):
          var
            deltaX = distanceToEdgeWeight()
          #printf("deltaX = %d\n", deltaX)
          # When falling into a wall or doortop after turning or running, deltaX is likely to be either 8, 11 or 12
          # Calling inWall() here produces the desired fix (no glitching through wall), but only when deltaX >= 10
          # The code below "emulates" inWall() to always get the same effect as if deltaX == 10
          # (which distance actually looks/behaves best is a matter of preference)
          const
            deltaXReference = 10
          if (deltaX >= 8):
            deltaX = -5 + deltaX - deltaXReference
            Char.x = byte(charDxForward(deltaX))

            Char.fallX = 0 # not in inWall(), but we do need to cancel horizontal movement

  else:
    when (FIX_JUMP_THROUGH_WALL_ABOVE_GATE):
      if (fixes.fixJumpThroughWallAboveGate != 0):
        # At this point, Char.currCol has not yet been updated since checkBumped()
        # Basically, Char.currCol is still set to the column of the wall itself (even though the kid
        # may have 'bumped' against the wall, with Char.x being offset)

        # To prevent inWall() from being called we need to update Char.currCol here.
        # (inWall() only makes things worse, because it tries to 'eject' the kid from the wall the wrong way.
        # For this reason, the kid can end up behind a closed gate below, like is possible in Level 7)

        # The fix is calling determineCol() here.

        # One caveat: the kid can also land on the rightmost edge of a closed gate tile, when doing a running jump
        # to the left from two floors up. This 'trick' can be used in original PoP, but is 'fixed' by this change.
        # To still allow this trick to be possible, we can check that we not jumping into a gate tile.
        # (By the way, strangely enough, in unmodified PoP the trick even works with a tapestry + floor tile...)

        if (getTileAtChar() != ord(tiles4Gate)):
          determineCol()

    when (FIX_DROP_THROUGH_TAPESTRY):
      if (getTileAtChar() == ord(tiles20Wall)):
        inWall()
      elif ((fixes.fixDropThroughTapestry != 0) and getTileAtChar() == ord(
          tiles12Doortop) and Char.direction == ord(dir_FFLeft)):
        if (distanceToEdgeWeight() >= 8): # only intervene if the kid is actually IN FRONT of the tapestry
          inWall()
    else:
      if (getTileAtChar() == ord(tiles20Wall)):
        inWall()

    if (tileIsFloor(int(currTile2)) != 0):
      land()
    else:
      incCurrRow()


# seg005:0090
proc land() =
  var
    seqId: word
    loc5EE6: bool = false
  isScreaming = 0
  Char.y = byte(yLand[Char.currRow + 1])
  if (getTileAtChar() != ord(tiles2Spike)):
    when (FIX_LAND_AGAINST_GATE_OR_TAPESTRY):
      if ((tileIsFloor(getTileInfrontofChar()) != 0) and
          distanceToEdgeWeight() < 3):
        Char.x = byte(charDxForward(-3))
      elif (fixes.fixLandAgainstGateOrTapestry != 0):
        # A closed gate right in front of the landing spot should not behave like an open floor tile, but like a wall
        # Similar for a tapestry tile (with floor)
        discard getTileInfrontofChar()
        if (Char.direction == ord(dir_FFLeft) and (
                (currTile2 == ord(tiles4Gate) and (canBumpIntoGate() != 0)) or
                (currTile2 == ord(tiles7DoortopWithFloor))) and
                    distanceToEdgeWeight() < 3
        ):
          Char.x = byte(charDxForward(-3))
    else:
      if ((tileIsFloor(getTileInfrontofChar()) == 0) and distanceToEdgeWeight() < 3):
        Char.x = byte(charDxForward(-3))
    startChompers()
  else:
    # fell on spikes
    loc5EE6 = true
  block myBlock:
    if (Char.alive < 0 or loc5EE6):
      # alive
      if ((distanceToEdgeWeight() >= 12 and getTileBehindChar() == ord(
          tiles2Spike)) or getTileAtChar() == ord(tiles2Spike)) or loc5EE6:
        # fell on spikes
        when (FIX_SAFE_LANDING_ON_SPIKES):
          if (isSpikeHarmful() != 0):
            spiked()
            return
          elif ((fixes.fixSafeLandingOnSpikes != 0) and currRoomModif[
              currTilepos] == 0):
            # spikes ARE dangerous, just not out yet.
            spiked()
            return
        else:
          if (isSpikeHarmful() != 0):
            spiked()
            return
      if (Char.fallY < 22):
        # fell 1 row
        if (Char.charid >= ord(charid2Guard) or Char.sword == ord(sword2Drawn)):
          Char.sword = ord(sword2Drawn)
          seqId = ord(seq63GuardStandActive) # stand active after landing
        else:
          seqId = ord(seq17SoftLand) # crouch (soft land)
        if (Char.charid == ord(charid0Kid)):
          playSound(ord(sound17SoftLand)) # soft land (crouch)
          isGuardNotice = 1
      elif (Char.fallY < 33):
        # fell 2 rows
        if (Char.charid == ord(charid1Shadow)):
          if (Char.charid >= ord(charid2Guard) or Char.sword == ord(sword2Drawn)):
            Char.sword = ord(sword2Drawn)
            seqId = ord(seq63GuardStandActive) # stand active after landing
          else:
            seqId = ord(seq17SoftLand) # crouch (soft land)
          if (Char.charid == ord(charid0Kid)):
            playSound(ord(sound17SoftLand)) # soft land (crouch)
            isGuardNotice = 1
          break myBlock
        if (Char.charid == ord(charid2Guard)):
          discard takeHp(100)
          playSound(ord(sound0FellToDeath))
          seqId = ord(seq22Crushed)
          break myBlock
        # kid (or skeleton (bug!))
        if (takeHp(1) == 0):
          # still alive
          playSound(ord(sound16MediumLand)) # medium land
          isGuardNotice = 1
          seqId = ord(seq20MediumLand) # medium land (lose 1 HP, crouch)
      else:
        # dead (this was the last HP)
        playSound(ord(sound0FellToDeath))
        seqId = ord(seq22Crushed)
    else:
      # fell 3 or more rows
      discard takeHp(100)
      playSound(ord(sound0FellToDeath))
      seqId = ord(seq22Crushed)
  seqtblOffsetChar(int16(seqId))
  playSeq()
  Char.fallY = 0


# seg005:01B7
proc spiked() =
  # If someone falls into spikes, those spikes become harmless (to others).
  currRoomModif[currTilepos] = 0xFF
  Char.y = byte(yLand[Char.currRow + 1])
  when(FIX_OFFSCREEN_GUARDS_DISAPPEARING):
    # a guard can get teleported to the other side of kid's room
    # when landing on spikes in another room
    if fixes.fixOffscreenGuardsDisappearing != 0'u8:
      var
        spikeCol: int16 = tileCol
      if Char.room != currRoom:
        if (currRoom == level.roomlinks[Char.room - 1].right):
          spikeCol += 10
        elif (currRoom == level.roomlinks[Char.room - 1].left):
          spikeCol -= 10
      Char.x = byte(xBump[spikeCol + 5] + 10)
    else:
      Char.x = byte(xBump[tileCol + 5] + 10)
  else:
    Char.x = byte(xBump[tileCol + 5] + 10)

  Char.fallY = 0
  playSound(ord(sound48Spiked)) # something spiked
  discard takeHp(100)
  seqtblOffsetChar(ord(seq51Spiked)) # spiked
  playSeq()


# seg005:0213
proc control() =
  var
    charFrame: int16
    charAction: int16
    # crouchAfterClimbing: bool = false
  charFrame = int16(Char.frame)
  if (Char.alive >= 0):
    if (charFrame == ord(frame15Stand) or # stand
      charFrame == ord(frame166StandInactive) or # stand
      charFrame == ord(frame158StandWithSword) or # stand with sword
      charFrame == ord(frame171StandWithSword) # stand with sword
    ):
      seqtblOffsetChar(ord(seq71Dying)) # dying (not stabbed)
  else:
    charAction = int16(Char.action)
    if (charAction == ord(actions5Bumped) or
      charAction == ord(actions4InFreefall)
    ):
      discard releaseArrows()
    elif (Char.sword == ord(sword2Drawn)):
      controlWithSword()
    elif (Char.charid >= ord(charid2Guard)):
      controlGuardInactive()
    elif (charFrame == ord(frame15Stand) or # standing
      (charFrame >= ord(frame50Turn) and charFrame < 53) # end of turning
    ):
      controlStanding()
    elif (charFrame == ord(frame48Turn)): # a frame in turning
      controlTurning()
    elif (charFrame < 4): # start run
      controlStartrun()
    elif (charFrame >= ord(frame67StartJumpUp1) and charFrame < ord(
        frame70Jumphang)): # start jump up
      controlJumpup()
    elif (charFrame < 15): # running
      controlRunning()
    elif (charFrame >= ord(frame87Hanging1) and charFrame < 100): # hanging
      controlHanging()
    elif (charFrame == ord(frame109Crouch)): # crouching
      controlCrouched()
    else:
      # crouchAfterClimbing = true

      when (ALLOW_CROUCH_AFTER_CLIMBING):
      # When ducking with down+forward, give time to release the forward control (prevents unintended crouch-hops)
        if fixes.enableCrouchAfterClimbing and ((
            fixes.enableCrouchAfterClimbing != 0) and Char.currSeq >=
            seqtblOffsets[ord(seq50Crouch)] and Char.currSeq < seqtblOffsets[
            ord(seq49StandUpFromCrouch)]): # while stooping
          if (controlForward < 1):
            controlForward = 0
      else:
        discard

    when (FIX_MOVE_AFTER_DRINK):
      if ((fixes.fixMoveAfterDrink != 0) and charFrame >= ord(frame191Drink) and
          charFrame <= ord(frame205Drink)):
        discard releaseArrows()

    when (FIX_MOVE_AFTER_SHEATHE):
      if ((fixes.fixMoveAfterSheathe != 0) and
          Char.currSeq >= seqtblOffsets[ord(seq92PutSwordAway)] and
          Char.currSeq < seqtblOffsets[ord(seq93PutSwordAwayFast)]
      ):
        discard releaseArrows()


# seg005:02EB
proc controlCrouched() =
  if (needLevel1Music != 0 and currentLevel == custom.introMusicLevel):
    # Special event: music when crouching
    if (checkSoundPlaying() == 0):
      if (needLevel1Music == 1):
        playSound(ord(sound25Presentation)) # presentation (level 1 start)
        needLevel1Music = 2
      else:
        when (USE_REPLAY):
          if (recording != 0):
            specialMove = ord(MOVE_EFFECT_END)
          if (replaying == 0): # during replays, crouch immobilization gets cancelled in doReplayMove()
            needLevel1Music = 0
        else:
          needLevel1Music = 0
  else:
    needLevel1Music = 0
    if (controlShift2 < 0 and (checkGetItem() != 0)):
      return
    if (controlY != 1):
      seqtblOffsetChar(ord(seq49StandUpFromCrouch)) # stand up from crouch
    else:
      if (controlForward < 0):
        controlForward = 1 # disable automatic repeat
        seqtblOffsetChar(ord(seq79CrouchHop)) # crouch-hop


# seg005:0358
proc controlStanding() =
  var
    var2: int16
  if (controlShift2 < 0 and controlShift < 0 and (checkGetItem() != 0)):
    return

  if (Char.charid != ord(charid0Kid) and controlDown < 0 and controlForward < 0):
    drawSword()
    return
#else
  if haveSword != 0:
    if (offguard != 0 and controlShift >= 0):
      if (controlForward < 0):
        if ((isKeyboardMode != 0) and controlUp < 0):
          standingJump()
        else:
          forwardPressed()
      elif (controlBackward < 0):
        backPressed()
      elif (controlUp < 0):
        if ((isKeyboardMode != 0) and controlForward < 0):
          standingJump()
        else:
          upPressed()
      elif (controlDown < 0):
        downPressed()
      elif (controlX < 0):
        forwardPressed()
      return
    if (canGuardSeeKid >= 2):
      var2 = int16(charOppDist())
      if (var2 >= -10 and var2 < 90):
        holdingSword = 1
        if (cast[word](var2) < cast[word](-6)):
          if (Opp.charid == ord(charid1Shadow) and
            (Opp.action == ord(actions3InMidair) or (Opp.frame >= ord(
                frame107FallLand1) and Opp.frame < 118))
          ):
            offguard = 0
          else:
            drawSword()
            return
        else:
          backPressed()
          return
    else:
      offguard = 0
  if (controlShift < 0):
    if (controlBackward < 0):
      backPressed()
    elif (controlUp < 0):
      upPressed()
    elif (controlDown < 0):
      downPressed()
    elif (controlX < 0 and controlForward < 0):
      safeStep()
  elif (controlForward < 0):
    if ((isKeyboardMode != 0) and controlUp < 0):
      standingJump()
    else:
      forwardPressed()
  elif (controlBackward < 0):
    backPressed()
  elif (controlUp < 0):
    if ((isKeyboardMode != 0) and controlForward < 0):
      standingJump()
    else:
      upPressed()
  elif (controlDown < 0):
    downPressed()
  elif (controlX < 0):
    forwardPressed()


# seg005:0482
proc upPressed() =
  var
    leveldoorTilepos: int32 = -1
  if (getTileAtChar() == ord(tiles16LevelDoorLeft)):
    leveldoorTilepos = int32(currTilepos)
  elif (getTileBehindChar() == ord(tiles16LevelDoorLeft)):
    leveldoorTilepos = int32(currTilepos)
  elif (getTileInfrontofChar() == ord(tiles16LevelDoorLeft)):
    leveldoorTilepos = int32(currTilepos)
  if ((leveldoorTilepos != -1) and word(level.startRoom) != drawnRoom and
    (if fixes.fixExitDoor != 0: currRoomModif[leveldoorTilepos] >=
        42 else: # this door must be fully open
      leveldoorOpen != 0)):
    goUpLeveldoor()
  else:
    if (controlX < 0):
      standingJump()
    else:
      checkJumpUp()


# seg005:04C7
proc downPressed() =
  controlDown = 1 # disable automatic repeat
  if ((tileIsFloor(getTileInfrontofChar()) == 0) and distanceToEdgeWeight() < 3):
    Char.x = byte(charDxForward(5))
    loadFramDetCol()
  else:
    if ((tileIsFloor(getTileBehindChar()) == 0) and distanceToEdgeWeight() >= 8):
      throughTile = byte(getTileBehindChar())
      discard getTileAtChar()
      if ((canGrab() != 0) and (when (ALLOW_CROUCH_AFTER_CLIMBING): (not(
          (fixes.enableCrouchAfterClimbing != 0) and controlForward ==
          -1)) else: true) and (Char.direction >= ord(ord(dir0Right)) or
              getTileAtChar() != ord(tiles4Gate) or currRoomModif[
              currTilepos] shr 2 >= 6)):
        Char.x = byte(charDxForward(distanceToEdgeWeight() - 9))
        seqtblOffsetChar(ord(seq68ClimbDown)) # climb down
      else:
        crouch()
    else:
      crouch()


# seg005:0574
proc goUpLeveldoor() =
  Char.x = byte(xBump[tileCol + 5] + 10)
  Char.direction = ord(dirFFLeft) # right
  seqtblOffsetChar(ord(seq70GoUpOnLevelDoor)) # go up on level door


# seg005:058F
proc controlTurning() =
  if (controlShift >= 0 and controlX < 0 and controlY >= 0):
    seqtblOffsetChar(ord(seq43StartRunAfterTurn)) # start run and run (after turning)


  # Added:
  # When using a joystick, the kid may sometimes jump/duck/turn unintendedly after turning around.
  # To prevent this: clear the remembered controls, so that if the stick has already moved to another/neutral position,
  # the kid will not jump, duck, or turn again.
  if (isJoystMode != 0):
    if (controlUp < 0 and controlY >= 0):
      controlUp = 0

    if (controlDown < 0 and controlY <= 0):
      controlDown = 0

    if (controlBackward < 0 and controlX == 0):
      controlBackward = 0


# seg005:05AD
proc crouch() =
  seqtblOffsetChar(ord(seq50Crouch)) # crouch
  controlDown = cast[sbyte](releaseArrows())


# seg005:05BE
proc backPressed() =
  var
    seqId: word
  controlBackward = cast[sbyte](releaseArrows())
  # After turn, Kid will draw sword if ...
  if (haveSword == 0 or
      canGuardSeeKid < 2 or
      charOppDist() > 0 or
      distanceToEdgeWeight() < 2):
    seqId = ord(seq5Turn) # turn
  else:
    Char.sword = ord(sword2Drawn)
    offguard = 0
    seqId = ord(seq89TurnDrawSword) # turn and draw sword

  seqtblOffsetChar(int16(seqId))


# seg005:060F
proc forwardPressed() =
  var
    distance: int16
  distance = int16(getEdgeDistance())
  when (ALLOW_CROUCH_AFTER_CLIMBING):
    if ((fixes.enableCrouchAfterClimbing != 0) and controlDown < 0):
      downPressed()
      controlForward = 0
      return

  if (edgeType == 1 and currTile2 != ord(tiles18Chomper) and distance < 8):
    # If char is near a wall, step instead of run.
    if (controlForward < 0):
      safeStep()
  else:
    seqtblOffsetChar(ord(seq1StartRun)) # start run and run


# seg005:0649
proc controlRunning() =
  if (controlX == 0 and (Char.frame == ord(frame7Run) or Char.frame == ord(frame11Run))):
    controlForward = cast[sbyte](releaseArrows())
    seqtblOffsetChar(ord(seq13StopRun)) # stop run
  elif (controlX > 0):
    controlBackward = cast[sbyte](releaseArrows())
    seqtblOffsetChar(ord(seq6RunTurn)) # run-turn
  elif (controlY < 0 and controlUp < 0):
    runJump()
  elif (controlDown < 0):
    controlDown = 1 # disable automatic repeat
    seqtblOffsetChar(ord(seq26CrouchWhileRunning)) # crouch while running


# seg005:06A8
proc safeStep() =
  var
    distance: int16
  controlShift2 = 1 # disable automatic repeat
  controlForward = 1 # disable automatic repeat
  distance = int16(getEdgeDistance())
  if distance != 0:
    Char.repeat = 1
    seqtblOffsetChar(distance + 28) # 29..42: safe step to edge
  elif (edgeType != 1 and Char.repeat != 0):
    Char.repeat = 0
    seqtblOffsetChar(ord(seq44StepOnEdge)) # step on edge
  else:
    seqtblOffsetChar(ord(seq39SafeStep11)) # unsafe step (off ledge)


# seg005:06F0
proc checkGetItem(): int32 =
  if (getTileAtChar() == ord(tiles10Potion) or currTile2 == ord(tiles22Sword)):
    if tileIsFloor(getTileBehindChar()) == 0:
      return 0

    Char.x = byte(charDxForward(-14))
    loadFramDetCol()

  if (getTileInfrontofChar() == ord(tiles10Potion) or currTile2 == ord(tiles22Sword)):
    getItem()
    return 1

  return 0


# seg005:073E
proc getItem() =
  var
    distance: int16
  if (Char.frame != ord(frame109Crouch)): # crouching
    distance = int16(getEdgeDistance())
    if (edgeType != 2):
      Char.x = byte(charDxForward(distance))

    if (Char.direction >= ord(dir0Right)):
      Char.x = byte(charDxForward(int(currTile2 == ord(tiles10Potion)) - 2))

    crouch()
  elif (currTile2 == ord(tiles22Sword)):
    doPickup(-1)
    seqtblOffsetChar(ord(seq91GetSword)) # get sword
  else: # potion
    doPickup(int(currRoomModif[currTilepos] shr 3))
    seqtblOffsetChar(ord(seq78Drink)) # drink
    when (USE_COPYPROT):
      if (currentLevel == 15):
        for index in 0 ..< 14:
          # remove letter on potions level
          if (copyprotRoom[index] == word(currRoom) and copyprotTile[index] == currTilepos):
            copyprotRoom[index] = 0
            break


# seg005:07FF
proc controlStartrun() =
  if (controlY < 0 and controlX < 0):
    standingJump()


# seg005:0812
proc controlJumpup() =
  if (controlX < 0 or controlForward < 0):
    standingJump()


# seg005:0825
proc standingJump() =
  controlUp = 1
  controlForward = 1 # disable automatic repeat
  seqtblOffsetChar(ord(seq3StandingJump)) # standing jump


# seg005:0836
proc checkJumpUp() =
  controlUp = cast[sbyte](releaseArrows())
  throughTile = byte(getTileAboveChar())
  discard getTileFrontAboveChar()
  if (canGrab() != 0):
    grabUpWithFloorBehind()
  else:
    throughTile = byte(getTileBehindAboveChar())
    discard getTileAboveChar()
    if (canGrab() != 0):
      jumpUpOrGrab()
    else:
      jumpUp()


# seg005:087B
proc jumpUpOrGrab() =
  var
    distance: int16
  distance = int16(distanceToEdgeWeight())
  if (distance < 6):
    jumpUp()
  elif (tileIsFloor(getTileBehindChar()) == 0):
    # There is not floor behind char.
    grabUpNoFloorBehind()
  else:
    # There is floor behind char, go back a bit.
    Char.x = byte(charDxForward(distance - 14))
    loadFramDetCol()
    grabUpWithFloorBehind()


# seg005:08C7
proc grabUpNoFloorBehind() =
  discard getTileAboveChar()
  Char.x = byte(charDxForward(distanceToEdgeWeight() - 10))
  seqtblOffsetChar(ord(seq16JumpUpAndGrab)) # jump up and grab (no floor behind)


# seg005:08E6
proc jumpUp() =
  var
    distance: int16
  controlUp = cast[sbyte](releaseArrows())
  distance = int16(getEdgeDistance())
  if (distance < 4 and edgeType == 1):
    Char.x = byte(charDxForward(distance - 3))

  when (FIX_JUMP_DISTANCE_AT_EDGE):
  # When climbing up two floors, turning around and jumping upward, the kid falls down.
  # This fix makes the workaround of Trick 25 unnecessary.
    if ((fixes.fixJumpDistanceAtEdge != 0) and distance == 3 and edgeType == 0):
      Char.x = byte(charDxForward(-1))

  discard getTile(int(Char.room), int32(getTileDivMod(backDeltaX(0) + int32(
      dxWeight()) - 6)), int32(Char.currRow - 1))
  if (currTile2 != ord(tiles20Wall) and (tileIsFloor(int(currTile2)) == 0)):
    seqtblOffsetChar(ord(seq28JumpUpWithNothingAbove)) # jump up with nothing above
  else:
    seqtblOffsetChar(ord(seq14JumpUpIntoCeiling)) # jump up with wall or floor above


# seg005:0968
proc controlHanging() =
  if (Char.alive < 0):
    if (grabTimer == 0 and controlY < 0):
      canClimbUp()
    elif (controlShift < 0):
      # hanging against a wall or a doortop
      if (Char.action != ord(actions6HangStraight) and (getTileAtChar() == ord(
          tiles20Wall) or (Char.direction == ord(dirFFLeft) and ( # facing left
        currTile2 == ord(tiles7DoortopWithFloor) or
        currTile2 == ord(tiles12Doortop)
      )))):
        if (grabTimer == 0):
          playSound(ord(sound8Bumped)) # touching a wall (hang against wall)
        seqtblOffsetChar(ord(seq25HangAgainstWall)) # hang against wall (straight)
      else:
        if (tileIsFloor(getTileAboveChar()) == 0):
          hangFall()
    else:
      hangFall()
  else:
    hangFall()


# seg005:09DF
proc canClimbUp() =
  var
    seqId: int16
  seqId = ord(seq10ClimbUp) # climb up
  controlUp = cast[sbyte](releaseArrows())
  controlShift2 = controlUp
  discard getTileAboveChar()
  if (((currTile2 == ord(tiles13Mirror) or currTile2 == ord(tiles18Chomper)) and
      Char.direction == ord(dir0Right)) or (currTile2 == ord(tiles4Gate) and
      Char.direction != ord(dir0Right) and currRoomModif[currTilepos] shr 2 < 6)
  ):
    seqId = ord(seq73ClimbUpToClosedGate) # climb up to closed gate and down

  seqtblOffsetChar(seqId)


# seg005:0A46
proc hangFall() =
  controlDown = cast[sbyte](releaseArrows())
  if ((tileIsFloor(getTileBehindChar()) == 0) and (tileIsFloor(getTileAtChar()) == 0)):
    seqtblOffsetChar(ord(seq23ReleaseLedgeAndFall)) # release ledge and fall
  else:
    if (getTileAtChar() == ord(tiles20Wall) or
      (Char.direction < ord(dir0Right) and ( # looking left
        currTile2 == ord(tiles7DoortopWithFloor) or
        currTile2 == ord(tiles12Doortop)
      ))
    ):
      Char.x = byte(charDxForward(-7))

    seqtblOffsetChar(ord(seq11ReleaseLedgeAndLand)) # end of climb down



# seg005:0AA8
proc grabUpWithFloorBehind() =
  var
    distance: int16
  distance = int16(distanceToEdgeWeight())

  # The global variable edgeType (which we need!) gets set as a side effect of getEdgeDistance()
  var
    edgeDistance: int16 = int16(getEdgeDistance())
    jumpStraightCondition: bool
  #printf("Distance to edge weight: %d\tedge type: %d\tedge distance: %d\n", distance, edgeType, edgeDistance)

  when (FIX_EDGE_DISTANCE_CHECK_WHEN_CLIMBING):
  # When climbing to a higher floor, the game unnecessarily checks how far away the edge below is
  # This contributes to sometimes "teleporting" considerable distances when climbing from firm ground
    if (fixes.fixEdgeDistanceCheckWhenClimbing != 0):
      jumpStraightCondition = (distance < 4 and edgeType != 1)
    else:
      jumpStraightCondition = (distance < 4 and edgeDistance < 4 and edgeType != 1)
  else:
    jumpStraightCondition = distance < 4 and edgeDistance < 4 and edgeType != 1

  if (jumpStraightCondition):
    Char.x = byte(charDxForward(distance))
    seqtblOffsetChar(ord(seq8JumpUpAndGrabStraight)) # jump up and grab (when?)
  else:
    Char.x = byte(charDxForward(distance - 4))
    seqtblOffsetChar(ord(seq24JumpUpAndGrabForward)) # jump up and grab (with floor behind)


# seg005:0AF7
proc runJump() =
  var
    xpos: int16
    col: int16
    var8: int16
  if (Char.frame >= ord(frame7Run)):
    # Align Kid to edge of floor.
    xpos = int16(charDxForward(4))
    col = int16(getTileDivModM7(xpos))
    for var2 in 0 ..< 2:
      col += dirFront[Char.direction + 1]
      discard getTile(int(Char.room), int32(col), int32(Char.currRow))
      if (currTile2 == ord(tiles2Spike) or (tileIsFloor(int(currTile2)) == 0)):
        var8 = int16(distanceToEdge(xpos)) + 14 * int16(var2) - 14
        if (var8 < -8 or var8 >= 2):
          if (var8 < 128):
            return
          var8 = -3
        Char.x = byte(charDxForward(var8 + 4))
        break
    controlUp = cast[sbyte](releaseArrows()) # disable automatic repeat
    seqtblOffsetChar(ord(seq4RunJump)) # run-jump


# sseg005:0BB5
proc backWithSword() =
  var
    frame: int16
  frame = int16(Char.frame)
  if (frame == ord(frame158StandWithSword) or frame == ord(
      frame170StandWithSword) or frame == ord(frame171StandWithSword)):
    controlBackward = 1 # disable automatic repeat
    seqtblOffsetChar(ord(seq57BackWithSword)) # back with sword


# seg005:0BE3
proc forwardWithSword() =
  var
    frame: int16
  frame = int16(Char.frame)
  if (frame == ord(frame158StandWithSword) or frame == ord(
      frame170StandWithSword) or frame == ord(frame171StandWithSword)):
    controlForward = 1 # disable automatic repeat
    if (Char.charid != ord(charid0Kid)):
      seqtblOffsetChar(ord(seq56GuardForwardWithSword)) # forward with sword (Guard)
    else:
      seqtblOffsetChar(ord(seq86ForwardWithSword)) # forward with sword (Kid)


# seg005:0C1D
proc drawSword() =
  var
    seqId: word
  seqId = ord(seq55DrawSword) # draw sword
  controlForward = cast[sbyte](releaseArrows())
  controlShift2 = controlForward
  when (FIX_UNINTENDED_SWORD_STRIKE):
    if (fixes.fixUnintendedSwordStrike != 0):
      ctrl1Shift2 = 1 # prevent restoring controlShift2 to -1 in restCtrl1()

  if (Char.charid == ord(charid0Kid)):
    playSound(ord(sound19DrawSword)) # taking out the sword
    offguard = 0
  elif (Char.charid != ord(charid1Shadow)):
    seqId = ord(seq90EnGarde) # stand active

  Char.sword = ord(sword2Drawn)
  seqtblOffsetChar(int16(seqId))


# seg005:0C67
proc controlWithSword() =
  var
    distance: int16
  if (Char.action < ord(actions2HangClimb)):
    if (getTileAtChar() == ord(tiles11Loose) or canGuardSeeKid >= 2):
      distance = int16(charOppDist())
      if (distance < 90):
        swordfight()
        return
      elif (distance < 0):
        if (distance < -4):
          seqtblOffsetChar(ord(seq60TurnWithSword)) # turn with sword (after switching places)
          return
        else:
          swordfight()
          return
  if (Char.charid == ord(charid0Kid) and Char.alive < 0):
    holdingSword = 0
  if (Char.charid < ord(charid2Guard)):
    # frame 171: stand with sword
    if (Char.frame == ord(frame171StandWithSword)):
      Char.sword = ord(sword0Sheathed)
      seqtblOffsetChar(ord(seq92PutSwordAway)) # put sword away (Guard died)
  else:
    swordfight()


# seg005:0CDB
proc swordfight() =
  var
    frame: int16
    seqId: int16
    charid: int16
  frame = int16(Char.frame)
  charid = int16(Char.charid)
  # frame 161: parry
  if (frame == ord(frame161Parry) and controlShift2 >= 0):
    seqtblOffsetChar(ord(seq57BackWithSword)) # back with sword (when parrying)
    return
  elif (controlShift2 < 0):
    if (charid == ord(charid0Kid)):
      kidSwordStrike = 15
    swordStrike()
    if (controlShift2 == 1):
      return
  if (controlDown < 0):
    if (frame == ord(frame158StandWithSword) or frame == ord(
        frame170StandWithSword) or frame == ord(frame171StandWithSword)):
      controlDown = 1 # disable automatic repeat
      Char.sword = ord(sword0Sheathed)
      if (charid == ord(charid0Kid)):
        offguard = 1
        guardRefrac = 9
        holdingSword = 0
        seqId = ord(seq93PutSwordAwayFast) # put sword away fast (down pressed)
      elif (charid == ord(charid1Shadow)):
        seqId = ord(seq92PutSwordAway) # put sword away
      else:
        seqId = ord(seq87GuardBecomeInactive) # stand inactive (when Kid leaves sight)
      seqtblOffsetChar(seqId)
  elif (controlUp < 0):
    parry()
  elif (controlForward < 0):
    forwardWithSword()
  elif (controlBackward < 0):
    backWithSword()


# seg005:0DB0
proc swordStrike() =
  var
    frame: int16
    seqId: int16
  frame = int16(Char.frame)
  if (frame == ord(frame157WalkWithSword) or # walk with sword
    frame == ord(frame158StandWithSword) or # stand with sword
    frame == ord(frame170StandWithSword) or # stand with sword
    frame == ord(frame171StandWithSword) or # stand with sword
    frame == ord(frame165WalkWithSword) # walk with sword
  ):
    if (Char.charid == ord(charid0Kid)):
      seqId = ord(seq75Strike) # strike with sword (Kid)
    else:
      seqId = ord(seq58GuardStrike) # strike with sword (Guard)
  elif (frame == ord(frame150Parry) or frame == ord(frame161Parry)): # parry
    seqId = ord(seq66StrikeAfterParry) # strike with sword after parrying
  else:
    return

  controlShift2 = 1 # disable automatic repeat
  seqtblOffsetChar(seqId)


# seg005:0E0F
proc parry() =
  var
    oppFrame: int16
    charFrame: int16
    var6: int16
    seqId: int16
    charCharid: int16
  charFrame = int16(Char.frame)
  oppFrame = int16(Opp.frame)
  charCharid = int16(Char.charid)
  seqId = ord(seq62Parry) # defend (parry) with sword
  var6 = 0
  if (
    charFrame == ord(frame158StandWithSword) or # stand with sword
    charFrame == ord(frame170StandWithSword) or # stand with sword
    charFrame == ord(frame171StandWithSword) or # stand with sword
    charFrame == ord(frame168Back) or # back?
    charFrame == ord(frame165WalkWithSword) # walk with sword
  ):
    if (charOppDist() >= 32 and charCharid != ord(charid0Kid)):
      backWithSword()
      return
    elif (charCharid == ord(charid0Kid)):
      if (oppFrame == ord(frame168Back)):
        return
      if (oppFrame != ord(frame151Strike1) and
        oppFrame != ord(frame152Strike2) and
        oppFrame != ord(frame162BlockToStrike)
      ):
        if (oppFrame == ord(frame153Strike3)): # strike
          var6 = 1
      else:
        if (charCharid != ord(charid0Kid)):
          backWithSword()
          return
    else:
      if (oppFrame != ord(frame152Strike2)):
        return
  else:
    if (charFrame != ord(frame167Blocked)):
      return
    seqId = ord(seq61ParryAfterStrike) # parry after striking with sword

  controlUp = 1 # disable automatic repeat
  seqtblOffsetChar(seqId)
  if (var6 != 0):
    playSeq()
