# data:1888
# var
#   seqtblOffsets: seq[word]

# seg005:000A
proc seqtblOffsetChar(seqIndex: int16) =
  Char.currSeq = seqtblOffsets[seqIndex]


# seg005:001D
proc seqtblOffsetOpp(seqIndex: int32) =
  Opp.currSeq = seqtblOffsets[seqIndex]


# seg005:0030
proc doFall() =
  if (isScreaming == 0 and Char.fallY >= 31):
    playSound(sound1Falling) # falling
    isScreaming = 1

  if (yLand[Char.currRow + 1] > int16(Char.y)):
    checkGrab()

    when (FIX_GLIDE_THROUGH_WALL):
      if (fixes.fixGlideThroughWall != 0):
        # Fix for the kid falling through walls after turning around while running (especially when weightless)
        determineCol()
        discard getTileAtChar()
        if (currTile2 == tiles20Wall or ((currTile2 == tiles12Doortop or
            currTile2 == tiles7DoortopWithFloor) and
            Char.direction == ord(dir_FFLeft))):
          var
            deltaX = distanceToEdgeWeight()
          #printf("deltaX = %d\n", deltaX)
          # When falling into a wall or doortop after turning or running, deltaX is likely to be either 8, 11 or 12
          # Calling inWall() here produces the desired fix (no glitching through wall), but only when deltaX >= 10
          # The code below "emulates" inWall() to always get the same effect as if deltaX == 10
          # (which distance actually looks/behaves best is a matter of preference)
          const
            deltaXReference = 10
          if (deltaX >= 8):
            deltaX = -5 + deltaX - deltaXReference
            Char.x = byte(charDxForward(deltaX))

            Char.fallX = 0 # not in inWall(), but we do need to cancel horizontal movement

  else:
    when (FIX_JUMP_THROUGH_WALL_ABOVE_GATE):
      if (fixes.fixJumpThroughWallAboveGate != 0):
        # At this point, Char.currCol has not yet been updated since checkBumped()
        # Basically, Char.currCol is still set to the column of the wall itself (even though the kid
        # may have 'bumped' against the wall, with Char.x being offset)

        # To prevent inWall() from being called we need to update Char.currCol here.
        # (inWall() only makes things worse, because it tries to 'eject' the kid from the wall the wrong way.
        # For this reason, the kid can end up behind a closed gate below, like is possible in Level 7)

        # The fix is calling determineCol() here.

        # One caveat: the kid can also land on the rightmost edge of a closed gate tile, when doing a running jump
        # to the left from two floors up. This 'trick' can be used in original PoP, but is 'fixed' by this change.
        # To still allow this trick to be possible, we can check that we not jumping into a gate tile.
        # (By the way, strangely enough, in unmodified PoP the trick even works with a tapestry + floor tile...)

        if (getTileAtChar() != tiles4Gate):
          determineCol()

    when (FIX_DROP_THROUGH_TAPESTRY):
      if (getTileAtChar() == tiles20Wall):
        inWall()
      elif ((fixes.fixDropThroughTapestry != 0) and getTileAtChar() ==
          tiles12Doortop and Char.direction == ord(dir_FFLeft)):
        if (distanceToEdgeWeight() >= 8): # only intervene if the kid is actually IN FRONT of the tapestry
          inWall()
    else:
      if (getTileAtChar() == tiles20Wall):
        inWall()

    if (tileIsFloor(currTile2) != 0):
      land()
    else:
      incCurrRow()


# seg005:0090
proc land() =
  var
    seqId: word
    loc5EE6: bool = false
  isScreaming = 0
  Char.y = byte(yLand[Char.currRow + 1])
  if (getTileAtChar() != tiles2Spike):
    when (FIX_LAND_AGAINST_GATE_OR_TAPESTRY):
      if ((tileIsFloor(getTileInfrontofChar()) != 0) and
          distanceToEdgeWeight() < 3):
        Char.x = byte(charDxForward(-3))
      elif (fixes.fixLandAgainstGateOrTapestry != 0):
        # A closed gate right in front of the landing spot should not behave like an open floor tile, but like a wall
        # Similar for a tapestry tile (with floor)
        discard getTileInfrontofChar()
        if (Char.direction == ord(dir_FFLeft) and (
                (currTile2 == tiles4Gate and (canBumpIntoGate() != 0)) or
                (currTile2 == tiles7DoortopWithFloor)) and
                    distanceToEdgeWeight() < 3
        ):
          Char.x = byte(charDxForward(-3))
    else:
      if ((tileIsFloor(getTileInfrontofChar()) == 0) and distanceToEdgeWeight() < 3):
        Char.x = byte(charDxForward(-3))
    startChompers()
  else:
    # fell on spikes
    loc5EE6 = true
  block myBlock:
    if (Char.alive < 0 or loc5EE6):
      # alive
      if ((distanceToEdgeWeight() >= 12 and getTileBehindChar() ==
          tiles2Spike) or getTileAtChar() == tiles2Spike) or loc5EE6:
        # fell on spikes
        when (FIX_SAFE_LANDING_ON_SPIKES):
          if (isSpikeHarmful() != 0):
            spiked()
            return
          elif ((fixes.fixSafeLandingOnSpikes != 0) and currRoomModif[
              currTilepos] == 0):
            # spikes ARE dangerous, just not out yet.
            spiked()
            return
        else:
          if (isSpikeHarmful() != 0):
            spiked()
            return
      if (Char.fallY < 22):
        # fell 1 row
        if (Char.charid >= ord(charid2Guard) or Char.sword == ord(sword2Drawn)):
          Char.sword = ord(sword2Drawn)
          seqId = ord(seq63GuardActiveAfterFall) # stand active after landing
        else:
          seqId = ord(seq17SoftLand) # crouch (soft land)
        if (Char.charid == ord(charid0Kid)):
          playSound(sound17SoftLand) # soft land (crouch)
          isGuardNotice = 1
      elif (Char.fallY < 33):
        # fell 2 rows
        if (Char.charid == ord(charid1Shadow)):
          if (Char.charid >= ord(charid2Guard) or Char.sword == ord(sword2Drawn)):
            Char.sword = ord(sword2Drawn)
            seqId = ord(seq63GuardActiveAfterFall) # stand active after landing
          else:
            seqId = ord(seq17SoftLand) # crouch (soft land)
          if (Char.charid == ord(charid0Kid)):
            playSound(sound17SoftLand) # soft land (crouch)
            isGuardNotice = 1
          break myBlock
        if (Char.charid == ord(charid2Guard)):
          discard takeHp(100)
          playSound(sound0FellToDeath)
          seqId = ord(seq22Crushed)
          break myBlock
        # kid (or skeleton (bug!))
        if (takeHp(1) == 0):
          # still alive
          playSound(sound16MediumLand) # medium land
          isGuardNotice = 1
          seqId = ord(seq20MediumLand) # medium land (lose 1 HP, crouch)
      else:
        # dead (this was the last HP)
        playSound(sound0FellToDeath)
        seqId = ord(seq22Crushed)
    else:
      # fell 3 or more rows
      discard takeHp(100)
      playSound(sound0FellToDeath)
      seqId = ord(seq22Crushed)
  seqtblOffsetChar(int16(seqId))
  playSeq()
  Char.fallY = 0


# seg005:01B7
proc spiked() =
  # If someone falls into spikes, those spikes become harmless (to others).
  currRoomModif[currTilepos] = 0xFF
  Char.y = byte(yLand[Char.currRow + 1])
  when(FIX_OFFSCREEN_GUARDS_DISAPPEARING):
    # a guard can get teleported to the other side of kid's room
    # when landing on spikes in another room
    if fixes.fixOffscreenGuardsDisappearing != 0'u8:
      var
        spikeCol: int16 = tileCol
      if int16(Char.room) != currRoom:
        if (currRoom == int16(level.roomlinks[Char.room - 1].right)):
          spikeCol += 10
        elif (currRoom == int16(level.roomlinks[Char.room - 1].left)):
          spikeCol -= 10
      Char.x = byte(xBump[spikeCol + FIRST_ONSCREEN_COLUMN] + 10)
    else:
      Char.x = byte(xBump[tileCol + FIRST_ONSCREEN_COLUMN] + 10)
  else:
    Char.x = byte(xBump[tileCol + FIRST_ONSCREEN_COLUMN] + 10)

  Char.fallY = 0
  playSound(sound48Spiked) # something spiked
  discard takeHp(100)
  seqtblOffsetChar(ord(seq51Spiked)) # spiked
  playSeq()


# seg005:0213
proc control() =
  var
    charFrame: int16
    charAction: int16
    # crouchAfterClimbing: bool = false
  charFrame = int16(Char.frame)
  if (Char.alive >= 0):
    if (charFrame == ord(frame15Stand) or # stand
      charFrame == ord(frame166StandInactive) or # stand
      charFrame == ord(frame158StandWithSword) or # stand with sword
      charFrame == ord(frame171StandWithSword) # stand with sword
    ):
      seqtblOffsetChar(ord(seq71Dying)) # dying (not stabbed)
  else:
    charAction = int16(Char.action)
    if (charAction == ord(actions5Bumped) or
      charAction == ord(actions4InFreefall)
    ):
      discard releaseArrows()
    elif (Char.sword == ord(sword2Drawn)):
      controlWithSword()
    elif (Char.charid >= ord(charid2Guard)):
      controlGuardInactive()
    elif (charFrame == ord(frame15Stand) or # standing
      (charFrame >= ord(frame50Turn) and charFrame < 53) # end of turning
    ):
      controlStanding()
    elif (charFrame == ord(frame48Turn)): # a frame in turning
      controlTurning()
    elif (charFrame < 4): # start run
      controlStartrun()
    elif (charFrame >= ord(frame67StartJumpUp1) and charFrame < ord(
        frame70Jumphang)): # start jump up
      controlJumpup()
    elif (charFrame < 15): # running
      controlRunning()
    elif (charFrame >= ord(frame87Hanging1) and charFrame < 100): # hanging
      controlHanging()
    elif (charFrame == ord(frame109Crouch)): # crouching
      controlCrouched()
    else:
      # crouchAfterClimbing = true

      when (ALLOW_CROUCH_AFTER_CLIMBING):
      # When ducking with down+forward, give time to release the forward control (prevents unintended crouch-hops)
        if fixes.enableCrouchAfterClimbing and ((
            fixes.enableCrouchAfterClimbing != 0) and Char.currSeq >=
            seqtblOffsets[ord(seq50Crouch)] and Char.currSeq < seqtblOffsets[
            ord(seq49StandUpFromCrouch)]): # while stooping
          if (controlForward != CONTROL_IGNORE):
            controlForward = CONTROL_RELEASED
      else:
        discard

    when (FIX_MOVE_AFTER_DRINK):
      if ((fixes.fixMoveAfterDrink != 0) and charFrame >= ord(frame191Drink) and
          charFrame <= ord(frame205Drink)):
        discard releaseArrows()

    when (FIX_MOVE_AFTER_SHEATHE):
      if ((fixes.fixMoveAfterSheathe != 0) and
          Char.currSeq >= seqtblOffsets[ord(seq92PutSwordAway)] and
          Char.currSeq < seqtblOffsets[ord(seq93PutSwordAwayFast)]
      ):
        discard releaseArrows()


# seg005:02EB
proc controlCrouched() =
  if (needLevel1Music != 0 and currentLevel == custom.introMusicLevel):
    # Special event: music when crouching
    if (checkSoundPlaying() == 0):
      if (needLevel1Music == 1):
        playSound(sound25Presentation) # presentation (level 1 start)
        needLevel1Music = 2
      else:
        when (USE_REPLAY):
          if (recording != 0):
            specialMove = ord(MOVE_EFFECT_END)
          if (replaying == 0): # during replays, crouch immobilization gets cancelled in doReplayMove()
            needLevel1Music = 0
        else:
          needLevel1Music = 0
  else:
    needLevel1Music = 0
    if (controlShift2 == CONTROL_HELD and (checkGetItem() != 0)):
      return
    if (controlY != CONTROL_HELD_DOWN):
      seqtblOffsetChar(ord(seq49StandUpFromCrouch)) # stand up from crouch
    else:
      if (controlForward == CONTROL_HELD):
        controlForward = CONTROL_IGNORE # disable automatic repeat
        seqtblOffsetChar(ord(seq79CrouchHop)) # crouch-hop


# seg005:0358
proc controlStanding() =
  if (controlShift2 == CONTROL_HELD and controlShift == CONTROL_HELD and (
      checkGetItem() != 0)):
    return

  if (Char.charid != ord(charid0Kid) and controlDown == CONTROL_HELD and
      controlForward == CONTROL_HELD):
    drawSword()
    return
#else
  if haveSword != 0:
    if (offguard != 0 and controlShift >= CONTROL_RELEASED):
      if (controlForward == CONTROL_HELD):
        if ((isKeyboardMode != 0) and controlUp == CONTROL_HELD):
          standingJump()
        else:
          forwardPressed()
      elif (controlBackward == CONTROL_HELD):
        backPressed()
      elif (controlUp == CONTROL_HELD):
        if ((isKeyboardMode != 0) and controlForward == CONTROL_HELD):
          standingJump()
        else:
          upPressed()
      elif (controlDown == CONTROL_HELD):
        downPressed()
      elif (controlX == CONTROL_HELD):
        forwardPressed()
      return
    if (canGuardSeeKid >= 2):
      let
        distance: int16 = int16(charOppDist())
      if (distance >= -10 and distance < 90):
        holdingSword = 1
        if (cast[word](distance) < cast[word](-6)):
          if (Opp.charid == ord(charid1Shadow) and
            (Opp.action == ord(actions3InMidair) or (Opp.frame >= ord(
                frame107FallLand1) and Opp.frame < 118))
          ):
            offguard = 0
          else:
            drawSword()
            return
        else:
          backPressed()
          return
    else:
      offguard = 0
  if (controlShift == CONTROL_HELD):
    if (controlBackward == CONTROL_HELD):
      backPressed()
    elif (controlUp == CONTROL_HELD):
      upPressed()
    elif (controlDown == CONTROL_HELD):
      downPressed()
    elif (controlX == CONTROL_HELD_FORWARD and controlForward == CONTROL_HELD):
      safeStep()
  elif (controlForward == CONTROL_HELD):
    if ((isKeyboardMode != 0) and controlUp == CONTROL_HELD):
      standingJump()
    else:
      forwardPressed()
  elif (controlBackward == CONTROL_HELD):
    backPressed()
  elif (controlUp == CONTROL_HELD):
    if ((isKeyboardMode != 0) and controlForward == CONTROL_HELD):
      standingJump()
    else:
      upPressed()
  elif (controlDown == CONTROL_HELD):
    downPressed()
  elif (controlX == CONTROL_HELD_FORWARD):
    forwardPressed()

var
  sourceModifier: int32
  sourceRoom: int32
  sourceTilepos: int32

# seg005:0482
proc upPressed() =
  # If there is an open level door nearby, enter it.
  var
    leveldoorTilepos: int32 = -1
  if (getTileAtChar() == tiles16LevelDoorLeft):
    leveldoorTilepos = int32(currTilepos)
  elif (getTileBehindChar() == tiles16LevelDoorLeft):
    leveldoorTilepos = int32(currTilepos)
  elif (getTileInfrontofChar() == tiles16LevelDoorLeft):
    leveldoorTilepos = int32(currTilepos)
  if ((leveldoorTilepos != -1) and word(level.startRoom) != drawnRoom and
    (if fixes.fixExitDoor != 0: currRoomModif[leveldoorTilepos] >=
        42 else: # this door must be fully open
      leveldoorOpen != 0)):
    goUpLeveldoor()
    return

  when USE_TELEPORTS:
    # If there is a teleport nearby, enter it.
    # (A teleport is a repurposed left half balcony with a non-zero modifier.)
    leveldoorTilepos = -1
    # THis detection is not perfect...
    if getTileAtChar() == tiles23BalconyLeft:
      leveldoorTilepos = int32(currTilepos)
    elif getTileBehindChar() == tiles23BalconyLeft:
      leveldoorTilepos = int32(currTilepos)
    elif getTileInfrontofChar() == tiles23BalconyLeft:
      leveldoorTilepos = int32(currTilepos)
    if leveldoorTilepos != -1:
      # We reuse pickup_obj_type for storing the identifier of the teleporter.
      pickupObjType = int16(currRoomModif[currTilepos])
      # Balconies with zero modifiers remain regular balconies.
      if pickupObjType > 0:
        sourceModifier = pickupObjType
        sourceRoom = currRoom
        sourceTilepos = int32(currTilepos)
        goUpLeveldoor()
        seqtblOffsetChar(ord(seqTeleport))
        return

  # Else just jump up.
  if (controlX == CONTROL_HELD_FORWARD):
    standingJump()
  else:
    checkJumpUp()


# seg005:04C7
proc downPressed() =
  controlDown = CONTROL_IGNORE # disable automatic repeat
  if ((tileIsFloor(getTileInfrontofChar()) == 0) and distanceToEdgeWeight() < 3):
    Char.x = byte(charDxForward(5))
    loadFramDetCol()
  else:
    if ((tileIsFloor(getTileBehindChar()) == 0) and distanceToEdgeWeight() >= 8):
      throughTile = getTileBehindChar()
      discard getTileAtChar()
      if ((canGrab() != 0) and (when (ALLOW_CROUCH_AFTER_CLIMBING): (not(
          (fixes.enableCrouchAfterClimbing != 0) and controlForward ==
          CONTROL_HELD)) else: true) and (Char.direction >= ord(dir0Right) or
              getTileAtChar() != tiles4Gate or currRoomModif[
              currTilepos] shr 2 >= 6)):
        Char.x = byte(charDxForward(distanceToEdgeWeight() - 9))
        seqtblOffsetChar(ord(seq68ClimbDown)) # climb down
      else:
        crouch()
    else:
      crouch()


# seg005:0574
proc goUpLeveldoor() =
  Char.x = byte(xBump[tileCol + 5] + 10)
  Char.direction = ord(dirFFLeft) # right
  seqtblOffsetChar(ord(seq70GoUpOnLevelDoor)) # go up on level door


# seg005:058F
proc controlTurning() =
  if (controlShift >= CONTROL_RELEASED and controlX == CONTROL_HELD_FORWARD and
      controlY >= CONTROL_RELEASED):
    when (FIX_TURN_RUN_NEAR_WALL):
      if (fixes.fixTurnRunningNearWall != 0):
        let
          distance = getEdgeDistance()
        # the same logic as the safe-step condition in the forward_pressed() method
        if (edgeType == EDGE_TYPE_WALL and currTile2 != tiles18Chomper and
            distance < 8):
          controlForward = CONTROL_HELD # ensures the value is not CONTROL_IGNORE since control_x == CONTROL_HELD_FORWARD
        else:
          seqtblOffsetChar(ord(seq43StartRunAfterTurn))
      else:
        seqtblOffsetChar(ord(seq43StartRunAfterTurn))
    else:
      seqtblOffsetChar(ord(seq43StartRunAfterTurn)) # start run and run (after turning)

  # Added:
  # When using a joystick, the kid may sometimes jump/duck/turn unintendedly after turning around.
  # To prevent this: clear the remembered controls, so that if the stick has already moved to another/neutral position,
  # the kid will not jump, duck, or turn again.
  if (isJoystMode != 0):
    if (controlUp == CONTROL_HELD and controlY >= CONTROL_RELEASED):
      controlUp = CONTROL_RELEASED

    if (controlDown == CONTROL_HELD and controlY <= CONTROL_RELEASED):
      controlDown = CONTROL_RELEASED

    if (controlBackward == CONTROL_HELD and controlX == CONTROL_RELEASED):
      controlBackward = CONTROL_RELEASED


# seg005:05AD
proc crouch() =
  seqtblOffsetChar(ord(seq50Crouch)) # crouch
  controlDown = releaseArrows()


# seg005:05BE
proc backPressed() =
  var
    seqId: word
  controlBackward = releaseArrows()
  # After turn, Kid will draw sword if ...
  if (haveSword == 0 or
      canGuardSeeKid < 2 or
      charOppDist() > 0 or
      distanceToEdgeWeight() < 2):
    seqId = ord(seq5Turn) # turn
  else:
    Char.sword = ord(sword2Drawn)
    offguard = 0
    seqId = ord(seq89TurnDrawSword) # turn and draw sword

  seqtblOffsetChar(int16(seqId))


# seg005:060F
proc forwardPressed() =
  let
    distance: int16 = int16(getEdgeDistance())
  when (ALLOW_CROUCH_AFTER_CLIMBING):
    if ((fixes.enableCrouchAfterClimbing != 0) and controlDown <
        CONTROL_RELEASED):
      downPressed()
      controlForward = CONTROL_RELEASED
      return

  if (edgeType == EDGE_TYPE_WALL and currTile2 != tiles18Chomper and
      distance < 8):
    # If char is near a wall, step instead of run.
    if (controlForward == CONTROL_HELD):
      safeStep()
  else:
    seqtblOffsetChar(ord(seq1StartRun)) # start run and run


# seg005:0649
proc controlRunning() =
  if (controlX == CONTROL_RELEASED and (Char.frame == ord(frame7Run) or
      Char.frame == ord(frame11Run))):
    controlForward = releaseArrows()
    seqtblOffsetChar(ord(seq13StopRun)) # stop run
  elif (controlX == CONTROL_HELD_BACKWARD):
    controlBackward = releaseArrows()
    seqtblOffsetChar(ord(seq6RunTurn)) # run-turn
  elif (controlY == CONTROL_HELD_UP and controlUp == CONTROL_HELD):
    runJump()
  elif (controlDown == CONTROL_HELD):
    controlDown = CONTROL_IGNORE # disable automatic repeat
    seqtblOffsetChar(ord(seq26CrouchWhileRunning)) # crouch while running


# seg005:06A8
proc safeStep() =
  let
    distance: int16 = int16(getEdgeDistance())
  controlShift2 = CONTROL_IGNORE # disable automatic repeat
  controlForward = CONTROL_IGNORE # disable automatic repeat

  if distance != 0:
    Char.repeat = 1
    seqtblOffsetChar(distance + 28) # 29..42: safe step to edge
  elif (edgeType != EDGE_TYPE_WALL and Char.repeat != 0):
    Char.repeat = 0
    seqtblOffsetChar(ord(seq44StepOnEdge)) # step on edge
  else:
    seqtblOffsetChar(ord(seq39SafeStep11)) # unsafe step (off ledge)


# seg005:06F0
proc checkGetItem(): int32 =
  if (getTileAtChar() == tiles10Potion or currTile2 == tiles22Sword):
    if tileIsFloor(getTileBehindChar()) == 0:
      return 0

    Char.x = byte(charDxForward(-14))
    loadFramDetCol()

  if (getTileInfrontofChar() == tiles10Potion or currTile2 == tiles22Sword):
    getItem()
    return 1

  return 0


# seg005:073E
proc getItem() =
  if (Char.frame != ord(frame109Crouch)): # crouching
    let
      distance: int16 = int16(getEdgeDistance())
    if (edgeType != EDGE_TYPE_FLOOR):
      Char.x = byte(charDxForward(distance))

    if (Char.direction >= ord(dir0Right)):
      Char.x = byte(charDxForward(int32(currTile2 == tiles10Potion) - 2))

    crouch()
  elif (currTile2 == tiles22Sword):
    doPickup(-1)
    seqtblOffsetChar(ord(seq91GetSword)) # get sword
  else: # potion
    doPickup(int32(currRoomModif[currTilepos] shr 3))
    seqtblOffsetChar(ord(seq78Drink)) # drink
    when (USE_COPYPROT):
      if (currentLevel == 15):
        for index in 0 ..< 14:
          # remove letter on potions level
          if (copyprotRoom[index] == word(currRoom) and copyprotTile[index] == currTilepos):
            copyprotRoom[index] = 0
            break


# seg005:07FF
proc controlStartrun() =
  if (controlY == CONTROL_HELD_UP and controlX == CONTROL_HELD_FORWARD):
    standingJump()


# seg005:0812
proc controlJumpup() =
  if (controlX == CONTROL_HELD_FORWARD or controlForward == CONTROL_HELD):
    standingJump()


# seg005:0825
proc standingJump() =
  controlUp = CONTROL_IGNORE
  controlForward = CONTROL_IGNORE # disable automatic repeat
  seqtblOffsetChar(ord(seq3StandingJump)) # standing jump


# seg005:0836
proc checkJumpUp() =
  controlUp = releaseArrows()
  throughTile = getTileAboveChar()
  discard getTileFrontAboveChar()
  if (canGrab() != 0):
    grabUpWithFloorBehind()
  else:
    throughTile = getTileBehindAboveChar()
    discard getTileAboveChar()
    if (canGrab() != 0):
      jumpUpOrGrab()
    else:
      jumpUp()


# seg005:087B
proc jumpUpOrGrab() =
  let
    distance: int16 = int16(distanceToEdgeWeight())
  if (distance < 6):
    jumpUp()
  elif (tileIsFloor(getTileBehindChar()) == 0):
    # There is not floor behind char.
    grabUpNoFloorBehind()
  else:
    # There is floor behind char, go back a bit.
    Char.x = byte(charDxForward(distance - TILE_SIZEX))
    loadFramDetCol()
    grabUpWithFloorBehind()


# seg005:08C7
proc grabUpNoFloorBehind() =
  discard getTileAboveChar()
  Char.x = byte(charDxForward(distanceToEdgeWeight() - 10))
  seqtblOffsetChar(ord(seq16JumpUpAndGrab)) # jump up and grab (no floor behind)


# seg005:08E6
proc jumpUp() =
  controlUp = releaseArrows()
  let
    distance: int16 = int16(getEdgeDistance())
  if (distance < 4 and edgeType == EDGE_TYPE_WALL):
    Char.x = byte(charDxForward(distance - 3))

  when (FIX_JUMP_DISTANCE_AT_EDGE):
  # When climbing up two floors, turning around and jumping upward, the kid falls down.
  # This fix makes the workaround of Trick 25 unnecessary.
    if ((fixes.fixJumpDistanceAtEdge != 0) and distance == 3 and edgeType ==
        EDGE_TYPE_CLOSER):
      Char.x = byte(charDxForward(-1))

  discard getTile(int32(Char.room), int32(getTileDivMod(backDeltaX(0) + int32(
      dxWeight()) - 6)), int32(Char.currRow - 1))
  if (currTile2 != tiles20Wall and (tileIsFloor(currTile2) == 0)):
    seqtblOffsetChar(ord(seq28JumpUpWithNothingAbove)) # jump up with nothing above
  else:
    seqtblOffsetChar(ord(seq14JumpUpIntoCeiling)) # jump up with wall or floor above


# seg005:0968
proc controlHanging() =
  if (Char.alive < 0):
    if (grabTimer == 0 and controlY == CONTROL_HELD):
      canClimbUp()
    elif (controlShift == CONTROL_HELD):
      # hanging against a wall or a doortop
      if (Char.action != ord(actions6HangStraight) and (getTileAtChar() ==
          tiles20Wall or (Char.direction == ord(dirFFLeft) and ( # facing left
        currTile2 == tiles7DoortopWithFloor or
        currTile2 == tiles12Doortop
      )))):
        if (grabTimer == 0):
          playSound(sound8Bumped) # touching a wall (hang against wall)
        seqtblOffsetChar(ord(seq25HangAgainstWall)) # hang against wall (straight)
      else:
        if (tileIsFloor(getTileAboveChar()) == 0):
          hangFall()
    else:
      hangFall()
  else:
    hangFall()


# seg005:09DF
proc canClimbUp() =
  var
    seqId: int16
  seqId = ord(seq10ClimbUp) # climb up
  controlUp = releaseArrows()
  controlShift2 = controlUp
  discard getTileAboveChar()
  if (((currTile2 == tiles13Mirror or currTile2 == tiles18Chomper) and
      Char.direction == ord(dir0Right)) or (currTile2 == tiles4Gate and
      Char.direction != ord(dir0Right) and currRoomModif[currTilepos] shr 2 < 6)
  ):
    seqId = ord(seq73ClimbUpToClosedGate) # climb up to closed gate and down

  seqtblOffsetChar(seqId)


# seg005:0A46
proc hangFall() =
  controlDown = releaseArrows()
  if ((tileIsFloor(getTileBehindChar()) == 0) and (tileIsFloor(getTileAtChar()) == 0)):
    seqtblOffsetChar(ord(seq23ReleaseLedgeAndFall)) # release ledge and fall
  else:
    if (getTileAtChar() == tiles20Wall or
      (Char.direction < ord(dir0Right) and ( # looking left
        currTile2 == tiles7DoortopWithFloor or
        currTile2 == tiles12Doortop
      ))
    ):
      Char.x = byte(charDxForward(-7))

    seqtblOffsetChar(ord(seq11ReleaseLedgeAndLand)) # end of climb down



# seg005:0AA8
proc grabUpWithFloorBehind() =
  let
    distance: int16 = int16(distanceToEdgeWeight())

  # The global variable edgeType (which we need!) gets set as a side effect of getEdgeDistance()
  var
    edgeDistance: int16 = int16(getEdgeDistance())
    jumpStraightCondition: bool
  #printf("Distance to edge weight: %d\tedge type: %d\tedge distance: %d\n", distance, edgeType, edgeDistance)

  when (FIX_EDGE_DISTANCE_CHECK_WHEN_CLIMBING):
  # When climbing to a higher floor, the game unnecessarily checks how far away the edge below is
  # This contributes to sometimes "teleporting" considerable distances when climbing from firm ground
    if (fixes.fixEdgeDistanceCheckWhenClimbing != 0):
      jumpStraightCondition = (distance < 4 and edgeType != EDGE_TYPE_WALL)
    else:
      jumpStraightCondition = (distance < 4 and edgeDistance < 4 and edgeType != EDGE_TYPE_WALL)
  else:
    jumpStraightCondition = distance < 4 and edgeDistance < 4

  if (jumpStraightCondition):
    Char.x = byte(charDxForward(distance))
    seqtblOffsetChar(ord(seq8JumpUpAndGrabStraight)) # jump up and grab (when?)
  else:
    Char.x = byte(charDxForward(distance - 4))
    seqtblOffsetChar(ord(seq24JumpUpAndGrabForward)) # jump up and grab (with floor behind)


# seg005:0AF7
proc runJump() =
  var
    xpos: int16
    col: int32
    posAdjustment: int16
  if (Char.frame >= ord(frame7Run)):
    # Align Kid to edge of floor.
    xpos = int16(charDxForward(4))
    col = getTileDivModM7(xpos)
    # Iterate through current tile and the next two tiles looking for a tile
    # the player should jump from
    for tilesForward in 0 ..< 2:
      col += dirFront[Char.direction + 1]
      discard getTile(int32(Char.room), col, int32(Char.currRow))
      if (currTile2 == tiles2Spike or (tileIsFloor(currTile2) == 0)):
        posAdjustment = int16(distanceToEdge(xpos)) + TILE_SIZEX * int16(
            tilesForward) - TILE_SIZEX
        if (posAdjustment < -8 or posAdjustment >= 2):
          if (posAdjustment < 128):
            return
          posAdjustment = -3
        Char.x = byte(charDxForward(posAdjustment + 4))
        break
    controlUp = releaseArrows() # disable automatic repeat
    seqtblOffsetChar(ord(seq4RunJump)) # run-jump


# sseg005:0BB5
proc backWithSword() =
  var
    frame: int16
  frame = int16(Char.frame)
  if (frame == ord(frame158StandWithSword) or frame == ord(
      frame170StandWithSword) or frame == ord(frame171StandWithSword)):
    controlBackward = CONTROL_IGNORE # disable automatic repeat
    seqtblOffsetChar(ord(seq57BackWithSword)) # back with sword


# seg005:0BE3
proc forwardWithSword() =
  var
    frame: int16
  frame = int16(Char.frame)
  if (frame == ord(frame158StandWithSword) or frame == ord(
      frame170StandWithSword) or frame == ord(frame171StandWithSword)):
    controlForward = CONTROL_IGNORE # disable automatic repeat
    if (Char.charid != ord(charid0Kid)):
      seqtblOffsetChar(ord(seq56GuardForwardWithSword)) # forward with sword (Guard)
    else:
      seqtblOffsetChar(ord(seq86ForwardWithSword)) # forward with sword (Kid)


# seg005:0C1D
proc drawSword() =
  var
    seqId: word
  seqId = ord(seq55DrawSword) # draw sword
  controlForward = releaseArrows()
  controlShift2 = controlForward
  when (FIX_UNINTENDED_SWORD_STRIKE):
    if (fixes.fixUnintendedSwordStrike != 0):
      ctrl1Shift2 = CONTROL_IGNORE # prevent restoring controlShift2 to -1 in restCtrl1()

  if (Char.charid == ord(charid0Kid)):
    playSound(sound19DrawSword) # taking out the sword
    offguard = 0
  elif (Char.charid != ord(charid1Shadow)):
    seqId = ord(seq90EnGarde) # stand active

  Char.sword = ord(sword2Drawn)
  seqtblOffsetChar(int16(seqId))


# seg005:0C67
proc controlWithSword() =
  if (Char.action < ord(actions2HangClimb)):
    if (getTileAtChar() == tiles11Loose or canGuardSeeKid >= 2):
      let
        distance: int16 = int16(charOppDist())
      if (cast[word](distance) < cast[word](90)):
        swordfight()
        return
      elif (distance < 0):
        if (distance < -4):
          seqtblOffsetChar(ord(seq60TurnWithSword)) # turn with sword (after switching places)
          return
        else:
          swordfight()
          return
  if (Char.charid == ord(charid0Kid) and Char.alive < 0):
    holdingSword = 0
  if (Char.charid < ord(charid2Guard)):
    # frame 171: stand with sword
    if (Char.frame == ord(frame171StandWithSword)):
      Char.sword = ord(sword0Sheathed)
      seqtblOffsetChar(ord(seq92PutSwordAway)) # put sword away (Guard died)
  else:
    swordfight()


# seg005:0CDB
proc swordfight() =
  var
    frame: int16
    seqId: int16
    charid: int16
  frame = int16(Char.frame)
  charid = int16(Char.charid)
  # frame 161: parry
  if (frame == ord(frame161Parry) and controlShift2 >= CONTROL_RELEASED):
    seqtblOffsetChar(ord(seq57BackWithSword)) # back with sword (when parrying)
    return
  elif (controlShift2 == CONTROL_HELD):
    if (charid == ord(charid0Kid)):
      kidSwordStrike = 15
    swordStrike()
    if (controlShift2 == CONTROL_IGNORE):
      return
  if (controlDown == CONTROL_HELD):
    if (frame == ord(frame158StandWithSword) or frame == ord(
        frame170StandWithSword) or frame == ord(frame171StandWithSword)):
      controlDown = CONTROL_IGNORE # disable automatic repeat
      Char.sword = ord(sword0Sheathed)
      if (charid == ord(charid0Kid)):
        offguard = 1
        guardRefrac = 9
        holdingSword = 0
        seqId = ord(seq93PutSwordAwayFast) # put sword away fast (down pressed)
      elif (charid == ord(charid1Shadow)):
        seqId = ord(seq92PutSwordAway) # put sword away
      else:
        seqId = ord(seq87GuardBecomeInactive) # stand inactive (when Kid leaves sight)
      seqtblOffsetChar(seqId)
  elif (controlUp == CONTROL_HELD):
    parry()
  elif (controlForward == CONTROL_HELD):
    forwardWithSword()
  elif (controlBackward == CONTROL_HELD):
    backWithSword()


# seg005:0DB0
proc swordStrike() =
  var
    frame: int16
    seqId: int16
  frame = int16(Char.frame)
  if (frame == ord(frame157WalkWithSword) or # walk with sword
    frame == ord(frame158StandWithSword) or # stand with sword
    frame == ord(frame170StandWithSword) or # stand with sword
    frame == ord(frame171StandWithSword) or # stand with sword
    frame == ord(frame165WalkWithSword) # walk with sword
  ):
    if (Char.charid == ord(charid0Kid)):
      seqId = ord(seq75Strike) # strike with sword (Kid)
    else:
      seqId = ord(seq58GuardStrike) # strike with sword (Guard)
  elif (frame == ord(frame150Parry) or frame == ord(frame161Parry)): # parry
    seqId = ord(seq66StrikeAfterParry) # strike with sword after parrying
  else:
    return

  controlShift2 = CONTROL_IGNORE # disable automatic repeat
  seqtblOffsetChar(seqId)


# seg005:0E0F
proc parry() =
  var
    oppFrame = int16(Opp.frame)
    charFrame = int16(Char.frame)
    doPlaySeq: int16
    seqId: int16
    charCharid = int16(Char.charid)
  seqId = ord(seq62Parry) # defend (parry) with sword
  if (
    charFrame == ord(frame158StandWithSword) or # stand with sword
    charFrame == ord(frame170StandWithSword) or # stand with sword
    charFrame == ord(frame171StandWithSword) or # stand with sword
    charFrame == ord(frame168Back) or # back?
    charFrame == ord(frame165WalkWithSword) # walk with sword
  ):
    if (charOppDist() >= 32 and charCharid != ord(charid0Kid)):
      backWithSword()
      return
    elif (charCharid == ord(charid0Kid)):
      if (oppFrame == ord(frame168Back)):
        return
      if (oppFrame != ord(frame151Strike1) and
        oppFrame != ord(frame152Strike2) and
        oppFrame != ord(frame162BlockToStrike)
      ):
        if (oppFrame == ord(frame153Strike3)): # strike
          doPlaySeq = 1
      else:
        if (charCharid != ord(charid0Kid)):
          backWithSword()
          return
    else:
      if (oppFrame != ord(frame152Strike2)):
        return
  else:
    if (charFrame != ord(frame167Blocked)):
      return
    seqId = ord(seq61ParryAfterStrike) # parry after striking with sword

  controlUp = CONTROL_IGNORE # disable automatic repeat
  seqtblOffsetChar(seqId)
  if (doPlaySeq != 0):
    playSeq()

when USE_TELEPORTS:
  proc teleporter() =
    var
      found: bool = false
      destRoom: int
      destTilepos: int

    for i in 0..29:
      if destRoom == sourceRoom and i == sourceTilepos:
        continue

      # The pair is a balcony tile with the same modifier.
      if getCurrTile(int16(i)) == tiles23BalconyLeft and
          int32(currModifier) == sourceModifier:
        found = true
        destTilepos = i
        break

    if found:
      # We found a pair. Put the kid there.
      # Based on doStartpos().
      Char.room = byte(destRoom)
      Char.currCol = sbyte(destTilepos mod 10)
      Char.currRow = sbyte(destTilepos div 10)
      # Center on the destination teleport.
      Char.x = xBump[Char.currCol + 5] + 14 + 7
      Char.y = byte(yLand[Char.currRow + 1])
      nextRoom = Char.room
      # Without this, the prince will sometimes end up at the wrong place.
      clearCollRooms()
      when FIX_DISAPPEARING_GUARD_B:
        if nextRoom != drawnRoom:
          leaveGuard()
      else:
        leaveGuard()
      when FIX_DISAPPEARING_GUARD_A:
        if nextRoom == drawnRoom:
          drawnRoom = 0
      seqtblOffsetChar(ord(seq5Turn))
      playSound(sound45JumpThroughMirror)
    else:
      # No pair found
      # showDialog("Error: THis teleport has no pair.")
      showDialog("Error: There is no other teleport with modifier " & $(
          pickupObjType) & ".")
      Char.x = xBump[Char.currCol + 5] + 14
      Char.y = byte(yLand[Char.currRow + 1])
      seqtblOffsetChar(ord(seq17SoftLand))
      playSound(sound0FellToDeath)
