# seg007:0000
proc processTrobs() =
  var
    needDelete: word
    newIndex: word
  needDelete = 0
  if (trobsCount == 0):
    return
  for index in 0 ..< trobsCount:
    trob = trobs[index]
    animateTile()
    trobs[index].`type` = trob.`type`
    if (trob.`type` < 0):
      needDelete = 1


  if needDelete != 0:
    newIndex = 0
    for index in 0 ..< trobsCount:
      if (trobs[index].`type` >= 0):
        trobs[newIndex] = trobs[index]
        inc(newIndex)


    trobsCount = int16(newIndex)


# seg007:00AF
proc animateTile() =
  getRoomAddress(int(trob.room))
  case (getCurrTile(int16(trob.tilepos))):
  of ord(tiles19Torch), ord(tiles30TorchWithDebris):
    animateTorch()
  of ord(tiles6Closer), ord(tiles15Opener):
    animateButton()
  of ord(tiles2Spike):
    animateSpike()
  of ord(tiles11Loose):
    animateLoose()
  of ord(tiles0Empty):
    animateEmpty()
  of ord(tiles18Chomper):
    animateChomper()
  of ord(tiles4Gate):
    animateDoor()
  of ord(tiles16LevelDoorLeft):
    animateLeveldoor()
  of ord(tiles10Potion):
    animatePotion()
  of ord(tiles22Sword):
    animateSword()
  else:
    trob.`type` = -1

  currRoomModif[trob.tilepos] = currModifier


# seg007:0166
proc isTrobInDrawnRoom(): int16 =
  if (word(trob.room) != drawnRoom):
    trob.`type` = -1
    return 0
  else:
    return 1



# seg007:017E
proc setRedrawAnimRight() =
  setRedrawAnim(getTrobRightPosInDrawnRoom(), 1)


# seg007:018C
proc setRedrawAnimCurr() =
  setRedrawAnim(getTrobPosInDrawnRoom(), 1)


# seg007:019A
proc redrawAtTrob() =
  var
    tilepos: word
  redrawHeight = 63
  tilepos = word(getTrobPosInDrawnRoom())
  setRedrawFull(int16(tilepos), 1)
  setWipe(int16(tilepos), 1)


# seg007:01C5
proc redraw21h() =
  redrawHeight = 0x21
  redrawTileHeight()


# seg007:01D0
proc redraw11h() =
  redrawHeight = 0x11
  redrawTileHeight()


# seg007:01DB
proc redraw20h() =
  redrawHeight = 0x20
  redrawTileHeight()


# seg007:01E6
proc drawTrob() =
  var
    var2: word
  var2 = word(getTrobRightPosInDrawnRoom())
  setRedrawAnim(int16(var2), 1)
  setRedrawFore(int16(var2), 1)
  setRedrawAnim(getTrobRightAbovePosInDrawnRoom(), 1)


# seg007:0218
proc redrawTileHeight() =
  var
    tilepos: int16
  tilepos = int16(getTrobPosInDrawnRoom())
  setRedrawFull(tilepos, 1)
  setWipe(tilepos, 1)
  tilepos = int16(getTrobRightPosInDrawnRoom())
  setRedrawFull(tilepos, 1)
  setWipe(tilepos, 1)


# seg007:0258
proc getTrobPosInDrawnRoom(): int16 =
  var
    tilepos: int16
  tilepos = int16(trob.tilepos)
  if (word(trob.room) == room_A):
    if (tilepos >= 20 and tilepos < 30):
      # 20..29 . -1..-10
      tilepos = 19 - tilepos
    else:
      tilepos = 30
  else:
    if (word(trob.room) != drawnRoom):
      tilepos = 30
  return tilepos


# seg007:029D
proc getTrobRightPosInDrawnRoom(): int16 =
  var
    tilepos: word
  tilepos = trob.tilepos
  if (word(trob.room) == drawnRoom):
    if (tilepos mod 10 != 9):
      inc(tilepos)
    else:
      tilepos = 30
  elif (word(trob.room) == room_L):
    if (tilepos mod 10 == 9):
      tilepos -= 9
    else:
      tilepos = 30
  elif (word(trob.room) == room_A):
    if (tilepos >= 20'u16 and tilepos < 29'u16):
      # 20..28 . -2..-10
      tilepos = 18'u16 - tilepos
    else:
      tilepos = 30
  elif (word(trob.room) == room_AL and tilepos == 29):
    tilepos = high(typeof(tilepos))
  else:
    tilepos = 30

  return cast[int16](tilepos)


# seg007:032C
proc getTrobRightAbovePosInDrawnRoom(): int16 =
  var
    tilepos: int16
  tilepos = int16(trob.tilepos)
  if (word(trob.room) == drawnRoom):
    if (tilepos mod 10 != 9):
      if (tilepos < 10):
        # 0..8 . -2..-10
        tilepos = -(tilepos + 2)
      else:
        tilepos -= 9
    else:
      tilepos = 30
  elif (word(trob.room) == roomL):
    if (tilepos == 9):
      tilepos = -1
    else:
      if (tilepos mod 10 == 9):
        tilepos -= 19
      else:
        tilepos = 30
  elif (word(trob.room) == roomB):
    if (tilepos < 9):
      tilepos += 21
    else:
      tilepos = 30
  elif (word(trob.room) == roomBL and tilepos == 9):
    tilepos = 20
  else:
    tilepos = 30

  return int16(tilepos)


# seg007:03CF
proc animateTorch() =
  #if (isTrobInDrawnRoom()):
  # Keep animating torches in the rightmost column of the left-side room as well, because they are visible in the current room.
  if (word(trob.room) == drawnRoom or (word(trob.room) == room_L and (
      trob.tilepos mod 10) == 9)):
    currModifier = byte(getTorchFrame(int16(currModifier)))
    setRedrawAnimRight()
  else:
    trob.`type` = -1


# seg007:03E9
proc animatePotion() =
  var
    `type`: word
  if (trob.`type` >= 0 and (isTrobInDrawnRoom() != 0)):
    `type` = word(currModifier and 0xF8)
    currModifier = byte(word(bubbleNextFrame(int16(currModifier)) and 0x07) or `type`)
    when (FIX_LOOSE_NEXT_TO_POTION):
      redrawAtTrob()
    else:
      setRedrawAnimCurr()


# seg007:0425
proc animateSword() =
  if isTrobInDrawnRoom() != 0:
    dec(currModifier)
    if (currModifier == 0):
      currModifier = byte( (prandom(255) and 0x3F) + 0x28)
      when (FIX_LOOSE_NEXT_TO_POTION):
        redrawAtTrob()
      else:
        setRedrawAnimCurr()


# seg007:0448
proc animateChomper() =
  var
    blood: word
    frame: word
  if (trob.`type` >= 0):
    blood = word(currModifier and 0x80)
    frame = word( (currModifier and 0x7F) + 1)
    if (frame > custom.chomperSpeed): # 15'u16
      frame = 1

    currModifier = byte(blood or frame)
    if (frame == 2):
      playSound(ord(sound47Chomper)) # chomper

    # If either:
    # - Kid left this room
    # - Kid left this row
    # - Kid died but not in this chomper
    # and chomper is past frame 6
    # then stop.
    if ((word(trob.room) != drawnRoom or trob.tilepos div 10 != byte(
        Kid.currRow) or
      (Kid.alive >= 0 and blood == 0)) and (currModifier and 0x7F) >= 6
    ):
      trob.`type` = -1


  if ((currModifier and 0x7F) < 6):
    redrawAtTrob()


# seg007:04D3
proc animateSpike() =
  if (trob.`type` >= 0):
    # 0xFF means a disabled spike.
    if (currModifier == 0xFF):
      return
    if (currModifier and 0x80) != 0:
      dec(currModifier)
      if (currModifier and 0x7F) != 0:
        return
      currModifier = 6
    else:
      inc(currModifier)
      if (currModifier == 5):
        currModifier = 0x8F
      elif (currModifier == 9):
        currModifier = 0
        trob.`type` = -1
  redraw21h()


const
  # data:27B2
  gateCloseSpeeds: array[9, byte] = [0'u8, 0'u8, 0'u8, 20'u8, 40'u8, 60'u8,
      80'u8, 100'u8, 120'u8]
  # data:27C0
  # Note: in C -1'u8 == 255
  doorDelta: array[3, byte] = [high(byte), 4'u8, 4'u8]

# seg007:0522
proc animateDoor() =
# /*
# Possible values of animType:
# 0: closing
# 1: open
# 2: permanent open
# 3,4,5,6,7,8: fast closing with speeds 20,40,60,80,100,120 /4 pixel/frame
# */
  var
    animType: sbyte
  animType = trob.`type`
  if (animType >= 0):
    if (animType >= 3):
      # closing fast
      if (animType < 8):
        inc(animType)
        trob.`type` = animType
      var
        newMod: int16 = int16(currModifier) - int16(gateCloseSpeeds[animType])
      currModifier = cast[byte](newMod)
      #if ((sbyte)currModifier < 0):
      if (newMod < 0):
      #if ((currModifier -= gateCloseSpeeds[animType]) < 0):
        currModifier = 0
        trob.`type` = -1
        playSound(ord(sound6GateClosingFast)) # gate closing fast
    else:
      if (currModifier != 0xFF):
        # 0xFF means permanently open.
        currModifier += doorDelta[animType]
        if (animType == 0):
          # closing
          if (currModifier != 0):
            if (currModifier < 188):
              if ((currModifier and 3) == 3):
                playDoorSoundIfVisible(ord(sound4GateClosing)) # gate closing
          else:
            gateStop()
        else:
          # opening
          if (currModifier < 188):
            if ((currModifier and 7) == 0):
              playSound(ord(sound5GateOpening)) # gate opening
          else:
            # stop
            if (animType < 2):
              # after regular open
              currModifier = 238
              trob.`type` = 0 # closing
              playSound(ord(sound7GateStop)) # gate stop (after opening)
            else:
              # after permanent open
              currModifier = 0xFF # keep open
              gateStop()
      else:
        gateStop()
  drawTrob()


# seg007:05E3
proc gateStop() =
  trob.`type` = -1
  playDoorSoundIfVisible(ord(sound7GateStop)) # gate stop (after closing)

const
  # data:27B8
  leveldoorCloseSpeeds: array[5, byte] = [0'u8, 5'u8, 17'u8, 99'u8, 0'u8]
# seg007:05F1
proc animateLeveldoor() =
# /*
# Possible values of trobType:
# 0: open
# 1: open (with button)
# 2: open
# 3,4,5,6: fast closing with speeds 0,5,17,99 pixel/frame
# */
  var
    trobType: word
  trobType = word(trob.`type`)
  if (trob.`type` >= 0):
    if (trobType >= 3'u16):
      # closing
      inc(trob.`type`)
      currModifier -= leveldoorCloseSpeeds[trob.`type` - 3]
      if (cast[sbyte](currModifier) < 0):
        currModifier = 0
        trob.`type` = -1
        playSound(ord(sound14LeveldoorClosing)) # level door closing
      else:
        if (trob.`type` == 4 and
          ((soundFlags and ord(sfDigi)) != 0)
        ):
          soundInterruptible[ord(sound15LeveldoorSliding)] = 1
          playSound(ord(sound15LeveldoorSliding)) # level door sliding (closing)
    else:
      # opening
      inc(currModifier)
      if (currModifier >= 43):
        trob.`type` = -1
        when (FIX_FEATHER_INTERRUPTED_BY_LEVELDOOR):
          if (word(fixes.fixFeatherInterruptedByLeveldoor) and isFeatherFall) == 0:
            stopSounds()
        else:
          stopSounds()
        if (leveldoorOpen == 0 or leveldoorOpen == 2):
          leveldoorOpen = 1
          if (currentLevel == custom.mirrorLevel):
            # Special event: place mirror
            discard getTile(int(custom.mirrorRoom), int32(custom.mirrorColumn),
                int32(custom.mirrorRow))
            currRoomTiles[currTilepos] = custom.mirrorTile
      else:
        soundInterruptible[ord(sound15LeveldoorSliding)] = 0
        playSound(ord(sound15LeveldoorSliding)) # level door sliding (opening)
  setRedrawAnimRight()


# seg007:06AD
proc bubbleNextFrame(curr: int16): int16 =
  var
    next: int16
  next = curr + 1
  if (next >= 8):
    next = 1
  return next


# seg007:06CD
proc getTorchFrame(curr: int16): int16 =
  var
    next: int16
  next = int16(prandom(255))
  if (next != curr):
    if (next < 9):
      return next
    else:
      next = curr
  inc(next)
  if (next >= 9):
    next = 0
  return next


# seg007:070A
proc setRedrawAnim(tilepos: int16, frames: byte) =
  var
    tileposE: int16 = tilepos
  if (tileposE < 30):
    if (tileposE < 0):
      inc(tileposE)
      redrawFramesAbove[-tileposE] = frames
      # or simply: ~tileposE
    else:
      redrawFramesAnim[tileposE] = frames


# seg007:0738
proc setRedraw2(tilepos: int16, frames: byte) =
  var
    tileposE: int16 = tilepos
  if (tileposE < 30):
    if (tileposE < 0):
      # trying to draw a mob at a negative tileposE, in the range -1 .. -10
      # used e.g. when the kid is climbing up to the room above
      # however, loose tiles falling out of the room end up with a negative tileposE {-2 .. -11} !
      tileposE = (-tileposE) - 1
      if (tileposE > 9):
        tileposE = 9 # prevent array index out of bounds!
      redrawFramesAbove[tileposE] = frames
    else:
      redrawFrames2[tileposE] = frames


# seg007:0766
proc setRedrawFloorOverlay(tilepos: int16, frames: byte) =
  var
    tileposE: int16 = tilepos
  if (tileposE < 30):
    if (tileposE < 0):
      inc(tileposE)
      redrawFramesAbove[-tileposE] = frames
      # or simply: ~tileposE
    else:
      redrawFramesFloorOverlay[tileposE] = frames


# seg007:0794
proc setRedrawFull(tilepos: int16, frames: byte) =
  var
    tileposE: int16 = tilepos
  if (tileposE < 30):
    if (tileposE < 0):
      inc(tileposE)
      redrawFramesAbove[-tileposE] = frames
      # or simply: ~tileposE
    else:
      redrawFramesFull[tileposE] = frames


# seg007:07C2
proc setRedrawFore(tilepos: int16, frames: byte) =
  if (tilepos < 30 and tilepos >= 0):
    redrawFramesFore[tilepos] = frames


# seg007:07DF
proc setWipe(tilepos: int16, frames: byte) =
  if (tilepos < 30 and tilepos >= 0):
    if (wipeFrames[tilepos] != 0):
      redrawHeight = max(wipeHeights[tilepos], redrawHeight)

    wipeHeights[tilepos] = sbyte(redrawHeight)
    wipeFrames[tilepos] = frames


# seg007:081E
proc startAnimTorch(room, tilepos: int16) =
  currRoomModif[tilepos] = byte(prandom(8))
  addTrob(byte(room), byte(tilepos), 1)


# seg007:0847
proc startAnimPotion(room, tilepos: int16) =
  currRoomModif[tilepos] = currRoomModif[tilepos] and 0xF8'u8
  currRoomModif[tilepos] = byte(word(currRoomModif[tilepos]) or prandom(6) + 1)
  addTrob(byte(room), byte(tilepos), 1)


# seg007:087C
proc startAnimSword(room, tilepos: int16) =
  currRoomModif[tilepos] = byte(prandom(0xFF) and 0x1F)
  addTrob(byte(room), byte(tilepos), 1)


# seg007:08A7
proc startAnimChomper(room, tilepos: int16, modifier: byte) =
  var
    oldModifier: int16
  oldModifier = int16(currRoomModif[tilepos])
  if (oldModifier == 0 or oldModifier >= 6):
    currRoomModif[tilepos] = modifier
    addTrob(byte(room), byte(tilepos), 1)


# seg007:08E3
proc startAnimSpike(room, tilepos: int16) =
  var
    oldModifier: sbyte
  oldModifier = cast[sbyte](currRoomModif[tilepos])
  if (oldModifier <= 0):
    if (oldModifier == 0):
      addTrob(byte(room), byte(tilepos), 1)
      playSound(ord(sound49Spikes)) # spikes
    else:
      # 0xFF means a disabled spike.
      if (oldModifier != 0xFF'i8):
        currRoomModif[tilepos] = 0x8F


# # seg007:092C
proc triggerGate(room, tilepos, buttonType: int16): int16 =
  var
    modifier: byte
  modifier = currRoomModif[tilepos]
  if (buttonType == ord(tiles15Opener)):
    # If the gate is permanently open, don't to anything.
    if (modifier == 0xFF):
      return -1
    if (modifier >= 188): # if it's already open
      currRoomModif[tilepos] = 238 # keep it open for a while
      return -1
    currRoomModif[tilepos] = (modifier + 3'u8) and 0xFC'u8
    return 1 # regular open
  elif (buttonType == ord(tiles14Debris)):
    # If it's not fully open:
    if (modifier < 188):
      return 2 # permanent open
    currRoomModif[tilepos] = 0xFF # keep open
    return -1
  else:
    if (modifier != 0):
      return 3 # close fast
    else:
      # already closed
      return -1


# seg007:0999
proc trigger1(targetType, room, tilepos, buttonType: int16): int16 =
  result = -1
  if (targetType == ord(tiles4Gate)):
    result = triggerGate(room, tilepos, buttonType)
  elif (targetType == ord(tiles16LevelDoorLeft)):
    if (currRoomModif[tilepos] != 0):
      result = -1
    else:
      result = 1
  elif custom.allowTriggeringAnyTile != 0: #allowTriggeringAnyTile hack
    result = 1


# seg007:09E5
proc doTriggerList(index, buttonType: int16) =
  var
    room: word
    tilepos: word
    targetType: byte
    triggerResult: sbyte
    indexI: int16 = index
#  while (doorlink1Ad[index] != -1) { # these can't be equal!
  while true: # Same as the above but just a little faster and no compiler warning.
    room = word(getDoorlinkRoom(indexI))
    getRoomAddress(int16(room))
    tilepos = word(getDoorlinkTile(indexI))
    targetType = byte(currRoomTiles[tilepos] and 0x1F)
    triggerResult = cast[sbyte](trigger1(int16(targetType), int16(room), int16(
        tilepos), buttonType))
    if (triggerResult >= 0):
      addTrob(byte(room), byte(tilepos), triggerResult)

    if (getDoorlinkNext(indexI) == 0):
      break
    inc(indexI)


# seg007:0A5A
proc addTrob(room, tilepos: byte, `type`: sbyte) =
  var
    found: int16
  if (trobsCount >= 30):
    showDialog("Trobs Overflow")
    return # added

  trob.room = room
  trob.tilepos = tilepos
  trob.`type` = `type`
  found = findTrob()
  if (found == -1):
    # add new
    if (trobsCount == 30):
      return
    trobs[trobsCount] = trob
    inc(trobsCount)
  else:
    # change existing
    trobs[found].`type` = trob.`type`



# seg007:0ACA
proc findTrob(): int16 =
  for index in 0'i16 ..< trobsCount:
    if (trobs[index].tilepos == trob.tilepos and
        trobs[index].room == trob.room):
      return index

  return -1


# seg007:0B0A
proc clearTileWipes() =
  for i in 0..29:
    redrawFramesFull[i] = 0
    wipeFrames[i] = 0
    wipeHeights[i] = 0
    redrawFramesAnim[i] = 0
    redrawFramesFore[i] = 0
    redrawFrames2[i] = 0
    redrawFramesFloorOverlay[i] = 0
    tileObjectRedraw[i] = 0

  for i in 0..9:
    redrawFramesAbove[i] = 0


# seg007:0BB6
proc getDoorlinkTimer(index: int16): int16 =
  return int16(doorlink2Ad[index] and 0x1F)


# seg007:0BCD
proc setDoorlinkTimer(index: int16, value: byte): int16 =
  doorlink2Ad[index] = byte(doorlink2Ad[index] and 0xE0)
  doorlink2Ad[index] = byte(doorlink2Ad[index] or (value and 0x1F))
  return int16(doorlink2Ad[index])


# seg007:0BF2
proc getDoorlinkTile(index: int16): int16 =
  return int16(doorlink1Ad[index] and 0x1F)


# seg007:0C09
proc getDoorlinkNext(index: int16): int16 =
  return int16((doorlink1Ad[index] and 0x80) == 0)


# seg007:0C26
proc getDoorlinkRoom(index: int16): int16 =
  return int16(((doorlink1Ad[index] and 0x60) shr 5) +
    ((doorlink2Ad[index] and 0xE0) shr 3))


# seg007:0C53
proc triggerButton(playsound, buttonType, modifier: int32) =
  var
    linkTimer: sbyte
    modifierI: int32 = modifier
    buttontypeI: int32 = buttonType
  discard getCurrTile(int16(currTilepos))
  if (buttonTypeI == 0):
    # 0 means currently selected
    buttonTypeI = int32(currTile)

  if (modifierI == -1):
    # -1 means currently selected
    modifierI = int32(currmodifier)

  linkTimer = cast[sbyte](getDoorlinkTimer(int16(modifierI)))
  # is the event jammed?
  if (linkTimer != 0x1F):
    discard setDoorlinkTimer(int16(modifierI), 5)
    if (linkTimer < 2):
      addTrob(byte(currRoom), byte(currTilepos), 1)
      redraw11h()
      isGuardNotice = 1
      if playsound != 0:
        playSound(ord(sound3ButtonPressed)) # button pressed

    doTriggerList(int16(modifierI), int16(buttonTypeI))


# seg007:0CD9
proc diedOnButton() =
  var
    buttonType: word
    modifier: word
  buttonType = word(getCurrTile(int16(currTilepos)))
  modifier = currModifier
  if (currTile == ord(tiles15Opener)):
    currRoomTiles[currTilepos] = ord(tiles1Floor)
    currRoomModif[currTilepos] = 0
    buttonType = ord(tiles14Debris) # force permanent open
  else:
    currRoomTiles[currTilepos] = ord(tiles5Stuck)

  triggerButton(1, int32(buttonType), int32(modifier))


# seg007:0D3A
proc animateButton() =
  var
    var2: word
  if (trob.`type` >= 0):
    var2 = word(getDoorlinkTimer(int16(currModifier)) - 1)
    discard setDoorlinkTimer(int16(currModifier), byte(var2))
    if (var2 < 2):
      trob.`type` = -1
      redraw11h()


# seg007:0D72
proc startLevelDoor(room, tilepos: int16) =
  currRoomModif[tilepos] = 43 # start fully open
  addTrob(byte(room), byte(tilepos), 3)


# seg007:0D93
proc animateEmpty() =
  trob.`type` = -1
  redraw20h()


const
# data:2284
  yLooseLand: array[5, word] = [2'u16, 65'u16, 128'u16, 191'u16, 254'u16]
# seg007:0D9D
proc animateLoose() =
  var
    room: word
    row: word
    tilepos: word
    animType: int16
  animType = trob.`type`
  if (animType >= 0):
    inc(currModifier)
    if (currModifier and 0x80) != 0:
      # just shaking
      # don't stop on level 13, needed for the auto-falling floors
      if (currentLevel == 13):
        return
      if currModifier >= 0x84:
        currModifier = 0
        trob.`type` = -1
      looseShake(int(not(currModifier)))
    else:
      # something is on the floor
      # should it fall already?
      if (currModifier >= custom.looseFloorDelay):
        room = word(trob.room)
        tilepos = word(trob.tilepos)
        currModifier = byte(removeLoose(int(room), int32(tilepos)))
        trob.`type` = -1
        curmob.xh = byte( (tilepos mod 10) shl 2)
        row = tilepos div 10
        curmob.y = byte(yLooseLand[row + 1])
        curmob.room = byte(room)
        curmob.speed = 0
        curmob.`type` = 0
        curmob.row = byte(row)
        addMob()
      else:
        looseShake(0)
  redraw20h()


const
  # data:2734
  looseSound: array[12, byte] = [0'u8, 1'u8, 1'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8,
      1'u8, 0'u8, 0'u8, 0'u8]
# seg007:0E55
proc looseShake(arg0: int32) =
  var
    soundId: word
  if (arg0 or int32(looseSound[currModifier and 0x7F])) != 0:
    soundId = prandom(2) + ord(sound20LooseShake1)
    while(soundId == lastLooseSound):
      soundId = prandom(2) + ord(sound20LooseShake1)
      # Sounds 20,21,22: loose floor shaking

    when(USE_REPLAY):
      # Skip this prandom call if we are replaying, and the replay file was made with an old version of SDLPoP (which didn't have this call).
      if (not((replaying != 0) and (gDeprecationNumber < 2))):
        discard prandom(2) # For vanilla pop compatibility, an RNG cycle is wasted here
        # Note: In DOS PoP, it's wasted a few lines below.
    else:
      discard prandom(2) # For vanilla pop compatibility, an RNG cycle is wasted here
      # Note: In DOS PoP, it's wasted a few lines below.

    if (soundFlags and ord(sfDigi)) != 0:
      lastLooseSound = soundId
      # random sample rate (10500..11500)
      #soundPointers[soundId].samplerate = prandom(1000) + 10500
    playSound(int(soundId))


# seg007:0EB8
proc removeLoose(room, tilepos: int32): int32 =
  currRoomTiles[tilepos] = ord(tiles0Empty)
  # note: the level type is used to determine the modifier of the empty space left behind
  return int32(custom.tblLevelType[currentLevel])


# seg007:0ED5
proc makeLooseFall(modifier: byte) =
  # is it a "solid" loose floor?
  if ((currRoomTiles[currTilepos] and 0x20) == 0):
    if (cast[sbyte](currRoomModif[currTilepos]) <= 0):
      currRoomModif[currTilepos] = modifier
      addTrob(byte(currRoom), currTilepos, 0)
      redraw20h()


# seg007:0F13
proc startChompers() =
  var
    timing: int16
    modifier: int16
    tilepos: int16
    column: int16
  timing = 15
  if (cast[byte](Char.currRow) < 3):
    getRoomAddress(int(Char.room))
    tilepos = int16(tblLine[Char.currRow])
    for column in 0 ..< 10:
      if (getCurrTile(tilepos) == ord(tiles18Chomper)):
        modifier = int16(currModifier and 0x7F)
        if (modifier == 0 or modifier >= 6):
          startAnimChomper(int16(Char.room), tilepos, byte(timing) or byte(
              currModifier and 0x80))
          timing = int16(nextChomperTiming(byte(timing)))
      inc(tilepos)


# seg007:0F9A
proc nextChomperTiming(timing: byte): int32 =
  # 15,12,9,6,13,10,7,14,11,8,repeat
  var
    timingI: byte = byte(timing - 3)
  if (timingI < 6):
    timingI += 10

  return int32(timingI)


# seg007:0FB4
proc looseMakeShake() =
  # don't shake on level 13
  if (currRoomModif[currTilepos] == 0 and currentLevel != 13):
    currRoomModif[currTilepos] = 0x80
    addTrob(byte(currRoom), currTilepos, 1)


# seg007:0FE0
proc doKnock(room, tileRow: int32) =
  for tileCol in 0 ..< 10:
    if (getTile(room, tileCol, tileRow) == ord(tiles11Loose)):
      looseMakeShake()


# seg007:1010
proc addMob() =
  if (mobsCount >= 14):
    showDialog("Mobs Overflow")
    return # added

  mobs[mobsCount] = curmob
  inc(mobsCount)


# seg007:1041
proc getCurrTile(tilepos: int16): int16 =
  currModifier = currRoomModif[tilepos]
  currTile = currRoomTiles[tilepos] and 0x1F'u8
  return int16(currTile)


var
  # data:43DC
  curmobIndex: word

# seg007:1063
proc doMobs() =
  var
    nMobs: int16
    newIndex: int16
  nMobs = mobsCount
  curmobIndex = 0
  while curmobIndex < word(nMobs):
    curmob = mobs[curmobIndex]
    moveMob()
    checkLooseFallOnKid()
    mobs[curmobIndex] = curmob
    inc(curmobIndex)

  newIndex = 0
  for index in 0..<mobsCount:
    if (mobs[index].speed != -1):
      mobs[newIndex] = mobs[index]
      inc(newIndex)


  mobsCount = newIndex


# seg007:110F
proc moveMob() =
  if (curmob.`type` == 0):
    moveLoose()

  if (curmob.speed <= 0):
    inc(curmob.speed)


const
  # data:227A
  ySomething: array[5, int16] = [ -1'i16, 62'i16, 125'i16, 188'i16, 25'i16]
var
  # data:594A
  currTileTemp: word

# seg007:1126
proc moveLoose() =
  if (curmob.speed < 0):
    return
  if (curmob.speed < 29):
    curmob.speed += 3
  curmob.y += byte(curmob.speed)
  if (curmob.room == 0):
    if (curmob.y < 210):
      return
    else:
      curmob.speed = -2
      return
  if (curmob.y < 226 and ySomething[curmob.row + 1] <= int16(curmob.y)):
    # fell into a different row
    currTileTemp = word(getTile(int(curmob.room), int32(curmob.xh shr 2), int32(curmob.row)))
    if (currTileTemp == ord(tiles11Loose)):
      looseFall()
    if (currTileTemp == ord(tiles0Empty) or
      currTileTemp == ord(tiles11Loose)
    ):
      mobDownARow()
      return
    playSound(ord(sound2TileCrashing)) # tile crashing
    doKnock(int(curmob.room), int32(curmob.row))
    curmob.y = byte(ySomething[curmob.row + 1])
    curmob.speed = -2
    looseLand()


# seg007:11E8
proc looseLand() =
  var
    buttonType: int16
    tiletype: int16
  buttonType = 0
  tiletype = int16(getTile(int(curmob.room), int32(curmob.xh shr 2), int32(curmob.row)))
  case (tiletype)
  of ord(tiles15Opener):
    currRoomTiles[currTilepos] = ord(tiles14Debris)
    buttonType = ord(tiles14Debris)
    # fallthrough!
    triggerButton(1, buttonType, -1)
    tiletype = int16(getTile(int(curmob.room), int32(curmob.xh shr 2), int32(curmob.row)))
    # fallthrough!
    if (tiletype == ord(tiles19Torch) or
        tiletype == ord(tiles30TorchWithDebris)
    ):
      currRoomTiles[currTilepos] = ord(tiles30TorchWithDebris)
    else:
      currRoomTiles[currTilepos] = ord(tiles14Debris)

    redrawAtCurMob()
    if (tileCol != 0):
      setRedrawFull(int16(currTilepos - 1), 1)
  of ord(tiles6Closer):
    triggerButton(1, buttonType, -1)
    tiletype = int16(getTile(int(curmob.room), int32(curmob.xh shr 2), int32(curmob.row)))
    # fallthrough!
    if (tiletype == ord(tiles19Torch) or
        tiletype == ord(tiles30TorchWithDebris)
    ):
      currRoomTiles[currTilepos] = ord(tiles30TorchWithDebris)
    else:
      currRoomTiles[currTilepos] = ord(tiles14Debris)

    redrawAtCurMob()
    if (tileCol != 0):
      setRedrawFull(int16(currTilepos - 1), 1)
  of ord(tiles1Floor), ord(tiles2Spike), ord(tiles10Potion), ord(tiles19Torch),
      ord(tiles30TorchWithDebris):
    if (tiletype == ord(tiles19Torch) or
        tiletype == ord(tiles30TorchWithDebris)
    ):
      currRoomTiles[currTilepos] = ord(tiles30TorchWithDebris)
    else:
      currRoomTiles[currTilepos] = ord(tiles14Debris)

    redrawAtCurMob()
    if (tileCol != 0):
      setRedrawFull(int16(currTilepos - 1), 1)
  else:
    discard


# seg007:12CB
proc looseFall() =
  currRoomModif[currTilepos] = byte(removeLoose(int(currRoom), int32(currTilepos)))
  curmob.speed = curmob.speed shr 1
  mobs[curmobIndex] = curmob
  curmob.y += 6
  mobDownARow()
  addMob()
  curmob = mobs[curmobIndex]
  redrawAtCurMob()


# seg007:132C
proc redrawAtCurMob() =
  if (word(curmob.room) == drawnRoom):
    redrawHeight = 0x20
    setRedrawFull(int16(currTilepos), 1)
    setWipe(int16(currTilepos), 1)
    # Redraw tile to the right only if it's in the same room.
    if ((currTilepos mod 10) + 1 < 10): # changed
      setRedrawFull(int16(currTilepos + 1), 1)
      setWipe(int16(currTilepos) + 1, 1)


# seg007:1387
proc mobDownARow() =
  inc(curmob.row)
  if (curmob.row >= 3):
    curmob.y -= 192
    curmob.row = 0
    curmob.room = level.roomlinks[curmob.room - 1].down


# seg007:13AE
proc drawMobs() =
  for index in 0 ..< mobsCount:
    curmob = mobs[index]
    drawMob()


# seg007:13E5
proc drawMob() =
  var
    tileRow: int16
    ypos: int16
    topRow: int16
    tilepos: int16
    tileCol: int16
  ypos = int16(curmob.y)
  if (word(curmob.room) == drawnRoom):
    if (curmob.y >= 210):
      return
  elif (word(curmob.room) == room_B):
    if (abs(ypos) >= 18):
      return
    curmob.y += 192
    ypos = int16(curmob.y)
  elif (word(curmob.room) == room_A):
    if (curmob.y < 174):
      return
    ypos = int16(curmob.y - 189)
  else:
    return

  tileCol = int16(curmob.xh shr 2)
  tileRow = int16(yToRowMod4(ypos))
  objTilepos = byte(getTileposNominus(tileCol, tileRow))
  inc(tileCol)
  tilepos = int16(getTilepos(tileCol, tileRow))
  setRedraw2(tilepos, 1)
  setRedrawFore(tilepos, 1)
  topRow = int16(yToRowMod4(ypos - 18))
  if (topRow != tileRow):
    tilepos = int16(getTilepos(tileCol, topRow))
    setRedraw2(tilepos, 1)
    setRedrawFore(tilepos, 1)

  addMobToObjtable(ypos)


# seg007:14DE
proc addMobToObjtable(ypos: int32) =
  var
    index: word
  index = word(tableCounts[4])
  inc(tableCounts[4])
  objtable[index].objType = byte(curmob.`type` or 0x80)
  objtable[index].xh = cast[sbyte](curmob.xh)
  objtable[index].xl = 0
  objtable[index].y = int16(ypos)
  objtable[index].chtabId = ord(idChtab6Environment)
  objtable[index].id = 10
  objtable[index].clip.top = 0
  objtable[index].clip.left = 0
  objtable[index].clip.right = 40
  markObjTileRedraw(int(index))


# seg007:153E
proc sub9A8E() =
  # This function is not used.
  method1BlitRect(onscreenSurface, offscreenSurface, rectTop, rectTop, 0)


# seg007:1556
proc isSpikeHarmful(): int32 =
  var
    modifier: sbyte
  modifier = cast[sbyte](currRoomModif[currTilepos])
  if (modifier == 0 or modifier == -1):
    return 0
  elif (modifier < 0):
    return 1
  elif (modifier < 5):
    return 2
  else:
    return 0


# seg007:1591
proc checkLooseFallOnKid() =
  loadkid()
  if (Char.room == curmob.room and
    byte(Char.currCol) == curmob.xh shr 2 and
    curmob.y < Char.y and
    Char.y - 30 < curmob.y
  ):
    fellOnYourHead()
    savekid()


# seg007:15D3
proc fellOnYourHead() =
  var
    frame: int16
    action: int16
  frame = int16(Char.frame)
  action = int16(Char.action)
  # loose floors hurt you in frames 5..14 (running) only on level 13
  if (
    (currentLevel == 13 or (frame < ord(frame5StartRun) or frame >= 15)) and
    (action < ord(actions2HangClimb) or action == ord(actions7Turn))
  ):
    Char.y = byte(yLand[Char.currRow + 1])
    if takeHp(1) != 0:
      seqtblOffsetChar(ord(seq22Crushed)) # dead (because of loose floor)
      if (frame == ord(frame177Spiked)): # spiked
        Char.x = byte(charDxForward(-12))
    else:
      if (frame != ord(frame109Crouch)): # crouching
        if (getTileBehindChar() == 0):
          Char.x = byte(charDxForward(-2))
        seqtblOffsetChar(ord(seq52LooseFloorFellOnKid)) # loose floor fell on Kid


# seg007:1669
proc playDoorSoundIfVisible(soundId: int32) =
  var
    hasSound: word
    tilepos: word
    gateRoom: word
  tilepos = trob.tilepos
  gateRoom = trob.room
  hasSound = 0

  when (FIX_GATE_SOUNDS):
    var
      hasSoundCondition: sbyte
    if fixes.fixGateSounds != 0:
      hasSoundCondition = cast[sbyte]((gateRoom == roomL and tilepos mod 10 ==
          9) or (gateRoom == drawnRoom and tilepos mod 10 != 9))
    else:
      hasSoundCondition = if gateRoom == roomL: cast[sbyte](tilepos mod 10 ==
          9) else: cast[sbyte](gateRoom == drawnRoom and tilepos mod 10 != 9)
    let
      GATE_SOUND_CONDITION = hasSoundCondition
  else:
    let
      GATE_SOUND_CONDITION = if gateRoom == roomL: tilepos mod 10 == 9 else:
                              (gateRoom == drawnRoom and tilepos mod 10 != 9)
  # Special event: sound of closing gates
  if ((currentLevel == 3 and gateRoom == 2) or (GATE_SOUND_CONDITION != 0)):
    hasSound = 1

  if hasSound != 0:
    playSound(soundId)
