import os, strutils, tables, times

when (USE_MENU):
  var
    arrowheadUpImageData: seq[byte]
    arrowheadDownImageData: seq[byte]
    arrowheadLeftImageData: seq[byte]
    arrowheadRightImageData: seq[byte]
    arrowheadUpImage: ImageType
    arrowheadDownImage: ImageType
    arrowheadLeftImage: ImageType
    arrowheadRightImage: ImageType

  proc loadArrowheadImage() =
    # Make a dummy palette for decode_image().
    var
      datPal: DatPalType

    # white
    datPal.vga[1].r = 0x3F
    datPal.vga[1].g = 0x3F
    datPal.vga[1].b = 0x3F

    if arrowHeadUpImage == nil:
      arrowheadUpImage = decodeImage(cast[ptr ImageDataType](addr(
          arrowheadUpImageData[0]))[], datPal)
    if arrowHeadDownImage == nil:
      arrowheadDownImage = decodeImage(cast[ptr ImageDataType](addr(
          arrowheadDownImageData[0]))[], datPal)

    # dat_pal.vga[1] = vga_palette[color_7_lightgray];
    if arrowHeadLeftImage == nil:
      arrowheadLeftImage = decodeImage(cast[ptr ImageDataType](addr(
          arrowheadLeftImageData[0]))[], datPal)
    if arrowHeadRightImage == nil:
      arrowheadRightImage = decodeImage(cast[ptr ImageDataType](addr(
          arrowheadRightImageData[0]))[], datPal)

  const
    MAX_MENU_ITEM_LENGTH = 32

  type
    PauseMenuItemType = ref object
      id: int32
      previous: PauseMenuItemType
      next: PauseMenuItemType
      required: pointer
      text: string

    pauseMenuItemIds = enum
      PAUSE_MENU_RESUME,
      PAUSE_MENU_CHEATS,
      PAUSE_MENU_SAVE_GAME,
      PAUSE_MENU_LOAD_GAME,
      PAUSE_MENU_RESTART_LEVEL,
      PAUSE_MENU_SETTINGS,
      PAUSE_MENU_RESTART_GAME,
      PAUSE_MENU_QUIT_GAME,
      SETTINGS_MENU_GENERAL,
      SETTINGS_MENU_GAMEPLAY,
      SETTINGS_MENU_VISUALS,
      SETTINGS_MENU_MODS,
      SETTINGS_MENU_LEVEL_CUSTOMIZATION,
      SETTINGS_MENU_BACK

  when(USE_QUICKSAVE):
    let
      pauseMenuItems: array[7, PauseMenuItemType] = [
          PauseMenuItemType(id: int32(PAUSE_MENU_RESUME), text: "RESUME"),
          PauseMenuItemType(id: int32(PAUSE_MENU_SAVE_GAME), text: "QUICKSAVE"),
          PauseMenuItemType(id: int32(PAUSE_MENU_LOAD_GAME), text: "QUICKLOAD"),
          PauseMenuItemType(id: int32(PAUSE_MENU_RESTART_LEVEL),
              text: "RESTART LEVEL"),
          PauseMenuItemType(id: int32(PAUSE_MENU_SETTINGS), text: "SETTINGS"),
          PauseMenuItemType(id: int32(PAUSE_MENU_RESTART_GAME),
              text: "RESTART GAME"),
          PauseMenuItemType(id: int32(PAUSE_MENU_QUIT_GAME), text: "QUIT GAME")
        ]
  else:
    let
      pauseMenuItems: array[5, PauseMenuItemType] = [
          PauseMenuItemType(id: int32(PAUSE_MENU_RESUME), text: "RESUME"),
          PauseMenuItemType(id: int32(PAUSE_MENU_RESTART_LEVEL),
              text: "RESTART LEVEL"),
          PauseMenuItemType(id: int32(PAUSE_MENU_SETTINGS), text: "SETTINGS"),
          PauseMenuItemType(id: int32(PAUSE_MENU_RESTART_GAME),
              text: "RESTART GAME"),
          PauseMenuItemType(id: int32(PAUSE_MENU_QUIT_GAME), text: "QUIT GAME")
        ]
  var
    hoveringPauseMenuItem: int32 = int32(PAUSE_MENU_RESUME)
    nextPauseMenuItem: PauseMenuItemType
    previousPauseMenuItem: PauseMenuItemType
    drawnMenu: int32
    pauseMenuAlpha: byte
    currentDialogBox: int32
    currentDialogText: string
    menuCurrentLevel: word = 1
    needCloseMenu: bool

  type
    menuDialogIds = enum
      DIALOG_NONE,
      DIALOG_RESTORE_DEFAULT_SETTINGS,
      DIALOG_CONFIRM_QUIT,
      DIALOG_SELECT_LEVEL

  let
    settingsMenuItems: array[0..4, PauseMenuItemType] = [
        PauseMenuItemType(id: int32(SETTINGS_MENU_GENERAL), text: "GENERAL"),
        PauseMenuItemType(id: int32(SETTINGS_MENU_GAMEPLAY), text: "GAMEPLAY"),
        PauseMenuItemType(id: int32(SETTINGS_MENU_VISUALS), text: "VISUALS"),
        PauseMenuItemType(id: int32(SETTINGS_MENU_MODS), text: "MODS"),
        PauseMenuItemType(id: int32(SETTINGS_MENU_BACK), text: "BACK")
      ]
  var
    activeSettingsSubsection: int32 = 0
    highlightedSettingsSubsection: int32 = 0
    scrollPosition: int32 = 0
    menuControlY: int32
    menuControlX: int32
    menuControlBack: int32

  type
    menuSettingsStyleIds = enum
      SETTING_STYLE_TOGGLE,
      SETTING_STYLE_NUMBER,
      SETTING_STYLE_TEXT_ONLY
    menuSettingNumberTypeIds = enum
      SETTING_BYTE = 0,
      SETTING_SBYTE = 1,
      SETTING_WORD = 2,
      SETTING_SHORT = 3,
      #SETTING_DWORD = 4,
      SETTING_INT = 5
    settingIds = enum
      SETTING_RESET_ALL_SETTINGS,
      SETTING_SHOW_MENU_ON_PAUSE,
      SETTING_ENABLE_INFO_SCREEN,
      SETTING_ENABLE_SOUND,
      SETTING_ENABLE_MUSIC,
      SETTING_ENABLE_CONTROLLER_RUMBLE,
      SETTING_JOYSTICK_THRESHOLD,
      SETTING_JOYSTICK_ONLY_HORIZONTAL,
      SETTING_FULLSCREEN,
      SETTING_USE_CORRECT_ASPECT_RATIO,
      SETTING_USE_INTEGER_SCALING,
      SETTING_SCALING_TYPE,
      SETTING_ENABLE_FADE,
      SETTING_ENABLE_FLASH,
      SETTING_ENABLE_LIGHTING,
      SETTING_ENABLE_CHEATS,
      SETTING_ENABLE_COPYPROT,
      SETTING_ENABLE_QUICKSAVE,
      SETTING_ENABLE_QUICKSAVE_PENALTY,
      SETTING_ENABLE_REPLAY,
      SETTING_USE_FIXES_AND_ENHANCEMENTS,
      SETTING_ENABLE_CROUCH_AFTER_CLIMBING,
      SETTING_ENABLE_FREEZE_TIME_DURING_END_MUSIC,
      SETTING_ENABLE_REMEMBER_GUARD_HP,
      SETTING_FIX_GATE_SOUNDS,
      SETTING_TWO_COLL_BUG,
      SETTING_FIX_INFINITE_DOWN_BUG,
      SETTING_FIX_GATE_DRAWING_BUG,
      SETTING_FIX_BIGPILLAR_CLIMB,
      SETTING_FIX_JUMP_DISTANCE_AT_EDGE,
      SETTING_FIX_EDGE_DISTANCE_CHECK_WHEN_CLIMBING,
      SETTING_FIX_PAINLESS_FALL_ON_GUARD,
      SETTING_FIX_WALL_BUMP_TRIGGERS_TILE_BELOW,
      SETTING_FIX_STAND_ON_THIN_AIR,
      SETTING_FIX_PRESS_THROUGH_CLOSED_GATES,
      SETTING_FIX_GRAB_FALLING_SPEED,
      SETTING_FIX_SKELETON_CHOMPER_BLOOD,
      SETTING_FIX_MOVE_AFTER_DRINK,
      SETTING_FIX_LOOSE_LEFT_OF_POTION,
      SETTING_FIX_GUARD_FOLLOWING_THROUGH_CLOSED_GATES,
      SETTING_FIX_SAFE_LANDING_ON_SPIKES,
      SETTING_FIX_GLIDE_THROUGH_WALL,
      SETTING_FIX_DROP_THROUGH_TAPESTRY,
      SETTING_FIX_LAND_AGAINST_GATE_OR_TAPESTRY,
      SETTING_FIX_UNINTENDED_SWORD_STRIKE,
      SETTING_FIX_RETREAT_WITHOUT_LEAVING_ROOM,
      SETTING_FIX_RUNNING_JUMP_THROUGH_TAPESTRY,
      SETTING_FIX_PUSH_GUARD_INTO_WALL,
      SETTING_FIX_JUMP_THROUGH_WALL_ABOVE_GATE,
      SETTING_FIX_CHOMPERS_NOT_STARTING,
      SETTING_FIX_FEATHER_INTERRUPTED_BY_LEVELDOOR,
      SETTING_FIX_OFFSCREEN_GUARDS_DISAPPEARING,
      SETTING_FIX_MOVE_AFTER_SHEATHE,
      SETTING_FIX_HIDDEN_FLOORS_DURING_FLASHING,
      SETTING_FIX_HANG_ON_TELEPORT,
      SETTING_FIX_EXIT_DOOR,
      SETTING_FIX_QUICKSAVE_DURING_FEATHER,
      SETTING_FIX_CAPED_PRINCE_SLIDING_THROUGH_GATE,
      SETTING_FIX_DOORTOP_DISABLING_GUARD,
      SETTING_USE_CUSTOM_OPTIONS,
      SETTING_START_MINUTES_LEFT,
      SETTING_START_TICKS_LEFT,
      SETTING_START_HITP,
      SETTING_MAX_HITP_ALLOWED,
      SETTING_SAVING_ALLOWED_FIRST_LEVEL,
      SETTING_SAVING_ALLOWED_LAST_LEVEL,
      SETTING_START_UPSIDE_DOWN,
      SETTING_START_IN_BLIND_MODE,
      SETTING_COPYPROT_LEVEL,
      SETTING_DRAWN_TILE_TOP_LEVEL_EDGE,
      SETTING_DRAWN_TILE_LEFT_LEVEL_EDGE,
      SETTING_LEVEL_EDGE_HIT_TILE,
      SETTING_ALLOW_TRIGGERING_ANY_TILE,
      SETTING_ENABLE_WDA_IN_PALACE,
      SETTING_FIRST_LEVEL,
      SETTING_SKIP_TITLE,
      SETTING_SHIFT_L_ALLOWED_UNTIL_LEVEL,
      SETTING_SHIFT_L_REDUCED_MINUTES,
      SETTING_SHIFT_L_REDUCED_TICKS,
      SETTING_DEMO_HITP,
      SETTING_DEMO_END_ROOM,
      SETTING_INTRO_MUSIC_LEVEL,
      SETTING_HAVE_SWORD_FROM_LEVEL,
      SETTING_CHECKPOINT_LEVEL,
      SETTING_CHECKPOINT_RESPAWN_DIR,
      SETTING_CHECKPOINT_RESPAWN_ROOM,
      SETTING_CHECKPOINT_RESPAWN_TILEPOS,
      SETTING_CHECKPOINT_CLEAR_TILE_ROOM,
      SETTING_CHECKPOINT_CLEAR_TILE_COL,
      SETTING_CHECKPOINT_CLEAR_TILE_ROW,
      SETTING_SKELETON_LEVEL,
      SETTING_SKELETON_ROOM,
      SETTING_SKELETON_TRIGGER_COLUMN_1,
      SETTING_SKELETON_TRIGGER_COLUMN_2,
      SETTING_SKELETON_COLUMN,
      SETTING_SKELETON_ROW,
      SETTING_SKELETON_REQUIRE_OPEN_LEVEL_DOOR,
      SETTING_SKELETON_SKILL,
      SETTING_SKELETON_REAPPEAR_ROOM,
      SETTING_SKELETON_REAPPEAR_X,
      SETTING_SKELETON_REAPPEAR_ROW,
      SETTING_SKELETON_REAPPEAR_DIR,
      SETTING_MIRROR_LEVEL,
      SETTING_MIRROR_ROOM,
      SETTING_MIRROR_COLUMN,
      SETTING_MIRROR_ROW,
      SETTING_MIRROR_TILE,
      SETTING_SHOW_MIRROR_IMAGE,
      SETTING_FALLING_EXIT_LEVEL,
      SETTING_FALLING_EXIT_ROOM,
      SETTING_FALLING_ENTRY_LEVEL,
      SETTING_FALLING_ENTRY_ROOM,
      SETTING_MOUSE_LEVEL,
      SETTING_MOUSE_ROOM,
      SETTING_MOUSE_DELAY,
      SETTING_MOUSE_OBJECT,
      SETTING_MOUSE_START_X,
      SETTING_LOOSE_TILES_LEVEL,
      SETTING_LOOSE_TILES_ROOM_1,
      SETTING_LOOSE_TILES_ROOM_2,
      SETTING_LOOSE_TILES_FIRST_TILE,
      SETTING_LOOSE_TILES_LAST_TILE,
      SETTING_JAFFAR_VICTORY_LEVEL,
      SETTING_JAFFAR_VICTORY_FLASH_TIME,
      SETTING_HIDE_LEVEL_NUMBER_FIRST_LEVEL,
      SETTING_LEVEL_13_LEVEL_NUMBER,
      SETTING_VICTORY_STOPS_TIME_LEVEL,
      SETTING_WIN_LEVEL,
      SETTING_WIN_ROOM,
      SETTING_LOOSE_FLOOR_DELAY,
      SETTING_BASE_SPEED,
      SETTING_FIGHT_SPEED,
      SETTING_CHOMPER_SPEED,
      SETTING_LEVEL_SETTINGS,
      SETTING_LEVEL_TYPE,
      SETTING_LEVEL_COLOR,
      SETTING_GUARD_TYPE,
      SETTING_GUARD_HP,
      SETTING_CUTSCENE,
      SETTING_ENTRY_POSE,
      SETTING_SEAMLESS_EXIT

    SettingType = object
      index: int32
      id: int32
      previous, next: int32
      style: byte
      numberType: byte
      linked: pointer
      required: pointer
      sMin, sMax: int32
      text: string
      explanation: string
      # namesList: NamesListType
      namesList: seq[string]

  var
    generalSettings: seq[SettingType] = @[
        SettingType(id: int32(SETTING_SHOW_MENU_ON_PAUSE), style: byte(
            SETTING_STYLE_TOGGLE), linked: addr(enablePauseMenu),
            text: "Enable pause menu",
            explanation: "Show the in-game menu when you pause the game.\nIf disabled, you can still bring up the menu by pressing Backspace."),
        SettingType(id: int32(SETTING_ENABLE_INFO_SCREEN), style: byte(
            SETTING_STYLE_TOGGLE), linked: addr(enableInfoScreen),
            text: "Display info screen on launch",
            explanation: "Display the SDLPoP information screen when the game starts."),
        SettingType(id: int32(SETTING_ENABLE_SOUND), style: byte(
            SETTING_STYLE_TOGGLE), linked: addr(isSoundOn),
                text: "Enable sound",
            explanation: "Turn sound on or off."),
        SettingType(id: int32(SETTING_ENABLE_CONTROLLER_RUMBLE), style: byte(
            SETTING_STYLE_TOGGLE), linked: addr(enableControllerRumble),
            text: "Enable controller rumble",
            explanation: "If using a controller with a rumble motor, provide haptic feedback when the kid is hurt."),
        SettingType(id: int32(SETTING_JOYSTICK_THRESHOLD), style: byte(
            SETTING_STYLE_NUMBER), numberType: byte(SETTING_INT), linked: addr(
            joystickThreshold), sMin: 0, sMax: high(int16),
            text: "Joystick threshold",
            explanation: "Joystick 'dead zone' sensitivity threshold."),
        SettingType(id: int32(SETTING_JOYSTICK_ONLY_HORIZONTAL), style: byte(
            SETTING_STYLE_TOGGLE), linked: addr(joystickOnlyHorizontal),
            text: "Horizontal joystick movement only",
            explanation: "Use joysticks for horizontal movement only, not all-directional. This may make the game easier to control for some controllers."),
        SettingType(id: int32(SETTING_RESET_ALL_SETTINGS), style: byte(
            SETTING_STYLE_TEXT_ONLY), text: "Restore defaults...",
            explanation: "Revert all settings to the default state.")
      ]
    # scalingTypeSettingNames: array[0..2, string] =
    scalingTypeSettingNames: seq[string] =
      @["Sharp", "Fuzzy", "Blurry"]
    integerScalingPossible: int32 = when(sdl.PATCHLEVEL >= 5):
        1
      else:
        0
    visualsSettings: seq[SettingType] = @[
        SettingType(id: int32(SETTING_FULLSCREEN), style: byte(
            SETTING_STYLE_TOGGLE), linked: addr(startFullscreen),
            text: "Start fullscreen",
            explanation: "Start the game in fullscreen mode.\nYou can also toggle fullscreen by pressing Alt+Enter."),
        SettingType(id: int32(SETTING_USE_CORRECT_ASPECT_RATIO), style: byte(
            SETTING_STYLE_TOGGLE), linked: addr(useCorrectAspectRatio),
            text: "Use 4:3 aspect ratio",
            explanation: "Render the game in the originally intended 4:3 aspect ratio.\nNB. Works best using a high resolution."),
        SettingType(id: int32(SETTING_USE_INTEGER_SCALING), style: byte(
            SETTING_STYLE_TOGGLE), linked: addr(useIntegerScaling),
            required: addr(integerScalingPossible), text: "Use integer scaling",
            explanation: "Enable pixel perfect scaling. That is, make all pixels the same size by forcing integer scale factors.\nCombining with 4:3 aspect ratio requires at least 1600x1200.\nYou need to compile with SDL 2.0.5 or newer to enable this."),
        SettingType(id: int32(SETTING_SCALING_TYPE), style: byte(
            SETTING_STYLE_NUMBER), numberType: byte(SETTING_BYTE), sMax: 2,
            linked: addr(scalingType), namesList: scalingTypeSettingNames,
            text: "Scaling method",
            explanation: "Sharp - Use nearest neighbour resampling.\nFuzzy - First upscale to double size, then use smooth scaling.\nBlurry - Use smooth scaling.")
      ]
    gameplaySettings: seq[SettingType] = @[
        SettingType(id: int32(SETTING_ENABLE_CHEATS), style: byte(
            SETTING_STYLE_TOGGLE), linked: addr(cheatsEnabled),
            text: "Enable cheats", explanation: "Turn cheats on or off."), # "\nAlso, display the CHEATS option on the pause menu."*/ ),
      SettingType(id: int32(SETTING_USE_FIXES_AND_ENHANCEMENTS), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(useFixesAndEnhancements),
          text: "Enhanced mode (allow bug fixes)",
          explanation: "Turn on game fixes and enhancements.\nBelow, you can turn individual fixes/enhancements on or off.\nNOTE: Some fixes disable 'tricks' that depend on game quirks."),
      SettingType(id: int32(SETTING_ENABLE_CROUCH_AFTER_CLIMBING), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.enableCrouchAfterClimbing), required: addr(
          useFixesAndEnhancements), text: "Enable crouching after climbing",
          explanation: "Adds a way to crouch immediately after climbing up: press down and forward simultaneously. In the original game, this could not be done (pressing down always causes the kid to climb down)."),
      SettingType(id: int32(SETTING_ENABLE_FREEZE_TIME_DURING_END_MUSIC),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.enableFreezeTimeDuringEndMusic), required: addr(
          useFixesAndEnhancements), text: "Freeze time during level end music",
          explanation: "Time runs out while the level ending music plays; however, the music can be skipped by disabling sound. This option stops time while the ending music is playing (so there is no need to disable sound)."),
      SettingType(id: int32(SETTING_ENABLE_REMEMBER_GUARD_HP), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.enableRememberGuardHp),
          required: addr(useFixesAndEnhancements),
          text: "Remember guard hitpoints",
          explanation: "Enable guard hitpoints not resetting to their default (maximum) value when re-entering the room."),
      SettingType(id: int32(SETTING_FIX_GATE_SOUNDS), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixGateSounds),
              required: addr(
          useFixesAndEnhancements), text: "Fix gate sounds bug",
          explanation: "If a room is linked to itself on the left, the closing sounds of the gates in that room can't be heard."),
      SettingType(id: int32(SETTING_TWO_COLL_BUG), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixTwoCollBug),
              required: addr(
          useFixesAndEnhancements), text: "Fix two collisions bug",
          explanation: "An open gate or chomper may enable the Kid to go through walls. (Trick 7, 37, 62)"),
      SettingType(id: int32(SETTING_FIX_INFINITE_DOWN_BUG), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixInfiniteDownBug),
          required: addr(useFixesAndEnhancements),
              text: "Fix infinite down bug",
          explanation: "If a room is linked to itself at the bottom, and the Kid's column has no floors, the game hangs."),
      SettingType(id: int32(SETTING_FIX_GATE_DRAWING_BUG), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixGateDrawingBug),
          required: addr(useFixesAndEnhancements), text: "Fix gate drawing bug",
          explanation: "When a gate is under another gate, the top of the bottom gate is not visible."),
      SettingType(id: int32(SETTING_FIX_BIGPILLAR_CLIMB), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixBigpillarClimb),
          required: addr(useFixesAndEnhancements),
          text: "Fix big pillar climbing bug",
          explanation: "When climbing up to a floor with a big pillar top behind, turned right, Kid sees through floor."),
      SettingType(id: int32(SETTING_FIX_JUMP_DISTANCE_AT_EDGE), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixJumpDistanceAtEdge),
          required: addr(useFixesAndEnhancements),
          text: "Fix jump distance at edge",
          explanation: "When climbing up two floors, turning around and jumping upward, the kid falls down. This fix makes the workaround of Trick 25 unnecessary."),
      SettingType(id: int32(SETTING_FIX_EDGE_DISTANCE_CHECK_WHEN_CLIMBING),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixEdgeDistanceCheckWhenClimbing), required: addr(
          useFixesAndEnhancements), text: "Fix edge distance check when climbing",
          explanation: "When climbing to a higher floor, the game unnecessarily checks how far away the edge below is. Sometimes you will \"teleport\" some distance when climbing from firm ground."),
      SettingType(id: int32(SETTING_FIX_PAINLESS_FALL_ON_GUARD), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(
              fixesSaved.fixPainlessFallOnGuard),
          required: addr(useFixesAndEnhancements),
          text: "Fix painless fall on guard",
          explanation: "Falling from a great height directly on top of guards does not hurt."),
      SettingType(id: int32(SETTING_FIX_WALL_BUMP_TRIGGERS_TILE_BELOW),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixWallBumpTriggersTileBelow), required: addr(
          useFixesAndEnhancements), text: "Fix wall bump triggering tile below",
          explanation: "Bumping against a wall may cause a loose floor below to drop, even though it has not been touched. (Trick 18, 34)"),
      SettingType(id: int32(SETTING_FIX_STAND_ON_THIN_AIR), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixStandOnThinAir),
          required: addr(useFixesAndEnhancements),
          text: "Fix standing on thin air",
          explanation: "When pressing a loose tile, you can temporarily stand on thin air by standing up from crouching."),
      SettingType(id: int32(SETTING_FIX_PRESS_THROUGH_CLOSED_GATES),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixPressThroughClosedGates), required: addr(
          useFixesAndEnhancements), text: "Fix pressing through closed gates",
          explanation: "Buttons directly to the right of gates can be pressed even though the gate is closed (Trick 1)"),
      SettingType(id: int32(SETTING_FIX_GRAB_FALLING_SPEED), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixGrabFallingSpeed),
          required: addr(useFixesAndEnhancements),
              text: "Fix grab falling speed",
          explanation: "By jumping and bumping into a wall, you can sometimes grab a ledge two stories down (which should not be possible)."),
      SettingType(id: int32(SETTING_FIX_SKELETON_CHOMPER_BLOOD), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(
              fixesSaved.fixSkeletonChomperBlood),
          required: addr(useFixesAndEnhancements),
          text: "Fix skeleton chomper blood",
          explanation: "When chomped, skeletons cause the chomper to become bloody even though skeletons do not have blood."),
      SettingType(id: int32(SETTING_FIX_MOVE_AFTER_DRINK), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixMoveAfterDrink),
          required: addr(useFixesAndEnhancements),
          text: "Fix movement after drinking",
          explanation: "Controls do not get released properly when drinking a potion, sometimes causing unintended movements."),
      SettingType(id: int32(SETTING_FIX_LOOSE_LEFT_OF_POTION), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixLooseLeftOfPotion),
          required: addr(useFixesAndEnhancements),
          text: "Fix loose floor left of potion",
          explanation: "A drawing bug occurs when a loose tile is placed to the left of a potion (or sword)."),
      SettingType(id: int32(SETTING_FIX_GUARD_FOLLOWING_THROUGH_CLOSED_GATES),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixGuardFollowingThroughClosedGates), required: addr(
          useFixesAndEnhancements), text: "Fix guards passing closed gates",
          explanation: "Guards may \"follow\" the kid to the room on the left or right, even though there is a closed gate in between."),
      SettingType(id: int32(SETTING_FIX_SAFE_LANDING_ON_SPIKES), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(
              fixesSaved.fixSafeLandingOnSpikes),
          required: addr(useFixesAndEnhancements),
          text: "Fix safe landing on spikes",
          explanation: "When landing on the edge of a spikes tile, it is considered safe. (Trick 65)"),
      SettingType(id: int32(SETTING_FIX_GLIDE_THROUGH_WALL), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixGlideThroughWall),
          required: addr(useFixesAndEnhancements),
          text: "Fix gliding through walls",
          explanation: "The kid may glide through walls after turning around while running (especially when weightless)."),
      SettingType(id: int32(SETTING_FIX_DROP_THROUGH_TAPESTRY), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(
              fixesSaved.fixDropThroughTapestry),
          required: addr(useFixesAndEnhancements),
          text: "Fix dropping through tapestries",
          explanation: "The kid can drop down through a closed gate, when there is a tapestry (doortop) above the gate."),
      SettingType(id: int32(SETTING_FIX_LAND_AGAINST_GATE_OR_TAPESTRY),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixLandAgainstGateOrTapestry), required: addr(
          useFixesAndEnhancements), text: "Fix land against gate or tapestry",
          explanation: "When dropping down and landing right in front of a wall, the entire landing animation should normally play. However, when falling against a closed gate or a tapestry(+floor) tile, the animation aborts."),
      SettingType(id: int32(SETTING_FIX_UNINTENDED_SWORD_STRIKE), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixUnintendedSwordStrike), required: addr(
          useFixesAndEnhancements), text: "Fix unintended sword strike",
          explanation: "Sometimes, the kid may automatically strike immediately after drawing the sword. This especially happens when dropping down from a higher floor and then turning towards the opponent."),
      SettingType(id: int32(SETTING_FIX_RETREAT_WITHOUT_LEAVING_ROOM),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixRetreatWithoutLeavingRoom), required: addr(
          useFixesAndEnhancements), text: "Fix retreat without leaving room",
          explanation: "By repeatedly pressing 'back' in a swordfight, you can retreat out of a room without the room changing. (Trick 35)"),
      SettingType(id: int32(SETTING_FIX_RUNNING_JUMP_THROUGH_TAPESTRY),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixRunningJumpThroughTapestry), required: addr(
          useFixesAndEnhancements), text: "Fix running jumps through tapestries",
          explanation: "The kid can jump through a tapestry with a running jump to the left, if there is a floor above it."),
      SettingType(id: int32(SETTING_FIX_PUSH_GUARD_INTO_WALL), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixPushGuardIntoWall),
          required: addr(useFixesAndEnhancements),
          text: "Fix pushing guards into walls",
          explanation: "Guards can be pushed into walls, because the game does not correctly check for walls located behind a guard."),
      SettingType(id: int32(SETTING_FIX_JUMP_THROUGH_WALL_ABOVE_GATE),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixJumpThroughWallAboveGate), required: addr(
          useFixesAndEnhancements), text: "Fix jump through wall above gate",
          explanation: "By doing a running jump into a wall, you can fall behind a closed gate two floors down. (e.g. skip in Level 7)"),
      SettingType(id: int32(SETTING_FIX_CHOMPERS_NOT_STARTING), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(
              fixesSaved.fixChompersNotStarting),
          required: addr(useFixesAndEnhancements),
          text: "Fix chompers not starting",
          explanation: "If you grab a ledge that is one or more floors down, the chompers on that row will not start."),
      SettingType(id: int32(SETTING_FIX_FEATHER_INTERRUPTED_BY_LEVELDOOR),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixFeatherInterruptedByLeveldoor), required: addr(
          useFixesAndEnhancements),
          text: "Fix leveldoor interrupting feather fall",
          explanation: "As soon as a level door has completely opened, the feather fall effect is interrupted because the sound stops."),
      SettingType(id: int32(SETTING_FIX_OFFSCREEN_GUARDS_DISAPPEARING),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixOffscreenGuardsDisappearing), required: addr(
          useFixesAndEnhancements), text: "Fix offscreen guards disappearing",
          explanation: "Guards will often not reappear in another room if they have been pushed (partly or entirely) offscreen."),
      SettingType(id: int32(SETTING_FIX_MOVE_AFTER_SHEATHE), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixMoveAfterSheathe),
          required: addr(useFixesAndEnhancements),
          text: "Fix movement after sheathing",
          explanation: "While putting the sword away, if you press forward and down, and then release down, the kid will still duck."),
      SettingType(id: int32(SETTING_FIX_HIDDEN_FLOORS_DURING_FLASHING),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixHiddenFloorsDuringFlashing), required: addr(
          useFixesAndEnhancements), text: "Fix hidden floors during flashing",
          explanation: "After uniting with the shadow in level 12, the hidden floors will not appear until after the flashing stops."),
      SettingType(id: int32(SETTING_FIX_HANG_ON_TELEPORT), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixHangOnTeleport),
          required: addr(useFixesAndEnhancements),
          text: "Fix hang on teleport bug",
          explanation: "By jumping towards one of the bottom corners of the room and grabbing a ledge, you can teleport to the room above."),
      SettingType(id: int32(SETTING_FIX_EXIT_DOOR), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(fixesSaved.fixExitDoor),
              required: addr(
          useFixesAndEnhancements), text: "Fix exit doors",
          explanation: "You can enter closed exit doors after you met the shadow or Jaffar died, or after you opened one of multiple exits."),
      SettingType(id: int32(SETTING_FIX_QUICKSAVE_DURING_FEATHER), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixQuicksaveDuringFeather), required: addr(
          useFixesAndEnhancements), text: "Fix quick save in feather mode",
          explanation: "You cannot save game while floating in feather mode."),
      SettingType(id: int32(SETTING_FIX_CAPED_PRINCE_SLIDING_THROUGH_GATE),
          style: byte(SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixCapedPrinceSlidingThroughGate), required: addr(
          useFixesAndEnhancements), text: "Fix sliding through closed gate",
          explanation: "If you are using the caped prince graphics, and crouch with your back towards a closed gate on the left edge on the room, then the prince will slide through the gate"),
      SettingType(id: int32(SETTING_FIX_DOORTOP_DISABLING_GUARD), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(
          fixesSaved.fixDoortopDisablingGuard), required: addr(
          useFixesAndEnhancements), text: "Fix door top disabling guard",
          explanation: "Guards become inactive if they are standing on a door top (with floor), or if the prince is standing on a door top."),
    ]

    # tileTypeSettingNames: array[0..31, string] = [
    tileTypeSettingNames: seq[string] = @[
        "Empty", "Floor", "Spikes", "Pillar", "Gate", # 0..4
      "Stuck button", "Closer button", "Tapestry/floor", "Big pillar: bottom",
        "Big pillar: top",               # 5..9
      "Potion", "Loose floor", "Tapestry", "Mirror", "Floor/debris", # 10..14
      "Raise button", "Level door: left", "Level door: right", "Chomper",
        "Torch",                         # 15..19
      "Wall", "Skeleton", "Sword", "Balcony: left", "Balcony: right", # 20..24
      "Lattice: pillar", "Lattice: down", "Lattice: small", "Lattice: left",
        "Lattice: right",                # 25..29
      "Torch/debris", "Tile 31 (unused)" # 30
    ]
    # rowSettingNames: array[0..2, string] = [
    rowSettingNames: seq[string] = @[
        "Top", "Middle", "Bottom"
      ]

    neverIs16 = @["never", "16"]

    # directionSettingNames = [("Left", dirFFLeft), ("Right", dir0Right)].toTable
    directionSettingNames = @["Left", "Right"]

    modsSettings: seq[SettingType] = @[
      SettingType(id: int32(SETTING_USE_CUSTOM_OPTIONS), style: byte(
          SETTING_STYLE_TOGGLE), linked: addr(useCustomOptions),
              text: "Use customization options",
          explanation: "Turn customization options on or off.\n(default: OFF)"),
      SettingType(id: int32(SETTING_LEVEL_SETTINGS), style: byte(
          SETTING_STYLE_TEXT_ONLY), required: addr(useCustomOptions),
              text: "Customize level...",
          explanation: "Change level-specific options (such as level type, guard type, number of guard hitpoints)."),
      SettingType(id: int32(SETTING_START_MINUTES_LEFT), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.startMinutesLeft), numberType: byte(SETTING_SHORT),
          sMin: -1, sMax: high(int16), text: "Starting minutes left",
          explanation: "Starting minutes left. (default = 60)\nTo disable the time limit completely, set this to -1."),
      SettingType(id: int32(SETTING_START_TICKS_LEFT), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.startTicksLeft), numberType: byte(SETTING_WORD),
          sMax: int32(high(uint16)), text: "Starting seconds left",
          explanation: "Starting number of seconds left in the first minute.\n(default = 59.92)"),
      SettingType(id: int32(SETTING_START_HITP), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.startHitp),
          numberType: byte(SETTING_WORD), sMax: int32(high(uint16)),
          text: "Starting hitpoints",
          explanation: "Starting hitpoints. (default = 3)"),
      SettingType(id: int32(SETTING_MAX_HITP_ALLOWED), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.maxHitpAllowed), numberType: byte(SETTING_WORD),
          sMax: int32(high(uint16)), text: "Max hitpoints allowed",
          explanation: "Maximum number of hitpoints you can get. (default = 10)"),
      SettingType(id: int32(SETTING_SAVING_ALLOWED_FIRST_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.savingAllowedFirstLevel), numberType: byte(SETTING_WORD),
          sMax: 16, namesList: neverIs16,
          text: "Saving allowed: first level",
          explanation: "First level where you can save the game. (default = 3)"),
      SettingType(id: int32(SETTING_SAVING_ALLOWED_LAST_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.savingAllowedLastLevel), numberType: byte(SETTING_WORD),
          sMax: 16, namesList: neverIs16, text: "Saving allowed: last level",
          explanation: "Last level where you can save the game. (default = 13)"),
      SettingType(id: int32(SETTING_START_UPSIDE_DOWN), style: byte(
          SETTING_STYLE_TOGGLE), required: addr(useCustomOptions), linked: addr(
          customSaved.startUpsideDown), text: "Start with the screen flipped",
          explanation: "Start the game with the screen flipped upside down, similar to Shift+I (default: OFF)"),
      SettingType(id: int32(SETTING_START_IN_BLIND_MODE), style: byte(
          SETTING_STYLE_TOGGLE), required: addr(useCustomOptions), linked: addr(
          customSaved.startInBlindMode), text: "Start in blind mode",
          explanation: "Start in blind mode, similar to Shift+B (default: OFF)"),
      SettingType(id: int32(SETTING_COPYPROT_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.copyprotLevel), numberType: byte(SETTING_WORD), sMax: 16,
          namesList: neverIs16, text: "Copy protection before level",
          explanation: "The potions level will appear before this level. (default = 2)"),
      SettingType(id: int32(SETTING_DRAWN_TILE_TOP_LEVEL_EDGE), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions),
          namesList: tileTypeSettingNames, linked: addr(
          customSaved.drawnTileTopLevelEdge), numberType: byte(SETTING_BYTE),
          sMax: 31, text: "Drawn tile: top level edge",
          explanation: "Tile drawn at the top of the room if there is no room that way. (default: floor)"),
      SettingType(id: int32(SETTING_DRAWN_TILE_LEFT_LEVEL_EDGE), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions),
          namesList: tileTypeSettingNames, linked: addr(
          customSaved.drawnTileLeftLevelEdge), numberType: byte(SETTING_BYTE),
          sMax: 31, text: "Drawn tile: left level edge",
          explanation: "Tile drawn at the left of the room if there is no room that way. (default: wall)"),
      SettingType(id: int32(SETTING_LEVEL_EDGE_HIT_TILE), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions),
              namesList: tileTypeSettingNames,
          linked: addr(customSaved.levelEdgeHitTile), numberType: byte(
          SETTING_BYTE), sMax: 31, text: "Level edge hit tile",
          explanation: "Tile behavior at the top or left of the room if there is no room that way (default: wall)"),
      SettingType(id: int32(SETTING_ALLOW_TRIGGERING_ANY_TILE), style: byte(
          SETTING_STYLE_TOGGLE), required: addr(useCustomOptions), linked: addr(
          customSaved.allowTriggeringAnyTile),
          text: "Allow triggering any tile",
          explanation: "Enable triggering any tile. For example a button could make loose floors fall, or start a stuck chomper. (default: OFF)"),
      SettingType(id: int32(SETTING_ENABLE_WDA_IN_PALACE), style: byte(
          SETTING_STYLE_TOGGLE), required: addr(useCustomOptions), linked: addr(
          customSaved.enableWdaInPalace), text: "Enable WDA in palace",
          explanation: "Enable the dungeon wall drawing algorithm in the palace.\nN.B. Use with a modified VPALACE.DAT that provides dungeon-like wall graphics! (default: OFF)"),
      SettingType(id: int32(SETTING_FIRST_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.firstLevel),
          numberType: byte(SETTING_WORD), sMax: 15, text: "First level",
          explanation: "Level that will be loaded when starting a new game.\n(default = 1)"),
      SettingType(id: int32(SETTING_SKIP_TITLE), style: byte(
          SETTING_STYLE_TOGGLE), required: addr(useCustomOptions), linked: addr(
              customSaved.skipTitle),
          text: "Skip title sequence",
          explanation: "Always skip the title sequence: the first level will be loaded immediately.\n(default: OFF)"),
      SettingType(id: int32(SETTING_SHIFT_L_ALLOWED_UNTIL_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.shift_LAllowedUntilLevel), numberType: byte(SETTING_WORD),
          sMax: 16, namesList: neverIs16,
          text: "Shift+L allowed until level",
          explanation: "First level where level skipping with Shift+L is denied in non-cheat mode.\n(default = 4)"),
      SettingType(id: int32(SETTING_SHIFT_L_REDUCED_MINUTES), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.shift_LReducedMinutes), numberType: byte(SETTING_WORD),
          sMax: int32(high(uint16)), text: "Minutes left after Shift+L used",
          explanation: "Number of minutes left after Shift+L is used in non-cheat mode.\n(default = 15)"),
      SettingType(id: int32(SETTING_SHIFT_L_REDUCED_TICKS), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.shift_LReducedTicks), numberType: byte(SETTING_WORD),
          sMax: int32(high(uint16)), text: "Seconds left after Shift+L used",
          explanation: "Number of seconds left after Shift+L is used in non-cheat mode.\n(default = 59.92)"),
      SettingType(id: int32(SETTING_DEMO_HITP), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.demoHitp),
          numberType: byte(SETTING_WORD), sMax: int32(high(uint16)),
          text: "Demo level hitpoints",
          explanation: "Hitpoints the kid has on the demo level.\n(default = 4)"),
      SettingType(id: int32(SETTING_DEMO_END_ROOM), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.demoEndRoom),
          numberType: byte(SETTING_WORD), sMin: 1, sMax: 24,
          text: "Demo level ending room",
          explanation: "Demo level ending room.\n(default = 24)"),
      SettingType(id: int32(SETTING_INTRO_MUSIC_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.introMusicLevel), numberType: byte(SETTING_WORD),
              sMax: 16,
          namesList: neverIs16, text: "Level with intro music",
          explanation: "Level where the presentation music is played when the kid crouches down. (default = 1)\nNote: only works if this level is the starting level."),
      SettingType(id: int32(SETTING_HAVE_SWORD_FROM_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.haveSwordFromLevel), numberType: byte(SETTING_WORD),
          sMin: 1, sMax: 16, namesList: neverIs16,
          text: "Have sword from level",
          explanation: "First level (except the demo level) where kid has the sword.\n(default = 2)\n"),
      SettingType(id: int32(SETTING_CHECKPOINT_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.checkpointLevel), numberType: byte(SETTING_WORD),
              sMax: 16,
          namesList: neverIs16, text: "Checkpoint level",
          explanation: "Level where there is a checkpoint. (default = 3)\nThe checkpoint is triggered when leaving room 7 to the left."),
      SettingType(id: int32(SETTING_CHECKPOINT_RESPAWN_DIR), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.checkpointRespawnDir), numberType: byte(SETTING_SBYTE),
          sMin: -1, sMax: 0, namesList: directionSettingNames,
          text: "Checkpoint respawn direction",
          explanation: "Respawn direction after triggering the checkpoint.\n(default: left)"),
      SettingType(id: int32(SETTING_CHECKPOINT_RESPAWN_ROOM), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.checkpointRespawnRoom), numberType: byte(SETTING_BYTE),
          sMin: 1, sMax: 24, text: "Checkpoint respawn room",
          explanation: "Room where you respawn after triggering the checkpoint.\n(default = 2)"),
      SettingType(id: int32(SETTING_CHECKPOINT_RESPAWN_TILEPOS), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.checkpointRespawnTilepos), numberType: byte(SETTING_BYTE),
          sMax: 29, text: "Checkpoint respawn tile position",
          explanation: "Tile position (0 to 29) where you respawn after triggering the checkpoint.\n(default = 6)"),
      SettingType(id: int32(SETTING_CHECKPOINT_CLEAR_TILE_ROOM), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.checkpointClearTileRoom), numberType: byte(SETTING_BYTE),
          sMin: 1, sMax: 24, text: "Checkpoint clear tile room",
          explanation: "Room where a tile is cleared after respawning at the checkpoint location.\n(default = 7)"),
      SettingType(id: int32(SETTING_CHECKPOINT_CLEAR_TILE_COL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.checkpointClearTileCol), numberType: byte(SETTING_BYTE),
          sMax: 9, text: "Checkpoint clear tile column",
          explanation: "Location (column/row) of the cleared tile after respawning at the checkpoint location.\n(default: column: 4, row: top)"),
      SettingType(id: int32(SETTING_CHECKPOINT_CLEAR_TILE_ROW), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.checkpointClearTileRow), numberType: byte(SETTING_BYTE),
          sMax: 2, namesList: rowSettingNames,
          text: "Checkpoint clear tile row",
          explanation: "Location (column/row) of the cleared tile after respawning at the checkpoint location.\n(default: column: 4, row: top)"),
      SettingType(id: int32(SETTING_SKELETON_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.skeletonLevel), numberType: byte(SETTING_WORD), sMax: 16,
          namesList: neverIs16, text: "Skeleton awakes level",
          explanation: "Level and room where a skeleton can come alive.\n(default: level = 3, room: 1)"),
      SettingType(id: int32(SETTING_SKELETON_ROOM), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.skeletonRoom), numberType: byte(SETTING_BYTE), sMin: 1,
          sMax: 24, text: "Skeleton awakes room",
          explanation: "Level and room where a skeleton can come alive.\n(default: level = 3, room: 1)"),
      SettingType(id: int32(SETTING_SKELETON_TRIGGER_COLUMN_1), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.skeletonTriggerColumn_1), numberType: byte(SETTING_BYTE),
          sMax: 9, text: "Skeleton trigger column (1)",
          explanation: "The skeleton will wake up if the kid is on one of these two columns.\n(defaults: 2,3)"),
      SettingType(id: int32(SETTING_SKELETON_TRIGGER_COLUMN_2), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.skeletonTriggerColumn_2), numberType: byte(SETTING_BYTE),
          sMax: 9, text: "Skeleton trigger column (2)",
          explanation: "The skeleton will wake up if the kid is on one of these two columns.\n(defaults: 2,3)"),
      SettingType(id: int32(SETTING_SKELETON_COLUMN), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.skeletonColumn), numberType: byte(SETTING_BYTE), sMax: 9,
          text: "Skeleton tile column",
          explanation: "Location (column/row) of the skeleton tile that will awaken.\n(default: column: 5, row: middle)"),
      SettingType(id: int32(SETTING_SKELETON_ROW), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.skeletonRow),
          numberType: byte(SETTING_BYTE), sMax: 2, namesList: rowSettingNames,
          text: "Skeleton tile row",
          explanation: "Location (column/row) of the skeleton tile that will awaken.\n(default: column: 5, row: middle)"),
      SettingType(id: int32(SETTING_SKELETON_REQUIRE_OPEN_LEVEL_DOOR),
          style: byte(SETTING_STYLE_TOGGLE), required: addr(useCustomOptions),
              linked: addr(
          customSaved.skeletonRequireOpenLevelDoor),
          text: "Skeleton requires level door",
          explanation: "Whether the level door must first be opened before the skeleton awakes.\n(default: true)"),
      SettingType(id: int32(SETTING_SKELETON_SKILL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.skeletonSkill), numberType: byte(SETTING_BYTE), sMax: 15,
          text: "Skeleton skill",
          explanation: "Skill of the awoken skeleton.\n(default = 2)"),
      SettingType(id: int32(SETTING_SKELETON_REAPPEAR_ROOM), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.skeletonReappearRoom), numberType: byte(SETTING_BYTE),
          sMin: 1, sMax: 24, text: "Skeleton reappear room",
          explanation: "If the skeleton falls into this room, it will reappear there.\n(default = 3)"),
      SettingType(id: int32(SETTING_SKELETON_REAPPEAR_X), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.skeletonReappearX), numberType: byte(SETTING_BYTE),
          sMax: 255, text: "Skeleton reappear X coordinate",
          explanation: "Horizontal coordinate where the skeleton reappears.\n(default = 133)\n(58: left edge of the room, 198: right edge)"),
      SettingType(id: int32(SETTING_SKELETON_REAPPEAR_ROW), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.skeletonReappearRow), numberType: byte(SETTING_BYTE),
          sMax: 2, namesList: rowSettingNames, text: "Skeleton reappear row",
          explanation: "Row on which the skeleton reappears.\n(default: middle)"),
      SettingType(id: int32(SETTING_SKELETON_REAPPEAR_DIR), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.skeletonReappearDir), numberType: byte(SETTING_SBYTE),
          sMin: -1, sMax: 0, namesList: directionSettingNames,
          text: "Skeleton reappear direction",
          explanation: "Direction the skeleton is facing when it reappears.\n(default: right)"),
      SettingType(id: int32(SETTING_MIRROR_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.mirrorLevel),
          numberType: byte(SETTING_WORD), sMax: 16, namesList: neverIs16,
          text: "Mirror level",
          explanation: "Level and room where the mirror appears.\n(default: level = 4, room: 4)"),
      SettingType(id: int32(SETTING_MIRROR_ROOM), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.mirrorRoom),
          numberType: byte(SETTING_BYTE), sMin: 1, sMax: 24,
              text: "Mirror room",
          explanation: "Level and room where the mirror appears.\n(default: level = 4, room: 4)"),
      SettingType(id: int32(SETTING_MIRROR_COLUMN), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.mirrorColumn), numberType: byte(SETTING_BYTE), sMax: 9,
          text: "Mirror column",
          explanation: "Location (column/row) of the tile where the mirror appears.\n(default: column: 4, row: top)"),
      SettingType(id: int32(SETTING_MIRROR_ROW), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.mirrorRow),
          numberType: byte(SETTING_BYTE), sMax: 2, namesList: rowSettingNames,
          text: "Mirror row",
          explanation: "Location (column/row) of the tile where the mirror appears.\n(default: column: 4, row: top)"),
      SettingType(id: int32(SETTING_MIRROR_TILE), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.mirrorTile),
          numberType: byte(SETTING_BYTE), sMax: 31,
          namesList: tileTypeSettingNames, text: "Mirror tile",
          explanation: "Tile type that appears when the mirror should appear.\n(default: mirror)"),
      SettingType(id: int32(SETTING_SHOW_MIRROR_IMAGE), style: byte(
          SETTING_STYLE_TOGGLE), required: addr(useCustomOptions), linked: addr(
          customSaved.showMirrorImage), numberType: byte(SETTING_BYTE),
          text: "Show mirror image",
          explanation: "Show the kid's mirror image in the mirror.\n(default: true)"),
      SettingType(id: int32(SETTING_FALLING_EXIT_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.fallingExitLevel), numberType: byte(SETTING_WORD),
              sMax: 16,
          namesList: neverIs16, text: "Falling exit level",
          explanation: "Level where the kid can progress to the next level by falling off a specific room.\n(default = 6)"),
      SettingType(id: int32(SETTING_FALLING_EXIT_ROOM), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.fallingExitRoom), numberType: byte(SETTING_BYTE), sMin: 1,
          sMax: 24, text: "Falling exit room",
          explanation: "Room where the kid can progress to the next level by falling down.\n(default = 1)"),
      SettingType(id: int32(SETTING_FALLING_ENTRY_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.fallingEntryLevel), numberType: byte(SETTING_WORD),
          sMax: 16, namesList: neverIs16, text: "Falling entry level",
          explanation: "If the kid starts in this level in this room, the starting room will not be shown,\nbut the room below instead, to allow for a falling entry. (default: level = 7, room: 17)"),
      SettingType(id: int32(SETTING_FALLING_ENTRY_ROOM), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.fallingEntryRoom), numberType: byte(SETTING_BYTE),
              sMin: 1,
          sMax: 24, text: "Falling entry room",
          explanation: "If the kid starts in this level in this room, the starting room will not be shown,\nbut the room below instead, to allow for a falling entry. (default: level = 7, room: 17)"),
      SettingType(id: int32(SETTING_MOUSE_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.mouseLevel),
          numberType: byte(SETTING_WORD), sMax: 16, namesList: neverIs16,
          text: "Mouse level",
          explanation: "Level where the mouse appears.\n(default = 8)"),
      SettingType(id: int32(SETTING_MOUSE_ROOM), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.mouseRoom),
          numberType: byte(SETTING_BYTE), sMin: 1, sMax: 24, text: "Mouse room",
          explanation: "Room where the mouse appears.\n(default = 16)"),
      SettingType(id: int32(SETTING_MOUSE_DELAY), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.mouseDelay),
          numberType: byte(SETTING_WORD), sMax: int32(high(uint16)),
              text: "Mouse delay",
          explanation: "Number of seconds to wait before the mouse appears.\n(default = 12.5)"),
      SettingType(id: int32(SETTING_MOUSE_OBJECT), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.mouseObject),
          numberType: byte(SETTING_BYTE), sMax: 255, text: "Mouse object",
          explanation: "Mouse object type. (default = 24)\nBe careful: a value not 24 will change the mouse for the kid."),
      SettingType(id: int32(SETTING_MOUSE_START_X), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.mouseStartX),
          numberType: byte(SETTING_BYTE), sMax: 255,
          text: "Mouse start X coordinate",
          explanation: "Horizontal starting coordinate of the mouse.\n(default = 200)"),
      SettingType(id: int32(SETTING_LOOSE_TILES_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.looseTilesLevel), numberType: byte(SETTING_WORD),
              sMax: 16,
          namesList: neverIs16, text: "Loose tiles level",
          explanation: "Level where loose floor tiles will fall down.\n(default = 13)"),
      SettingType(id: int32(SETTING_LOOSE_TILES_ROOM_1), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.looseTilesRoom_1), numberType: byte(SETTING_BYTE),
              sMin: 1,
          sMax: 24, text: "Loose tiles room (1)",
          explanation: "Rooms where visible loose floor tiles will fall down.\n(default = 23, 16)"),
      SettingType(id: int32(SETTING_LOOSE_TILES_ROOM_2), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.looseTilesRoom_2), numberType: byte(SETTING_BYTE),
              sMin: 1,
          sMax: 24, text: "Loose tiles room (2)",
          explanation: "Rooms where visible loose floor tiles will fall down.\n(default = 23, 16)"),
      SettingType(id: int32(SETTING_LOOSE_TILES_FIRST_TILE), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.looseTilesFirstTile), numberType: byte(SETTING_BYTE),
          sMax: 29, text: "Loose tiles first tile",
          explanation: "Range of loose floor tile positions that will be pressed.\n(default = 22 to 27)"),
      SettingType(id: int32(SETTING_LOOSE_TILES_LAST_TILE), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.looseTilesLastTile), numberType: byte(SETTING_BYTE),
          sMax: 29, text: "Loose tiles last tile",
          explanation: "Range of loose floor tile positions that will be pressed.\n(default = 22 to 27)"),
      SettingType(id: int32(SETTING_JAFFAR_VICTORY_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.jaffarVictoryLevel), numberType: byte(SETTING_WORD),
          sMax: 16, namesList: neverIs16, text: "Jaffar victory level",
          explanation: "Killing the guard in this level causes the screen to flash, and event 0 to be triggered upon leaving the room.\n(default = 13)"),
      SettingType(id: int32(SETTING_JAFFAR_VICTORY_FLASH_TIME), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.jaffarVictoryFlashTime), numberType: byte(SETTING_BYTE),
          sMax: int32(high(uint16)), text: "Jaffar victory flash time",
          explanation: "How long the screen will flash after killing Jaffar.\n(default = 18)"),
      SettingType(id: int32(SETTING_HIDE_LEVEL_NUMBER_FIRST_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.hideLevelNumberFromLevel), numberType: byte(SETTING_WORD),
          sMax: 16, namesList: neverIs16,
          text: "Hide level number from level",
          explanation: "First level where the level number will not be displayed.\n(default = 14)"),
      SettingType(id: int32(SETTING_LEVEL_13_LEVEL_NUMBER), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.level_13LevelNumber), numberType: byte(SETTING_BYTE),
          sMax: int32(high(uint16)), text: "Level 13 displayed level number",
          explanation: "Level number displayed on level 13.\n(default = 12)"),
      SettingType(id: int32(SETTING_VICTORY_STOPS_TIME_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.victoryStopsTimeLevel), numberType: byte(SETTING_WORD),
          sMax: 16, namesList: neverIs16, text: "Victory stops time level",
          explanation: "Level where Jaffar's death stops time.\n(default = 13)"),
      SettingType(id: int32(SETTING_WIN_LEVEL), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.winLevel),
          numberType: byte(SETTING_WORD), sMax: 16, namesList: neverIs16,
          text: "Level where you can win",
          explanation: "Level and room where you can win the game.\n(default: level = 14, room: 5)"),
      SettingType(id: int32(SETTING_WIN_ROOM), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
              customSaved.winRoom),
          numberType: byte(SETTING_BYTE), sMin: 1, sMax: 24,
          text: "Room where you can win",
          explanation: "Level and room where you can win the game.\n(default: level = 14, room: 5)"),
      SettingType(id: int32(SETTING_LOOSE_FLOOR_DELAY), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.looseFloorDelay), numberType: byte(SETTING_BYTE), sMin: 0,
          sMax: 127, text: "Loose floor delay",
          explanation: "Number of seconds to wait before a loose floor falls.\n(default = 0.92)"),
      SettingType(id: int32(SETTING_BASE_SPEED), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.baseSpeed), numberType: byte(SETTING_BYTE), sMin: 0,
          sMax: 127, text: "Base speed",
          explanation: "Game speed when not fighting (delay between frames in 1/60 seconds). Smaller is faster..\n(default = 5)"),
      SettingType(id: int32(SETTING_FIGHT_SPEED), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.fightSpeed), numberType: byte(SETTING_BYTE), sMin: 0,
          sMax: 127, text: "Fight speed",
          explanation: "Game speed when fighting (delay between frames in 1/60 seconds). Smaller is faster..\n(default = 6)"),
      SettingType(id: int32(SETTING_CHOMPER_SPEED), style: byte(
          SETTING_STYLE_NUMBER), required: addr(useCustomOptions), linked: addr(
          customSaved.chomperSpeed), numberType: byte(SETTING_BYTE), sMin: 0,
          sMax: 127, text: "Chomper speed",
          explanation: "Chomper speed (length of the animation cycle in frames). Smaller is faster..\n(default = 15)"),
      ]

    levelTypeSettingNames: seq[string] = @[
        "Dungeon", "Palace"
      ]
    guardTypeSettingNames: seq[string] = @[
        "None", "Normal", "Fat", "Skeleton", "Vizier", "Shadow"
      ]
    entryPoseSettingNames: seq[string] = @[
        "Turning", "Falling", "Running"
      ]
    offSettingName: seq[string] = @[
        "Off"
      ]

    levelSettings: seq[SettingType] = @[
        SettingType(id: int32(SETTING_LEVEL_SETTINGS), style: byte(
            SETTING_STYLE_TEXT_ONLY), required: addr(useCustomOptions),
            text: "Customize another level...",
            explanation: "Select another level to customize."),
        SettingType(id: int32(SETTING_LEVEL_TYPE), style: byte(
            SETTING_STYLE_NUMBER), required: addr(useCustomOptions),
            namesList: levelTypeSettingNames, linked: nil,
            numberType: byte(SETTING_BYTE), sMax: 1, text: "Level type",
            explanation: "Which environment is used in this level.\n(either dungeon or palace)"),
        SettingType(id: int32(SETTING_LEVEL_COLOR), style: byte(
            SETTING_STYLE_NUMBER), required: addr(useCustomOptions),
                linked: nil,
            numberType: byte(SETTING_WORD), sMax: 4,
                text: "Level color palette",
            explanation: "0: colors from VDUNGEON.DAT/VPALACE.DAT\n>0: colors from PRINCE.DAT.\nYou need a PRINCE.DAT from PoP 1.3 or 1.4 for this."),
        SettingType(id: int32(SETTING_GUARD_TYPE), style: byte(
            SETTING_STYLE_NUMBER), required: addr(useCustomOptions),
            namesList: guardTypeSettingNames, linked: nil,
            numberType: byte(SETTING_SHORT), sMin: -1, sMax: 4,
                text: "Guard type",
            explanation: "Guard type used in this level (normal, fat, skeleton, vizier, or shadow)."),
        SettingType(id: int32(SETTING_GUARD_HP), style: byte(
            SETTING_STYLE_NUMBER), required: addr(useCustomOptions),
                linked: nil,
            numberType: byte(SETTING_BYTE), sMax: int32(high(uint8)),
                text: "Guard hitpoints",
            explanation: "Number of hitpoints guards have in this level."),
        SettingType(id: int32(SETTING_CUTSCENE), style: byte(
            SETTING_STYLE_NUMBER), required: addr(useCustomOptions),
                linked: nil,
            numberType: byte(SETTING_BYTE), sMax: 15,
                text: "Cutscene before level",
            explanation: "Cutscene that plays between the previous level and this level.\n0: none, 2 or 6: standing, 4: lying down, 8: mouse leaves,\n9: mouse returns, 12: standing or turn around"),
        SettingType(id: int32(SETTING_ENTRY_POSE), style: byte(
            SETTING_STYLE_NUMBER), required: addr(useCustomOptions),
                linked: nil,
            numberType: byte(SETTING_BYTE), sMax: 2,
                namesList: entryPoseSettingNames,
            text: "Entry pose",
            explanation: "The pose the kid has when the level starts.\n"),
        SettingType(id: int32(SETTING_SEAMLESS_EXIT), style: byte(
            SETTING_STYLE_NUMBER), required: addr(useCustomOptions),
                linked: nil,
            numberType: byte(SETTING_SBYTE), sMin: -1, sMax: 24,
            namesList: offSettingName, text: "Seamless exit",
            explanation: "Entering this room moves the kid to the next level.\nSet to -1 to disable."),

      ]

  when(USE_FADE):
    visualsSettings.add(SettingType(id: int32(SETTING_ENABLE_FADE), style: byte(
        SETTING_STYLE_TOGGLE), linked: addr(enableFade),
        explanation: "Turn fading on or off."))
  when(USE_FLASH):
    visualsSettings.add(SettingType(id: int32(SETTING_ENABLE_FLASH),
        style: byte(SETTING_STYLE_TOGGLE), linked: addr(enableFlash),
        explanation: "Turn flashing on or off."))
  when(USE_LIGHTING):
    visualsSettings.add(SettingType(id: int32(SETTING_ENABLE_LIGHTING),
        style: byte(SETTING_STYLE_TOGGLE), linked: addr(enableLighting),
        explanation: "Darken those parts of the screen which are not near a torch."))
  when(USE_COPYPROT):
    gameplaySettings.add(SettingType(id: int32(SETTING_ENABLE_COPYPROT),
        style: byte(SETTING_STYLE_TOGGLE), linked: addr(enableCopyprot),
        text: "Enable copy protection level",
        explanation: "Enable or disable the potions (copy protection) level."))
  when(USE_QUICKSAVE):
    gameplaySettings.add(SettingType(id: int32(SETTING_ENABLE_QUICKSAVE),
        style: byte(SETTING_STYLE_TOGGLE), linked: addr(enableQuicksave),
        text: "Enable quicksave",
        explanation: "Enable quicksave/load feature.\nPress F6 to quicksave, F9 to quickload."))
    gameplaySettings.add(SettingType(id: int32(
        SETTING_ENABLE_QUICKSAVE_PENALTY), style: byte(SETTING_STYLE_TOGGLE),
        linked: addr(enableQuicksavePenalty), text: "Quicksave time penalty",
        explanation: "Try to let time run out when quickloading (similar to dying).\nActually, the 'remaining time' will still be restored, but a penalty (up to one minute) will be applied."))
  when(USE_REPLAY):
    gameplaySettings.add(SettingType(id: int32(SETTING_ENABLE_REPLAY),
        style: byte(SETTING_STYLE_TOGGLE), linked: addr(enableReplay),
        text: "Enable replays",
        explanation: "Enable recording/replay feature.\nPress Ctrl+Tab in-game to start recording.\nTo stop, press Ctrl+Tab again."))
  type
    SettingsAreaType = ref object
      settings: seq[SettingType]
      settingCount: int32

  var
    generalSettingsArea: SettingsAreaType = new (SettingsAreaType)
    gameplaySettingsArea: SettingsAreaType = new (SettingsAreaType)
    visualsSettingsArea: SettingsAreaType = new (SettingsAreaType)
    modsSettingsArea: SettingsAreaType = new (SettingsAreaType)
    levelSettingsArea: SettingsAreaType = new (SettingsAreaType)

  generalSettingsArea.settings = generalSettings
  generalSettingsArea.settingCount = generalSettings.len
  gameplaySettingsArea.settings = gameplaySettings
  gameplaySettingsArea.settingCount = gameplaySettings.len
  visualsSettingsArea.settings = visualsSettings
  visualsSettingsArea.settingCount = visualsSettings.len
  modsSettingsArea.settings = modsSettings
  modsSettingsArea.settingCount = modsSettings.len
  levelSettingsArea.settings = levelSettings
  levelSettingsArea.settingCount = levelSettings.len

  proc getSettingsArea(menuItemId: int32): SettingsAreaType =
    case menuItemId
    of int32(SETTINGS_MENU_GENERAL):
      return generalSettingsArea
    of int32(SETTINGS_MENU_GAMEPLAY):
      return gameplaySettingsArea
    of int32(SETTINGS_MENU_VISUALS):
      return visualsSettingsArea
    of int32(SETTINGS_MENU_MODS):
      return modsSettingsArea
    of int32(SETTINGS_MENU_LEVEL_CUSTOMIZATION):
      return levelSettingsArea
    else:
      return nil

  proc initPauseMenuItems(firstItem: openArray[PauseMenuItemType],
      itemCount: int32) =
    # original implementation
    # for index, item in firstItem:
    #   if index != 0:
    #     item.previous = firstItem[index - 1]
    #   else:
    #     item.previous = firstItem[^1]
    #   if index != itemCount:
    #     item.next = firstItem[index + 1]
    #   else:
    #     item.next = firstItem[0]
    # this implementation should be more efficient
    for i in 1..<itemCount-1:
      firstItem[i].next = firstItem[i + 1]
      firstItem[i].previous = firstItem[i - 1]
    firstItem[0].next = firstItem[1]
    firstItem[0].previous = firstItem[^1]
    firstItem[^1].next = firstItem[0]
    firstItem[^1].previous = firstItem[^2]

  proc initSettingsList(firstSetting: var openArray[SettingType],
      settingCount: int32) =
    # for index, item in firstSetting:
    #   item.index = index
    for i in 1..<settingCount-1:
      firstSetting[i].index = i
      firstSetting[i].previous = firstSetting[i - 1].id
      firstSetting[i].next = firstSetting[i + 1].id

    firstSetting[0].index = 0
    firstSetting[0].previous = firstSetting[0].id
    firstSetting[0].next = firstSetting[1].id

    firstSetting[^1].index = settingCount - 1
    firstSetting[^1].previous = firstSetting[^2].id
    firstSetting[^1].next = firstSetting[^1].id

  proc initMenu() =
    loadArrowheadImage()

    initPauseMenuItems(pauseMenuItems, pauseMenuItems.len)
    initPauseMenuItems(settingsMenuItems, settingsMenuItems.len)

    initSettingsList(generalSettings, generalSettings.len)
    initSettingsList(visualsSettings, visualsSettings.len)
    initSettingsList(gameplaySettings, gameplaySettings.len)
    initSettingsList(modsSettings, modsSettings.len)
    initSettingsList(levelSettings, levelSettings.len)

  proc isMouseOverRect(rect: var RectType): bool =
    return (mouseX >= rect.left and mouseX < rect.right and mouseY >=
        rect.top and mouseY < rect.bottom)

  # Maps the cursor position into a coordinate between (0,0) and (320,200) and sets mouse_x, mouse_y and mouse_moved.
  proc readMouseState() =
    var
      scaleX, scaleY: cfloat
      logicalWidth, logicalHeight: cint

    renderGetScale(renderer, addr(scaleX), addr(scaleY))
    renderGetLogicalSize(renderer, addr(logicalWidth), addr(logicalHeight))

    var
      logicalScaleX: cint = logicalWidth div 320
      logicalScaleY: cint = logicalHeight div 200
    scaleX *= cfloat(logicalScaleX)
    scaley *= cfloat(logicalScaleY)
    if (not(scaleX > 0 and scaleY > 0 and logicalScaleX > 0 and logicalScaleY > 0)):
      return

    var
      viewport: Rect

    renderGetViewport(renderer, addr(viewport))
    viewport.x = viewport.x div logicalScaleX
    viewport.y = viewport.y div logicalScaleY
    var
      lastMouseX: cint = mouseX
      lastMouseY: cint = mouseY

    discard getMouseState(addr(mouseX), addr(mouseY))
    mouseX = cint(float(mouseX)/scaleX - float(viewport.x) + 0.5)
    mouseY = cint(float(mouseY)/scaleY - float(viewport.y) + 0.5)
    mouseMoved = lastMouseX != mouseX or lastMouseY != mouseY

  var
    explanationRect: RectType = RectType(top: 170, left: 20, bottom: 200, right: 300)
    highlightedSettingId: int32 = int32(SETTING_ENABLE_INFO_SCREEN)
    # Whether the focus is on the left (0) or right (1) part of the screen in the Settings menu.
    controlledArea: int32 = 0
    nextSettingId = 0 # For navigating up/down.
    previousSettingId: int32 = 0
    # When navigating up using keyboard/controller, whether we also need to scroll up
    atScrollUpBoundary: bool
    # When navigating down using keyboard/controller, whether we also need to scroll down
    atScrollDownBoundary: bool

  proc playMenuSound(soundId: int32) =
    playSound(soundId)
    playNextSound()

  proc enterSettingsSubsection(settingsMenuId: int32) =
    var
      settingsArea: SettingsAreaType = getSettingsArea(settingsMenuId)
    if activeSettingsSubsection != settingsMenuId:
      highlightedSettingId = settingsArea.settings[0].id
    activeSettingsSubsection = settingsMenuId
    highlightedSettingsSubsection = settingsMenuId
    if not(mouseClicked):
      hoveringPauseMenuItem = 0
    controlledArea = 1
    scrollPosition = 0

    # Special case: for the level customization submenu, the linked variables should depend on menu_current_level.
    # So we need to initialize them now.
    if settingsMenuId == int32(SETTINGS_MENU_LEVEL_CUSTOMIZATION):
      for i in 0..<settingsArea.settingCount:
        # var
        #   setting: SettingType = settingsArea.settings[i]
        case settingsArea.settings[i].id
        of ord(SETTING_LEVEL_TYPE):
          settingsArea.settings[i].linked = addr(customSaved.tblLevelType[menuCurrentLevel])
        of ord(SETTING_LEVEL_COLOR):
          settingsArea.settings[i].linked = addr(customSaved.tblLevelColor[menuCurrentLevel])
        of ord(SETTING_GUARD_TYPE):
          settingsArea.settings[i].linked = addr(customSaved.tblGuardType[menuCurrentLevel])
        of ord(SETTING_GUARD_HP):
          settingsArea.settings[i].linked = addr(customSaved.tblGuardHp[menuCurrentLevel])
        of ord(SETTING_CUTSCENE):
          settingsArea.settings[i].linked = addr(
              customSaved.tblCutscenesByIndex[menuCurrentLevel])
        of ord(SETTING_ENTRY_POSE):
          settingsArea.settings[i].linked = addr(customSaved.tblEntryPose[menuCurrentLevel])
        of ord(SETTING_SEAMLESS_EXIT):
          settingsArea.settings[i].linked = addr(customSaved.tblSeamlessExit[menuCurrentLevel])
        else:
          discard

  proc leaveSettingsSubsection() =
    if activeSettingsSubsection == ord(SETTINGS_MENU_LEVEL_CUSTOMIZATION):
      enterSettingsSubsection(ord(SETTINGS_MENU_MODS))
    else:
      # Go back to the top level of the settings menu.
      controlledArea = 0
      hoveringPauseMenuItem = activeSettingsSubsection
      activeSettingsSubsection = 0
      highlightedSettingsSubsection = 0

  proc resetPausedMenu() =
    drawnMenu = 0
    controlledArea = 0
    hoveringPauseMenuItem = ord(PAUSE_MENU_RESUME)

  proc pauseMenuClicked(item: PauseMenuItemType) =
    playMenuSound(int(sound22LooseShake3))
    case item.id
    of ord(PAUSE_MENU_RESUME):
      needCloseMenu = true
    of ord(PAUSE_MENU_SAVE_GAME):
      # TODO: Manual save games?
      if (Kid.alive < 0):
        needQuickSave = 1
      needCloseMenu = true
    of ord(PAUSE_MENU_LOAD_GAME):
      needQuickLoad = 1
      needCloseMenu = true
      stopSounds()
    of ord(PAUSE_MENU_RESTART_LEVEL):
      lastKeyScancode = ord(SCANCODE_A) or ord(WITH_CTRL)
    of ord(PAUSE_MENU_SETTINGS):
      drawnMenu = 1
      hoveringPauseMenuItem = ord(SETTINGS_MENU_GENERAL)
      highlightedSettingsSubsection = ord(SETTINGS_MENU_GENERAL)
      activeSettingsSubsection = 0
      controlledArea = 0
    of ord(PAUSE_MENU_RESTART_GAME):
      lastKeyScancode = ord(SCANCODE_R) or ord(WITH_CTRL)
    of ord(PAUSE_MENU_QUIT_GAME):
      currentDialogBox = ord(DIALOG_CONFIRM_QUIT)
      currentDialogText = "Quit SDLPoP?"
    of ord(SETTINGS_MENU_GENERAL), ord(SETTINGS_MENU_GAMEPLAY), ord(
        SETTINGS_MENU_VISUALS), ord(SETTINGS_MENU_MODS):
      enterSettingsSubsection(item.id)
    of ord(SETTINGS_MENU_BACK):
      resetPausedMenu()
    else:
      discard
    # prevent "click-through" because the screen changes
    clearMenuControls()

  proc drawPauseMenuItem(item: PauseMenuItemType, parent: var RectType,
      yOffset: var int32, inactiveTextColor: int32) =
    if item.required != nil:
      if cast[ptr sbyte](item.required)[] == 0:
        # skip this item (disabled)
        return

    var
      textRect: RectType = parent
      textColor = inactiveTextColor
    textRect.top += int16(yOffset)

    var
      selectionBox: RectType = textRect
    selectionBox.bottom = selectionBox.top + 8
    selectionBox.top -= 3

    var
      highlighted: bool = hoveringPauseMenuItem == item.id
    if haveMouseInput and isMouseOverRect(selectionBox):
      hoveringPauseMenuItem = item.id
      highlighted = true

    if highlighted:
      previousPauseMenuItem = item.previous
      nextPauseMenuItem = item.next
      # Skip over disabled items (such as the CHEATS menu in non-cheat mode)
      if previousPauseMenuItem.required != nil:
        while cast[ptr sbyte](previousPauseMenuItem.required)[] == 0:
          previousPauseMenuItem = previousPauseMenuItem.previous
          if previousPauseMenuItem.required == nil:
            break

      if nextPauseMenuItem.required != nil:
        while cast[ptr sbyte](nextPauseMenuItem.required)[] == 0:
          nextPauseMenuItem = nextPauseMenuItem.next
          if nextPauseMenuItem.required == nil:
            break
      textColor = int32(color15Brightwhite)
      drawRectContours(selectionBox, byte(color7LightGray))

      if mouseClicked:
        if isMouseOverRect(selectionBox):
          pauseMenuClicked(item)
      elif pressedEnter and (drawnMenu == 0 or (drawnMenu == 1 and
          controlledArea == 0)):
        pauseMenuClicked(item)

    showTextWithColor(textRect, 0, -1, item.text, textColor)
    yOffset += 13

  proc drawPauseMenu() =
    pauseMenuAlpha = 120
    drawRectWithAlpha(screenRect, byte(color0Black), pauseMenuAlpha)
    drawRectWithAlpha(rectBottomText, byte(color0Black), 0'u8)
    var
      pauseRectOuter = RectType(top: 0, left: 110, bottom: 192, right: 210)
      pauseRectInner: RectType
    discard shrink2Rect(pauseRectInner, pauseRectOuter, 5, 5)

    if not (haveMouseInput):
      if menuControlY == 1:
        playMenuSound(int(sound21LooseShake2))
        hoveringPauseMenuItem = nextPauseMenuItem.id
      elif menuControlY == -1:
        playMenuSound(int(sound21LooseShake2))
        hoveringPauseMenuItem = previousPauseMenuItem.id

    var
      yOffset: int32 = 50

    for i in 0..<pauseMenuItems.len:
      drawPauseMenuItem(pauseMenuItems[i], pauseRectInner, yOffset, int32(color15Brightwhite))

  var
    wereSettingsChanged: bool

  proc turnSettingOnOff(settingId: int32, newState: byte, linked: pointer) =
    wereSettingsChanged = true

    case settingId
    of int32(SETTING_FULLSCREEN):
      startFullscreen = newState
      discard setWindowFullscreen(window, uint32(newState != 0) * uint32(WINDOW_FULLSCREEN))
    of int32(SETTING_USE_CORRECT_ASPECT_RATIO):
      useCorrectAspectRatio = newState
      applyAspectRatio()
    of int32(SETTING_USE_INTEGER_SCALING):
      useIntegerScaling = newState
      if newState != 0:
        windowResized()
      elif sdl.PATCHLEVEL >= 5:
        discard renderSetIntegerScale(renderer, false)
    of int32(SETTING_ENABLE_LIGHTING):
      when(USE_LIGHTING):
        enableLighting = newState
        if (newState != 0) and (lightingMask.isNil):
          initLighting()
        needFullRedraw = 1
      else:
        discard
    of int32(SETTING_ENABLE_SOUND):
      turnSoundOnOff(byte(newState != 0) * 15'u8)
    of int32(SETTING_ENABLE_MUSIC):
      turnMusicOnOff(newState)
    of int32(SETTING_USE_FIXES_AND_ENHANCEMENTS):
      turnFixesAndEnhancementsOnOff(newState)
    of int32(SETTING_USE_CUSTOM_OPTIONS):
      turnCustomOptionsOnOff(newState)
    else:
      if linked != nil:
        cast[ptr byte](linked)[] = newState

  proc turnSettingOnOffWithSound(setting: SettingType, newState: byte) =
    playMenuSound(int(sound10SwordVsSword))
    turnSettingOnOff(setting.id, newState, setting.linked)

  proc getSettingValue(setting: SettingType): int32 =
    var
      value: int32 = 0
    if setting.linked != nil:
      case setting.numberType
      of byte(SETTING_BYTE):
        value = int32(cast[ptr byte](setting.linked)[])
      of byte(SETTING_SBYTE):
        value = int32(cast[ptr sbyte](setting.linked)[])
      of byte(SETTING_WORD):
        value = int32(cast[ptr word](setting.linked)[])
      of byte(SETTING_SHORT):
        value = int32(cast[ptr int16](setting.linked)[])
      of byte(SETTING_INT):
        value = cast[ptr int32](setting.linked)[]
      else:
        value = int32(cast[ptr byte](setting.linked)[])

    return value

  proc setSettingValue(setting: SettingType, value: int32) =
    if setting.linked != nil:
      case setting.numberType
      of byte(SETTING_BYTE):
        cast[ptr byte](setting.linked)[] = byte(value)
      of byte(SETTING_SBYTE):
        cast[ptr sbyte](setting.linked)[] = sbyte(value)
      of byte(SETTING_WORD):
        cast[ptr word](setting.linked)[] = word(value)
      of byte(SETTING_SHORT):
        cast[ptr int16](setting.linked)[] = int16(value)
      of byte(SETTING_INT):
        cast[ptr int32](setting.linked)[] = value
      else:
        cast[ptr byte](setting.linked)[] = byte(value)

  proc increaseSetting(setting: SettingType, oldValue: int32) =
    var
      newValue: int32

    if setting.id == int32(SETTING_JOYSTICK_THRESHOLD):
      newValue = ( (oldValue div 1000) + 1) * 1000
    else:
      newValue = oldValue + 1

    if setting.linked != nil and newValue <= setting.sMax:
      wereSettingsChanged = true
      setSettingValue(setting, newValue)

  proc decreaseSetting(setting: SettingType, oldValue: int32) =
    var
      newValue: int32

    if setting.id == int32(SETTING_JOYSTICK_THRESHOLD):
      newValue = ( ((oldValue + 999) div 1000) - 1) * 1000
    else:
      newValue = oldValue - 1

    if setting.linked != nil and newValue >= setting.sMin:
      wereSettingsChanged = true
      setSettingValue(setting, newValue)

  proc drawSettingExplanation(setting: SettingType) =
    showTextWithColor(explanationRect, 0, -1, setting.explanation, int32(color7LightGray))

  proc drawImageWithBlending(image: ImageType, xpos, ypos: int32) =
    var
      srcRect: Rect = Rect(x: 0, y: 0, w: image.w, h: image.h)
      destRect: Rect = Rect(x: xpos, y: ypos, w: image.w, h: image.h)
    discard setColorKey(image, 1, 0)
    if blitSurface(image, addr(srcRect), currentTargetSurface, addr(
        destRect)) != 0:
      sdlperror("SDL_BlitSurface")
      quitPoP(1)

  proc printSettingValue(setting: SettingType, value: int32): string =
    var
      hasName: bool = false
      buffer: string
      # list: seq[string] = setting.namesList
      # maxLen: uint = min(MAX_OPTION_VALUE_NAME_LENGTH, b)

    if setting.namesList.len == 0:
      # if (setting.namesList.`type` == 0) and (value >= 0) and (value < setting.namesList.names.count)
      if (value >= 0) and (value < setting.namesList.len):
        buffer = setting.namesList[value]
        hasName = true

    if not(hasName):
      if (setting.id == int32(SETTING_START_TICKS_LEFT) or setting.id == int32(
          SETTING_SHIFT_L_REDUCED_TICKS) or setting.id == int32(
          SETTING_MOUSE_DELAY) or setting.id == int32(
              SETTING_LOOSE_FLOOR_DELAY)):
        var
          seconds: float = float(value) * (1.0/12.0)
        snprintfCheck(buffer, uint(MAX_OPTION_VALUE_NAME_LENGTH), $(seconds))
      else:
        snprintfCheck(buffer, uint(MAX_OPTION_VALUE_NAME_LENGTH), $(value))

    return buffer

  proc drawSetting(setting: SettingType, parent: var RectType,
      yOffset: var int32, inactiveTextColor: int32) =
    var
      textRect: RectType = parent
    textRect.top += int16(yOffset)
    var
      textColor: int32 = inactiveTextColor
      selectedColor: int32 = ord(color15Brightwhite)
      unselectedColor: int32 = ord(color7LightGray)

      settingBox: RectType = textRect
    settingBox.top -= 5
    settingBox.bottom = settingBox.top + 15
    settingBox.left -= 10
    settingBox.right += 10

    if mouseClicked and isMouseOverRect(settingBox):
      highlightedSettingId = setting.id
      controlledArea = 1

    if highlightedSettingId == setting.id:
      nextSettingId = setting.next
      previousSettingId = setting.previous
      atScrollUpBoundary = (setting.index == scrollPosition)
      atScrollDownBoundary = (setting.index == scrollPosition + 8)

      var
        destRect: Rect
      rectToSdlrect(settingBox, destRect)
      var
        rgbColor: uint32 = mapRGBA(overlaySurface.format, 55, 55, 55, 255)
      if fillRect(overlaySurface, addr(destRect), rgbColor) != 0:
        sdlperror("SDL_FillRect")
        quitPoP(1)
      var
        leftSideOfSettingBox: RectType = settingBox
      leftSideOfSettingBox.left = settingBox.left - 2
      leftSideOfSettingBox.right = settingBox.left
      drawRect(leftSideOfSettingBox, ord(color15Brightwhite))
      drawSettingExplanation(setting)

    var
      disabled: bool = false
    if setting.required != nil:
      disabled = cast[ptr byte](setting.required)[] == 0
    if disabled:
      textColor = ord(color7LightGray)

    showTextWithColor(textRect, -1, -1, setting.text, textColor)

    if setting.style == ord(SETTING_STYLE_TOGGLE) and not(disabled):
      var
        settingEnabled: bool = true
      if not(setting.linked.isNil):
        settingEnabled = cast[ptr byte](setting.linked)[] != 0
      # Toggling the setting: either by clicking on "ON" or "OFF", or by pressing left/right.
      if highlightedSettingId == setting.id:
        if mouseClicked:
          if not(settingEnabled):
            var
              onHitbox: RectType = settingBox
            onHitbox.left = settingBox.right - 22
            if isMouseOverRect(onHitbox):
              turnSettingOnOffWithSound(setting, 1)
              settingEnabled = false
          else:
            var
              offHitbox: RectType = settingBox
            offHitbox.left = settingBox.right - 49
            offHitbox.right = settingBox.right - 22
            if isMouseOverRect(offHitbox):
              turnSettingOnOffWithSound(setting, 0)
              settingEnabled = true
        elif settingEnabled and (menuControlX < 0):
          turnSettingOnOffWithSound(setting, 0)
          settingEnabled = false
        elif not(settingEnabled) and (menuControlX > 0):
          turnSettingOnOffWithSound(setting, 1)
          settingEnabled = true

      let
        offColor = if settingEnabled:
            unselectedColor
          else:
            selectedColor
        onColor = if settingEnabled:
            selectedColor
          else:
            unselectedColor

      showTextWithColor(textRect, 1, -1, "ON", onColor)
      textRect.right -= 15
      showTextWithColor(textRect, 1, -1, "OFF", offColor)

    elif setting.style == int32(SETTING_STYLE_NUMBER) and not(disabled):
      var
        value: int32 = getSettingValue(setting)
      if highlightedSettingId == setting.id:
        if mouseClicked:
          var
            rightHitbox: RectType = RectType(top: settingBox.top,
                left: textRect.right - 5, bottom: settingBox.bottom,
                right: textRect.right + 10)
          if isMouseOverRect(rightHitbox):
            increaseSetting(setting, value)
          else:
            var
              valueText: string = printSettingValue(setting, value)
              valueTextWidth: int32 = getLineWidth(valueText, valueText.len)
              leftHitbox: RectType = rightHitbox
            leftHitbox.left -= int16(valueTextWidth + 10)
            leftHitbox.right += int16(valueTextWidth + 5)
            if isMouseOverRect(leftHitbox):
              decreaseSetting(setting, value)
        elif menuControlX > 0:
          increaseSetting(setting, value)
        elif menuControlX < 0:
          decreaseSetting(setting, value)

      # may have been updated
      value = getSettingValue(setting)
      var
        valueText: string = printSettingValue(setting, value)
      showTextWithColor(textRect, 1, -1, valueText, selectedColor)

      if highlightedSettingId == setting.id:
        let
          valueTextWidth = getLineWidth(valueText, valueText.len)
        drawImageWithBlending(arrowHeadRightImage, textRect.right + 2, textRect.top)
        drawImageWithBlending(arrowHeadLeftImage, textRect.right -
            valueTextWidth - 6, textRect.top)
    else:
      # show text only
      if highlightedSettingId == setting.id and (setting.required.isNil or cast[
          ptr sbyte](setting.required)[] != 0):
        if pressedEnter or (mouseClicked and isMouseOverRect(settingBox)):
          if setting.id == int32(SETTING_RESET_ALL_SETTINGS):
            playMenuSound(int(sound22LooseShake3))
            currentDialogBox = int32(DIALOG_RESTORE_DEFAULT_SETTINGS)
            currentDialogText = "Restore all settings to their default values?"
          elif setting.id == int32(SETTING_LEVEL_SETTINGS):
            playMenuSound(int(sound22LooseShake3))
            currentDialogBox = int32(DIALOG_SELECT_LEVEL)

    yOffset += 15

  proc menuScroll(y: int32) =
    var
      currentSettingsArea: SettingsAreaType = getSettingsArea(activeSettingsSubsection)
    if currentSettingsArea != nil:
      let
        maxScroll: int32 = max(0, currentSettingsArea.settingCount - 9)
      if drawnMenu == 1 and controlledArea == 1:
        if y < 0 and scrollPosition > 0:
          dec(scrollPosition)
        elif y > 0 and scrollPosition < maxScroll:
          inc(scrollPosition)

  proc drawSettingsArea(settingsArea: SettingsAreaType) =
    if settingsArea == nil:
      return
    var
      settingsAreaRect: RectType = RectType(top: 0, left: 80, bottom: 170, right: 320)
      yOffset: int32 = 0
      numDrawnSettings: int32 = 0
    discard shrink2Rect(settingsAreaRect, settingsAreaRect, 20, 20)

    # The MODS subsection with level specific settings is a special case:
    # We want to display which level we are editing, and start drawing slightly lower.
    if activeSettingsSubsection == int32(SETTINGS_MENU_LEVEL_CUSTOMIZATION):
      yOffset = 15
      var
        levelText: string
      snprintfCheck(levelText, 16, "LEVEL " & $(int(menuCurrentLevel)))
      showTextWithColor(settingsAreaRect, 0, -1, levelText, int32(color15Brightwhite))

    for i in 0..<settingsArea.settingCount:
      if numDrawnSettings >= 9:
        break
      elif i >= scrollPosition:
        inc(numDrawnSettings)
        drawSetting(settingsArea.settings[i], settingsAreaRect, yOffset, int32(color15Brightwhite))

    if scrollPosition > 0:
      drawImageWithBlending(arrowheadUpImage, 200, 10)
    if scrollPosition + numDrawnSettings < settingsArea.settingCount:
      drawImageWithBlending(arrowheadDownImage, 200, 151)

    # Draw a scroll bar if needed.
    # It's not clickable yet, it just shows where you are in the list.
    if numDrawnSettings < settingsArea.settingCount:
      const
        scrollbarWidth: int32 = 2
      var
        scrollbarRect: RectType = RectType(top: settingsAreaRect.top - 5,
            bottom: settingsAreaRect.bottom, left: int16(
            settingsAreaRect.right + 10 - scrollbarWidth),
            right: settingsAreaRect.right + 10)
      discard method5Rect(scrollbarRect, ord(blitters0NoTransp), ord(color8Darkgray))

      let
        scrollbarHeight: int32 = scrollbarRect.bottom - scrollbarRect.top
      var
        scrollbarSliderRect: RectType = RectType(top: int16(scrollbarRect.top +
            scrollPosition * scrollbarHeight div settingsArea.settingCount),
            bottom: int16(scrollbarRect.top + (scrollPosition +
            numDrawnSettings) * scrollbarHeight div settingsArea.settingCount),
            left: scrollbarRect.left, right: scrollbarRect.right)
      discard method5Rect(scrollbarSliderRect, ord(blitters0NoTransp), ord(color7LightGray))

  proc drawSettingsMenu() =
    var
      settingsArea = getSettingsArea(activeSettingsSubsection)
      pauseMenuAlpha: byte = if settingsArea == nil:
          220
        else:
          255

    drawRectWithAlpha(screenRect, byte(color0Black), pauseMenuAlpha)

    var
      pauseRectOuter: RectType = RectType(top: 0, left: 10, bottom: 192, right: 80)
      pauseRectInner: RectType
    discard shrink2Rect(pauseRectInner, pauseRectOuter, 5, 5)

    if not(haveMouseInput):
      var
        hoveringItemChanged: bool = false
      if controlledArea == 0:
        var
          oldHoveringItemId: int32 = hoveringPauseMenuItem
        if menuControlY == 1:
          hoveringPauseMenuItem = nextPauseMenuItem.id
        elif menuControlY == -1:
          hoveringPauseMenuItem = previousPauseMenuItem.id
        if oldHoveringItemId != hoveringPauseMenuItem:
          hoveringItemChanged = true
      elif controlledArea == 1:
        var
          # settings area
          oldHighlightedSettingId: int32 = hoveringPauseMenuItem

          # Why does the global variable contain the ID instead of the index?...
          # Find the index from the ID.
          currentSettingsArea = getSettingsArea(activeSettingsSubsection)
          highlightedSettingIndex: int32 = -1
        for i in 0..<currentSettingsArea.settingCount:
          if highlightedSettingId == currentSettingsArea.settings[i].id:
            highlightedSettingIndex = i
            break

        var
          last: int32 = currentSettingsArea.settingCount - 1
          maxScroll: int32 = max(0, currentSettingsArea.settingCount - 9)

        if menuControlY > 0:
          # DOWN
          highlightedSettingIndex += menuControlY
          if highlightedSettingIndex > last:
            highlightedSettingIndex = last

          # With Page Down, try to leave the selection in the same row visually.
          if menuControlY > 1:
            scrollPosition += menuControlY
        elif menuControlY < 0:
          # UP
          highlightedSettingIndex += menuControlY
          if highlightedSettingIndex < 0:
            highlightedSettingIndex = 0

          # With Page Down, try to leave the selection in the same row visually.
          if menuControlY < -1:
            scrollPosition += menuControlY

        if menuControlY != 0:
          # We check both directions in both cases, to scroll the highlighted row back into sight even if the user scrolled it out of sight (with the mouse wheel).
          if highlightedSettingIndex - 8 > scrollPosition:
            scrollPosition = highlightedSettingIndex - 8
          if highlightedSettingIndex < scrollPosition:
            scrollPosition = highlightedSettingIndex
          if scrollPosition > maxScroll:
            scrollPosition = maxScroll
          if scrollPosition < 0:
            scrollPosition = 0

        # Find the ID from the index.
        highlightedSettingId = currentSettingsArea.settings[
            highlightedSettingIndex].id

        if oldHighlightedSettingId != highlightedSettingId:
          hoveringItemChanged = true
      if hoveringItemChanged:
        playMenuSound(int(sound21LooseShake2))

    var
      yOffset: int32 = 50
    for i in 0..<settingsMenuItems.len:
      var
        item: PauseMenuItemType = settingsMenuItems[i]
        textColor: int32 = if highlightedSettingsSubsection == item.id:
            int32(color15Brightwhite)
          else:
            int32(color7LightGray)
      drawPauseMenuItem(settingsMenuItems[i], pauseRectInner, yOffset, textColor)

    drawSettingsArea(settingsArea)

  type
    DialogButtonIds = enum
      DIALOG_BUTTON_CANCEL,
      DIALOG_BUTTON_OK

  proc confirmationDialogResult(whichDialog: int32, button: int32) =
    if button == int32(DIALOG_BUTTON_OK):
      if whichDialog == int32(DIALOG_RESTORE_DEFAULT_SETTINGS):
        playMenuSound(int(sound10SwordVsSword))
        wereSettingsChanged = true
        setOptionsToDefault()
        turnSettingOnOff(int(SETTING_USE_INTEGER_SCALING), useIntegerScaling, nil)
        when(USE_LIGHTING):
          turnSettingOnOff(int(SETTING_ENABLE_LIGHTING), enableLighting, nil)
        applyAspectRatio()
        turnSoundOnOff(byte(isSoundOn != 0) * 15'u8)
        turnMusicOnOff(enableMusic)
      elif whichDialog == int32(DIALOG_CONFIRM_QUIT):
        lastKeyScancode = int32(SCANCODE_Q) or int32(WITH_CTRL)
        discard keyTestQuit()
    else:
      playMenuSound(int(sound22LooseShake3))

  var
    cancelTextRect: RectType = RectType(top: 104, left: 162, bottom: 118, right: 212)
    cancelHighlightRect: RectType = RectType(top: 103, left: 162, bottom: 116, right: 212)
    okTextRect: RectType = RectType(top: 104, left: 108, bottom: 118, right: 158)
    okHighlightRect: RectType = RectType(top: 103, left: 108, bottom: 116, right: 158)

  proc drawConfirmationDialog(whichDialog: int32, text: string) =
    var
      highlightedButton: int32 = int32(DIALOG_BUTTON_OK)
      oldHighlightedButton: int32 = -1
    while true:
      processEvents()
      discard keyTestPausedMenu(keyTestQuit())
      processAdditionalMenuInput()

      if menuControlBack == 1:
        confirmationDialogResult(whichDialog, int32(DIALOG_BUTTON_CANCEL))
        break

      if haveMouseInput:
        if isMouseOverRect(okHighlightRect):
          highlightedButton = int32(DIALOG_BUTTON_OK)
        elif isMouseOverRect(cancelHighlightRect):
          highlightedButton = int32(DIALOG_BUTTON_CANCEL)


      if menuControlX < 0:
        highlightedButton = int32(DIALOG_BUTTON_OK)
      elif menuControlX > 0:
        highlightedButton = int32(DIALOG_BUTTON_CANCEL)
      elif mouseClicked or pressedEnter:
        confirmationDialogResult(whichDialog, highlightedButton)
        break

      if highlightedButton != oldHighlightedButton:
        oldHighlightedButton = highlightedButton
        # Need to redraw the dialog box.
        var
          clearColor: uint32 = mapRGBA(currentTargetSurface.format, 0, 0, 0, 255)
          rect: RectType

        discard fillRect(overlaySurface, nil, clearColor)
        drawRect(copyprotDialog.peelRect, int32(color0Black))
        dialogMethod2Frame(copyprotDialog)
        discard shrink2Rect(rect, copyprotDialog.textRect, 2, 1)
        rect.bottom -= 14
        showTextWithColor(rect, 0, 0, text, int32(color15Brightwhite))
        clearKbdBuf()

        var
          highlightRect: ptr RectType
          okTextColor, cancelTextColor: int32

        if highlightedButton == int32(DIALOG_BUTTON_OK):
          highlightRect = addr(okHighlightRect)
          okTextColor = int32(color15Brightwhite)
          cancelTextColor = int32(color7LightGray)
        else:
          highlightRect = addr(cancelHighlightRect)
          okTextColor = int32(color7LightGray)
          cancelTextColor = int32(color15Brightwhite)

        drawRect(highlightRect[], int32(color8Darkgray))
        showTextWithColor(okTextRect, 0, 0, "OK", okTextColor)
        showTextWithColor(cancelTextRect, 0, 0, "Cancel", cancelTextColor)
        updateScreen()

      # Prevent 100% cpu usage
      delay(1)

    currentDialogBox = 0
    clearMenuControls()

  proc drawSelectLevelDialog() =
    clearMenuControls()
    var
      oldEditedLevelNumber: int32 = -1
    while true:
      processEvents()
      discard keyTestPausedMenu(keyTestQuit())
      processAdditionalMenuInput()

      if menuControlBack == 1:
        menuControlBack = 0
        playMenuSound(int(sound22LooseShake3))
        break

      if menuControlX < 0:
        menuCurrentLevel = max(0'u16, menuCurrentLevel - 1'u16)
      elif menuControlX > 0:
        menuCurrentLevel = min(15'u16, menuCurrentLevel + 1'u16)
      elif mouseClicked or pressedEnter:
        enterSettingsSubsection(int(SETTINGS_MENU_LEVEL_CUSTOMIZATION))
        highlightedSettingsSubsection = int32(SETTINGS_MENU_MODS)
        playMenuSound(int(sound22LooseShake3))
        break

      if int32(menuCurrentLevel) != oldEditedLevelNumber:
        # Need to redraw the dialog box.
        var
          savedFont: FontType = textstate.ptrFont
        textstate.ptrFont = hcFont
        oldEditedLevelNumber = int32(menuCurrentLevel)
        var
          clearColor: uint32 = mapRGBA(currentTargetSurface.format, 0, 0, 0, 255)
          rect: RectType
          inputRect: RectType = RectType(top: 104, left: 64, bottom: 118, right: 256)
          levelText: string
        discard fillRect(overlaySurface, nil, clearColor)
        drawRect(copyprotDialog.peelRect, ord(color0Black))
        dialogMethod2Frame(copyprotDialog)
        discard shrink2Rect(rect, copyprotDialog.textRect, 2, 1)
        rect.bottom -= 14
        showTextWithColor(rect, 0, 0, "Customize level...", ord(color15Brightwhite))
        clearKbdBuf()
        levelText = $(menuCurrentLevel)
        showTextWithColor(inputRect, 0, 0, levelText, ord(color15Brightwhite))
        drawImageWithBlending(arrowheadRightImage, 175, inputRect.top + 3)
        drawImageWithBlending(arrowheadLeftImage, 145 - 3, inputRect.top + 3)

        updateScreen()
        textstate.ptrFont = savedFont

      # Prevent 100% cpu usage
      delay(1)

    clearMenuControls()

  var
    needFullMenuRedrawCount: int32

  proc drawMenu() =
    escapeKeySuppressed = (keyStates[SCANCODE_BACKSPACE] or keyStates[
        SCANCODE_ESCAPE]) != 0
    var
      savedTargetSurface: SurfaceType = currentTargetSurface
    currentTargetSurface = overlaySurface

    needCloseMenu = false
    while not(needCloseMenu):
      clearMenuControls()
      processEvents()
      if processKey() != 0:
        # Menu was forcefully closed, for example by pressing Ctrl+A.
        break
      processAdditionalMenuInput()

      if currentDialogBox != ord(DIALOG_NONE):
        if currentDialogBox == ord(DIALOG_SELECT_LEVEL):
          drawSelectLevelDialog()
        else:
          drawConfirmationDialog(currentDialogBox, currentDialogText)
        currentDialogBox = ord(DIALOG_NONE)
        clearMenuControls()

      if isMenuShown == 1:
        # reset the menu if the menu is drawn for the first time
        isMenuShown = -1
        needFullMenuRedrawCount = 2
        resetPausedMenu()

      if menuControlBack == 1:
        playMenuSound(ord(sound22LooseShake3))
        if drawnMenu == 1:
          if controlledArea == 1:
            leaveSettingsSubsection()
          else:
            resetPausedMenu()
        else:
          # Go back to the top level pause menu.
          break

      if menuControlY != 0:
        menuScroll(menuControlScrollY)

      if haveMouseInput or haveKeyboardOrControllerInput:
        # The menu is updated+drawn within the same routine, so redrawing may be necessary after the first time.
        # TODO: Maybe in the future fully separate updating from drawing?
        needFullMenuRedrawCount = 2
      else:
        if needFullMenuRedrawCount == 0:
          delay(1)
          # Don't redraw if there is no input to process (save CPU cycles).
          continue

      var
        savedFont: FontType = textstate.ptrFont
      textstate.ptrFont = hcSmallFont
      if drawnMenu == 0:
        drawPauseMenu()
      elif drawnMenu == 1:
        drawSettingsMenu()
      textstate.ptrFont = savedFont
      if not(needCloseMenu):
        updateScreen()

      dec(needFullMenuRedrawCount)

    currentTargetSurface = savedTargetSurface

  proc clearMenuControls() =
    pressedEnter = false
    mouseMoved = false
    mouseClicked = false
    mouseButtonClickedRight = false
    haveMouseInput = false
    haveKeyboardOrControllerInput = false
    menuControlX = 0
    menuControlY = 0
    menuControlBack = 0
    menuControlScrollY = 0

  proc processAdditionalMenuInput() =
    readMouseState()
    haveKeyboardOrControllerInput = (menuControlX or menuControlY or
        menuControlBack) != 0 or pressedEnter
    haveMouseInput = mouseMoved or mouseClicked or mouseButtonClickedRight or
        (menuControlScrollY != 0)

    var
      flags: dword = getWindowFlags(window)
    if (flags and WINDOW_FULLSCREEN_DESKTOP) != 0:
      if haveMouseInput:
        discard showCursor(cint(1))
      elif haveKeyboardOrControllerInput:
        discard showCursor(cint(0))
    else:
      discard showCursor(cint(1))

  var
    joyABXYButtonsReleased: bool
    joyXyReleased: bool
    joyXyTimeoutCounter: uint64

  proc keyTestPausedMenu(key: int32): int32 =
    var
      keyT = key
    menuControlX = 0
    menuControlY = 0
    menuControlBack = 0

    if mouseButtonClickedRight:
      # Can use RMB to close menus.
      menuControlBack = 1

    if isJoystMode != 0:
      var
        joyX: int32 = joyHatStates[0]
        joyY: int32 = joyHatStates[1]
        yThreshold: int32 = 14000
        # Less sensitive, to prevent accidentally changing a setting.
        xThreshold: int32 = 26000

      if joyAxis[int(CONTROLLER_AXIS_LEFTY)] < -yThreshold:
        joyY = -1
      elif joyAxis[int(CONTROLLER_AXIS_LEFTY)] > yThreshold:
        joyY = 1
      elif joyAxis[int(CONTROLLER_AXIS_LEFTX)] > xThreshold:
        joyX = -1
      elif joyAxis[int(CONTROLLER_AXIS_LEFTX)] > xThreshold:
        joyX = 1

      var
        # Delay for hold-down repeated input.
        neededTimeoutS: float = 0.1
      if joyX == 0 and joyY == 0:
        joyXyReleased = true
        joyXyTimeoutCounter = 0
      else:
        if joyXyReleased:
          neededTimeoutS = 0.3
          joyXyReleased = false
        var
          currentCounter: uint64 = getPerformanceCounter()
        if currentCounter > joyXyTimeoutCounter:
          menuControlX = joyX
          menuControlY = joyY
          joyXyTimeoutCounter = currentCounter + uint64(float(
              getPerformanceFrequency()) * neededTimeoutS)
          # cancel other input
          return 0

      if joyAYButtonsState == 0 and joyBButtonState == 0:
        joyABXYButtonsReleased = true
      elif joyABXYButtonsReleased:
        joyABXYButtonsReleased = false
        if joyAYButtonsState == 1:
          # A pressed
          keyT = int32(SCANCODE_RETURN)
          # Prevent 'down' input being passed to the controls if the game is unpaused.
          joyAYButtonsState = 0
        elif joyBButtonState == 1:
          keyT = int32(SCANCODE_ESCAPE)

    case keyT
    of ord(SCANCODE_UP):
      menuControlY = -1
    of ord(SCANCODE_DOWN):
      menuControlY = 1
    of ord(SCANCODE_PAGEUP):
      menuControlY = -9
    of ord(SCANCODE_PAGEDOWN):
      menuControlY = 9
    of ord(SCANCODE_HOME):
      menuControlY = -1000
    of ord(SCANCODE_END):
      menuControlY = 1000
    of ord(SCANCODE_RIGHT):
      menuControlX = 1
    of ord(SCANCODE_LEFT):
      menuControlX = -1
    of ord(SCANCODE_RETURN), ord(SCANCODE_SPACE):
      pressedEnter = true
    of ord(SCANCODE_ESCAPE), ord(SCANCODE_BACKSPACE):
      menuControlBack = 1
    of ord(SCANCODE_F6), ord(SCANCODE_F6) or ord(WITH_SHIFT):
      if Kid.alive < 0:
        needQuickSave = 1
      needCloseMenu = true
    of ord(SCANCODE_F9), ord(SCANCODE_F9) or ord(WITH_SHIFT):
      needQuickLoad = 1
      needCloseMenu = true
    else:
      if (keyT and ord(WITH_CTRL)) != 0:
        needCloseMenu = true
        # Allow Ctrl+R, etc
        return keyT

    return 0

  type
    RwProcessFuncType = proc(rw: ptr RWops, data: pointer,
        dataSize: csize_t): csize_t

  # For serializing/unserializing options in the in-game settings menu
  template process(x: typed) =
    if processFunc(rw, addr(x), csize_t(sizeof(x))) == 0:
      return
  proc processIngameSettingsUserManaged(rw: ptr RWops,
      processFunc: RwProcessFuncType) =
    process(enablePauseMenu)
    process(enableInfoScreen)
    process(isSoundOn)
    process(enableMusic)
    process(enableControllerRumble)
    process(joystickThreshold)
    process(joystickOnlyHorizontal)
    process(enableReplay)
    process(startFullscreen)
    process(useCorrectAspectRatio)
    process(useIntegerScaling)
    process(scalingType)
    process(enableFade)
    process(enableFlash)
    when(USE_LIGHTING):
      process(enableLighting)

  proc processIngameSettingsModManaged(rw: ptr RWops,
      processFunc: RwProcessFuncType) =
    process(enableCopyprot)
    process(enableQuicksave)
    process(enableQuicksavePenalty)
    process(useFixesAndEnhancements)
    process(fixesSaved)
    process(useCustomOptions)
    process(customSaved)

  # CRC-32 implementation uses crc32 nim module
  var
    exeCrc: dword = 0

  proc saveIngameSettings() =
    var
      rw: ptr RWops = rwFromFile(locateFile("SDLPoP.cfg"), "wb")
    if rw != nil:
      var
        levelsetNameLength: byte = byte(levelsetName.len)
        crcStr: string = paramStr(0)
      crc32FromFile(crcStr)
      exeCrc = uint32(parseHexInt(crcStr))
      discard rwWrite(rw, addr(exeCrc), csize_t(sizeof(exeCrc)), 1)
      discard rwWrite(rw, addr(levelsetNameLength), csize_t(sizeof(
          levelsetNameLength)), 1)
      processIngameSettingsUserManaged(rw, processRwWrite)
      processIngameSettingsModManaged(rw, processRwWrite)
      discard rwClose(rw)

  proc loadIngameSettings() =
    # We want the SDLPoP.cfg file (in-game menu settings) to override the
    # SDLPoP.ini file, but ONLY if the .ini file wasn't modified since the last
    # time the .cfg file was saved!
    let
      cfgFilename: string = locateFile("SDLPoP.cfg")
      iniFilename: string = locateFile("SDLPoP.ini")
    if fileExists(cfgFilename) and fileExists(iniFilename):
      if getLastModificationTime(iniFilename) > getLastModificationTime(cfgFilename):
        # SDLPoP.ini is newer than SDLPoP.cfg, so just go with the .ini
        # configuration
        return

    # If there is a SDLPoP.cfg file, let it override the settings
    var
      rw: ptr RWops = rwFromFile(cfgFilename, "rb")

    if rw != nil:
      # SDLPoP.cfg should be invalidated if the prince executable changes.
      # This allows us not to worry about future and backward compatibility
      # of this file.
      var
        expectedCrc: dword = 0
        crcStr: string = paramStr(0)
      crc32FromFile(crcStr)
      exeCrc = uint32(parseHexInt(crcStr))
      discard rwRead(rw, addr(expectedCrc), csize_t(sizeof(expectedCrc)), 1)
      if exeCrc == expectedCrc:
        var
          cfgLevelsetNameLength: byte
          cfgLevelsetName: array[0..255, char]
        discard rwRead(rw, addr(cfgLevelsetNameLength), csize_t(sizeof(
            cfgLevelsetNameLength)), 1)
        discard rwRead(rw, addr(cfgLevelsetName), csize_t(
            cfgLevelsetNameLength), 1)
        # Load the settings
        processIngameSettingsUserManaged(rw, processRwRead)
        # For mod-managed settings: discard the CFG settings when switching to
        # different mod.
        var
          levelsetEqual: bool = true
        if levelsetName.len < 256:
          for i in 0..<levelsetName.len:
            if levelsetName[i] != cfgLevelsetName[i]:
              levelsetEqual = false
              break
        if levelsetEqual:
          processIngameSettingsModManaged(rw, processRwRead)

      discard rwClose(rw)

  proc menuWasClosed() =
    isPaused = 0
    isMenuShown = 0
    escapeKeySuppressed = (keyStates[SCANCODE_BACKSPACE] or keyStates[
        SCANCODE_ESCAPE]) != 0
    if wereSettingsChanged:
      saveIngameSettings()
      wereSettingsChanged = false
    # In fullscreen mode, hide the mouse cursor (because it is only needed in the
    # menu).
    var
      flags: dword = getWindowFlags(window)
    if (flags and WINDOW_FULLSCREEN_DESKTOP) != 0:
      discard showCursor(0)
    else:
      discard showCursor(1)

  # Small font (hardcoded).
  # The alphanumeric characters were adapted from the freeware font '04b_03' by
  # Yuji Oshimoto. See: http://www.04.jp.org/
  template BINARY_8(b7, b6, b5, b4, b3, b2, b1, b0: typed): untyped =
    (b0 or (b1 shl 1) or (b2 shl 2) or (b3 shl 3) or (b4 shl 4) or (b5 shl 5) or
        (b6 shl 6) or (b7 shl 7))
  template BINARY_4(b7, b6, b5, b4: typed): untyped =
    ((b4 shl 4) or (b5 shl 5) or (b6 shl 6) or (b7 shl 7))
  # template wordX(x: untyped): untyped =
  #   byte(x), byte(x shr 8)
  # template IMAGE_DATA(height, width, flags: typed): untyped =
  #   wordX(height), wordX(width), wordX(flags)

  var
    hcSmallFontData: seq[byte] = @[
      32'u8, 126'u8, byte(5), byte(5 shr 8), byte(2), byte(2 shr 8), byte(1),
          byte(1 shr 8), byte(1), byte(1 shr 8),

        # offsets (will be initialized at run-time)
      byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),
          byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(
          0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0),
          byte(0 shr 8), byte(0), byte(0 shr 8),               # 41
      byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),
          byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(
          0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0),
          byte(0 shr 8), byte(0), byte(0 shr 8),               # 51
      byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),
          byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(
          0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0),
          byte(0 shr 8), byte(0), byte(0 shr 8),               # 61
      byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),
          byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(
          0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0),
          byte(0 shr 8), byte(0), byte(0 shr 8),               # 71
      byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),
          byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(
          0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0),
          byte(0 shr 8), byte(0), byte(0 shr 8),               # 81
      byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),
          byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(
          0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0),
          byte(0 shr 8), byte(0), byte(0 shr 8),               # 91
      byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),
          byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(
          0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0),
          byte(0 shr 8), byte(0), byte(0 shr 8),               # 101
      byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),
          byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(
          0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0),
          byte(0 shr 8), byte(0), byte(0 shr 8),               # 111
      byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),
          byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(
          0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0),
          byte(0 shr 8), byte(0), byte(0 shr 8),               # 121
      byte(0), byte(0 shr 8), byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),
          byte(0), byte(0 shr 8), byte(0), byte(0 shr 8),      # 126

      byte(1), byte(1 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # space
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), byte(1), byte(1 shr 8), # !
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # "
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), # #
      BINARY_8(0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),

      byte(6), byte(6 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # $
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(6), byte(6 shr 8), byte(1), byte(1 shr 8), # %
      BINARY_8(0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 0'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 0'u8, 1'u8, 0'u8, 1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 0'u8, 0'u8, 1'u8, 1'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), # &
      BINARY_8(0'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),

      byte(2), byte(2 shr 8), byte(1), byte(1 shr 8), byte(1), byte(1 shr 8), # '
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # (
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # )
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(4), byte(4 shr 8), byte(5), byte(5 shr 8), byte(1), byte(1 shr 8),
      BINARY_8(0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8), # *
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),

      byte(4), byte(4 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # +
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(6), byte(6 shr 8), byte(2), byte(2 shr 8), byte(1), byte(1 shr 8), # ,
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(3), byte(3 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # -
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), byte(1), byte(1 shr 8), # .
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # /
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # 0
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(2), byte(2 shr 8), byte(1), byte(1 shr 8), # 1
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # 2
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # 3
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # 4
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # 5
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # 6
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # 7
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # 8
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # 9
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), byte(1), byte(1 shr 8), # :
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(6), byte(6 shr 8), byte(2), byte(2 shr 8), byte(1), byte(1 shr 8), # ;
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # <
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),

      byte(4), byte(4 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # =
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # >
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # ?
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),

      byte(6), byte(6 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # @
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # A
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # B
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # C
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # D
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # E
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # F
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # G
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # H
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # I
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # J
      BINARY_4(0'u8, 0'u8, 1'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # K
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # L
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), # M
      BINARY_8(1'u8, 0'u8, 0'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 0'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 0'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 0'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # N
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # O
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # P
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(6), byte(6 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # Q
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # R
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # S
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # T
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # U
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # V
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(5), byte(5 shr 8), byte(1), byte(1 shr 8),
      BINARY_8(1'u8, 0'u8, 0'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8), # W
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # X
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # Y
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # Z
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(2), byte(2 shr 8), byte(1), byte(1 shr 8), # [
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # '\'
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # ]
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),

      byte(2), byte(2 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # ^
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # _
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(2), byte(2 shr 8), byte(2), byte(2 shr 8), byte(1), byte(1 shr 8), # `
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # a
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # b
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # c
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # d
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # e
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # f
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(7), byte(7 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # g
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # h
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), byte(1), byte(1 shr 8), # i
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(7), byte(7 shr 8), byte(2), byte(2 shr 8), byte(1), byte(1 shr 8), # j
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # k
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), byte(1), byte(1 shr 8), # l
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), # m
      BINARY_8(0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # n
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # o
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(7), byte(7 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # p
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(7), byte(7 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # q
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # r
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # s
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 1'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # t
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # u
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # v
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), # w
      BINARY_8(0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8), # x
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),

      byte(7), byte(7 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # y
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 1'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # z
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 1'u8, 1'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # {
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 1'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(1), byte(1 shr 8), byte(1), byte(1 shr 8), # |
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # }
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 0'u8),
      BINARY_4(1'u8, 0'u8, 0'u8, 0'u8),

      byte(5), byte(5 shr 8), byte(4), byte(4 shr 8), byte(1), byte(1 shr 8), # ~
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 1'u8, 0'u8, 1'u8),
      BINARY_4(1'u8, 0'u8, 1'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_4(0'u8, 0'u8, 0'u8, 0'u8)
    ]
  arrowHeadUpImageData = @[
      byte(4), byte(4 shr 8), byte(7), byte(7 shr 8), byte(1), byte(1 shr 8),
      BINARY_8(0'u8, 0'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 0'u8, 1'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 0'u8)
    ]
  arrowHeadDownImageData = @[
      byte(4), byte(4 shr 8), byte(7), byte(7 shr 8), byte(1), byte(1 shr 8),
      BINARY_8(1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 0'u8, 1'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 0'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8)
    ]
  arrowHeadLeftImageData = @[
      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8),
      BINARY_8(0'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(0'u8, 0'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8)
    ]
  arrowHeadRightImageData = @[
      byte(5), byte(5 shr 8), byte(3), byte(3 shr 8), byte(1), byte(1 shr 8),
      BINARY_8(1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8),
      BINARY_8(1'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8)
    ]
