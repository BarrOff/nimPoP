# translation of opl3.h

const
  OPL_WRITEBUF_SIZE = 1024
  OPL_WRITEBUF_DELAY = 2

type
  Bitu = uint
  Bits = uint
  Bit64u = uint64
  Bit64s = int64
  Bit32u = uint32
  Bit32s = int32
  Bit16u = uint16
  Bit16s = int16
  Bit8u = uint8
  Bit8s = int8

  # Channel types
  ChannelEnum = enum
    ch2Op, ch4Op, ch4Op2, chDrum

  Opl3Slot = object
    channel: ptr Opl3Channel
    chip: Opl3Chip
    `out`: Bit16s
    fbmod: Bit16s
    `mod`: ptr Bit16s
    prout: Bit16s
    egRout: Bit16s
    egOut: Bit16s
    egInc: Bit8u
    egGen: Bit8u
    egRate: Bit8u
    egKsl: Bit8u
    trem: ptr Bit8u
    regVib: Bit8u
    regType: Bit8u
    regKsr: Bit8u
    regMult: Bit8u
    regKsl: Bit8u
    regTl: Bit8u
    regAr: Bit8u
    regDr: Bit8u
    regSl: Bit8u
    regRr: Bit8u
    regWf: Bit8u
    key: Bit8u
    pgPhase: Bit32u
    timer: Bit32u

  Opl3Channel = object
    slots: array[2, ptr Opl3Slot]
    pair: ptr Opl3Channel
    chip: Opl3Chip
    `out`: array[4, ptr Bit16s]
    chtype: ChannelEnum
    fNum: Bit16u
    `block`: Bit8u
    fb: Bit8u
    con: Bit8u
    alg: Bit8u
    ksv: Bit8u
    cha, chb: Bit16u

  Opl3Writebuf = object
    time: Bit64u
    reg: Bit16u
    data: Bit8u

  Opl3Chip* = ref Opl3ChipObject
  Opl3ChipObject = object
    channel: array[18, Opl3Channel]
    slot: array[36, Opl3Slot]
    timer: Bit16u
    newm: Bit8u
    nts: Bit8u
    rhy: Bit8u
    vibpos: Bit8u
    vibshift: Bit8u
    tremolo: Bit8u
    tremolopos: Bit8u
    tremoloshift: Bit8u
    noise: Bit32u
    zeromod: Bit16s
    mixbuff: array[2, Bit32s]
    # OPL3L
    rateratio: Bit32s
    samplecnt: Bit32s
    oldsamples: array[2, Bit16s]
    samples: array[2, Bit16s]

    writebufSamplecnt: Bit64u
    writebufCur: Bit32u
    writebufLast: Bit32u
    writebufLasttime: Bit64u
    writebuf: array[0..OPL_WRITEBUF_SIZE, Opl3Writebuf]

proc opl3Generate(chip: Opl3Chip, buf: ptr Bit16s)
proc opl3GenerateResampled(chip: Opl3Chip, buf: ptr Bit16s)
proc opl3Reset*(chip: Opl3Chip, samplerate: Bit32u)
proc opl3WriteReg*(chip: Opl3Chip, reg: Bit16u, v: Bit8u)
proc opl3WriteRegBuffered(chip: Opl3Chip, reg: Bit16u, v: Bit8u)
# proc opl3GenerateStream(chip: Opl3Chip, sndptr: ptr Bit16s,
# change ptr to seq
proc opl3GenerateStream*(chip: Opl3Chip, sndptr: var seq[Bit16s],
    numsamples: Bit32u)







# translation of opl3.c

const
  RSM_FRAC = 10

type
  # Envelope key types
  KeyEnum = enum
    egkNorm, egkDrum

  EnvelopeSinfunc = proc(phase: Bit16u, envelope: Bit16u): Bit16s
  EnvelopeGenfunc = proc(slot: var Opl3Slot)

  EnvelopeGenNum = enum
    envelopeGenNumOff,
    envelopeGenNumAttack,
    envelopeGenNumDecay,
    envelopeGenNumSustain,
    envelopeGenNumRelease

let
  logsinrom: array[0..255, Bit16u] = [
    0x859'u16, 0x6c3'u16, 0x607'u16, 0x58b'u16, 0x52e'u16, 0x4e4'u16, 0x4a6'u16,
    0x471'u16,
    0x443'u16, 0x41a'u16, 0x3f5'u16, 0x3d3'u16, 0x3b5'u16, 0x398'u16, 0x37e'u16,
    0x365'u16,
    0x34e'u16, 0x339'u16, 0x324'u16, 0x311'u16, 0x2ff'u16, 0x2ed'u16, 0x2dc'u16,
    0x2cd'u16,
    0x2bd'u16, 0x2af'u16, 0x2a0'u16, 0x293'u16, 0x286'u16, 0x279'u16, 0x26d'u16,
    0x261'u16,
    0x256'u16, 0x24b'u16, 0x240'u16, 0x236'u16, 0x22c'u16, 0x222'u16, 0x218'u16,
    0x20f'u16,
    0x206'u16, 0x1fd'u16, 0x1f5'u16, 0x1ec'u16, 0x1e4'u16, 0x1dc'u16, 0x1d4'u16,
    0x1cd'u16,
    0x1c5'u16, 0x1be'u16, 0x1b7'u16, 0x1b0'u16, 0x1a9'u16, 0x1a2'u16, 0x19b'u16,
    0x195'u16,
    0x18f'u16, 0x188'u16, 0x182'u16, 0x17c'u16, 0x177'u16, 0x171'u16, 0x16b'u16,
    0x166'u16,
    0x160'u16, 0x15b'u16, 0x155'u16, 0x150'u16, 0x14b'u16, 0x146'u16, 0x141'u16,
    0x13c'u16,
    0x137'u16, 0x133'u16, 0x12e'u16, 0x129'u16, 0x125'u16, 0x121'u16, 0x11c'u16,
    0x118'u16,
    0x114'u16, 0x10f'u16, 0x10b'u16, 0x107'u16, 0x103'u16, 0x0ff'u16, 0x0fb'u16,
    0x0f8'u16,
    0x0f4'u16, 0x0f0'u16, 0x0ec'u16, 0x0e9'u16, 0x0e5'u16, 0x0e2'u16, 0x0de'u16,
    0x0db'u16,
    0x0d7'u16, 0x0d4'u16, 0x0d1'u16, 0x0cd'u16, 0x0ca'u16, 0x0c7'u16, 0x0c4'u16,
    0x0c1'u16,
    0x0be'u16, 0x0bb'u16, 0x0b8'u16, 0x0b5'u16, 0x0b2'u16, 0x0af'u16, 0x0ac'u16,
    0x0a9'u16,
    0x0a7'u16, 0x0a4'u16, 0x0a1'u16, 0x09f'u16, 0x09c'u16, 0x099'u16, 0x097'u16,
    0x094'u16,
    0x092'u16, 0x08f'u16, 0x08d'u16, 0x08a'u16, 0x088'u16, 0x086'u16, 0x083'u16,
    0x081'u16,
    0x07f'u16, 0x07d'u16, 0x07a'u16, 0x078'u16, 0x076'u16, 0x074'u16, 0x072'u16,
    0x070'u16,
    0x06e'u16, 0x06c'u16, 0x06a'u16, 0x068'u16, 0x066'u16, 0x064'u16, 0x062'u16,
    0x060'u16,
    0x05e'u16, 0x05c'u16, 0x05b'u16, 0x059'u16, 0x057'u16, 0x055'u16, 0x053'u16,
    0x052'u16,
    0x050'u16, 0x04e'u16, 0x04d'u16, 0x04b'u16, 0x04a'u16, 0x048'u16, 0x046'u16,
    0x045'u16,
    0x043'u16, 0x042'u16, 0x040'u16, 0x03f'u16, 0x03e'u16, 0x03c'u16, 0x03b'u16,
    0x039'u16,
    0x038'u16, 0x037'u16, 0x035'u16, 0x034'u16, 0x033'u16, 0x031'u16, 0x030'u16,
    0x02f'u16,
    0x02e'u16, 0x02d'u16, 0x02b'u16, 0x02a'u16, 0x029'u16, 0x028'u16, 0x027'u16,
    0x026'u16,
    0x025'u16, 0x024'u16, 0x023'u16, 0x022'u16, 0x021'u16, 0x020'u16, 0x01f'u16,
    0x01e'u16,
    0x01d'u16, 0x01c'u16, 0x01b'u16, 0x01a'u16, 0x019'u16, 0x018'u16, 0x017'u16,
    0x017'u16,
    0x016'u16, 0x015'u16, 0x014'u16, 0x014'u16, 0x013'u16, 0x012'u16, 0x011'u16,
    0x011'u16,
    0x010'u16, 0x00f'u16, 0x00f'u16, 0x00e'u16, 0x00d'u16, 0x00d'u16, 0x00c'u16,
    0x00c'u16,
    0x00b'u16, 0x00a'u16, 0x00a'u16, 0x009'u16, 0x009'u16, 0x008'u16, 0x008'u16,
    0x007'u16,
    0x007'u16, 0x007'u16, 0x006'u16, 0x006'u16, 0x005'u16, 0x005'u16, 0x005'u16,
    0x004'u16,
    0x004'u16, 0x004'u16, 0x003'u16, 0x003'u16, 0x003'u16, 0x002'u16, 0x002'u16,
    0x002'u16,
    0x002'u16, 0x001'u16, 0x001'u16, 0x001'u16, 0x001'u16, 0x001'u16, 0x001'u16,
    0x001'u16,
    0x000'u16, 0x000'u16, 0x000'u16, 0x000'u16, 0x000'u16, 0x000'u16, 0x000'u16, 0x000'u16
  ]
  exprom: array[0..255, Bit16u] = [
  0x000'u16, 0x003'u16, 0x006'u16, 0x008'u16, 0x00b'u16, 0x00e'u16, 0x011'u16,
  0x014'u16,
    0x016'u16, 0x019'u16, 0x01c'u16, 0x01f'u16, 0x022'u16, 0x025'u16, 0x028'u16,
  0x02a'u16,
    0x02d'u16, 0x030'u16, 0x033'u16, 0x036'u16, 0x039'u16, 0x03c'u16, 0x03f'u16,
  0x042'u16,
    0x045'u16, 0x048'u16, 0x04b'u16, 0x04e'u16, 0x051'u16, 0x054'u16, 0x057'u16,
  0x05a'u16,
    0x05d'u16, 0x060'u16, 0x063'u16, 0x066'u16, 0x069'u16, 0x06c'u16, 0x06f'u16,
  0x072'u16,
    0x075'u16, 0x078'u16, 0x07b'u16, 0x07e'u16, 0x082'u16, 0x085'u16, 0x088'u16,
  0x08b'u16,
    0x08e'u16, 0x091'u16, 0x094'u16, 0x098'u16, 0x09b'u16, 0x09e'u16, 0x0a1'u16,
  0x0a4'u16,
    0x0a8'u16, 0x0ab'u16, 0x0ae'u16, 0x0b1'u16, 0x0b5'u16, 0x0b8'u16, 0x0bb'u16,
  0x0be'u16,
    0x0c2'u16, 0x0c5'u16, 0x0c8'u16, 0x0cc'u16, 0x0cf'u16, 0x0d2'u16, 0x0d6'u16,
  0x0d9'u16,
    0x0dc'u16, 0x0e0'u16, 0x0e3'u16, 0x0e7'u16, 0x0ea'u16, 0x0ed'u16, 0x0f1'u16,
  0x0f4'u16,
    0x0f8'u16, 0x0fb'u16, 0x0ff'u16, 0x102'u16, 0x106'u16, 0x109'u16, 0x10c'u16,
  0x110'u16,
    0x114'u16, 0x117'u16, 0x11b'u16, 0x11e'u16, 0x122'u16, 0x125'u16, 0x129'u16,
  0x12c'u16,
    0x130'u16, 0x134'u16, 0x137'u16, 0x13b'u16, 0x13e'u16, 0x142'u16, 0x146'u16,
  0x149'u16,
    0x14d'u16, 0x151'u16, 0x154'u16, 0x158'u16, 0x15c'u16, 0x160'u16, 0x163'u16,
  0x167'u16,
    0x16b'u16, 0x16f'u16, 0x172'u16, 0x176'u16, 0x17a'u16, 0x17e'u16, 0x181'u16,
  0x185'u16,
    0x189'u16, 0x18d'u16, 0x191'u16, 0x195'u16, 0x199'u16, 0x19c'u16, 0x1a0'u16,
  0x1a4'u16,
    0x1a8'u16, 0x1ac'u16, 0x1b0'u16, 0x1b4'u16, 0x1b8'u16, 0x1bc'u16, 0x1c0'u16,
  0x1c4'u16,
    0x1c8'u16, 0x1cc'u16, 0x1d0'u16, 0x1d4'u16, 0x1d8'u16, 0x1dc'u16, 0x1e0'u16,
  0x1e4'u16,
    0x1e8'u16, 0x1ec'u16, 0x1f0'u16, 0x1f5'u16, 0x1f9'u16, 0x1fd'u16, 0x201'u16,
  0x205'u16,
    0x209'u16, 0x20e'u16, 0x212'u16, 0x216'u16, 0x21a'u16, 0x21e'u16, 0x223'u16,
  0x227'u16,
    0x22b'u16, 0x230'u16, 0x234'u16, 0x238'u16, 0x23c'u16, 0x241'u16, 0x245'u16,
  0x249'u16,
    0x24e'u16, 0x252'u16, 0x257'u16, 0x25b'u16, 0x25f'u16, 0x264'u16, 0x268'u16,
  0x26d'u16,
    0x271'u16, 0x276'u16, 0x27a'u16, 0x27f'u16, 0x283'u16, 0x288'u16, 0x28c'u16,
  0x291'u16,
    0x295'u16, 0x29a'u16, 0x29e'u16, 0x2a3'u16, 0x2a8'u16, 0x2ac'u16, 0x2b1'u16,
  0x2b5'u16,
    0x2ba'u16, 0x2bf'u16, 0x2c4'u16, 0x2c8'u16, 0x2cd'u16, 0x2d2'u16, 0x2d6'u16,
  0x2db'u16,
    0x2e0'u16, 0x2e5'u16, 0x2e9'u16, 0x2ee'u16, 0x2f3'u16, 0x2f8'u16, 0x2fd'u16,
  0x302'u16,
    0x306'u16, 0x30b'u16, 0x310'u16, 0x315'u16, 0x31a'u16, 0x31f'u16, 0x324'u16,
  0x329'u16,
    0x32e'u16, 0x333'u16, 0x338'u16, 0x33d'u16, 0x342'u16, 0x347'u16, 0x34c'u16,
  0x351'u16,
    0x356'u16, 0x35b'u16, 0x360'u16, 0x365'u16, 0x36a'u16, 0x370'u16, 0x375'u16,
  0x37a'u16,
    0x37f'u16, 0x384'u16, 0x38a'u16, 0x38f'u16, 0x394'u16, 0x399'u16, 0x39f'u16,
  0x3a4'u16,
    0x3a9'u16, 0x3ae'u16, 0x3b4'u16, 0x3b9'u16, 0x3bf'u16, 0x3c4'u16, 0x3c9'u16,
  0x3cf'u16,
    0x3d4'u16, 0x3da'u16, 0x3df'u16, 0x3e4'u16, 0x3ea'u16, 0x3ef'u16, 0x3f5'u16, 0x3fa'u16
  ]
  mt: array[0..15, Bit8u] = [
    1'u8, 2'u8, 4'u8, 6'u8, 8'u8, 10'u8, 12'u8, 14'u8,
    16'u8, 18'u8, 20'u8, 20'u8, 24'u8, 24'u8, 30'u8, 30'u8
  ]
  # ksl table
  kslrom: array[0..15, Bit8u] = [
    0'u8, 32'u8, 40'u8, 45'u8, 48'u8, 51'u8, 53'u8, 55'u8,
    56'u8, 58'u8, 59'u8, 60'u8, 61'u8, 62'u8, 63'u8, 64'u8
  ]
  kslshift: array[0..3, Bit8u] = [
    8'u8, 1'u8, 2'u8, 0'u8
  ]

  # envelope generator constants
  egIncstep: array[0..2, array[0..3, array[0..7, Bit8u]]] = [
  [
    [0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8],
    [0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8],
    [0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8],
    [0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8, 0'u8]
  ],
  [
    [0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 1'u8, 0'u8, 1'u8],
    [0'u8, 1'u8, 0'u8, 1'u8, 1'u8, 1'u8, 0'u8, 1'u8],
    [0'u8, 1'u8, 1'u8, 1'u8, 0'u8, 1'u8, 1'u8, 1'u8],
    [0'u8, 1'u8, 1'u8, 1'u8, 0'u8, 1'u8, 1'u8, 1'u8]
  ],
  [
    [1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8],
    [2'u8, 2'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8],
    [2'u8, 2'u8, 1'u8, 1'u8, 2'u8, 2'u8, 1'u8, 1'u8],
    [2'u8, 2'u8, 2'u8, 2'u8, 2'u8, 2'u8, 1'u8, 1'u8]
  ]
  ]
  egIncdesc: array[0..15, Bit8u] = [
    0'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8, 1'u8,
    1'u8, 2'u8, 2'u8, 2'u8
  ]
  egIncsh: array[0..15, Bit8s] = [
    0'i8, 11'i8, 10'i8, 9'i8, 8'i8, 7'i8, 6'i8, 5'i8, 4'i8, 3'i8, 2'i8, 1'i8,
    0'i8, 0'i8, -1'i8, -2'i8
  ]

  # address decoding
  adSlot: array[0..31, Bit8s] = [
    0'i8, 1'i8, 2'i8, 3'i8, 4'i8, 5'i8, -1'i8, -1'i8,
    6'i8, 7'i8, 8'i8, 9'i8, 10'i8, 11'i8, -1'i8, -1'i8,
    12'i8, 13'i8, 14'i8, 15'i8, 16'i8, 17'i8, -1'i8, -1'i8,
    -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, -1'i8, -1'i8
  ]
  chSlot: array[0..17, Bit8u] = [
    0'u8, 1'u8, 2'u8, 6'u8, 7'u8, 8'u8, 12'u8, 13'u8, 14'u8,
    18'u8, 19'u8, 20'u8, 24'u8, 25'u8, 26'u8, 30'u8, 31'u8, 32'u8
  ]

# Envelope generator
proc opl3EnvelopeCalcExp(level: Bit32u): Bit16s =
  if level > 0x1fff'u32:
    return Bit16s(((exprom[(0x1fff and 0xff) xor 0xff] or 0x400) shl 1) shr (
        0x1fff'u32 shr 8))

  return Bit16s(((exprom[(level and 0xff) xor 0xff] or 0x400) shl 1) shr (level shr 8))

proc opl3EnvelopeCalcSin0(phase: Bit16u, envelope: Bit16u): Bit16s =
  var
    outB: Bit16u = 0
    neg: Bit16s = 0
    phaseIntern: Bit16u = phase and 0x3ff
  if (phaseIntern and 0x200) != 0:
    neg = not(neg)

  if (phaseIntern and 0x100) != 0:
    outB = logsinrom[(phaseIntern and 0xff) xor 0xff]
  else:
    outB = logsinrom[phaseIntern and 0xff]

  return opl3EnvelopeCalcExp(outB + (envelope shl 3)) xor neg


proc opl3EnvelopeCalcSin1(phase: Bit16u, envelope: Bit16u): Bit16s =
  var
    outB: Bit16u = 0
    phaseIntern: Bit16u = phase and 0x3ff
  if (phaseIntern and 0x200) != 0:
    outB = 0x1000
  elif (phaseIntern and 0x100) != 0:
    outB = logsinrom[(phaseIntern and 0xff) xor 0xff]
  else:
    outB = logsinrom[phaseIntern and 0xff]

  return opl3EnvelopeCalcExp(outB + (envelope shl 3))

proc opl3EnvelopeCalcSin2(phase: Bit16u, envelope: Bit16u): Bit16s =
  var
    outB: Bit16u = 0
    phaseIntern: Bit16u = phase and 0x3ff
  if (phaseIntern and 0x100) != 0:
    outB = logsinrom[(phaseIntern and 0xff) xor 0xff]
  else:
    outB = logsinrom[phaseIntern and 0xff]

  return opl3EnvelopeCalcExp(outB + (envelope shl 3))

proc opl3EnvelopeCalcSin3(phase: Bit16u, envelope: Bit16u): Bit16s =
  var
    outB: Bit16u = 0
    phaseIntern: Bit16u = phase and 0x3ff
  if (phaseIntern and 0x100) != 0:
    outB = 0x1000
  else:
    outB = logsinrom[phaseIntern and 0xff]

  return opl3EnvelopeCalcExp(outB + (envelope shl 3))

proc opl3EnvelopeCalcSin4(phase: Bit16u, envelope: Bit16u): Bit16s =
  var
    outB: Bit16u = 0
    neg: Bit16s = 0
    phaseIntern: Bit16u = phase and 0x3ff
  if (phaseIntern and 0x300) == 0x100:
    neg = not(neg)

  if (phaseIntern and 0x200) != 0:
    outB = 0x1000
  elif (phaseIntern and 0x80) != 0:
    outB = logsinrom[((phaseIntern xor 0xff) shl 1) and 0xff]
  else:
    outB = logsinrom[(phaseIntern shl 1) and 0xff]

  return opl3EnvelopeCalcExp(outB + (envelope shl 3)) xor neg

proc opl3EnvelopeCalcSin5(phase: Bit16u, envelope: Bit16u): Bit16s =
  var
    outB: Bit16u = 0
    phaseIntern: Bit16u = phase and 0x3ff
  if (phaseIntern and 0x200) != 0:
    outB = 0x1000
  elif (phaseIntern and 0x80) != 0:
    outB = logsinrom[((phaseIntern xor 0xff) shl 1) and 0xff]
  else:
    outB = logsinrom[(phaseIntern shl 1) and 0xff]

  return opl3EnvelopeCalcExp(outB + (envelope shl 3))

proc opl3EnvelopeCalcSin6(phase: Bit16u, envelope: Bit16u): Bit16s =
  var
    neg: Bit16s = 0
    phaseIntern: Bit16u = phase and 0x3ff
  if (phaseIntern and 0x200) != 0:
    neg = not(neg)

  return opl3EnvelopeCalcExp(envelope shl 3) xor neg

proc opl3EnvelopeCalcSin7(phase: Bit16u, envelope: Bit16u): Bit16s =
  var
    outB: Bit16u = 0
    neg: Bit16s = 0
    phaseIntern: Bit16u = phase and 0x3ff
  if (phaseIntern and 0x200) != 0:
    neg = not(neg)
    phaseIntern = (phaseIntern and 0x1ff) xor 0x1ff

  outB = phaseIntern shl 3

  return opl3EnvelopeCalcExp(outB + (envelope shl 3)) xor neg

let
  EnvelopeCalcSin0: EnvelopeSinfunc = opl3EnvelopeCalcSin0
  EnvelopeCalcSin1: EnvelopeSinfunc = opl3EnvelopeCalcSin1
  EnvelopeCalcSin2: EnvelopeSinfunc = opl3EnvelopeCalcSin2
  EnvelopeCalcSin3: EnvelopeSinfunc = opl3EnvelopeCalcSin3
  EnvelopeCalcSin4: EnvelopeSinfunc = opl3EnvelopeCalcSin4
  EnvelopeCalcSin5: EnvelopeSinfunc = opl3EnvelopeCalcSin5
  EnvelopeCalcSin6: EnvelopeSinfunc = opl3EnvelopeCalcSin6
  EnvelopeCalcSin7: EnvelopeSinfunc = opl3EnvelopeCalcSin7
  envelopeSin: array[8, EnvelopeSinfunc] = [
    EnvelopeCalcSin0,
    EnvelopeCalcSin1,
    EnvelopeCalcSin2,
    EnvelopeCalcSin3,
    EnvelopeCalcSin4,
    EnvelopeCalcSin5,
    EnvelopeCalcSin6,
    EnvelopeCalcSin7
  ]

proc opl3EnvelopeGenOff(slot: var Opl3Slot)
proc opl3EnvelopeGenAttack(slot: var Opl3Slot)
proc opl3EnvelopeGenDecay(slot: var Opl3Slot)
proc opl3EnvelopeGenSustain(slot: var Opl3Slot)
proc opl3EnvelopeGenRelease(slot: var Opl3Slot)

let
  EnvelopeGenOff: EnvelopeGenfunc = opl3EnvelopeGenOff
  EnvelopeGenAttack: EnvelopeGenfunc = opl3EnvelopeGenAttack
  EnvelopeGenDecay: EnvelopeGenfunc = opl3EnvelopeGenDecay
  EnvelopeGenSustain: EnvelopeGenfunc = opl3EnvelopeGenSustain
  EnvelopeGenRelease: EnvelopeGenfunc = opl3EnvelopeGenRelease
  envelopeGen: array[5, EnvelopeGenfunc] = [
    EnvelopeGenOff,
    EnvelopeGenAttack,
    EnvelopeGenDecay,
    EnvelopeGenSustain,
    EnvelopeGenRelease
  ]

proc opl3EnvelopeCalcRate(slot: var Opl3Slot, regRate: Bit8u): Bit8u =
  if regRate == 0x00'u8:
    return 0x00'u8

  if (bool)(slot.regKsr):
    result = (regRate shl 2) + slot.channel.ksv
  else:
    result = (regRate shl 2) + (slot.channel.ksv shr 2)

  if result > 0x3c'u8:
    result = 0x3c

proc opl3EnvelopeUpdateKSL(slot: var Opl3Slot) =
  var
    ksl: Bit16s = (Bit16s(kslrom[slot.channel.fNum shr 6]) shl 2) - (Bit16s(
        0x08'u8 - slot.channel.`block`) shl 5)

  if ksl < 0:
    ksl = 0

  slot.egKsl = Bit8u(ksl)

proc opl3EnvelopeUpdateRate(slot: var Opl3Slot) =
  case slot.egGen
  of uint8(envelopeGenNumOff), ord(envelopeGenNumAttack):
    slot.egRate = opl3EnvelopeCalcRate(slot, slot.regAr)
  of ord(envelopeGenNumDecay):
    slot.egRate = opl3EnvelopeCalcRate(slot, slot.regDr)
  of uint8(envelopeGenNumSustain), ord(envelopeGenNumRelease):
    slot.egRate = opl3EnvelopeCalcRate(slot, slot.regRr)
  else:
    discard

proc opl3EnvelopeGenOff(slot: var Opl3Slot) =
  slot.egRout = 0x1ff

proc opl3EnvelopeGenAttack(slot: var Opl3Slot) =
  if slot.egRout == 0:
    slot.egGen = ord(envelopeGenNumDecay)
    opl3EnvelopeUpdateRate(slot)
    return
  slot.egRout += (not(slot.egRout) * Bit16s(slot.egInc)) shr 3
  if slot.egRout < 0:
    slot.egRout = 0

proc opl3EnvelopeGenDecay(slot: var Opl3Slot) =
  if slot.egRout >= Bit16s(slot.regSl shl 4):
    slot.egGen = ord(envelopeGenNumSustain)
    opl3EnvelopeUpdateRate(slot)
    return

  slot.egRout += Bit16s(slot.egInc)

proc opl3EnvelopeGenSustain(slot: var Opl3Slot) =
  if slot.regType == 0:
    opl3EnvelopeGenRelease(slot)

proc opl3EnvelopeGenRelease(slot: var Opl3Slot) =
  if slot.egRout == 0x1ff:
    slot.egGen = ord(envelopeGenNumOff)
    slot.egRout = 0x1ff
    opl3EnvelopeUpdateRate(slot)
    return

  slot.egRout += Bit16s(slot.egInc)

proc opl3EnvelopeCalc(slot: var Opl3Slot) =
  var
    rateH, rateL: Bit8u
    incB: Bit8u = 0

  rateH = slot.egRate shr 2
  rateL = slot.egRate and 3'u8

  if egIncsh[rateH] > 0:
    if (slot.chip.timer and ((1'u16 shl egIncsh[rateH]) - 1'u16)) == 0:
      incB = egIncstep[egIncdesc[rateH]][rateL][((slot.chip.timer) shr egIncsh[
          rateH]) and 0x07]
  else:
    incB = egIncstep[egIncdesc[rateH]][rateL][slot.chip.timer and 0x07] shl (
        -egIncsh[rateH])

  slot.egInc = incB
  slot.egOut = slot.egRout +
    (Bit16s(slot.regTl) shl 2) +
    (Bit16s(slot.egKsl) shr kslshift[slot.regKsl]) +
    Bit16s(slot.trem[])
  envelopeGen[slot.egGen](slot)

proc opl3EnvelopeKeyOn(slot: var Opl3Slot, typeB: Bit8u) =
  if slot.key == 0:
    slot.egGen = ord(envelopeGenNumAttack)
    opl3EnvelopeUpdateRate(slot)
    if (slot.egRate shr 2) == 0x0f:
      slot.egGen = ord(envelopeGenNumDecay)
      opl3EnvelopeUpdateRate(slot)
      slot.egRout = 0x00

    slot.pgPhase = 0x00

  slot.key = slot.key or typeB

proc opl3EnvelopeKeyOff(slot: var Opl3Slot, typeB: Bit8u) =
  if slot.key != 0:
    slot.key = slot.key and not(typeB)
    if slot.key == 0:
      slot.egGen = ord(envelopeGenNumRelease)
      opl3EnvelopeUpdateRate(slot)

# Phase Generator

proc opl3PhaseGenerate(slot: var Opl3Slot) =
  var
    fNum: Bit16u
    basefreq: Bit32u

  fNum = slot.channel.fNum
  if slot.regVib != 0:
    var
      rangeB: Bit8s
      vibpos: Bit8u

    rangeB = Bit8s((fNum shr 7) and 7)
    vibpos = slot.chip.vibpos

    if (vibpos and 3) == 0:
      rangeB = 0
    elif (vibpos and 1) != 0:
      rangeb = rangeb shr 1

    rangeB = rangeB shr slot.chip.vibshift

    if (vibpos and 4) != 0:
      rangeB = -rangeB

    fNum += cast[Bit16u](rangeB)

  basefreq = (Bit32u(fNum) shl slot.channel.`block`) shr 1
  slot.pgPhase += (basefreq * mt[slot.regMult]) shr 1

# Noise Generator

proc opl3NoiseGenerate(chip: Opl3Chip) =
  if (chip.noise and 0x01) != 0:
    chip.noise = chip.noise xor 0x800302

  chip.noise = chip.noise shr 1

# Slot

proc opl3SlotWrite20(slot: var Opl3Slot, data: Bit8u) =
  if ((data shr 7) and 0x01) != 0:
    slot.trem = addr(slot.chip.tremolo)
  else:
    slot.trem = cast[ptr Bit8u](addr(slot.chip.zeromod))

  slot.regVib = uint8((data shr 6) and 0x01)
  slot.regType = uint8((data shr 5) and 0x01)
  slot.regKsr = uint8( (data shr 4) and 0x01)
  slot.regMult = uint8(data and 0x0f)
  opl3EnvelopeUpdateRate(slot)

proc opl3SlotWrite40(slot: var Opl3Slot, data: Bit8u) =
  slot.regKsl = uint8((data shr 6) and 0x03)
  slot.regTl = uint8(data and 0x3f)
  opl3EnvelopeUpdateKSL(slot)

proc opl3SlotWrite60(slot: var Opl3Slot, data: Bit8u) =
  slot.regAr = uint8((data shr 4) and 0x0f)
  slot.regDr = uint8(data and 0x0f)
  opl3EnvelopeUpdateRate(slot)

proc opl3SlotWrite80(slot: var Opl3Slot, data: Bit8u) =
  slot.regSl = uint8((data shr 4) and 0x0f)
  if slot.regSl == 0x0f:
    slot.regSl = 0x1f
  slot.regRr = uint8(data and 0x0f)
  opl3EnvelopeUpdateRate(slot)

proc opl3SlotWriteE0(slot: var Opl3Slot, data: Bit8u) =
  slot.regWf = uint8(data and 0x07)
  if slot.chip.newm == 0x00:
    slot.regWf = slot.regWf and 0x03'u8

proc opl3SlotGeneratePhase(slot: var Opl3Slot, phase: Bit16u) =
  slot.`out` = envelopeSin[slot.regWf](phase, Bit16u(slot.egOut))

proc opl3SlotGenerate(slot: var Opl3Slot) =
  opl3SlotGeneratePhase(slot, cast[uint16](int32(slot.pgPhase shr 9) + int32(
      slot.`mod`[])))

proc opl3SlotGenerateZM(slot: var Opl3Slot) =
  opl3SlotGeneratePhase(slot, Bit16u(slot.pgPhase shr 9))

proc opl3SlotCalcFB(slot: var Opl3Slot) =
  if slot.channel.fb != 0x00:
    slot.fbmod = (slot.prout + slot.`out`) shr (0x09 - slot.channel.fb)
  else:
    slot.fbmod = 0
  slot.prout = slot.`out`

# Channel

proc opl3ChannelSetupAlg(channel: var Opl3Channel)

proc opl3ChannelUpdateRhythm(chip: Opl3Chip, data: Bit8u) =
  var
    channel6: ptr Opl3Channel
    channel7: ptr Opl3Channel
    channel8: ptr Opl3Channel
    chnum: Bit8u

  chip.rhy = uint8(data and 0x3f)
  if (chip.rhy and 0x20) != 0:
    channel6 = addr(chip.channel[6])
    channel7 = addr(chip.channel[7])
    channel8 = addr(chip.channel[8])
    channel6.`out`[0] = addr(channel6.slots[1].`out`)
    channel6.`out`[1] = addr(channel6.slots[1].`out`)
    channel6.`out`[2] = addr(chip.zeromod)
    channel6.`out`[3] = addr(chip.zeromod)
    channel7.`out`[0] = addr(channel7.slots[0].`out`)
    channel7.`out`[1] = addr(channel7.slots[0].`out`)
    channel7.`out`[2] = addr(channel7.slots[1].`out`)
    channel7.`out`[3] = addr(channel7.slots[1].`out`)
    channel8.`out`[0] = addr(channel8.slots[0].`out`)
    channel8.`out`[1] = addr(channel8.slots[0].`out`)
    channel8.`out`[2] = addr(channel8.slots[1].`out`)
    channel8.`out`[3] = addr(channel8.slots[1].`out`)
    chip.channel[6].chtype = chDrum
    chip.channel[7].chtype = chDrum
    chip.channel[8].chtype = chDrum
    opl3ChannelSetupAlg(channel6[])
    # hh
    if (chip.rhy and 0x01) != 0:
      opl3EnvelopeKeyOn(channel7.slots[0][], Bit8u(egkDrum))
    else:
      opl3EnvelopeKeyOff(channel7.slots[0][], Bit8u(egkDrum))
    # tc
    if (chip.rhy and 0x02) != 0:
      opl3EnvelopeKeyOn(channel8.slots[1][], Bit8u(egkDrum))
    else:
      opl3EnvelopeKeyOff(channel8.slots[1][], Bit8u(egkDrum))
    # tom
    if (chip.rhy and 0x04) != 0:
      opl3EnvelopeKeyOn(channel8.slots[0][], Bit8u(egkDrum))
    else:
      opl3EnvelopeKeyOff(channel8.slots[0][], Bit8u(egkDrum))
    # sd
    if (chip.rhy and 0x08) != 0:
      opl3EnvelopeKeyOn(channel7.slots[1][], Bit8u(egkDrum))
    else:
      opl3EnvelopeKeyOff(channel7.slots[1][], Bit8u(egkDrum))
    # bd
    if (chip.rhy and 0x10) != 0:
      opl3EnvelopeKeyOn(channel6.slots[0][], Bit8u(egkDrum))
      opl3EnvelopeKeyOn(channel6.slots[1][], Bit8u(egkDrum))
    else:
      opl3EnvelopeKeyOff(channel6.slots[0][], Bit8u(egkDrum))
      opl3EnvelopeKeyOff(channel6.slots[1][], Bit8u(egkDrum))
  else:
    for chnum in 6'u8..<9'u8:
      chip.channel[chnum].chtype = ch2Op
      opl3ChannelSetupAlg(chip.channel[chnum])
      opl3EnvelopeKeyOff(chip.channel[chnum].slots[0][], Bit8u(egkDrum))
      opl3EnvelopeKeyOff(chip.channel[chnum].slots[1][], Bit8u(egkDrum))

proc opl3ChannelWriteA0(channel: var Opl3Channel, data: Bit8u) =
  if (channel.chip.newm != 0) and (channel.chtype == ch4Op2):
    return

  channel.fNum = (channel.fNum and 0x300) or data
  channel.ksv = Bit8u((channel.`block` shl 1) or uint8((channel.fNum shr (
      0x09'u8 - channel.chip.nts)) and 0x01))
  opl3EnvelopeUpdateKSL(channel.slots[0][])
  opl3EnvelopeUpdateKSL(channel.slots[1][])
  opl3EnvelopeUpdateRate(channel.slots[0][])
  opl3EnvelopeUpdateRate(channel.slots[1][])

  if (channel.chip.newm != 0) and (channel.chtype == ch4Op):
    channel.pair.fNum = channel.fNum
    channel.pair.ksv = channel.ksv
    opl3EnvelopeUpdateKSL(channel.pair.slots[0][])
    opl3EnvelopeUpdateKSL(channel.pair.slots[1][])
    opl3EnvelopeUpdateRate(channel.pair.slots[0][])
    opl3EnvelopeUpdateRate(channel.pair.slots[1][])

proc opl3ChannelWriteB0(channel: var Opl3Channel, data: Bit8u) =
  if (channel.chip.newm != 0) and (channel.chtype == ch4Op2):
    return

  channel.fNum = (channel.fNum and 0xff) or (uint16(data and 0x03) shl 8)
  channel.`block` = uint8( (data shr 2) and 0x07)
  channel.ksv = Bit8u((channel.`block` shl 1) or uint8((channel.fNum shr (
      0x09'u8 - channel.chip.nts)) and 0x01))
  opl3EnvelopeUpdateKSL(channel.slots[0][])
  opl3EnvelopeUpdateKSL(channel.slots[1][])
  opl3EnvelopeUpdateRate(channel.slots[0][])
  opl3EnvelopeUpdateRate(channel.slots[1][])

  if (channel.chip.newm != 0) and (channel.chtype == ch4Op):
    channel.pair.fNum = channel.fNum
    channel.pair.`block` = channel.`block`
    channel.pair.ksv = channel.ksv
    opl3EnvelopeUpdateKSL(channel.pair.slots[0][])
    opl3EnvelopeUpdateKSL(channel.pair.slots[1][])
    opl3EnvelopeUpdateRate(channel.pair.slots[0][])
    opl3EnvelopeUpdateRate(channel.pair.slots[1][])

proc opl3ChannelSetupAlg(channel: var Opl3Channel) =
  if channel.chtype == chDrum:
    case channel.alg and 0x01
    of 0x00:
      channel.slots[0].`mod` = addr(channel.slots[0].fbmod)
      channel.slots[1].`mod` = addr(channel.slots[0].`out`)
    of 0x01:
      channel.slots[0].`mod` = addr(channel.slots[0].fbmod)
      channel.slots[1].`mod` = addr(channel.chip.zeromod)
    else:
      discard
    return
  if (channel.alg and 0x08) != 0:
    return
  if (channel.alg and 0x04) != 0:
    channel.pair.`out`[0] = addr(channel.chip.zeromod)
    channel.pair.`out`[1] = addr(channel.chip.zeromod)
    channel.pair.`out`[2] = addr(channel.chip.zeromod)
    channel.pair.`out`[3] = addr(channel.chip.zeromod)
    case channel.alg and 0x03
    of 0x00:
      channel.pair.slots[0].`mod` = addr(channel.pair.slots[0].fbmod)
      channel.pair.slots[1].`mod` = addr(channel.pair.slots[0].`out`)
      channel.slots[0].`mod` = addr(channel.pair.slots[1].`out`)
      channel.slots[1].`mod` = addr(channel.slots[0].`out`)
      channel.`out`[0] = addr(channel.slots[1].`out`)
      channel.`out`[1] = addr(channel.chip.zeromod)
      channel.`out`[2] = addr(channel.chip.zeromod)
      channel.`out`[3] = addr(channel.chip.zeromod)
    of 0x01:
      channel.pair.slots[0].`mod` = addr(channel.pair.slots[0].fbmod)
      channel.pair.slots[1].`mod` = addr(channel.pair.slots[0].`out`)
      channel.slots[0].`mod` = addr(channel.chip.zeromod)
      channel.slots[1].`mod` = addr(channel.slots[0].`out`)
      channel.`out`[0] = addr(channel.pair.slots[1].`out`)
      channel.`out`[1] = addr(channel.chip.zeromod)
      channel.`out`[2] = addr(channel.chip.zeromod)
      channel.`out`[3] = addr(channel.chip.zeromod)
    of 0x02:
      channel.pair.slots[0].`mod` = addr(channel.pair.slots[0].fbmod)
      channel.pair.slots[1].`mod` = addr(channel.chip.zeromod)
      channel.slots[0].`mod` = addr(channel.pair.slots[1].`out`)
      channel.slots[1].`mod` = addr(channel.slots[0].`out`)
      channel.`out`[0] = addr(channel.pair.slots[0].`out`)
      channel.`out`[1] = addr(channel.slots[0].`out`)
      channel.`out`[2] = addr(channel.chip.zeromod)
      channel.`out`[3] = addr(channel.chip.zeromod)
    of 0x03:
      channel.pair.slots[0].`mod` = addr(channel.pair.slots[0].fbmod)
      channel.pair.slots[1].`mod` = addr(channel.chip.zeromod)
      channel.slots[0].`mod` = addr(channel.pair.slots[1].`out`)
      channel.slots[1].`mod` = addr(channel.chip.zeromod)
      channel.`out`[0] = addr(channel.pair.slots[0].`out`)
      channel.`out`[1] = addr(channel.slots[0].`out`)
      channel.`out`[2] = addr(channel.slots[1].`out`)
      channel.`out`[3] = addr(channel.chip.zeromod)
    else:
      discard
  else:
    case channel.alg and 0x01
    of 0x00:
      channel.slots[0].`mod` = addr(channel.slots[0].fbmod)
      channel.slots[1].`mod` = addr(channel.slots[0].`out`)
      channel.`out`[0] = addr(channel.slots[1].`out`)
      channel.`out`[1] = addr(channel.chip.zeromod)
      channel.`out`[2] = addr(channel.chip.zeromod)
      channel.`out`[3] = addr(channel.chip.zeromod)
    of 0x01:
      channel.slots[0].`mod` = addr(channel.slots[0].fbmod)
      channel.slots[1].`mod` = addr(channel.chip.zeromod)
      channel.`out`[0] = addr(channel.slots[0].`out`)
      channel.`out`[1] = addr(channel.slots[1].`out`)
      channel.`out`[2] = addr(channel.chip.zeromod)
      channel.`out`[3] = addr(channel.chip.zeromod)
    else:
      discard

proc opl3ChannelWriteC0(channel: var Opl3Channel, data: Bit8u) =
  channel.fb = uint8( (data and 0x0e) shr 1)
  channel.con = uint8(data and 0x01)
  channel.alg = channel.con
  if (channel.chip.newm) != 0:
    if channel.chtype == ch4Op:
      channel.pair.alg = 0x04'u8 or (channel.con shl 1) or channel.pair.con
      channel.alg = 0x08
      opl3ChannelSetupAlg(channel.pair[])
    elif channel.chtype == ch4Op2:
      channel.alg = 0x04'u8 or (channel.pair.con shl 1) or channel.con
      channel.pair.alg = 0x08
      opl3ChannelSetupAlg(channel)
    else:
      opl3ChannelSetupAlg(channel)
  else:
    opl3ChannelSetupAlg(channel)
  if (channel.chip.newm) != 0:
    channel.cha = if ((data shr 4) and 0x01) != 0: not(0'u16) else: 0'u16
    channel.chb = if ((data shr 5) and 0x01) != 0: not(0'u16) else: 0'u16
  else:
    channel.cha = not(0'u16)
    channel.chb = not(0'u16)

proc opl3ChannelKeyOn(channel: var Opl3Channel) =
  if (channel.chip.newm) != 0:
    if channel.chtype == ch4Op:
      opl3EnvelopeKeyOn(channel.slots[0][], ord(egkNorm))
      opl3EnvelopeKeyOn(channel.slots[1][], ord(egkNorm))
      opl3EnvelopeKeyOn(channel.pair.slots[0][], ord(egkNorm))
      opl3EnvelopeKeyOn(channel.pair.slots[1][], ord(egkNorm))
    elif (channel.chtype == ch2Op) or (channel.chtype == chdrum):
      opl3EnvelopeKeyOn(channel.slots[0][], ord(egkNorm))
      opl3EnvelopeKeyOn(channel.slots[1][], ord(egkNorm))
  else:
    opl3EnvelopeKeyOn(channel.slots[0][], ord(egkNorm))
    opl3EnvelopeKeyOn(channel.slots[1][], ord(egkNorm))

proc opl3ChannelKeyOff(channel: var Opl3Channel) =
  if (channel.chip.newm) != 0:
    if channel.chtype == ch4Op:
      opl3EnvelopeKeyOff(channel.slots[0][], ord(egkNorm))
      opl3EnvelopeKeyOff(channel.slots[1][], ord(egkNorm))
      opl3EnvelopeKeyOff(channel.pair.slots[0][], ord(egkNorm))
      opl3EnvelopeKeyOff(channel.pair.slots[1][], ord(egkNorm))
    elif (channel.chtype == ch2Op) or (channel.chtype == chdrum):
      opl3EnvelopeKeyOff(channel.slots[0][], ord(egkNorm))
      opl3EnvelopeKeyOff(channel.slots[1][], ord(egkNorm))
  else:
    opl3EnvelopeKeyOff(channel.slots[0][], ord(egkNorm))
    opl3EnvelopeKeyOff(channel.slots[1][], ord(egkNorm))

proc opl3ChannelSet4Op(chip: Opl3Chip, data: Bit8u) =
  var
    chnum: Bit8u
  for bit in 0'u8..<6'u8:
    chnum = bit
    if bit >= 3:
      chnum += 9 - 3
    if ((data shr bit) and 0x01) != 0:
      chip.channel[chnum].chtype = ch4Op
      chip.channel[chnum + 3].chtype = ch4Op2
    else:
      chip.channel[chnum].chtype = ch2Op
      chip.channel[chnum + 3].chtype = ch2Op

proc opl3ClipSample(sample: Bit32s): Bit16s =
  var
    sampleIntern: Bit16s
  if sample > 32767:
    sampleIntern = 32767
  elif sample < -32768:
    sampleIntern = -32768
  else:
    return Bit16s(sample)

  return sampleIntern

proc opl3GenerateRhythm1(chip: Opl3Chip) =
  let
    channel6: ptr Opl3Channel = addr(chip.channel[6])
    channel7: ptr Opl3Channel = addr(chip.channel[7])
    channel8: ptr Opl3Channel = addr(chip.channel[8])

  var
    phase14: Bit16u
    phase17: Bit16u
    phase: Bit16u
    phasebit: Bit16u

  opl3SlotGenerate(channel6.slots[0][])
  phase14 = Bit16u((channel7.slots[0].pgPhase shr 9) and 0x3ff)
  phase17 = Bit16u((channel8.slots[1].pgPhase shr 9) and 0x3ff)
  phase = 0x00
  # hh tc phase bit
  phasebit = if ((phase14 and 0x08) or (((phase14 shr 5) xor phase14) and
      0x04) or (((phase17 shr 2) xor phase17) and 0x08)) != 0: 0x01 else: 0x00
  # hh
  phase = (phasebit shl 9) or uint16(0x34 shl ((uint32(phasebit) xor (
      chip.noise and 0x01)) shl 1))
  opl3SlotGeneratePhase(channel7.slots[0][], phase)
  # tt
  opl3SlotGenerateZM(channel8.slots[0][])

proc opl3GenerateRhythm2(chip: Opl3Chip) =
  var
    channel6: ptr Opl3Channel = addr(chip.channel[6])
    channel7: ptr Opl3Channel = addr(chip.channel[7])
    channel8: ptr Opl3Channel = addr(chip.channel[8])

  var
    phase14: Bit16u
    phase17: Bit16u
    phase: Bit16u
    phasebit: Bit16u

  opl3SlotGenerate(channel6.slots[1][])
  phase14 = Bit16u((channel7.slots[0].pgPhase shr 9) and 0x3ff)
  phase17 = Bit16u((channel8.slots[1].pgPhase shr 9) and 0x3ff)
  phase = 0x00
  # hh tc phase bit
  phasebit = if ((phase14 and 0x08) or (((phase14 shr 5) xor phase14) and
      0x04) or (((phase17 shr 2) xor phase17) and 0x08)) != 0: 0x01 else: 0x00
  # sd
  phase = Bit16u(Bit32u(0x100 shl ((phase14 shr 8) and 0x01)) xor ((
      chip.noise and 0x01) shl 8))
  opl3SlotGeneratePhase(channel7.slots[1][], phase)
  # tc
  phase = 0x100'u16 or (phasebit shl 9)
  opl3SlotGenerateZM(channel8.slots[1][])

proc opl3Generate(chip: Opl3Chip, buf: ptr Bit16s) =
  var
    accm: Bit16s

  cast[ptr UncheckedArray[Bit16s]](buf)[1] = opl3ClipSample(chip.mixbuff[1])

  for ii in 0..<12:
    opl3SlotCalcFB(chip.slot[ii])
    opl3PhaseGenerate(chip.slot[ii])
    opl3EnvelopeCalc(chip.slot[ii])
    opl3SlotGenerate(chip.slot[ii])

  for ii in 12..<15:
    opl3SlotCalcFB(chip.slot[ii])
    opl3PhaseGenerate(chip.slot[ii])
    opl3EnvelopeCalc(chip.slot[ii])

  if (chip.rhy and 0x20) != 0:
    opl3GenerateRhythm1(chip)
  else:
    opl3SlotGenerate(chip.slot[12])
    opl3SlotGenerate(chip.slot[13])
    opl3SlotGenerate(chip.slot[14])

  chip.mixbuff[0] = 0
  for ii in 0..<18:
    opl3SlotCalcFB(chip.slot[ii])
    opl3PhaseGenerate(chip.slot[ii])
    opl3EnvelopeCalc(chip.slot[ii])

  if (chip.rhy and 0x20) != 0:
    opl3GenerateRhythm2(chip)
  else:
    opl3SlotGenerate(chip.slot[15])
    opl3SlotGenerate(chip.slot[16])
    opl3SlotGenerate(chip.slot[17])

  cast[ptr UncheckedArray[Bit16s]](buf)[1] = opl3ClipSample(chip.mixbuff[0])

  for ii in 18'u8..<33'u8:
    opl3SlotCalcFB(chip.slot[ii])
    opl3PhaseGenerate(chip.slot[ii])
    opl3EnvelopeCalc(chip.slot[ii])
    opl3SlotGenerate(chip.slot[ii])

  chip.mixbuff[1] = 0
  for ii in 0..<18:
    accm = 0
    for jj in 0'u8..<4'u8:
      accm += chip.channel[ii].`out`[jj][]
    chip.mixbuff[1] += Bit32s(accm) and Bit32s(chip.channel[ii].chb)

  for ii in 33..<36:
    opl3SlotCalcFB(chip.slot[ii])
    opl3PhaseGenerate(chip.slot[ii])
    opl3EnvelopeCalc(chip.slot[ii])
    opl3SlotGenerate(chip.slot[ii])

  opl3NoiseGenerate(chip)

  if (chip.timer and 0x3f) == 0x3f:
    chip.tremolopos = (chip.tremolopos + 1'u8) mod 210'u8
  if (chip.tremolopos < 105):
    chip.tremolo = chip.tremolopos shr chip.tremoloshift
  else:
    chip.tremolo = (210'u8 - chip.tremolopos) shr chip.tremoloshift

  if (chip.timer and 0x3ff) == 0x3ff:
    chip.vibpos = uint8((chip.vibpos + 1'u8) and 7)

  inc(chip.timer)

  while chip.writebuf[chip.writebufCur].time <= chip.writebufSamplecnt:
    if (chip.writebuf[chip.writebufCur].reg and 0x200) == 0:
      break
    chip.writebuf[chip.writebufCur].time = chip.writebuf[
        chip.writebufCur].reg and 0x1ff
    opl3WriteReg(chip, chip.writebuf[chip.writebufCur].reg, chip.writebuf[
        chip.writebufCur].data)
    chip.writebufCur = (chip.writebufCur + 1) mod OPL_WRITEBUF_SIZE

  inc(chip.writebufSamplecnt)

proc opl3GenerateResampled(chip: Opl3Chip, buf: ptr Bit16s) =
  while chip.samplecnt >= chip.rateratio:
    chip.oldsamples[0] = chip.samples[0]
    chip.oldsamples[1] = chip.samples[1]
    opl3Generate(chip, addr(chip.samples[0]))
    chip.samplecnt -= chip.rateratio

  buf[] = Bit16s(Bit32s(chip.oldsamples[0] * (chip.rateratio -
      chip.samplecnt) + chip.samples[0] * chip.samplecnt) div chip.rateratio)
  cast[ptr UncheckedArray[Bit16s]](buf)[1] = Bit16s((chip.oldsamples[1] * (
      chip.rateratio - chip.samplecnt) + chip.samples[1] *
      chip.samplecnt) div chip.rateratio)
  chip.samplecnt += Bit32s(1 shl RSM_FRAC)

proc opl3Reset*(chip: Opl3Chip, samplerate: Bit32u) =
  memset(addr(chip[]), 0, csize_t(sizeof(Opl3ChipObject)))
  for slotnum in 0..<36:
    chip.slot[slotnum].chip = chip
    chip.slot[slotnum].`mod` = addr(chip.zeromod)
    chip.slot[slotnum].egRout = 0x1ff
    chip.slot[slotnum].egOut = 0x1ff
    chip.slot[slotnum].egGen = ord(envelopeGenNumOff)
    chip.slot[slotnum].trem = cast[ptr Bit8u](addr(chip.zeromod))

  for channum in 0..<18:
    chip.channel[channum].slots[0] = addr(chip.slot[chSlot[channum]])
    chip.channel[channum].slots[1] = addr(chip.slot[chSlot[channum] + 3])
    chip.slot[chSlot[channum]].channel = addr(chip.channel[channum])
    chip.slot[chSlot[channum] + 3].channel = addr(chip.channel[channum])
    if (channum mod 9) < 3:
      chip.channel[channum].pair = addr(chip.channel[channum + 3])
    elif (channum mod 9) < 6:
      chip.channel[channum].pair = addr(chip.channel[channum - 3])
    chip.channel[channum].chip = chip
    chip.channel[channum].`out`[0] = addr(chip.zeromod)
    chip.channel[channum].`out`[1] = addr(chip.zeromod)
    chip.channel[channum].`out`[2] = addr(chip.zeromod)
    chip.channel[channum].`out`[3] = addr(chip.zeromod)
    chip.channel[channum].chtype = ch2Op
    chip.channel[channum].cha = not(0'u16)
    chip.channel[channum].chb = not(0'u16)
    opl3ChannelSetupAlg(chip.channel[channum])

  chip.noise = 0x306600
  # Carefull: might be wrong switching from / to div
  chip.rateratio = Bit32s( (samplerate shl RSM_FRAC) div 49716)
  chip.tremoloshift = 4
  chip.vibshift = 1

proc opl3WriteReg*(chip: Opl3Chip, reg: Bit16u, v: Bit8u) =
  var
    highB: Bit8u = Bit8u(reg shr 8) and 0x01'u8
    regm: Bit8u = Bit8u(reg and 0xff)
  case regm and 0xf0
  of 0x00:
    if (highB) != 0:
      case regm and 0x0f
      of 0x04:
        opl3ChannelSet4Op(chip, v)
      of 0x05:
        chip.newm = Bit8u(v and 0x01)
      else:
        discard
    else:
      case regm and 0x0f
      of 0x08:
        chip.nts = Bit8u((v shr 6) and 0x01)
      else:
        discard
  of 0x20, 0x30:
    if adSlot[regm and 0x1f] >= 0:
      opl3SlotWrite20(chip.slot[Bit16u(18 * highB) + Bit16u(adSlot[regm and
          0x1f])], v)
  of 0x40, 0x50:
    if adSlot[regm and 0x1f] >= 0:
      opl3SlotWrite40(chip.slot[Bit16u(18 * highB) + Bit16u(adSlot[regm and
          0x1f])], v)
  of 0x60, 0x70:
    if adSlot[regm and 0x1f] >= 0:
      opl3SlotWrite60(chip.slot[Bit16u(18 * highB) + Bit16u(adSlot[regm and
          0x1f])], v)
  of 0x80, 0x90:
    if adSlot[regm and 0x1f] >= 0:
      opl3SlotWrite80(chip.slot[Bit16u(18 * highB) + Bit16u(adSlot[regm and
          0x1f])], v)
  of 0xe0, 0xf0:
    if adSlot[regm and 0x1f] >= 0:
      opl3SlotWriteE0(chip.slot[Bit16u(18 * highB) + Bit16u(adSlot[regm and
          0x1f])], v)
  of 0xa0:
    if adSlot[regm and 0x0f] < 9:
      opl3ChannelWriteA0(chip.channel[Bit16u(9 * highB) + Bit16u(regm and
          0x0f)], v)
  of 0xb0:
    if (regm == 0xbd) and not((highB)) != 0:
      chip.tremoloshift = Bit8u( ((v shr 7) xor 1) shl 1) + 2'u8
      chip.vibshift = Bit8u((v shr 6) and 0x01 xor 1)
      opl3ChannelUpdateRhythm(chip, v)
    elif (regm and 0x0f) < 9:
      opl3ChannelWriteB0(chip.channel[Bit16u(9 * highB) + Bit16u(regm and
          0x0f)], v)
      if (v and 0x20) != 0:
        opl3ChannelKeyOn(chip.channel[Bit16u(9 * highB) + Bit16u(regm and 0x0f)])
      else:
        opl3ChannelKeyOff(chip.channel[Bit16u(9 * highB) + Bit16u(regm and 0x0f)])
  of 0xc0:
    if (regm and 0x0f) < 9:
      opl3ChannelWriteC0(chip.channel[Bit16u(9 * highB) + Bit16u(regm and
          0x0f)], v)
  else:
    discard

proc opl3WriteRegBuffered(chip: Opl3Chip, reg: Bit16u, v: Bit8u) =
  var
    time1, time2: Bit64u

  if (chip.writebuf[chip.writebufLast].reg and 0x200) != 0:
    opl3WriteReg(chip, chip.writebuf[chip.writebufLast].reg and 0x1ff,
        chip.writebuf[chip.writebufLast].data)

    chip.writebufCur = (chip.writebufLast + 1) mod OPL_WRITEBUF_SIZE
    chip.writebufSamplecnt = chip.writebuf[chip.writebufLast].time

  chip.writebuf[chip.writebufLast].reg = reg or 0x200
  chip.writebuf[chip.writebufLast].data = v
  time1 = chip.writebufLasttime + OPL_WRITEBUF_DELAY
  time2 = chip.writebufSamplecnt

  if time1 < time2:
    time1 = time2

  chip.writebuf[chip.writebufLast].time = time1
  chip.writebufLasttime = time1
  chip.writebufLast = (chip.writebufLast + 1) mod OPL_WRITEBUF_SIZE

proc opl3GenerateStream*(chip: Opl3Chip, sndptr: var seq[Bit16s],
    numsamples: Bit32u) =
  # TODO: sndptr type changing
  for i in 0'u32..<numsamples:
    opl3GenerateResampled(chip, addr(sndptr[2'u32*i]))
    # sndptr[] += 2
    # sndptr = sndptr[2..high(sndptr)]
