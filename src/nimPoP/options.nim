proc turnFixesAndEnhancementsOnOff(newState: byte) =
  useFixesandEnhancements = newState
  fixes = if (newState != 0): addr(fixesSaved) else: addr(fixesDisabledState)

proc turnCustomOptionsOnOff(newState: byte) =
  useCustomOptions = newState
  custom = if (newState != 0): addr(customSaved) else: addr(customDefaults)

const
  # this is needed because use_hardware_acceleration is not a bool!
  useHardwareAccelerationNames* = ["false", "true", "defult"]
  levelTypeNames* = ["dungeon", "palace"]
  guardTypeNames* = ["guard", "fat", "skel", "vizier", "shadow"]
  tileTypeNames* = ["empty", "floor", "spike", "pillar", "gate",      # 0..4
    "stuck", "closer", "doortopWithFloor", "bigpillarBottom", "bigpillarTop", # 5..9
    "potion", "loose", "doortop", "mirror", "debris",                 # 10..14
    "opener", "levelDoorLeft", "levelDoorRight", "chomper", "torch",  # 15..19
    "wall", "skeleton", "sword", "balconyLeft", "balconyRight",       # 20..24
    "latticePillar", "latticeDown", "latticeSmall", "latticeLeft",
    "latticeRight",                                                   # 25..29
    "torchWithDebris"]
  scalingTypeNames* = ["sharp", "fuzzy", "blurry"]
  rowNames* = ["top", "middle", "bottom"]
  entryPoseNames* = ["turning", "falling", "running"]
  INI_NO_VALID_NAME* = -9999

const
  directionNames: Table[string, Directions] = {"left": dirFFLeft,
      "right": dir0Right}.toTable
  # 16 is higher than any level, so some options can be disabled by setting it
  # to this value
  # neverIs16* = {"Never": 16}.toTable

proc iniLoad*(filename: string) =
  var
    name, value, section: string
    f = newFileStream(filename, fmRead)
  if f != nil:
    var p: CfgParser

    open(p, f, filename)
    while true:
      var e = next(p)
      case e.kind
      of cfgEof: break
      of cfgSectionStart:
        section = e.section
      of cfgKeyValuePair:
        name = e.key
        value = e.value
        case section
        of "General":
          case name
          of "enable_copyprot":
            enableCopyprot = byte(parseBool(value))
          of "enable_music":
            enableMusic = byte(parseBool(value))
          of "enable_fade":
            enableFade = byte(parseBool(value))
          of "enableFlash":
            enableFlash = byte(parseBool(value))
          of "enable_text":
            enableText = byte(parseBool(value))
          of "enable_info_screen":
            enableInfoScreen = byte(parseBool(value))
          of "start_fullscreen":
            startFullscreen = byte(parseBool(value))
          of "pop_window_width":
            if value != "" and value != "default":
              popWindowWidth = word(parseInt(value))
          of "pop_window_height":
            if value != "" and value != "default":
              popWindowHeight = word(parseInt(value))
          of "use_hardware_acceleration":
            if value != "" and value != "default":
              useHardwareAcceleration = byte(parseBool(value))
          of "use_correct_aspect_ratio":
            useCorrectAspectRatio = byte(parseBool(value))
          of "use_integer_scaling":
            useIntegerScaling = byte(parseBool(value))
          of "scaling_type":
            if value in scalingTypeNames:
              scalingType = byte(find(scalingTypeNames, value))
          of "enableControllerRumble":
            enableControllerRumble = byte(parseBool(value))
          of "joystick_only_horizontal":
            joystickOnlyHorizontal = byte(parseBool(value))
          of "joystick_threshold":
            joystickThreshold = int32(parseInt(value))
          of "levelset":
            if value == "" or value == "original" or value == "default":
              useCustomLevelset = 0
            else:
              useCustomLevelset = 1
              levelsetName = value
          of "always_use_original_music":
            alwaysUseOriginalMusic = byte(parseBool(value))
          of "always_use_original_graphics":
            alwaysUseOriginalGraphics = byte(parseBool(value))
          of "gamecontrollerdb_file":
            if value != "":
              gamecontrollerdbFile = value
          else:
            when(USE_MENU):
              if name == "enable_pause_menu":
                enablePauseMenu = byte(parseBool(value))
              elif name == "mods_folder":
                if value.len != 0 and value != "default":
                  modsFolder = value
            else:
              discard
        of "AdditionalFeatures":
          when(USE_REPLAY and USE_LIGHTING):
            case name
            of "enable_quicksave":
              enableQuicksave = byte(parseBool(value))
            of "enable_quicksave_penalty":
              enableQuicksavePenalty = byte(parseBool(value))
            of "enable_replay":
              enableReplay = byte(parseBool(value))
            of "replays_folder":
              if value != "" and value != "default":
                replaysFolder = value
            of "enable_lighting":
              enableLighting = byte(parseBool(value))
            else:
              discard
          when(USE_REPLAY):
            case name
            of "enable_quicksave":
              enableQuicksave = byte(parseBool(value))
            of "enable_quicksave_penalty":
              enableQuicksavePenalty = byte(parseBool(value))
            of "enable_replay":
              enableReplay = byte(parseBool(value))
            of "replays_folder":
              if value != "" and value != "default":
                replaysFolder = value
            else:
              discard
          elif(USE_LIGHTING):
            case name
            of "enable_quicksave":
              enableQuicksave = byte(parseBool(value))
            of "enable_quicksave_penalty":
              enableQuicksavePenalty = byte(parseBool(value))
            of "enable_lighting":
              enableLighting = byte(parseBool(value))
            else:
              discard
        of "Enhancements":
          case name
          of "use_fixes_and_enhancements":
            case value
            of "true":
              useFixesAndEnhancements = 1
            of "false":
              useFixesAndEnhancements = 0
            of "prompt":
              useFixesAndEnhancements = 2
            else:
              discard
          of "enable_crouch_after_climbing":
            fixesSaved.enableCrouchAfterClimbing = byte(parseBool(value))
          of "enable_freeze_time_during_end_music":
            fixesSaved.enableFreezeTimeDuringEndMusic = byte(parseBool(value))
          of "enable_remember_guard_hp":
            fixesSaved.enableRememberGuardHp = byte(parseBool(value))
          of "fix_gate_sounds":
            fixesSaved.fixGateSounds = byte(parseBool(value))
          of "fix_two_coll_bug":
            fixesSaved.fixTwoCollBug = byte(parseBool(value))
          of "fix_infinite_down_bug":
            fixesSaved.fixInfiniteDownBug = byte(parseBool(value))
          of "fix_gate_drawing_bug":
            fixesSaved.fixGateDrawingBug = byte(parseBool(value))
          of "fix_bigpillar_climb":
            fixesSaved.fixBigpillarClimb = byte(parseBool(value))
          of "fix_jump_distance_at_edge":
            fixesSaved.fixJumpDistanceAtEdge = byte(parseBool(value))
          of "fix_edge_distance_check_when_climbing":
            fixesSaved.fixEdgeDistanceCheckWhenClimbing = byte(parseBool(value))
          of "fix_painless_fall_on_guard":
            fixesSaved.fixPainlessFallOnGuard = byte(parseBool(value))
          of "fix_wall_bump_triggers_tile_below":
            fixesSaved.fixWallBumpTriggersTileBelow = byte(parseBool(value))
          of "fix_stand_on_thin_air":
            fixesSaved.fixStandOnThinAir = byte(parseBool(value))
          of "fix_press_through_closed_gates":
            fixesSaved.fixPressThroughClosedGates = byte(parseBool(value))
          of "fix_grab_falling_speed":
            fixesSaved.fixGrabFallingSpeed = byte(parseBool(value))
          of "fix_skeleton_chomper_blood":
            fixesSaved.fixSkeletonChomperBlood = byte(parseBool(value))
          of "fix_move_after_drink":
            fixesSaved.fixMoveAfterDrink = byte(parseBool(value))
          of "fix_loose_left_of_potion":
            fixesSaved.fixLooseLeftOfPotion = byte(parseBool(value))
          of "fix_guard_following_through_closed_gates":
            fixesSaved.fixGuardFollowingThroughClosedGates = byte(parseBool(value))
          of "fix_safe_landing_on_spikes":
            fixesSaved.fixSafeLandingOnSpikes = byte(parseBool(value))
          of "fix_glide_through_wall":
            fixesSaved.fixGlideThroughWall = byte(parseBool(value))
          of "fix_drop_through_tapestry":
            fixesSaved.fixDropThroughTapestry = byte(parseBool(value))
          of "fix_land_against_gate_or_tapestry":
            fixesSaved.fixLandAgainstGateOrTapestry = byte(parseBool(value))
          of "fix_unintended_sword_strike":
            fixesSaved.fixUnintendedSwordStrike = byte(parseBool(value))
          of "fix_retreat_without_leaving_room":
            fixesSaved.fixRetreatWithoutLeavingRoom = byte(parseBool(value))
          of "fix_running_jump_through_tapestry":
            fixesSaved.fixRunningJumpThroughTapestry = byte(parseBool(value))
          of "fix_push_guard_into_wall":
            fixesSaved.fixPushGuardIntoWall = byte(parseBool(value))
          of "fix_jump_through_wall_above_gate":
            fixesSaved.fixJumpThroughWallAboveGate = byte(parseBool(value))
          of "fix_chompers_not_starting":
            fixesSaved.fixChompersNotStarting = byte(parseBool(value))
          of "fix_feather_interrupted_by_leveldoor":
            fixesSaved.fixFeatherInterruptedByLeveldoor = byte(parseBool(value))
          of "fix_offscreen_guards_disappearing":
            fixesSaved.fixOffscreenGuardsDisappearing = byte(parseBool(value))
          of "fix_move_after_sheathe":
            fixesSaved.fixMoveAfterSheathe = byte(parseBool(value))
          of "fix_hidden_floors_during_flashing":
            fixesSaved.fixHiddenFloorsDuringFlashing = byte(parseBool(value))
          of "fix_hang_on_teleport":
            fixesSaved.fixHangOnTeleport = byte(parseBool(value))
          of "fix_exit_door":
            fixesSaved.fixExitDoor = byte(parseBool(value))
          of "fix_quicksave_during_feather":
            fixesSaved.fixQuicksaveDuringFeather = byte(parseBool(value))
          of "fix_caped_prince_sliding_through_gate":
            fixesSaved.fixCapedPrinceSlidingThroughGate = byte(parseBool(value))
          of "fix_doortop_disabling_guard":
            fixesSaved.fixDoortopDisablingGuard = byte(parseBool(value))
          of "fix_jumping_over_guard":
            fixesSaved.fixJumpingOverGuard = byte(parseBool(value))
          of "fix_drop_2_rooms_climbing_loose_tile":
            fixesSaved.fixDrop2RoomsClimbingLooseTile = byte(parseBool(value))
          of "fix_falling_through_floor_during_sword_strike":
            fixesSaved.fixFallingThroughFloorDuringSwordStrike = byte(parseBool(value))
          of "enable_jump_grab":
            fixesSaved.enableJumpGrab = byte(parseBool(value))
          of "fix_register_quick_input":
            fixesSaved.fixRegisterQuickInput = byte(parsebool(value))
          of "fix_turn_running_near_wall":
            fixesSaved.fixTurnRunningNearWall = byte(parsebool(value))
          else:
            discard
        of "CustomGameplay":
          case name
          of "use_custom_options":
            useCustomOptions = byte(parseBool(value))
          of "start_minutes_left":
            customSaved.startTicksLeft = word(parseInt(value))
          of "start_hitp":
            customSaved.startHitp = word(parseInt(value))
          of "max_hitp_allowed":
            customSaved.maxHitpAllowed = word(parseInt(value))
          of "saving_allowed_first_level":
            if value == "never" or value == "16":
              customSaved.savingAllowedFirstLevel = byte(16)
          of "saving_allowed_last_level":
            if value == "never" or value == "16":
              customSaved.savingAllowedLastLevel = byte(16)
          of "start_upside_down":
            customSaved.startUpsideDown = byte(parseBool(value))
          of "start_in_blind_mode":
            customSaved.startInBlindMode = byte(parseBool(value))
          of "copyprot_level":
            if value == "never" or value == "16":
              customSaved.copyprotLevel = byte(16)
          of "drawn_tile_top_level_edge":
            if value in tileTypeNames:
              customSaved.drawnTileTopLevelEdge = Tiles(find(tileTypeNames, value))
          of "drawn_tile_left_level_edge":
            if value in tileTypeNames:
              customSaved.drawnTileLeftLevelEdge = Tiles(find(tileTypeNames, value))
          of "level_edge_hit_tile":
            if value in tileTypeNames:
              customSaved.levelEdgeHitTile = Tiles(find(tileTypeNames, value))
          of "allow_triggering_any_tile":
            customSaved.allowTriggeringAnyTile = byte(parseBool(value))
          of "enable_wda_in_palace":
            customSaved.enableWdaInPalace = byte(parseBool(value))

          of "first_level":
            customSaved.firstLevel = word(parseInt(value))
          of "skip_title":
            customSaved.skipTitle = byte(parseBool(value))
          of "shift_L_allowed_until_level":
            if value == "never" or value == "16":
              customSaved.shiftLAllowedUntilLevel = 16'u16
          of "shift_L_reduced_minutes":
            customSaved.shiftLReducedMinutes = word(parseInt(value))
          of "shift_L_reduced_ticks":
            customSaved.shiftLReducedTicks = word(parseInt(value))
          of "demo_hitp":
            customSaved.demoHitp = word(parseInt(value))
          of "demo_end_room":
            customSaved.demoEndRoom = word(parseInt(value))
          of "intro_music_level":
            if value == "never" or value == "16":
              customSaved.introMusicLevel = 16'u16
          of "have_sword_from_level":
            if value == "never" or value == "16":
              customSaved.haveSwordFromLevel = 16'u16
          of "checkpoint_level":
            if value == "never" or value == "16":
              customSaved.checkpointLevel = 16'u16
          of "checkpoint_respawn_dir":
            if directionNames.hasKey(value):
              customSaved.checkpointRespawnDir = sbyte(directionNames[value])
          of "checkpoint_respawn_room":
            customSaved.checkpointRespawnRoom = byte(parseInt(value))
          of "checkpoint_respawn_tilepos":
            customSaved.checkpointRespawnTilepos = byte(parseInt(value))
          of "checkpoint_clear_tile_room":
            customSaved.checkpointClearTileRoom = byte(parseInt(value))
          of "checkpoint_clear_tile_col":
            customSaved.checkpointClearTileCol = byte(parseInt(value))
          of "checkpoint_clear_tile_row":
            if value in rowNames:
              customSaved.checkpointClearTileRow = byte(find(rowNames, value))
          of "skeleton_level":
            if value == "never" or value == "16":
              customSaved.skeletonLevel = 16'u16
          of "skeleton_room":
            customSaved.skeletonRoom = byte(parseInt(value))
          of "skeleton_trigger_column_1":
            customSaved.skeletonTriggerColumn1 = byte(parseInt(value))
          of "skeleton_trigger_column_2":
            customSaved.skeletonTriggerColumn2 = byte(parseInt(value))
          of "skeleton_column":
            customSaved.skeletonColumn = byte(parseInt(value))
          of "skeleton_row":
            if value in rowNames:
              customSaved.skeletonRow = byte(find(rowNames, value))
          of "skeleton_require_open_level_door":
            customSaved.skeletonRequireOpenLevelDoor = byte(parseBool(value))
          of "skeleton_skill":
            customSaved.skeletonSkill = byte(parseInt(value))
          of "skeleton_reappear_room":
            customSaved.skeletonReappearRoom = byte(parseInt(value))
          of "skeleton_reappear_x":
            customSaved.skeletonReappearX = byte(parseInt(value))
          of "skeleton_reappear_row":
            if value in rowNames:
              customSaved.skeletonReappearRow = byte(find(rowNames, value))
          of "skeleton_reappear_dir":
            if directionNames.hasKey(value):
              customSaved.skeletonReappearDir = byte(directionNames[value])
          of "mirror_level":
            if value == "never" or value == "16":
              customSaved.mirrorLevel = 16'u16
          of "mirror_room":
            customSaved.mirrorRoom = byte(parseInt(value))
          of "mirror_column":
            customSaved.mirrorColumn = byte(parseInt(value))
          of "mirror_row":
            if value in rowNames:
              customSaved.mirrorRow = byte(find(rowNames, value))
          of "mirror_tile":
            if value in tileTypeNames:
              customSaved.mirrorTile = Tiles(find(tileTypeNames, value))
          of "show_mirror_image":
            customSaved.showMirrorImage = byte(parseBool(value))
          of "shadow_steal_level":
            customSaved.shadowStealLevel = byte(parseInt(value))
          of "shadow_steal_room":
            customSaved.shadowStealRoom = byte(parseInt(value))
          of "shadow_step_level":
            customSaved.shadowStepLevel = byte(parseInt(value))
          of "shadow_step_room":
            customSaved.shadowStepRoom = byte(parseInt(value))
          of "falling_exit_level":
            if value == "never" or value == "16":
              customSaved.fallingExitLevel = 16'u16
          of "falling_exit_room":
            customSaved.fallingExitRoom = byte(parseInt(value))
          of "falling_entry_level":
            if value == "never" or value == "16":
              customSaved.fallingEntryLevel = 16'u16
          of "falling_entry_room":
            customSaved.fallingEntryRoom = byte(parseInt(value))
          of "mouse_level":
            if value == "never" or value == "16":
              customSaved.mouseLevel = 16'u16
          of "mouse_room":
            customSaved.mouseRoom = byte(parseInt(value))
          of "mouse_delay":
            customSaved.mouseDelay = word(parseInt(value))
          of "mouse_object":
            customSaved.mouseObject = byte(parseInt(value))
          of "mouse_start_x":
            customSaved.mouseStartX = byte(parseInt(value))
          of "loose_tiles_level":
            if value == "never" or value == "16":
              customSaved.looseTilesLevel = 16'u16
          of "loose_tiles_room_1":
            customSaved.looseTilesRoom_1 = byte(parseInt(value))
          of "loose_tiles_room_2":
            customSaved.looseTilesRoom_2 = byte(parseInt(value))
          of "loose_tiles_first_tile":
            customSaved.looseTilesFirstTile = byte(parseInt(value))
          of "loose_tiles_last_tile":
            customSaved.looseTilesLastTile = byte(parseInt(value))
          of "jaffar_victory_level":
            if value == "never" or value == "16":
              customSaved.jaffarVictoryLevel = 16'u16
          of "jaffar_victory_flash_time":
            customSaved.jaffarVictoryFlashTime = byte(parseInt(value))
          of "hide_level_number_from_level":
            if value == "never" or value == "16":
              customSaved.hideLevelNumberFromLevel = 16'u16
          of "level_13_level_number":
            customSaved.level_13LevelNumber = byte(parseInt(value))
          of "victory_stops_time_level":
            if value == "never" or value == "16":
              customSaved.victoryStopsTimeLevel = 16'u16
          of "win_level":
            if value == "never" or value == "16":
              customSaved.winLevel = 16'u16
          of "win_room":
            customSaved.winRoom = byte(parseInt(value))
          of "loose_floor_delay":
            customSaved.looseFloorDelay = byte(parseInt(value))
          of "base_speed":
            customSaved.baseSpeed = byte(parseInt(value))
          of "fight_speed":
            customSaved.fightSpeed = byte(parseInt(value))
          of "chomper_speed":
            customSaved.chomperSpeed = byte(parseInt(value))
          of "no_mouse_in_ending":
            customSaved.noMouseInEnding = byte(parseInt(value))
          else:
            if "vga_color_" in name:
              let
                iniPaletteColor = parseInt(name[10..high(name)])
                colors = split(value, ',')
              if colors.len == 3:
                customSaved.vgaPalette[iniPaletteColor].r = byte(parseInt(
                    colors[0]) div 4)
                customSaved.vgaPalette[iniPaletteColor].g = byte(parseInt(
                    colors[1]) div 4)
                customSaved.vgaPalette[iniPaletteColor].b = byte(parseInt(
                    colors[2]) div 4)
        else:
          if "Level" in section:
            let
              iniLevel = parseInt(split(section, ' ')[1])
            case name
            of "level_type":
              if value in levelTypeNames:
                customSaved.tblLevelType[iniLevel] = byte(find(levelTypeNames, value))
            of "level_color":
              customSaved.tblLevelColor[iniLevel] = word(parseInt(value))
            of "guard_type":
              if value in guardTypeNames:
                customSaved.tblGuardType[iniLevel] = int16(find(guardTypeNames, value))
            of "guard_hp":
              customSaved.tblGuardHp[iniLevel] = byte(parseInt(value))
            of "entry_pose":
              if value in entryPoseNames:
                customSaved.tblEntryPose[iniLevel] = byte(find(entryPoseNames, value))
            of "seamless_exit":
              customSaved.tblSeamlessExit[iniLevel] = cast[sbyte](parseInt(value))
            of "cutscene":
              if value != "default":
                customSaved.tblCutscenesByIndex[iniLevel] = byte(parseInt(value))
            else:
              echo "Warning: INvalid section [Level ", iniLevel, "] in the INI!"
          elif "Skill" in section:
            let
              iniSkill = parseInt(split(section, ' ')[1])
            if iniSkill >= 0 and iniSkill < NUM_GUARD_SKILLS:
              case name:
              of "strikeprob":
                customSaved.strikeprob[iniSkill] = byte(parseInt(value))
              of "restrikeprob":
                customSaved.restrikeprob[iniSkill] = byte(parseInt(value))
              of "blockprob":
                customSaved.blockprob[iniSkill] = byte(parseInt(value))
              of "impblockprob":
                customSaved.impblockprob[iniSkill] = byte(parseInt(value))
              of "advprob":
                customSaved.advprob[iniSkill] = byte(parseInt(value))
              of "refractimer":
                customSaved.refractimer[iniSkill] = byte(parseInt(value))
              of "extrastrength":
                customSaved.extrastrength[iniSkill] = byte(parseInt(value))
              else:
                echo "Warning: Invalid section [Skill ", iniSkill, "] in the INI!"
            else:
              echo "Warning: Invalid section ", section, " in the INI!"
      of cfgOption:
        name = e.key
        value = e.value
        # discard report(section, name, value)
      of cfgError:
        echo "Got an error parsing ini file: " & filename
        echo "Error message: " & e.msg
        close(p)
        quitPoP(1)
    close(p)
  else:
    echo "cannot open ini file: " & filename
    quitPoP(1)

proc setOptionsToDefault*() =
  when(USE_MENU):
    enablePauseMenu = 1
  enableCopyprot = 0
  enableMusic = 1
  enableFade = 1
  enableFlash = 1
  enableText = 1
  enableInfoScreen = 1
  startFullscreen = 0
  useHardwareAcceleration = 2
  useCorrectAspectRatio = 0
  useIntegerScaling = 0
  scalingType = 0
  enableControllerRumble = 1
  joystickOnlyHorizontal = 1
  joystickThreshold = 8000
  enableQuicksave = 1
  enableQuicksavePenalty = 1
  enableReplay = 1
  when(USE_LIGHTING):
    enableLighting = 0
  memset(addr(fixesSaved), 1, csize_t(sizeof(fixesSaved)))
  customSaved = customDefaults
  turnFixesAndEnhancementsOnOff(0)
  turnCustomOptionsOnOff(0)

proc loadGlobalOptions() =
  setOptionsToDefault()
  iniLoad("SDLPoP.ini")
  loadDosExeModifications(".")

proc checkModParam() =
  let
    params = commandLineParams()
    pos = find(params, "mod")
  if pos != -1 and pos < high(params):
    useCustomLevelset = byte(true)
    levelsetName = params[pos]

type
  dosVersion = enum
    dos10Packed = 0,
    dos10Unpacked,
    dos13Packed,
    dos13Unpacked,
    dos14Packed,
    dos14Unpacked

proc readExeBytes(dest: pointer, nbytes: uint, exeMemory: ptr byte, exeOffset,
    exeSize: int32): bool =
  if exeOffset < 0:
    return false
  if exeOffset < exeSize:
    # memcpy(dest, exe_memory + exe_offset, nbytes);
    discard
  return true

proc identifyDosExeVersion(filesize: int64): int64 =
  var
    dosVersion: int64 = -1
  case filesize
  of 123335:
    dosVersion = ord(dos10Packed)
  of 125115:
    dosVersion = ord(dos13Packed)
  of 110855:
    dosVersion = ord(dos14Packed)
  of 129504:
    dosVersion = ord(dos10Unpacked)
  of 129472:
    dosVersion = ord(dos13Unpacked)
  of 115008:
    dosVersion = ord(dos14Unpacked)
  else:
    discard

  return dosVersion

proc process[T](exeMemory: FileStream, x: var T, offsets: array[
    0..5, int], version: int64, exeSize: int32): bool =
  if offsets[version] < exeSize:
    setPosition(exeMemory, offsets[version])
    read(exeMemory, x)
    return true
  else:
    return false

proc loadDosExeModifications(folderName: string) =
  var
    filename: string = folderName & "/PRINCE.EXE"
    fp: File
    dosVersion: int64 = -1

  if fileExists(filename):
    discard open(fp, filename, fmRead)
    if fp != nil and getFileSize(fp) > 0:
      dosVersion = identifyDosExeVersion(getFileSize(fp))
    else:
      # PRINCE.EXE not found, try to search for other .EXE files in the same folder.
      for kind, path in walkDir(foldername):
        if kind == pcFile or kind == pcLinkToFile:
          if path[^4..^1].toLowerAscii() == ".exe":
            close(fp)
            fp = open(path, fmRead)
            if getFileSize(fp) > 0:
              dosVersion = identifyDosExeVersion(getFileSize(fp))
              if dosVersion >= 0:
                break
  else:
    # PRINCE.EXE not found, try to search for other .EXE files in the same folder.
    for kind, path in walkDir(foldername):
      if kind == pcFile or kind == pcLinkToFile:
        if path[^4..^1].toLowerAscii() == ".exe":
          close(fp)
          fp = open(path, fmRead)
          if getFileSize(fp) > 0:
            dosVersion = identifyDosExeVersion(getFileSize(fp))
            if dosVersion >= 0:
              break

  defer: close(fp)

  if dosVersion >= 0:
    let
      fSize: int32 = int32(getFileSize(fp))
    var
      exeMemory: FileStream
      readOk: bool
      tempBytes: array[64, byte]
      tempWord: word
    turnCustomOptionsOnOff(1)
    # var
    #   exeMemory: ptr byte =
    exeMemory = newFileStream(fp)

    discard process(exeMemory, customSaved.startMinutesLeft, [0x04a23, 0x060d3,
        0x04ea3, 0x055e3, 0x0495f, 0x05a8f], dosversion, fSize)
    discard process(exeMemory, customSaved.startTicksLeft, [0x04a29,
        0x060d9, 0x04ea9, 0x055e9, 0x04965, 0x05a95], dosVersion, fSize)
    discard process(exeMemory, customSaved.startHitp, [0x04a2f, 0x060df,
        0x04eaf, 0x055ef, 0x0496b, 0x05a9b], dosVersion, fSize)
    discard process(exeMemory, customSaved.firstLevel, [0x00707, 0x01db7,
        0x007db, 0x00f1b, 0x0079f, 0x018cf], dosVersion, fSize)
    discard process(exeMemory, customSaved.maxHitpAllowed, [0x013f1,
        0x02aa1, 0x015ac, 0x01cec, 0x014a3, 0x025d3], dosVersion, fSize)
    readOk = process(exeMemory, customSaved.savingAllowedFirstLevel, [
        0x007c8, 0x01e78, 0x008b4, 0x00ff4, 0x00878, 0x019a8], dosVersion, fSize)
    if readOk:
      customSaved.savingAllowedFirstLevel += 1
    readOk = process(exeMemory, customSaved.savingAllowedLastLevel, [
        0x007cf, 0x01e7f, 0x008bb, 0x00ffb, 0x0087f, 0x019af], dosVersion, fSize)
    if readOk:
      customSaved.savingAllowedLastLevel -= 1
    if dosVersion == ord(dos10Packed) or dosVersion == ord(dos10Unpacked):
      let
        comparison: array[22, byte] = [0xa3'u8, 0x92'u8, 0x4e'u8, 0xa3'u8,
            0x5c'u8, 0x40'u8, 0xa3'u8, 0x8e'u8, 0x4e'u8, 0xa2'u8, 0x2a'u8,
            0x3d'u8, 0xa2'u8, 0x29'u8, 0x3d'u8, 0xa3'u8, 0xee'u8, 0x42'u8,
            0xa2'u8, 0x2e'u8, 0x3d'u8, 0x98'u8]
      customSaved.startUpsideDown = byte(true)
      for index in 0..high(comparison):
        discard process(exeMemory, tempBytes[index], [0x04c9b + index, 0x0634b +
            index, -1, -1, -1, -1], dosVersion, fSize)
        if tempBytes[index] != comparison[index]:
          customSaved.startUpsideDown = byte(false)
          break
    discard process(exeMemory, customSaved.startInBlindMode, [0x04e46,
        0x064f6, 0x052ce, 0x05a0e, 0x04d8a, 0x05eba], dosVersion, fSize)
    discard process(exeMemory, customSaved.copyprotLevel, [0x1aaeb, 0x1c62e,
        0x1b89b, 0x1c49e, 0x17c3d, 0x18e18], dosVersion, fSize)
    discard process(exeMemory, customSaved.drawnTileTopLevelEdge, [
        0x0a1f0, 0x0b8a0, 0x0a69c, 0x0addc, 0x0a158, 0x0b288], dosVersion, fSize)
    discard process(exeMemory, customSaved.drawnTileLeftLevelEdge, [
        0x0a26b, 0x0b91b, -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.levelEdgeHitTile, [0x06f02,
        0x085b2, -1, -1, -1, -1], dosVersion, fSize)
    readOk = process(exeMemory, tempBytes[0], [0x9111, 0xA7C1, 0x95BE, 0x9CFE,
        0x907A, 0xA1AA], dosVersion, fSize) # allow triggering any tile
    readOk = readOk and process(exeMemory, tempBytes[1], [0x9111+1, 0xA7C1+1,
        0x95BE+1, 0x9CFE+1, 0x907A+1, 0xA1AA+1], dosVersion,
        fSize) # allow triggering any tile
    if readOk:
      customSaved.allowTriggeringAnyTile =
        byte((tempBytes[0] == 0x75 and tempBytes[1] == 0x13) or
        (tempBytes[0] == 0x90 and tempBytes[1] == 0x90)) # used in Micro Palace
    readOk = process(exeMemory, tempBytes[0], [0x0a7bb, 0x0be6b, 0x0ac67,
        0x0b3a7, 0x0a723, 0x0b853], dosVersion, fSize) # enable WDA in palace
    if readOk:
      customSaved.enableWdaInPalace = byte(tempBytes[0] != 116)
    discard process(exeMemory, customSaved.tblLevelType, [0x1acea, 0x1c842,
        0x1b9ae, 0x1c5c6, 0x17d4c, 0x18f3c], dosVersion, fSize)
    discard process(exeMemory, customSaved.tblGuardHp, [0x1b8a8, 0x1d46a,
        0x1c6c5, 0x1d35c, 0x18a97, 0x19d06], dosVersion, fSize)
    discard process(exeMemory, customSaved.tblGuardType, [-1, 0x1c964, -1,
        0x1c702, -1, 0x1905e], dosVersion, fSize)
    discard process(exeMemory, customSaved.vgaPalette, [0x1d141, 0x1f136,
        0x1df5e, 0x1f02a, 0x1a335, 0x1b9de], dosVersion, fSize)
    readOk = process(exeMemory, tempWord, [0x003e2, 0x01a92, 0x0046b, 0x00bab,
        0x00455, 0x01585], dosVersion, fSize) # titles skipping
    if readOk:
      customSaved.skipTitle = byte(tempWord != 63558)
    readOk = process(exeMemory, customSaved.shiftLAllowedUntilLevel, [0x0085c,
        0x01f0c, 0x00955, 0x01095, 0x00919, 0x01a49], dosVersion, fSize)
    if readOk:
      customSaved.shiftLAllowedUntilLevel += 1
    discard process(exeMemory, customSaved.shiftLReducedMinutes, [0x008ad,
        0x01f5d, 0x00991, 0x010d1, 0x00955, 0x01a85], dosVersion, fSize)
    discard process(exeMemory, customSaved.shiftLReducedTicks, [0x008b3,
        0x01f63, 0x00997, 0x010d7, 0x0095b, 0x01a8b], dosVersion, fSize)
    # TODO: cutscenes
    # TODO: color variations
    discard process(exeMemory, customSaved.demoHitp, [0x04c28, 0x062d8, 0x050b0,
        0x057f0, 0x04b6c, 0x05c9c], dosVersion, fSize)
    discard process(exeMemory, customSaved.demoEndRoom, [0x00b40, 0x021f0,
        0x00c25, 0x01365, 0x00be9, 0x01d19], dosVersion, fSize)
    discard process(exeMemory, customSaved.introMusicLevel, [0x04c37, 0x062e7,
        0x050bf, 0x057ff, 0x04b7b, 0x05cab], dosVersion, fSize)
    readOk = process(exeMemory, tempBytes[0], [0x04b29, 0x061d9, 0x04fa9,
        0x056e9, 0x04a65, 0x05b95], dosVersion, fSize) # where the kid will have the sword
    if readOk:
      customSaved.haveSwordFromLevel = if tempBytes[0] == 0xEB:
          16'u16
        else:
          2'u16
    discard process(exeMemory, customSaved.checkpointLevel, [0x04b9e, 0x0624e,
        0x05026, 0x05766, 0x04ae2, 0x05c12], dosVersion, fSize)
    discard process(exeMemory, customSaved.checkpointRespawnDir, [0x04bac,
        0x0625c, 0x05034, 0x05774, 0x04af0, 0x05c20], dosVersion, fSize)
    discard process(exeMemory, customSaved.checkpointRespawnRoom, [0x04bb1,
        0x06261, 0x05039, 0x05779, 0x04af5, 0x05c25], dosVersion, fSize)
    discard process(exeMemory, customSaved.checkpointRespawnTilepos, [0x04bb6,
        0x06266, 0x0503e, 0x0577e, 0x04afa, 0x05c2a], dosVersion, fSize)
    discard process(exeMemory, customSaved.checkpointClearTileRoom, [0x04bb8,
        0x06268, 0x05040, 0x05780, 0x04afc, 0x05c2c], dosVersion, fSize)
    discard process(exeMemory, customSaved.checkpointClearTileCol, [0x04bbc,
        0x0626c, 0x05044, 0x05784, 0x04b00, 0x05c30], dosVersion, fSize)
    readOk = process(exeMemory, tempWord, [0x04bbf, 0x0626f, 0x05047, 0x05787,
        0x04b03, 0x05c33], dosVersion, fSize) # row of the tile to clear
    if readOk:
      if tempWord == 49195:
        customSaved.checkpointClearTileRow = 0
      elif tempWord == 432:
        customSaved.checkpointClearTileRow = 1
      elif tempWord == 688:
        customSaved.checkpointClearTileRow = 2
    discard process(exeMemory, customSaved.skeletonLevel, [0x046a4, 0x05d54, -1,
        -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.skeletonRoom, [0x046b8, 0x05d68, -1,
        -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.skeletonTriggerColumn1, [0x046cc,
        0x05d7c, -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.skeletonTriggerColumn2, [0x046d3,
        0x05d83, -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.skeletonColumn, [0x046de, 0x05d8e,
        0x04b5e, 0x0529e, 0x0461a, 0x0574a], dosVersion, fSize)
    discard process(exeMemory, customSaved.skeletonRow, [0x046e2, 0x05d92,
        0x04b62, 0x052a2, 0x0461e, 0x0574e], dosVersion, fSize)
    readOk = process(exeMemory, tempBytes[0], [0x046c3, 0x05d73, -1, -1, -1,
        -1], dosVersion, fSize)
    if readOk:
      customSaved.skeletonRequireOpenLevelDoor = byte(tempBytes[0] == 0xEB)
    discard process(exeMemory, customSaved.skeletonSkill, [0x0478f, 0x05e3f, -1,
        -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.skeletonReappearRoom, [0x03b32,
        0x051e2, 0x03fb2, 0x046f2, 0x03a6e, 0x04b9e], dosVersion, fSize)
    discard process(exeMemory, customSaved.skeletonReappearX, [0x03b39, 0x051e9,
        -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.skeletonReappearRow, [0x03b3e,
        0x051ee, -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.skeletonReappearDir, [0x03b43,
        0x051f3, -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.mirrorLevel, [0x08dc7, 0x0a477,
        0x09274, 0x099b4, 0x08d30, 0x09e60], dosVersion, fSize)
    readOk = process(exeMemory, customSaved.mirrorRoom, [0x08dcb, 0x0a47b,
        0x09278, 0x099b8, 0x08d34, 0x09e64], dosVersion, fSize)
    if readOk:
      var
        # If opcode is not initialized, I get a warning, but only with -O2: "'opcode' may be used uninitialized". Huh?
        opcode: byte = 0
      discard process(exeMemory, opcode, [0x08dcb+2, 0x0a47b+2, 0x09278+2,
          0x099b8+2, 0x08d34+2, 0x09e64+2], dosVersion, fSize)
      if opcode == 0x50'u8:
        # 0xA47A: B8 XX 00 50 50 where XX is room *and* column!
        customSaved.mirrorColumn = customSaved.mirrorRoom
      elif opcode == 0x64'u8:
        # 0xA47A: 68 RR 00 6A CC where RR is the room, CC is the column
        discard process(exeMemory, customSaved.mirrorColumn, [0x08dcb+3,
            0x0a47b+3, 0x09278+3, 0x099b8+3, 0x08d34+3, 0x09e64+3], dosVersion, fSize)
    readOk = process(exeMemory, tempWord, [0x08dcf, 0x0a47f, 0x0927c, 0x099bc,
        0x08d38, 0x09e68], dosVersion, fSize) # mirror row
    if readOk:
      if tempWord == 0xC02B:
        # 2B C0 = sub ax,ax
        customSaved.mirrorRow = 0
      elif tempWord == 0x01B0:
        # B0 01 = mov al,1
        customSaved.mirrorRow = 1
      elif tempWord == 0x02B0:
        # B0 02 = mov al,2
        customSaved.mirrorRow = 2
    discard process(exeMemory, customSaved.mirrorTile, [0x08de3, 0x0a493,
        0x09290, 0x099d0, 0x08d4c, 0x09e7c], dosVersion, fSize)
    readOk = process(exeMemory, tempBytes[0], [0x051a2, 0x06852, 0x05636,
        0x05d76, 0x050f2, 0x06222], dosVersion, fSize)
    if readOk:
      customSaved.showMirrorImage = byte(tempBytes[0] != 0xEB)
    discard process(exeMemory, customSaved.shadowStealLevel, [-1, 0x5017, -1,
        -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.shadowStealRoom, [-1, 0x5021, -1, -1,
        -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.shadowStepLevel, [-1, 0x4FE7, -1, -1,
        -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.shadowStepRoom, [-1, 0x4FF1, -1, -1,
        -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.fallingExitLevel, [0x03eb2, 0x05562,
        -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.fallingExitRoom, [0x03eb9, 0x05569,
        -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.fallingEntryLevel, [0x04cbd, 0x0636d,
        -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.fallingEntryRoom, [0x04cc4, 0x06374,
        -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.mouseLevel, [0x05166, 0x06816,
        0x055fa, 0x05d3a, 0x050b6, 0x061e6], dosVersion, fSize)
    discard process(exeMemory, customSaved.mouseRoom, [0x0516d, 0x0681d,
        0x05601, 0x05d41, 0x050bd, 0x061ed], dosVersion, fSize)
    discard process(exeMemory, customSaved.mouseDelay, [0x0517f, 0x0682f,
        0x05613, 0x05d53, 0x050cf, 0x061ff], dosVersion, fSize)
    discard process(exeMemory, customSaved.mouseObject, [0x054b3, 0x06b63,
        0x05947, 0x06087, 0x05403, 0x06533], dosVersion, fSize)
    discard process(exeMemory, customSaved.mouseStartX, [0x054b8, 0x06b68,
        0x0594c, 0x0608c, 0x05408, 0x06538], dosVersion, fSize)
    block:
      var
        level: byte = 0
        room: byte = 0
      readOk = process(exeMemory, level, [0x00b84, 0x02234, 0x00c6d, 0x013ad,
          0x00c31, 0x01d61], dosVersion, fSize) # seamless exit
      if readOk:
        readOk = process(exeMemory, room, [0x00b8b, 0x0223b, 0x00c74, 0x013b4,
            0x00c38, 0x01d68], dosVersion, fSize)
      if readOk and level < 16:
        memset(addr(customSaved.tblSeamlessExit[0]), -1, csize_t(sizeof(
            customSaved.tblSeamlessExit[0]) * customSaved.tblSeamlessExit.len))
        customSaved.tblSeamlessExit[level] = sbyte(room)
    discard process(exeMemory, customSaved.looseTilesLevel, [0x0120d, 0x028bd,
        -1, -1, 0x01358, 0x02488], dosVersion, fSize)
    discard process(exeMemory, customSaved.looseTilesRoom1, [0x01214, 0x028c4,
        -1, -1, 0x0135f, 0x0248f], dosVersion, fSize)
    discard process(exeMemory, customSaved.looseTilesRoom2, [0x0121b, 0x028cb,
        -1, -1, 0x01366, 0x02496], dosVersion, fSize)
    discard process(exeMemory, customSaved.looseTilesFirstTile, [0x0122e,
        0x028de, -1, -1, 0x01379, 0x024a9], dosVersion, fSize)
    discard process(exeMemory, customSaved.looseTilesLastTile, [0x0124d,
        0x028fd, -1, -1, 0x01398, 0x024c8], dosVersion, fSize)
    discard process(exeMemory, customSaved.jaffarVictoryLevel, [0x084b3,
        0x09b63, 0x08963, 0x090a3, 0x0841f, 0x0954f], dosVersion, fSize)
    discard process(exeMemory, customSaved.jaffarVictoryFlashTime, [0x084c0,
        0x09b70, 0x08970, 0x090b0, 0x0842c, 0x0955c], dosVersion, fSize)
    discard process(exeMemory, customSaved.hideLevelNumberFromLevel, [0x0c3d9,
        0x0da89, 0x0c8cd, 0x0d00d, 0x0c389, 0x0d4b9], dosVersion, fSize)
    readOk = process(exeMemory, tempBytes[0], [0x0c3d9, 0x0da89, 0x0c8cd,
        0x0d00d, 0x0c389, 0x0d4b9], dosVersion, fSize)
    if readOk:
      customSaved.level13LevelNumber = if tempBytes[0] == 0xEB:
          13
        else:
          12
    discard process(exeMemory, customSaved.victoryStopsTimeLevel, [0x0c2e0,
        0x0d990, -1, -1, -1, -1], dosVersion, fSize)
    discard process(exeMemory, customSaved.winLevel, [0x011dc, 0x0288c, 0x01397,
        0x01ad7, 0x01327, 0x02457], dosVersion, fSize)
    discard process(exeMemory, customSaved.winRoom, [0x011e3, 0x02893, 0x0139e,
        0x01ade, 0x0132e, 0x0245e], dosVersion, fSize)
    discard process(exeMemory, customSaved.looseFloorDelay, [0x9536, 0xABE6, -1,
        -1, -1, -1], dosVersion, fSize)

    # guard skills
    discard process(exeMemory, customSaved.strikeprob, [-1, 0x1D3C2, -1,
        0x1D2B4, -1, 0x19C5E], dosVersion, fSize)
    discard process(exeMemory, customSaved.restrikeprob, [-1, 0x1D3DA, -1,
        0x1D2CC, -1, 0x19C76], dosVersion, fSize)
    discard process(exeMemory, customSaved.blockprob, [-1, 0x1D3F2, -1,
        0x1D2E4, -1, 0x19C8E], dosVersion, fSize)
    discard process(exeMemory, customSaved.impblockprob, [-1, 0x1D40A, -1,
        0x1D2FC, -1, 0x19CA6], dosVersion, fSize)
    discard process(exeMemory, customSaved.advprob, [-1, 0x1D422, -1, 0x1D314,
        -1, 0x19CBE], dosVersion, fSize)
    discard process(exeMemory, customSaved.refractimer, [-1, 0x1D43A, -1,
        0x1D32C, -1, 0x19CD6], dosVersion, fSize)
    discard process(exeMemory, customSaved.extrastrength, [-1, 0x1D452, -1,
        0x1D344, -1, 0x19CEE], dosVersion, fSize)

    # shadow's starting positions
    discard process(exeMemory, customSaved.initShad6, [0x1B8B8, 0x1D47A,
        0x1C6D5, 0x1D36C, 0x18AA7, 0x19D16], dosVersion, fSize)
    discard process(exeMemory, customSaved.initShad5, [0x1B8C0, 0x1D482,
        0x1C6DD, 0x1D374, 0x18AAF, 0x19D1E], dosVersion, fSize)
    discard process(exeMemory, customSaved.initShad12, [-1, 0x1D48A, -1,
        0x1D37C, -1, 0x19D26], dosVersion,
        fSize) # in the packed versions, the five zero bytes at the end are compressed
    # automatic moves
    discard process(exeMemory, customSaved.shadDrinkMove, [-1, 0x1D492, -1,
        0x1D384, -1, 0x19D2E], dosVersion,
        fSize) # in the packed versions, the four zero bytes at the start are compressed
    discard process(exeMemory, customSaved.demoMoves, [0x1B8EE, 0x1D4B2,
        0x1C70B, 0x1D3A4, 0x18ADD, 0x19D4E], dosVersion, fSize)

    # speeds
    discard process(exeMemory, customSaved.baseSpeed, [0x4F01, 0x65B1, 0x5389,
        0x5AC9, 0x4E45, 0x5F75], dosVersion, fSize)
    discard process(exeMemory, customSaved.fightSpeed, [0x4EF9, 0x65A9, 0x5381,
        0x5AC1, 0x4E3D, 0x5F6D], dosVersion, fSize)
    discard process(exeMemory, customSaved.chomperSpeed, [0x8BBD, 0xA26D,
        0x906D, 0x97AD, 0x8B29, 0x9C59], dosVersion, fSize)
    # Skip the mouse in the ending scene. Used in Christmas of Persia.
    # Details: https://forum.princed.org/viewtopic.php?p=34897#p34897
    discard process(exeMemory, tempBytes[0], [0x2B8C, 0x423C, 0x2FE4, 0x3724,
        0x2B28, 0x3C58], dosVersion, fSize)
    discard process(exeMemory, tempBytes[1], [0x2B8C+1, 0x423C+1, 0x2FE4+1,
        0x3724+1, 0x2B28+1, 0x3C58+1], dosVersion, fSize)
    customSaved.noMouseInEnding = byte(tempBytes[0] == 0xEB and tempBytes[1] == 0x27)

    # The order of offsets is: dos10Packed, dos10Unpacked, dos13Packed, dos13Unpacked, dos14Packed, dos14Unpacked

proc loadModOptions() =
  if (useCustomLevelset != 0):
    # find the folder containing the mod's files
    var
      folderName: string
      ok: bool = false
      fi: FileInfo
      locatedFolderName: string
    snprintfCheck(folderName, POP_MAX_PATH, modsFolder & levelsetName)
    locatedFolderName = locateFile(folderName)
    fi = getFileInfo(folderName)

    if fi.kind == pcDir:
      # It's a directory
      ok = true
      var
        modIniFilename: string
      snprintfCheck(modDataPath, POP_MAX_PATH, locatedFolderName)
      # Try to load PRINCE.EXE (DOS)
      loadDosExeModifications(locatedFolderName)
      snprintfCheck(modIniFilename, POP_MAX_PATH, locatedFolderName & "mod.ini")
      if fileExists(modIniFilename):
        useCustomOptions = 1
        iniLoad(modIniFilename)
    else:
      echo "Could not load mod '" & levelsetName & "' - not a directory"
      showDialog("Cannot find the mod " & levelsetName & " in the mods folder.")

      when(USE_REPLAY):
        if (replaying != 0):
          showDialog("If the replay file restarts the level or advances to the next level, wrong level will be loaded.")

    if not(ok):
      useCustomLevelset = 0
      levelsetName = ""
  turnFixesAndEnhancementsOnOff(useFixesAndEnhancements)
  turnCustomOptionsOnOff(useCustomOptions)

proc processRwWrite(rw: ptr RWops, data: pointer, dataSize: csize_t): int32 =
  return int32(write(rw, data, dataSize, 1))

proc processRwRead(rw: ptr RWops, data: pointer, dataSize: csize_t): int32 =
  return int32(read(rw, data, dataSize, 1))
  # if this returns 0, most likely the end of the stream has been reached
