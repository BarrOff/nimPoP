var
  # data:4CB4
  cutsceneWaitFrames: int16
  # data:3D14
  cutsceneFrameTime: int16
  # data:588C
  disableKeys: int16
  # data:436A
  hourglassSandflow: int16
  # data:5964
  hourglassState: int16
  # data:4CC4
  whichTorch: int16

type
  HofType = object
    name: string
    minH, tick: int16

const
  MAX_HOF_COUNT = 6
  N_STARS = 6

var
  # data:589A
  hof: array[MAX_HOF_COUNT, HofType]
  # data:0D92
  hofRects: seq[RectType] = @[
    RectType(top: 84, left: 72, bottom: 96, right: 248),
    RectType(top: 98, left: 72, bottom: 110, right: 248),
    RectType(top: 112, left: 72, bottom: 124, right: 248),
    RectType(top: 126, left: 72, bottom: 138, right: 248),
    RectType(top: 140, left: 72, bottom: 152, right: 248),
    RectType(top: 154, left: 72, bottom: 166, right: 248)
  ]

# seg001:0004
proc procCutsceneFrame(waitFrames: int32): int32 =
  cutsceneWaitFrames = int16(waitFrames)
  resetTimer(ord(timer0))
  setTimerLength(ord(timer0), cutsceneFrameTime)
  playBothSeq()
  # changed order of drects and flash
  drawProomDrects()
  if flashTime != 0:
    doFlash(int16(flashColor))
  if flashTime != 0:
    dec(flashTime)
    removeFlash()
  if checkSoundPlaying() == 0:
    playNextSound()
  if (disableKeys == 0) and (doPaused() != 0):
    stopSounds()
    drawRect(screenRect, 0)
    when(USE_FADE):
      if isGlobalFading != 0:
        fadePaletteBuffer.procRestoreFree(fadePaletteBuffer)
        isGlobalFading = 0
    return 1
  when(USE_FADE):
    if isGlobalFading != 0:
      if fadePaletteBuffer.procFadeFrame(fadePaletteBuffer) != 0:
        fadePaletteBuffer.procRestoreFree(fadePaletteBuffer)
        isGlobalFading = 0
        return 2
    else:
      idle()
      delayTicks(1)
  else:
    idle()
  # busy waiting?
  while(hasTimerStopped(ord(timer0)) == 0):
    if (disableKeys == 0) and (doPaused() != 0):
      stopSounds()
      drawRect(screenRect, 0)
      when(USE_FADE):
        if isGlobalFading != 0:
          fadePaletteBuffer.procRestoreFree(fadePaletteBuffer)
          isGlobalFading = 0
      return 1
    when(USE_FADE):
      if isGlobalFading != 0:
        if fadePaletteBuffer.procFadeFrame(fadePaletteBuffer) != 0:
          fadePaletteBuffer.procRestoreFree(fadePaletteBuffer)
          isGlobalFading = 0
          return 2
      else:
        idle()
        delayTicks(1)
    else:
      idle()
  dec(cutsceneWaitFrames)
  while (cutsceneWaitFrames != 0):
    setTimerLength(ord(timer0), cutsceneFrameTime)
    playBothSeq()
    # changed order of drects and flash
    drawProomDrects()
    if flashTime != 0:
      doFlash(int16(flashColor))
    if flashTime != 0:
      dec(flashTime)
      removeFlash()
    if checkSoundPlaying() == 0:
      playNextSound()
    if (disableKeys == 0) and (doPaused() != 0):
      stopSounds()
      drawRect(screenRect, 0)
      when(USE_FADE):
        if isGlobalFading != 0:
          fadePaletteBuffer.procRestoreFree(fadePaletteBuffer)
          isGlobalFading = 0
      return 1
    when(USE_FADE):
      if isGlobalFading != 0:
        if fadePaletteBuffer.procFadeFrame(fadePaletteBuffer) != 0:
          fadePaletteBuffer.procRestoreFree(fadePaletteBuffer)
          isGlobalFading = 0
          return 2
      else:
        idle()
        delayTicks(1)
    else:
      idle()
    # busy waiting?
    while (hasTimerStopped(ord(timer0)) == 0):
      if (disableKeys == 0) and (doPaused() != 0):
        stopSounds()
        drawRect(screenRect, 0)
        when(USE_FADE):
          if isGlobalFading != 0:
            fadePaletteBuffer.procRestoreFree(fadePaletteBuffer)
            isGlobalFading = 0
        return 1
      when(USE_FADE):
        if isGlobalFading != 0:
          if fadePaletteBuffer.procFadeFrame(fadePaletteBuffer) != 0:
            fadePaletteBuffer.procRestoreFree(fadePaletteBuffer)
            isGlobalFading = 0
            return 2
        else:
          idle()
          delayTicks(1)
      else:
        idle()
    dec(cutsceneWaitFrames)
  return 0

# seg001:00D0
proc playBothSeq() =
  playKidSeq()
  playOppSeq()

# seg001:00E6
proc drawProomDrects() =
  drawPrincessRoomBg()
  when(USE_FADE):
    if isGlobalFading == 0:
      while drectsCount != 0:
        dec(drectsCount)
        copyScreenRect(drects[drectsCount])
  else:
    while drectsCount != 0:
      dec(drectsCount)
      copyScreenRect(drects[drectsCount])
  drectsCount = 0
  if (cutsceneWaitFrames and 1) != 0:
    drawStar(int(prandom(N_STARS - 1)), 1)

# seg001:0128
proc playKidSeq() =
  loadkid()
  if Char.frame != 0:
    playSeq()
    savekid()

# seg001:013F
proc playOppSeq() =
  loadshadAndOpp()
  if Char.frame != 0:
    playSeq()
    saveshad()

# seg001:0156
proc drawPrincessRoomBg() =
  for i in 0..high(tableCounts):
    tableCounts[i] = 0
  loadkid()
  if Char.frame != 0:
    loadFrameToObj()
    objTilepos = 30
    addObjtable(0)
  loadshad()
  if Char.frame != 0:
    loadFrameToObj()
    objTilepos = 30
    addObjtable(0)
  redrawNeededTiles()
  discard addForetable(ord(idChtab8Princessroom), 2, 30, 0, 167, ord(
      blitters10hTransp), 0)
  princessRoomTorch()
  drawHourglass()
  drawTables()

# seg001:01E0
proc seqtblOffsetShadChar(seqIndex: int32) =
  loadshad()
  seqtblOffsetChar(int16(seqIndex))
  saveshad()

# seg001:01F9
proc seqtblOffsetKidChar(seqIndex: int32) =
  loadkid()
  seqtblOffsetChar(int16(seqIndex))
  savekid()

# seg001:0212
proc initMouseCu8() =
  initMouseGo()
  Char.x = 144
  seqtblOffsetChar(ord(seq106Mouse))
  playSeq()

# seg001:022A
proc initMouseGo() =
  Char.charid = ord(charid24Mouse)
  Char.x = 199
  Char.y = 167
  Char.direction = ord(dirFFLeft)
  seqtblOffsetChar(ord(seq105MouseForward))
  playSeq()

# seg001:024D
proc princessCrouching() =
  initPrincess()
  Char.x = 131
  Char.y = 169
  seqtblOffsetChar(ord(seq110PrincessCrouchingPV2))
  playSeq()

# seg001:026A
proc princessStand() =
  initPrincessRight()
  Char.x = 144
  Char.y = 169
  seqtblOffsetChar(ord(seq94PrincessStandPV1))
  playSeq()

# seg001:0287
proc initPrincessX156() =
  initPrincess()
  Char.x = 156

# seg001:0291
proc princessLying() =
  initPrincess()
  Char.x = 92
  Char.y = 162
  seqtblOffsetChar(ord(seq103PrincessLyingPV2)) # princess lying [PV2]
  playSeq()

# seg001:02AE
proc initPrincessRight() =
  initPrincess()
  Char.direction = ord(dir0Right)

# seg001:02B8
proc initEndingPrincess() =
  initPrincess()
  Char.x = 136
  Char.y = 164
  seqtblOffsetChar(ord(seq109PrincessStandPV2)) # princess standing [PV2]
  playSeq()

# seg001:02D5
proc initMouse1() =
  initMouseGo()
  Char.x -= 2
  Char.y = 164

# seg001:02E4
proc initPrincess() =
  Char.charid = ord(charid5Princess)
  Char.x = 120
  Char.y = 166
  Char.direction = ord(dirFFLeft)
  seqtblOffsetChar(ord(seq94PrincessStandPV1)) # princess stand [PV1]
  playSeq()

# seg001:0307
proc initVizier() =
  Char.charid = ord(charid6Vizier)
  Char.x = 198
  Char.y = 166
  Char.direction = ord(dirFFLeft)
  seqtblOffsetChar(ord(seq95JaffarStandPV1)) # Jaffar stand [PV1]
  playSeq()

# seg001:032A
proc initEndingKid() =
  Char.charid = ord(charid0Kid)
  Char.x = 198
  Char.y = 164
  Char.direction = ord(dirFFLeft)
  seqtblOffsetChar(ord(seq1StartRun)) # start run
  playSeq()

# seg001:034D
proc cutscene8() =
  playSound(ord(sound35Cutscene89))
  setHourglassState(hourglassFrame())
  initMouseCu8()
  savekid()
  princessCrouching()
  saveshad()
  if fadeIn1() != 0:
    return
  if procCutsceneFrame(20) != 0:
    return
  # mouse stand up and go
  seqtblOffsetKidChar(ord(seq107MouseStandUpAndGo))
  if procCutsceneFrame(20) != 0:
    return
  # princess stand up [PV2]
  seqtblOffsetShadChar(ord(seq111PrincessStandUpPV2))
  if procCutsceneFrame(20) != 0:
    return
  Kid.frame = 0
  discard fadeOut1()

# # seg001:03B7
proc cutscene9() =
  playSound(ord(sound35Cutscene89))
  setHourglassState(hourglassFrame())
  princessStand()
  saveshad()
  if fadeIn1() != 0:
    return
  initMouseGo()
  savekid()
  if procCutsceneFrame(5) != 0:
    return
  # princess crouch down [PV2]
  seqtblOffsetShadChar(ord(seq112PrincessCrouchDownPV2))
  if procCutsceneFrame(9) != 0:
    return
  # mouse stand
  seqtblOffsetKidChar(ord(seq114MouseStand))
  if procCutsceneFrame(58) != 0:
    return
  discard fadeOut1()

# seg001:041C
proc endSequenceAnim() =
  disableKeys = 1
  if (isSoundOn == 0):
    turnSoundOnOff(0x0F)
  copyScreenRect(screenRect)
  playSound(ord(sound26Embrace)) # arrived to princess
  initEndingPrincess()
  saveshad()
  initEndingKid()
  savekid()
  if procCutsceneFrame(8) != 0:
    return
  seqtblOffsetShadChar(ord(seq108PrincessTurnAndHug)) # princess turn and hug [PV2]
  if procCutsceneFrame(5) != 0:
    return
  seqtblOffsetKidChar(ord(seq13StopRun)) # stop run
  if procCutsceneFrame(2) != 0:
    return
  Kid.frame = 0
  if procCutsceneFrame(39) != 0:
    return
  initMouse1()
  savekid()
  if procCutsceneFrame(9) != 0:
    return
  seqtblOffsetKidChar(ord(seq101MouseStandsUp)) # mouse stands up
  if procCutsceneFrame(41) != 0:
    return
  discard fadeOut1()
  while (checkSoundPlaying() != 0):
    idle()
    delayTicks(1)

# seg001:04D3
proc timeExpired() =
  disableKeys = 1
  setHourglassState(7)
  hourglassSandflow = -1
  playSound(ord(sound36OutOfTime)) # time over
  if fadeIn1() != 0:
    return
  if procCutsceneFrame(2) != 0:
    return
  if procCutsceneFrame(100) != 0:
    return
  discard fadeOut1()
  while (checkSoundPlaying() != 0):
    idle()
    discard doPaused()
    delayTicks(1)

# seg001:0525
proc cutscene12() =
  var
    var2: int16 = int16(hourglassFrame())
  if (var2 >= 6):
    setHourglassState(var2)
    initPrincessX156()
    saveshad()
    playSound(ord(sound40Cutscene12ShortTime)) # cutscene 12 short time
    if fadeIn1() != 0:
      return
    if procCutsceneFrame(2) != 0:
      return
    seqtblOffsetShadChar(98) # princess turn around [PV1]
    if procCutsceneFrame(24) != 0:
      return
    discard fadeOut1()
  else:
    cutscene26()

# seg001:0584
proc cutscene4() =
  playSound(ord(sound27Cutscene24612)) # cutscene 2, 4, 6, 12
  setHourglassState(hourglassFrame())
  princessLying()
  saveshad()
  if fadeIn1() != 0:
    return
  if procCutsceneFrame(26) != 0:
    return
  discard fadeOut1()

# seg001:05B8
proc cutscene26() =
  playSound(ord(sound27Cutscene24612)) # cutscene 2, 4, 6, 12
  setHourglassState(hourglassFrame())
  initPrincessRight()
  saveshad()
  if fadeIn1() != 0:
    return
  if procCutsceneFrame(26) != 0:
    return
  discard fadeOut1()

# seg001:05EC
proc pvScene() =
  initPrincess()
  saveshad()
  if fadeIn1() != 0:
    return
  initVizier()
  savekid()
  if procCutsceneFrame(2) != 0:
    return
  playSound(ord(sound50Story2Princess)) # story 2: princess waiting
  if procCutsceneFrame(1) != 0:
    return
  #idle()
  while (checkSoundPlaying() != 0):
    if procCutsceneFrame(1) != 0:
      return
    #idle()
  cutsceneFrameTime = 8
  if procCutsceneFrame(5) != 0:
    return
  playSound(ord(sound4GateClosing)) # gate closing
  if procCutsceneFrame(1) != 0:
    return
  while (checkSoundPlaying() != 0):
    if procCutsceneFrame(1) != 0:
      return
  playSound(ord(sound51PrincessDoorOpening)) # princess door opening
  if procCutsceneFrame(3) != 0:
    return
  seqtblOffsetShadChar(98) # princess turn around [PV1]
  if procCutsceneFrame(5) != 0:
    return
  seqtblOffsetKidChar(96) # Jaffar walk [PV1]
  if procCutsceneFrame(6) != 0:
    return
  playSound(ord(sound53Story3JaffarComes)) # story 3: Jaffar comes
  seqtblOffsetKidChar(97) # Jaffar stop [PV1]
  if procCutsceneFrame(4) != 0:
    return
  if procCutsceneFrame(18) != 0:
    return
  seqtblOffsetKidChar(96) # Jaffar walk [PV1]
  if procCutsceneFrame(30) != 0:
    return
  seqtblOffsetKidChar(97) # Jaffar stop [PV1]
  if procCutsceneFrame(35) != 0:
    return
  seqtblOffsetKidChar(102) # Jaffar conjuring [PV1]
  cutsceneFrameTime = 7
  if procCutsceneFrame(1) != 0:
    return
  seqtblOffsetShadChar(99) # princess step back [PV1]
  if procCutsceneFrame(17) != 0:
    return
  hourglassState = 1
  flashTime = 5
  flashColor = 15 # white
  if procCutsceneFrame(1) != 0:
    return
  #idle()
  while (checkSoundPlaying() != 0):
    if procCutsceneFrame(1) != 0:
      return
    #idle()
  seqtblOffsetKidChar(100) # Jaffar end conjuring and walk [PV1]
  hourglassSandflow = 0
  if procCutsceneFrame(6) != 0:
    return
  playSound(ord(sound52Story4JaffarLeaves)) # story 4: Jaffar leaves
  if procCutsceneFrame(24) != 0:
    return
  hourglassState = 2
  if procCutsceneFrame(9) != 0:
    return
  seqtblOffsetShadChar(113) # princess look down [PV1]
  if procCutsceneFrame(28) != 0:
    return
  discard fadeOut1()

# seg001:07C7
proc setHourglassState(state: int32) =
  hourglassSandflow = 0
  hourglassState = int16(state)

const
  # data:0DEC
  timeBound: array[4, int16] = [6'i16, 17'i16, 33'i16, 65'i16]

# seg001:07DA
proc hourglassFrame(): int32 =
  var
    boundIndex: int16
  for i in 0..<4:
    if (timeBound[i] > remMin):
      boundIndex = int16(i)
      break
  return 6 - boundIndex

var
  # data:0DF4
  princessTorchPosXh: array[2, int16] = [11'i16, 26'i16]
  # data:0DF8
  princessTorchPosXl: array[2, int16] = [5'i16, 3'i16]
  # data:0DFC
  princessTorchFrame: array[2, int16] = [1'i16, 6'i16]

# seg001:0808
proc princessRoomTorch() =
  var
    which: int16 = 2
  while which != 0:
    dec(which)
    whichTorch = if whichTorch == 0: 1 else: 0
    princessTorchFrame[whichTorch] = getTorchFrame(princessTorchFrame[whichTorch])
    discard addBacktable(ord(idChtab1Flameswordpotion), int32(
        princessTorchFrame[whichTorch]) + 1, int8(princessTorchPosXh[
        whichTorch]), int8(princessTorchPosXl[whichTorch]), 116, 0, 0)

  # seg001:0863
proc drawHourglass() =
  if hourglassSandflow >= 0:
    hourglassSandflow = (hourglassSandflow + 1) mod 3
    if hourglassState >= 7:
      return
    discard addForetable(ord(idChtab8Princessroom), hourglassSandflow + 10, 20,
        0, 164, ord(blitters10hTransp), 0)
  if hourglassState != 0:
    discard addMidtable(ord(idChtab8Princessroom), hourglassState + 2, 19, 0,
        168, ord(blitters10hTransp), 1)

# seg001:08CA
proc resetCutscene() =
  Guard.frame = 0
  Kid.frame = 0
  whichTorch = 0
  disableKeys = 0
  hourglassState = 0
  # memsetNear(byte1ED6E, 0, 8) # not used elsewhere
  hourglassSandflow = -1
  cutsceneFrameTime = 6
  clearTileWipes()
  nextSound = -1

# seg001:0908
proc doFlash(color: int16) =
  # stub
  if color != 0:
    if graphicsMode == ord(gmMcgaVga):
      resetTimer(ord(timer2))
      setTimerLength(ord(timer2), 2)
      setBgAttr(0, color)
      if color != 0:
        doSimpleWait(ord(timer2)) # give some time to show the flash
    else:
      discard # ...

proc delayTicks(ticks: uint32) =
  when (USEREPLAY):
    if (replaying and skippingReplay) != 0:
      return
  delay(uint32(float(ticks) * (1000 / 60)))

# seg001:0981
proc removeFlash() =
  # stub
  if graphicsMode == ord(gmMcgaVga):
    setBgAttr(0, 0)
  else:
    discard # ...

# seg001:09D7
proc endSequence() =
  var
    peel: ptr PeelType
    bgcolor: int16
    color: int16
    rect: RectType
    hofIndex: int16
    hofIndex2: int16
  color = 0
  bgcolor = 15
  loadIntro(1, endSequenceAnim, 1)
  clearScreenAndSounds()
  isEndingSequence = true # added (fix being able to pause the game during the end sequence)
  loadOptSounds(ord(sound56EndingMusic), ord(sound56EndingMusic)) # winning theme
  playSoundFromBuffer(soundPointers[ord(sound56EndingMusic)]) # winning theme
  if (offscreenSurface != nil):
    freeSurface(offscreenSurface) # missing in original
  offscreenSurface = makeOffscreenBuffer(screenRect)
  loadTitleImages(0)
  currentTargetSurface = offscreenSurface
  drawFullImage(STORY_FRAME)
  drawFullImage(STORY_HAIL)
  fadeIn2(offscreenSurface, 0x800)
  discard popWait(ord(timer0), 900)
  startTimer(ord(timer0), 240)
  drawFullImage(TITLE_MAIN)
  transitionLtr()
  discard doWait(ord(timer0))
  for i in 0..<hofCount:
    if (hof[i].minH < remMin or (hof[i].minH == remMin and word(hof[i].tick) < remTick)):
      hofindex = int16(i)
      break
  if (hofIndex < MAX_HOF_COUNT and hofIndex <= hofCount):
    fadeOut2(0x1000)
    hofIndex2 = 5
    while hofIndex2 >= hofIndex + 1:
      hof[hofIndex2] = hof[hofIndex2 - 1]
      dec(hofIndex2)
    hof[hofIndex2].name = ""
    hof[hofIndex2].minH = remMin
    hof[hofIndex2].tick = int16(remTick)
    if (hofCount < MAX_HOF_COUNT):
      inc(hofCount)
    drawFullImage(STORY_FRAME)
    drawFullImage(HOF_POP)
    showHof()
    discard offset4RectAdd(rect, hofRects[hofIndex], -4, -1, -40, -1)
    peel = readPeelFromScreen(rect)
    if graphicsMode == ord(gmMcgaVga):
      color = 0xBE
      bgcolor = 0xB7
    drawRect(rect, bgcolor)
    fadeIn2(offscreenSurface, 0x1800)
    currentTargetSurface = onscreenSurface
    while(inputStr(rect, hof[hofIndex].name, 24, "", 0, 4, color, bgcolor) <= 0):
      continue
    restorePeel(peel)
    showHofText(hofRects[hofIndex], -1, 0, hof[hofIndex].name)
    hofWrite()
    discard popWait(ord(timer0), 120)
    currentTargetSurface = offscreenSurface
    drawFullImage(TITLE_MAIN)
    transitionLtr()

  while (checkSoundPlaying() and not(keyTestQuit())) != 0:
    idle()
    delayTicks(1)

  fadeOut2(0x1000)
  startLevel = -1
  isEndingSequence = false
  startGame()

# seg001:0C94
proc expired() =
  if (demoMode == 0):
    if (offscreenSurface != nil):
      freeSurface(offscreenSurface) # missing in original
    offscreenSurface = nil
    clearScreenAndSounds()
    offscreenSurface = makeOffscreenBuffer(screenRect)
    loadIntro(1, timeExpired, 1)

  startLevel = -1
  startGame()

# seg001:0CCD
proc loadIntro(whichImgs: int32, `func`: CutscenePtrType, freeSounds: int32) =
  var
    currentStar: int16
  drawRect(screenRect, 0)
  if freeSounds != 0:
    freeOptionalSounds()

  freeAllChtabsFrom(ord(idChtab3Princessinstory))
  loadChtabFromFile(ord(idChtab8Princessroom), 950, "PV.DAT", 1 shl 13)
  loadChtabFromFile(ord(idChtab9Princessbed), 980, "PV.DAT", 1 shl 14)
  currentTargetSurface = offscreenSurface
  discard method6BlitImgToScr(getImage(ord(idChtab8Princessroom), 0), 0, 0, 0)
  discard method6BlitImgToScr(getImage(ord(idChtab9Princessbed), 0), 0, 142,
      ord(blitters2Or))

  # Free the images that are not needed anymore.
  freeAllChtabsFrom(ord(idChtab9Princessbed))
  freeSurface(getImage(ord(idChtab8Princessroom), 0))
  if (chtabAddrs[ord(idChtab8Princessroom)] != nil):
    chtabAddrs[ord(idChtab8Princessroom)].images[0] = nil

  loadChtabFromFile(ord(idChtab3Princessinstory), 800, "PV.DAT", 1 shl 9)
  loadChtabFromFile(ord(idChtab4JaffarinstoryPrincessincutscenes),
                       50*whichImgs + 850, "PV.DAT", 1 shl 10)
  for currentStar in 0..<N_STARS:
    drawStar(currentStar, 0)

  currentTargetSurface = onscreenSurface
  while (checkSoundPlaying() != 0):
    idle()
    discard doPaused()
    delayTicks(1)

  needDrects = 1
  resetCutscene()
  isCutscene = 1
  `func`()
  isCutscene = 0
  freeAllChtabsFrom(3)
  drawRect(screenRect, 0)

type
  StarType = object
    x, y, color: int16

var
  # data:0DC2
  stars: array[NSTARS, StarType] = [
      StarType(x: 20, y: 97, color: 0),
      StarType(x: 16, y: 104, color: 1),
      StarType(x: 23, y: 110, color: 2),
      StarType(x: 17, y: 116, color: 3),
      StarType(x: 24, y: 120, color: 4),
      StarType(x: 18, y: 128, color: 0)
    ]
const
  N_STAR_COLORS = 5
  # data:0DE6
  starColors: array[N_STAR_COLORS, byte] = [8'u8, 7'u8, 15'u8, 15'u8, 7'u8]

# seg001:0E1C
proc drawStar(whichStar: int32, markDirty: int32) =
  # The stars in the window of the princess's room.
  var
    rect: RectType
    starColor: int16
  starColor = 15
  rect.right = stars[whichStar].x
  rect.left = stars[whichStar].x
  inc(rect.right)
  rect.bottom = stars[whichStar].y
  rect.top = stars[whichStar].y
  inc(rect.bottom)
  if (graphicsMode != ord(gmCga) and graphicsMode != ord(gmHgaHerc)):
    stars[whichStar].color = (stars[whichStar].color + 1) mod N_STAR_COLORS
    starColor = int16(starColors[stars[whichStar].color])

  drawRect(rect, starColor)
  if markDirty != 0:
    addDrect(rect)

# seg001:0E94
proc showHof() =
  # Hall of Fame
  var
    timeText: string
  for index in 0..<hofCount:
    when (ALLOW_INFINITE_TIME):
      var
        minutes, seconds: int32
      if hof[index].minH > 0:
        minutes = hof[index].minH - 1
        seconds = hof[index].tick div 12
      else:
        # negative minutes means time ran 'forward' from 0:00 upwards
        minutes = abs(hof[index].minH) - 1
        seconds = (719 - hof[index].tick) div 12

      timeText = $(minutes) & ":" & $(seconds)
    else:
      timeText = $(hof[index].minH - 1) & ":" & $(hof[index].tick / 12)

    showHofText(hofRects[index], -1, 0, hof[index].name)
    showHofText(hofRects[index], 1, 0, timeText)
  # stub

const
  hofFile: string = "PRINCE.HOF"

proc getHofPath(customPathBuffer: var string, maxLen: int32): string =
  if (useCustomLevelset == 0):
    return hofFile

  # if playing a custom levelset, try to use the mod folder
  customPathBuffer = modDataPath & hofFile
  return customPathBuffer

# seg001:0F17
proc hofWrite() =
  var
    handle: File
    customHofPath: string
    hofPath = getHofPath(customHofPath, sizeof(customHofPath))
  discard open(handle, hofPath, fmWrite)
  if (handle == nil or writeBuffer(handle, addr(hofCount), 2) != 2 or
      writeBuffer(handle, addr(hof), sizeof(hof)) != sizeof(hof)):
    sdlperror(hofPath)
  if (handle != nil):
    close(handle)

# seg001:0F6C
proc hofRead() =
  var
    handle: File
    customHofPath: string
    hofPath = getHofPath(customHofPath, sizeof(customHofPath))
  hofCount = 0
  discard open(handle, hofPath, fmRead)
  if (handle == nil):
    return
  if (readBuffer(handle, addr(hofCount), 2) != 2 or readBuffer(handle, addr(
      hof), sizeof(hof)) != sizeof(hof)):
    sdlperror(hofPath)
    hofCount = 0

  close(handle)

# seg001:0FC3
proc showHofText(rect: var RectType, xAlign, yAlign: int32, text: string) =
  var
    shadowColor: int16
    textColor: int16
    rect2: RectType
  textColor = 15
  shadowColor = 0
  if (graphicsMode == ord(gmMcgaVga)):
    textColor = 0xB7

  discard offset2Rect(rect2, rect, 1, 1)
  showTextWithColor(rect2, xAlign, yAlign, text, shadowColor)
  showTextWithColor(rect, xAlign, yAlign, text, textColor)

# seg001:1029
proc fadeIn1(): int32 =
  when (USE_FADE):
    #	sbyte index
    var
      interrupted: word
    if (graphicsMode == ord(gmMcgaVga)):
      fadePaletteBuffer = makePalBufferFadein(offscreenSurface, 0x6689, 2)
      isGlobalFading = 1
      interrupted = word(procCutsceneFrame(1))
      if (interrupted == 1):
        return 1
      while interrupted == 0:
        interrupted = word(procCutsceneFrame(1))
        if (interrupted == 1):
          return 1
      isGlobalFading = 0
    else:
      discard # ...

    return 0
  else:
    # stub
    method1BlitRect(onscreenSurface, offscreenSurface, screenRect, screenRect, 0)
    updateScreen()
    #	SDLUpdateRect(onscreenSurface_, 0, 0, 0, 0) # debug
    return 0

# seg001:112D
proc fadeOut1(): int32 =
  when (USE_FADE):
    var
      interrupted: word
    if (graphicsMode == ord(gmMcgaVga)):
      fadePaletteBuffer = makePalBufferFadeout(0x6689, 2)
      isGlobalFading = 1
      interrupted = word(procCutsceneFrame(1))
      if interrupted == 1:
        return 1
      while interrupted == 0:
        interrupted = word(procCutsceneFrame(1))
        if interrupted == 1:
          return 1
      isGlobalFading = 0
    else:
      discard # ...
  # stub
  return 0
