var
  # data:432F
  bumpColLeftOfWall: sbyte
  # data:436E
  bumpColRightOfWall: sbyte
  # data:4C0A
  rightCheckedCol: sbyte
  # data:408A
  leftCheckedCol: sbyte

  # data:4C0C
  collTileLeftXpos: int16


const
  # These two arrays are indexed with the return value of wallType.
  # data:24BA
  wallDistFromLeft: array[6, sbyte] = [0'i8, 10, 0, -1, 0, 0]
  # data:24C0
  wallDistFromRight: array[6, sbyte] = [0'i8, 0, 10, 13, 0, 0]


# seg004:0004
proc checkCollisions() =
  bumpColLeftOfWall = -1
  bumpColRightOfWall = -1
  if (Char.action == ord(actions7Turn)):
    return
  collisionRow = Char.currRow
  moveCollToPrev()
  prevCollisionRow = collisionRow
  rightCheckedCol = sbyte(min(getTileDivModM7(charXRightColl) + 2, 11))
  leftCheckedCol = sbyte(getTileDivModM7(charXLeftColl) - 1)
  getRowCollisionData(collisionRow, currRowCollRoom, currRowCollFlags)
  getRowCollisionData(collisionRow + 1, belowRowCollRoom, belowRowCollFlags)
  getRowCollisionData(collisionRow - 1, aboveRowCollRoom, aboveRowCollFlags)
  for column in countdown(9'i8, 0'i8, 1):
    if (currRowCollRoom[column] >= 0 and
      prevCollRoom[column] == currRowCollRoom[column]):
      # char bumps into left of wall
      if (prevCollFlags[column] and 0x0F) == 0 and (currRowCollFlags[column] and
          0x0F) != 0:
        bumpColLeftOfWall = column

      # char bumps into right of wall
      if (prevCollFlags[column] and 0xF0) == 0 and (currRowCollFlags[column] and
          0xF0) != 0:
        bumpColRightOfWall = column


# seg004:00DF
proc moveCollToPrev() =
  var
    rowCollRoomPtr: array[10, sbyte]
    rowCollFlagsPtr: array[10, byte]
  if (collisionRow == prevCollisionRow or
      collisionRow + 3 == prevCollisionRow or
      collisionRow - 3 == prevCollisionRow):
    rowCollRoomPtr = currRowCollRoom
    rowCollFlagsPtr = currRowCollFlags
  elif (
      collisionRow + 1 == prevCollisionRow or
      collisionRow - 2 == prevCollisionRow):
    rowCollRoomPtr = aboveRowCollRoom
    rowCollFlagsPtr = aboveRowCollFlags
  else:
    rowCollRoomPtr = belowRowCollRoom
    rowCollFlagsPtr = belowRowCollFlags

  for column in 0 ..< 10:
    prevCollRoom[column] = rowCollRoomPtr[column]
    prevCollFlags[column] = rowCollFlagsPtr[column]
    belowRowCollRoom[column] = -1
    aboveRowCollRoom[column] = -1
    currRowCollRoom[column] = -1
    when (FIX_COLL_FLAGS):
      # bugfix:
      currRowCollFlags[column] = 0
      belowRowCollFlags[column] = 0
      aboveRowCollFlags[column] = 0


# seg004:0185
proc getRowCollisionData(row: int16, rowCollRoomPtr: var openArray[sbyte],
    rowCollFlagsPtr: var openArray[byte]) =
  var
    rightWallXpos: int16
    currFlags: byte
    room: int16
    leftWallXpos: int16
  room = int16(Char.room)
  collTileLeftXpos = int16(xBump[leftCheckedCol + FIRST_ONSCREEN_COLUMN]) + TILE_MIDX
  for column in leftCheckedCol..rightCheckedCol:
    leftWallXpos = int16(getLeftWallXpos(room, column, row))
    rightWallXpos = int16(getRightWallXpos(room, column, row))
    # char bumps into left of wall
    currFlags = byte(leftWallXpos < charXRightColl) * 0x0F'u8
    # char bumps into right of wall
    currFlags = currFlags or (byte(rightWallXpos > charXLeftColl) * 0xF0'u8)
    rowCollFlagsPtr[tileCol] = currFlags
    rowCollRoomPtr[tileCol] = sbyte(currRoom)
    collTileLeftXpos += TILE_SIZEX


# seg004:0226
proc getLeftWallXpos(room, column, row: int32): int32 =
  var
    `type`: int16
  `type` = int16(wallType(getTile(room, column, row)))
  if `type` != 0:
    return wallDistFromLeft[`type`] + collTileLeftXpos
  else:
    return 0xFF


# seg004:025F
proc getRightWallXpos(room, column, row: int32): int32 =
  var
    `type`: int16
  `type` = int16(wallType(getTile(room, column, row)))
  if `type` != 0:
    return collTileLeftXpos - wallDistFromRight[`type`] + TILE_RIGHTX
  else:
    return 0


# seg004:029D
proc checkBumped() =
  # frames 135..149: climb up
  if (Char.action != ord(actions2HangClimb) and Char.action != ord(
      actions6HangStraight) and (Char.frame < ord(frame135Climbing1) or
      Char.frame >= 149)):
    when (FIX_TWO_COLL_BUG):
      if (bumpColLeftOfWall >= 0):
        checkBumpedLookRight()
        if fixes.fixTwoCollBug == 0:
          return # check for the left-oriented collision only with the fix enabled

      if (bumpColRightOfWall >= 0):
        checkBumpedLookLeft()

    else:
      if (bumpColLeftOfWall >= 0):
        checkBumpedLookRight()

      elif (bumpColRightOfWall >= 0):
        checkBumpedLookLeft()


# seg004:02D2
proc checkBumpedLookLeft() =
  if ((Char.sword == ord(sword2Drawn) or Char.direction < ord(dir0Right)) and # looking left
    (isObstacleAtCol(bumpColRightOfWall) != 0)):
    when(USE_JUMP_GRAB):
      # Prince can grab a floor on top of a wall during a jump if Shift and
      # up arrow keys are pressed.
      if fixes.enableJumpGrab != 0 and controlShift == CONTROL_HELD:
        if checkGrabRunJump():
          return
        # reset obstacle tile values
        discard isObstacleAtCol(bumpColRightOfWall)
    bumped(sbyte(getRightWallXpos(currRoom, tileCol, tileRow) - charXLeftColl),
      ord(dir0Right))


# seg004:030A
proc checkBumpedLookRight() =
  if ((Char.sword == ord(sword2Drawn) or Char.direction == ord(dir0Right)) and # looking right
    (isObstacleAtCol(bumpColLeftOfWall) != 0)):
    when(USE_JUMP_GRAB):
      # Prince can grab a floor on top of a wall during a jump if Shift and
      # up arrow keys are pressed.
      if fixes.enableJumpGrab != 0 and controlShift == CONTROL_HELD:
        if checkGrabRunJump():
          return
        # reset obstacle tile values
        discard isObstacleAtCol(bumpColLeftOfWall)
    bumped(sbyte(getLeftWallXpos(currRoom, tileCol, tileRow) - charXRightColl),
      ord(dirFFLeft))


# seg004:0343
proc isObstacleAtCol(tileCol: int32): int32 =
  var
    tileRow: int16
  tileRow = Char.currRow
  if (tileRow < 0):
    tileRow += 3

  if (tileRow >= 3):
    tileRow -= 3

  discard getTile(currRowCollRoom[tileCol], tileCol, tileRow)
  return isObstacle()


# seg004:037E
proc isObstacle(): int32 =
  if (currTile2 == tiles10Potion):
    return 0
  elif (currTile2 == tiles4Gate):
    if canBumpIntoGate() == 0:
      return 0
  elif (currTile2 == tiles18Chomper):
    # is the chomper closed?
    if (currRoomModif[currTilepos] != 2):
      return 0
  elif (currTile2 == tiles13Mirror and Char.charid == ord(charid0Kid) and
      Char.frame >= ord(frame39StartRunJump6) and Char.frame < ord(
      frame44RunningJump5) and Char.direction < ord(dir0Right)): # right-to-left only

    currRoomModif[currTilepos] = 0x56 # broken mirror or what?
    jumpedThroughMirror = -1
    return 0

  collTileLeftXpos = int16(xposInDrawnRoom(int32(xBump[tileCol +
      FIRST_ONSCREEN_COLUMN]))) + TILE_MIDX
  return 1


# seg004:0405
proc xposInDrawnRoom(xpos: int32): int32 =
  var
    xposIn: int32 = xpos
  if (word(currRoom) != drawnRoom):
    if (word(currRoom) == room_L or word(currRoom) == room_BL):
      xposIn -= TILE_SIZEX * SCREEN_TILECOUNTX
    elif (word(currRoom) == room_R or word(currRoom) == room_BR):
      xposIn += TILE_SIZEX * SCREEN_TILECOUNTX

  return xposIn


# seg004:0448
proc bumped(deltaX, direction: sbyte) =
  # frame 177: spiked
  if (Char.alive < 0 and Char.frame != ord(frame177Spiked)):
    Char.x += cast[byte](deltaX)
    if (direction < ord(dir0Right)):
      # pushing left
      if (currTile2 == tiles20Wall):
        dec(tileCol)
        discard getTile(currRoom, tileCol, tileRow)

    else:
      # pushing right
      if (currTile2 == tiles12Doortop or
          currTile2 == tiles7DoortopWithFloor or
          currTile2 == tiles20Wall):
        inc(tileCol)
        if (currRoom == 0 and tileCol == 10):
          currRoom = int16(Char.room)
          tileCol = 0

        discard getTile(currRoom, tileCol, tileRow)


    if tileIsFloor(currTile2) != 0:
      bumpedFloor(direction)
    else:
      bumpedFall()


# seg004:04E4
proc bumpedFall() =
  var
    action: int16
  action = int16(Char.action)
  Char.x = byte(charDxForward(-4))
  if (action == ord(actions4InFreefall)):
    Char.fallX = 0
  else:
    seqtblOffsetChar(ord(seq45Bumpfall)) # fall after bumped
    playSeq()

  bumpedSound()


# seg004:0520
proc bumpedFloor(direction: sbyte) =
  var
    frame: int16
    seqIndex: int16
  if (Char.sword != ord(sword2Drawn) and yLand[Char.currRow + 1] - int16(
      Char.y) >= 15):
    bumpedFall()
  else:
    Char.y = byte(yLand[Char.currRow + 1])
    if (Char.fallY >= 22):
      Char.x = byte(charDxForward(-5))
    else:
      Char.fallY = 0
      if Char.alive != 0:
        if (Char.sword == ord(sword2Drawn)):
          if (direction == Char.direction):
            seqtblOffsetChar(ord(seq65BumpForwardWithSword)) # pushed forward with sword (Kid)
            playSeq()
            Char.x = byte(charDxForward(1))
            return
          else:
            seqIndex = ord(seq64PushedBackWithSword) # pushed back with sword

        else:
          frame = int16(Char.frame)
          if (frame == 24 or frame == 25 or
              (frame >= 40 and frame < 43) or
              (frame >= ord(frame102StartFall1) and frame < 107)):
            seqIndex = ord(seq46Hardbump) # bump into wall after run-jump (crouch)
          else:
            seqIndex = ord(seq47Bump) # bump into wall


        seqtblOffsetChar(seqIndex)
        playSeq()
        bumpedSound()


# seg004:05F1
proc bumpedSound() =
  isGuardNotice = 1
  playSound(sound8Bumped) # touching a wall


# seg004:0601
proc clearCollRooms() =
  for i in 0..high(prevCollRoom):
    prevCollRoom[i] = -1
    currRowCollRoom[i] = -1
    belowRowCollRoom[i] = -1
    aboveRowCollRoom[i] = -1
    when (FIX_COLL_FLAGS):
      # workaround
      prevCollFlags[i] = 0
      currRowCollFlags[i] = 0
      belowRowCollFlags[i] = 0
      aboveRowCollFlags[i] = 0
  prevCollisionRow = -1


# seg004:0657
proc canBumpIntoGate(): int32 =
  return int32(word(currRoomModif[currTilepos] shr 2'u8) + 6'u16 < charHeight)


# seg004:067C
proc getEdgeDistance(): int32 =
#Possible results in edgeType:
#0: closer/sword/potion
#1: edge
#2: floor (nothing near char)
  var
    distance: int16
    tiletype: Tiles
  determineCol()
  loadFrameToObj()
  setCharCollision()
  tiletype = getTileAtChar()
  block tileBlock:
    if (wallType(tiletype) != 0):
      tileCol = Char.currCol
      distance = int16(distFromWallForward(tiletype))
      if (distance >= 0):
        if (distance < TILE_RIGHTX):
          edgeType = EDGE_TYPE_WALL
        else:
          edgeType = EDGE_TYPE_FLOOR
          distance = 11

      else:
        tiletype = getTileInfrontofChar()
        if (tiletype == tiles12Doortop and Char.direction >= ord(dir0Right)):
          edgeType = EDGE_TYPE_CLOSER
          distance = int16(distanceToEdgeWeight())
        else:
          if (wallType(tiletype) != 0):
            tileCol = infrontx
            distance = int16(distFromWallForward(tiletype))
            if (distance >= 0):
              if (distance < 14):
                edgeType = EDGE_TYPE_WALL
              else:
                edgeType = EDGE_TYPE_FLOOR
                distance = 11
              break tileBlock

          if (tiletype == tiles11Loose):
            edgeType = EDGE_TYPE_CLOSER
            distance = int16(distanceToEdgeWeight())
            break tileBlock
          if (
              tiletype == tiles6Closer or
              tiletype == tiles22Sword or
              tiletype == tiles10Potion):
            distance = int16(distanceToEdgeWeight())
            if (distance != 0):
              edgeType = EDGE_TYPE_CLOSER
            else:
              edgeType = EDGE_TYPE_FLOOR
              distance = 11

          else:
            if tileIsFloor(tiletype) != 0:
              edgeType = EDGE_TYPE_FLOOR
              distance = 11
            else:
              edgeType = EDGE_TYPE_CLOSER
              distance = int16(distanceToEdgeWeight())

    else:
      tiletype = getTileInfrontofChar()
      if (tiletype == tiles12Doortop and Char.direction >= ord(dir0Right)):
        edgeType = EDGE_TYPE_CLOSER
        distance = int16(distanceToEdgeWeight())
      else:
        if (wallType(tiletype) != 0):
          tileCol = infrontx
          distance = int16(distFromWallForward(tiletype))
          if (distance >= 0):
            if (distance < 14):
              edgeType = EDGE_TYPE_WALL
            else:
              edgeType = EDGE_TYPE_FLOOR
              distance = 11
            break tileBlock

        if (tiletype == tiles11Loose):
          edgeType = EDGE_TYPE_CLOSER
          distance = int16(distanceToEdgeWeight())
          break tileBlock
        if (
            tiletype == tiles6Closer or
            tiletype == tiles22Sword or
            tiletype == tiles10Potion):
          distance = int16(distanceToEdgeWeight())
          if (distance != 0):
            edgeType = EDGE_TYPE_CLOSER
          else:
            edgeType = EDGE_TYPE_FLOOR
            distance = 11

        else:
          if tileIsFloor(tiletype) != 0:
            edgeType = EDGE_TYPE_FLOOR
            distance = 11
          else:
            edgeType = EDGE_TYPE_CLOSER
            distance = int16(distanceToEdgeWeight())

  currTile2 = tiletype
  return int32(distance)


# seg004:076B
proc checkChompedKid() =
  let
    tileRow: int32 = int32(Char.currRow)
  for tileCol in 0'i32 ..< 10:
    if (currRowCollFlags[tileCol] == 0xFF and
        getTile(int32(currRowCollRoom[tileCol]), tileCol, tileRow) ==
            tiles18Chomper and (currRoomModif[currTilepos] and 0x7F) == 2): # closed chomper
      chomped()


# seg004:07BF
proc chomped() =
  when (FIX_SKELETON_CHOMPER_BLOOD):
    if not((fixes.fixSkeletonChomperBlood != 0) and Char.charid == ord(
        charid4Skeleton)):
      currRoomModif[currTilepos] = byte(currRoomModif[currTilepos] or 0x80) # put blood
  else:
    currRoomModif[currTilepos] = byte(currRoomModif[currTilepos] or 0x80) # put blood
  if (Char.frame != ord(frame178Chomped) and int16(Char.room) == currRoom):
    when (FIX_OFFSCREEN_GUARDS_DISAPPEARING):
      # a guard can get teleported to the other side of kid's room
      # when hitting a chomper in another room
      if (fixes.fixOffscreenGuardsDisappearing != 0'u8):
        var
          chomperCol: int16 = tileCol
        if (currRoom != int16(Char.room)):
          if (currRoom == int16(level.roomlinks[Char.room - 1].right)):
            chomperCol += SCREEN_TILECOUNTX
          elif (currRoom == int16(level.roomlinks[Char.room - 1].left)):
            chomperCol -= SCREEN_TILECOUNTX
        Char.x = byte(xBump[chomperCol + FIRST_ONSCREEN_COLUMN] + TILE_MIDX)
      else:
        Char.x = byte(xBump[tileCol + FIRST_ONSCREEN_COLUMN] + TILE_MIDX)
    else:
      Char.x = byte(xBump[tileCol + FIRST_ONSCREEN_COLUMN] + TILE_MIDX)
    Char.x = byte(charDxForward(7 - not(Char.direction)))
    Char.y = byte(yLand[Char.currRow + 1])
    discard takeHp(100)
    playSound(sound46Chomped) # something chomped
    seqtblOffsetChar(ord(seq54Chomped)) # chomped
    playSeq()


# seg004:0833
proc checkGatePush() =
  # Closing gate pushes Kid
  let
    frame: int16 = int16(Char.frame)

  if (Char.action == ord(actions7Turn) or frame == ord(frame15Stand) or # stand
    (frame >= ord(frame108FallLand2) and frame < 111)): # crouch
    discard getTileAtChar()
    let
      origCol: int16 = tileCol
      origRoom: int32 = currRoom
    dec(tileCol)
    if ((currTile2 == tiles4Gate or
        getTile(currRoom, tileCol, tileRow) == tiles4Gate) and
        (currRowCollFlags[tileCol] and prevCollFlags[tileCol]) == 0xFF and
        (canBumpIntoGate() != 0)):
      bumpedSound()
      when (FIX_CAPED_PRINCE_SLIDING_THROUGH_GATE):
        if (fixes.fixCapedPrinceSlidingThroughGate != 0'u8):
          # If get_tile() changed curr_room from orig_room to the left neighbor of orig_room (because tile_col was outside room orig_room),
          # then change tile_col (and curr_room) so that orig_col and tile_col are meant in the same room.
          if (currRoom == int16(level.roomlinks[origRoom - 1].left)):
            tileCol -= 10
            currRoom = int16(origRoom)

      # push Kid left if var4 <= tileCol, gate at char's tile
      # push Kid right if var4 > tileCol, gate is left from char's tile
      Char.x += 5'u8 - byte(origCol <= tileCol) * 10'u8


# seg004:08C3
proc checkGuardBumped() =
  if (
      Char.action == ord(actions1RunJump) and
      Char.alive < 0 and
      Char.sword >= ord(sword2Drawn)):
    if (

        #  Should also check for a wall BEHIND the guard, instead of only the current tile
      (when (FIX_PUSH_GUARD_INTO_WALL): ((fixes.fixPushGuardIntoWall != 0) and
          getTileBehindChar() == tiles20Wall) else: false) or
          getTileAtChar() == tiles20Wall or
      currTile2 == tiles7DoortopWithFloor or
      (currTile2 == tiles4Gate and (canBumpIntoGate() != 0)) or
      (Char.direction >= ord(dir0Right) and (dec(tileCol);
      getTile(currRoom, tileCol, tileRow) == tiles7DoortopWithFloor or
      (currTile2 == tiles4Gate and (canBumpIntoGate() != 0))
    ))):
      loadFrameToObj()
      setCharCollision()
      if isObstacle() != 0:
        var
          deltaX: int16
        deltaX = int16(distFromWallBehind(currTile2))
        if (deltaX < 0 and deltaX > -13):
          Char.x = byte(charDxForward(-deltaX))
          seqtblOffsetChar(ord(seq65BumpForwardWithSword)) # pushed to wall with sword (Guard)
          playSeq()
          loadFramDetCol()


# seg004:0989
proc checkChompedGuard() =
  discard getTileAtChar()
  if checkChompedHere() == 0:
    inc(tileCol)
    discard getTile(currRoom, tileCol, tileRow)
    discard checkChompedHere()


# seg004:09B0
proc checkChompedHere(): int32 =
  if (currTile2 == tiles18Chomper and (currRoomModif[currTilepos] and
      0x7F) == 2):
    collTileLeftXpos = int16(xBump[tileCol + FIRST_ONSCREEN_COLUMN]) + TILE_MIDX
    if (getLeftWallXpos(currRoom, tileCol, tileRow) < charXRightColl and
        getRightWallXpos(currRoom, tileCol, tileRow) > charXLeftColl):
      chomped()
      return 1
    else:
      return 0

  else:
    return 0


# seg004:0A10
proc distFromWallForward(tiletype: Tiles): int32 =
  var
    `type`: int16
  if (tiletype == tiles4Gate and (canBumpIntoGate() == 0)):
    return -1
  else:
    collTileLeftXpos = int16(xBump[tileCol + 5]) + TILE_MIDX
    `type` = int16(wallType(tiletype))
    if (type == 0):
      return -1
    if (Char.direction < ord(dir0Right)):
      # looking left
      #return wallDistFromRight[type] + charXLeftColl - collTileLeftXpos - TILE_RIGHTX
      return charXLeftColl - (collTileLeftXpos + TILE_RIGHTX -
          wallDistFromRight[`type`])
    else:
      # looking right
      return wallDistFromLeft[`type`] + collTileLeftXpos - charXRightColl


# seg004:0A7B
proc distFromWallBehind(tiletype: Tiles): int32 =
  var
    `type`: int16
  `type` = int16(wallType(tiletype))
  if (`type` == 0):
    return 99
  else:
    if (Char.direction >= ord(dir0Right)):
      # looking right
      #return wallDistFromRight[type] + charXLeftColl - collTileLeftXpos - TILE_RIGHTX
      return charXLeftColl - (collTileLeftXpos + TILE_RIGHTX -
          wallDistFromRight[`type`])
    else:
      # looking left
      return wallDistFromLeft[`type`] + collTileLeftXpos - charXRightColl
