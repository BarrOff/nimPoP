# DESCRIPTION:     MIDI playback routines for SDLPoP.
# CREDITS:     The OPL integration code is based in part on Chocolate Doom's MIDI interface, by Simon Howard (GPLv2-licensed).
# Uses the Nuked OPL3 emulator by Alexey Khokholov (GPLv2-licensed).
# MIDI playback code also published as standalone playback program 'popmidi' by Falcury (GPLv3)

const
  MAX_MIDI_CHANNELS = 16
  MAX_OPL_VOICES = 18
  # OPL3 supports more channels (voices) than OPL2: you could increase the number of used voices (try it!)
  # However, released notes can linger longer, so for some tracks it will sound too 'washed out'.
  # For example, listen to these sounds with 18 voices enabled:
  # * The potion music in Potion1.mff and Potion2.mff
  # * The hourglass summoning sound in Jaffar.mff
  NUM_OPL_VOICES = 9
  ONE_SECOND_IN_US = 1_000_000

var
  midiPlaying: int16
  digiAudiospec: ptr AudioSpec = nil
  digiUnavailable: int32

  oplChip: Opl3Chip = new(Opl3Chip)
  instrumentsData: pointer
  instruments: ptr InstrumentType
  numInstruments: int32
  voiceNote: array[MAX_OPL_VOICES, byte]
  voiceInstrument: array[MAX_OPL_VOICES, int]
  voiceChannel: array[MAX_OPL_VOICES, int]
  channelInstrument: array[MAX_MIDI_CHANNELS, int32]
  lastUsedVoice: int32
  numMidiTracks: int32
  parsedMidi: ParsedMidiType
  midiTracks: ptr MidiTrackType
  # midiTracks: seq[MidiTrackType]
  midiCurrentPos: int64          # in MIDI ticks
  midiCurrentPosFractPart: float # partial ticks after the decimal point
  ticksToNextPause: int          # in MIDI ticks
  usPerBeat: dword
  ticksPerBeat: dword
  mixingFreq: int32
  midiSemitonesHigher: sbyte
  currentMidiTempoModifier: float

  # variable shows if Midi is initialized
  initializedMidi: bool = false
    # Tempo adjustments for specific songs:
    # * PV scene, with 'Story 3 Jaffar enters':
  #   Speed must be exactly right, otherwise it will not line up with the flashing animation in the cutscene.
  #   The 'Jaffar enters' song has tempo 705882 (maybe this tempo was carefully fine-tuned)?
  # * Intro music: playback speed must match the title appearances/transitions.
  midiTempoModifiers: array[58, float]
  # The hardcoded instrument is used as a fallback, if instrument data is not
    # available for some reason.
  hardcodedInstrument = InstrumentType(blocknumLow: 0x13'u8,
      blocknumHigh: 0x09'u8, FBConn: 0x04'u8, operators: [OperatorType(
      mul: 0x02, kslTl: 0x8D, aD: 0xD7, sR: 0x37, waveform: 0x00), OperatorType(
          mul: 0x03, kslTl: 0x03, aD: 0xF5, sR: 0x18, waveform: 0x00)],
      percussions: 0x00'u8, unknown: [0x00'u8, 0x00'u8])

# set specific tempo modifiers
midiTempoModifiers[ord(sound53Story3JaffarComes)] = -0.03
midiTempoModifiers[ord(sound54IntroMusic)] = 0.03

# Read a variable length integer (max 4 bytes).
proc midiReadVariableLength(bufferPosition: var ptr byte): dword =
  result = 0
  var
    pos: ptr byte = bufferPosition
    rounds = 0

  for i in 0..<4:
    result = (result shl 7) or dword(cast[ptr UncheckedArray[byte]](pos)[i] and 0x7F)
    inc(rounds)
    if (cast[ptr UncheckedArray[byte]](pos)[i] and 0x80) == 0:
      break

  bufferPosition = addr(cast[ptr UncheckedArray[byte]](bufferPosition)[rounds])

proc freeParsedMidi(parsedMidi: var ParsedMidiType) =
  for i in 0..<parsedMidi.numTracks:
    dealloc(cast[ptr UncheckedArray[MidiTrackType]](parsedMidi.tracks)[i].events)
  dealloc(parsedMidi.tracks)
  zeroMem(addr(parsedMidi), sizeof(parsedMidi))

proc parseMidi(midi: var MidiRawChunkType,
    parsedMidi: var ParsedMidiType): bool =
  parsedMidi.ticksPerBeat = 24
  if (midi.chunktype[0] != 'M') or (midi.chunktype[1] != 'T') or (
      midi.chunktype[2] != 'h') or (midi.chunktype[3] != 'd'):
    echo "Warning: Tried to play a midi sound without the 'MThd' chunk header."
    return false
  var
    swapped32: uint32 = swapBE32(midi.chunkLength)
    swapped16: uint16
  if swapped32 != 6:
    echo ("Warning: Midi file with an invalid header length (expected 6, is ",
        swapped32, ")")
    return false
  swapped16 = swapBE16(midi.header.format)

  if swapped16 >= 2'u16:
    echo "Warning: Unsupported midi format ", swapped16, " (only type 0 or 1 files are supported)"
    return false
  swapped16 = swapBE16(midi.header.numTracks)
  let
    numTracks: uint16 = swapped16
  if swapped16 < 1:
    echo "Warning: Midi sound does not have any tracks."
    return false
  var
    division: int32 = int32(swapBE16(midi.header.timeDivision))
  if division < 0:
    # Translate time delta from the alternative SMTPE format.
    division = -(division div 256) * (division and 0xFF)
  parsedMidi.ticksPerBeat = uint32(division)

  parsedMidi.tracks = cast[ptr MidiTrackType](alloc0(int(numTracks) * sizeof(
      MidiTrackType)))
  parsedMidi.numTracks = int32(numTracks)
  var
    # The first track chunk starts after the header chunk.
    nextTrackChunk: ptr MidiRawChunkType = cast[ptr MidiRawChunkType](addr(
        midi.header.tracks[0]))
    lastEventType = 0'u8
  for trackIndex in 0..<int(numTracks):
    var
      trackChunk = nextTrackChunk
    if (trackChunk.chunkType[0] != 'M') or (trackChunk.chunkType[1] != 'T') or (
        trackChunk.chunkType[2] != 'r') or (trackChunk.chunkType[3] != 'k'):
      echo "Warning: midi track without 'MTrk' chunk header."
      dealloc(parsedMidi.tracks)
      zeroMem(addr(parsedMidi), sizeof(parsedMidi))
      return false
    var
      bufferPosition: ptr byte = cast[ptr byte](addr(trackChunk.header))
    nextTrackChunk = cast[ptr MidiRawChunkType](addr(cast[ptr UncheckedArray[
        byte]](bufferPosition)[swapBE32(trackChunk.chunkLength)]))
    var
      track: ptr MidiTrackType = addr(cast[ptr UncheckedArray[MidiTrackType]](
          parsedMidi.tracks)[trackIndex])
    while true:
      inc(track.numEvents)
      let
        newTrackEvents = cast[ptr MidiEventType](realloc(track.events,
          track.numEvents * sizeof(MidiEventType)))
      if isNil(newTrackEvents):
        echo "parse_midi: realloc failed!"
        quit(1)
      track.events = newTrackEvents
      var
        event: ptr MidiEventType = addr(cast[ptr UncheckedArray[MidiEventType]](
            track.events)[track.numEvents - 1])
      event.deltaTime = midiReadVariableLength(bufferPosition)
      event.eventType = bufferPosition[]
      if (event.eventType and 0x80) != 0:
        if event.eventType < 0xF8:
          lastEventType = event.eventType
        bufferPosition = addr(cast[ptr UncheckedArray[byte]](bufferPosition)[1])
      else:
        event.eventType = lastEventType
      var
        numChannelEventParams = 1
      # Determine the event type and parse the event.
      case event.eventType and 0xF0
      of 0x80, 0x90, 0xA0, 0xB0, 0xE0: # note off, note on, aftertouch, controller, pitch bend
        numChannelEventParams = 2 # fallthrough
        # Read the channel event.
        event.metu.channel.channel = byte(event.eventType and 0x0F)
        event.eventType = byte(event.eventType and 0xF0)
        event.metu.channel.param1 = bufferPosition[]
        bufferPosition = addr(cast[ptr UncheckedArray[byte]](bufferPosition)[1])
        if numChannelEventParams == 2:
          event.metu.channel.param2 = bufferPosition[]
          bufferPosition = addr(cast[ptr UncheckedArray[byte]](bufferPosition)[1])
      of 0xC0, 0xD0: # program change, channel aftertouch
        # Read the channel event.
        event.metu.channel.channel = byte(event.eventType and 0x0F)
        event.eventType = byte(event.eventType and 0xF0)
        event.metu.channel.param1 = bufferPosition[]
        bufferPosition = addr(cast[ptr UncheckedArray[byte]](bufferPosition)[1])
        if numChannelEventParams == 2:
          event.metu.channel.param2 = bufferPosition[]
          bufferPosition = addr(cast[ptr UncheckedArray[byte]](bufferPosition)[1])
      else:
        # Not a channel event
        case event.eventType
        of 0xF0, 0xF7: # SysEx, SysEx split
          # Read SysEx event
          event.metu.sysex.length = midiReadVariableLength(bufferPosition)
          event.metu.sysex.data = bufferPosition
          bufferPosition = addr(cast[ptr UncheckedArray[byte]](bufferPosition)[
              event.metu.sysex.length])
        of 0xFF: # Meta event
          event.metu.meta.`type` = bufferPosition[]
          bufferPosition = addr(cast[ptr UncheckedArray[byte]](bufferPosition)[1])
          event.metu.meta.length = midiReadVariableLength(bufferPosition)
          event.metu.meta.data = bufferPosition
          bufferPosition = addr(cast[ptr UncheckedArray[byte]](bufferPosition)[
              event.metu.meta.length])
        else:
          echo "Warning: unknown midi event type " & $(int(event.eventType)) &
              " (track " & $(trackIndex) & ", event " & $(track.numEvents - 1) & ")"
          freeParsedMidi(parsedMidi)
          return false
      if (event.eventType == 0xFF) and (event.metu.meta.`type` == 0x2F):
        break
      if bufferPosition >= cast[ptr byte](nextTrackChunk):
        echo "Error parsing MIDI events (track ", trackIndex, ")"
        freeParsedMidi(parsedMidi)
        return false
  return true

var
  oplCachedRegs: array[512, byte]

proc oplReset(freq: int32) =
  opl3Reset(oplChip, uint32(freq))
  zeroMem(addr(oplCachedRegs[0]), oplCachedRegs.len * sizeof(oplCachedRegs[0]))

proc oplWriteReg(reg: word, value: byte) =
  opl3WriteReg(oplChip, reg, value)
  oplCachedRegs[reg] = value

proc oplWriteRegMasked(reg: word, value: byte, mask: byte) =
  let
    cached = oplCachedRegs[reg] and (not(mask))
  oplWriteReg(reg, cached or (value and mask))

var
  # Reference: https://www.fit.vutbr.cz/~arnost/opl/opl3.html#appendixA
  # static u8 adlib_op[] = {0, 1, 2, 8, 9, 10, 16, 17, 18};
  sbproOp: array[18, byte] = [0'u8, 1, 2, 6, 7, 8, 12, 13, 14, 18, 19, 20, 24,
      25, 26, 30, 31, 32]

  regPairOffsets: array[36, word] = [0x000'u16, 0x001, 0x002, 0x003, 0x004,
      0x005, 0x008, 0x009, 0x00A, 0x00B, 0x00C, 0x00D, 0x010, 0x011, 0x012,
      0x013, 0x014, 0x015, 0x100, 0x101, 0x102, 0x103, 0x104, 0x105, 0x108,
      0x109, 0x10A, 0x10B, 0x10C, 0x10D, 0x110, 0x111, 0x112, 0x113, 0x114, 0x115]

  regSingleOffsets: array[18, word] = [0'u16, 1, 2, 3, 4, 5, 6, 7, 8, 0x100,
      0x101, 0x102, 0x103, 0x104, 0x105, 0x106, 0x107, 0x108]

proc oplRegPairOffset(voice: byte, op: byte): word =
  # result is reg_offset
  result = regPairOffsets[sbproOp[voice]]
  if op == 1:
    result += 3

proc oplWriteInstrument(instrument: ptr InstrumentType, voice: byte) =
  # OPL3: L+R speaker enable
  oplWriteReg(word(0xC0'u16 + regSingleOffsets[voice]), byte(
      instrument.FBConn or 0x30))
  for operatorIndex in 0'u8..<2'u8:
    let
      operator: OperatorType = instrument.operators[operatorIndex]
      opReg: word = oplRegPairOffset(voice, operatorIndex)
    oplWriteReg(0x20'u16 + opReg, operator.mul)
    oplWriteReg(0x40'u16 + opReg, operator.kslTl)
    oplWriteReg(0x60'u16 + opReg, operator.aD)
    oplWriteReg(0x80'u16 + opReg, operator.sR)
    oplWriteReg(0xE0'u16 + opReg, operator.waveform)

proc midiNoteOff(event: var MidiEventType) =
  var
    note: byte = event.metu.channel.param1
    channel: byte = event.metu.channel.channel
  for voice in 0..<NUM_OPL_VOICES:
    if (voiceChannel[voice] == int32(channel)) and (voiceNote[voice] == note):
      # release key
      oplWriteRegMasked(0xB0'u16 + regSingleOffsets[voice], 0, 0x20)
      # This voice is now free to be re-used
      voiceNote[voice] = 0
      break

proc getInstrument(id: int32): ptr InstrumentType =
  if id < numInstruments and id >= 0:
    return addr(cast[ptr UncheckedArray[InstrumentType]](instruments)[id])
  else:
    return addr(cast[ptr UncheckedArray[InstrumentType]](instruments)[0])

proc midiNoteOn(event: var MidiEventType) =
  var
    note: byte = event.metu.channel.param1
    velocity: byte = event.metu.channel.param2
    channel: byte = event.metu.channel.channel
    instrumentId: int32 = channelInstrument[channel]
    instrument: ptr InstrumentType = getInstrument(instrumentId)

  if velocity == 0:
    midiNoteOff(event)
  else:
    # Find a free OPL voice
    var
      voice: int32 = -1
      testVoice: int32 = lastUsedVoice
    for i in 0..<NUM_OPL_VOICES:
      # Don't use the same voice immediately again: that note is probably still be in the release phase.
      inc(testVoice)
      testVoice = testVoice mod NUM_OPL_VOICES
      if voiceNote[testVoice] == 0:
        voice = testVoice
        break

    lastUsedVoice = voice
    if voice >= 0:
      # Set the correct instrument for this voice.
      if voiceInstrument[voice] != instrumentId:
        oplWriteInstrument(instrument, byte(voice))
        voiceInstrument[voice] = instrumentId

      voiceNote[voice] = note
      voiceChannel[voice] = int32(channel)

      # Calculate frequency for a MIDI note: note number 69 = A4 = 440 Hz.
      # However, Prince of Persia treats notes as one octave (12 semitones) lower than that, by default.
      # A special MIDI SysEx event is used to change the frequency of all notes.
      let
        octavesFromA4: float = float(int(event.metu.channel.param1) - 69 - 12 +
            midiSemitonesHigher) / float(12.0)
        frequency: float = pow(float(2.0), octavesFromA4) * float(440.0)
        fNumberFloat: float = frequency * float(1 shl 20) / float(49716.0)
        fBlock: int32 = int32(log2(fNumberFloat) - 9) and 7
        f: int32 = (int32(fNumberFloat) shr fBlock) and 1023
        regOffset: word = regSingleOffsets[voice]

      oplWriteReg(0xA0'u16 + regOffset, byte(f and 0xFF))
      oplWriteReg(0xB0'u16 + regOffset, byte(0x20 or (fBlock shl 2) or (f shr 8)))

      # The modulator always uses its own base volume level.
      oplWriteRegMasked(0x40'u16 + oplRegPairOffset(byte(voice), 0'u8),
          instrument.operators[0].kslTl, 0x3F)

      # The carrier volume level is calculated as a combination of its base
      # volume and the MIDI note velocity.
      # PRINCE.EXE disassembly: seg009:6C3C
      let
        instrVolume: int32 = int32(instrument.operators[1].kslTl and 0x3F)
      var
        carrierVolume: int32 = ((instrVolume + 64) * 225) div (int32(velocity) + 161)
      if carrierVolume < 64:
        carrierVolume = 64
      if carrierVolume > 127:
        carrierVolume = 127
      carrierVolume -= 64
      oplWriteRegMasked(0x40'u16 + oplRegPairOffset(byte(voice), 1'u8),
          byte(carrierVolume), 0x3F)
    else:
      echo "skipping note, not enough OPL voices"

proc processMidiEvent(event: var MidiEventType) =
  case event.eventType
  of 0x80: # note off
    midiNoteOff(event)
  of 0x90: # note on
    midiNoteOn(event)
  of 0xC0: # program change
    channelInstrument[event.metu.channel.channel] = int32(
        event.metu.channel.param1)
  of 0xF0: # SysEx event
    if event.metu.sysex.length == 7:
      let
        sysexData: ptr UncheckedArray[byte] = cast[ptr UncheckedArray[byte]](
            event.metu.sysex.data)
      if sysexData[2] == 0x34 and (sysexData[3] == 0 or sysexData[3] == 1) and
          sysexData[4] == 0:
        midiSemitonesHigher = int8(sysexData[5])
  of 0xFF: # Meta event
    case event.metu.meta.`type`
    of 0x51: # set tempo
      let
        metaData: ptr UncheckedArray[byte] = cast[ptr UncheckedArray[byte]](
            event.metu.meta.data)
      var
        newTempo: int32 = int32(metaData[0]) shl 16 or int32(metaData[
            1]) shl 8 or int32(metaData[2])
      newTempo = int32(float(newTempo) * (1.0 + currentMidiTempoModifier))
      usPerBeat = uint32(newTempo)
    of 0x54: # SMTPE offset
      discard
    of 0x58: # time signature
      discard
    of 0x2F: # end of track
      discard
    else:
      discard
  else:
    discard

proc midiCallback(userdata: pointer, stream: ptr uint8, length: int32) =
  if (midiPlaying == 0) or (length <= 0):
    return
  var
    framesNeeded = length div 4
    streamInternal: ptr uint8 = stream
  while framesNeeded > 0:
    if ticksToNextPause > 0:
      # Fill the audio buffer (we have already processed the MIDI events up till
      # this point)
      var
        usToNextPause: int64 = int64(ticksToNextPause) * int64(
            usPerBeat) div int64(ticksPerBeat)
        usNeeded: int64 = framesNeeded * ONE_SECOND_IN_US div mixingFreq
        advanceUs: int64 = min(usToNextPause, usNeeded)
        availableFrames: int32 = int32( ((advanceUs * mixingFreq) +
            ONE_SECOND_IN_US - 1) div ONE_SECOND_IN_US)
        advanceFrames: int32 = min(availableFrames, framesNeeded)
      advanceUs = advanceFrames * ONE_SECOND_IN_US div mixingFreq
      var
        tempBuffer: seq[int16] = newSeq[int16](advanceFrames * 4)
      opl3GenerateStream(oplChip, tempBuffer, uint32(advanceFrames))
      if (isSoundOn and enableMusic) != 0:
        for sample in 0..<(advanceFrames * 2):
          cast[ptr UncheckedArray[int16]](streamInternal)[sample] += tempBuffer[sample]

      framesNeeded = framesNeeded - advanceFrames
      streamInternal = addr(cast[ptr UncheckedArray[byte]](streamInternal)[
          advanceFrames * 4])
      # Advance the current MIDI tick position.
      # Keep track of the partial ticks that have elapsed so that we do not fall behind.
      var
        ticksElapsedFloat: float = float(advanceUs * int32(ticksPerBeat)) /
            float(usPerBeat)
        ticksElapsed: int64 = int64(ticksElapsedFloat)
      midiCurrentPosFractPart += (ticksElapsedFloat - float(ticksElapsed))
      if midiCurrentPosFractPart > 1.0:
        midiCurrentPosFractPart -= 1.0
        ticksElapsed += 1
      midiCurrentPos += ticksElapsed
      ticksToNextPause -= int32(ticksElapsed)
    else:
      # Need to process MIDI events on one or more tracks.
      var
        numFinishedTracks = 0
      for trackIndex in 0..<numMidiTracks:
        var
          track: ptr MidiTrackType = addr(cast[ptr UncheckedArray[
              MidiTrackType]](midiTracks)[trackIndex])

        while midiCurrentPos >= track.nextPauseTick:
          var
            eventsLeft: int32 = track.numEvents - track.eventIndex
          if eventsLeft > 0:
            var
              event: ptr MidiEventType = addr(cast[ptr UncheckedArray[
                  MidiEventType]](track.events)[track.eventIndex])
            inc(track.eventIndex)
            processMidiEvent(event[])

            # Need to look ahead: must delay processing of the next event, if there is a pause.
            if eventsLeft > 1:
              var
                nextEvent: ptr MidiEventType = addr(cast[ptr UncheckedArray[
                    MidiEventType]](track.events)[track.eventIndex])
              if nextEvent.deltaTime != 0:
                track.nextPauseTick += int32(nextEvent.deltaTime)

          else:
            # reached the last event in this track.
            inc(numFinishedTracks)
            break

      if numFinishedTracks >= numMidiTracks:
        # All tracks have finished. Fill the remaining samples with silence and stop playback.
        zeroMem(streaminternal, framesNeeded * 4)
        lockAudio()
        midiPlaying = 0
        unlockAudio()
        return
      else:
        # Need to delay (let the OPL chip do its work) until one of the tracks
        # needs to process a MIDI event again.
        var
          firstNextPauseTick: int64 = high(int64)
        for i in 0..<numMidiTracks:
          let
            track: ptr MidiTrackType = addr(cast[ptr UncheckedArray[
                MidiTrackType]](midiTracks)[i])
          if track.eventIndex >= track.numEvents or midiCurrentPos >=
              track.nextPauseTick:
            continue
          firstNextPauseTick = min(firstNextPauseTick, track.nextPauseTick)
        if firstNextPauseTick == high(int64):
          echo "MIDI: Couldn't figure out how long to delay (this is a bug)"
          quitPoP(1)
        ticksToNextPause = int32(firstNextPauseTick - midiCurrentPos)
        if ticksToNextPause < 0:
          # This should never happen?
          echo "Tried to delay a negative amount of time (this is a bug)"
          quitPoP(1)

proc stopMidi() =
  if (midiPlaying == 0):
    return
  lockAudio()
  midiPlaying = 0
  unlockAudio()

proc initMidi() =
  if initializedMidi:
    return
  initializedMidi = true

  # unused if instruments can be loaded normally
  instruments = addr(hardcodedInstrument)
  var
    size: int32
    dathandle: DatType = openDat("PRINCE.DAT", 0)
  instrumentsData = loadFromOpendatsAlloc(1, "bin", nil, addr(size))
  if isNil(instrumentsData):
    echo "Missing MIDI instruments data (resource 1)"
  else:
    numInstruments = int32((cast[ptr byte](instrumentsData))[])
    if size == 1 + numInstruments * sizeof(InstrumentType):
      # TODO: find suitable solution for instruments
      # instruments = InstrumentType(cast[ ptr byte ]instrumentsData + 1)
      instruments = cast[ptr InstrumentType](addr(cast[ptr UncheckedArray[
          byte]](instrumentsData)[1]))
    else:
      echo "MIDI instruments data (resource 1) is not the expected size"
      numInstruments = 1
  if not(isNil(dathandle)):
    closeDat(dathandle)

proc playMidiSound(buffer: ptr SoundBufferType) =
  stopMidi()
  if isNil(buffer):
    return
  initDigi()
  if digiUnavailable != 0:
    return
  initMidi()

  if not(parseMidi(cast[ptr MidiRawChunkType](addr(buffer.bu.midi))[], parsedMidi)):
    echo "Error reading MIDI music"
    return

  # Initialize the OPL chip.
  oplReset(digiAudiospec.freq)
  # OPL3 enable
  oplWriteReg(0x105, 0x01)
  for voice in 0..<NUM_OPL_VOICES:
    oplWriteInstrument(instruments, byte(voice))
    voiceInstrument[voice] = 0
    voiceNote[voice] = 0

  for channel in 0'i32..<MAX_MIDI_CHANNELS:
    channelInstrument[channel] = channel

  midiCurrentPos = 0
  midiCurrentPosFractPart = 0
  ticksToNextPause = 0
  midiTracks = parsedMidi.tracks
  numMidiTracks = parsedMidi.numTracks
  midiSemitonesHigher = 0
  # default tempo (500_000 us/beat == 120 bpm)
  usPerBeat = 500_000
  currentMidiTempoModifier = midiTempoModifiers[currentSound]
  ticksPerBeat = parsedMidi.ticksPerBeat
  mixingFreq = digiAudiospec.freq
  midiPlaying = 1
  pauseAudio(0)
