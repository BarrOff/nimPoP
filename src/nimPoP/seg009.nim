import os, strutils, times

# Most functions in this file are different from those in the original game.

proc sdlperror(header: string) =
  let
    error: cstring = sdl.getError()
  echo(header & ": " & $(error))
  #quit(1)

var
  exeDir: string = "."
  foundExeDir: bool = false

proc findExeDir() =
  if (foundExeDir):
    return
  snprintfCheck(exeDir, POP_MAX_PATH, getAppFilename())
  var
    lastSlash: int32 = -1
    pos: string = exeDir
  # for (char c = *pos; c != '\0'; c = *(++pos)) {
  for index, value in pos:
    if (value == '/' or value == '\\'):
      lastSlash = index
  if (lastSlash != -1):
    exeDir = exeDir[0..lastSlash - 1]
  else:
    exeDir = "./"
  foundExeDir = true

# Not necessary, function is supplied by os.fileExists
#bool fileExists(const char* filename) {
#    return (access(filename, F_OK) != -1)


proc locateFile(filename: string): string =
  if(fileExists(filename)):
    return filename
  else:
    # If failed, it may be that SDLPoP is being run from the wrong different working directory.
    # We can try to rescue the situation by loading from the directory of the executable.
    findExeDir()
    return exeDir & "/" & filename


# the following are not necessary in nim
##ifdef _WIN32
## These macros are from the SDL2 source. (src/core/windows/SDLWindows.h)
## The pointers returned by these macros must be freed with SDLFree().
##define WIN_StringToUTF8(S) SDLIconvString("UTF-8", "UTF-16LE", (char *)(S), (SDLWcslen(S)+1)*sizeof(WCHAR))
##define WIN_UTF8ToString(S) (WCHAR *)SDLIconvString("UTF-16LE", "UTF-8", (char *)(S), SDLStrlen(S)+1)

## This hack is needed because SDL uses UTF-8 everywhere (even in argv!), but fopen on Windows uses whatever code page is currently set.
#FILE* fopen_UTF8(const char* filename_UTF8, const char* mode_UTF8) {
#  WCHAR* filename_UTF16 = WIN_UTF8ToString(filename_UTF8)
#  WCHAR* mode_UTF16 = WIN_UTF8ToString(mode_UTF8)
#  FILE* result = Wfopen(filename_UTF16, mode_UTF16)
#  SDLFree(mode_UTF16)
#  SDLFree(filename_UTF16)
#  return result


#int chdir_UTF8(const char* path_UTF8) {
#  WCHAR* path_UTF16 = WIN_UTF8ToString(path_UTF8)
#  int result = Wchdir(path_UTF16)
#  SDLFree(path_UTF16)
#  return result


#int access_UTF8(const char* filename_UTF8, int mode) {
#  WCHAR* filename_UTF16 = WIN_UTF8ToString(filename_UTF8)
#  int result = Waccess(filename_UTF16, mode)
#  SDLFree(filename_UTF16)
#  return result

##endif #_WIN32

## OS abstraction for listing directory contents
## Directory listing using dirent.h is available using MinGW on Windows, but not using MSVC (need to use Win32 API).
## - Under GNU/Linux, etc (or if compiling with MinGW on Windows), we can use dirent.h
## - Under Windows, we'd like to directly call the Win32 API. (Note: MSVC does not include dirent.h)
## NOTE: If we are using MinGW, we'll opt to use the Win32 API as well: dirent.h would just wrap Win32 anyway!

##ifdef _WIN32
#struct directoryListingType {
#  WIN32_FIND_DATAW findData
#  HANDLE searchHandle
#  char* currentFilename_UTF8


#directoryListingType* createDirectoryListingAndFindFirstFile(const char* directory, const char* extension) {
#  directoryListingType* directoryListing = calloc(1, sizeof(directoryListingType))
#  char searchPattern[POP_MAX_PATH]
#  snprintfCheck(searchPattern, POP_MAX_PATH, "mods/*.mods", directory, extension)
#  WCHAR* searchPattern_UTF16 = WIN_UTF8ToString(searchPattern)
#  directoryListing.searchHandle = FindFirstFileW( searchPattern_UTF16, &directoryListing.findData )
#  SDLFree(searchPattern_UTF16)

#  if (directoryListing.searchHandle != INVALID_HANDLE_VALUE):
#    return directoryListing
# else {
#    free(directoryListing)
#    return NULL



#char* getCurrentFilenameFromDirectoryListing(directoryListingType* data) {

#  SDLFree(data.currentFilename_UTF8)
#  data.currentFilename_UTF8 = NULL
#  data.currentFilename_UTF8 = WIN_StringToUTF8(data.findData.cFileName)
#  return data.currentFilename_UTF8



#bool findNextFile(directoryListingType* data) {
#  return (bool) FindNextFileW( data.searchHandle, &data.findData )


#proc closeDirectoryListing(directoryListingType* data) =
#  FindClose(data.searchHandle)
#  SDLFree(data.currentFilename_UTF8)
#  data.currentFilename_UTF8 = NULL
#  free(data)



##else # use dirent.h API for listing files

#struct directoryListingType {
#  DIR* dp
#  char* foundFilename
#  const char* extension


#directoryListingType* createDirectoryListingAndFindFirstFile(const char* directory, const char* extension) {

#  directoryListingType* data = calloc(1, sizeof(directoryListingType))
#  bool ok = false
#  data.dp = opendir(directory)
#  if (data.dp != NULL):
#    struct dirent* ep
#    while ((ep = readdir(data.dp))) {
#      char *ext = strrchr(ep.dName, '.')
#      if (ext != NULL and strcasecmp(ext+1, extension) == 0):
#        data.foundFilename = ep.dName
#        data.extension = extension
#        ok = true
#        break



#  if (ok):
#    return data

# else {
#    free(data)
#    return NULL



#char* getCurrentFilenameFromDirectoryListing(directoryListingType* data) {
#  return data.foundFilename


#bool findNextFile(directoryListingType* data) {
#  bool ok = false
#  struct dirent* ep

#  while ((ep = readdir(data.dp))) {
#    char *ext = strrchr(ep.dName, '.')
#    if (ext != NULL and strcasecmp(ext+1, data.extension) == 0):
#      data.foundFilename = ep.dName
#      ok = true
#      break


#  return ok


#proc closeDirectoryListing(directoryListingType *data) =
#  closedir(data.dp)
#  free(data)


##endif #_WIN32

var
  datChainPtr: DatType = nil
  lastTextInput: char

# seg009:000D
proc readKey(): int32 =
  # stub
  var
    key: int32 = int32(lastKeyScancode)
  lastKeyScancode = 0
  return key


# seg009:019A
proc clearKbdBuf() =
  # stub
  lastKeyScancode = 0
  # TODO: DANGER!!! might need to use another 0
  lastTextInput = char(0)


# seg009:040A
proc prandom(`max`: word): word =
  if seedWasInit == 0:
    # init from current time
    randomSeed = dword(toUnix(getTime()))
    seedWasInit = 1

  randomSeed = randomSeed * 214013 + 2531011
  return word((randomSeed shr 16) mod dword(`max` + 1))


# seg009:0467
proc roundXposToByte(xpos, roundDirection: int32): int32 =
  # stub
  return xpos


# seg009:0C7A
proc quitPoP(exitCode: int32) =
  restoreStuff()
  system.quit(exitCode)


# seg009:0C90
proc restoreStuff() =
  sdl.quit()


# seg009:0E33
proc keyTestQuit(): int32 =
  var
    key: word
  key = word(readKey())
  if (key == (ord(SCANCODE_Q) or ord(WITH_CTRL))): # ctrl-q
    when (USE_REPLAY):
      if (recording) != 0:
        discard saveRecordedReplayDialog()
    when (USE_MENU):
      if (isMenuShown) != 0:
        menuWasClosed()
    quitPoP(0)

  return int32(key)


# not needed in Nim, we have the parseopt std library
# # seg009:0E54
# proc checkParam(param: string): string
#   # stub
#   argIndex: int16
#   for argIndex in 1..< gArgc:

#     char* currArg = gArgv[argIndex]

#     # Filenames (e.g. replays) should never be a valid 'normal' param so we should skip these to prevent conflicts.
#     # We can lazily distinguish filenames from non-filenames by checking whether they have a dot in them.
#     # (Assumption: all relevant files, e.g. replay files, have some file extension anyway)
#     if (strchr(currArg, '.') != NULL):
#       continue


#     # List of params that expect a specifier ('sub-') arg directly after it (e.g. the mod's name, after "mod" arg)
#     # Such sub-args may conflict with the normal params (so, we should 'skip over' them)
#     static const char paramsWithOneSubparam[][16] = { "mod", "validate", /*...*/ }

#     bool currArgHasOneSubparam = false
#     int i
#     for (i = 0; i < COUNT(paramsWithOneSubparam); ++i) {
#       if (strncasecmp(currArg, paramsWithOneSubparam[i], strlen(paramsWithOneSubparam[i])) == 0):
#         currArgHasOneSubparam = true
#         break



#     if (currArgHasOneSubparam):
#       # Found an arg that has one sub-param, so we want to:
#       # 1: skip over the next arg                (if we are NOT checking for this specific param)
#       # 2: return a pointer below to the SUB-arg (if we ARE checking for this specific param)
#       ++argIndex
#       if (!(argIndex < gArgc)) return NULL # not enough arguments


#     if (/*strnicmp*/strncasecmp(currArg, param, strlen(param)) == 0):
#       return gArgv[argIndex]


#   return NULL


# seg009:0EDF
proc popWait(timerIndex, time: int32): int32 =
  startTimer(timerIndex, time)
  return doWait(timerIndex)


proc openDatFromRootOrDataDir(filename: string): File =
  var
    fp: File = nil
  discard open(fp, filename, fmRead)

  # if failed, try if the DAT file can be opened in the data/ directory, instead of the main folder
  if (fp == nil):
    var
      dataPath: string
    snprintfCheck(dataPath, POP_MAX_PATH, "data/" & filename)
    if not(fileExists(dataPath)):
      findExeDir()
      snprintfCheck(dataPath, POP_MAX_PATH, exeDir & "/data/" & filename)
    # verify that this is a regular file and not a directory (otherwise, don't open)
    if fileExists(dataPath):
      var
        pathStat: FileInfo = getFileInfo(dataPath)
      if (pathStat.kind == pcFile):
        discard open(fp, dataPath, fmRead)

  return fp


# TODO: DANGER!!! reading the data has variable size
# seg009:0F58
proc openDat(filename: string, drive: int32): DatType =
  var
    fp: File = nil
  if useCustomLevelset == 0:
    fp = openDatFromRootOrDataDir(filename)
  else:
    if not(skipModDataFiles):
      var
        filenameMod: string
      # before checking the root directory, first try mods/MODNAME/
      snprintfCheck(filenameMod, POP_MAX_PATH, modDataPath & "/" & filename)
      discard open(fp, filenameMod, fmRead)
    if (fp == nil and not(skipNormalDataFiles)):
      fp = openDatFromRootOrDataDir(filename)
  var
    datHeader: DatHeaderType
    datTable: ptr DatTableType

    datPointer: DatType = new(DatType)
  snprintfCheck(datPointer.filename, uint(filename.len + 1), filename)
  datPointer.nextDat = datChainPtr
  datChainPtr = datPointer

  if (fp != nil):
    if (readBuffer(fp, addr(datHeader), 6) != 6):
      perror(filename)
      if (fp != nil):
        close(fp)
      if datTable != nil:
        dealloc(datTable)
      return datPointer
    datTable = cast[ptr DatTableType](alloc(datHeader.tableSize))
    if datTable == nil:
      perror(filename)
      if (fp != nil):
        close(fp)
      if datTable != nil:
        dealloc(datTable)
      return datPointer
    setFilePos(fp, int64(datHeader.tableOffset))
    if fread(datTable, csize_t(datHeader.tableSize), 1, fp) != 1:
      perror(filename)
      if (fp != nil):
        close(fp)
      if datTable != nil:
        dealloc(datTable)
      return datPointer
    datPointer.handle = fp
    datPointer.datTable = datTable

  # stub
  return datPointer


# seg009:9CAC
proc setLoadedPalette(palettePtr: var DatPalType) =
  var
    destRow, destIndex, sourceRow: int32
  destRow = 0
  destIndex = 0
  sourceRow = 0
  while destRow < 16:
    if (int(palettePtr.rowBits) and (1 shl destRow)) != 0:
      setPalArr(destIndex, 16, addr(palettePtr.vga[sourceRow*0x10]), 1)
      inc(sourceRow)
    inc(destRow)
    destIndex += 0x10


var
  # data:3356
  chtabPaletteBits: word = 1

# seg009:104E
proc loadSpritesFromFile(resource, paletteBits, quitOnError: int32): ChtabType =
  var
    nImages: int32 = 0
  #int hasPaletteBits = 1
    chtab: ChtabType = nil
    shpl: ptr DatShplType = cast[ptr DatShplType](loadFromOpendatsAlloc(
        resource, "pal", nil, nil))
  if (shpl == nil):
    echo("Can't load sprites from resource ", $(resource), ".")
    #if (quitOnError) quitPoP(1)
    return nil

  var
    palPtr: ptr DatPalType = addr(shpl.palette)
  if (graphicsMode == ord(gmMcgaVga)):
    if (paletteBits == 0):
      #[
      paletteBits = addPaletteBits(palPtr.nColors)
      if (paletteBits == 0):
        quitPoP(1)
      ]#
      discard
    else:
      chtabPaletteBits = chtabPaletteBits or word(paletteBits)
      #hasPaletteBits = 0
    palPtr.rowBits = word(paletteBits)

  nImages = int32(shpl.nImages)
  let
    allocSize: csize_t = csize_t(sizeof(ChtabTypeObject) + nImages * sizeof(ImageType))
  # chtab = new(ChtabType)
  chtab = cast[ChtabType](alloc(allocSize))
  memset(addr(chtab[]), 0, allocSize)
  chtab.nImages = word(nImages)
  for i in 1..nImages:
    var
      image: Surface = loadImage(resource + i, palPtr[])
    # if (image == NULL) printf(" failed")
    if (image != nil):
      if (setSurfaceAlphaMod(image, 0) != 0):
        sdlperror("SDL_SetAlpha")
        quitPoP(1)
      #[
      if (SDL_SetColorKey(image, SDL_SRCCOLORKEY, 0) != 0):
        sdlperror("SDL_SetColorKey")
        quitPoP(1)

      ]#
    # printf("\n")
    chtab.images[i-1] = image
  setLoadedPalette(palPtr[])
  return chtab


# seg009:11A8
proc freeChtab(chtabPtr: ChtabType) =
  var
    currImage: ImageType
    nImages: word
  if (graphicsMode == ord(gmMcgaVga) and (chtabPtr.hasPaletteBits != 0)):
    chtabPaletteBits = chtabPaletteBits and not(chtabPtr.chtabPaletteBits)
  nImages = chtabPtr.nImages
  for id in 0 ..< int32(nImages):
    currImage = chtabPtr.images[id]
    if (currImage != nil):
      freeSurface(currImage)
  # not needed, because ChtabType is a ref object -> managed by gc
  # freeNear(chtabPtr)

{.emit: """
uint8_t* changeBytePointer(uint8_t* pntr, int x) {
  return pntr + x;
  }
""".}

proc changeBytePointer(pntr: ptr byte, x: int32): ptr byte {.importc, nodecl.}

# seg009:8CE6
proc decompressRleLr(destination: var ptr byte, source: var UncheckedArray[
    byte], destLength: int32) =
  var
    srcPos: ptr byte = addr(source[0])
    destPos: ptr byte = destination
    remLength: int16 = int16(destLength)
    destIndex: int32 = 0
    srcIndex: int32 = 0
  while remLength != 0:
    var
      count: sbyte = sbyte(srcPos[])
    srcPos = changeBytePointer(srcPos, 1)
    if (count >= 0): # copy
      inc(count)
      destPos[] = srcPos[]
      destPos = changeBytePointer(destPos, 1)
      srcPos = changeBytePointer(srcPos, 1)
      dec(remLength)
      dec(count)
      while (int16(count) and remLength) != 0:
        destPos[] = srcPos[]
        destPos = changeBytePointer(destPos, 1)
        srcPos = changeBytePointer(srcPos, 1)
        dec(remLength)
        dec(count)
    else:
      var
        al: byte = srcPos[]
      srcPos = changeBytePointer(srcPos, 1)
      count = -count
      destPos[] = al
      destPos = changeBytePointer(destPos, 1)
      dec(remLength)
      dec(count)
      while (int16(count) and remLength) != 0:
        count = -count
        destPos[] = al
        destPos = changeBytePointer(destPos, 1)
        dec(remLength)
        dec(count)


# seg009:8D1C
proc decompressRleUd(destination: var ptr byte, source: var UncheckedArray[
    byte], destLength, width, height: int32) =
  var
    remHeight: int16 = int16(height)
    srcPos: ptr byte = addr(source[0])
    destPos: ptr byte = destination
    remLength: int16 = int16(destLength)
    remWidth: int32 = width
  dec(remLength)
  dec(remWidth)
  while remLength != 0:
    var
      count: sbyte = cast[sbyte](srcPos[])
    srcPos = changeBytePointer(srcPos, 1)
    if (count >= 0): # copy
      inc(count)
      destPos[] = srcPos[]
      srcPos = changeBytePointer(srcPos, 1)
      destPos = changeBytePointer(destPos, 1 + remWidth)
      dec(remHeight)
      if remHeight == 0:
        destPos = changeBytePointer(destPos, remLength)
        remLength = int16(height)
      dec(remLength)
      dec(count)
      while (int(count) and remLength) != 0:
        destPos[] = srcPos[]
        srcPos = changeBytePointer(srcPos, 1)
        destPos = changeBytePointer(destPos, 1 + remWidth)
        dec(remHeight)
        if remHeight == 0:
          destPos = changeBytePointer(destPos, remLength)
          remLength = int16(height)
        dec(remLength)
        dec(count)
    else:
      var
        al: byte = srcPos[]
      count = if count > -128: -count else: -128
      destPos[] = al
      destPos = changeBytePointer(destPos, 1 + remWidth)
      dec(remHeight)
      if remHeight == 0:
        destPos = changeBytePointer(destPos, - remLength)
        remHeight = int16(height)
      dec(remLength)
      if count > -128:
        dec(count)
      else:
        count = 127
      # dec(count)
      while (int16(count) and remLength) != 0:
        destPos[] = al
        destPos = changeBytePointer(destPos, 1 + remWidth)
        dec(remHeight)
        if remHeight == 0:
          destPos = changeBytePointer(destPos, - remLength)
          remHeight = int16(height)
        dec(remLength)
        dec(count)


# seg009:90FA
proc decompressLzgLr(dest: var ptr byte, source: var UncheckedArray[byte],
    destLength: int32): ptr byte =
  var
    window: ptr byte = cast[ptr byte](alloc0(0x400))
  if window == nil:
    return nil
  var
    windowPos: ptr byte = changeBytePointer(window, 0x400 - 0x42)
    # bx
    remaining: int16 = int16(destLength)
    # cx
    windowEnd: ptr byte = changeBytePointer(window, 0x400)                 # dx
    sourcePos: ptr byte = addr(source[0])
    destPos: ptr byte = dest
    mask: word = 0
  mask = mask shr 1
  if (mask and 0xFF00) == 0:
    mask = word(sourcePos[]) or 0xFF00
    sourcePos = changeBytePointer(sourcePos, 1)
  if (mask and 1) != 0:
    windowPos[] = sourcePos[]
    destPos[] = sourcePos[]
    windowPos = changeBytePointer(windowPos, 1)
    destPos = changeBytePointer(destPos, 1)
    sourcePos = changeBytePointer(sourcePos, 1)
    if windowPos >= windowEnd:
      windowPos = window
    dec(remaining)
  else:
    var
      copyInfo: word = word(sourcePos[])
    sourcePos = changeBytePointer(sourcePos, 1)
    copyInfo = (copyInfo shl 8) or word(sourcePos[])
    sourcePos = changeBytePointer(sourcePos, 1)
    var
      copySource: ptr byte = changeBytePointer(window, int32(copyInfo and 0x3FF))
      copyLength: byte = byte( (copyInfo shr 10) + 3)
    windowPos[] = copySource[]
    destPos[] = copySource[]
    windowPos = changeBytePointer(windowPos, 1)
    destPos = changeBytePointer(destPos, 1)
    copySource = changeBytePointer(copySource, 1)
    if copySource >= windowEnd:
      copySource = window
    if windowPos >= windowEnd:
      windowPos = window
    dec(remaining)
    dec(copyLength)
    while (remaining and copyLength) != 0:
      windowPos[] = copySource[]
      destPos[] = copySource[]
      windowPos = changeBytePointer(windowPos, 1)
      destPos = changeBytePointer(destPos, 1)
      copySource = changeBytePointer(copySource, 1)
      if copySource >= windowEnd:
        copySource = window
      if windowPos >= windowEnd:
        windowPos = window
      dec(remaining)
      dec(copyLength)

  while remaining != 0:
    mask = mask shr 1
    if (mask and 0xFF00) == 0:
      mask = word(sourcePos[]) or 0xFF00
      sourcePos = changeBytePointer(sourcePos, 1)
    if (mask and 1) != 0:
      windowPos[] = sourcePos[]
      destPos[] = sourcePos[]
      windowPos = changeBytePointer(windowPos, 1)
      destPos = changeBytePointer(destPos, 1)
      sourcePos = changeBytePointer(sourcePos, 1)
      if windowPos >= windowEnd:
        windowPos = window
      dec(remaining)
    else:
      var
        copyInfo: word = word(sourcePos[])
      sourcePos = changeBytePointer(sourcePos, 1)
      copyInfo = (copyInfo shl 8) or word(sourcePos[])
      sourcePos = changeBytePointer(sourcePos, 1)
      var
        copySource: ptr byte = changeBytePointer(window, int32(copyInfo and 0x3FF))
        copyLength: byte = byte( (copyInfo shr 10) + 3)
      windowPos[] = copySource[]
      destPos[] = copySource[]
      windowPos = changeBytePointer(windowPos, 1)
      destPos = changeBytePointer(destPos, 1)
      copySource = changeBytePointer(copySource, 1)
      if copySource >= windowEnd:
        copySource = window
      if windowPos >= windowEnd:
        windowPos = window
      dec(remaining)
      dec(copyLength)
      while (remaining and copyLength) != 0:
        windowPos[] = copySource[]
        destPos[] = copySource[]
        windowPos = changeBytePointer(windowPos, 1)
        destPos = changeBytePointer(destPos, 1)
        copySource = changeBytePointer(copySource, 1)
        if copySource >= windowEnd:
          copySource = window
        if windowPos >= windowEnd:
          windowPos = window
        dec(remaining)
        dec(copyLength)

  #  end:
  dealloc(window)
  return dest


# seg009:91AD
proc decompressLzgUd(dest: var ptr byte, source: var UncheckedArray[byte],
    destLength, stride, height: int32): ptr byte =
  var
    # mallocNear(0x400)
    window: ptr byte = cast[ptr byte](alloc0(0x400))
  if window == nil:
    return nil
  var
    # bx
    windowPos: ptr byte = changeBytePointer(window, 0x400 - 0x42)
    # addr(cast[ptr UncheckedArray[byte]]( window) [ 0x400 - 0x42 ] )
    # cx
    remaining: int16 = int16(height)
    # dx
    windowEnd: ptr byte = changeBytePointer(window, 0x400)
    # addr(cast[ptr UncheckedArray[byte]]( window)[ 0x400 ] )
    sourcePos: ptr byte = addr(source[0])
    destPos: ptr byte = dest
    mask: word = 0
    var6: int16 = int16(destLength - 1)
    remDestLength: int32 = destLength
  mask = mask shr 1
  if (mask and 0xFF00) == 0:
    mask = word(sourcePos[]) or 0xFF00
    sourcePos = changeBytePointer(sourcePos, 1)
    # addr(cast[ptr UncheckedArray[byte]](sourcePos)[1])
  if (mask and 1) != 0:
    window[] = sourcePos[]
    dest[] = sourcePos[]
    window = changeBytePointer(window, 1)
    # addr(cast[ptr UncheckedArray[byte]](window)[1])
    sourcePos = changeBytePointer(sourcePos, 1)
    # addr(cast[ptr UncheckedArray[byte]](sourcePos)[1])
    destPos = changeBytePointer(destPos, stride)
    # addr(cast[ptr UncheckedArray[byte]](destPos)[stride])
    dec(remaining)
    if remaining == 0:
      destPos = changeBytePointer(destPos, - int32(var6))
      remaining = int16(height)
    if windowPos >= windowEnd:
      windowPos = window
    dec(remDestLength)
  else:
    var
      copyInfo: word = word(sourcePos[])
    sourcePos = changeBytePointer(sourcePos, 1)
    copyInfo = (copyInfo shl 8) or word(sourcePos[])
    sourcePos = changeBytePointer(sourcePos, 1)
    var
      copySource: ptr byte = changeBytePointer(window, int32(copyInfo and 0x3FF))
      copyLength: byte = byte( (copyInfo shr 10) + 3)
    windowPos[] = copySource[]
    destPos[] = copySource[]
    windowPos = changeBytePointer(windowPos, 1)
    copySource = changeBytePointer(copySource, 1)
    destPos = changeBytePointer(destPos, stride)
    dec(remaining)
    if remaining == 0:
      destPos = changeBytePointer(destPos, - int32(var6))
      remaining = int16(height)
    if copySource >= windowEnd:
      copySource = window
    if windowPos >= windowEnd:
      windowPos = window
    dec(remDestLength)
    dec(copyLength)
    while (remDestLength and copyLength) != 0:
      windowPos[] = copySource[]
      destPos[] = copySource[]
      windowPos = changeBytePointer(windowPos, 1)
      copySource = changeBytePointer(copySource, 1)
      destPos = changeBytePointer(destPos, stride)
      dec(remaining)
      if remaining == 0:
        destPos = changeBytePointer(destPos, - int32(var6))
        remaining = int16(height)
      if copySource >= windowEnd:
        copySource = window
      if windowPos >= windowEnd:
        windowPos = window
      dec(remDestLength)
      dec(copyLength)

  while remDestLength != 0:
    mask = mask shr 1
    if (mask and 0xFF00) == 0:
      mask = word(sourcePos[]) or 0xFF00
      sourcePos = changeBytePointer(sourcePos, 1)
      # addr(cast[ptr UncheckedArray[byte]](sourcePos)[1])
    if (mask and 1) != 0:
      window[] = sourcePos[]
      dest[] = sourcePos[]
      window = changeBytePointer(window, 1)
      # addr(cast[ptr UncheckedArray[byte]](window)[1])
      sourcePos = changeBytePointer(sourcePos, 1)
      # addr(cast[ptr UncheckedArray[byte]](sourcePos)[1])
      destPos = changeBytePointer(destPos, stride)
      # addr(cast[ptr UncheckedArray[byte]](destPos)[stride])
      dec(remaining)
      if remaining == 0:
        destPos = changeBytePointer(destPos, - int32(var6))
        remaining = int16(height)
      if windowPos >= windowEnd:
        windowPos = window
      dec(remDestLength)
    else:
      var
        copyInfo: word = word(sourcePos[])
      sourcePos = changeBytePointer(sourcePos, 1)
      copyInfo = (copyInfo shl 8) or word(sourcePos[])
      sourcePos = changeBytePointer(sourcePos, 1)
      var
        copySource: ptr byte = changeBytePointer(window, int32(copyInfo and 0x3FF))
        copyLength: byte = byte( (copyInfo shr 10) + 3)
      windowPos[] = copySource[]
      destPos[] = copySource[]
      windowPos = changeBytePointer(windowPos, 1)
      copySource = changeBytePointer(copySource, 1)
      destPos = changeBytePointer(destPos, stride)
      dec(remaining)
      if remaining == 0:
        destPos = changeBytePointer(destPos, - int32(var6))
        remaining = int16(height)
      if copySource >= windowEnd:
        copySource = window
      if windowPos >= windowEnd:
        windowPos = window
      dec(remDestLength)
      dec(copyLength)
      while (remDestLength and copyLength) != 0:
        windowPos[] = copySource[]
        destPos[] = copySource[]
        windowPos = changeBytePointer(windowPos, 1)
        copySource = changeBytePointer(copySource, 1)
        destPos = changeBytePointer(destPos, stride)
        dec(remaining)
        if remaining == 0:
          destPos = changeBytePointer(destPos, - int32(var6))
          remaining = int16(height)
        if copySource >= windowEnd:
          copySource = window
        if windowPos >= windowEnd:
          windowPos = window
        dec(remDestLength)
        dec(copyLength)

  # end:
  dealloc(window)
  return dest


# seg009:938E
proc decomprImg(dest: var ptr byte, source: var ImageDataType, decompSize,
    cmeth, stride: int32) =
  case (cmeth)
  of 0: # RAW left-to-right
    memcpy(dest, addr(source.data), csize_t(decompSize))
  of 1: # RLE left-to-right
    decompressRleLr(dest, source.data, decompSize)
  of 2: # RLE up-to-down
    decompressRleUd(dest, source.data, decompSize, stride, int32(source.height))
  of 3: # LZG left-to-right
    discard decompressLzgLr(dest, source.data, decompSize)
  of 4: # LZG up-to-down
    discard decompressLzgUd(dest, source.data, decompSize, stride, int32(source.height))
  else:
    discard


proc calcStride(imageData: ImageDataType): int32 =
  var
    width: int32 = int32(imageData.width)
    flags: int32 = int32(imageData.flags)
    depth: int32 = ((flags shr 12) and 7) + 1
  return (depth * width + 7) div 8


proc convTo8bpp(inData: ptr byte, width, height, stride,
    depth: int32): ptr byte =
  var
    outData: ptr byte = cast[ptr byte](alloc(width * height))
    xPixel, pixelInByte: int32
    pixelsPerByte: int32 = 8 div depth
    mask: int32 = (1 shl depth) - 1
  for y in 0 ..< height:
    var
      inPos: ptr byte = changeBytePointer(inData, int32(y*stride))
      outPos: ptr byte = changeBytePointer(outData, int32(y*width))
    xPixel = 0
    for xByte in 0 ..< stride:
      var
        v: byte = inPos[]
        shift: int32 = 8
      pixelInByte = 0
      # for (pixelInByte = 0; pixelInByte < pixelsPerByte and xPixel < width; ++pixelInByte, ++xPixel) {
      while pixelInByte < pixelsPerByte and xPixel < width:
        shift -= depth
        outPos[] = byte((v shr shift) and mask)
        outPos = changeBytePointer(outPos, 1)
        inc(pixelInByte)
        inc(xPixel)
      inPos = changeBytePointer(inPos, 1)

  return outData


proc decodeImage(imageData: var ImageDataType,
    palette: var DatPalType): ImageType =
  var
    height: int32 = int32(imageData.height)
  if (height == 0):
    return nil
  var
    width: int32 = int32(imageData.width)
    flags: int32 = int32(imageData.flags)
    depth: int32 = ((flags shr 12) and 7) + 1
    cmeth: int32 = (flags shr 8) and 0x0F
    stride: int32 = calcStride(imageData)
    destSize: int32 = stride * height
    dest: ptr byte = cast[ptr byte](alloc0(destSize))
  # memset(dest, 0, destSize)
  decomprImg(dest, imageData, destSize, cmeth, stride)
  var
    image8bpp: ptr byte = convTo8bpp(dest, width, height, stride, depth)
  dealloc(dest)
  dest = nil
  var
    image: ImageType = createRGBSurface(0, width, height, 8, 0, 0, 0, 0)
  if (image == nil):
    sdlperror("SDL_CreateRGBSurface")
    quitPoP(1)
  if (lockSurface(image) != 0):
    sdlperror("SDL_LockSurface")

  for y in 0 ..< height:
    # fill image with data
    memcpy(addr(cast[ptr UncheckedArray[byte]](image.pixels)[
        y*image.pitch]), addr(cast[ptr UncheckedArray[byte]](image8bpp)[
            y*width]), csize_t(width))

  unlockSurface(image)

  # the following line is not necessary, as this variable is gc'ed
  # free(image8bpp); image8bpp = NULL
  var
    colors: array[16, Color]
  for i in 0 ..< 16:
    colors[i].r = palette.vga[i].r shl 2
    colors[i].g = palette.vga[i].g shl 2
    colors[i].b = palette.vga[i].b shl 2
    colors[i].a = ord(ALPHA_OPAQUE) # SDL2's SDL_Color has a fourth alpha component

  # Force 0th color to be black for non-transparent blitters. (hitpoints, shadow)
  # This is needed to remove the colored rectangles around hitpoints and the shadow, when using Brain's SNES graphics for example.
  colors[0].r = 0
  colors[0].g = 0
  colors[0].b = 0
  colors[0].a = ord(ALPHA_TRANSPARENT)
  discard setPaletteColors(image.format.palette, addr(colors[0]), 0,
      16) # SDL_SetColors = deprecated
  return image


# seg009:121A
proc loadImage(resourceId: int32, palette: var DatPalType): ImageType =
  var
  # stub
    resultIntern: DataLocation
    size: int32
    imageData: pointer = loadFromOpendatsAlloc(resourceId, "png", addr(
        resultIntern), addr(size))
    image: ImageType = nil
  case (resultIntern)
  of dataNone:
    return nil
  of data_DAT: # DAT
    image = decodeImage(cast[ptr ImageDataType](imageData)[], palette)
  of dataDirectory: # directory
    var
      rw: ptr RWops = rwFromConstMem(imageData, cint(size))
    if (rw == nil):
      sdlperror("SDL_RWFromConstMem")
      return nil

    image = load_RW(rw, 0)
    if (image == nil):
      echo("load_RW: " & $(sdl.getError()))
      quitPoP(1)

    if (rwClose(rw) != 0):
      sdlperror("SDL_RWclose")

  if (imageData != nil):
    dealloc(imageData)

  if (image != nil):
    # should immediately start using the onscreen pixel format, so conversion will not be needed

    if (setColorKey(image, 1, 0) != 0): #sdl 1.2: SDL_SRCCOLORKEY
      sdlperror("SDL_SetColorKey")
      quitPoP(1)

#    printf("bpp = modd\n", image.format.BitsPerPixel)
  if (setSurfaceAlphaMod(image, 0) != 0): #sdl 1.2: SDL_SetAlpha removed
    sdlperror("SDL_SetAlpha")
    quitPoP(1)

#    imageType* coloredImage = SDL_ConvertSurfaceFormat(image, SDL_PIXELFORMAT_ARGB8888, 0)
#    if (!coloredImage) {
#      sdlperror("SDL_ConvertSurfaceFormat")
#      quitPoP(1)
#    }
#    SDL_FreeSurface(image)
#    image = coloredImage

  return image


# seg009:13C4
proc drawImageTransp(image: ImageType, mask: ImageType, xpos, ypos: int32) =
  if (graphicsMode == ord(gmMcgaVga)):
    drawImageTranspVga(image, xpos, ypos)
  else:
    # ...
    discard


# seg009:157E
proc setJoyMode(): int32 =
  # stub
  if (numJoysticks() < 1):
    isJoystMode = 0
  else:
    if (gamecontrollerdbFile[0] != '\0'):
      discard gameControllerAddMappingsFromFile(gamecontrollerdbFile)
    if (isGameController(0)):
      sdlController = gameControllerOpen(0)
      if (sdlController == nil):
        isJoystMode = 0
      else:
        isJoystMode = 1

    # We have a joystick connected, but it's NOT compatible with the SDL_GameController
    # interface, so we resort to the classic SDL_Joystick interface instead
    else:
      sdlJoystick = joystickOpen(0)
      isJoystMode = 1
      usingSdlJoystickInterface = 1

  if (word(enableControllerRumble) and isJoystMode) != 0:
    sdlHaptic = hapticOpen(0)
    discard hapticRumbleInit(sdlHaptic) # initialize the device for simple rumble
  else:
    sdlHaptic = nil

  isKeyboardMode = not(isJoystMode)
  return int32(isJoystMode)


# seg009:178B
proc makeOffscreenBuffer(rect: var RectType): Surface =
  # stub
  when not(USE_ALPHA):
    # Bit order matches onscreen buffer, good for fading.
    return createRGBSurface(0, rect.right, rect.bottom, 24, 0xFF, 0xFF shl 8,
        0xFF shl 16, 0) #RGB888 (little endian)
  else:
    return createRGBSurface(0, rect.right, rect.bottom, 32, 0xFF, 0xFF shl 8,
        0xFF shl 16, 0xFF'u32 shl 24)
  #return surface


# seg009:17BD
# proc freeSurface(surface: Surface) =
#   freeSurface(surface)


# seg009:17EA
proc freePeel(peelPtr: ptr PeelType) =
  freeSurface(peelPtr.peel)
  # the following is not necessary, as PeelType is just an object managed by gc
  # free(peelPtr)


# seg009:182F
proc setHcPal() =
  # stub
  if (graphicsMode == ord(gmMcgaVga)):
    setPalArr(0, 16, addr(custom.vgaPalette[0]), 1)
  else:
    # ...
    discard


# seg009:2446
proc flipNotEga(memory: ptr byte, height, stride: int32) =
  var
    rowBuffer: ptr UncheckedArray[byte] = cast[ptr UncheckedArray[byte]](alloc(stride))
    topPtr: ptr byte
    bottomPtr: ptr byte
    remRows: int16 = int16(height shr 1)
    memoryIntern: ptr UncheckedArray[byte] = cast[ptr UncheckedArray[byte]](memory)
    topIndex: int32 = 0
    bottomIndex: int32 = (height - 1) * stride
  topPtr = addr(memoryIntern[topIndex])
  bottomPtr = addr(memoryIntern[bottomIndex])
  defer: dealloc(rowBuffer)
  memcpy(rowBuffer, topPtr, csize_t(stride))
  memcpy(topPtr, bottomPtr, csize_t(stride))
  memcpy(bottomPtr, rowBuffer, csize_t(stride))
  topIndex += stride
  bottomIndex -= stride
  topPtr = addr(memoryIntern[topIndex])
  bottomPtr = addr(memoryIntern[bottomIndex])
  dec(remRows)
  while remRows != 0:
    memcpy(rowBuffer, topPtr, csize_t(stride))
    memcpy(topPtr, bottomPtr, csize_t(stride))
    memcpy(bottomPtr, rowBuffer, csize_t(stride))
    topIndex += stride
    bottomIndex -= stride
    topPtr = addr(memoryIntern[topIndex])
    bottomPtr = addr(memoryIntern[bottomIndex])
    dec(remRows)


# seg009:19B1
proc flipScreen(surface: Surface) =
  # stub
  if (graphicsMode != ord(gmEga)):
    if (lockSurface(surface) != 0):
      sdlperror("SDL_LockSurface")
      quitPoP(1)

    flipNotEga(cast[ptr byte](surface.pixels), surface.h, surface.pitch)
    unlockSurface(surface)
  else:
    # ...
    discard


when not(USE_FADE):
  # seg009:19EF
  proc fadeIn2(sourceSurface: SurfaceType, whichRows: int32) =
    # stub
    method1BlitRect(onscreenSurface, sourceSurface, screenRect, screenRect, 0)


  # seg009:1CC9
  proc fadeOut2(rows: int32) =
    # stub
    discard


# seg009:2288
proc drawImageTranspVga(image: ImageType, xpos, ypos: int32) =
  # stub
  discard method6BlitImgToScr(image, xpos, ypos, ord(blitters10hTransp))


when (USE_TEXT):

  var
    hcFontData: array[1527, byte] = [
        0x20'u8, 0x83'u8, 0x07'u8, 0x00'u8, 0x02'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0xD2'u8, 0x00'u8, 0xD8'u8, 0x00'u8, 0xE5'u8, 0x00'u8,
        0xEE'u8, 0x00'u8, 0xFA'u8, 0x00'u8, 0x07'u8, 0x01'u8, 0x14'u8, 0x01'u8,
        0x21'u8, 0x01'u8, 0x2A'u8, 0x01'u8, 0x37'u8, 0x01'u8, 0x44'u8, 0x01'u8,
        0x50'u8, 0x01'u8, 0x5C'u8, 0x01'u8, 0x6A'u8, 0x01'u8, 0x74'u8, 0x01'u8,
        0x81'u8, 0x01'u8, 0x8E'u8, 0x01'u8, 0x9B'u8, 0x01'u8, 0xA8'u8, 0x01'u8,
        0xB5'u8, 0x01'u8, 0xC2'u8, 0x01'u8, 0xCF'u8, 0x01'u8, 0xDC'u8, 0x01'u8,
        0xE9'u8, 0x01'u8, 0xF6'u8, 0x01'u8, 0x03'u8, 0x02'u8, 0x10'u8, 0x02'u8,
        0x1C'u8, 0x02'u8, 0x2A'u8, 0x02'u8, 0x37'u8, 0x02'u8, 0x42'u8, 0x02'u8,
        0x4F'u8, 0x02'u8, 0x5C'u8, 0x02'u8, 0x69'u8, 0x02'u8, 0x76'u8, 0x02'u8,
        0x83'u8, 0x02'u8, 0x90'u8, 0x02'u8, 0x9D'u8, 0x02'u8, 0xAA'u8, 0x02'u8,
        0xB7'u8, 0x02'u8, 0xC4'u8, 0x02'u8, 0xD1'u8, 0x02'u8, 0xDE'u8, 0x02'u8,
        0xEB'u8, 0x02'u8, 0xF8'u8, 0x02'u8, 0x05'u8, 0x03'u8, 0x12'u8, 0x03'u8,
        0x1F'u8, 0x03'u8, 0x2C'u8, 0x03'u8, 0x39'u8, 0x03'u8, 0x46'u8, 0x03'u8,
        0x53'u8, 0x03'u8, 0x60'u8, 0x03'u8, 0x6D'u8, 0x03'u8, 0x7A'u8, 0x03'u8,
        0x87'u8, 0x03'u8, 0x94'u8, 0x03'u8, 0xA1'u8, 0x03'u8, 0xAE'u8, 0x03'u8,
        0xBB'u8, 0x03'u8, 0xC8'u8, 0x03'u8, 0xD5'u8, 0x03'u8, 0xE2'u8, 0x03'u8,
        0xEB'u8, 0x03'u8, 0xF9'u8, 0x03'u8, 0x02'u8, 0x04'u8, 0x0F'u8, 0x04'u8,
        0x1C'u8, 0x04'u8, 0x29'u8, 0x04'u8, 0x36'u8, 0x04'u8, 0x43'u8, 0x04'u8,
        0x50'u8, 0x04'u8, 0x5F'u8, 0x04'u8, 0x6C'u8, 0x04'u8, 0x79'u8, 0x04'u8,
        0x88'u8, 0x04'u8, 0x95'u8, 0x04'u8, 0xA2'u8, 0x04'u8, 0xAF'u8, 0x04'u8,
        0xBC'u8, 0x04'u8, 0xC9'u8, 0x04'u8, 0xD8'u8, 0x04'u8, 0xE7'u8, 0x04'u8,
        0xF4'u8, 0x04'u8, 0x01'u8, 0x05'u8, 0x0E'u8, 0x05'u8, 0x1B'u8, 0x05'u8,
        0x28'u8, 0x05'u8, 0x35'u8, 0x05'u8, 0x42'u8, 0x05'u8, 0x51'u8, 0x05'u8,
        0x5E'u8, 0x05'u8, 0x6B'u8, 0x05'u8, 0x78'u8, 0x05'u8, 0x85'u8, 0x05'u8,
        0x8D'u8, 0x05'u8, 0x9A'u8, 0x05'u8, 0xA7'u8, 0x05'u8, 0xBB'u8, 0x05'u8,
        0xD9'u8, 0x05'u8, 0x00'u8, 0x00'u8, 0x03'u8, 0x00'u8, 0x00'u8, 0x00'u8,
        0x07'u8, 0x00'u8, 0x02'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC0'u8, 0xC0'u8,
        0xC0'u8, 0xC0'u8, 0xC0'u8, 0x00'u8, 0xC0'u8, 0x03'u8, 0x00'u8, 0x05'u8,
        0x00'u8, 0x01'u8, 0x00'u8, 0xD8'u8, 0xD8'u8, 0xD8'u8, 0x06'u8, 0x00'u8,
        0x07'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x6C'u8, 0xFE'u8, 0x6C'u8,
        0xFE'u8, 0x6C'u8, 0x07'u8, 0x00'u8, 0x07'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x10'u8, 0x7C'u8, 0xD0'u8, 0x7C'u8, 0x16'u8, 0x7C'u8, 0x10'u8, 0x07'u8,
        0x00'u8, 0x08'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC3'u8, 0xC6'u8, 0x0C'u8,
        0x18'u8, 0x30'u8, 0x63'u8, 0xC3'u8, 0x07'u8, 0x00'u8, 0x08'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0x38'u8, 0x6C'u8, 0x38'u8, 0x7A'u8, 0xCC'u8, 0xCE'u8,
        0x7B'u8, 0x03'u8, 0x00'u8, 0x03'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x60'u8,
        0x60'u8, 0xC0'u8, 0x07'u8, 0x00'u8, 0x04'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x30'u8, 0x60'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0x60'u8, 0x30'u8, 0x07'u8,
        0x00'u8, 0x04'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC0'u8, 0x60'u8, 0x30'u8,
        0x30'u8, 0x30'u8, 0x60'u8, 0xC0'u8, 0x06'u8, 0x00'u8, 0x07'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0x00'u8, 0x6C'u8, 0x38'u8, 0xFE'u8, 0x38'u8, 0x6C'u8,
        0x06'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x30'u8,
        0x30'u8, 0xFC'u8, 0x30'u8, 0x30'u8, 0x08'u8, 0x00'u8, 0x03'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x60'u8,
        0x60'u8, 0xC0'u8, 0x04'u8, 0x00'u8, 0x04'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x00'u8, 0x00'u8, 0x00'u8, 0xF0'u8, 0x07'u8, 0x00'u8, 0x02'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0xC0'u8,
        0xC0'u8, 0x07'u8, 0x00'u8, 0x08'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x03'u8,
        0x06'u8, 0x0C'u8, 0x18'u8, 0x30'u8, 0x60'u8, 0xC0'u8, 0x07'u8, 0x00'u8,
        0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x78'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8,
        0xCC'u8, 0xCC'u8, 0x78'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8,
        0x00'u8, 0x30'u8, 0x70'u8, 0xF0'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0xFC'u8,
        0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x78'u8, 0xCC'u8,
        0x0C'u8, 0x18'u8, 0x30'u8, 0x60'u8, 0xFC'u8, 0x07'u8, 0x00'u8, 0x06'u8,
        0x00'u8, 0x01'u8, 0x00'u8, 0x78'u8, 0xCC'u8, 0x0C'u8, 0x18'u8, 0x0C'u8,
        0xCC'u8, 0x78'u8, 0x07'u8, 0x00'u8, 0x07'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x1C'u8, 0x3C'u8, 0x6C'u8, 0xCC'u8, 0xFE'u8, 0x0C'u8, 0x0C'u8, 0x07'u8,
        0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xF8'u8, 0xC0'u8, 0xC0'u8,
        0xF8'u8, 0x0C'u8, 0x0C'u8, 0xF8'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0x78'u8, 0xC0'u8, 0xC0'u8, 0xF8'u8, 0xCC'u8, 0xCC'u8,
        0x78'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xFC'u8,
        0x0C'u8, 0x18'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0x07'u8, 0x00'u8,
        0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x78'u8, 0xCC'u8, 0xCC'u8, 0x78'u8,
        0xCC'u8, 0xCC'u8, 0x78'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8,
        0x00'u8, 0x78'u8, 0xCC'u8, 0xCC'u8, 0x7C'u8, 0x0C'u8, 0xCC'u8, 0x78'u8,
        0x06'u8, 0x00'u8, 0x02'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0xC0'u8,
        0xC0'u8, 0x00'u8, 0xC0'u8, 0xC0'u8, 0x08'u8, 0x00'u8, 0x03'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0x00'u8, 0x60'u8, 0x60'u8, 0x00'u8, 0x00'u8, 0x60'u8,
        0x60'u8, 0xC0'u8, 0x07'u8, 0x00'u8, 0x05'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x18'u8, 0x30'u8, 0x60'u8, 0xC0'u8, 0x60'u8, 0x30'u8, 0x18'u8, 0x05'u8,
        0x00'u8, 0x04'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0xF0'u8,
        0x00'u8, 0xF0'u8, 0x07'u8, 0x00'u8, 0x05'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0xC0'u8, 0x60'u8, 0x30'u8, 0x18'u8, 0x30'u8, 0x60'u8, 0xC0'u8, 0x07'u8,
        0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x78'u8, 0xCC'u8, 0x0C'u8,
        0x18'u8, 0x30'u8, 0x00'u8, 0x30'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0x78'u8, 0xCC'u8, 0xDC'u8, 0xDC'u8, 0xD8'u8, 0xC0'u8,
        0x78'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x78'u8,
        0xCC'u8, 0xCC'u8, 0xFC'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0x07'u8, 0x00'u8,
        0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xF8'u8, 0xCC'u8, 0xCC'u8, 0xF8'u8,
        0xCC'u8, 0xCC'u8, 0xF8'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8,
        0x00'u8, 0x78'u8, 0xCC'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0xCC'u8, 0x78'u8,
        0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xF8'u8, 0xCC'u8,
        0xCC'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0xF8'u8, 0x07'u8, 0x00'u8, 0x05'u8,
        0x00'u8, 0x01'u8, 0x00'u8, 0xF8'u8, 0xC0'u8, 0xC0'u8, 0xF0'u8, 0xC0'u8,
        0xC0'u8, 0xF8'u8, 0x07'u8, 0x00'u8, 0x05'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0xF8'u8, 0xC0'u8, 0xC0'u8, 0xF0'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0x07'u8,
        0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x78'u8, 0xCC'u8, 0xC0'u8,
        0xDC'u8, 0xCC'u8, 0xCC'u8, 0x78'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0xFC'u8, 0xCC'u8, 0xCC'u8,
        0xCC'u8, 0x07'u8, 0x00'u8, 0x04'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xF0'u8,
        0x60'u8, 0x60'u8, 0x60'u8, 0x60'u8, 0x60'u8, 0xF0'u8, 0x07'u8, 0x00'u8,
        0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x0C'u8, 0x0C'u8, 0x0C'u8, 0x0C'u8,
        0x0C'u8, 0xCC'u8, 0x78'u8, 0x07'u8, 0x00'u8, 0x07'u8, 0x00'u8, 0x01'u8,
        0x00'u8, 0xC6'u8, 0xCC'u8, 0xD8'u8, 0xF0'u8, 0xD8'u8, 0xCC'u8, 0xC6'u8,
        0x07'u8, 0x00'u8, 0x05'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC0'u8, 0xC0'u8,
        0xC0'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0xF8'u8, 0x07'u8, 0x00'u8, 0x08'u8,
        0x00'u8, 0x01'u8, 0x00'u8, 0xC3'u8, 0xE7'u8, 0xFF'u8, 0xDB'u8, 0xC3'u8,
        0xC3'u8, 0xC3'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0xCC'u8, 0xCC'u8, 0xEC'u8, 0xFC'u8, 0xDC'u8, 0xCC'u8, 0xCC'u8, 0x07'u8,
        0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x78'u8, 0xCC'u8, 0xCC'u8,
        0xCC'u8, 0xCC'u8, 0xCC'u8, 0x78'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0xF8'u8, 0xCC'u8, 0xCC'u8, 0xF8'u8, 0xC0'u8, 0xC0'u8,
        0xC0'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x78'u8,
        0xCC'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0xD8'u8, 0x6C'u8, 0x07'u8, 0x00'u8,
        0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xF8'u8, 0xCC'u8, 0xCC'u8, 0xF8'u8,
        0xD8'u8, 0xCC'u8, 0xCC'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8,
        0x00'u8, 0x78'u8, 0xCC'u8, 0xC0'u8, 0x78'u8, 0x0C'u8, 0xCC'u8, 0x78'u8,
        0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xFC'u8, 0x30'u8,
        0x30'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0x07'u8, 0x00'u8, 0x06'u8,
        0x00'u8, 0x01'u8, 0x00'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8,
        0xCC'u8, 0x7C'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0xCC'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0x78'u8, 0x30'u8, 0x07'u8,
        0x00'u8, 0x08'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC3'u8, 0xC3'u8, 0xC3'u8,
        0xDB'u8, 0xFF'u8, 0xE7'u8, 0xC3'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0xCC'u8, 0xCC'u8, 0x78'u8, 0x30'u8, 0x78'u8, 0xCC'u8,
        0xCC'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xCC'u8,
        0xCC'u8, 0xCC'u8, 0x78'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0x07'u8, 0x00'u8,
        0x08'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xFF'u8, 0x06'u8, 0x0C'u8, 0x18'u8,
        0x30'u8, 0x60'u8, 0xFF'u8, 0x07'u8, 0x00'u8, 0x04'u8, 0x00'u8, 0x01'u8,
        0x00'u8, 0xF0'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0xF0'u8,
        0x07'u8, 0x00'u8, 0x08'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC0'u8, 0x60'u8,
        0x30'u8, 0x18'u8, 0x0C'u8, 0x06'u8, 0x03'u8, 0x07'u8, 0x00'u8, 0x04'u8,
        0x00'u8, 0x01'u8, 0x00'u8, 0xF0'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0x30'u8,
        0x30'u8, 0xF0'u8, 0x03'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x30'u8, 0x78'u8, 0xCC'u8, 0x08'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8,
        0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x00'u8,
        0xFC'u8, 0x03'u8, 0x00'u8, 0x04'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC0'u8,
        0x60'u8, 0x30'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x00'u8, 0x00'u8, 0x78'u8, 0x0C'u8, 0x7C'u8, 0xCC'u8, 0x7C'u8, 0x07'u8,
        0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC0'u8, 0xC0'u8, 0xF8'u8,
        0xCC'u8, 0xCC'u8, 0xCC'u8, 0xF8'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x78'u8, 0xCC'u8, 0xC0'u8, 0xCC'u8,
        0x78'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x0C'u8,
        0x0C'u8, 0x7C'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0x7C'u8, 0x07'u8, 0x00'u8,
        0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x78'u8, 0xCC'u8,
        0xFC'u8, 0xC0'u8, 0x7C'u8, 0x07'u8, 0x00'u8, 0x05'u8, 0x00'u8, 0x01'u8,
        0x00'u8, 0x38'u8, 0x60'u8, 0xF8'u8, 0x60'u8, 0x60'u8, 0x60'u8, 0x60'u8,
        0x09'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8,
        0x78'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0x7C'u8, 0x0C'u8, 0x78'u8, 0x07'u8,
        0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC0'u8, 0xC0'u8, 0xF8'u8,
        0xCC'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0x07'u8, 0x00'u8, 0x02'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0xC0'u8, 0x00'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8,
        0xC0'u8, 0x09'u8, 0x00'u8, 0x04'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x30'u8,
        0x00'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0x30'u8, 0xE0'u8,
        0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC0'u8, 0xC0'u8,
        0xCC'u8, 0xD8'u8, 0xF0'u8, 0xD8'u8, 0xCC'u8, 0x07'u8, 0x00'u8, 0x02'u8,
        0x00'u8, 0x01'u8, 0x00'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8,
        0xC0'u8, 0xC0'u8, 0x07'u8, 0x00'u8, 0x08'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x00'u8, 0x00'u8, 0xFE'u8, 0xDB'u8, 0xDB'u8, 0xDB'u8, 0xDB'u8, 0x07'u8,
        0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0xF8'u8,
        0xCC'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x78'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8,
        0x78'u8, 0x09'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8,
        0x00'u8, 0xF8'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0xF8'u8, 0xC0'u8, 0xC0'u8,
        0x09'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8,
        0x78'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0x7C'u8, 0x0C'u8, 0x0C'u8, 0x07'u8,
        0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x78'u8,
        0xCC'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8,
        0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x78'u8, 0xC0'u8, 0x78'u8, 0x0C'u8,
        0xF8'u8, 0x07'u8, 0x00'u8, 0x05'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x60'u8,
        0x60'u8, 0xF8'u8, 0x60'u8, 0x60'u8, 0x60'u8, 0x38'u8, 0x07'u8, 0x00'u8,
        0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0xCC'u8, 0xCC'u8,
        0xCC'u8, 0xCC'u8, 0x7C'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8,
        0x00'u8, 0x00'u8, 0x00'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0x78'u8, 0x30'u8,
        0x07'u8, 0x00'u8, 0x08'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8,
        0xC3'u8, 0xC3'u8, 0xDB'u8, 0xFF'u8, 0x66'u8, 0x07'u8, 0x00'u8, 0x06'u8,
        0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0xCC'u8, 0x78'u8, 0x30'u8,
        0x78'u8, 0xCC'u8, 0x09'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x00'u8, 0x00'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0xCC'u8, 0x7C'u8, 0x0C'u8,
        0x78'u8, 0x07'u8, 0x00'u8, 0x06'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8,
        0x00'u8, 0xFC'u8, 0x18'u8, 0x30'u8, 0x60'u8, 0xFC'u8, 0x07'u8, 0x00'u8,
        0x04'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0x30'u8, 0x60'u8, 0x60'u8, 0xC0'u8,
        0x60'u8, 0x60'u8, 0x30'u8, 0x07'u8, 0x00'u8, 0x02'u8, 0x00'u8, 0x01'u8,
        0x00'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8, 0x00'u8, 0xC0'u8, 0xC0'u8, 0xC0'u8,
        0x07'u8, 0x00'u8, 0x04'u8, 0x00'u8, 0x01'u8, 0x00'u8, 0xC0'u8, 0x60'u8,
        0x60'u8, 0x30'u8, 0x60'u8, 0x60'u8, 0xC0'u8, 0x02'u8, 0x00'u8, 0x07'u8,
        0x00'u8, 0x01'u8, 0x00'u8, 0x76'u8, 0xDC'u8, 0x07'u8, 0x00'u8, 0x07'u8,
        0x00'u8, 0x01'u8, 0x00'u8, 0x00'u8, 0x00'u8, 0x70'u8, 0xC4'u8, 0xCC'u8,
        0x8C'u8, 0x38'u8, 0x07'u8, 0x00'u8, 0x07'u8, 0x00'u8, 0x01'u8, 0x00'u8,
        0x00'u8, 0x06'u8, 0x0C'u8, 0xD8'u8, 0xF0'u8, 0xE0'u8, 0xC0'u8, 0x08'u8,
        0x00'u8, 0x10'u8, 0x00'u8, 0x02'u8, 0x00'u8, 0x7F'u8, 0xFE'u8, 0xCD'u8,
        0xC7'u8, 0xB5'u8, 0xEF'u8, 0xB5'u8, 0xEF'u8, 0x85'u8, 0xEF'u8, 0xB5'u8,
        0xEF'u8, 0xB4'u8, 0x6F'u8, 0x08'u8, 0x00'u8, 0x13'u8, 0x00'u8, 0x03'u8,
        0x00'u8, 0x7F'u8, 0xFF'u8, 0xC0'u8, 0xCC'u8, 0x46'u8, 0xE0'u8, 0xB6'u8,
        0xDA'u8, 0xE0'u8, 0xBE'u8, 0xDA'u8, 0xE0'u8, 0xBE'u8, 0xC6'u8, 0xE0'u8,
        0xB6'u8, 0xDA'u8, 0xE0'u8, 0xCE'u8, 0xDA'u8, 0x20'u8, 0x7F'u8, 0xFF'u8,
        0xC0'u8, 0x08'u8, 0x00'u8, 0x11'u8, 0x00'u8, 0x03'u8, 0x00'u8, 0x7F'u8,
        0xFF'u8, 0x00'u8, 0xC6'u8, 0x73'u8, 0x80'u8, 0xDD'u8, 0xAD'u8, 0x80'u8,
        0xCE'u8, 0xEF'u8, 0x80'u8, 0xDF'u8, 0x6F'u8, 0x80'u8, 0xDD'u8, 0xAD'u8,
        0x80'u8, 0xC6'u8, 0x73'u8, 0x80'u8, 0x7F'u8, 0xFF'u8, 0x00
      ]

  # This is a helper function in pure c, because pointer subtraction is not possible in Nim
  {.emit: """
    uint16_t pointerSubtraction(uint8_t* p1, uint8_t* p2) {
      return (unsigned short) ( p1 - p2 );
    }
  """.}

  proc pointerSubtraction(p1, p2: ptr byte): word {.importc, nodecl.}
  # TODO: not sure how to handle the pointers
  proc loadFontCharacterOffsets(data: ptr RawFontType) =
    var
      nChars: int32 = int32(data.lastChar - data.firstChar + 1)
      pos: ptr byte = cast[ptr byte](addr(cast[ptr UncheckedArray[word]](
          data.offsets)[nChars]))
    for index in 0 ..< nChars:
      data.offsets[index] = pointerSubtraction(pos, cast[ptr byte](data))
      var
        imageData: ptr ImageDataType = cast[ptr ImageDataType](pos)
        imageBytes: int32 = int32(imageData.height) * calcStride(imageData[])
      pos = addr(imageData.data[imageBytes])


  proc loadFontFromData(data: ptr RawFontType): FontType =
    var
      font: FontType = new(FontType)
    font.firstChar = data.firstChar
    font.lastChar = data.lastChar
    font.heightAboveBaseline = data.heightAboveBaseline
    font.heightBelowBaseline = data.heightBelowBaseline
    font.spaceBetweenLines = data.spaceBetweenLines
    font.spaceBetweenChars = data.spaceBetweenChars
    var
      nChars: int32 = int32(font.lastChar - font.firstChar + 1)
    # Allow loading a font even if the offsets for each character image were not supplied in the raw data.
    if (data.offsets[0] == 0):
      loadFontCharacterOffsets(data)

    var
      # chtab: ChtabType = new(ChtabType) # malloc(sizeof(chtabType) + sizeof(imageType* far) * nChars)
      chtab: ChtabType = cast[ChtabType](alloc(sizeof(ChtabTypeObject) + sizeof(
          ImageType) * nChars))
      `chr`, index: int32
      # Make a dummy palette for decodeImage().
      datPal: DatPalType
    memset(addr(datPal), 0, csize_t(sizeof(datPal)))
    datPal.vga[1].r = 0x3F
    datPal.vga[1].g = 0x3F
    datPal.vga[1].b = 0x3F # white
    index = 0
    `chr` = int32(data.firstChar)
    # for (index = 0, chr = data.firstChar; chr <= data.lastChar; ++index, ++chr) {
    while `chr` <= data.lastChar:
      var
        # TODO: not sure how to handle the pointers
        # imageData: ImageDataType = (/*const*/ imageDataType*)((/*const*/ byte*)data + data.offsets[index])
        imageData: ptr ImageDataType = cast[ptr ImageDataType](addr(cast[
            ptr UncheckedArray[byte]](data)[data.offsets[index]]))
      #imageData.flags=0
      if (imageData.height == 0):
        imageData.height = 1 # HACK: decodeImage() returns NULL if height==0.
      var
        image: ImageType
      image = decodeImage(imageData[], datPal)
      chtab.images[index] = image
      if setColorKey(image, cint(true), 0) != 0:
        sdlperror("SDL_SetColorKey")
        quitPoP(1)
      inc(index)
      inc(`chr`)

    font.chtab = chtab
    return font

  proc loadFont() =
    # Try to load font from a file.
    var
      dathandle: DatType = openDat("font", 0)
    hcFont.chtab = loadSpritesFromFile(1000, 1 shl 1, 0)
    closeDat(dathandle)
    if (hcFont.chtab == nil):
      # Use built-in font.
      hcFont = loadFontFromData(cast[ptr RawfontType](addr(hcFontData[0])))
    when (USE_MENU):
      hcSmallFont = loadFontFromData(cast[ptr RawfontType](addr(hcSmallFontData[0])))


  # seg009:35C5
  proc getCharWidth(character: byte): int32 =
    var
      font: FontType = textstate.ptrFont
      width: int32 = 0
    if (character <= font.lastChar and character >= font.firstChar):
      var
        image: ImageType = font.chtab.images[character - font.firstChar]
      if (image != nil):
        width += image.w #charPtrs[character - font.firstChar].width
        if width != 0:
          width += font.spaceBetweenChars
    return width


  # seg009:3E99
  proc findLinebreak(text: string, length, breakWidth, xAlign: int32): int32 =
    var
      currLineWidth: int16 # in pixels
      lastBreakPos: int16  # in characters
      currCharPos: int32 = 0
    lastBreakPos = 0
    currLineWidth = 0
    while (currCharPos < length):
      currLineWidth += int16(getCharWidth(byte(text[currCharPos])))
      if (currLineWidth <= breakWidth):
        inc(currCharPos)
        let
          currChar: char = text[currCharPos - 1]
        if (currChar == '\n'):
          return currCharPos
        if currCharPos < text.len:
          if (currChar == '-' or
            (xAlign <= 0 and (currChar == ' ' or text[currCharPos] == ' ')) or
            (text[currCharPos] == ' ' and currChar == ' ')
          ):
            # May break here.
            lastBreakPos = int16(currCharPos)
      else:
        if (lastBreakPos == 0):
          # If the first word is wider than breakWidth then break it.
          return currCharPos
        else:
          # Otherwise break at the last space.
          return lastBreakPos
    return currCharPos


  # seg009:403F
  proc getLineWidth(text: string, length: int32): int32 =
    var
      width: int32 = 0
      lengthIntern: int32 = length
      textPos: int32 = 0
    dec(lengthIntern)
    while (lengthIntern >= 0):
      width += getCharWidth(byte(text[textPos]))
      inc(textPos)
      dec(lengthIntern)

    return width


  # seg009:3706
  proc drawTextCharacter(character: byte): int32 =
    #printf("going to do drawTextCharacter...\n")
    var
      font: FontType = textstate.ptrFont
      width: int32 = 0
    if (character <= font.lastChar and character >= font.firstChar):
      var
        image: ImageType = font.chtab.images[character -
            font.firstChar] #charPtrs[character - font.firstChar]
      if (image != nil):
        discard method3BlitMono(image, int32(textstate.currentX), int32(
            textstate.currentY - font.heightAboveBaseline), int32(
            textstate.textblit), byte(textstate.textcolor))
        width = font.spaceBetweenChars + image.w
    textstate.currentX += int16(width)
    return width


  # seg009:377F
  proc drawTextLine(text: string, length: int32): int32 =
    #hideCursor()
    var
      width: int32 = 0
      textPos: int32 = 0
      lengthIntern: int32 = length - 1
    while (lengthIntern >= 0):
      width += drawTextCharacter(byte(text[textPos]))
      dec(lengthIntern)
      inc(textPos)
    #showCursor()
    return width


  # seg009:3755
  proc drawCstring(text: string): int32 =
    #hideCursor()
    var
      width: int32 = 0
      textPos: int32 = 0
    # while (*textPos) {
    for letter in text:
      width += drawTextCharacter(byte(letter))
    #showCursor()
    return width


  # seg009:3F01
  proc drawText(rectPtr: var RectType, xAlign, yAlign: int32, text: string,
      length: int32): RectType =
    #printf("going to do drawText()...\n")
    var
      rectTop: int16
      rectHeight: int16
      rectWidth: int16
      #textinfoType var_C
      numLines: int16
      fontLineDistance: int16
    #hideCursor()
    #getTextinfo(&var_C)
    setClipRect(rectPtr)
    rectWidth = rectPtr.right - rectPtr.left
    rectTop = rectPtr.top
    rectHeight = rectPtr.bottom - rectPtr.top
    numLines = 0
    var
      remLength: int32 = length
      lineStart: int32 = 0
    const
      MAX_LINES = 100
    var
      lineStarts: array[MAX_LINES, int32]
      lineLengths: array[MAX_LINES, int32]
    block doBlock:
      var
        lineLength: int32 = findLinebreak(text[lineStart..^1], remLength,
            rectWidth, xAlign)
      if lineLength == 0:
        break doBlock
      if numLines >= MAX_LINES:
        echo "drawText(): Too many lines!"
        quitPoP(1)
      # TODO: use correct target
      lineStarts[numLines] = lineStart
      lineLengths[numLines] = lineLength
      inc(numLines)
      lineStart += lineLength
      remLength -= lineLength
      while remLength != 0:
        var
          lineLength: int32 = findLinebreak(text[lineStart..^1], remLength,
              rectWidth, xAlign)
        if lineLength == 0:
          break doBlock
        if numLines >= MAX_LINES:
          echo "drawText(): Too many lines!"
          quitPoP(1)
        # TODO: use correct target
        lineStarts[numLines] = lineStart
        lineLengths[numLines] = lineLength
        inc(numLines)
        lineStart += lineLength
        remLength -= lineLength
    var
      font: FontType = textstate.ptrFont
    fontLineDistance = font.heightAboveBaseline + font.heightBelowBaseline +
        font.spaceBetweenLines
    var
      textHeight: int32 = fontLineDistance * numLines - font.spaceBetweenLines
      textTop: int32 = rectTop
    if (yAlign >= 0):
      if (yAlign <= 0):
        # middle
        # The +1 is for simulating SHR + ADC/SBB.
        textTop += (rectHeight+1) div 2 - (textHeight+1) div 2
      else:
        # bottom
        textTop += rectHeight - textHeight
    textstate.currentY = int16(textTop + font.heightAboveBaseline)
    for i in 0 ..< numLines:
      var
        linePos: int32 = lineStarts[i]
        lineLength: int32 = lineLengths[i]
      if (xAlign < 0 and
        text[linePos] == ' ' and
        i != 0 and
        text[linePos-1] != '\n'
      ):
        # Skip over space if it's not at the beginning of a line.
        inc(linePos)
        dec(lineLength)
        if (lineLength != 0 and
          text[linePos] == ' ' and
          text[linePos-2] == '.'
        ):
          # Skip over second space after point.
          inc(linePos)
          dec(lineLength)
      var
        lineWidth: int32 = getLineWidth(text[linePos..^1], lineLength)
        textLeft: int32 = rectPtr.left
      if (xAlign >= 0):
        if (xAlign <= 0):
          # center
          textLeft += rectWidth div 2 - lineWidth div 2
        else:
          # right
          textLeft += rectWidth - lineWidth
      textstate.currentX = int16(textLeft)
      #printf("going to draw text line...\n")
      discard drawTextLine(text[linePos..^1], lineLength)
      textstate.currentY += fontLineDistance
    resetClipRect()
    #setTextinfo(...)
    #showCursor()
    return rectPtr


  # seg009:3E4F
  proc showText(rectPtr: ptr RectType, xAlign, yAlign: int32, text: string) =
    # stub
    #printf("showText: mods\n",text)
    discard drawText(rectPtr[], xAlign, yAlign, text, text.len)


  # seg009:04FF
  proc showTextWithColor(rectPtr: var RectType, xAlign, yAlign: int32,
      text: string, color: int32) =
    var
      savedTextcolor: int16
    savedTextcolor = textstate.textcolor
    textstate.textcolor = int16(color)
    showText(addr(rectPtr), xAlign, yAlign, text)
    textstate.textcolor = savedTextcolor


  # seg009:3A91
  proc setCurrPos(xpos, ypos: int32) =
    textstate.currentX = int16(xpos)
    textstate.currentY = int16(ypos)


  # # seg009:145A
  proc initCopyprotDialog() =
    copyprotDialog = makeDialogInfo(addr(dialogSettings), dialogRect1, dialogRect1)
    copyprotDialog.peel = readPeelFromScreen(copyprotDialog.peelRect)


  # seg009:0838
  proc showmessage(text: string, arg4: int32, arg0: pointer): int32 =
    var
      key: word
      rect: RectType
    #fontType* savedFontPtr
    #surfaceType* oldTarget
    #oldTarget = currentTargetSurface
    #currentTargetSurface = onscreenSurface_
    # In the disassembly there is some messing with the currentTargetSurface and font (?)
    # However, this does not seem to be strictly necessary
    method1BlitRect(offscreenSurface, onscreenSurface, copyprotDialog.peelRect,
        copyprotDialog.peelRect, 0)
    drawDialogFrame(copyprotDialog)
    #savedFontPtr = textstate.ptrFont
    #savedFontPtr = currentTargetSurface.ptrFont
    #currentTargetSurface.ptrFont = ptrFont
    discard shrink2Rect(rect, copyprotDialog.textRect, 2, 1)
    showTextWithColor(rect, 0, 0, text, ord(color15Brightwhite))
    #textstate.ptrFont = savedFontPtr
    #currentTargetSurface.ptrFont = savedFontPtr
    clearKbdBuf()
    idle()
    key = word(keyTestQuit())
    while(key == 0):
      idle()
      key = word(keyTestQuit()) # Press any key to continue...
    #restoreDialogPeel2(copyprotDialog.peel)
    #currentTargetSurface = oldTarget
    needFullRedraw = 1 # lazy: instead of neatly restoring only the relevant part, just redraw the whole screen
    return int32(key)


  # seg009:08FB
  proc makeDialogInfo(settings: ptr DialogSettingsType, dialogRect,
      textRect: var RectType, dialogPeel: var PeelType): ptr DialogType =
    var
      dialogInfo: ptr DialogType
    dialogInfo = cast[ptr DialogType](alloc0(sizeof(DialogType)))
    dialogInfo.settings = settings
    dialogInfo.hasPeel = 0
    dialogInfo.peel = addr(dialogPeel)
    dialogInfo.textRect = textRect
    calcDialogPeelRect(dialogInfo)
    # does not seem to be quite right; see seg009:0948 (?)
    readDialogPeel(dialogInfo)

    return dialogInfo

  proc makeDialogInfo(settings: ptr DialogSettingsType, dialogRect,
      textRect: var RectType): ptr DialogType =
    var
      dialogInfo: ptr DialogType
    dialogInfo = cast[ptr DialogType](alloc0(sizeof(DialogType)))
    dialogInfo.settings = settings
    dialogInfo.hasPeel = 0
    dialogInfo.peel = nil
    dialogInfo.textRect = textRect
    calcDialogPeelRect(dialogInfo)
    # does not seem to be quite right; see seg009:0948 (?)
    readDialogPeel(dialogInfo)

    return dialogInfo

  # seg009:0BE7
  proc calcDialogPeelRect(dialog: ptr DialogType) =
    var
      settings: ptr DialogSettingsType
    settings = dialog.settings
    dialog.peelRect.left = dialog.textRect.left - settings.leftBorder
    dialog.peelRect.top = dialog.textRect.top - settings.topBorder
    dialog.peelRect.right = dialog.textRect.right + settings.rightBorder +
        settings.shadowRight
    dialog.peelRect.bottom = dialog.textRect.bottom + settings.bottomBorder +
        settings.shadowBottom


  # seg009:0BB0
  proc readDialogPeel(dialog: ptr DialogType) =
    var
      peel: ptr PeelType
    if (dialog.hasPeel) != 0:
      if (dialog.peel == nil):
        peel = readPeelFromScreen(dialog.peelRect)
        dialog.peel = peel

      dialog.hasPeel = 1
      drawDialogFrame(dialog)


  # seg009:09DE
  proc drawDialogFrame(dialog: ptr DialogType) =
    dialog.settings.method2Frame(dialog)


  # A pointer to this function is the first field of dialogSettings (data:2944)
  # Perhaps used when replacing a dialog's text with another text (?)
  # seg009:096F
  proc addDialogRect(dialog: ptr DialogType) =
    drawRect(dialog.textRect, ord(color0Black))


  # seg009:09F0
  proc dialogMethod2Frame(dialog: ptr DialogType) =
    var
      rect: RectType
      shadowRight: int16 = dialog.settings.shadowRight
      shadowBottom: int16 = dialog.settings.shadowBottom
      bottomBorder: int16 = dialog.settings.bottomBorder
      outerBorder: int16 = dialog.settings.outerBorder
      peelTop: int16 = dialog.peelRect.top
      peelLeft: int16 = dialog.peelRect.left
      peelBottom: int16 = dialog.peelRect.bottom
      peelRight: int16 = dialog.peelRect.right
      textTop: int16 = dialog.textRect.top
      textLeft: int16 = dialog.textRect.left
      textBottom: int16 = dialog.textRect.bottom
      textRight: int16 = dialog.textRect.right
    # Draw outer border
    rect = RectType(top: peelTop, left: peelLeft, bottom: peelBottom -
        shadowBottom, right: peelRight - shadowRight)
    drawRect(rect, ord(color0Black))
    # Draw shadow (right)
    rect = RectType(top: textTop, left: peelRight - shadowRight,
        bottom: peelBottom, right: peelRight)
    drawRect(rect, getTextColor(0, ord(color8Darkgray), 0))
    # Draw shadow (bottom)
    rect = RectType(top: peelBottom - shadowBottom, left: textLeft,
        bottom: peelBottom, right: peelRight)
    drawRect(rect, getTextColor(0, ord(color8Darkgray), 0))
    # Draw inner border (left)
    rect = RectType(top: peelTop + outerBorder, left: peelLeft + outerBorder,
        bottom: textBottom, right: textLeft)
    drawRect(rect, ord(color15Brightwhite))
    # Draw inner border (top)
    rect = RectType(top: peelTop + outerBorder, left: textLeft, bottom: textTop,
        right: textRight + dialog.settings.rightBorder - outerBorder)
    drawRect(rect, ord(color15Brightwhite))
    # Draw inner border (right)
    rect.top = textTop
    rect.left = textRight
    rect.bottom = textBottom + bottomBorder - outerBorder; # (rect.right stays the same)
    drawRect(rect, ord(color15Brightwhite))
    # Draw inner border (bottom)
    rect = RectType(top: textBottom, left: peelLeft + outerBorder,
        bottom: textBottom + bottomBorder - outerBorder, right: textRight)
    drawRect(rect, ord(color15Brightwhite))


  # seg009:0C44
  proc showDialog(text: string) =
    var
      strng: string = newStringOfCap(256)
    # snprintf(string, sizeof(string), "mods\n\nPress any key to continue.", text)
    strng = text & "\n\nPress any key to continue."
    # showmessage(strng, 1, &keyTestQuit)
    discard showmessage(strng, 1, nil)


  # seg009:0791
  proc getTextCenterY(rect: ptr RectType): int32 =
    var
      font: ptr FontType
      emptyHeight: int16 # height of empty space above+below the line of text
    font = addr(hcFont) #currentTargetSurface.ptrFont
    emptyHeight = rect.bottom - font.heightAboveBaseline -
        font.heightBelowBaseline - rect.top
    return ((emptyHeight - emptyHeight mod 2) shr 1) +
        font.heightAboveBaseline + emptyHeight mod 2 + rect.top


  # seg009:3E77
  proc getCstringWidth(text: string): int32 =
    var
      width: int32 = 0
      textPos: int32 = 0
    while (textPos < text.len):
      width += getCharWidth(byte(text[textPos]))
      inc(textPos)

    return width


  # seg009:0767
  proc drawTextCursor(xpos, ypos, color: int32) =
    setCurrPos(xpos, ypos)
    textstate.textcolor = int16(color)
    discard drawTextCharacter(byte('_'))
    #restoreCurrColor()
    textstate.textcolor = 15


  # seg009:053C
  proc inputStr(rect: var RectType, buffer: var string, maxLength: int32,
      initial: string, hasInitial, arg4, color, bgcolor: int32): int32 =
    var
      length: int16
      key: word
      cursorVisible: int16
      currentXpos: int16
      ypos: int16
      initLength: int16
    length = 0
    cursorVisible = 0
    drawRect(rect, bgcolor)
    initLength = int16(initial.len)
    if (hasInitial) != 0:
      buffer = initial
      length = initLength

    currentXpos = int16(rect.left + arg4)
    ypos = int16(getTextCenterY(addr(rect)))
    setCurrPos(currentXpos, ypos)
    textstate.textcolor = int16(color)
    discard drawCstring(initial)
    #restoreCurrPos?()
    currentXpos += int16(getCstringWidth(initial) + int32(initLength != 0) * arg4)
    while true:
      key = 0
      while true:
        if (cursorVisible) != 0:
          drawTextCursor(currentXpos, ypos, color)
        else:
          drawTextCursor(currentXpos, ypos, bgcolor)
        cursorVisible = not(cursorVisible)
        startTimer(ord(timer0), 6)
        if (key) != 0:
          if (cursorVisible) != 0:
            drawTextCursor(currentXpos, ypos, color)
            cursorVisible = not(cursorVisible)
          if (key == ord(SCANCODE_RETURN)): # enter
            buffer[length] = '\0'
            return length
          else:
            break
        key = word(keyTestQuit())
        while ((hasTimerStopped(ord(timer0)) == 0) and key == 0):
          idle()
          key = word(keyTestQuit())
      # Only use the printable ASCII chars (UTF-8 encoding)
      var
        enteredChar: char = if byte(lastTextInput) <= 0x7E: char(
            lastTextInput) else: '\0'
      clearKbdBuf()

      if (key == ord(SCANCODE_ESCAPE)): # esc
        drawRect(rect, bgcolor)
        buffer[0] = '\0'
        return -1

      if (length != 0 and (key == ord(SCANCODE_BACKSPACE) or
          key == ord(SCANCODE_DELETE))): # backspace, delete
        dec(length)
        drawTextCursor(currentXpos, ypos, bgcolor)
        currentXpos -= int16(getCharWidth(byte(buffer[length])))
        setCurrPos(currentXpos, ypos)
        textstate.textcolor = int16(bgcolor)
        discard drawTextCharacter(byte(buffer[length]))
        #restoreCurrPos?()
        drawTextCursor(currentXpos, ypos, color)
      elif (byte(enteredChar) >= 0x20 and byte(enteredChar) <= 0x7E and length < maxLength):
        # Would the new character make the cursor go past the right side of the rect?
        if (getCharWidth(byte('_')) + getCharWidth(byte(enteredChar)) +
            currentXpos < rect.right):
          drawTextCursor(currentXpos, ypos, bgcolor)
          setCurrPos(currentXpos, ypos)
          textstate.textcolor = int16(color)
          buffer[length] = enteredChar
          currentXpos += int16(drawTextCharacter(byte(buffer[length])))
          inc(length)

else: # USE_TEXT

  # seg009:3706
  proc drawTextCharacter(character: byte): int32 =
    # stub
    echo("drawTextCharacter: " & $(character))
    return 0


  # seg009:3E4F
  proc showText(rectPtr: ptr RectType, xAlign, yAlign: int32, text: string) =
    # stub
    echo("showText: " & text)


  # seg009:04FF
  proc showTextWithColor(rectPtr: var RectType, xAlign, yAlign: int32,
      text: string, color: int32) =
    #short savedTextcolor
    #savedTextcolor = textstate.textcolor
    #textstate.textcolor = color
    showText(addr(rectPtr), xAlign, yAlign, text)
    #textstate.textcolor = savedTextcolor


  # seg009:3A91
  proc setCurrPos(xpos, ypos: int32) =
    # stub
    discard


  # seg009:0C44
  proc showDialog(text: string) =
    # stub
    echo(text)


  # seg009:053C
  proc inputStr(rect: var RectType, buffer: var string, maxLength: int32,
      initial: string, hasInitial, arg4, color, bgcolor: int32): int32 =
    # stub
    buffer = "dummy input text"
    return buffer.len

  proc showmessage(text: string, arg4: int32, arg0: pointer): int32 =
    # stub
    echo text
    return 0

  proc initCopyprotDialog() =
    # stub
    discard

  proc drawDialogFrame(dialog: ptr DialogType) =
    # stub
    discard

  proc addDialogRect(dialog: ptr DialogType) =
    # stub
    discard

  proc dialogMethod2Frame(dialog: ptr DialogType) =
    # stub
    discard

# seg009:37E8
proc drawRect(rect: var RectType, color: int32) =
  discard method5Rect(rect, ord(blitters0NoTransp), byte(color))


# seg009:3985
proc rectSthg(surface: SurfaceType, rect: ptr RectType): SurfaceType =
  # stub
  return surface


# seg009:39CE
proc shrink2Rect(targetRect, sourceRect: var RectType, deltaX,
    deltaY: int32): RectType =
  targetRect.top = int16(sourceRect.top + deltaY)
  targetRect.left = int16(sourceRect.left + deltaX)
  targetRect.bottom = int16(sourceRect.bottom - deltaY)
  targetRect.right = int16(sourceRect.right - deltaX)
  return targetRect


# seg009:3BBA
proc restorePeel(peelPtr: ptr PeelType) =
  #printf("restoring peel at (x=modd, y=modd)\n", peelPtr.rect.left, peelPtr.rect.top) # debug
  discard method6BlitImgToScr(peelPtr.peel, peelPtr.rect.left, peelPtr.rect.top, 0)
  freePeel(peelPtr)
  #SDL_FreeSurface(peelPtr.peel)


# seg009:3BE9
proc readPeelFromScreen(rect: var RectType): ptr PeelType =
  # stub
  result = cast[ptr PeelType](alloc0(sizeof(PeelType)))
  #memset(&result, 0, sizeof(result))
  result.rect = rect
  when (USE_ALPHA):
    var
      peelSurface: Surface = createRGBSurface(0, rect.right - rect.left,
          rect.bottom - rect.top, 32, 0xFF, 0xFF shl 8, 0xFF shl 16, 0xFF shl 24)
  else:
    var
      peelSurface: Surface = createRGBSurface(0, rect.right - rect.left,
          rect.bottom - rect.top, 24, 0xFF, 0xFF shl 8, 0xFF shl 16, 0)
  if (peelSurface == nil):
    sdlperror("readPeelFromScreen: SDL_CreateRGBSurface")
    quitPoP(1)

  result.peel = peelSurface
  var
    targetRect: RectType = RectType(top: 0, left: 0, bottom: rect.right -
        rect.left, right: rect.bottom - rect.top)
  method1BlitRect(result.peel, currentTargetSurface, targetRect, rect, 0)
  return result


# seg009:3D95
proc intersectRect(output, input1, input2: var RectType): int32 =
  var
    left: int16 = max(input1.left, input2.left)
    right: int16 = min(input1.right, input2.right)
  if (left < right):
    output.left = left
    output.right = right
    var
      top: int16 = max(input1.top, input2.top)
      bottom: int16 = min(input1.bottom, input2.bottom)
    if (top < bottom):
      output.top = top
      output.bottom = bottom
      return 1

  # memset(output, 0, sizeof(rectType))
  output.top = 0
  output.bottom = 0
  output.left = 0
  output.right = 0
  return 0


# seg009:4063
proc unionRect(output, input1, input2: var RectType): RectType =
  var
    top: int16 = min(input1.top, input2.top)
    left: int16 = min(input1.left, input2.left)
    bottom: int16 = max(input1.bottom, input2.bottom)
    right: int16 = max(input1.right, input2.right)
  output.top = top
  output.left = left
  output.bottom = bottom
  output.right = right
  return output


type
  userevents = enum
    userevent_SOUND,
    userevent_TIMER


var
  speakerPlaying: int16 = 0
  digiPlaying: int16 = 0
  oggPlaying: int16 = 0

  # The currently playing sound buffer for the PC speaker.
  currentSpeakerSound: ptr SpeakerType
  # Index for which note is currently playing.
  speakerNoteIndex: int32
  # Tracks how long the last (partially played) speaker note has been playing (for the audio callback).
  currentSpeakerNoteSamplesAlreadyEmitted: int32

proc speakerSoundStop() =
  if speakerPlaying == 0:
    return
  lockAudio()
  speakerPlaying = 0
  currentSpeakerSound = nil
  speakerNoteIndex = 0
  currentSpeakerNoteSamplesAlreadyEmitted = 0
  unlockAudio()

var
  # The current buffer, holds the resampled sound data.
  digiBuffer: ptr byte = nil
  # The current position in digiBuffer.
  digiRemainingPos: ptr byte = nil
  # The remaining length.
  digiRemainingLength: int32 = 0

const
  # The desired samplerate. Everything will be resampled to this.
  digiSamplerate: int32 = 44100

proc stopDigi() =
#  SDL_PauseAudio(1)
  if digiPlaying == 0:
    return
  lockAudio()
  digiPlaying = 0

  # if (SDL_GetAudioStatus() == SDL_AUDIO_PLAYING) {
  #   SDL_PauseAudio(1)
  #   SDL_CloseAudio()
  # }
  # if (digiAudiospec != NULL):
  #   free(digiAudiospec)
  #   digiAudiospec = NULL

  digiBuffer = nil
  digiRemainingLength = 0
  digiRemainingPos = nil
  unlockAudio()


var
  # Decoder for the currently playing OGG sound. (This also holds the playback position.)
  oggDecoder: StbVorbis

proc stopOgg() =
  pauseAudio(1)
  if oggPlaying == 0:
    return
  oggPlaying = 0
  lockAudio()
  oggDecoder = nil
  unlockAudio()


# seg009:7214
proc stopSounds() =
  # stub
  stopDigi()
  stopMidi()
  speakerSoundStop()
  stopOgg()


var
  squareWaveState: int16 = 4000 # If the amplitude is too high, the speaker sounds will be really loud!
  squareWaveSamplesSinceLastFlip: float

proc generateSquareWave(stream: ptr byte, noteFreq: float, samples: int32) =
  var
    channels: int32 = int32(digiAudiospec.channels)
    halfPeriodInSamples: float = (float(digiAudiospec.freq) / noteFreq) * 0.5
    waveStream: ptr byte = stream

    samplesLeft: int32 = samples
  while (samplesLeft > 0):
    if (squareWaveSamplesSinceLastFlip > halfPeriodInSamples):
      # Produce a square wave by flipping the signal.
      squareWaveState = not(squareWaveState)
      # Note(Falcury): not completely sure that this is the right way to prevent glitches in the sound...
      # Because I can still hear some hiccups, e.g. in the music. Especially when switching between notes.
      squareWaveSamplesSinceLastFlip -= halfPeriodInSamples
    else:
      var
        samplesUntilNextFlip: int32 = int32(halfPeriodInSamples - squareWaveSamplesSinceLastFlip)
      inc(samplesUntilNextFlip) # round up.

      var
        samplesToEmit: int32 = min(samplesUntilNextFlip, samplesLeft)
      for i in 0 ..< samplesToEmit * channels:
        cast[ptr int16](waveStream)[] = squareWaveState
        waveStream = addr(cast[ptr UncheckedArray[byte]](waveStream)[sizeof(int16)])

      samplesLeft -= samplesToEmit
      squareWaveSamplesSinceLastFlip += float(samplesToEmit)


proc speakerCallback(userdata: pointer, stream: ptr uint8, length: cint) =
  var
    outputChannels: int32 = int32(digiAudiospec.channels)
    bytesPerSample: int32 = sizeof(int16) * outputChannels
    samplesRequested: int32 = length div bytesPerSample
    speakerStream: ptr uint8 = stream

  if (currentSpeakerSound == nil):
    return
  var
    tempo: word = currentSpeakerSound.tempo
    totalSamplesLeft: int32 = samplesRequested
  while (totalSamplesLeft > 0):
    var
      note: ptr NoteType = addr(currentSpeakerSound.notes[speakerNoteIndex])
    if (note.frequency == 0x12):
      speakerPlaying = 0
      currentSpeakerSound = nil
      speakerNoteIndex = 0
      var
        event: Event
      memset(addr(event), 0, csize_t(sizeof(event)))
      event.kind = USEREVENT
      event.user.code = ord(userevent_SOUND)
      discard pushEvent(addr(event))
      return

    var
      noteLengthInSamples: int32 = (note.length * digiAudiospec.freq) div int32(tempo)
      noteSamplesToEmit: int32 = min(noteLengthInSamples -
          currentSpeakerNoteSamplesAlreadyEmitted, totalSamplesLeft)
    totalSamplesLeft -= noteSamplesToEmit
    var
      copyLen: csize_t = csize_t(noteSamplesToEmit * bytesPerSample)
    if (note.frequency <= 0x01):
      memset(speakerStream, cint(digiAudiospec.silence), csize_t(copyLen))
    else:
      generateSquareWave(speakerStream, float(note.frequency), noteSamplesToEmit)

    speakerStream = addr(cast[ptr UncheckedArray[byte]](speakerStream)[copyLen])

    var
      noteSamplesEmitted: int32 = currentSpeakerNoteSamplesAlreadyEmitted + noteSamplesToEmit
    if (noteSamplesEmitted < noteLengthInSamples):
      currentSpeakerNoteSamplesAlreadyEmitted += noteSamplesToEmit
    else:
      inc(speakerNoteIndex)
      currentSpeakerNoteSamplesAlreadyEmitted = 0


# seg009:7640
proc playSpeakerSound(buffer: var SoundBufferType) =
  speakerSoundStop()
  stopSounds()
  currentSpeakerSound = addr(buffer.bu.speaker)
  speakerNoteIndex = 0
  speakerPlaying = 1
  pauseAudio(0)


proc digiCallback(userdata: pointer, stream: ptr byte, length: cint) =
  # Don't go over the end of either the input or the output buffer.
  var
    copyLen: csize_t = csize_t(min(length, digiRemainingLength))
  #printf("digiCallback(): copyLen = modd\n", copyLen)
  #printf("digiCallback(): length = modd\n", length)
  if (isSoundOn) != 0:
    # Copy the next part of the input of the output.
    memcpy(stream, digiRemainingPos, copyLen)
    # In case the sound does not fill the buffer: fill the rest of the buffer with silence.
    memset(stream, cint(digiAudiospec.silence), csize_t(length) - copyLen)
  else:
    # If sound is off: Mute the sound but keep track of where we are.
    memset(stream, cint(digiAudiospec.silence), csize_t(length))

  # If the sound ended, push an event.
  if ((digiPlaying != 0) and digiRemainingLength == 0):
    #printf("digiCallback(): sound ended\n")
    var
      event: Event
    memset(addr(event), 0, csize_t(sizeof(event)))
    event.kind = USEREVENT
    event.user.code = ord(userevent_SOUND)
    digiPlaying = 0
    discard pushEvent(addr(event))

  # Advance the pointer.
  digiRemainingLength -= int32(copyLen)
  digiRemainingPos = addr(cast[ptr UncheckedArray[byte]](digiRemainingPos)[copyLen])


proc oggCallback(userdata: pointer, stream: ptr byte, length: cint) =
  var
    outputChannels: int32 = int32(digiAudiospec.channels)
    bytesPerSample: int32 = sizeof(int16) * outputChannels
    samplesRequested: int32 = length div bytesPerSample

    samplesFilled: int32
  if (isSoundOn) != 0:
    samplesFilled = stbVorbisGetSamplesShortInterleaved(oggDecoder,
        outputChannels, cast[ptr int16](stream), cint(length div sizeof(int16)))
    if (samplesFilled < samplesRequested):
      # In case the sound does not fill the buffer: fill the rest of the buffer with silence.
      var
        bytesFilled: int32 = samplesFilled * bytesPerSample
        remainingBytes: int32 = (samplesRequested - samplesFilled) * bytesPerSample
      memset(stream, cint(digiAudiospec.silence), csize_t(remainingBytes))
  else:
    # If sound is off: Mute the sound, but keep track of where we are.
    memset(stream, cint(digiAudiospec.silence), csize_t(length))
    # Let the decoder run normally (to advance the position), but discard the result.
    var
      discardedSamples: ptr byte = cast[ptr byte](alloc(length))
    samplesFilled = stbVorbisGetSamplesShortInterleaved(oggDecoder,
        outputChannels, cast[ptr int16](discardedSamples), cint(
            length div sizeof(int16)))

  # Push an event if the sound has ended.
  if (samplesFilled == 0):
    #printf("oggCallback(): sound ended\n")
    var
      event: Event
    memset(addr(event), 0, csize_t(sizeof(event)))
    event.kind = USEREVENT
    event.user.code = ord(userevent_SOUND)
    oggPlaying = 0
    discard pushEvent(addr(event))


when (USE_FAST_FORWARD) and (FAST_FORWARD_RESAMPLE_SOUND):
  var
    cvt: AudioCVT
    cvtInitialized: bool = false

proc audioCallback(userdata: pointer, streamOrig: ptr uint8,
    lengthOrig: cint) {.cdecl.} =
  setupForeignThreadGc()

  var
    stream: ptr uint8
    length: int32
  when (USE_FAST_FORWARD):
    if (audioSpeed > 1):
      length = lengthOrig * audioSpeed;
      stream = cast[ptr uint8](alloc(length))
    else:
      length = lengthOrig
      stream = streamOrig
  else:
    length = lengthOrig
    stream = streamOrig

  memset(stream, cint(digiAudiospec.silence), csize_t(length))
  if (digiPlaying) != 0:
    digiCallback(userdata, stream, length)
  elif (speakerPlaying) != 0:
    speakerCallback(userdata, stream, length)

  # Note: music sounds and digi sounds are allowed to play simultaneously (will be blended together)
  # I.e., digi sounds and music will not cut each other short.
  if (midiPlaying) != 0:
    midiCallback(userdata, stream, length)
  elif (oggPlaying) != 0:
    oggCallback(userdata, stream, length)
  tearDownForeignThreadGc()

  when (USE_FAST_FORWARD):
    if (audioSpeed > 1):

      when (FAST_FORWARD_MUTE):
        memset(streamOrig, digiAudiospec.silence, lenOrig)
      else:
        when (FAST_FORWARD_RESAMPLE_SOUND):
          if not(cvtInitialized):
            BuildAudioCVT(addr(cvt),
              digiAudiospec.format, digiAudiospec.channels, digiAudiospec.freq *
                  audioSpeed,
              digiAudiospec.format, digiAudiospec.channels, digiAudiospec.freq)
            cvtInitialized = true

          #realloc(stream, len * cvt.lenMult);
          #cvt.buf = stream;
          cvt.`len` = length
          cvt.buf = alloc(cvt.`len` * cvt.lenMult)
          memcpy(cvt.buf, stream, csize_t(cvt.`len`))
          #printf("cvt.needed = %d\n", cvt.needed);
          #printf("cvt.lenMult = %d\n", cvt.lenMult);
          #printf("cvt.lenRatio = %lf\n", cvt.lenRatio);
          ConvertAudio(addr(cvt))

          memcpy(streamOrig, cvt.buf, csize_t(lengthOrig))
          dealloc(cvt.buf)
          cvt.buf = nil
        else:
          # Hack: use the beginning of the buffer instead of resampling.
          memcpy(streamOrig, stream, csize_t(lengthOrig))

      dealloc(stream)

proc initDigi() =
  if (digiUnavailable) != 0:
    return
  if (digiAudiospec != nil):
    return
  # Open the audio device. Called once.
  #printf("initDigi(): called\n")

  var
    desiredAudioformat: AudioFormat
    version: Version
  getVersion(addr(version))
  #printf("SDL Version = modd.modd.modd\n", version.major, version.minor, version.patch)
  if (version.major <= 2 and version.minor <= 0 and version.patch <= 3):
    # In versions before 2.0.4, 16-bit audio samples don't work properly (the sound becomes garbled).
    # See: https:#bugzilla.libsdl.org/showBug.cgi?id=2389
    # Workaround: set the audio format to 8-bit, if we are linking against an older SDL2 version.
    desiredAudioformat = AUDIO_U8
    echo("Your SDL.dll is older than 2.0.4. Using 8-bit audio format to work around resampling bug.")
  else:
    desiredAudioformat = AUDIO_S16SYS

  var
    desired: ptr AudioSpec
  desired = cast[ptr AudioSpec](alloc(sizeof(AudioSpec)))
  memset(desired, 0, csize_t(sizeof(AudioSpec)))
  desired.freq = digiSamplerate #buffer.digi.sampleRate
  desired.format = desiredAudioformat
  desired.channels = 2
  desired.samples = 1024
  desired.callback = audioCallback
  desired.userdata = nil
  if (openAudio(desired, nil) != 0):
    sdlperror("SDL_OpenAudio")
    #quit(1)
    digiUnavailable = 1
    return

  #SDL_PauseAudio(0)
  digiAudiospec = desired

var
  soundNamesInitialized: bool = false

proc loadSoundNames() =
  var
    namesPath: string = locateFile("data/music/names.txt")
  if (soundNamesInitialized):
    return
  soundNamesInitialized = true
  var
    fp: File
  if not(open(fp, namesPath, fmRead)):
    return
  # soundNames = (char**) calloc(sizeof(char*) * maxSoundId, 1)
  # soundNames = newSeqOfCap[string](maxSoundId)
  for line in lines(fp):
    var
      splits = split(line, '=')
    if splits.len != 2:
      continue
    var
      index: int32 = int32(parseInt(splits[0]))
    if index >= 0 and index < maxSoundId:
      soundNames[index] = splits[1]

  close(fp)


proc soundName(index: int32): string =
  if (soundNamesInitialized and index >= 0 and index < maxSoundId):
    return soundNames[index]
  else:
    return ""


proc convertDigiSound(digiBuffer: ptr SoundBufferType): ptr SoundBufferType

proc loadSound(index: int32): ptr SoundBufferType =
  # soundBufferType* result = NULL
  #printf("loadSound(modd)\n", index)
  initDigi()
  if ((enableMusic != 0) and (digiUnavailable == 0) and result == nil and
      index >= 0 and index < maxSoundId):
    #printf("Trying to load from music folder\n")

    #loadSoundNames();  # Moved to loadSounds()
    if (soundNamesInitialized and soundName(index).len != 0):
      #printf("Loading from music folder\n")
      block soundBlock:
        var
          fp: File
          filename: string # = newStringOfCap( POP_MAX_PATH )
        if not(skipModDataFiles):
          # before checking the root directory, first try mods/MODNAME/
          # snprintfCheck(filename, sizeof(filename), "mods/music/mods.ogg", modDataPath, soundName(index))
          filename = modDataPath & "/music/" & soundName(index) & ".ogg"
          if not(open(fp, filename, fmRead)) and not(skipNormalDataFiles):
          # snprintfCheck(filename, sizeof(filename), "data/music/mods.ogg", soundName(index))
            filename = "data/music/" & soundName(index) & ".ogg"
            discard open(fp, locateFile(filename), fmRead)
        if (fp == nil):
          break soundBlock

        # Read the entire file (undecoded) into memory.
        var
          info: FileInfo
        info = getFileInfo(fp)
        # if (fstat(fileno(fp), &info))
        #   break
        var
          fileSize: cint = cint(max(0, info.size))
          fileContents: ptr byte = cast[ptr byte](alloc(fileSize))
        # if (fread(fileContents, 1, fileSize, fp) != fileSize):
        if readBuffer(fp, fileContents, fileSize) != fileSize:
          dealloc(fileContents)
          close(fp)
          break soundBlock

        close(fp)

        # Decoding the entire file immediately would make the loading time much longer.
        # However, we can also create the decoder now, and only use it when we are actually playing the file.
        # (In the audio callback, we'll decode chunks of samples to the output stream, as needed).
        var
          decoder: StbVorbis = stbVorbisOpenMemory(fileContents, fileSize, nil, nil)
        if (decoder == nil):
          dealloc(fileContents)
          break soundBlock

        result = cast[ptr SoundBufferType](alloc(sizeof(SoundBufferType)))
        result.`type` = ord(soundOgg)
        result.bu.ogg.totalLength = int32(stbVorbisStreamLengthInSamples(
            decoder)) * sizeof(int16)
        result.bu.ogg.fileContents = fileContents # Remember in case we want to free the sound later.
        result.bu.ogg.decoder = decoder
    else:
      #printf("soundNames = modp\n", soundNames)
      #printf("soundNames[modd] = modp\n", index, soundName(index))
      discard
  if (result == nil):
    #printf("Trying to load from DAT\n")
    result = cast[ptr SoundBufferType](loadFromOpendatsAlloc(index + 10000,
        "bin", nil, nil))

  if (result != nil and (result.`type` and 7) == ord(soundDigi)):
    var
      converted: ptr SoundBufferType = convertDigiSound(result)
    dealloc(result)
    result = converted

  if (result == nil and not(skipNormalDataFiles)):
    write(stderr, "Failed to load sound " & $(index) & " '" & soundName(index) & "'")

  return result


proc playOggSound(buffer: ptr SoundBufferType) =
  initDigi()
  if (digiUnavailable) != 0:
    return
  stopSounds()

  # Need to rewind the music, or else the decoder might continue where it left off, the last time this sound played.
  stbVorbisSeekStart(buffer.bu.ogg.decoder)

  lockAudio()
  oggDecoder = buffer.bu.ogg.decoder
  unlockAudio()
  pauseAudio(0)

  oggPlaying = 1


var
  waveVersion: int32 = -1

type
  WaveInfoType = object
    sampleRate, sampleSize, sampleCount: int32
    samples: ptr byte

proc determineWaveVersion(buffer: ptr SoundBufferType,
    waveinfo: var WaveInfoType): bool =
  var
    version: int32 = waveVersion
  if (version == -1):
    # Determine the version of the wave data.
    version = 0
    if (buffer.bu.digi.sampleSize == 8):
      version += 1
    if (buffer.bu.digiNew.sampleSize == 8):
      version += 2
    if (version == 1 or version == 2):
      waveVersion = version

  case version
  of 1: # 1.0 and 1.1
    waveinfo.sampleRate = int32(buffer.bu.digi.sampleRate)
    waveinfo.sampleSize = int32(buffer.bu.digi.sampleSize)
    waveinfo.sampleCount = int32(buffer.bu.digi.sampleCount)
    waveinfo.samples = cast[ptr byte](addr(buffer.bu.digi.samples[0]))
    return true
  of 2: # 1.3 and 1.4 (and PoP2)
    waveinfo.sampleRate = int32(buffer.bu.digiNew.sampleRate)
    waveinfo.sampleSize = int32(buffer.bu.digiNew.sampleSize)
    waveinfo.sampleCount = int32(buffer.bu.digiNew.sampleCount)
    waveinfo.samples = cast[ptr byte](addr(buffer.bu.digiNew.samples[0]))
    return true
  of 3: # ambiguous
    echo("Warning: Ambiguous wave version.")
    return false
  else: # of 0, unknown
    echo("Warning: Can't determine wave version.")
    return false


proc convertDigiSound(digiBuffer: ptr SoundBufferType): ptr SoundBufferType =
  initDigi()
  if (digiUnavailable) != 0:
    return nil
  var
    waveinfo: WaveInfoType
  if (false == determineWaveVersion(digiBuffer, waveinfo)):
    return nil

  var
    freqRatio: float = float(waveinfo.sampleRate) / float(digiAudiospec.freq)

    sourceLength: int32 = waveinfo.sampleCount
    expandedFrames: int32 = sourceLength *
        digiAudiospec.freq div waveinfo.sampleRate
    expandedLength: int32 = expandedFrames * 2 * sizeof(int16)
    convertedBuffer: ptr SoundBufferType = cast[ptr SoundBufferType](alloc(
        sizeof(SoundBufferType) + expandedLength))

  convertedBuffer.`type` = ord(soundDigiConverted)
  convertedBuffer.bu.converted.length = expandedLength

  var
    source: ptr byte = waveinfo.samples
    dest: ptr UncheckedArray[int16] = addr(convertedBuffer.bu.converted.samples)
    destPosition: int32 = 0

  for i in 0 ..< expandedFrames:
    var
      srcFrameFloat: float = float(i) * freqRatio
      srcFrame0: int32 = int32(srcFrameFloat) # truncation

      sample0: int32 = (int32(cast[ptr UncheckedArray[byte]](source)[
          srcFrame0]) or (int32(cast[ptr UncheckedArray[byte]](source)[
          srcFrame0]) shl 8)) - 32768
      interpolatedSample: int16
    if (srcFrame0 >= waveinfo.sampleCount-1):
      interpolatedSample = int16(sample0)
    else:
      var
        srcFrame1: int32 = srcFrame0 + 1
        alpha: float = srcFrameFloat - float(srcFrame0)
        sample1: int32 = (int32(cast[ptr UncheckedArray[byte]](source)[
            srcFrame1]) or (int32(cast[ptr UncheckedArray[byte]](source)[
            srcFrame1]) shl 8)) - 32768
      interpolatedSample = int16((1.0 - alpha) * float(sample0) + alpha * float(sample1))

    for channel in 0 ..< digiAudiospec.channels:
      dest[destPosition] = interpolatedSample
      inc(destPosition)

  return convertedBuffer


# seg009:74F0
proc playDigiSound(buffer: ptr SoundBufferType) =
  #if (!isSoundOn) return
  initDigi()
  if (digiUnavailable) != 0:
    return
  stopDigi()
  #  stopSounds()
  #printf("playDigiSound(): called\n")
  if ((buffer.`type` and 7) != ord(soundDigiConverted)):
    echo("Tried to play unconverted digi sound.")
    return

  lockAudio()
  digiBuffer = cast[ptr byte](addr(buffer.bu.converted.samples[0]))
  digiPlaying = 1
  digiRemainingLength = buffer.bu.converted.length
  digiRemainingPos = digiBuffer
  unlockAudio()
  pauseAudio(0)


proc freeSound(buffer: ptr SoundBufferType) =
  if (buffer == nil):
    return
  if (buffer.`type` == ord(soundOgg)):
    stbVorbisClose(buffer.bu.ogg.decoder)
    dealloc(buffer.bu.ogg.fileContents)
  dealloc(buffer)


# seg009:7220
proc playSoundFromBuffer(buffer: ptr SoundBufferType) =

  when (USE_REPLAY):
    if (replaying and skippingReplay) != 0:
      return

  # stub
  if (buffer == nil):
    echo("Tried to play NULL sound.")
    #quit(1)
    return

  case (buffer.`type` and 7)
  of ord(soundSpeaker):
    playSpeakerSound(buffer[])
  of ord(soundDigiConverted), ord(soundDigi):
    playDigiSound(buffer)
  of ord(soundMidi):
    playMidiSound(buffer)
  of ord(soundOgg):
    playOggSound(buffer)
  else:
    echo("Tried to play unimplemented sound type " & $(int64(buffer.`type`)))
    quitPoP(1)


proc turnMusicOnOff(newState: byte) =
  enableMusic = newState
  turnSoundOnOff(isSoundOn)


# seg009:7273
proc turnSoundOnOff(newState: byte) =
  # stub
  isSoundOn = newState
  #if (!isSoundOn) stopSounds()


# seg009:7299
proc checkSoundPlaying(): int32 =
  return speakerPlaying or digiPlaying or midiPlaying or oggPlaying


proc applyAspectRatio() =
  # Allow us to use a consistent set of screen co-ordinates, even if the screen size changes
  if (useCorrectAspectRatio) != 0:
    discard renderSetLogicalSize(renderer, 320 * 5, 200 * 6) # 4:3
  else:
    discard renderSetLogicalSize(renderer, 320, 200) # 16:10

  windowResized()


proc windowResized() =
#if SDL_VERSION_ATLEAST(2,0,5) # SDL_RenderSetIntegerScale
  when(sdl.PATCHLEVEL >= 5):
    if (useIntegerScaling) != 0:
      var
        windowWidth, windowHeight: cint
      # On high-DPI screens, this is what we need instead of SDL_GetWindowSize().
      #SDL_GL_GetDrawableSize(window_, &windowWidth, &windowHeight)
      discard getRendererOutputSize(renderer, addr(windowWidth), addr(windowHeight))
      var
        renderWidth, renderHeight: cint
      renderGetLogicalSize(renderer, addr(renderWidth), addr(renderHeight))
      # Disable integer scaling if it would result in downscaling.
      # Because then the only suitable integer scaling factor is zero, i.e. the picture disappears.
      var
        makesSense: bool = (windowWidth >= renderWidth and windowHeight >= renderHeight)
      discard renderSetIntegerScale(renderer, makesSense)
  else:
    discard

var
  initializedOverlays: bool = false

proc initOverlay() =
  if not(initializedOverlays):
    overlaySurface = createRGBSurface(0, 320, 200, 32, 0xFF, 0xFF shl 8,
        0xFF shl 16, 0xFF'u32 shl 24)
    mergedSurface = createRGBSurface(0, 320, 200, 24, 0xFF, 0xFF shl 8,
        0xFF shl 16, 0)
    initializedOverlays = true



var
  onscreenSurface2x: Surface

proc initScaling() =
  # Don't crash in validate mode.
  if (renderer == nil):
    return
  if (textureSharp == nil):
    textureSharp = createTexture(renderer, PIXELFORMAT_RGB24,
        TEXTUREACCESS_STREAMING, 320, 200)

  if (scalingType == 1):
    if (not(isRendererTargettextureSupported) and onscreenSurface2x == nil):
      onscreenSurface2x = createRGBSurface(0, 320*2, 200*2, 24, 0xFF,
          0xFF shl 8, 0xFF shl 16, 0)

    if (textureFuzzy == nil):
      discard setHint(HINT_RENDER_SCALE_QUALITY, "1")
      var
        access: TextureAccess = if isRendererTargettextureSupported: TEXTUREACCESS_TARGET else: TEXTUREACCESS_STREAMING
      textureFuzzy = createTexture(renderer, PIXELFORMAT_RGB24, access, 320*2, 200*2)
      discard setHint(HINT_RENDER_SCALE_QUALITY, "0")

    targetTexture = textureFuzzy
  elif (scalingType == 2):
    if (textureBlurry == nil):
      discard setHint(HINT_RENDER_SCALE_QUALITY, "1")
      textureBlurry = createTexture(renderer, PIXELFORMAT_RGB24,
          TEXTUREACCESS_STREAMING, 320, 200)
      discard setHint(HINT_RENDER_SCALE_QUALITY, "0")
    targetTexture = textureBlurry
  else:
    targetTexture = textureSharp
  if (targetTexture == nil):
    sdlperror("SDL_CreateTexture")
    quitPoP(1)


# seg009:38ED
proc setGrMode(grmode: byte) =
  # when (HINT_WINDOWS_DISABLE_THREAD_NAMING):
  # discard setHint(HINT_WINDOWS_DISABLE_THREAD_NAMING, "1")
  if (sdl.init(INIT_VIDEO or INIT_TIMER or INIT_NOPARACHUTE or
      INIT_GAMECONTROLLER or INIT_HAPTIC) != 0):
    sdlperror("SDL_Init")
    quitPoP(1)

  #SDL_EnableUNICODE(1) #deprecated
  var
    flags: uint32 = 0
  # TODO: find another solution to read the parameter full
  if (startFullscreen == 0) and fullCommandOption:
    startFullscreen = 1
  if (startFullscreen) != 0:
    flags = flags or ord(WINDOW_FULLSCREEN_DESKTOP)
  flags = flags or ord(WINDOW_RESIZABLE)
  flags = flags or ord(WINDOW_ALLOW_HIGHDPI) # for Retina displays

  # Should use different default window dimensions when using 4:3 aspect ratio
  if ((useCorrectAspectRatio != 0) and popWindowWidth == 640 and
      popWindowHeight == 400):
    popWindowHeight = 480

  # TODO: ignore for now to let windows know that we are DPI aware
  #when (win32)
  #  # Tell Windows that the application is DPI aware, to prevent unwanted bitmap stretching.
  #  # SetProcessDPIAware() is only available on Windows Vista and later, so we need to load it dynamically.
  #  BOOL WINAPI (*SetProcessDPIAware)()
  #  HMODULE user32dll = LoadLibraryA("User32.dll")
  #  if (user32dll):
  #    SetProcessDPIAware = GetProcAddress(user32dll, "SetProcessDPIAware")
  #    if (SetProcessDPIAware):
  #      SetProcessDPIAware()

  #    FreeLibrary(user32dll)

  ##endif

  when (USE_REPLAY):
    if isValidateMode == 0: # run without a window if validating a replay
      window = createWindow(WINDOW_TITLE, WINDOWPOS_UNDEFINED,
          WINDOWPOS_UNDEFINED, cint(popWindowWidth), cint(popWindowHeight), flags)
  else:
    window = createWindow(WINDOW_TITLE, WINDOWPOS_UNDEFINED,
        WINDOWPOS_UNDEFINED, cint(popWindowWidth), cint(popWindowHeight), flags)
  # Make absolutely sure that VSync will be off, to prevent timer issues.
  discard setHint(HINT_RENDER_VSYNC, "0")
  when (USE_HW_ACCELERATION):
    const
      RENDER_BACKEND: uint32 = ord(RENDERER_ACCELERATED)
  else:
    const
      RENDER_BACKEND: uint32 = ord(RENDERER_SOFTWARE)
  renderer = createRenderer(window, -1, RENDER_BACKEND or ord(RENDERER_TARGETTEXTURE))
  var
    rendererInfo: RendererInfo
  if (getRendererInfo(renderer, addr(rendererInfo)) == 0):
    if (rendererInfo.flags and ord(RENDERER_TARGETTEXTURE)) != 0:
      isRendererTargettextureSupported = true
  if (useIntegerScaling) != 0:
    #if SDL_VERSION_ATLEAST(2,0,5) # SDL_RenderSetIntegerScale
    when(sdl.PATCHLEVEL >= 5):
      discard renderSetIntegerScale(renderer, true)
    else:
      echo("Warning: You need to compile with SDL 2.0.5 or newer for the useIntegerScaling option.")

  var
    icon: Surface = load(locateFile("data/icon.png"))
  if (icon == nil):
    sdlperror("Could not load icon")
  else:
    setWindowIcon(window, icon)

  applyAspectRatio()
  windowResized()

  # Migration to SDL2: everything is still blitted to onscreenSurface_, however:
  # SDL2 renders textures to the screen instead of surfaces; so, every screen
  # update causes the onscreenSurface_ to be copied into the targetTexture, which is
  # subsequently displayed.
  # The function handling the screen updates is updateScreen()
  # */
  onscreenSurface = createRGBSurface(0, 320, 200, 24, 0xFF, 0xFF shl 8,
      0xFF shl 16, 0)
  if (onscreenSurface == nil):
    sdlperror("createRGBSurface")
    quitPoP(1)
  initOverlay()
  initScaling()
  if (startFullscreen) != 0:
    discard showCursor(DISABLE)



  #  SDL_WM_SetCaption(WINDOW_TITLE, NULL)
  #  if (SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL) != 0) {  #deprecated
  #    sdlperror("SDL_EnableKeyRepeat")
  #    quitPoP(1)
  #  }
  graphicsMode = ord(gmMcgaVga)
  when (USE_TEXT):
    loadFont()


proc getFinalSurface(): Surface =
  if not(isOverlayDisplayed):
    return onscreenSurface
  else:
    return mergedSurface


proc drawOverlay() =
  var
    overlay: int32 = 0
  isOverlayDisplayed = false
  when (USE_DEBUG_CHEATS):
    if ((isTimerDisplayed != 0) and startLevel > 0):
      overlay = 1 # Timer overlay
    elif ((fixes.fixQuicksaveDuringFeather != 0'u8) and
          (isFeatherTimerDisplayed != 0'u8) and
          startLevel > 0'i16 and
          isFeatherFall > 0'u16):
      overlay = 3 # Feather timer overlay
  when (USE_MENU):
    # Menu overlay - not drawn here directly, only copied from the overlay surface.
    if (isPaused and word(isMenuShown)) != 0:
      overlay = 2
  if (overlay != 0):
    isOverlayDisplayed = true
    var
      savedTargetSurface: Surface = currentTargetSurface
    currentTargetSurface = overlaySurface
    var
      drawnRect: RectType
    if (overlay == 1):
      when (USE_DEBUG_CHEATS):
        var
          timerText: string
        if (remMin < 0):
          # snprintf(timerText, sizeof(timerText), "mod02d:mod02d:mod02d",
          timerText = $( -(remMin + 1)) & ":" & $( (719 - remTick) div 12) &
              ":" & $((719 - remTick) mod 12)
        else:
          # snprintf(timerText, sizeof(timerText), "mod02d:mod02d:mod02d",
          timerText = $(remMin - 1) & ":" & $(remTick div 12) & ":" & $(remTick mod 12)

        var
          expectedNumericChars: int32 = 6
          extraNumericChars: int32 = max(0, timerText.len - 8)
          lineWidth: int32 = 5 + (expectedNumericChars + extraNumericChars) * 9

          timerBoxRect: RectType = RectType(top: 0, left: 0, bottom: 11,
              right: int16(2 + lineWidth))
          timerTextRect: RectType = RectType(top: 2, left: 2, bottom: 10, right: 100)
        drawRectWithAlpha(timerBoxRect, ord(color0Black), 128)
        showText(addr(timerTextRect), -1, -1, timerText)

        when (USE_REPLAY):
          # During playback, display the number of ticks since start, if the timer is shown (debug cheats: T).
          if (replaying):
            let
              ticksText: String = "T: " & $(currTick)
            var
              ticksBoxRect: RectType = timerBoxRect
              ticksTextRect: RectType = timerTextRect
            ticksBoxRect.top += 12
            ticksBoxRect.bottom += 12
            ticksTextRect.top += 12
            ticksTextRect.bottom += 12

            drawRectWithAlpha(ticksBoxRect, ord(color0Black), 128)
            showText(ticksTextRect, -1, -1, ticksText)

            timerBoxRect.bottom += 12

        drawnRect = timerBoxRect # Only need to blit this bit to the mergedSurface.
      else:
        discard
    elif (overlay == 3): # Feather timer
      when (USE_DEBUG_CHEATS):
        let
          expectedNumericChars: int32 = 6
          ticksPerSec: int32 = int32(getTicksPerSec(timer1))
        var
          timerText: string = $(isFeatherFall div ticksPerSec) & "&" & $(
              isFeatherFall mod ticksPerSec)
          # snprintf(timerText, sizeof(timerText), "%02d:%02d", isFeatherFall / ticksPerSec, isFeatherFall % ticksPerSec);
        let
          extraNumericChars: int32 = max(0, max(len(timerText), 24)) # sizeof(timerText) - 8)
          lineWidth: int32 = 5 + (expectedNumericChars + extraNumericChars) * 9;

        var
          timerBoxRect: RectType = RectType(top: 0, left: 0, bottom: 11,
              right: int16(2 + lineWidth))
          timerTextRect: RectType = RectType(top: 2, left: 2, bottom: 10, right: 100)
        drawRectWithAlpha(timerBoxRect, ord(color0Black), 128)
        showTextWithColor(timerTextRect, -1, -1, timerText, ord(color10Brightgreen))

        drawnRect = timerBoxRect # Only need to blit this bit to the mergedSurface.
      else:
        discard
    else:
      drawnRect = screenRect # We'll blit the whole contents of overlaySurface to the mergedSurface.
    var
      sdlRect: Rect
    rectToSdlrect(drawnRect, sdlRect)
    discard blitSurface(onscreenSurface, nil, mergedSurface, nil)
    discard blitSurface(overlaySurface, addr(sdlRect), mergedSurface, addr(sdlRect))
    currentTargetSurface = savedTargetSurface


proc updateScreen() =
  drawOverlay()
  var
    surface: Surface = getFinalSurface()
  initScaling()
  if (scalingType == 1):
    # Make "fuzzy pixels" like DOSBox does:
    # First scale to double size with nearest-neighbor scaling, then scale to full screen with smooth scaling.
    # The result is not as blurry as if we did only a smooth scaling, but not as sharp as if we did only nearest-neighbor scaling.
    if (isRendererTargettextureSupported):
      discard updateTexture(textureSharp, nil, surface.pixels, surface.pitch)
      discard setHint(HINT_RENDER_SCALE_QUALITY, "1")
      discard setRenderTarget(renderer, targetTexture)
      discard setHint(HINT_RENDER_SCALE_QUALITY, "0")
      discard renderClear(renderer)
      discard renderCopy(renderer, textureSharp, nil, nil)
      discard setRenderTarget(renderer, nil)
    else:
      discard blitScaled(surface, nil, onscreenSurface2x, nil)
      surface = onscreenSurface2x
      discard updateTexture(targetTexture, nil, surface.pixels, surface.pitch)
  else:
    discard updateTexture(targetTexture, nil, surface.pixels, surface.pitch)

  discard renderClear(renderer)
  discard renderCopy(renderer, targetTexture, nil, nil)
  renderPresent(renderer)


# seg009:9289
proc setPalArr(start, count: int32, `array`: pointer, vsync: int32) =
  # stub
  for i in 0 ..< count:
    if (array != nil):
      let
        paletteArray: ptr UncheckedArray[RgbType] = cast[ptr UncheckedArray[
            RgbType]](`array`)
      setPal(start + i, int32(paletteArray[i].r), int32(paletteArray[i].g),
          int32(paletteArray[i].b), vsync)
    else:
      setPal(start + i, 0, 0, 0, vsync)


var
  palette: array[256, RgbType]

# seg009:92DF
proc setPal(index, red, green, blue, vsync: int32) =
  # stub
  #palette[index] = ((red&0x3F)shl2)|((green&0x3F)shl2shl8)|((blue&0x3F)shl2shl16)
  palette[index].r = byte(red)
  palette[index].g = byte(green)
  palette[index].b = byte(blue)


# seg009:969C
proc addPaletteBits(nColors: byte): int32 =
  # stub
  return 0


# seg009:9C36
proc findFirstPalRow(whichRowsMask: int32): int32 =
  var
    whichRow: word = 0
    rowMask: word = 1
  if (int(rowMask) and whichRowsMask) != 0:
    return int32(whichRow)
  inc(whichRow)
  rowMask = rowMask shl 1
  while (whichRow < 16):
    if (int(rowMask) and whichRowsMask) != 0:
      return int32(whichRow)
    inc(whichRow)
    rowMask = rowMask shl 1
  return 0


# seg009:9C6C
proc getTextColor(cgaColor, lowHalf, highHalfMask: int32): int32 =
  if (graphicsMode == ord(gmCga) or graphicsMode == ord(gmHgaHerc)):
    return cgaColor
  elif (graphicsMode == ord(gmMcgaVga) and highHalfMask != 0):
    return (findFirstPalRow(highHalfMask) shl 4) + lowHalf
  else:
    return lowHalf


proc loadFromOpendatsMetadata(resourceId: int32, extension: string,
    outFp: var File, res: var DataLocation, checksum: ptr byte, size: var int32,
    outPointer: var DatType) =
  var
    imageFilename: string
    fp: File
    pntr: DatType
  res = dataNone
  pntr = datChainPtr
  # Go through all open DAT files.
  # for (pntr = datChainPtr; fp == NULL and pntr != NULL; pntr = pntr.nextDat) {
  while fp == nil and pntr != nil:
    outpointer = pntr
    if (pntr.handle != nil):
      # If it's an actual DAT file:
      fp = pntr.handle
      var
        datTable: ptr DatTableType = pntr.datTable
        iOut: int32 = int32(datTable.resCount)
      for i in 0 ..< int32(datTable.resCount):
        if (int(datTable.entries[i].id) == resourceId):
          iOut = int32(i)
          break
      if (iOut < int32(datTable.resCount)):
        # found
        res = data_DAT
        size = int32(datTable.entries[iOut].size)
        # if (fseek(fp, datTable.entries[iOut].offset, SEEK_SET) or
        #     fread(checksum, 1, 1, fp) != 1) {
        setFilePos(fp, int64(datTable.entries[iOut].offset), fspSet)
        if readBuffer(fp, checksum, 1) != 1:
          perror(pntr.filename)
          fp = nil
      else:
        # not found
        fp = nil
    else:
      # If it's a directory:
      var
        filenameNoExt: string
      # strip the .DAT file extension from the filename (use folders simply named TITLE, KID, VPALACE, etc.)
      filenameNoExt = pntr.filename
      var
        length = filenameNoExt.len
      if (length >= 5 and filenameNoExt[length-4] == '.'):
        filenameNoExt = filenameNoExt[0..length-5] # = '\0' # terminate, so ".DAT" is deleted from the filename

      imageFilename = "data/" & filenameNoExt & "/res" & $(resourceId) & "." & extension
      if useCustomLevelset == 0:
        #printf("loading (binary) mods",imageFilename)
        if fileExists(imageFilename):
          discard open(fp, locateFile(imageFilename), fmRead)

      else:
        if not(skipModDataFiles):
          var
            imageFilenameMod: string
          # before checking data/, first try mods/MODNAME/data/
          # snprintfCheck(imageFilenameMod, sizeof(imageFilenameMod), "mods/mods", modDataPath, imageFilename)
          imageFilenameMod = modDataPath & "/" & imageFilename
          #printf("loading (binary) mods",imageFilenameMod)
          discard open(fp, locateFile(imageFilenameMod), fmRead)
        if (fp == nil and not(skipNormalDataFiles)):
          discard open(fp, locateFile(imageFilename), fmRead)
      if (fp != nil):
        var
          buf: FileInfo
        buf = getFileInfo(fp)
        # if (fstat(fileno(fp), &buf) == 0):
        res = dataDirectory
        size = int32(buf.size)
        # getFileInfo can not fail in Nim, so no need for the following 4 lines
        # else:
        #   perror(imageFilename)
        #   fclose(fp)
        #   fp = NULL
    pntr = pntr.nextDat
  outFp = fp
  if (fp == nil):
    res = dataNone
    #    printf(" FAILED\n")
    #return NULL
  #...


# seg009:9F34
proc closeDat(pntr: DatType) =
  var
    prev: ptr DatType = addr(datChainPtr)
    curr: DatType = datChainPtr
  while (curr != nil):
    if (curr == pntr):
      prev[] = curr.nextDat
      if (curr.handle != nil):
        close(curr.handle)
      if (curr.datTable != nil):
        dealloc(curr.datTable)
      # not necessary, because DatType is ref object -> gc takes care
      # free(curr)
      return

    curr = curr.nextDat
    prev = addr(prev[].nextDat)

  # stub


# seg009:9F80
proc loadFromOpendatsAlloc(resource: int32, extension: string,
    outResult: ptr DataLocation, outSize: ptr int32): pointer =
  # stub
  #printf("id = modd\n",resource)
  var
    pntr: DatType
    res: DataLocation
    checksum: byte
    size: int32
    fp: File
  loadFromOpendatsMetadata(resource, extension, fp, res, addr(checksum), size, pntr)
  # if (outres != NULL) *outres = res
  # if (outSize != NULL) *outSize = size
  if outResult != nil:
    outResult[] = res
  if outSize != nil:
    outSize[] = size
  if (res == dataNone):
    return nil
  var
    area: pointer = alloc(size)
  #read(fd, area, size)
  # if (fread(area, size, 1, fp) != 1):
  if readBuffer(fp, area, size) != size:
    # write(stderr, "mods: mods, resource modd, size modd, failed: mods\n",
    #   _Func__, pntr.filename, resource,
    #   size, strerror(errno))
    write(stderr, pntr.filename & ", resource " & $(resource) & ", size " & $(
        size) & ", failed")
    dealloc(area)
    area = nil

  if (res == dataDirectory):
    close(fp)
  # /* XXX: check checksum */
  return area


# seg009:A172
proc loadFromOpendatsToArea(resource: int32, area: pointer, length: int32,
    extension: string): int32 =
  # stub
  #return 0
  var
    pntr: DatType
    res: DataLocation
    checksum: byte
    size: int32
    fp: File
  loadFromOpendatsMetadata(resource, extension, fp, res, addr(checksum), size, pntr)
  if (res == dataNone):
    return 0
  # if (fread(area, MIN(size, length), 1, fp) != 1):
  if readBuffer(fp, area, min(size, length)) != min(size, length):
    # fprintf(stderr, "mods: mods, resource modd, size modd, failed: mods\n",
    #   _Func__, pntr.filename, resource,
    #   size, strerror(errno))
    write(stderr, pntr.filename & ", resource " & $(resource) & ", size " & $(
        size) & ", failed")
    memset(area, 0, csize_t(min(size, length)))

  if (res == dataDirectory):
    close(fp)
  # /* XXX: check checksum */
  return 0


# SDL-specific implementations

proc rectToSdlrect(rect: var RectType, sdlrect: var Rect) =
  sdlrect.x = rect.left
  sdlrect.y = rect.top
  sdlrect.w = rect.right - rect.left
  sdlrect.h = rect.bottom - rect.top


proc method1BlitRect(targetSurface, sourceSurface: SurfaceType, targetRect,
    sourceRect: var RectType, blit: int32) =
  var
    srcRect: Rect
    destRect: Rect
  rectToSdlrect(sourceRect, srcRect)
  rectToSdlrect(targetRect, destRect)

  if (blit == ord(blitters0NoTransp)):
    # Disable transparency.
    if (setColorKey(sourceSurface, 0, 0) != 0):
      sdlperror("SDL_SetColorKey")
      quitPoP(1)
  else:
    # Enable transparency.
    if (setColorKey(sourceSurface, cint(true), 0) != 0):
      sdlperror("SDL_SetColorKey")
      quitPoP(1)
  if (blitSurface(sourceSurface, addr(srcRect), targetSurface, addr(
      destRect)) != 0):
    sdlperror("SDL_BlitSurface")
    quitPoP(1)


proc method3BlitMono(image: ImageType, xpos, ypos, blitter: int32,
    color: byte): ImageType =
  var
    w: int32 = image.w
    h: int32 = image.h
  if (setColorKey(image, cint(true), 0) != 0):
    sdlperror("SDL_SetColorKey")
    quitPoP(1)

  var
    coloredImage: Surface = convertSurfaceFormat(image,
        PIXELFORMAT_ARGB8888, 0)

  discard setSurfaceBlendMode(coloredImage, BLENDMODE_NONE)
  # /* Causes problems with SDL 2.0.5 (see #105)
  # if (setColorKey(coloredImage, cint(true), 0) != 0):
  #   sdlperror("SDL_SetColorKey")
  #   quitPoP(1)

  # */

  if (lockSurface(coloredImage) != 0):
    sdlperror("SDL_LockSurface")
    quitPoP(1)

  var
    paletteColor: RgbType = palette[color]
    rgbColor: uint32 = mapRGB(coloredImage.format, paletteColor.r shl 2,
        paletteColor.g shl 2, paletteColor.b shl 2) and 0xFFFFFF
    stride: int32 = coloredImage.pitch
  for y in 0 ..< h:
    var
      pixelPtr: ptr uint32 = cast[ptr uint32](addr(cast[ptr UncheckedArray[
          byte]](coloredImage.pixels)[stride * y]))
    for x in 0 ..< w:
      # set RGB but leave alpha
      pixelPtr[] = (pixelPtr[] and 0xFF000000'u32) or rgbColor
      #printf("pixel x=modd, y=modd, color = 0xmod8x\n", x, y, *pixelPtr)
      pixelPtr = cast[ptr uint32](addr(cast[ptr UncheckedArray[uint32]](
          pixelPtr)[1]))

  unlockSurface(coloredImage)

  var
    srcRect: Rect = Rect(x: 0, y: 0, w: image.w, h: image.h)
    destRect: Rect = Rect(x: xpos, y: ypos, w: image.w, h: image.h)

  discard setSurfaceBlendMode(coloredImage, BLENDMODE_BLEND)
  discard setSurfaceBlendMode(currentTargetSurface, BLENDMODE_BLEND)
  discard setSurfaceAlphaMod(coloredImage, 255)
  if (blitSurface(coloredImage, addr(srcRect), currentTargetSurface, addr(
      destRect)) != 0):
    sdlperror("SDL_BlitSurface")
    quitPoP(1)

  freeSurface(coloredImage)

  return image


# Workaround for a bug in SDL2 (before v2.0.4):
# https:#bugzilla.libsdl.org/showBug.cgi?id=2986
# SDL_FillRect onto a 24-bit surface swaps Red and Blue component

var
  RGB24BugChecked: bool = false
  RGB24BugAffected: bool

proc RGB24BugCheck(): bool =
  if not(RGB24BugChecked):
    var
      # Check if the bug occurs in this version of SDL.
      testSurface: Surface = createRGBSurface(0, 1, 1, 24, 0, 0, 0, 0)
    if (testSurface == nil):
      sdlperror("SDL_CreateSurface in RGB24BugCheck")
    # Fill with red.
    discard fillRect(testSurface, nil, mapRGB(testSurface.format, 0xFF, 0, 0))
    if (0 != lockSurface(testSurface)):
      sdlperror("SDL_LockSurface in RGB24BugCheck")
    # Read red component of pixel.
    RGB24BugAffected = (cast[ptr uint32](testSurface.pixels)[] and
        testSurface.format.Rmask) == 0
    unlockSurface(testSurface)
    freeSurface(testSurface)
    RGB24BugChecked = true
  return RGB24BugAffected


proc safeSDLFillRect(dst: Surface, rect: var Rect, color: uint32): int32 =
  if (dst.format.BitsPerPixel == 24 and RGB24BugCheck()):
    var
      internalColor: uint32
    # In the buggy version, SDL_FillRect swaps R and B, so we swap it once more.
    internalColor = ((color and 0xFF) shl 16) or (color and 0xFF00) or ((
        color and 0xFF0000) shr 16)
  return fillRect(dst, addr(rect), color)

# End of workaround.

proc method5Rect(rect: var RectType, blit: int32, color: byte): ptr RectType =
  var
    destRect: Rect
    paletteColor: RgbType = palette[color]
  rectToSdlrect(rect, destRect)
  when (USE_ALPHA):
    var
      rgbColor: uint32 = mapRGBA(currentTargetSurface.format,
          paletteColor.r shl 2, paletteColor.g shl 2, paletteColor.b shl 2, 0xFF)
  else:
    var
      rgbColor: uint32 = mapRGBA(currentTargetSurface.format,
          paletteColor.r shl 2, paletteColor.g shl 2, paletteColor.b shl 2,
          if color == 0: ALPHA_TRANSPARENT else: ALPHA_OPAQUE)
  if (safeSDLFillRect(currentTargetSurface, destRect, rgbColor) != 0):
    sdlperror("SDL_FillRect")
    quitPoP(1)
  return addr(rect)


proc drawRectWithAlpha(rect: var RectType, color, alpha: byte) =
  var
    destRect: Rect
    paletteColor: RgbType = palette[color]
  rectToSdlrect(rect, destRect)
  var
    rgbColor: uint32 = mapRGBA(overlaySurface.format, paletteColor.r shl 2,
        paletteColor.g shl 2, paletteColor.b shl 2, alpha)
  if (safeSDLFillRect(currentTargetSurface, destRect, rgbColor) != 0):
    sdlperror("SDL_FillRect")
    quitPoP(1)


proc drawRectContours(rect: var RectType, color: byte) =
  # TODO: handle 24 bit surfaces? (currently, 32 bit surface is assumed)
  if (currentTargetSurface.format.BitsPerPixel != 32):
    echo("drawRectContours: not implemented for " & $(
        int32(currentTargetSurface.format.BitsPerPixel)) & " bit surfaces\n")
    return

  var
    destRect: Rect
    paletteColor: RgbType = palette[color]
  rectToSdlrect(rect, destRect)
  var
    rgbColor: uint32 = mapRGBA(overlaySurface.format, paletteColor.r shl 2,
        paletteColor.g shl 2, paletteColor.b shl 2, 0xFF)
  if (lockSurface(currentTargetSurface) != 0):
    sdlperror("SDL_LockSurface")
    quitPoP(1)

  var
    bytesPerPixel: int32 = int32(currentTargetSurface.format.BytesPerPixel)
    pitch: int32 = currentTargetSurface.pitch
    pixels: ptr byte = cast[ptr byte](currentTargetSurface.pixels)
    xmin: int32 = min(destRect.x, currentTargetSurface.w)
    xmax: int32 = min(destRect.x + destRect.w, currentTargetSurface.w)
    ymin: int32 = min(destRect.y, currentTargetSurface.h)
    ymax: int32 = min(destRect.y + destRect.h, currentTargetSurface.h)
    row: ptr byte = addr(cast[ptr UncheckedArray[byte]](pixels)[ymin*pitch])
    pixel: ptr uint32 = cast[ptr uint32](addr(cast[ptr UncheckedArray[byte]](
        row)[xmin*bytesPerPixel]))
  for x in xmin ..< xmax:
    pixel[] = rgbColor
    pixel = addr(cast[ptr UncheckedArray[uint32]](pixel)[1])
  for y in ymin+1 ..< ymax-1:
    row = addr(cast[ptr UncheckedArray[byte]](row)[pitch])
    cast[ptr uint32](addr(cast[ptr UncheckedArray[byte]](row)[xmin *
        bytesPerPixel]))[] = rgbColor
    cast[ptr uint32](addr(cast[ptr UncheckedArray[byte]](row)[(xmax - 1) *
        bytesPerPixel]))[] = rgbColor

  pixel = cast[ptr uint32](addr(cast[ptr UncheckedArray[byte]](pixels)[(ymax -
      1) * pitch + xmin * bytesPerPixel]))
  for x in xmin ..< xmax:
    pixel[] = rgbColor
    pixel = addr(cast[ptr UncheckedArray[uint32]](pixel)[1])

  unlockSurface(currentTargetSurface)


proc blitXor(targetSurface: Surface, destRect: var Rect, image: Surface,
    srcRect: var Rect) =
  if (destRect.w != srcRect.w or destRect.h != srcRect.h):
    echo("blitXor: destRect and srcRect have different sizes")
    quitPoP(1)

  var
    helperSurface: Surface = createRGBSurface(0, destRect.w, destRect.h, 24,
        0xFF, 0xFF shl 8, 0xFF shl 16, 0)
  if (helperSurface == nil):
    sdlperror("SDL_CreateRGBSurface")
    quitPoP(1)

  var
    image24: Surface = convertSurface(image, helperSurface.format, 0)
  #SDL_CreateRGBSurface(0, srcRect.w, srcRect.h, 24, 0xFF, 0xFFshl8, 0xFFshl16, 0)
  if (image24 == nil):
    sdlperror("SDL_CreateRGBSurface")
    quitPoP(1)

  var
    destRect2: Rect = srcRect
  # Read what is currently where we want to draw the new image.
  if (blitSurface(targetSurface, addr(destRect), helperSurface, addr(
      destRect2)) != 0):
    sdlperror("SDL_BlitSurface")
    quitPoP(1)

  if (lockSurface(image24) != 0):
    sdlperror("SDL_LockSurface")
    quitPoP(1)

  if (lockSurface(helperSurface) != 0):
    sdlperror("SDL_LockSurface")
    quitPoP(1)

  var
    size: int32 = helperSurface.h * helperSurface.pitch
    pSrc: ptr byte = cast[ptr byte](image24.pixels)
    pDest: ptr byte = cast[ptr byte](helperSurface.pixels)

  # Xor the old area with the image.
  for i in 0 ..< size:
    pDest[] = pDest[] xor pSrc[]
    pSrc = addr(cast[ptr UncheckedArray[byte]](pSrc)[1])
    pDest = addr(cast[ptr UncheckedArray[byte]](pDest)[1])

  unlockSurface(image24)
  unlockSurface(helperSurface)
  # Put the new area in place of the old one.
  if (blitSurface(helperSurface, addr(srcRect), targetSurface, addr(
      destRect)) != 0):
    sdlperror("SDL_BlitSurface 2065")
    quitPoP(1)

  freeSurface(image24)
  freeSurface(helperSurface)


when (USE_COLORED_TORCHES):
  proc drawColoredTorch(color: int32, image: Surface, xpos, ypos: int32) =
    if (setColorKey(image, cint(true), 0) != 0):
      sdlperror("SDL_SetColorKey")
      quitPoP(1)

    var
      coloredImage: Surface = convertSurfaceFormat(image, PIXELFORMAT_ARGB8888, 0)
    discard setSurfaceBlendMode(coloredImage, BLENDMODE_NONE)

    if (lockSurface(coloredImage) != 0):
      sdlperror("SDL_LockSurface")
      quitPoP(1)

    var
      w: int32 = coloredImage.w
      h: int32 = coloredImage.h
      iRed: int32 = ((color shr 4) and 3) * 85
      iGreen: int32 = ((color shr 2) and 3) * 85
      iBlue: int32 = ((color shr 0) and 3) * 85
      oldColor: uint32 = mapRGB(coloredImage.format, 0xFC, 0x84, 0x00) and
          0xFFFFFF # the orange in the flame
      newColor: uint32 = mapRGB(coloredImage.format, byte(iRed), byte(iGreen),
          byte(iBlue)) and 0xFFFFFF
      stride: int32 = coloredImage.pitch
    for y in 0 ..< h:
      var
        pixelPtr: ptr uint32 = cast[ptr uint32](addr(cast[ptr UncheckedArray[
            byte]](coloredImage.pixels)[stride * y]))
      for x in 0 ..< w:
        if ((pixelPtr[] and 0xFFFFFF) == oldColor):
          # set RGB but leave alpha
          pixelPtr[] = (pixelPtr[] and 0xFF000000'u32) or newColor
        pixelPtr = addr(cast[ptr UncheckedArray[uint32]](pixelPtr)[1])
    unlockSurface(coloredImage)

    discard method6BlitImgToScr(coloredImage, xpos, ypos, ord(blitters0NoTransp))
    freeSurface(coloredImage)

proc method6BlitImgToScr(image: ImageType, xpos, ypos, blit: int32): ImageType =
  if (image == nil):
    echo("method6BlitImgToScr: image == nil")
    #quit(1)
    return nil

  if (blit == ord(blitters9Black)):
    discard method3BlitMono(image, xpos, ypos, ord(blitters9Black), 0)
    return image

  var
    srcRect: Rect = Rect(x: 0, y: 0, w: image.w, h: image.h)
    destRect: Rect = Rect(x: xpos, y: ypos, w: image.w, h: image.h)

  if (blit == ord(blitters3Xor)):
    blitXor(currentTargetSurface, destRect, image, srcRect)
    return image

  when (USE_COLORED_TORCHES):
    if (blit >= ord(blittersColoredFlame) and blit <= ord(
        blittersColoredFlameLast)):
      drawColoredTorch(blit - ord(blittersColoredFlame), image, xpos, ypos)
      return image

  discard setSurfaceBlendMode(image, BLENDMODE_NONE)
  discard setSurfaceAlphaMod(image, 255)

  if (blit == ord(blitters0NoTransp)):
    discard setColorKey(image, cint(false), 0)
  else:
    discard setColorKey(image, cint(true), 0)

  if (blitSurface(image, addr(srcRect), currentTargetSurface, addr(destRect)) != 0):
    sdlperror("SDL_BlitSurface 2247")
    quitPoP(1)

  if (setSurfaceAlphaMod(image, 0) != 0):
    sdlperror("SDL_SetAlpha")
    quitPoP(1)
  return image


when not(USE_COMPAT_TIMER):
  var
    fps: int32 = 60
    millisecondsPerTick: float = (1000.0 / 60.0)
    timerLastCounter: array[NUM_TIMERS, uint64]
var
  waitTime: array[NUM_TIMERS, int]


when (USE_COMPAT_TIMER):
  proc timerCallback(interval: uint32, param: pointer): uint32 {.cdecl.} =
    var
      event: Event
    memset(addr(event), 0, csize_t(sizeof(event)))
    event.kind = USEREVENT
    event.user.code = ord(userevent_TIMER)
    event.user.data1 = param
    discard pushEvent(addr(event))
    return interval

#endif

proc resetTimer(timerIndex: int32) =
  when not(USE_COMPAT_TIMER):
    timerLastCounter[timerIndex] = getPerformanceCounter()
  else:
    discard

proc getTicksPerSec(timerIndex: int32): float64 =
  return float64(fps) / float64(waitTime[timerIndex])

proc recalculateFeatherFallTimer(previousTicksPerSecond,
    ticksPerSecond: float64) =
  if ((isFeatherFall <= uint16(max(previousTicksPerSecond, ticksPerSecond)) or
    previousTicksPerSecond == ticksPerSecond)):
    return

  # there are more ticks per second in base mode vs fight mode so
  # feather fall length needs to be recalculated
  isFeatherFall = uint16(float64(isFeatherFall) / previousTicksPerSecond * ticksPerSecond)

proc setTimerLength(timerIndex, length: int32) =
  if (fixes.fixQuicksaveDuringFeather == 0'u8):
    waitTime[timerIndex] = length
    return

  if (isFeatherFall == 0'u16 or
      waitTime[timerIndex] < custom.baseSpeed or
      waitTime[timerIndex] > custom.fightSpeed):
    waitTime[timerIndex] = length
    return

  let
    previousTicksPerSecond: float64 = getTicksPerSec(timerIndex)
  waitTime[timerIndex] = length
  let
    ticksPerSecond: float64 = getTicksPerSec(timerIndex)
  recalculateFeatherFallTimer(previousTicksPerSecond, ticksPerSecond)


proc startTimer(timerIndex, length: int32) =
  when (USE_REPLAY):
    if (replaying and skippingReplay) != 0:
      return
  when not(USE_COMPAT_TIMER):
    timerLastCounter[timerIndex] = getPerformanceCounter()
  waitTime[timerIndex] = length


proc toggleFullscreen() =
  var
    flags: uint32 = getWindowFlags(window)
  if (flags and WINDOW_FULLSCREEN_DESKTOP) != 0:
    discard setWindowFullscreen(window, 0)
    discard showCursor(ENABLE)
  else:
    discard setWindowFullscreen(window, WINDOW_FULLSCREEN_DESKTOP)
    discard showCursor(DISABLE)


var
  ignoreTab: bool = false

proc processEvents() =
  # Process all events in the queue.
  # Previously, this procedure would wait for *one* event and process it, then return.
  # Much like the x86 HLT instruction.
  # (We still want to process all events in the queue. For instance, there might be
  # simultaneous SDL2 KEYDOWN and TEXTINPUT events.)
  var
    event: Event
  while (pollEvent(addr(event)) == 1): # while there are still events to be processed
    block pollBlock:
      case (event.kind)
      of KEYDOWN:
        var
          modifier: int32 = int32(event.key.keysym.mods)
          scancode: int32 = ord(event.key.keysym.scancode)
        # Handle these separately, so they won't interrupt things that are usually interrupted by a keypress. (pause, cutscene)
        if (scancode == ord(SCANCODE_GRAVE)):
          when(USE_FAST_FORWARD):
            initTimer(BASE_FPS * FAST_FORWARD_RATIO) # fast-forward on
            audioSpeed = FAST_FORWARD_RATIO
          break pollBlock
        when (USE_SCREENSHOT):
          if (scancode == ord(SCANCODE_F12)):
            if (modifier and ord(KMOD_SHIFT)) != 0:
              saveLevelScreenshot((modifier and ord(KMOD_CTRL)) != 0)
            else:
              saveScreenshot()
            break pollBlock
        when (USE_MENU):
          if (escapeKeySuppressed and
            (scancode == ord(SCANCODE_BACKSPACE) or ((enablePauseMenu != 0) and
                scancode == ord(SCANCODE_ESCAPE)))
          ):
            break pollBlock # Prevent repeated keystrokes opening/closing the menu as long as the key is held down.
        if ((modifier and ord(KMOD_ALT)) != 0 and
            scancode == ord(SCANCODE_RETURN)):
          # Only if the Enter key was pressed down right now.
          if (keyStates[scancode] == 0):
            # Alt-Enter: toggle fullscreen mode
            toggleFullscreen()
            keyStates[scancode] = 1
        else:
          keyStates[scancode] = 1
          case (scancode)
          # Keys that are ignored by themselves:
          of ord(SCANCODE_LCTRL):
            discard
          of ord(SCANCODE_LSHIFT):
            discard
          of ord(SCANCODE_LALT):
            discard
          of ord(SCANCODE_LGUI):
            discard
          of ord(SCANCODE_RCTRL):
            discard
          of ord(SCANCODE_RSHIFT):
            discard
          of ord(SCANCODE_RALT):
            discard
          of ord(SCANCODE_RGUI):
            discard
          of ord(SCANCODE_CAPSLOCK):
            discard
          of ord(SCANCODE_SCROLLLOCK):
            discard
          of ord(SCANCODE_NUMLOCKCLEAR):
            discard
          of ord(SCANCODE_APPLICATION):
            discard
          of ord(SCANCODE_PRINTSCREEN):
            discard
          of ord(SCANCODE_VOLUMEUP):
            discard
          of ord(SCANCODE_VOLUMEDOWN):
            discard
          # Why are there two mute key codes?
          of ord(SCANCODE_MUTE):
            discard
          of ord(SCANCODE_AUDIOMUTE):
            discard
          of ord(SCANCODE_PAUSE):
            discard
          else:
            # If Alt is held down from Alt+Tab: ignore it until it's released.
            if (scancode == ord(SCANCODE_TAB) and ignoreTab):
              discard
            else:
              lastKeyScancode = scancode
              if (modifier and ord(KMOD_SHIFT)) != 0:
                lastKeyScancode = lastKeyScancode or ord(WITH_SHIFT)
              if (modifier and ord(KMOD_CTRL)) != 0:
                lastKeyScancode = lastKeyScancode or ord(WITH_CTRL)
              if (modifier and ord(KMOD_ALT)) != 0:
                lastKeyScancode = lastKeyScancode or ord(WITH_ALT)

          when (USE_AUTO_INPUT_MODE):
            case (scancode)
            # Keys that are used for keyboard control:
            of ord(SCANCODE_LSHIFT), ord(SCANCODE_RSHIFT), ord(
                SCANCODE_LEFT), ord(SCANCODE_RIGHT), ord(SCANCODE_UP), ord(
                SCANCODE_DOWN), ord(SCANCODE_CLEAR), ord(SCANCODE_HOME),
                ord(SCANCODE_PAGEUP), ord(SCANCODE_KP2), ord(SCANCODE_KP4),
                ord(SCANCODE_KP5), ord(SCANCODE_KP6), ord(SCANCODE_KP7),
                ord(SCANCODE_KP8), ord(SCANCODE_KP9):
              if isKeyboardMode == 0:
                isKeyboardMode = 1
                isJoystMode = 0
            else:
              discard
      of KEYUP:
        # If Alt was held down from Alt+Tab but now it's released: stop ignoring Tab.
        if (event.key.keysym.scancode == SCANCODE_TAB and ignoreTab):
          ignoreTab = false
        if (event.key.keysym.scancode == SCANCODE_GRAVE):
          when(USE_FAST_FORWARD):
            initTimer(BASE_FPS) # fast-forward off
            audioSpeed = 1
          break pollBlock
        keyStates[ord(event.key.keysym.scancode)] = 0
        when (USE_MENU):
          # Prevent repeated keystrokes opening/closing the menu as long as the key is held down.
          if (event.key.keysym.scancode == SCANCODE_BACKSPACE or
              event.key.keysym.scancode == SCANCODE_ESCAPE):
            escapeKeySuppressed = false
      of CONTROLLERAXISMOTION:
        if (ord(event.caxis.axis) < 6):
          joyAxis[ord(event.caxis.axis)] = event.caxis.value
          when (USE_AUTO_INPUT_MODE):
            if ((isJoystMode == 0) and (event.caxis.value >=
                joystickThreshold or event.caxis.value <= -joystickThreshold)):
              isJoystMode = 1
              isKeyboardMode = 0
      of CONTROLLERBUTTONDOWN:
        when (USE_AUTO_INPUT_MODE):
          if isJoystMode == 0:
            isJoystMode = 1
            isKeyboardMode = 0
        case (event.cbutton.button)
        of CONTROLLER_BUTTON_DPAD_LEFT:
          joyHatStates[0] = -1 # left
        of CONTROLLER_BUTTON_DPAD_RIGHT:
          joyHatStates[0] = 1 # right
        of CONTROLLER_BUTTON_DPAD_UP:
          joyHatStates[1] = -1 # up
        of CONTROLLER_BUTTON_DPAD_DOWN:
          joyHatStates[1] = 1 # down
        of CONTROLLER_BUTTON_A:
          joy_AYButtonsState = 1 # /*** A (down) ***/
        of CONTROLLER_BUTTON_Y:
          joy_AYButtonsState = -1 # /*** Y (up) ***/
        of CONTROLLER_BUTTON_X:
          joy_XButtonState = 1 # /*** X (shift) ***/
        of CONTROLLER_BUTTON_B:
          joy_BButtonState = 1 # /*** B (unused) ***/
        of CONTROLLER_BUTTON_START, CONTROLLER_BUTTON_BACK:
          when (USE_MENU):
            lastKeyScancode = ord(SCANCODE_BACKSPACE) # /*** bring up pause menu ***/
          else:
            lastKeyScancode = ord(SCANCODE_ESCAPE) #  /*** back (pause game) ***/
        else:
          discard
      of CONTROLLERBUTTONUP:
        case (event.cbutton.button)
        of CONTROLLER_BUTTON_DPAD_LEFT:
          joyHatStates[0] = 0 # left
        of CONTROLLER_BUTTON_DPAD_RIGHT:
          joyHatStates[0] = 0 # right
        of CONTROLLER_BUTTON_DPAD_UP:
          joyHatStates[1] = 0 # up
        of CONTROLLER_BUTTON_DPAD_DOWN:
          joyHatStates[1] = 0 # down
        of CONTROLLER_BUTTON_A:
          joy_AYButtonsState = 0 # /*** A (down) ***/
        of CONTROLLER_BUTTON_Y:
          joy_AYButtonsState = 0 # /*** Y (up) ***/
        of CONTROLLER_BUTTON_X:
          joy_XButtonState = 0 # /*** X (shift) ***/
        of CONTROLLER_BUTTON_B:
          joy_BButtonState = 0 # /*** B (unused) ***/
        else:
          discard
      of JOYBUTTONDOWN, JOYBUTTONUP, JOYAXISMOTION:
        # Only handle the event if the joystick is incompatible with the SDL_GameController interface.
        # (Otherwise it will interfere with the normal action of the SDL_GameController API.)
        if usingSdlJoystickInterface == 0:
          break pollBlock
        if (event.kind == JOYAXISMOTION):
          if (event.jaxis.axis == SDL_JOYSTICK_X_AXIS):
            joyAxis[ord(CONTROLLER_AXIS_LEFTX)] = event.jaxis.value
          elif (event.jaxis.axis == SDL_JOYSTICK_Y_AXIS):
            joyAxis[ord(CONTROLLER_AXIS_LEFTY)] = event.jaxis.value

          # Disregard SDL_JOYAXISMOTION events within joystick 'dead zone'
          var
            joyX: int32 = joyAxis[ord(CONTROLLER_AXIS_LEFTX)]
            joyY: int32 = joyAxis[ord(CONTROLLER_AXIS_LEFTY)]
          if (dword(joyX*joyX) + dword(joyY*joyY) < dword(
              joystickThreshold*joystickThreshold)):
            break pollBlock
        when (USE_AUTO_INPUT_MODE):
          if isJoystMode == 0:
            isJoystMode = 1
            isKeyboardMode = 0
        if (event.kind == JOYBUTTONDOWN):
          if (event.jbutton.button == SDL_JOYSTICK_BUTTON_Y):
            joyAYButtonsState = -1 # Y (up)
          elif (event.jbutton.button == SDL_JOYSTICK_BUTTON_X):
            joyXButtonState = -1 # X (shift)
        elif (event.kind == JOYBUTTONUP):
          if (event.jbutton.button == SDL_JOYSTICK_BUTTON_Y):
            joyAYButtonsState = 0 # Y (up)
          elif (event.jbutton.button == SDL_JOYSTICK_BUTTON_X):
            joyXButtonState = 0 # X (shift)
      of TEXTINPUT:
        lastTextInput = event.text.text[0] # UTF-8 formatted char text input
      of WINDOWEVENT:
        # In case the user switches away while holding a key: do as if all keys were released.
        # (DOSBox does the same.)

        #[ not implemented in SDL2 for now

        if ((event.active.state & SDL_APPINPUTFOCUS) and event.active.gain == 0):
          memset(keyStates, 0, sizeof(keyStates))

        # Note: event.active.state can contain multiple flags or'ed.
        # If the game is in full screen, and I switch away (alt-tab) and back, most of the screen will be black, until it is redrawn.
        if ((event.active.state & SDL_APPACTIVE) and event.active.gain == 1):
          updateScreen()
        ]#
        case (event.window.event)
        of WINDOWEVENT_SIZE_CHANGED:
          windowResized()
          updateScreen()
        #case SDL_WINDOWEVENT_MOVED:
        #case SDL_WINDOWEVENT_RESTORED:
        of WINDOWEVENT_EXPOSED:
          updateScreen()
        of WINDOWEVENT_FOCUS_GAINED:
          # Fix for this bug: When playing back a recording, Alt+Tabbing back to SDLPoP stops the replay if Alt is released before Tab.
          # If Alt is held down from Alt+Tab: ignore it until it's released.
          var
            state: ptr array[512, uint8] = getKeyboardState(nil)
          if (state[ord(SCANCODE_TAB)]) != 0:
            ignoreTab = true
        else:
          discard
      of USEREVENT:
        if event.user.code == ord(userevent_TIMER): # and event.user.data1 == (void*)timerIndex):
          when (USE_COMPAT_TIMER):
            for index in 0 ..< NUM_TIMERS:
              if (waitTime[index] > 0):
                dec(waitTime[index])
          else:
            discard
        elif (event.user.code == ord(userevent_SOUND)):
          #soundTimer = 0
          #stopSounds()
          discard
      of MOUSEBUTTONDOWN:
        when (USE_MENU):
          case(event.button.button)
          of BUTTON_LEFT:
            if isMenuShown == 0:
              lastKeyScancode = ord(SCANCODE_BACKSPACE)
            else:
              mouseClicked = true
          of BUTTON_RIGHT, BUTTON_X1: # 'Back' button (on mice that have these extra buttons).
            mouseButtonClickedRight = true
          else:
            discard
        else:
          discard
      of MOUSEWHEEL:
        when(USE_MENU):
          if (isMenuShown) != 0:
            menuControlScrollY = -event.wheel.y
        else:
          discard
      of QUIT:
        when (USE_MENU):
          if (isMenuShown) != 0:
            menuWasClosed()
        quitPoP(0)
      else:
        discard


proc idle() =
  processEvents()
  updateScreen()


proc doSimpleWait(timerIndex: int32) =
  when (USE_REPLAY):
    if ((replaying and skippingReplay) or isValidateMode) != 0:
      return
  updateScreen()
  while hasTimerStopped(timerIndex) == 0:
    delay(1)
    processEvents()


var
  word1D63A: word = 1
proc doWait(timerIndex: int32): int32 =
  when (USE_REPLAY):
    if ((replaying and skippingReplay) or isValidateMode) != 0:
      return 0
  updateScreen()
  while hasTimerStopped(timerIndex) == 0:
    delay(1)
    processEvents()
    var
      key: int32 = doPaused()
    if (key != 0 and (word1D63A != 0 or key == 0x1B)):
      return 1

  return 0


when (USE_COMPAT_TIMER):
  var
    globalTimer: TimerID = 0
# seg009:78E9
proc initTimer(frequency: int32) =
  perfFrequency = getPerformanceFrequency()
  when not(USE_COMPAT_TIMER):
    fps = frequency
    millisecondsPerTick = 1000.0 / float(fps)
    perfCountersPerTick = perfFrequency div uint64(fps)
    millisecondsPerCounter = 1000.0 / float(perfFrequency)
  else:
    if (globalTimer != 0):
      if not(removeTimer(globalTimer)):
        sdlperror("SDL_RemoveTimer")
    globalTimer = addTimer(uint32(1000 div frequency), timerCallback, nil)
    if (globalTimer == 0):
      sdlperror("SDL_AddTimer")
      quitPoP(1)


# seg009:35F6
proc setClipRect(rect: var RectType) =
  var
    clipRect: Rect
  rectToSdlrect(rect, clipRect)
  discard setClipRect(currentTargetSurface, addr(clipRect))


# seg009:365C
proc resetClipRect() =
  discard setClipRect(currentTargetSurface, nil)


# seg009:1983
proc setBgAttr(vgaPalIndex, hcPalIndex: int32) =
  # stub
  when (USE_FLASH):
    #palette[vgaPalIndex] = vgaPalette[hcPalIndex]
    if enableFlash == 0:
      return
    if (vgaPalIndex == 0):
      #[
      if (SDL_SetAlpha(offscreenSurface, SDL_SRCALPHA, 0) != 0):
        sdlperror("SDL_SetAlpha")
        quitPoP(1)

      ]#
      # Make the black pixels transparent.
      if (setColorKey(offscreenSurface, cint(true), 0) != 0): # SDL_SRCCOLORKEY old
        sdlperror("SDL_SetColorKey")
        quitPoP(1)

      var
        rect: Rect = Rect(x: 0, y: 0, w: 0, h: 0)
      rect.w = offscreenSurface.w
      rect.h = offscreenSurface.h
      var
        paletteColor: RgbType = palette[hcPalIndex]
        rgbColor: uint32 = mapRGB(onscreenSurface.format, paletteColor.r shl 2,
            paletteColor.g shl 2, paletteColor.b shl 2) #& 0xFFFFFF*/
                                                        #SDL_UpdateRect(onscreenSurface_, 0, 0, 0, 0)
                                                        # First clear the screen with the color of the flash.
      if (safeSDLFillRect(onscreenSurface, rect, rgbColor) != 0):
        sdlperror("SDL_FillRect")
        quitPoP(1)
      #SDL_UpdateRect(onscreenSurface_, 0, 0, 0, 0)
      if (upsideDown) != 0:
        flipScreen(offscreenSurface)
      # Then draw the offscreen image onto it.
      if (blitSurface(offscreenSurface, addr(rect), onscreenSurface, addr(
          rect)) != 0):
        sdlperror("SDL_BlitSurface")
        quitPoP(1)

      when (USE_LIGHTING):
        if (hcPalIndex == 0):
          updateLighting(rectTop)
      if (upsideDown) != 0:
        flipScreen(offscreenSurface)

      # And show it!
      # updateScreen()
      # Give some time to show the flash.
      #SDL_Flip(onscreenSurface_)
      # if (hcPalIndex != 0) SDL_Delay(2*(1000/60))
      #SDL_Flip(onscreenSurface_)
      #[
      if (SDL_SetAlpha(offscreenSurface, 0, 0) != 0):
        sdlperror("SDL_SetAlpha")
        quitPoP(1)
      ]#
      if (setColorKey(offscreenSurface, 0, 0) != 0):
        sdlperror("SDL_SetColorKey")
        quitPoP(1)
  else:
    discard


# seg009:07EB
proc offset4RectAdd(dest, source: var RectType, dLeft, dTop, dRight,
    dBottom: int32): RectType =
  dest = source
  dest.left += int16(dLeft)
  dest.top += int16(dTop)
  dest.right += int16(dRight)
  dest.bottom += int16(dBottom)
  return dest


# seg009:3AA5
proc offset2Rect(dest, source: var RectType, deltaX, deltaY: int32): RectType =
  dest.top = source.top + int16(deltaY)
  dest.left = source.left + int16(deltaX)
  dest.bottom = source.bottom + int16(deltaY)
  dest.right = source.right + int16(deltaX)
  return dest


when (USE_FADE):
  # seg009:19EF
  proc fadeIn2(sourceSurface: Surface, whichRows: int32) =
    var
      paletteBuffer: PaletteFadeType
    if (graphicsMode == ord(gmMcgaVga)):
      paletteBuffer = makePalBufferFadein(sourceSurface, whichRows, 2)
      while (fadeInFrame(paletteBuffer) == 0):
        processEvents() # modified
        discard doPaused()
      palRestoreFreeFadein(paletteBuffer)
    else:
      # ...
      discard


  # seg009:1A51
  proc makePalBufferFadein(sourceSurface: Surface, whichRows,
      waitTime: int32): PaletteFadeType =
    var
      paletteBuffer: PaletteFadeType
      currRow: word
      var8: word
      currRowMask: word
    paletteBuffer = new(PaletteFadeType)
    paletteBuffer.whichRows = uint16(whichRows)
    paletteBuffer.waitTime = uint16(waitTime)
    paletteBuffer.fadePos = 0x40
    paletteBuffer.procRestoreFree = palRestoreFreeFadein
    paletteBuffer.procFadeFrame = fadeInFrame
    readPalette256(paletteBuffer.originalPal)
    memcpy(addr(paletteBuffer.fadedPal[0]), addr(paletteBuffer.originalPal[0]),
        csize_t(sizeof(paletteBuffer.fadedPal)))
    var8 = 0
    currRow = 0
    currRowMask = 1
    # for (currRow = 0, currRowMask = 1; currRow < 0x10; ++currRow, currRowMaskshl=1)
    while currRow < 0x10:
      if (whichRows and int32(currRowMask)) != 0:
        memset(addr(paletteBuffer.fadedPal[currRow shl 4]), 0, csize_t(sizeof(
            array[0x10, RgbType])))
        setPalArr(int(currRow shl 4), 0x10, nil, int32((var8 and 3) == 0))
        inc(var8)
      inc(currRow)
      currRowMask = currRowMask shl 1


    #method1BlitRect(onscreenSurface_, sourceSurface, &screenRect, &screenRect, 0)
    # for RGB
    #method5Rect(&screenRect, 0, 0)
    return paletteBuffer


  # seg009:1B64
  proc palRestoreFreeFadein(paletteBuffer: PaletteFadeType) =
    setPal256(paletteBuffer.originalPal)
    # freeFar(paletteBuffer)
    # for RGB
    method1BlitRect(onscreenSurface, offscreenSurface, screenRect, screenRect, 0)


  # seg009:1B88
  proc fadeInFrame(paletteBuffer: PaletteFadeType): int32 =
    var
      fadedPalPtr: ptr UncheckedArray[RgbType]
      start: word
      column: word
      originalPalPtr: ptr UncheckedArray[RgbType]
      currentRowMask: word
  #  void* var12
    startTimer(ord(timer1), int32(paletteBuffer.waitTime)) # too slow?
  
    #printf("start ticks = modu\n",SDL_GetTicks())
    dec(paletteBuffer.fadePos)
    start = 0
    currentRowMask = 1
    while start < 0x100:
      if (paletteBuffer.whichRows and currentRowMask) != 0:
        #var12 = paletteBuffer.
        originalPalPtr = cast[ptr UncheckedArray[RgbType]](addr(
            paletteBuffer.originalPal[start]))
        fadedPalPtr = cast[ptr UncheckedArray[RgbType]](addr(
            paletteBuffer.fadedPal[start]))
        column = 0
        while column < 0x10:
          if (originalPalPtr[column].r > paletteBuffer.fadePos):
            inc(fadedPalPtr[column].r)

          if (originalPalPtr[column].g > paletteBuffer.fadePos):
            inc(fadedPalPtr[column].g)

          if (originalPalPtr[column].b > paletteBuffer.fadePos):
            inc(fadedPalPtr[column].b)
          inc(column)
      start += 0x10
      currentRowMask = currentRowMask shl 1

    column = 0
    start = 0
    currentRowMask = 1
    while start < 0x100:
      if (paletteBuffer.whichRows and currentRowMask) != 0:
        setPalArr(int(start), 0x10, addr(paletteBuffer.fadedPal[start]), int32((
            column and 3) == 0))
        inc(column)
      start += 0x10
      currentRowMask = currentRowMask shl 1

    var
      h: int32 = offscreenSurface.h
    if (lockSurface(onscreenSurface) != 0):
      sdlperror("SDL_LockSurface")
      quitPoP(1)

    if (lockSurface(offscreenSurface) != 0):
      sdlperror("SDL_LockSurface")
      quitPoP(1)

    var
      onStride: int32 = onscreenSurface.pitch
      offStride: int32 = offscreenSurface.pitch
      fadePos: int32 = int32(paletteBuffer.fadePos)
    for y in 0 ..< h:
      var
        onPixelPtr: ptr byte = addr(cast[ptr UncheckedArray[byte]](
            onscreenSurface.pixels)[onStride * y])
        offPixelPtr: ptr byte = addr(cast[ptr UncheckedArray[byte]](
            offscreenSurface.pixels)[offStride * y])
      for x in 0 ..< onStride:
        #if (*offPixelPtr > paletteBuffer.fadePos) *pixelPtr += 4
        var
          v: int32 = offPixelPtr[] - fadePos*4
        if (v < 0):
          v = 0
        onPixelPtr[] = byte(v)
        onPixelPtr = addr(cast[ptr UncheckedArray[byte]](onPixelPtr)[1])
        offPixelPtr = addr(cast[ptr UncheckedArray[byte]](offPixelPtr)[1])


    unlockSurface(onscreenSurface)
    unlockSurface(offscreenSurface)

    #SDL_UpdateRect(onscreenSurface_, 0, 0, 0, 0) # debug

    doSimpleWait(1) # can interrupt fading of cutscene
    #doWait(timer1) # can interrupt fading of main title
    #printf("end ticks = modu\n",SDL_GetTicks())
    return int32(paletteBuffer.fadePos == 0)


  # seg009:1CC9
  proc fadeOut2(rows: int32) =
    var
      paletteBuffer: PaletteFadeType
    if (graphicsMode == ord(gmMcgaVga)):
      paletteBuffer = makePalBufferFadeout(rows, 2)
      while (fadeOutFrame(paletteBuffer) == 0):
        processEvents() # modified
        discard doPaused()
      palRestoreFreeFadeout(paletteBuffer)
    else:
      # ...
      discard



  # seg009:1D28
  proc makePalBufferFadeout(whichRows, waitTime: int32): PaletteFadeType =
    var
      paletteBuffer: PaletteFadeType
    paletteBuffer = new(PaletteFadeType)
    paletteBuffer.whichRows = word(whichRows)
    paletteBuffer.waitTime = word(waitTime)
    paletteBuffer.fadePos = 00 # modified
    paletteBuffer.procRestoreFree = palRestoreFreeFadeout
    paletteBuffer.procFadeFrame = fadeOutFrame
    readPalette256(paletteBuffer.originalPal)
    memcpy(addr(paletteBuffer.fadedPal[0]), addr(paletteBuffer.originalPal[0]),
        csize_t(paletteBuffer.fadedPal.len * sizeof(RgbType)))
    # for RGB
    method1BlitRect(onscreenSurface, offscreenSurface, screenRect, screenRect, 0)
    return paletteBuffer


  # seg009:1DAF
  proc palRestoreFreeFadeout(paletteBuffer: PaletteFadeType) =
    var
      surface: Surface
    surface = currentTargetSurface
    currentTargetSurface = onscreenSurface
    drawRect(screenRect, 0)
    currentTargetSurface = surface
    setPal256(paletteBuffer.originalPal)
    # free(paletteBuffer)
    # for RGB
    discard method5Rect(screenRect, 0, 0)


  # seg009:1DF7
  proc fadeOutFrame(paletteBuffer: PaletteFadeType): int32 =
    var
      fadedPalPtr: ptr UncheckedArray[RgbType]
      start: word
      var8: word
      column: word
      currentRowMask: word
      currColorPtr: ptr byte
    var8 = 1
    inc(paletteBuffer.fadePos) # modified
    startTimer(ord(timer1), int32(paletteBuffer.waitTime)) # too slow?
    start = 0
    currentRowMask = 1
    while start < 0x100:
      if (paletteBuffer.whichRows and currentRowMask) != 0:
        #var12 = paletteBuffer.
        #originalPalPtr = paletteBuffer.originalPal + start
        fadedPalPtr = cast[ptr UncheckedArray[RgbType]](addr (
            paletteBuffer.fadedPal[start]))
        column = 0
        while column < 0x10:
          currColorPtr = addr(fadedPalPtr[column].r)
          if (currColorPtr[] != 0):
            dec(currColorPtr[])
            var8 = 0

          currColorPtr = addr(fadedPalPtr[column].g)
          if (currColorPtr[] != 0):
            dec(currColorPtr[])
            var8 = 0

          currColorPtr = addr(fadedPalPtr[column].b)
          if (currColorPtr[] != 0):
            dec(currColorPtr[])
            var8 = 0
          inc(column)
      start += 0x10
      currentRowMask = currentRowMask shl 1

    column = 0
    start = 0
    currentRowMask = 1
    while start < 0x100:
      if (paletteBuffer.whichRows and currentRowMask) != 0:
        setPalArr(int(start), 0x10, addr(paletteBuffer.fadedPal[start]), int32((
            column and 3) == 0))
        inc(column)
      start += 0x10
      currentRowMask = currentRowMask shl 1

    var
      h: int32 = offscreenSurface.h
    if (lockSurface(onscreenSurface) != 0):
      sdlperror("SDL_LockSurface")
      quitPoP(1)

    if (lockSurface(offscreenSurface) != 0):
      sdlperror("SDL_LockSurface")
      quitPoP(1)

    var
      onStride: int32 = onscreenSurface.pitch
      offStride: int32 = offscreenSurface.pitch
      fadePos: int32 = int32(paletteBuffer.fadePos)
    for y in 0 ..< h:
      var
        onPixelPtr: ptr byte = addr(cast[ptr UncheckedArray[byte]](
            onscreenSurface.pixels)[onStride * y])
        offPixelPtr: ptr byte = addr(cast[ptr UncheckedArray[byte]](
            offscreenSurface.pixels)[offStride * y])
      for x in 0 ..< onStride:
        var
          v: int32 = offPixelPtr[] - fadePos*4
        if (v < 0):
          v = 0
        onPixelPtr[] = byte(v)
        onPixelPtr = addr(cast[ptr UncheckedArray[byte]](onPixelPtr)[1])
        offPixelPtr = addr(cast[ptr UncheckedArray[byte]](offPixelPtr)[1])

      unlockSurface(onscreenSurface)
      unlockSurface(offscreenSurface)

      doSimpleWait(ord(timer1)) # can interrupt fading of cutscene
      return int32(var8)


  # seg009:1F28
  proc readPalette256(target: var openArray[RgbType]) =
    for i in 0 ..< 256:
      target[i] = palette[i]

  # seg009:1F5E
  proc setPal256(source: var openArray[RgbType]) =
    for i in 0 ..< 256:
      palette[i] = source[i]


proc setChtabPalette(chtab: ChtabType, colors: ptr byte, nColors: int32) =
  if (chtab != nil):
    var
      scolors: ptr UncheckedArray[Color] = cast[ptr UncheckedArray[Color]](
          alloc(nColors*sizeof(Color)))
      internalColors: ptr byte = colors
    #printf("scolors\n",i)
    for i in 0 ..< nColors:
      #printf("i=modd\n",i)
      scolors[i].r = internalColors[] shl 2
      internalColors = addr(cast[ptr UncheckedArray[byte]](internalColors)[1])
      scolors[i].g = internalColors[] shl 2
      internalColors = addr(cast[ptr UncheckedArray[byte]](internalColors)[1])
      scolors[i].b = internalColors[] shl 2
      internalColors = addr(cast[ptr UncheckedArray[byte]](internalColors)[1])
      scolors[i].a = ALPHA_OPAQUE # the SDL2 SDL_Color struct has an alpha component

    # Color 0 of the palette data is not used, it is replaced by the background color.
    # Needed for correct alternate colors (v1.3) of level 8.
    scolors[0].r = 0
    scolors[0].g = 0
    scolors[0].b = 0

    #printf("setcolors\n",i)
    for i in 0 ..< int32(chtab.nImages):
      #printf("i=modd\n",i)
      var
        currentImage: ImageType = chtab.images[i]
      if (currentImage != nil):
        var
          nColorsToBeSet: int32 = nColors
          currentPalette: ptr Palette = currentImage.format.palette
        # one of the guard images (i=25) is only a single transparent pixel
        # this caused SDL_SetPaletteColors to fail, I think because that palette contains only 2 colors
        if (currentPalette.ncolors < nColorsToBeSet):
          nColorsToBeSet = currentPalette.ncolors
        if (setPaletteColors(currentPalette, cast[ptr Color](scolors), 0,
            nColorsToBeSet) != 0):
          sdlperror("SDL_SetPaletteColors")
          quitPoP(1)
    dealloc(scolors)


proc hasTimerStopped(timerIndex: int32): int32 =
  when (USE_COMPAT_TIMER):
    return int32(waitTime[timerIndex] == 0)
  else:
    when (USE_REPLAY):
      if (replaying and skippingReplay) != 0 or isValidateMode:
        return 1
    var
      currentCounter: uint64 = getPerformanceCounter()
      ticksElapsed: int32 = int32((float64(currentCounter) / float64(
          perfCountersPerTick)) - (float64(timerLastCounter[timerIndex]) /
              float64(perfCountersPerTick)))
      overshoot: int32 = ticksElapsed - waitTime[timerIndex]
    if (overshoot >= 0):
      #    float millisecondsElapsed = (currentCounter - timerLastCounter[timerIndex]) * millisecondsPerCounter
      #    printf("timer modd:   frametime (ms) = mod5.1f    fps = mod.1f    timer ticks elapsed = modd\n", timerIndex, millisecondsElapsed, 1000.0f / millisecondsElapsed, ticksElapsed)
      if (overshoot > 0 and overshoot <= 3):
        currentCounter -= uint64(overshoot) * perfCountersPerTick
      timerLastCounter[timerIndex] = currentCounter
      return int32(true)
    else:
      return int32(false)
