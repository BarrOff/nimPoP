when isMainModule:
  import std/[math, os, parsecfg, parseopt, strutils, streams, tables, times]
  from std/sequtils import any, apply
  from std/endians import bigEndian16, bigEndian32, littleEndian16, littleEndian32

  import crc32 except `$` #import crc32FromFile
  import sdl2
  import sdl2/[audio, gamecontroller, haptic, image, joystick]

  import nimPoP/common
  import nimPoP/data
  import nimPoP/seqtbl
  import nimPoP/opl3
  include nimPoP/proto
  include nimPoP/lighting
  include nimPoP/menu
  include nimPoP/options
  include nimPoP/midi
  include nimPoP/seg000
  include nimPoP/seg001
  include nimPoP/seg002
  include nimPoP/seg003
  include nimPoP/seg004
  include nimPoP/seg005
  include nimPoP/seg006
  include nimPoP/seg007
  include nimPoP/seg008
  include nimPoP/seg009

  proc main(): int =
    popMain()
    return 0

  discard main()
