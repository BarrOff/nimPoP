when isMainModule:
  when defined(windows) or defined(linux):
    {.emit: """
      #include <stdint.h>
    """.}
  import crc32 except `$` #import crc32FromFile
  import sdl2/sdl
  import sdl2/sdl_image

  import nimPoP/common
  include nimPoP/proto
  include nimPoP/data
  include nimPoP/lighting
  include nimPoP/menu
  include nimPoP/opl3
  include nimPoP/options
  include nimPoP/midi
  include nimPoP/seqtbl
  include nimPoP/seg000
  include nimPoP/seg001
  include nimPoP/seg002
  include nimPoP/seg003
  include nimPoP/seg004
  include nimPoP/seg005
  include nimPoP/seg006
  include nimPoP/seg007
  include nimPoP/seg008
  include nimPoP/seg009

  proc main(): int =
    popMain()
    return 0

  discard main()
