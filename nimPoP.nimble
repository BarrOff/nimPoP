# Package

version = "0.4.2"
author = "Joachim Kruth"
description = "port of SDLPoP to Nim"
license = "GLPv3"
srcDir = "src"
bin = @["nimPoP"]



# Dependencies

requires "nim >= 1.4.2"
requires "sdl2 >= 2.0.4"
requires "crc32 >= 0.5.0"


const
  sdlpopVersion = "1.23"
  outFile = "SDLPoP-" & sdlpopVersion & ".zip"

# Tasks
task prepare, "download the data for nimPoP":
  type
    DownloadTool = enum
      UseCurl, UseWget, UsePowershell, NoTool
  const
    url = "https://www.popot.org/get_the_games/software/SDLPoP/SDLPoP-" &
        sdlpopVersion & ".zip"
  if dirExists("data") and fileExists("SDLPoP.ini"):
    echo "data directory and config file already exist"
    echo "nothing to prepare"
  else:
    echo "data dir does not exist"
    echo "check if " & outFile & " is available"
    if fileExists(outFile):
      echo "already downloaded " & outFile
    else:
      let
        dt: DownloadTool = when defined(posix):
            if findExe("curl") != "":
              UseCurl
            elif findExe("wget") != "":
              UseWget
            else:
              NoTool
          else:
            UsePowershell
      case dt
      of UseCurl:
        echo "Download using curl..."
        exec "curl -o " & outFile & " " & url
      of UseWget:
        echo "Download using wget..."
        exec "wget " & url
      of UsePowershell:
        echo "Download using powershell..."
        exec "Invoke-Webrequest -Uri " & url & " -OutFile " & outFile
      of NoTool:
        echo "Could not find a tool to download the game data.\n Aborting..."
        quit(1)
    if not(dirExists("data")) and not(fileExists("SDLPoP.ini")):
      echo "unpacking nimPoP game data and config file..."
      exec "unzip -n " & outFile & " SDLPoP-" & sdlpopVersion &
          "/data/* SDLPoP-" & sdlpopVersion & "/SDLPoP.ini"
      mvDir("SDLPoP-" & sdlpopVersion & "/data", "./data")
      mvFile("SDLPoP-" & sdlpopVersion & "/SDLPoP.ini", "SDLPoP.ini")
      rmDir("SDLPoP-" & sdlpopVersion)
    elif not(dirExists("data")):
      echo "unpacking nimPoP game data..."
      exec "unzip -n " & outFile & " SDLPoP-" & sdlpopVersion & "/data/*"
      mvDir("SDLPoP-" & sdlpopVersion & "/data", "./data")
      rmDir("SDLPoP-" & sdlpopVersion)
    elif not(fileExists("SDLPoP.ini")):
      echo "unpacking nimPoP config file..."
      exec "unzip -n " & outFile & " SDLPoP-" & sdlpopVersion & "/SDLPoP.ini"
      mvFile("SDLPoP-" & sdlpopVersion & "/SDLPoP.ini", "SDLPoP.ini")
      rmDir("SDLPoP-" & sdlpopVersion)
    else:
      echo "Downloaded game data, but data and config file already exist."
  echo "ready to build nimPoP"

after clean:
  if dirExists("data"):
    echo "removing data directory..."
    rmDir("data")
  if fileExists(outFile):
    echo "removing downloaded file " & outFile
    rmFile(outFile)

before build:
  prepareTask()
