# Package

version = "0.1.7"
author = "Joachim Kruth"
description = "port of SDLPoP to Nim"
license = "GLPv3"
srcDir = "src"
bin = @["nimPoP"]



# Dependencies

requires "nim >= 1.4.2"
requires "sdl2_nim >= 2.0.5.0"
requires "crc32 >= 0.5.0"
