The following items still do not work as expected:

* sound not playing on start screen and during cut scenes, except when using `stdsnd`
* guard images are not loaded correctly and appear invisible
* guards do not follow into new room
* graphical artifacts, e.g. potions still appearing after being used (they disappear when leaving the room and reentering)
