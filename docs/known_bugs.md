The following items still do not work as expected:

- ~~sound not playing on start screen and during cut scenes, except when using `stdsnd`~~
- ~~some guard images are not loaded correctly and appear invisible (except for the sword)~~
- ~~guards do not follow into new room~~
- ~~guards do not turn around upon sound~~
- ~~guard and kid do not turn around when passing by each other in a sword fight~~
- graphical artifacts, e.g. potions still appearing after being used (they disappear when leaving the room and reentering)
- the shadow does not drink the potion, as he is supposed to
- wrong sprite for skeleton (uses the shadow)
